<?php

use Illuminate\Http\Request;
use App\Log;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('logs', function(){
   $logs = Log::paginate(5);

   return response()->json($logs);
});

Route::get('/invited-suppliers/invite/{id}', 'API\AbstractAPIController@getSuppliers');

Route::post('/new-supplier/invite/', 'API\AbstractAPIController@addNewSupplier');

Route::get('/uninvited-suppliers/invitation/{id}', 'API\AbstractAPIController@getRemainingSuppliers');

Route::get('/abstract/{abstract}/recommendations/supplier/{supplier}', 'API\AbstractAPIController@getRecommendedItems');

Route::get('/supplier/{supplier}', 'API\AbstractAPIController@showSupplier');

Route::get('/app/{year}', 'APPController@getAPPs');

Route::get('/personal-pmr/semester/{semester}/year/{year}/{user_id}', 'API\PMRAPIController@getPersonalPMR');

Route::get('/main-pmr/semester/{semester}/year/{year}', 'API\PMRAPIController@getMainPMR');

Route::get('/abstract/{abstract}', 'API\AbstractAPIController@getAbstract');

Route::get('/not-attached/abstract/{abstract}', 'API\AbstractAPIController@getAbstractSupplierUninvited');

Route::get('/app/{year}', 'API\AnnualProcurementPlanAPIController@getAPP');

Route::get('/uploaded-pmrs/year/{year}/semester/{semester}', 'API\PMRAPIController@getUploadedPMR');