<?php
use App\PurchaseRequest;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Route::middleware(['auth'])->group(function(){

	Route::get('/dashboard', 'HomeController@show_dashboard');

	Route::get('/get-reference/{reference}', 'ReferenceController@download');

	Route::get('/forbidden-access', 'HomeController@showForbidden');

	Route::get('notifications', 'HomeController@showNotifications');

	/** Approved PPMPS **/
	Route::get('/ppmps/approved', 'PPMPController@allApproved');

   /*********
	** APP **
	*********/

	Route::get('generate-app', 'APPController@generateApp');
	Route::get('/app/all', 'APPController@indexApp');
	Route::get('/app/view/{year}', 'APPController@showApp');
	Route::get('/app/edit/{year}', 'APPController@edit');
	Route::put('/app/update/{year}', 'APPController@update');
	Route::get('/app/moving', 'APPController@movingApp');

	Route::get('/ppmps/moving', 'PPMPController@movingPPMP');
	Route::get('/ppmps/{ppmp}/show-moving', 'PPMPController@showMoving');
	Route::get('/ppmps/{ppmp}/delete', 'PPMPController@delete');
	Route::post('/import/ppmp', 'PPMPController@import');
	Route::post('/upload-app', 'APPController@import');

	Route::get('/app', 'APPController@newAPPShow');

	Route::get('/app/template-1', 'APPController@downloadTemplate');
	Route::get('/app/template-2', 'APPController@downloadDeductedTemplate');
	Route::post('/app/import_deducted_app', 'APPController@import_deducted_app');

	
	/*************************
	** Resource Controllers **
	**************************/
	Route::resource('user', 'UserController');
	Route::resource('/ppmps', 'PPMPController');
	Route::resource('pr', 'PurchaseRequestController');
	Route::resource('supplier', 'SupplierController');
	Route::resource('announcement', 'AnnouncementController');
	Route::resource('reference', 'ReferenceController');

	Route::put('announcement/{id}', 'AnnouncementController@update');

	/************************
	** PPMP Related Routes **
	*************************/
	Route::group(['prefix' => '/ppmps/'], function (){
		Route::get('{id}/approve', 'PPMPController@approvePpmp');
		Route::put('{id}/reject', 'PPMPController@rejectPpmp');
	});

	/** Download PR File Attachment **/
	Route::get('/pr/download/{id}', 'PurchaseRequestController@download_attachment');

	/** Download Invitation File Attachment **/
	Route::get('/invitation/download/{id}', 'InvitationController@download_attachment');

	// PR HARD COPY RECEIVED
	Route::post('/pr/datereceived/{id}', 'PurchaseRequestController@setDateReceived');
	Route::get('/pr/hardcopyreceived/{id}', 'PurchaseRequestController@hardCopyReceived');

	/** Import PR **/
	Route::post('/import/pr', 'PurchaseRequestController@import');
	Route::get('/download-pr-template', 'PurchaseRequestController@download_template')->name('download-pr-template');

	/**********
	** Users **
	***********/
	Route::get('/user/{id}/deactivate', 'UserController@deactivate');
	Route::get('/user/{id}/activate', 'UserController@activate');

	/***************
	** User Roles **
	****************/
	Route::get('/user-roles', 'UserRoleController@index');
	Route::put('/user-roles/post', 'UserRoleController@update');

   /**************************
	** Mode of Procurements **
	**************************/
	Route::get('/procurement-modes/edit', 'ProcurementModeController@edit');
	Route::put('/procurement-modes/update', 'ProcurementModeController@update');
	Route::get('/procurement-modes/create', 'ProcurementModeController@create');
	Route::post('/procurement-modes/store', 'ProcurementModeController@store');

    /************************
	** Exporting to a file **
	*************************/
	Route::get('/export/ppmp/{id}', 'ExportAsController@export_ppmp');
	Route::get('/export/app/csv/{year}', 'ExportAsController@export_app_csv');
	Route::get('/export/pdf/pr/{id}', 'PurchaseRequestController@generate_pdf');
	Route::get('/export/docx/pr/{id}', 'ExportAsController@pr_generate_docx');
	Route::get('/export/pdf/aoq/{id}', 'ExportAsController@aoq_generate_pdf');
	Route::get('/export/excel/aoq/{id}', 'ExportAsController@aoq_generate_excel');
	Route::get('/export/docx/ntp/jo/{id}', 'ExportAsController@ntp_generate_docx');
	// Route::get('/export/docx/ntp/jo/{jo_id}/supplier/{sup_id}', 'ExportAsController@ntp_generate_per_item_docx');
	// Route::get('/export/docx/noa/jo/{jo_id}/supplier/{sup_id}', 'ExportAsController@noa_generate_per_item_docx');
	Route::get('/export/docx/noa/jo/{id}', 'ExportAsController@noa_generate_docx');
	Route::get('/export/docx/rfq/{id}', 'ExportAsController@generic_rfq_generate_docx');
	Route::get('/export/docx/rfq/{rfq_id}/supplier/{supplier_id}', 'ExportAsController@exclusive_rfq_generate_docx');
	Route::get('/export/docx/jo-po/{id}', 'ExportAsController@generate_jo');
	Route::get('/export/app/{year}', 'ExportAsController@export_app');
	Route::get('/export/xls/pr/{id}', 'ExportAsController@export_pr');
	Route::get('/export/docx/itb/{itb}', 'ExportAsController@exportITB');
	Route::get('/export/docx/rei/{rei}', 'ExportAsController@exportREI');
	Route::get('/export/xls/rfq/{rfq}', 'ExportAsController@exportRFQ');
	Route::get('/export/xls/rfp/{rfp}', 'ExportAsController@exportRFP');
	Route::get('/export/xls/po/{po}', 'ExportAsController@exportPO');
	Route::get('/export/xls/jo/{jo}', 'ExportAsController@exportJO');
	Route::get('/export/docx/contract/{contract}', 'ExportAsController@exportContract');
	Route::get('/export/pmr/personal/{pmr}', 'ExportAsController@exportPersonalPMR');
	Route::get('/export/main-pmr/semester/{semester}/year/{year}', 'ExportAsController@exportMainPMR');


	//consolidate prs
	Route::post('/export/consolidate/prs', 'ExportAsController@consolidate_pr');

	/**********************
	** PR Approve/Reject **
	***********************/
	Route::get('/pr/approve/{id}', 'PurchaseRequestController@pr_approve');
	Route::put('/pr/reject/{id}', 'PurchaseRequestController@pr_reject');

	/****************************
	** Assign Procurement User **
	*****************************/
	Route::post('/pr/assign/{id}', 'PurchaseRequestController@assign_proc');

	//delete pr file attachment
	Route::get('/pr/delete-attachment/{id}', 'PurchaseRequestController@deleteAttachment');

	/********
	** RFQ **
	*********/
	Route::get('/rfq/create/{id}', 'RFQController@create');
	Route::post('rfq/store/', 'RFQController@store');
	Route::get('/rfq', 'RFQController@index');
	Route::get('/rfq/{id}', 'RFQController@show');
	Route::get('/rfq/{id}/edit', 'RFQController@edit');
	Route::put('/rfq/{id}/update', 'RFQController@update');
	Route::get('/rfq/signed/{rfq}', 'RFQController@signed');

	/********
	** RFP **
	*********/
	Route::get('/rfp/create/{pr_id}', 'RFPController@create');
	Route::post('rfp/store/', 'RFPController@store');
	Route::get('/rfp/{rfp}', 'RFPController@show');
	Route::get('/rfp/{rfp}/edit', 'RFPController@edit');
	Route::put('/rfp/{rfp}/update', 'RFPController@update');
	Route::get('/rfp/signed/{rfp}', 'RFPController@signed');

	/********
	** REI **
	*********/
	Route::get('/rei/create/{pr_id}', 'ReqExpInterestController@create');
	Route::post('rei/store', 'ReqExpInterestController@store');
	Route::get('/rei/{reqExpInterest}', 'ReqExpInterestController@show');
	Route::get('/rei/{reqExpInterest}/edit', 'ReqExpInterestController@edit');
	Route::put('/rei/{reqExpInterest}/update', 'ReqExpInterestController@update');
	Route::get('/rei/signed/{rei}', 'ReqExpInterestController@signed');


	/*******************
	 * Abstract Parent *
	 *******************/
	Route::get('/abstracts', 'AbstractParentController@index');
	Route::get('/cancel/abstract/{abstract}', 'AbstractParentController@cancel');

	/********
	 * AOQ *
	 *******/
	Route::get('/aoq/create/{invitation}', 'AOQController@create');
	Route::get('/aoq/{aoq}/edit/', 'AOQController@edit');
	Route::post('/aoq/store/', 'AOQController@store');
	Route::get('/aoq/{aoq}/', 'AOQController@show');
	Route::get('/export/aoq/{aoq}', 'ExportAsController@exportAOQ');
	Route::put('/aoq/abstract/{abstract}', 'AOQController@update');

	/********
	 * AOB *
	 *******/
	Route::get('/aob/create/{invitation}', 'AOBController@create');
	Route::post('/aob/store/', 'AOBController@store');
	Route::get('/aob/{aob}', 'AOBController@show');
	Route::get('/aob/{aob}/edit','AOBController@edit');
	Route::put('/aob/{aob}', 'AOBController@update');
	Route::get('/export/aob/{aob}', 'ExportAsController@exportAOB');
	Route::put('/aob/abstract/{abstract}', 'AOBController@update');

	/********
	 * AOP *
	 *******/
	Route::get('/aop/create/{invitation}', 'AOPController@create');
	Route::post('/aop/store/', 'AOPController@store');
	Route::get('/aop/{aop}/', 'AOPController@show');
	Route::get('/export/aop/{aop}', 'ExportAsController@exportAOP');
	Route::get('/aop/{aop}/edit', 'AOPController@edit');
	Route::put('/aop/abstract/{abstract}', 'AOPController@update');

	/***************
	** INVITATION **
	***************/
	Route::get('/invitations', 'InvitationController@index');
	Route::get('/invitations/approve/{invitation}', 'InvitationController@approve');
	Route::put('/invitations/reject/{invitation}', 'InvitationController@reject');
	Route::get('/invitations/cancel/{invitation}', 'InvitationController@cancel');

	// Attach Invitation Forms to Suppliers
	Route::post('/invite-suppliers', 'InvitationController@attachSupplier')->name('invite-suppliers');

	/*************
	** Supplier **
	**************/	
	Route::get('/supplier/{supplier}/delete', 'SupplierController@destroy');

	/*********************
	** Generate RFQ PDF **
	**********************/
	Route::get('/generate/pdf/rfq/{id}', 'RFQController@generate_pdf');
	Route::get('/generate/pdf/rfq/{rfq_id}/supplier/{supplier_id}', 'RFQController@generate_exclusive_pdf');

	/************
	** JO / PO **
	*************/
	Route::get('/orders/all', 'OrdersParentController@index');
	Route::get('/po/create/{abstract}', 'PurchaseOrderController@create');
	Route::post('/po/store', 'PurchaseOrderController@store');
	Route::get('/po/{po}', 'PurchaseOrderController@show');
	Route::get('/po/{po}/edit', 'PurchaseOrderController@edit');
	Route::put('/po/{po}', 'PurchaseOrderController@update');
	/********************************************************************************/
	Route::get('/jo/create/{abstract}', 'JobOrderController@create');
	Route::post('/jo/store', 'JobOrderController@store');
	Route::get('/jo/{jo}', 'JobOrderController@show');
	Route::get('/jo/{jo}/edit', 'JobOrderController@edit');
	Route::put('/jo/{jo}/', 'JobOrderController@update');
	Route::post('/attach-files/order/{order}', 'OrdersParentController@attach_files');
	Route::get('/delete-files/order/{id}', 'OrdersParentController@delete_files');
	Route::get('/download-files/order/{id}', 'OrdersParentController@download_files');
	Route::get('/send-notice/{order}', 'OrdersParentController@send_notice');

	/**
	 * 
	 * Contract
	 * 
	 */
	Route::get('contract/create/{abstract}', 'ContractController@create');
	Route::post('/contract/store', 'ContractController@store');
	Route::get('/contract/{contract}', 'ContractController@show');
	Route::get('/contracts', 'ContractController@index');
	Route::get('/contracts/{contract}/edit', 'ContractController@edit');
	Route::put('/contracts/{contract}', 'ContractController@update');

	/*********
	** PMR **
	*********/
	Route::get('/main-pmr', 'PMRController@mainPMRShow');
	Route::get('/pmr/template/download', 'PMRController@downloadTemplate');
	Route::post('/upload-pmr', 'PMRController@import');

	Route::get('/uploaded-pmr', 'UploadedPmrController@index');

	/****************
	 * Personal PMR *
	 ****************/
	Route::get('/personal-pmr', 'PMRController@personalPMRShow');
	Route::get('/personal-pmr/edit/semester/{semester}/year/{year}/', 'PMRController@personalPMREdit');
	Route::put('/personal-pmr/{pmr}', 'PMRController@personalPMRUpdate');
	
	/**
	 * 
	 * Logs
	 * 
	 */
	Route::get('/logs', 'LogsController@index');

	/**
	 * 
	 * Dashboard Links
	 * 
	 */
	Route::get('/download/ppmp', 'HomeController@download_ppmp');
	Route::get('/download/pr', 'HomeController@download_pr');
	Route::get('/download/rfq', 'HomeController@download_rfq');
	Route::get('/download/aoq', 'HomeController@download_aoq');
	Route::get('/download/pojo', 'HomeController@download_pojo');
	Route::get('/download/procurement-manual', 'HomeController@download_proc_manual');
	Route::post('/upload/proc-manual', 'HomeController@upload_proc_manual');

	/*** ITB ***/
	Route::get('/itb/create/pr/{id}', 'InvitationToBidController@create');
	Route::post('/itb/store/', 'InvitationToBidController@store');
	Route::get('/itb/{invitationToBid}', 'InvitationToBidController@show');
	Route::get('/itb/{invitationToBid}/edit', 'InvitationToBidController@edit');
	Route::put('/itb/{invitationToBid}', 'InvitationToBidController@update');
	Route::get('/itb/signed/{invitationToBid}', 'InvitationToBidController@signed');

	/***User Settings***/
	Route::get('/settings', 'SuperAdminControlController@index');
	Route::post('/settings/update', 'SuperAdminControlController@update');

	/***********
	** Logout **
	************/
	Route::get('/logout', function(){

		// $log = 'User ' . Auth::user()->first_name . ' ' . Auth::user()->last_name . ' logged out.';
		// \ActivityLog::add($log);

		$log_arr = [
            "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            "user_office" => Auth::user()->office->office,
            "activity"  => 'logged out.',
        ];
        \ActivityLog::add($log_arr);

        \Session::flush();
		\Auth::logout();
		return redirect('/');
	});

	/**
	 * 
	 * PHPINFO()
	 * 
	 */
	Route::get('/phpinfo', function(){
		return view('info');
	});


	Route::get('/download-ppmp-template', 'HomeController@downloadPPMPImportTemplate');

	Route::post('/import-suppliers', 'SupplierController@import');

});

Auth::routes();