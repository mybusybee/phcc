@if($abstract_form->bid_type === 'As Calculated')
    <tr id="twg-header">
        <th colspan="{{ $colspan }}" style="background-color: #ccc !important; border: 3px solid #000">Technical Working Group</td>
    </tr>
    <tr id="twg-content">
        <td colspan="{{ $colspan }}" class="py-4">
            <div class="row">
                <div class="col-xl-2 offset-xl-1">
                    {{ Form::text('twg_chairman', $abstract_form->twg_chairman, ['class' => 'form-control form-control-sm border border-info', 'readonly']) }}
                    <p class="text-center">TWG Chairperson</p>
                </div>
                <div class="col-xl-2 offset-xl-2">
                    {{ Form::text('twg_member_1', $abstract_form->twg_member_1, ['class' => 'form-control form-control-sm border border-info', 'readonly']) }}
                    <p class="text-center">TWG Member</p>
                </div>
                <div class="col-xl-2 offset-xl-2">
                    {{ Form::text('twg_member_2', $abstract_form->twg_member_2, ['class' => 'form-control form-control-sm border border-info', 'readonly']) }}
                    <p class="text-center">TWG Member</p>
                </div>
            </div>
        </td>
    </tr>
@endif
<tr>
    <th colspan="{{ $colspan }}" style="background-color: #ccc !important; border: 3px solid #000">PCC Bids and Awards Committee:</td>
</tr>
<tr>
    <td colspan="{{ $colspan }}" class="py-4">
        <div class="row mb-3">
            <div class="col-xl-2 offset-xl-3">
                {{ Form::text('pbac_chairperson', $abstract_form->pbac_chairperson, ['class' => 'form-control form-control-sm border border-info', 'readonly']) }}
                <p class="text-center">PBAC Chairperson</p>
            </div>
            <div class="col-xl-2 offset-xl-2">
                {{ Form::text('pbac_vice_chairperson', $abstract_form->pbac_vice_chairperson, ['class' => 'form-control form-control-sm border border-info', 'readonly']) }}
                <p class="text-center">PBAC Vice Chairperson</p>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-2 offset-xl-1">
                {{ Form::text('pbac_member_1', $abstract_form->pbac_member_1, ['class' => 'form-control form-control-sm border border-info', 'readonly']) }}
                <p class="text-center">PBAC Member</p>
            </div>
            <div class="col-xl-2 offset-xl-2">
                {{ Form::text('pbac_member_2', $abstract_form->pbac_member_2, ['class' => 'form-control form-control-sm border border-info', 'readonly']) }}
                <p class="text-center">PBAC Member</p>
            </div>
            <div class="col-xl-2 offset-xl-2">
                {{ Form::text('provisional_member', $abstract_form->provisional_member, ['class' => 'form-control form-control-sm border border-info', 'readonly']) }}
                <p class="text-center">Provisional Member</p>
            </div>
        </div>
    </td>
</tr>
@if($abstract_form->bid_type === 'As Read')
    <tr id="obs-header">
        <th colspan="{{ $colspan }}" style="background-color: #ccc !important; border: 3px solid #000">Observers:</td>
    </tr>
    <tr id="obs-content">
        <td colspan="{{ $colspan }}" class="py-4">
            <div class="row">
                <div class="col-xl-2 offset-xl-1">
                    {{ Form::text('coa_representative', $abstract_form->coa_representative, ['class' => 'form-control form-control-sm border border-info', 'readonly']) }}
                    <p class="text-center">COA Representative</p>
                </div>
                <div class="col-xl-2 offset-xl-2">
                    {{ Form::text('private_observer_1', $abstract_form->private_observer_1, ['class' => 'form-control form-control-sm border border-info', 'readonly']) }}
                    <p class="text-center">Private Observer 1</p>
                </div>
                <div class="col-xl-2 offset-xl-2">
                    {{ Form::text('private_observer_2', $abstract_form->private_observer_2, ['class' => 'form-control form-control-sm border border-info', 'readonly']) }}
                    <p class="text-center">Private Observer 2</p>
                </div>
            </div>
        </td>
    </tr>
@endif