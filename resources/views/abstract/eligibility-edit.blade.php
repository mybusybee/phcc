<template>
    <tr class="eligibility_docs">
        <td colspan="3" rowspan="{{ count($abstract_form->abstract_parent->docs) + 1 }}" class="text-center" id="eli_req_title" style="vertical-align: middle;">ELIGIBILITY REQUIREMENT</td>
        <td colspan="3">
            <input type="hidden" name="eligibility_docs_count[]" id="legal_docs_count" value="0">
            <button type="button" class="add-eligibility-docs btn btn-success btn-xs" style="float: right;" @click="addEligibilityBtn()">+</button>
        </td>
        <template v-for="(supplier, ctr) in getSuppliers">
            <td colspan="2">
                <input type="hidden" :name="`supplier${ctr}`" :id="`supplier${ctr}`" :value="supplier.id">
            </td>
        </template>
    </tr>
    @php $eligibility_docs_counter = 1;  $doc_container_row_no = 1; @endphp
    @foreach($abstract_form->abstract_parent->docs as $doc)
        <tr class="eligibility_docs">
            <td colspan="3" class="eligibility_docs_cell" id="{{ 'eligibility_docs_cell_'.$eligibility_docs_counter }}">
                <input type="hidden" name="eligibility_docs_count[]" id="eligibility_docs_count">
                <button type="button" class="remove-doc-row btn btn-danger btn-xs" style="float: left;">-</button>
                {{ Form::select('eligibility_docs_'.$eligibility_docs_counter, $document_types, $doc->document_type, ['class' => 'document-select form-control form-control-sm border border-info', 'style' => 'width: 95%; float: right;']) }}
                @if($doc->document_type === 'f9')
                    <input type="text" name="{{ 'eligibility_docs_extra_'.$doc_container_row_no }}" id="additional_doc" class="additional_doc form-control form-control-sm border border-info mt-2" style="width:95%; float:right" value="{{ $doc->extra_document }}">
                @endif
            </td>
            <template v-for="(supplier, i_s) in getSuppliers">
                <template v-if="supplier.eligibilities">
                    <template v-for="(eligibility_compliance, i_e) in supplier.eligibilities" v-if="eligibility_compliance.abstract_docs_id === {{ $doc->id }}">
                        <td colspan="2">
                            <select :name="`eligibility_docs_{{ $eligibility_docs_counter }}_supplier${i_s}`" :id="`legal_docs_${i_e}_supplier${i_s}`" :class="`compliance-select compliance_supplier${i_s} form-control form-control-sm border border-info`">
                                <template v-if="eligibility_compliance.eligibility === 'Comply'">
                                    <option selected value="Comply">Comply</option>
                                    <option value="Not Comply">Not Comply</option>
                                </template>
                                <template v-else>
                                    <option value="Comply">Comply</option>
                                    <option value="Not Comply" selected>Not Comply</option>
                                </template>
                            </select>
                        </td>
                    </template>
                </template>
                <template v-else>
                    <td colspan="2">
                        <select :name="`eligibility_docs_{{ $eligibility_docs_counter }}_supplier${i_s}`" :id="`legal_docs_{{ $doc_container_row_no }}_supplier${i_s}`" :class="`compliance-select compliance_supplier${i_s} form-control form-control-sm border border-info`">
                            <option value="" disabled selected>Please select one...</option>
                            <option value="Comply">Comply</option>
                            <option value="Not Comply">Not Comply</option>
                        </select>
                    </td>
                </template>
            </template>
            {{-- @foreach ($doc->supplier_eligibilities as $supplier)
                <td colspan="2">
                    {{ Form::select('eligibility_docs_${eligibility_docs_counter}_supplier${i}', ['' => 'Please select one...', 'Comply' => 'Comply', 'Not Comply' => 'Not Comply'], $supplier->eligibility, ['class' => 'compliance-select compliance_supplier${i} form-control form-control-sm border border-info']) }}
                </td>
            @endforeach --}}
        </tr>
        @php $eligibility_docs_counter++; $doc_container_row_no++; @endphp
    @endforeach
    <tr>
        <td colspan="6">
            <center><strong><p>RESULT</p></strong></center>
        </td>
        <template v-for="(supplier, ctr) in getSuppliers">
            <template v-if="supplier.pivot">
                <td colspan="2">
                    <input type="text" :name="`result_compliance_supplier_id_${supplier.id}`" :id="`result_compliance_supplier${ctr}`" :class="`result_compliance_supplier_id_${supplier.id} form-control form-control-sm text-center`" readonly :value="supplier.pivot.compliance" style="font-weight: bold; font-size: 20px;">
                </td>
            </template>
            <template v-else>
                <td colspan="2">
                    <input type="text" :name="`result_compliance_supplier_id_${supplier.id}`" :id="`result_compliance_supplier${ctr}`" :class="`result_compliance_supplier_id_${supplier.id} form-control form-control-sm text-center`" readonly value="-" style="font-weight: bold; font-size: 20px;">
                </td>
            </template>
        </template>
    </tr>
</template>