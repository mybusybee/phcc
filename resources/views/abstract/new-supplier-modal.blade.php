<div id="newSupplierModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Register New Supplier</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-xl-12">
                        <label for="companyName">Company Name:</label>
                        <input id="companyName" type="text" class="form-control form-control-sm border border-info" v-model="supplier.company_name">
                    </div>
                    <div class="col-xl-12">
                        <label for="authorizedPersonnel">Authorized Personnel:</label>
                        <input type="text" class="form-control form-control-sm border border-info" id="authorizedPersonnel" v-model="supplier.authorized_personnel">
                    </div>
                    <div class="col-xl-12">
                        <label for="address">Address:</label>
                        <input type="text" class="form-control form-control-sm border border-info" id="address" v-model="supplier.address">
                    </div>
                    <div class="col-xl-12">
                        <label for="telephone">Telephone:</label>
                        <input type="text" class="form-control form-control-sm border border-info" v-model="supplier.telephone">
                    </div>
                    <div class="col-xl-12">
                        <label for="email">Email:</label>
                        <input type="text" class="form-control form-control-sm border border-info" v-model="supplier.email">
                    </div>
                    <div class="col-xl-12">
                        <label for="tin">TIN:</label>
                        <input type="text" class="form-control form-control-sm border border-info" v-model="supplier.tin_number">
                    </div>
                    <div class="col-xl-12">
                        <label for="category">Category:</label>
                        <input type="text" class="form-control form-control-sm border border-info" v-model="supplier.category">
                    </div>
                    <div class="col-xl-12">
                        <label for="mayorsPermit">Mayors Permit Expiry Date:</label>
                        <input type="text" class="form-control form-control-sm border border-info" v-model="supplier.mayors_permit">
                    </div>
                    <div class="col-xl-12">
                        <label for="philgepsNumber">PhilGEPS</label>
                        <input type="text" class="form-control form-control-sm border border-info" v-model="supplier.philgeps">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button :class="submitButton.classNames" type="button" @click.stop="submitNewSupplier">@{{ submitButton.value }}</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>