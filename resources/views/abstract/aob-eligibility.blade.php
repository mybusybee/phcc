<tr class="legal_docs">
    <td colspan="3" rowspan="6" class="text-center" id="eli_req_title" style="vertical-align: middle;">ELIGIBILITY REQUIREMENT</td>
    <td colspan="3">
        <input type="hidden" name="legal_docs_count[]" id="legal_docs_count" value="0">
        Class "A" - Legal Documents:
        <button type="button" class="add-legal-docs btn btn-success btn-xs" style="float: right;" @click="addLegalBtn($event)">+</button>
    </td>
    <template v-for="(supplier, ctr) in getSuppliers">
        <td colspan="2">
            <input type="hidden" :name="`supplier${ctr}`" :id="`supplier${ctr}`" :value="supplier.id">
        </td>
    </template>
</tr>
<tr class="tech_docs">
    <td colspan="3">
        <input type="hidden" name="tech_docs_count[]" id="legal_docs_count" value="0">
        Class "A" - Technical Documents:
        <button type="button" class="add-technical-docs btn btn-success btn-xs" style="float: right;" @click="addTechDocsBtn">+</button>
    </td>
    <template v-for="supplier in getSuppliers">
        <td colspan="2"></td>
    </template>
</tr>
<tr class="financial_docs">
    <td colspan="3">
        <input type="hidden" name="financial_docs_count[]" id="legal_docs_count" value="0">
        Class "A" - Financial Documents:
        <button type="button" class="add-finan-docs btn btn-success btn-xs" style="float: right;" @click="addFinancialDocsBtn">+</button>
    </td>
    <template v-for="supplier in getSuppliers">
        <td colspan="2"></td>
    </template>
</tr>
<tr class="classb_docs">
    <td colspan="3">
        <input type="hidden" name="classb_docs_count[]" id="legal_docs_count" value="0">
        Class "B" Document:
        <button type="button" class="add-classb-docs btn btn-success btn-xs" style="float: right;" @click="addClassBDocsBtn">+</button>
    </td>
    <template v-for="supplier in getSuppliers">
        <td colspan="2"></td>
    </template>
</tr>
<tr class="other_docs">
    <td colspan="3">
        <input type="hidden" name="other_docs_count[]" id="legal_docs_count" value="0">
        Other Technical Documents:
        <button type="button" class="add-other-docs btn btn-success btn-xs" style="float: right;" @click="addOthersBtn">+</button>
    </td>
    <template v-for="supplier in getSuppliers">
        <td colspan="2"></td>
    </template>
</tr>
<tr>
    <td colspan="3">
        <center><strong><p>RESULT</p></strong></center>
    </td>
    <template v-for="(supplier, ctr) in getSuppliers">
        <td colspan="2">
            <input type="text" :name="`result_compliance_supplier_id_${supplier.id}`" :id="`result_compliance_supplier${ctr}`" :class="`result_compliance_supplier_id_${supplier.id} form-control form-control-sm text-center`" readonly value="-" style="font-weight: bold; font-size: 20px;">
        </td>
    </template>
</tr>