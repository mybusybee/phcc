<tr class="legal_docs">
    <td colspan="3" rowspan="{{$abstract_form->extra_rows + 5}}" class="text-center" id="eli_req_title" style="vertical-align: middle;">ELIGIBILITY REQUIREMENT</td>
    <td colspan="3">
        <input type="hidden" name="legal_docs_count[]" id="legal_docs_count" value="0">
        Class "A" - Legal Documents:
    </td>
    @foreach($abstract_form->abstract_parent->suppliers as $supplier)
        <td colspan="2"></td>
    @endforeach
</tr>
@foreach ($abstract_form->abstract_parent->docs as $doc)
    @if ($doc->doc_class == 'legal')
    <tr>
        <td colspan="3">
            <input type="text" class="form-control form-control-sm" value="{{$doc->document_name}}" readonly>
        </td>
        @foreach ($doc->supplier_eligibilities as $supplier)
            <td colspan="2">
                <input type="text" class="form-control form-control-sm legal-docs-{{ $supplier->supplier->id }}" value="{{ $supplier->eligibility }}" readonly>
            </td>
        @endforeach
    </tr>
    @endif
@endforeach
<tr class="tech_docs">
    <td colspan="3">
        <input type="hidden" name="tech_docs_count[]" id="legal_docs_count" value="0">
        Class "A" - Technical Documents:
    </td>
    @foreach($abstract_form->abstract_parent->suppliers as $supplier)
        <td colspan="2"></td>
    @endforeach
</tr>
@foreach ($abstract_form->abstract_parent->docs as $doc)
    @if ($doc->doc_class == 'technical')
    <tr>
        <td colspan="3">
            <input type="text" class="form-control form-control-sm" value="{{$doc->document_name}}" readonly>    
        </td>
        @foreach ($doc->supplier_eligibilities as $supplier)
            <td colspan="2">
                <input type="text" class="form-control form-control-sm tech-docs-{{ $supplier->supplier->id }}" value="{{$supplier->eligibility}}" readonly>
            </td>
        @endforeach
    </tr>
    @endif
@endforeach
<tr class="financial_docs">
    <td colspan="3">
        <input type="hidden" name="financial_docs_count[]" id="legal_docs_count" value="0">
        Class "A" - Financial Documents:
    </td>
    @foreach($abstract_form->abstract_parent->suppliers as $supplier)
        <td colspan="2"></td>
    @endforeach
</tr>
@foreach ($abstract_form->abstract_parent->docs as $doc)
    @if ($doc->doc_class == 'financial')
    <tr>
        <td colspan="3">
            <input type="text" class="form-control form-control-sm" value="{{$doc->document_name}}" readonly>    
        </td>
        @foreach ($doc->supplier_eligibilities as $supplier)
            <td colspan="2">
            <input type="text" class="form-control form-control-sm financial-docs-{{ $supplier->supplier->id }}" value="{{$supplier->eligibility}}" readonly>   
            </td>
        @endforeach
    </tr>
    @endif
@endforeach
<tr class="classb_docs">
    <td colspan="3">
        <input type="hidden" name="classb_docs_count[]" id="legal_docs_count" value="0">
        Class "B" Document:
    </td>
    @foreach($abstract_form->abstract_parent->suppliers as $supplier)
        <td colspan="2"></td>
    @endforeach
</tr>
@foreach ($abstract_form->abstract_parent->docs as $doc)
    @if ($doc->doc_class == 'classb')
    <tr>
        <td colspan="3">
            <input type="text" class="form-control form-control-sm" value="{{$doc->document_name}}" readonly>    
        </td>
        @foreach ($doc->supplier_eligibilities as $supplier)
            <td colspan="2">
                <input type="text" class="form-control form-control-sm class-b-docs-{{ $supplier->supplier->id }}" value="{{$supplier->eligibility}}" readonly>   
            </td>
        @endforeach
    </tr>
    @endif
@endforeach
<tr class="other_docs">
    <td colspan="3">
        <input type="hidden" name="other_docs_count[]" id="legal_docs_count" value="0">
        Other Technical Documents:
    </td>
    @foreach($abstract_form->abstract_parent->suppliers as $supplier)
        <td colspan="2"></td>
    @endforeach
</tr>
@foreach ($abstract_form->abstract_parent->docs as $doc)
    @if ($doc->doc_class == 'other')
    <tr>
        <td colspan="3">
            <input type="text" class="form-control form-control-sm" value="{{$doc->document_name}}" readonly>
        </td>
        @foreach ($doc->supplier_eligibilities as $supplier)
            <td colspan="2">
                <input type="text" class="form-control form-control-sm other-docs-{{ $supplier->supplier->id }}" value="{{$supplier->eligibility}}" readonly>
            </td>
        @endforeach
    </tr>
    @endif
@endforeach
<tr>
    <th colspan="6" class="text-center">RESULT</th>
    @foreach ($abstract_form->abstract_parent->suppliers as $supplier)
        <th colspan="2" class="text-center">{{ $supplier->pivot->compliance }}</th>
    @endforeach
</tr>