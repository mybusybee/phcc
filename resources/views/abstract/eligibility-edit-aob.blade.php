@php
    $legal_docs_counter = 1;
    $tech_docs_counter = 1;
    $financial_docs_counter = 1;
    $class_b_docs_counter = 1;
    $other_tech_docs_counter = 1;
@endphp
<tr class="legal_docs">
    <td colspan="3" rowspan="{{ 6 + $legal_docs_counter + $tech_docs_counter + $financial_docs_counter + $class_b_docs_counter + $other_tech_docs_counter }}" class="text-center" id="eli_req_title" style="vertical-align: middle;">ELIGIBILITY REQUIREMENT</td>
    <td colspan="3">
        <input type="hidden" name="legal_docs_count[]" id="legal_docs_count" value="0">
        Class "A" - Legal Documents:
        <button type="button" class="add-legal-docs btn btn-success btn-xs" style="float: right;" @click="addLegalBtn($event)">+</button>
    </td>
    <template v-for="(supplier, ctr) in getSuppliers">
        <td colspan="2">
            <input type="hidden" :name="`supplier${ctr}`" :id="`supplier${ctr}`" :value="supplier.id">
        </td>
    </template>
</tr>
@foreach($abstract_form->legal_docs as $legal_doc)
    <tr class="legal_docs">
        <td colspan="3" class="legal_docs_cell" id="legal_docs_cell_{{ $legal_docs_counter }}">
            <input type="hidden" name="legal_docs_count[]" id="legal_docs_count">
            <button type="button" class="remove-doc-row btn btn-danger btn-xs" style="float: left;">-</button>
            {{ Form::select('legal_docs_'.$legal_docs_counter, ['a1' => 'SEC Registration/DTI Registration/CDA', 'a2' => "Mayor's/Business Permit", 'a3' => 'Tax Clearance', 'a4' => 'PhilGEPS Certificate of Registration & Membership (Platinum)', 'a9' => 'Others'], $legal_doc->document_type, ['class' => 'form-control form-control-sm border border-info document-select', 'style' => 'width: 95%; float: right;']) }}
        </td>
        <template v-for="(supplier, index) in getSuppliers">
            <template v-if="supplier.eligibilities">
                <template v-for="eligibility in supplier.eligibilities" v-if="eligibility.abstract_docs_id === {{ $legal_doc->id }}">
                    <td colspan="2">
                        <select :name="`legal_docs_{{ $legal_docs_counter }}_supplier${index}`" :id="`legal_docs_{{ $legal_docs_counter }}_supplier${index}`" required :class="`compliance-select compliance_supplier${index} form-control form-control-sm border border-info`">
                            <template v-if="eligibility.eligibility === 'Comply'">
                                <option value="" disabled>Select One</option>
                                <option selected value="Comply">Comply</option>
                                <option value="No comply">Not Comply</option>
                            </template>
                            <template v-else>
                                <option value="" disabled>Select One</option>
                                <option value="Comply">Comply</option>
                                <option selected value="No comply">Not Comply</option>
                            </template>
                        </select>
                    </td>
                </template>
            </template>
            <template v-else>
                <td colspan="2">
                    <select :name="`legal_docs_{{ $legal_docs_counter }}_supplier${index}`" :id="`legal_docs_{{ $legal_docs_counter }}_supplier${index}`" required :class="`compliance-select compliance_supplier${index} form-control form-control-sm border border-info`">
                        <option selected value="" disabled>Select One</option>
                        <option value="Comply">Comply</option>
                        <option value="No comply">Not Comply</option>
                    </select>
                </td>
            </template>
        </template>
    </tr>
    @php $legal_docs_counter++; @endphp
@endforeach
<tr class="tech_docs">
    <td colspan="3">
        <input type="hidden" name="tech_docs_count[]" id="legal_docs_count" value="0">
        Class "A" - Technical Documents:
        <button type="button" class="add-technical-docs btn btn-success btn-xs" style="float: right;" @click="addTechDocsBtn">+</button>
    </td>
    <template v-for="supplier in getSuppliers">
        <td colspan="2"></td>
    </template>
</tr>
@foreach ($abstract_form->tech_docs as $tech_doc)
    <tr class="tech_docs">
        <td colspan="3" class="technical_docs_cell" id="technical_docs_cell_{{ $tech_docs_counter }}">
            <input type="hidden" name="tech_docs_count[]" id="legal_docs_count">
            <button type="button" class="remove-doc-row btn btn-danger btn-xs" style="float: left;">-</button>
            {{ Form::select('technical_docs_'.$tech_docs_counter, ['b1' => 'Statement of all Ongoing Government and Private Contracts, including awarded but not yet started', 'b2' => "Statement of Single Largest Completed Contract (SLCC) similar to the contract to be bid", 'b3' => 'PBAC License (Infra.)', 'b9' => 'Others'], $tech_doc->document_type, ['class' => 'form-control form-control-sm border border-info document-select', 'style' => 'width: 95%; float: right;']) }}
        </td>
        <template v-for="(supplier, index) in getSuppliers">
            <template v-if="supplier.eligibilities">
                <template v-for="eligibility in supplier.eligibilities" v-if="eligibility.abstract_docs_id === {{ $tech_doc->id }}">
                    <td colspan="2">
                        <select :name="`technical_docs_{{ $tech_docs_counter }}_supplier${index}`" :id="`technical_docs_{{ $tech_docs_counter }}_supplier${index}`" required :class="`compliance-select compliance_supplier${index} form-control form-control-sm border border-info`">
                            <template v-if="eligibility.eligibility === 'Comply'">
                                <option value="" disabled>Select One</option>
                                <option selected value="Comply">Comply</option>
                                <option value="No comply">Not Comply</option>
                            </template>
                            <template v-else>
                                <option value="" disabled>Select One</option>
                                <option value="Comply">Comply</option>
                                <option selected value="No comply">Not Comply</option>
                            </template>
                        </select>
                    </td>
                </template>
            </template>
            <template v-else>
                <td colspan="2">
                    <select :name="`technical_docs_{{ $tech_docs_counter }}_supplier${index}`" :id="`technical_docs_{{ $tech_docs_counter }}_supplier${index}`" required :class="`compliance-select compliance_supplier${index} form-control form-control-sm border border-info`">
                        <option selected value="" disabled>Select One</option>
                        <option value="Comply">Comply</option>
                        <option value="No comply">Not Comply</option>
                    </select>
                </td>
            </template>
        </template>
    </tr>
    @php $tech_docs_counter++; @endphp
@endforeach
<tr class="financial_docs">
    <td colspan="3">
        <input type="hidden" name="financial_docs_count[]" id="legal_docs_count" value="0">
        Class "A" - Financial Documents:
        <button type="button" class="add-finan-docs btn btn-success btn-xs" style="float: right;" @click="addFinancialDocsBtn">+</button>
    </td>
    <template v-for="supplier in getSuppliers">
        <td colspan="2"></td>
    </template>
</tr>
@foreach($abstract_form->financial_docs as $financial_doc)
    <tr class="financial_docs">
        <td colspan="3" class="financial_docs_cell" id="financial_docs_cell_{{ $financial_docs_counter }}">
            <input type="hidden" name="financial_docs_count[]" id="legal_docs_count">
            <button type="button" class="remove-doc-row btn btn-danger btn-xs" style="float: left;">-</button>
            {{ Form::select('financial_docs_'.$financial_docs_counter, ['c1' => 'Audited Financial Statement (AFS)', 'c2' => 'Net Financial Contracting Capacity (NFCC) / Committed Line of Credit', 'c9' => 'Others'], $financial_doc->document_type, ['class' => 'document-select form-control form-control-sm border border-info', 'style' => 'width: 95%; float: right;']) }}
        </td>
        <template v-for="(supplier, index) in getSuppliers">
            <template v-if="supplier.eligibilities">
                <template v-for="eligibility in supplier.eligibilities" v-if="eligibility.abstract_docs_id === {{ $financial_doc->id }}">
                    <td colspan="2">
                        <select :name="`financial_docs_{{ $financial_docs_counter }}_supplier${index}`" :id="`financial_docs_{{ $financial_docs_counter }}_supplier${index}`" required :class="`compliance-select compliance_supplier${index} form-control form-control-sm border border-info`">
                            <template v-if="eligibility.eligibility === 'Comply'">
                                <option value="" disabled>Select One</option>
                                <option selected value="Comply">Comply</option>
                                <option value="No comply">Not Comply</option>
                            </template>
                            <template v-else>
                                <option value="" disabled>Select One</option>
                                <option value="Comply">Comply</option>
                                <option selected value="No comply">Not Comply</option>
                            </template>
                        </select>
                    </td>
                </template>
            </template>
            <template v-else>
                <td colspan="2">
                    <select :name="`financial_docs_{{ $financial_docs_counter }}_supplier${index}`" :id="`financial_docs_{{ $financial_docs_counter }}_supplier${index}`" required :class="`compliance-select compliance_supplier${index} form-control form-control-sm border border-info`">
                        <option selected value="" disabled>Select One</option>
                        <option value="Comply">Comply</option>
                        <option value="No comply">Not Comply</option>
                    </select>
                </td>
            </template>
        </template>
    </tr>
    @php $financial_docs_counter++; @endphp
@endforeach
<tr class="classb_docs">
    <td colspan="3">
        <input type="hidden" name="classb_docs_count[]" id="legal_docs_count" value="0">
        Class "B" Document:
        <button type="button" class="add-classb-docs btn btn-success btn-xs" style="float: right;" @click="addClassBDocsBtn">+</button>
    </td>
    <template v-for="supplier in getSuppliers">
        <td colspan="2"></td>
    </template>
</tr>
@foreach($abstract_form->class_b_docs as $class_b_doc)
    <tr class="classb_docs">
        <td colspan="3" class="classsb_docs_cell" id="{{ 'classb_docs_cell_'.$class_b_docs_counter }}">
            <input type="hidden" name="classb_docs_count[]" id="legal_docs_count" value="0">
            <button type="button" class="remove-doc-row btn btn-danger btn-xs" style="float: left;">-</button>
            {{ Form::select('classb_docs_'.$class_b_docs_counter, ['d1' => 'Valid Joint Venture Agreement (JVA)', 'd2' => 'N/A', 'c9' => 'Others'], $class_b_doc->document_type, ['class' => 'document-select form-control form-control-sm border border-info', 'style' => 'width: 95%; float: right;']) }}
        </td>
        <template v-for="(supplier, i) in getSuppliers">
            <template v-if="supplier.eligibilities">
                <template v-for="eligibility in supplier.eligibilities" v-if="eligibility.abstract_docs_id === {{ $class_b_doc->id }}">
                    <td colspan="2">
                        <select :name="`classb_docs_{{ $class_b_docs_counter }}_supplier${i}`" :id="`classb_docs_{{ $class_b_docs_counter }}_supplier${i}`" required :class="`compliance-select compliance_supplier${i} form-control form-control-sm border border-info`">
                            <template v-if="eligibility.eligibility === 'Comply'">
                                <option value="" disabled>Select One</option>
                                <option selected value="Comply">Comply</option>
                                <option value="No comply">Not Comply</option>
                                <option value="N/A">N/A</option>
                            </template>
                            <template v-else-if="eligibility.eligibility === 'No comply'">
                                <option value="" disabled>Select One</option>
                                <option value="Comply">Comply</option>
                                <option selected value="No comply">Not Comply</option>
                                <option value="N/A">N/A</option>
                            </template>
                            <template v-else>
                                <option value="" disabled>Select One</option>
                                <option value="Comply">Comply</option>
                                <option value="No comply">Not Comply</option>
                                <option selected value="N/A">N/A</option>
                            </template>
                        </select>
                    </td>
                </template>
            </template>
            <template v-else>
                <td colspan="2">
                    <select :name="`classb_docs_{{ $class_b_docs_counter }}_supplier${i}`" :id="`classb_docs_{{ $class_b_docs_counter }}_supplier${i}`" required :class="`compliance-select compliance_supplier${i} form-control form-control-sm border border-info`">
                        <option selected value="" disabled>Select One</option>
                        <option value="Comply">Comply</option>
                        <option value="No comply">Not Comply</option>
                        <option value="N/A">N/A</option>
                    </select>
                </td>
            </template>
        </template>
    </tr>
    @php $class_b_docs_counter++; @endphp
@endforeach
<tr class="other_docs">
    <td colspan="3">
        <input type="hidden" name="other_docs_count[]" id="legal_docs_count" value="0">
        Other Technical Documents:
        <button type="button" class="add-other-docs btn btn-success btn-xs" style="float: right;" @click="addOthersBtn">+</button>
    </td>
    <template v-for="supplier in getSuppliers">
        <td colspan="2"></td>
    </template>
</tr>
@foreach($abstract_form->other_tech_docs as $other)
    <tr class="other_docs">
        <td colspan="3" class="other_docs_cell" id="other_docs_cell_{{ $other_tech_docs_counter }}">
            <input type="hidden" name="other_docs_count[]" id="legal_docs_count">
            <button type="button" class="remove-doc-row btn btn-danger btn-xs" style="float: left;">-</button>

            {{ Form::select('other_docs_'.$other_tech_docs_counter, ['e1' => 'Bid Security', 'e2' => 'Omnibus Sworn Statement', 'e3' => 'Technical Specifications (for Goods)', 'e4' => 'Project Requirements (for Infra)', 'e5' => 'Corporate Secretary Certificate / Board Resolution for Authorized Representative', 'e9' => 'Others'], $other->document_type, ['class' => 'document-select form-control form-control-sm border border-info', 'style' => 'width: 95%; float: right;']) }}
        </td>
        <template v-for="(supplier, i) in getSuppliers">
            <template v-if="supplier.eligibilities">
                <template v-for="eligibility in supplier.eligibilities" v-if="eligibility.abstract_docs_id === {{ $other->id }}">
                    <td colspan="2">
                        <select :name="`other_docs_{{ $other_tech_docs_counter }}_supplier${i}`" :id="`other_docs_{{ $other_tech_docs_counter }}_supplier${i}`" required :class="`compliance-select compliance_supplier${i} form-control form-control-sm border border-info`">
                            <template v-if="eligibility.eligibility === 'Comply'">
                                <option value="" disabled>Select One</option>
                                <option value="Comply" selected>Comply</option>
                                <option value="No comply">Not Comply</option>
                            </template>
                            <template v-else>
                                <option value="" disabled>Select One</option>
                                <option value="Comply">Comply</option>
                                <option value="No comply" selected>Not Comply</option>
                            </template>
                        </select>
                    </td>
                </template>
            </template>
            <template v-else>
                <td colspan="2">
                    <select :name="`other_docs_{{ $other_tech_docs_counter }}_supplier${i}`" :id="`other_docs_{{ $other_tech_docs_counter }}_supplier${i}`" required :class="`compliance-select compliance_supplier${i} form-control form-control-sm border border-info`">
                        <option value="" disabled selected>Select One</option>
                        <option value="Comply">Comply</option>
                        <option value="No comply">Not Comply</option>
                    </select>
                </td>
            </template>
        </template>
    </tr>
    @php $other_tech_docs_counter++; @endphp
@endforeach
<tr>
    <td colspan="3">
        <center><strong><p>RESULT</p></strong></center>
    </td>
    <template v-for="(supplier, ctr) in getSuppliers">
        <template v-if="supplier.pivot">
            <td colspan="2">
                <input type="text" :name="`result_compliance_supplier_id_${supplier.id}`" :id="`result_compliance_supplier${ctr}`" :class="`result_compliance_supplier_id_${supplier.id} form-control form-control-sm text-center`" readonly :value="supplier.pivot.compliance" style="font-weight: bold; font-size: 20px;">
            </td>
        </template>
        <template v-else>
            <td colspan="2">
                <input type="text" :name="`result_compliance_supplier_id_${supplier.id}`" :id="`result_compliance_supplier${ctr}`" :class="`result_compliance_supplier_id_${supplier.id} form-control form-control-sm text-center`" readonly value="-" style="font-weight: bold; font-size: 20px;">
            </td>
        </template>
    </template>
</tr>