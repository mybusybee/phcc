@extends('layouts.base')
@section('content')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <div class="frm">
        <h2>AOBs/AOPs/AOQs</h2>
        <div class="table-responsive">
            <table class="table table-hover table-striped text-center" id="abstractsTable">
                <thead class="table-primary">
                    <th>Number</th>
                    <th>Date</th>
                    <th>Invitation No.</th>
                    <th>Invitation Date</th>
                    <th>PR No.</th>
                    @if(Auth::user()->user_role === 3)
                        <th>Assigned To</th>
                    @endif
                    <th>Status</th>
                    <th>Action</th>
                </thead>
                <tbody>
                    @foreach ($abstracts as $abstract)
                        <tr id="{{ $abstract->id }}">
                            <td>{{ $abstract->type }}#: {{ $abstract->form_no }}</td>
                            <td>{{ $abstract->created_at->format('F d Y, h:i A') }}</td>
                            <td>{{ $abstract->invite->type }}#: {{ $abstract->invite->form_no }}</td>
                            <td>{{ $abstract->invite->created_at }}</td>
                            <td>{{ $abstract->pr->pr_no }}</td>
                            @if(Auth::user()->user_role === 3)
                                <td>{{ $abstract->pr->proc_user->full_name }}</td>
                            @endif
                            @if ($abstract->pr->personal_pmr_item->status == 'Cancelled' || $abstract->pr->personal_pmr_item->status == 'Failed')
                                <td>CANCELLED</td>
                            @else
                                <td>{{ $abstract->status }}</td>
                            @endif
                            <td>
                                @if ($abstract->pr->personal_pmr_item->status == 'Cancelled' || $abstract->pr->personal_pmr_item->status == 'Failed')
                                    @if ($abstract->type === "AOB")
                                        <a href="{{ url('aob/'.$abstract->aob->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                                    @elseif($abstract->type === "AOQ")
                                        <a href="{{ url('aoq/'.$abstract->aoq->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                                    @elseif($abstract->type === "AOP")
                                        <a href="{{ url('aop/'.$abstract->aop->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                                    @endif
                                @else
                                    @if ($abstract->type === "AOB")
                                        <a href="{{ url('aob/'.$abstract->aob->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                                        <a href="{{ url('aob/'.$abstract->aob->id.'/edit') }}" class="btn btn-info"><i class="fa fa-edit"></i></a>
                                    @elseif($abstract->type === "AOQ")
                                        <a href="{{ url('aoq/'.$abstract->aoq->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                                        <a href="{{ url('aoq/'.$abstract->aoq->id.'/edit') }}" class="btn btn-info"><i class="fa fa-edit"></i></a>
                                    @elseif($abstract->type === "AOP")
                                        <a href="{{ url('aop/'.$abstract->aop->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                                        <a href="{{ url('aop/'.$abstract->aop->id.'/edit') }}" class="btn btn-info"><i class="fa fa-edit"></i></a>
                                    @endif
                                    <div class="input-group">
                                        <select name="" id="" class="form-control formSelector">
                                            <option value="" selected disabled>Please select one...</option>
                                            <option value="PO">PO</option>
                                            <option value="JO">JO</option>
                                            <option value="contract">Contract Agreement Form</option>
                                        </select>
                                        <div class="input-group-append">
                                            <a href="{{ url('/abstracts') }}" class="btn btn-info generateBtn">Generate</a>
                                        </div>
                                    </div>
                                @endif                        
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('js/abstracts/index.js') }}"></script>
@endsection