@if(count($abstract_form->abstract_parent->docs) === 1)
    <tr class="legal_docs">
        <td colspan="3" rowspan="{{ count($abstract_form->abstract_parent->docs) }}" class="text-center" id="eli_req_title" style="vertical-align: middle;">ELIGIBILITY REQUIREMENT</td>
        @foreach ($abstract_form->abstract_parent->docs as $doc)
            <td colspan="3">
                <input type="text" class="form-control form-control-sm" value="{{$doc->document_type}}" readonly>
            </td>
            @foreach ($doc->supplier_eligibilities as $supplier)
                <td colspan="2">
                    <input type="text" class="form-control form-control-sm legal-docs-{{ $supplier->supplier->id }}" value="{{ $supplier->eligibility }}" readonly>
                </td>
            @endforeach
        @endforeach
    </tr>
@else
    <tr class="legal_docs">
        <td colspan="3" rowspan="{{ count($abstract_form->abstract_parent->docs) + 1 }}" class="text-center" id="eli_req_title" style="vertical-align: middle;">ELIGIBILITY REQUIREMENT</td>
    </tr>
    @foreach ($abstract_form->abstract_parent->docs as $doc)
        <tr>
            <td colspan="3">
                <input type="text" class="form-control form-control-sm" value="{{$doc->document_type}}" readonly>
            </td>
            @foreach ($doc->supplier_eligibilities as $supplier)
                <td colspan="2">
                    <input type="text" class="form-control form-control-sm legal-docs-{{ $supplier->supplier->id }}" value="{{ $supplier->eligibility }}" readonly>
                </td>
            @endforeach
        </tr>
    @endforeach
@endif
<tr>
    <th colspan="6" class="text-center">RESULT</th>
    @foreach ($abstract_form->abstract_parent->suppliers as $supplier)
        <th colspan="2" class="text-center">{{ $supplier->pivot->compliance }}</th>
    @endforeach
</tr>