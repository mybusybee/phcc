<div class="modal fade" id="cancelAOBModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Cancel {{strtoupper($abstract_form->abstract_parent->type)}}</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Are you sure?</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
                <a href="{{ url('/cancel/abstract/'.$abstract_form->abstract_parent->id) }}" class="btn btn-danger">Yes</a>
            </div>
        </div>
    </div>
</div>