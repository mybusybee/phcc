<tr id="sig-header">
    {{-- <th :colspan="((getSuppliersCount * 2) + 6)/3" class="col-xl-4 offset-xl-1" style="background-color: #ccc !important; border: 3px solid #000">Canvassed by:</th>
    <th :colspan="((getSuppliersCount * 2) + 6)/3" class="col-xl-4 offset-xl-1" style="background-color: #ccc !important; border: 3px solid #000">Recommending Approval:</th>
    <th :colspan="((getSuppliersCount * 2) + 6)/3" class="col-xl-4 offset-xl-1" style="background-color: #ccc !important; border: 3px solid #000">Approved:</th>style="background-color: #ccc !important; border: 3px solid #000" --}}
    <th colspan="{{ $colspan }}" style="background-color: #ccc !important; border: 3px solid #000" class="py-0">
        <div class="row">
            <div class="col-xl-4">
                <p>Canvassed by:</p>
            </div>
            <div class="col-xl-4" style="border-left: 3px solid #000">
                <p>Recommending Approval:</p>
            </div>
            <div class="col-xl-4" style="border-left: 3px solid #000">
                <p>Approved:</p>
            </div>
        </div>
    </th>
</tr>
<tr id="sig-name">
    <td colspan="{{ $colspan }}" class="py-0">
        <div class="row">
            <div class="col-xl-4" style="padding-top: 10px; padding-bottom: 10px;">
                {{ Form::text('canvassed_by', $abstract_form->canvassed_by, ['class' => 'form-control form-control-sm border border-info w-50 mx-auto', 'placeholder' => 'Insert Name', 'readonly']) }}
            </div>
            <div class="col-xl-4" style="border-left: 3px solid #000; padding-top: 10px; padding-bottom: 10px;">
                {{ Form::text('recommending_approval', $abstract_form->recommending_approval, ['class' => 'form-control form-control-sm border border-info w-50 mx-auto', 'placeholder' => 'Insert Name', 'readonly']) }}
            </div>
            <div class="col-xl-4" style="border-left: 3px solid #000; padding-top: 10px; padding-bottom: 10px;">
                {{ Form::text('approved_by', $abstract_form->approved_by, ['class' => 'form-control form-control-sm border border-info w-50 mx-auto', 'placeholder' => 'Insert Name', 'readonly']) }}
                
            </div>
        </div>
    </td>
</tr>
<tr id="sig-designations">
    <td colspan="{{ $colspan }}" class="py-0">
        <div class="row">
            <div class="col-xl-4"  style="padding-top: 10px; padding-bottom: 10px;">
                {{ Form::text('canvassed_designation', $abstract_form->canvassed_by_designation, ['class' => 'form-control form-control-sm border border-info w-50 mx-auto', 'placeholder' => 'Insert Designation', 'readonly']) }}
            </div>
            <div class="col-xl-4"  style="border-left: 3px solid #000; padding-top: 10px; padding-bottom: 10px;">
                {{ Form::text('recommending_designation', $abstract_form->recommending_designation, ['class' => 'form-control form-control-sm border border-info w-50 mx-auto', 'placeholder' => 'Insert Designation', 'readonly']) }}
            </div>
            <div class="col-xl-4"  style="border-left: 3px solid #000; padding-top: 10px; padding-bottom: 10px;">
                {{ Form::text('approved_by_designation', $abstract_form->approved_by_designation, ['class' => 'form-control form-control-sm border border-info w-50 mx-auto', 'placeholder' => 'Insert Designation', 'readonly']) }}
            </div>
        </div>
    </td>
</tr>
