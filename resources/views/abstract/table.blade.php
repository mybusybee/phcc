@php
    $colspan = 6 + (count($invitation->invite->suppliers) * 2)
@endphp
<div class="form-group mt-3">
    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#newSupplierModal">Add New Supplier</button>
    <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#newRegisteredSupplierModal">Add Existing Supplier</button>
</div>
<div class="table-responsive table-wrapper mt-3 aob-table">
    <table class="table table-striped table-hover custom-table-bordered">
        <thead>
            <tr>
                <th colspan="6"></th>
                <th colspan="2" v-for="(supplier, index) in getSuppliers" class="text-center">
                    <button @click="removeSupplier(index, supplier.id)" class="btn btn-danger" type="button"><i class="fa fa-trash"></i></button>
                    <input type="hidden" name="supplierList[]" class="form-control form-control-sm" :value="supplier.id">
                </th>
            </tr>
            <tr>
                <th rowspan="6" class="text-center" style="width: 8%; vertical-align: middle;">Item No.</th>
                <th rowspan="6" class="text-center" style="width: 8%; vertical-align: middle;">Quantity</th>
                <th rowspan="6" class="text-center" style="width: 7%; vertical-align: middle;">Unit</th>
                <th rowspan="6" class="text-center resize-min-width" style="vertical-align: middle;">Description</th>
                <th colspan="2" class="text-right resize-min-width">Supplier's Name</th>
                <td colspan="2" class="resize-min-width" v-for="supplier in getSuppliers">@{{ supplier.company_name }}</td>
            </tr>
            <tr>
                <th colspan="2" class="text-right" style="vertical-align: middle;">Location Address</th>
                <td colspan="2" v-for="supplier in getSuppliers">@{{ supplier.address }}</td>
            </tr>
            <tr>
                <th colspan="2" class="text-right">Contact No.</th>
                <td colspan="2" v-for="supplier in getSuppliers">@{{ supplier.telephone }}</td>
            </tr>
            <tr>
                <th colspan="2" class="text-right">T.I.N.</th>
                <td colspan="2" v-for="supplier in getSuppliers">@{{ supplier.tin_number }}</td>
            </tr>
            <tr>
                <th colspan="2" class="text-center">Estimated</th>
                <td colspan="2" v-for="suppler in getSuppliers"></td>
            </tr>
            <tr>
                <th class="text-center">Unit Cost</th>
                <th class="text-center">Total</th>
                <template v-for="supplier in getSuppliers">
                    <th class="text-center">Unit Cost</th>
                    <th class="text-center">Total</th>
                </template>
            </tr>
        </thead>
        <tbody>
            <input type="hidden" name="abstract_type" id="abstract_type" class="abstract_type" value="{{$abstract_type}}">
            @if ($invitation->pr->pr_item_total->is_lot_purchase)
                <input type="hidden" name="purchase_type" id="purchase_type" class="purchase_type" value="per lot">
            @elseif ($invitation->pr->pr_item_total->is_per_item_purchase)
                <input type="hidden" name="purchase_type" id="purchase_type" class="purchase_type" value="per item">
            @endif
            <input type="hidden" name="supplier_count" id="supplier_count" value="{{ count($invitation->invite->suppliers) }}">
            @foreach($invitation->pr->pr_items as $item)
                <tr>
                    <td class="item_ids d-none">{{ $item->id }}</td>
                    <td>{{ $item->item_no }}</td>
                    <td id="item_quantity_{{ $item->id }}">{{ $item->quantity }}</td>
                    <td>{{ $item->unit }}</td>
                    <td>{{ $item->description }}</td>
                    <td>{{ $item->est_cost_unit }}</td>
                    @if(!count($item->pr_sub_items))
                        <td id="item_cost_total_{{ $item->id }}">{{ $item->est_cost_total }}</td>
                        @if ($abstract_type === 'aob')
                            <template v-for="(supplier,ctr) in getSuppliers">
                                <td>
                                    <input type="text" :name="`unit_cost_supplier_${supplier.id}_pr_item_{{ $item->id }}`" :id="`unit_cost_supplier_${supplier.id}_pr_item_{{ $item->id }}`" :class="`pr-item-unit-cost pr-item-unit-cost${ctr} form-control form-control-sm`" value="n/a" readonly>
                                </td>
                                <td>
                                    <input type="text" :name="`total_cost_supplier_${supplier.id}_pr_item_{{ $item->id }}`" :id="`total_cost_supplier_${supplier.id}_pr_item_{{ $item->id }}`" :class="`pr-item-total-cost pr-item-total-cost${ctr} pr-item_{{ $item->id }}_total-cost form-control form-control-sm`" readonly value="n/a">
                                </td>
                            </template>
                        @else
                            <template v-for="supplier in getSuppliers">
                                <td>
                                    <input type="text" :name="`unit_cost_supplier_${supplier.id}_pr_item_{{ $item->id }}`" :id="`unit_cost_supplier_${supplier.id}_pr_item_{{ $item->id }}`" class="pr-item-unit-cost form-control form-control-sm" value="0.00">
                                </td>
                                <td>
                                    <input type="text" :name="`total_cost_supplier_${supplier.id}_pr_item_{{ $item->id }}`" :id="`total_cost_supplier_${supplier.id}_pr_item_{{ $item->id }}`" class="pr-item-total-cost pr-item_{{ $item->id }}_total-cost form-control form-control-sm" readonly value="0.00">
                                </td>
                            </template>
                        @endif
                    @else
                        <td></td>
                        <template v-for="supplier in getSuppliers">
                            <td></td>
                            <td></td>
                        </template>
                    @endif
                </tr>
                @if (($invitation->pr->pr_item_total->is_per_item_purchase) && (!count($item->pr_sub_items)))
                    <tr>
                        <td colspan="6" rowspan="1" class="text-center" id="" style="vertical-align: middle;">
                            <center><strong><p>RANKING</p></strong></center>
                        </td>
                        @if ($abstract_type == 'aob')
                            <template v-for="(supplier, ctr) in getSuppliers">
                                <td colspan="2">
                                    <input type="text" :id="`ranking_supplier_${supplier.id}_pr_item_{{ $item->id }}_col_${ctr}`" class="form-control form-control-sm text-center" readonly :name="`ranking_supplier_${supplier.id}_pr_item_{{ $item->id }}_col_${ctr}`" value="N/A" style="font-weight: bold; font-size: 20px;">
                                </td>
                            </template>
                        @else
                            <template v-for="(supplier, ctr) in getSuppliers">
                                <td colspan="2">
                                    <input type="text" :id="`ranking_supplier_${supplier.id}_pr_item_{{ $item->id }}_col_${ctr}`" class="form-control form-control-sm text-center" readonly :name="`ranking_supplier_${supplier.id}_pr_item_{{ $item->id }}_col_${ctr}`" value="-" style="font-weight: bold; font-size: 20px;">
                                </td>
                            </template>
                        @endif
                    </tr>
                @endif
                @if(count($item->pr_sub_items))
                    <input type="hidden" name="sub_item_count" id="sub_item_count" value="{{ count($item->pr_sub_items) }}">
                    @foreach($item->pr_sub_items as $subItem)
                        <tr>
                            <td class="sub_item_ids d-none">{{ $subItem->id }}</td>
                            <td>{{ $subItem->item_no }}</td>
                            <td id="sub_item_quantity_{{ $subItem->id }}">{{ $subItem->quantity }}</td>
                            <td>{{ $subItem->unit }}</td>
                            <td>{{ $subItem->description }}</td>
                            <td>{{ $subItem->est_cost_unit }}</td>
                            <td id="sub_item_cost_total_{{ $subItem->id }}">{{ $subItem->est_cost_total }}</td>
                            <template v-for="(supplier, ctr) in getSuppliers">
                                @if ($abstract_type === 'aob')
                                <td>
                                    <input type="text" :name="`sub_unit_cost_supplier_${supplier.id}_pr_sub_item_{{ $subItem->id }}`" :id="`sub_unit_cost_supplier_${supplier.id}_pr_sub_item_{{ $subItem->id }}`" :class="`pr-sub-item-unit-cost pr-sub-item-unit-cost${ctr} form-control form-control-sm`" value="n/a" readonly>
                                </td>
                                <td>
                                    <input type="text" :name="`sub_total_cost_supplier_${supplier.id}_pr_sub_item_{{ $subItem->id }}`" :id="`sub_total_cost_supplier_${supplier.id}_pr_sub_item_{{ $subItem->id }}`" :class="`pr-sub-item-total-cost pr-sub-item-total-cost${ctr} pr-sub-item_{{ $subItem->id }}_total-cost form-control form-control-sm`" value="n/a" readonly>
                                </td>
                                @else
                                <td>
                                    <input type="text" :name="`sub_unit_cost_supplier_${supplier.id}_pr_sub_item_{{ $subItem->id }}`" :id="`sub_unit_cost_supplier_${supplier.id}_pr_sub_item_{{ $subItem->id }}`" class="pr-sub-item-unit-cost form-control form-control-sm" value="0.00">
                                </td>
                                <td>
                                    <input type="text" :name="`sub_total_cost_supplier_${supplier.id}_pr_sub_item_{{ $subItem->id }}`" :id="`sub_total_cost_supplier_${supplier.id}_pr_sub_item_{{ $subItem->id }}`" class="pr-sub-item-total-cost pr-sub-item_{{ $subItem->id }}_total-cost form-control form-control-sm" value="0.00" readonly>
                                </td>
                                @endif
                            </template>
                        </tr>
                        @if ($invitation->pr->pr_item_total->is_per_item_purchase)
                            <tr>
                                <td colspan="6" rowspan="1" class="text-center" id="" style="vertical-align: middle;">
                                    <center><strong><p>RANKING</p></strong></center>
                                </td>
                                @if ($abstract_type == 'aob')
                                    <template v-for="(supplier, ctr) in getSuppliers">
                                        <td colspan="2">
                                            <input type="text" :name="`ranking_supplier_${supplier.id}_pr_sub_item_{{ $subItem->id }}_col_${ctr}`" :id="`ranking_supplier_${supplier.id}_pr_sub_item_{{ $subItem->id }}_col_${ctr}`" class="form-control form-control-sm text-center" readonly :name="``" value="N/A" style="font-weight: bold; font-size: 20px;">
                                        </td>
                                    </template>
                                @else
                                    <template v-for="(supplier, ctr) in getSuppliers">
                                        <td colspan="2">
                                            <input type="text" :name="`ranking_supplier_${supplier.id}_pr_sub_item_{{ $subItem->id }}_col_${ctr}`" :id="`ranking_supplier_${supplier.id}_pr_sub_item_{{ $subItem->id }}_col_${ctr}`" class=" form-control form-control-sm text-center" readonly value="-" style="font-weight: bold; font-size: 20px;">
                                        </td>
                                    </template>
                                @endif
                            </tr>
                        @endif
                    @endforeach
                @endif
            @endforeach
            @if ($invitation->pr->pr_item_total->is_lot_purchase)
            <tr>
                <td colspan="5" rowspan="1" class="text-center" id="" style="vertical-align: middle;">
                    <center><strong><p>Amount Totals</p></strong></center>
                </td>
                <td>
                    <center><strong><p id="total_estimate">&#8369;{{$invitation->pr->pr_item_total->total_estimate}}</p></strong></center>
                </td>
                <template v-for="(supplier, ctr) in getSuppliers">
                    <td colspan="2">
                        <input type="text" :name="`total_cost_total_supplier${supplier.id}`" :id="`total_cost_total_supplier${supplier.id}`" :class="`total_cost_total_supplier${ctr}`" class="total_cost_total_suppliers form-control form-control-sm text-center" value="0.00" readonly>    
                    </td>
                </template>
            </tr>
            <tr>
                <td colspan="6" rowspan="1" class="text-center" id="" style="vertical-align: middle;">
                    <center><strong><p>RANKING</p></strong></center>
                </td>
                @if ($abstract_type == 'aob')
                    <template v-for="(supplier, ctr) in getSuppliers">
                        <td colspan="2">
                            <input type="text" :id="`supplier_ranking${ctr}`" class="supplier_ranking form-control form-control-sm text-center" readonly :name="`supplier_ranking${supplier.id}`" value="N/A" style="font-weight: bold; font-size: 20px;">
                        </td>
                    </template>
                @else
                    <template v-for="(supplier, ctr) in getSuppliers">
                        <td colspan="2">
                            <input type="text" :id="`supplier_ranking${ctr}`" class="supplier_ranking form-control form-control-sm text-center" readonly :name="`supplier_ranking${supplier.id}`" value="-" style="font-weight: bold; font-size: 20px;">
                        </td>
                    </template>
                @endif
            </tr>
            @endif
            @if ($abstract_type == 'aop')
            <tr>
                <td colspan="6" rowspan="1" id="" style="vertical-align: middle;">
                    <div class="col-xl-6 d-inline-block">
                        <strong><p class="text-right">TECHNICAL PROPOSAL</p></strong>    
                    </div>
                    <div class="col-xl-5 d-inline-block">
                        <div class="input-group input-group-sm w-50">
                            <input type="number" min="0" max="100" step=".001" :id="`tech_proposal_base`" class="tech_proposal_base form-control form-control-sm text-center" :name="`tech_proposal_base`" value="0">
                            <div class="input-group-append">
                                <span class="input-group-text">%</span>
                            </div>
                        </div>
                    </div>
                </td>
                <template v-for="(supplier, ctr) in getSuppliers">
                    <td colspan="2">
                        <input type="hidden" :id="`tech_proposal_res_${ctr}`" value="FAILED">
                        <div class="input-group input-group-sm mx-auto w-50">
                            <input type="number" min="0" step=".001" max="100" :id="`tech_proposal_${ctr}`" class="tech_proposals form-control form-control-sm text-center" :name="`tech_proposal_supplier${supplier.id}`" value="0">
                            <div class="input-group-append">
                                <span class="input-group-text">%</span>
                            </div>
                        </div>
                    </td>
                </template>
            </tr>    
            @endif
            @if ($abstract_type == 'aob')
                @include('abstract.aob-eligibility')
            @else
                @include('abstract.eligibility-create')
            @endif
            <tr>
                <td :colspan="(getSuppliersCount * 2) + 6">
                    <div class="row">
                        <div class="col-xl-12">
                            <p><strong>Recommendations:</strong></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-3">
                            @if ($abstract_type == 'aob' || $abstract_type == 'aoq')
                                <p id="bidder_grammar" style="text-indent: 2%">Single/Lowest Calculated and Responsive Bidder</p>
                            @elseif ($abstract_type == 'aop')
                                <p id="bidder_grammar" style="text-indent: 2%">Single/Highest Rated and Responsive Bidder</p>
                            @endif
                        </div>
                        <div class="col-xl-2">
                            <p>for Item/Lot #:</p>
                        </div>
                        <div class="col-xl-3">
                            <p>Sub-Total Amount:</p>
                        </div>
                        <div class="col-xl-3">
                            <p>Remarks:</p>
                        </div>
                        <div class="col-xl-1">
                        </div>
                    </div>
                    <div class="reco-section mb-2">
                        <input type="hidden" name="reco_row_count[]" id="reco_row_count" value="0">
                        <div class="row reco-row-base d-none">
                            <div class="col-xl-3">
                                <select class="form-control form-control-sm border border-info w-50" name="reco_supplier" id="reco_supplier">
                                    <option v-for="supplier in getSuppliers" :key="supplier.id" :value="supplier.id">@{{ supplier.company_name }}</option>
                                </select>
                                <select class="form-control form-control-sm border border-info w-50" name="reco_supplier_updated" id="reco_supplier_updated">
                                    
                                </select>
                            </div>
                            <div class="col-xl-3">
                                <select name="reco_item_no" id="reco_item_no" class="form-control form-control-sm border border-info w-25">
                                @foreach ($invitation->pr->pr_items as $item)
                                    @if (count($item->pr_sub_items) && ($invitation->pr->pr_item_total->is_per_item_purchase))
                                        @foreach ($item->pr_sub_items as $sub_item)
                                            <option value="{{$sub_item->id}}">{{$sub_item->item_no}}</option>
                                        @endforeach
                                    @else
                                        <option value="{{$item->id}}">{{$item->item_no}}</option>
                                    @endif
                                @endforeach
                                <select name="reco_item_no_updated" id="reco_item_no_updated" class="form-control form-control-sm border border-info w-25">
                                    
                                </select>
                            </div>
                            <div class="col-xl-3">
                                {{ Form::text('', null, ['class' => 'form-control form-control-sm border border-info w-50']) }}
                            </div>
                            <div class="col-xl-3">
                                {{ Form::text('', null, ['class' => 'form-control form-control-sm border border-info']) }}
                            </div>
                        </div>    
                    </div>
                    <div class="row">
                        <div class="col-xl-12">
                            <button type="button" class="add-recommendation-row btn btn-success btn-xs" style="float: right;">+</button>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td :colspan="(getSuppliersCount * 2) + 6">
                    <div class="row">
                        <div class="col-xl-2 offset-xl-3 text-right">
                            <p><strong>TOTAL:</strong></p>
                        </div>
                        <div class="col-xl-3">
                            {{ Form::text('reco_total', '0.00', ['class' => 'form-control form-control-sm border border-info w-50', 'readonly', 'id' => 'reco_total']) }}
                        </div>
                    </div>
                </td>
            </tr>
            @if ($abstract_type == 'aob')
                @include('abstract.aob-signatories')
            @else
                @include('abstract.signatories-create')
            @endif
        </tbody>
    </table>
</div>