@php
    $colspan = 6 + (count($abstract_form->abstract_parent->suppliers) * 2)
@endphp
<div class="form-group mt-3">
    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#newSupplierModal">Add New Supplier</button>
    <button class="btn btn-primary" type="button" data-toggle="modal" data-target="#newRegisteredSupplierModal">Add Existing Supplier</button>
</div>
<div class="table-responsive table-wrapper mt-3 aob-table">
    <table class="table table-striped table-hover custom-table-bordered">
        <thead>
            <tr>
                <th colspan="6"></th>
                <th colspan="2" v-for="(supplier, index) in getSuppliers" class="text-center">
                    <button @click="removeSupplier(index)" class="btn btn-danger" type="button"><i class="fa fa-trash"></i></button>
                    <input type="hidden" name="supplierList[]" class="form-control form-control-sm" :value="supplier.id">
                </th>
            </tr>
            <tr>
                <th rowspan="6" class="text-center" style="width: 8%; vertical-align: middle;">Item No.</th>
                <th rowspan="6" class="text-center" style="width: 8%; vertical-align: middle;">Quantity</th>
                <th rowspan="6" class="text-center" style="width: 7%; vertical-align: middle;">Unit</th>
                <th rowspan="6" class="text-center resize-min-width" style="vertical-align: middle;">Description</th>
                <th colspan="2" class="text-right resize-min-width">Supplier's Name</th>
                <td colspan="2" class="resize-min-width" v-for="supplier in getSuppliers">@{{ supplier.company_name }}</td>
            </tr>
            <tr>
                <th colspan="2" class="text-right" style="vertical-align: middle;">Location Address</th>
                <td colspan="2" v-for="supplier in getSuppliers">@{{ supplier.address }}</td>
            </tr>
            <tr>
                <th colspan="2" class="text-right">Contact No.</th>
                <td colspan="2" v-for="supplier in getSuppliers">@{{ supplier.telephone }}</td>
            </tr>
            <tr>
                <th colspan="2" class="text-right">T.I.N.</th>
                <td colspan="2" v-for="supplier in getSuppliers">@{{ supplier.tin_number }}</td>
            </tr>
            <tr>
                <th colspan="2" class="text-center">Estimated</th>
                <td colspan="2" v-for="suppler in getSuppliers"></td>
            </tr>
            <tr>
                <th class="text-center">Unit Cost</th>
                <th class="text-center">Total</th>
                <template v-for="supplier in getSuppliers">
                    <th class="text-center">Unit Cost</th>
                    <th class="text-center">Total</th>
                </template>
            </tr>
        </thead>
        <tbody>
            <input type="hidden" name="supplier_count" id="supplier_count" value="{{ count($abstract_form->abstract_parent->suppliers) }}">
            <input type="hidden" name="abstract_type" id="abstract_type" class="abstract_type" value="{{$abstract_type}}">
            @if ($abstract_form->pr->pr_item_total->is_lot_purchase)
                <input type="hidden" name="purchase_type" id="purchase_type" class="purchase_type" value="per lot">
            @elseif ($abstract_form->pr->pr_item_total->is_per_item_purchase)
                <input type="hidden" name="purchase_type" id="purchase_type" class="purchase_type" value="per item">
            @endif
            @foreach($abstract_form->pr->pr_items as $item)
                <tr>
                    <td class="item_ids d-none">{{ $item->id }}</td>
                    <td>{{ $item->item_no }}</td>
                    <td id="item_quantity_{{ $item->id }}">{{ $item->quantity }}</td>
                    <td>{{ $item->unit }}</td>
                    <td>{{ $item->description }}</td>
                    <td>{{ $item->est_cost_unit }}</td>
                    @if(!count($item->pr_sub_items))
                        <td id="item_cost_total_{{ $item->id }}">{{ $item->est_cost_total }}</td>
                        @if ($abstract_type === 'aob')
                            <template v-for="(supplier,ctr) in getSuppliers">
                                <template v-if="supplier.items">
                                    <template v-for="item in supplier.items" v-if="item.id === {{ $item->id }}">
                                        <td>
                                            <input type="text" :name="`unit_cost_supplier_${supplier.id}_pr_item_{{ $item->id }}`" :id="`unit_cost_supplier_${supplier.id}_pr_item_{{ $item->id }}`" :class="`pr-item-unit-cost pr-item-unit-cost${ctr} form-control form-control-sm`" :value="item.pivot.unit_cost">
                                        </td>
                                        <td>
                                            <input type="text" :name="`total_cost_supplier_${supplier.id}_pr_item_{{ $item->id }}`" :id="`total_cost_supplier_${supplier.id}_pr_item_{{ $item->id }}`" :class="`pr-item-total-cost pr-item-total-cost${ctr} pr-item_{{ $item->id }}_total-cost form-control form-control-sm`" :value="item.pivot.total">
                                        </td>
                                    </template>
                                </template>
                                <template v-else>
                                    <td>
                                        <input type="text" :name="`unit_cost_supplier_${supplier.id}_pr_item_{{ $item->id }}`" :id="`unit_cost_supplier_${supplier.id}_pr_item_{{ $item->id }}`" :class="`pr-item-unit-cost pr-item-unit-cost${ctr} form-control form-control-sm`" value="n/a" readonly>
                                    </td>
                                    <td>
                                        <input type="text" :name="`total_cost_supplier_${supplier.id}_pr_item_{{ $item->id }}`" :id="`total_cost_supplier_${supplier.id}_pr_item_{{ $item->id }}`" :class="`pr-item-total-cost pr-item-total-cost${ctr} pr-item_{{ $item->id }}_total-cost form-control form-control-sm`" readonly value="n/a">
                                    </td>
                                </template>
                            </template>
                        @else
                            <template v-for="supplier in getSuppliers">
                                <template v-if="supplier.items">
                                    <template v-for="item in supplier.items" v-if="item.id === {{ $item->id }}">
                                        <td>
                                            <input type="text" :name="`unit_cost_supplier_${supplier.id}_pr_item_{{ $item->id }}`" :id="`unit_cost_supplier_${supplier.id}_pr_item_{{ $item->id }}`" class="pr-item-unit-cost form-control form-control-sm" :value="item.pivot.unit_cost">
                                        </td>
                                        <td>
                                            <input type="text" :name="`total_cost_supplier_${supplier.id}_pr_item_{{ $item->id }}`" :id="`total_cost_supplier_${supplier.id}_pr_item_{{ $item->id }}`" class="pr-item-total-cost pr-item_{{ $item->id }}_total-cost form-control form-control-sm" readonly :value="item.pivot.total">
                                        </td>
                                    </template>
                                </template>
                                <template v-else>
                                    <td>
                                        <input type="text" :name="`unit_cost_supplier_${supplier.id}_pr_item_{{ $item->id }}`" :id="`unit_cost_supplier_${supplier.id}_pr_item_{{ $item->id }}`" class="pr-item-unit-cost form-control form-control-sm" value="0.00">
                                    </td>
                                    <td>
                                        <input type="text" :name="`total_cost_supplier_${supplier.id}_pr_item_{{ $item->id }}`" :id="`total_cost_supplier_${supplier.id}_pr_item_{{ $item->id }}`" class="pr-item-total-cost pr-item_{{ $item->id }}_total-cost form-control form-control-sm" readonly value="0.00">
                                    </td>
                                </template>
                            </template>
                        @endif
                    @else
                        <td></td>
                        <template v-for="supplier in getSuppliers">
                            <td></td>
                            <td></td>
                        </template>
                    @endif
                </tr>
                @if (($abstract_form->pr->pr_item_total->is_per_item_purchase) && (!count($item->pr_sub_items)))
                    <tr>
                        <td colspan="6" rowspan="1" class="text-center" id="" style="vertical-align: middle;">
                            <center><strong><p>RANKING</p></strong></center>
                        </td>
                        @if ($abstract_type == 'aob')
                            <template v-for="(supplier, ctr) in getSuppliers">
                                <template v-if="supplier.items">
                                    <template v-for="item in supplier.items" v-if="item.id === {{ $item->id }}">
                                        <td colspan="2">
                                            <input type="text" :id="`ranking_supplier_${supplier.id}_pr_item_{{ $item->id }}_col_${ctr}`" class="form-control form-control-sm text-center" readonly :name="`ranking_supplier_${supplier.id}_pr_item_{{ $item->id }}_col_${ctr}`" :value="item.pivot.per_item_ranking" style="font-weight: bold; font-size: 20px;">
                                        </td>
                                    </template>
                                </template>
                                <template v-else>
                                    <td colspan="2">
                                        <input type="text" :id="`ranking_supplier_${supplier.id}_pr_item_{{ $item->id }}_col_${ctr}`" class="form-control form-control-sm text-center" readonly :name="`ranking_supplier_${supplier.id}_pr_item_{{ $item->id }}_col_${ctr}`" value="N/A" style="font-weight: bold; font-size: 20px;">
                                    </td>
                                </template>
                            </template>
                        @else
                            <template v-for="(supplier, ctr) in getSuppliers">
                                <template v-if="supplier.items">
                                    <template v-for="item in supplier.items" v-if="item.id === {{ $item->id }}">
                                        <td colspan="2">
                                            <input type="text" :id="`ranking_supplier_${supplier.id}_pr_item_{{ $item->id }}_col_${ctr}`" class="form-control form-control-sm text-center" readonly :name="`ranking_supplier_${supplier.id}_pr_item_{{ $item->id }}_col_${ctr}`" :value="item.pivot.per_item_ranking" style="font-weight: bold; font-size: 20px;">
                                        </td>
                                    </template>
                                </template>
                                <template v-else>
                                    <td colspan="2">
                                        <input type="text" :id="`ranking_supplier_${supplier.id}_pr_item_{{ $item->id }}_col_${ctr}`" class="form-control form-control-sm text-center" readonly :name="`ranking_supplier_${supplier.id}_pr_item_{{ $item->id }}_col_${ctr}`" value="-" style="font-weight: bold; font-size: 20px;">
                                    </td>
                                </template>
                            </template>
                        @endif
                    </tr>
                @endif
                @if(count($item->pr_sub_items))
                    <input type="hidden" name="sub_item_count" id="sub_item_count" value="{{ count($item->pr_sub_items) }}">
                    @foreach($item->pr_sub_items as $subItem)
                        <tr>
                            <td class="sub_item_ids d-none">{{ $subItem->id }}</td>
                            <td>{{ $subItem->item_no }}</td>
                            <td id="sub_item_quantity_{{ $subItem->id }}">{{ $subItem->quantity }}</td>
                            <td>{{ $subItem->unit }}</td>
                            <td>{{ $subItem->description }}</td>
                            <td>{{ $subItem->est_cost_unit }}</td>
                            <td id="sub_item_cost_total_{{ $subItem->id }}">{{ $subItem->est_cost_total }}</td>
                            <template v-for="(supplier, ctr) in getSuppliers">
                                <template v-if="supplier.items">
                                    <template v-for="item in supplier.items" v-if="item.id === {{ $subItem->id }}">
                                        @if($abstract_type === 'aob')
                                            <td>
                                                <input type="text" :name="`sub_unit_cost_supplier_${supplier.id}_pr_sub_item_{{ $subItem->id }}`" :id="`sub_unit_cost_supplier_${supplier.id}_pr_sub_item_{{ $subItem->id }}`" :class="`pr-sub-item-unit-cost pr-sub-item-unit-cost${ctr} form-control form-control-sm`" :value="item.pivot.unit_cost">
                                            </td>
                                            <td>
                                                <input type="text" :name="`sub_total_cost_supplier_${supplier.id}_pr_sub_item_{{ $subItem->id }}`" :id="`sub_total_cost_supplier_${supplier.id}_pr_sub_item_{{ $subItem->id }}`" :class="`pr-sub-item-total-cost pr-sub-item-total-cost${ctr} pr-sub-item_{{ $subItem->id }}_total-cost form-control form-control-sm`" :value="item.pivot.total" readonly>
                                            </td>
                                        @else
                                            <td>
                                                <input type="text" :name="`sub_unit_cost_supplier_${supplier.id}_pr_sub_item_{{ $subItem->id }}`" :id="`sub_unit_cost_supplier_${supplier.id}_pr_sub_item_{{ $subItem->id }}`" class="pr-sub-item-unit-cost form-control form-control-sm" :value="item.pivot.unit_cost">
                                            </td>
                                            <td>
                                                <input type="text" :name="`sub_total_cost_supplier_${supplier.id}_pr_sub_item_{{ $subItem->id }}`" :id="`sub_total_cost_supplier_${supplier.id}_pr_sub_item_{{ $subItem->id }}`" class="pr-sub-item-total-cost pr-sub-item_{{ $subItem->id }}_total-cost form-control form-control-sm" :value="item.pivot.total" readonly>
                                            </td>
                                        @endif
                                    </template>
                                </template>
                                <template v-else>
                                    @if($abstract_type === 'aob')
                                        <td>
                                            <input type="text" :name="`sub_unit_cost_supplier_${supplier.id}_pr_sub_item_{{ $subItem->id }}`" :id="`sub_unit_cost_supplier_${supplier.id}_pr_sub_item_{{ $subItem->id }}`" :class="`pr-sub-item-unit-cost pr-sub-item-unit-cost${ctr} form-control form-control-sm`" value="0.00">
                                        </td>
                                        <td>
                                            <input type="text" :name="`sub_total_cost_supplier_${supplier.id}_pr_sub_item_{{ $subItem->id }}`" :id="`sub_total_cost_supplier_${supplier.id}_pr_sub_item_{{ $subItem->id }}`" :class="`pr-sub-item-total-cost pr-sub-item-total-cost${ctr} pr-sub-item_{{ $subItem->id }}_total-cost form-control form-control-sm`" value="0.00" readonly>
                                        </td>
                                    @else
                                        <td>
                                            <input type="text" :name="`sub_unit_cost_supplier_${supplier.id}_pr_sub_item_{{ $subItem->id }}`" :id="`sub_unit_cost_supplier_${supplier.id}_pr_sub_item_{{ $subItem->id }}`" class="pr-sub-item-unit-cost form-control form-control-sm" value="0.00">
                                        </td>
                                        <td>
                                            <input type="text" :name="`sub_total_cost_supplier_${supplier.id}_pr_sub_item_{{ $subItem->id }}`" :id="`sub_total_cost_supplier_${supplier.id}_pr_sub_item_{{ $subItem->id }}`" class="pr-sub-item-total-cost pr-sub-item_{{ $subItem->id }}_total-cost form-control form-control-sm" value="0.00" readonly>
                                        </td>
                                    @endif
                                </template>
                            </template>
                        </tr>
                        @if($abstract_form->pr->pr_item_total->is_per_item_purchase)
                            <tr>
                                <td colspan="6" class="text-center" id="" style="vertical-align: middle;">
                                    <center><strong><p>RANKING</p></strong></center>
                                </td>
                                <template v-for="(supplier, ctr) in getSuppliers">
                                    <template v-if="supplier.items">
                                        <template v-for="item in supplier.items" v-if="item.id === {{ $subItem->id }}">
                                            <td colspan="2">
                                                <input type="text" :name="`ranking_supplier_${supplier.id}_pr_sub_item_{{ $subItem->id }}_col_${ctr}`" :id="`ranking_supplier_${supplier.id}_pr_sub_item_{{ $subItem->id }}_col_${ctr}`" class=" form-control form-control-sm text-center" readonly :value="item.pivot.per_item_ranking" style="font-weight: bold; font-size: 20px;">
                                            </td>
                                        </template>
                                    </template>
                                    <template v-else>
                                        <td colspan="2">
                                            <input type="text" :name="`ranking_supplier_${supplier.id}_pr_sub_item_{{ $subItem->id }}_col_${ctr}`" :id="`ranking_supplier_${supplier.id}_pr_sub_item_{{ $subItem->id }}_col_${ctr}`" class=" form-control form-control-sm text-center" readonly value="-" style="font-weight: bold; font-size: 20px;">
                                        </td>
                                    </template>
                                </template>
                            </tr>
                        @endif
                    @endforeach
                @endif
            @endforeach
            @if($abstract_form->pr->pr_item_total->is_lot_purchase)
                <tr>
                    <td colspan="5" rowspan="1" class="text-center" id="" style="vertical-align: middle;">
                        <center><strong><p>Amount Totals</p></strong></center>
                    </td>
                    <td>
                        <center><strong><p id="total_estimate">&#8369;{{$abstract_form->pr->pr_item_total->total_estimate}}</p></strong></center>
                    </td>
                    <template v-for="(supplier, ctr) in getSuppliers">
                        <td colspan="2">
                            <input type="text" :name="`total_cost_total_supplier${supplier.id}`" :id="`total_cost_total_supplier${supplier.id}`" :class="`total_cost_total_supplier${ctr}`" class="total_cost_total_suppliers form-control form-control-sm text-center" :value="supplier.amount_total" readonly>
                        </td>
                    </template>
                </tr>
                <tr>
                    <th class="text-center" colspan="6">RANKING</th>
                    <template v-for="(supplier, ctr) in getSuppliers">
                        <td colspan="2" v-if="supplier.pivot">
                            <input type="text" :id="`supplier_ranking${ctr}`" class="supplier_ranking form-control form-control-sm text-center" readonly :name="`supplier_ranking${supplier.id}`" :value="supplier.pivot.ranking" style="font-weight: bold; font-size: 20px;">
                        </td>
                        <td colspan="2" v-else>
                            <input type="text" :id="`supplier_ranking${ctr}`" class="supplier_ranking form-control form-control-sm text-center" readonly :name="`supplier_ranking${supplier.id}`" value="-" style="font-weight: bold; font-size: 20px;">
                        </td>
                    </template>
                </tr>
            @endif
            @if ($abstract_type == 'aop')
            <tr>
                <td colspan="6" rowspan="1" id="" style="vertical-align: middle;">
                    <div class="col-xl-6 d-inline-block">
                        <strong><p class="text-right">TECHNICAL PROPOSAL</p></strong>    
                    </div>
                    <div class="col-xl-5 d-inline-block">
                        <div class="input-group input-group-sm w-50">
                        <input type="number" min="0" max="100" step=".001" :id="`tech_proposal_base`" class="tech_proposal_base form-control form-control-sm text-center" :name="`tech_proposal_base`" value="{{ $abstract_form->technical_proposal_base }}">
                            <div class="input-group-append">
                                <span class="input-group-text">%</span>
                            </div>
                        </div>
                    </div>
                </td>
                <template v-for="(supplier, ctr) in getSuppliers">
                    <template v-if="supplier.pivot">
                        <td colspan="2">
                            <input type="hidden" :id="`tech_proposal_res_${ctr}`" value="FAILED">
                            <div class="input-group input-group-sm mx-auto w-50">
                                <input type="number" min="0" step=".001" max="100" :id="`tech_proposal_${ctr}`" class="tech_proposals form-control form-control-sm text-center" :name="`tech_proposal_supplier${supplier.id}`" :value="supplier.pivot.technical_proposal">
                                <div class="input-group-append">
                                    <span class="input-group-text">%</span>
                                </div>
                            </div>
                        </td>
                    </template>
                    <template v-else>
                        <td colspan="2">
                            <input type="hidden" :id="`tech_proposal_res_${ctr}`" value="FAILED">
                            <div class="input-group input-group-sm mx-auto w-50">
                                <input type="number" min="0" step=".001" max="100" :id="`tech_proposal_${ctr}`" class="tech_proposals form-control form-control-sm text-center" :name="`tech_proposal_supplier${supplier.id}`" value="0">
                                <div class="input-group-append">
                                    <span class="input-group-text">%</span>
                                </div>
                            </div>
                        </td>
                    </template>
                </template>
            </tr>    
            @endif
            @if($abstract_type === 'aob')
                @include('abstract.eligibility-edit-aob')
            @else
                @include('abstract.eligibility-edit')
            @endif
            <tr>
                <td :colspan="(getSuppliersCount * 2) + 6">
                    <div class="row">
                        <div class="col-xl-12">
                            <p><strong>Recommendations:</strong></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-3">
                            <p id="bidder_grammar" style="text-indent: 2%">Single/Lowest Calculated and Responsive Bidder</p>
                        </div>
                        <div class="col-xl-2">
                            <p>for Item/Lot #:</p>
                        </div>
                        <div class="col-xl-3">
                            <p>Sub-Total Amount:</p>
                        </div>
                        <div class="col-xl-3">
                            <p>Remarks:</p>
                        </div>
                        <div class="col-xl-1">
                        </div>
                    </div>
                    <div class="reco-section mb-2">
                        @php $reco_row_counter = 1; @endphp
                        @foreach($abstract_form->abstract_parent->recommendation->recommendation_items as $reco_item)
                            <div class="row reco-row mb-2" id="{{ 'reco_row_'.$reco_row_counter }}">
                                <input type="hidden" name="reco_row_count[]" id="reco_row_count">
                                <div class="col-xl-3">
                                    {{ Form::select('reco_supplier_'.$reco_row_counter, ['0' => 'Please select one...'] + $reco_suppliers, $reco_item->supplier_id, ['class' => 'reco_suppliers form-control form-control-sm border border-info w-50', 'id' => 'reco_supplier_'.$reco_row_counter]) }}
                                </div>
                                <div class="col-xl-2">
                                    {{ Form::select('reco_item_no_'.$reco_row_counter.'[]', ['0' => 'Please select one...'] + $reco_items_edit, $reco_item->pr_item_ids_array, ['class' => 'reco_item_nos form-control form-control-sm border border-info w-50', 'id' => 'reco_item_no_'.$reco_row_counter, 'multiple' => 'multiple']) }}
                                </div>
                                <div class="col-xl-3">
                                    <input type="text" name="{{ 'reco_sub_total_'.$reco_row_counter }}" id="{{ 'reco_sub_total_'.$reco_row_counter }}" class="reco_sub_totals form-control form-control-sm border border-info w-50" value="{{ $reco_item->subtotal }}" readonly>
                                </div>
                                <div class="col-xl-3">
                                    <input type="text" name="{{ 'reco_remarks_'.$reco_row_counter }}" class="form-control form-control-sm border border-info" required value="{{ $reco_item->remarks }}">
                                </div>
                                <div class="col-xl-1">
                                    <button type="button" class="remove-reco-row btn btn-danger btn-xs" style="float: right;">-</button>
                                </div>
                            </div>
                            @php $reco_row_counter++; @endphp
                        @endforeach
                        <input type="hidden" name="reco_row_count[]" id="reco_row_count" value="0">
                        <div class="row reco-row-base d-none">
                            <div class="col-xl-3">
                                <select class="form-control form-control-sm border border-info w-50" name="reco_supplier" id="reco_supplier">
                                    <option v-for="supplier in getSuppliers" :key="supplier.id" :value="supplier.id">@{{ supplier.company_name }}</option>
                                </select>
                            </div>
                            <div class="col-xl-3">
                                <select name="reco_item_no" id="reco_item_no" class="form-control form-control-sm border border-info w-25">
                                    @foreach($abstract_form->pr->pr_items as $item)
                                        <option value="{{$item->id}}">{{$item->item_no}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-xl-3">
                                {{ Form::text('', null, ['class' => 'form-control form-control-sm border border-info w-50']) }}
                            </div>
                            <div class="col-xl-3">
                                {{ Form::text('', null, ['class' => 'form-control form-control-sm border border-info']) }}
                            </div>
                        </div>    
                    </div>
                    <div class="row">
                        <div class="col-xl-12">
                            <button type="button" class="add-recommendation-row btn btn-success btn-xs" style="float: right;">+</button>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td :colspan="(getSuppliersCount * 2) + 6">
                    <div class="row">
                        <div class="col-xl-2 offset-xl-3 text-right">
                            <p><strong>TOTAL:</strong></p>
                        </div>
                        <div class="col-xl-3">
                            {{ Form::text('reco_total', $abstract_form->abstract_parent->recommendation->total, ['class' => 'form-control form-control-sm border border-info w-50', 'readonly', 'id' => 'reco_total']) }}
                        </div>
                    </div>
                </td>
            </tr>
            @if ($abstract_type === 'aob')
                @include('abstract.signatories-edit-aob')
            @else
                @include('abstract.signatories-edit')
            @endif
        </tbody>
    </table>
</div>