<tr id="twg-header">
    <th :colspan="(getSuppliersCount * 2) + 6" style="background-color: #ccc !important; border: 3px solid #000">Technical Working Group</td>
</tr>
<tr id="twg-content">
    <td :colspan="(getSuppliersCount * 2) + 6" class="py-4">
        <div class="row">
            <div class="col-xl-2 offset-xl-1">
                {{ Form::text('twg_chairman', null, ['class' => 'form-control form-control-sm border border-info', 'placeholder' => 'insert name']) }}
                <p class="text-center">TWG Chairperson</p>
            </div>
            <div class="col-xl-2 offset-xl-2">
                {{ Form::text('twg_member_1', null, ['class' => 'form-control form-control-sm border border-info', 'placeholder' => 'insert name']) }}
                <p class="text-center">TWG Member</p>
            </div>
            <div class="col-xl-2 offset-xl-2">
                {{ Form::text('twg_member_2', null, ['class' => 'form-control form-control-sm border border-info', 'placeholder' => 'insert name']) }}
                <p class="text-center">TWG Member</p>
            </div>
        </div>
    </td>
</tr>
<tr>
    <th :colspan="(getSuppliersCount * 2) + 6" style="background-color: #ccc !important; border: 3px solid #000">PCC Bids and Awards Committee:</td>
</tr>
<tr>
    <td :colspan="(getSuppliersCount * 2) + 6" class="py-4">
        <div class="row mb-3">
            <div class="col-xl-2 offset-xl-3">
                {{ Form::text('pbac_chairperson', null, ['class' => 'form-control form-control-sm border border-info', 'placeholder' => 'insert name']) }}
                <p class="text-center">PBAC Chairperson</p>
            </div>
            <div class="col-xl-2 offset-xl-2">
                {{ Form::text('pbac_vice_chairperson', null, ['class' => 'form-control form-control-sm border border-info', 'placeholder' => 'insert name']) }}
                <p class="text-center">PBAC Vice Chairperson</p>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-2 offset-xl-1">
                {{ Form::text('pbac_member_1', null, ['class' => 'form-control form-control-sm border border-info', 'placeholder' => 'insert name']) }}
                <p class="text-center">PBAC Member</p>
            </div>
            <div class="col-xl-2 offset-xl-2">
                {{ Form::text('pbac_member_2', null, ['class' => 'form-control form-control-sm border border-info', 'placeholder' => 'insert name']) }}
                <p class="text-center">PBAC Member</p>
            </div>
            <div class="col-xl-2 offset-xl-2">
                {{ Form::text('provisional_member', null, ['class' => 'form-control form-control-sm border border-info', 'placeholder' => 'insert name']) }}
                <p class="text-center">Provisional Member</p>
            </div>
        </div>
    </td>
</tr>
<tr class="d-none" id="obs-header">
    <th :colspan="(getSuppliersCount * 2) + 6" style="background-color: #ccc !important; border: 3px solid #000">Observers:</td>
</tr>
<tr class="d-none" id="obs-content">
    <td :colspan="(getSuppliersCount * 2) + 6" class="py-4">
        <div class="row">
            <div class="col-xl-2 offset-xl-1">
                {{ Form::text('coa_representative', null, ['class' => 'form-control form-control-sm border border-info', 'placeholder' => 'insert name']) }}
                <p class="text-center">COA Representative</p>
            </div>
            <div class="col-xl-2 offset-xl-2">
                {{ Form::text('private_observer_1', null, ['class' => 'form-control form-control-sm border border-info', 'placeholder' => 'insert name']) }}
                <p class="text-center">Private Observer 1</p>
            </div>
            <div class="col-xl-2 offset-xl-2">
                {{ Form::text('private_observer_2', null, ['class' => 'form-control form-control-sm border border-info', 'placeholder' => 'insert name']) }}
                <p class="text-center">Private Observer 2</p>
            </div>
        </div>
    </td>
</tr>