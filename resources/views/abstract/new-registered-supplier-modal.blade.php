<div id="newRegisteredSupplierModal" class="modal fade">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Invite Registered Supplier</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-hover table-striped" id="suppliers-datatable">
                        <thead class="table-primary">
                            <tr>
                                <td>Company Name</td>
                                <td>Location Address</td>
                                <td>Contact No.</td>
                                <td>T.I.N.</td>
                                <td>Action</td>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="supplier in getUninvitedSuppliers" :key="supplier.id">
                                <td>@{{ supplier.company_name }}</td>
                                <td>@{{ supplier.address }}</td>
                                <td>@{{ supplier.telephone }}</td>
                                <td>@{{ supplier.tin_number }}</td>
                                <td><button class="btn btn-info" type="button" @click="addUninvitedSupplier(supplier, $event)">Add</button></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>