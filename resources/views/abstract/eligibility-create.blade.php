<tr class="eligibility_docs">
    <td colspan="3" rowspan="2" class="text-center" id="eli_req_title" style="vertical-align: middle;">ELIGIBILITY REQUIREMENT</td>
    <td colspan="3">
        <input type="hidden" name="eligibility_docs_count[]" id="legal_docs_count" value="0">
        <button type="button" class="add-eligibility-docs btn btn-success btn-xs" style="float: right;" @click="addEligibilityBtn()">+</button>
    </td>
    <template v-for="(supplier, ctr) in getSuppliers">
        <td colspan="2">
            <input type="hidden" :name="`supplier${ctr}`" :id="`supplier${ctr}`" :value="supplier.id">
        </td>
    </template>
</tr>
<tr>
    <td colspan="3">
        <center><strong><p>RESULT</p></strong></center>
    </td>
    <template v-for="(supplier, ctr) in getSuppliers">
        <td colspan="2">
            <input type="text" :name="`result_compliance_supplier_id_${supplier.id}`" :id="`result_compliance_supplier${ctr}`" :class="`result_compliance_supplier_id_${supplier.id} form-control form-control-sm text-center`" readonly value="-" style="font-weight: bold; font-size: 20px;">
        </td>
    </template>
</tr>