@php
    $colspan = 6 + (count($abstract_form->abstract_parent->suppliers) * 2)
@endphp
<div class="table-responsive table-wrapper mt-3 aob-table">
    <table class="table table-striped table-hover custom-table-bordered">
        <thead>
            <tr>
                <th rowspan="6" class="text-center" style="width: 8%; vertical-align: middle;">Item No.</th>
                <th rowspan="6" class="text-center" style="width: 8%; vertical-align: middle;">Quantity</th>
                <th rowspan="6" class="text-center" style="width: 7%; vertical-align: middle;">Unit</th>
                <th rowspan="6" class="text-center resize-min-width" style="vertical-align: middle;">Description</th>
                <th colspan="2" class="text-right resize-min-width">Supplier's Name</th>
                @foreach($abstract_form->abstract_parent->suppliers as $supplier)
                    <td colspan="2" class="resize-min-width">{{ $supplier->company_name }}</td>
                @endforeach
            </tr>
            <tr>
                <th colspan="2" class="text-right" style="vertical-align: middle;">Location Address</th>
                @foreach($abstract_form->abstract_parent->suppliers as $supplier)
                    <td colspan="2" class="resize-min-width">{{ $supplier->address }}</td>
                @endforeach
            </tr>
            <tr>
                <th colspan="2" class="text-right">Contact No.</th>
                @foreach($abstract_form->abstract_parent->suppliers as $supplier)
                    <td colspan="2" class="resize-min-width">{{ $supplier->telephone }}</td>
                @endforeach
            </tr>
            <tr>
                <th colspan="2" class="text-right">T.I.N.</th>
                @foreach($abstract_form->abstract_parent->suppliers as $supplier)
                    <td colspan="2" class="resize-min-width">{{ $supplier->tin_number }}</td>
                @endforeach
            </tr>
            <tr>
                <th colspan="2" class="text-center">Estimated</th>
                @foreach($abstract_form->abstract_parent->suppliers as $supplier)
                    <td colspan="2" class="resize-min-width"></td>
                @endforeach
            </tr>
            <tr>
                <th class="text-center">Unit Cost</th>
                <th class="text-center">Total</th>
                @foreach($abstract_form->abstract_parent->suppliers as $supplier)
                    <th class="text-center">Unit Cost</th>
                    <th class="text-center">Total</th>
                @endforeach
            </tr>
        </thead>
        <tbody>
            @foreach($abstract_form->pr->pr_items as $item)
                <tr>
                    <td>{{ $item->item_no }}</td>
                    <td>{{ $item->quantity }}</td>
                    <td>{{ $item->unit }}</td>
                    <td>{{ $item->description }}</td>
                    @if ($item->est_cost_unit != " ")
                        <td>&#8369; {{ number_format($item->est_cost_unit, 2, '.', ',') }}</td>
                    @else
                        <td></td>
                    @endif
                    <td>&#8369; {{ number_format($item->est_cost_total, 2, '.', ',') }}</td>
                    @if (count($item->suppliers->where('pivot.abstract_id', $abstract_form->abstract_parent->id)))
                         @foreach($item->suppliers->where('pivot.abstract_id', $abstract_form->abstract_parent->id) as $item_supplier)
                            @if($item_supplier->pivot->unit_cost === 'n/a')
                                <td>{{ $item_supplier->pivot->unit_cost }}</td>
                            @else
                                <td>&#8369; {{ number_format($item_supplier->pivot->unit_cost, 2, '.', ',') }}</td>
                            @endif
                            @if($item_supplier->pivot->total !== 'n/a')
                                @if($item_supplier->pivot->total > $item->est_cost_total)
                                    <td class="with-error">&#8369; {{ number_format($item_supplier->pivot->total, 2,'.', ',') }}</td>
                                @else
                                    <td>&#8369; {{ number_format($item_supplier->pivot->total, 2, '.', ',') }}</td>
                                @endif
                            @else
                                <td>n/a</td>
                            @endif
                        @endforeach
                    @else
                        @foreach ($abstract_form->abstract_parent->suppliers as $supplier)
                            @if(count($item->pr_sub_items) && ($abstract_form->pr->pr_item_total->is_lot_purchase))
                                <td></td>
                                @php $total_financial_proposal = 0; @endphp
                                @foreach ($supplier->pr_sub_items->where('pivot.abstract_id', $abstract_form->abstract_parent->id) as $subItem)
                                    @php $total_financial_proposal += $subItem->pivot->total @endphp
                                @endforeach
                                <td>&#8369; {{ number_format($total_financial_proposal, 2, '.', ',') }}</td>
                            @else
                                <td colspan="2"></td>
                            @endif
                        @endforeach
                    @endif
                </tr>
                @if (($abstract_form->pr->pr_item_total->is_per_item_purchase) && (!count($item->pr_sub_items)))
                    <tr>
                        <td colspan="6" rowspan="1" class="text-center" id="" style="vertical-align: middle;">
                            <center><strong><p>RANKING</p></strong></center>
                        </td>
                        @foreach($item->suppliers->where('pivot.abstract_id', $abstract_form->abstract_parent->id) as $item_supplier)
                        <td colspan="2">
                            <input type="text" class="form-control form-control-sm text-center" readonly value="{{$item_supplier->pivot->per_item_ranking}}" style="font-weight: bold; font-size: 20px;">
                        </td>
                        @endforeach
                    </tr>
                @endif
                @if(count($item->pr_sub_items))
                    @foreach($item->pr_sub_items as $subItem)
                        <tr>
                            <td>{{ $subItem->item_no }}</td>
                            <td>{{ $subItem->quantity }}</td>
                            <td>{{ $subItem->unit }}</td>
                            <td>{{ $subItem->description }}</td>
                            <td>&#8369; {{ number_format(str_replace(',', '', $subItem->est_cost_unit), 2, '.', ',') }}</td>
                            <td>&#8369; {{ number_format(str_replace(',', '', $subItem->est_cost_total), 2, '.', ',') }}</td>
                            @foreach($subItem->suppliers->where('pivot.abstract_id', $abstract_form->abstract_parent->id) as $subItem_supplier)
                                @if($subItem_supplier->pivot->unit_cost === 'n/a')
                                    <td>{{ $subItem_supplier->pivot->unit_cost }}</td>
                                @else
                                    <td>&#8369; {{ number_format($subItem_supplier->pivot->unit_cost, 2, '.', ',') }}</td>
                                @endif
                                @if($subItem_supplier->pivot->total !== 'n/a')
                                    @if($subItem_supplier->pivot->total > $item->est_cost_total)
                                        <td class="with-error">&#8369; {{ number_format($subItem_supplier->pivot->total, 2, '.', ',') }}</td>
                                    @else
                                        <td>&#8369; {{ number_format($subItem_supplier->pivot->total, 2, '.', ',') }}</td>
                                    @endif
                                @else
                                    <td>n/a</td>
                                @endif
                            @endforeach
                        </tr>
                        @if($abstract_form->pr->pr_item_total->is_per_item_purchase)
                            <tr>
                                <td colspan="6" rowspan="1" class="text-center" id="" style="vertical-align: middle;">
                                    <center><strong><p>RANKING</p></strong></center>
                                </td>
                                @foreach($subItem->suppliers->where('pivot.abstract_id', $abstract_form->abstract_parent->id) as $subItem_supplier)
                                <td colspan="2">
                                    <input type="text" class="form-control form-control-sm text-center" readonly value="{{$subItem_supplier->pivot->per_item_ranking}}" style="font-weight: bold; font-size: 20px;">
                                </td>
                                @endforeach
                            </tr>
                        @endif
                    @endforeach
                @endif
            @endforeach
            @if ($abstract_form->pr->pr_item_total->is_lot_purchase)
            <tr>
                <th colspan="6" class="text-center">RANKING</th>
                @foreach ($abstract_form->abstract_parent->suppliers as $supplier)
                    <th colspan="2" class="text-center">{{ $supplier->pivot->ranking }}</th>
                @endforeach
            </tr>
            @endif
            @if ($abstract_form->abstract_parent->type === 'AOP')
            <tr>
                <td colspan="6" rowspan="1" id="" style="vertical-align: middle;">
                    <div class="col-xl-6 d-inline-block">
                        <strong><p class="text-right">TECHNICAL PROPOSAL</p></strong>    
                    </div>
                    <div class="col-xl-5 d-inline-block">
                        <div class="input-group input-group-sm" style="width: 30%;">
                            <input type="number" class="form-control form-control-sm text-center" value="{{$abstract_form->technical_proposal_base}}" readonly>
                            <div class="input-group-append">
                                <span class="input-group-text">%</span>
                            </div>
                        </div>
                    </div>
                </td>
                @foreach ($abstract_form->abstract_parent->suppliers as $supplier)
                    <th colspan="2" class="text-center">{{ $supplier->pivot->technical_proposal }}%</th>
                @endforeach
            </tr>
            @endif
            @if($abstract_form->abstract_parent->type === 'AOB')
                @include('abstract.eligibility-show-aob')
            @else
                @include('abstract.eligibility-show')
            @endif
            <tr>
                <td colspan="{{ $colspan }}">
                    <div class="row">
                        <div class="col-xl-12">
                            <p><strong>Recommendations:</strong></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-3">
                            @if($abstract_form->bid_type === 'As Read')
                                @if($abstract_form->pb_type === 'For Consulting Services')
                                    <p style="text-indent: 2%">Single/Highest Rated Bidder:</p>
                                @else
                                    <p style="text-indent: 2%">Single/Lowest Calculated Bidder:</p>
                                @endif
                            @else
                                @if($abstract_form->pb_type === 'For Consulting Services')
                                    <p style="text-indent: 2%">Single/Highest Rated and Responsive Bidder:</p>
                                @else
                                    <p style="text-indent: 2%">Single/Lowest Calculated and Responsive Bidder:</p>
                                @endif
                            @endif
                        </div>
                        <div class="col-xl-3">
                            <p>for Item/Lot #:</p>
                        </div>
                        <div class="col-xl-3">
                            <p>Sub-Total Amount:</p>
                        </div>
                        <div class="col-xl-3">
                            <p>Remarks:</p>
                        </div>
                    </div>
                    <div class="reco-section mb-2">
                        <div class="row reco-row-base">
                            <div class="col-xl-3">
                                @foreach($abstract_form->abstract_parent->recommendation->recommendation_items as $reco_item)
                                    <input type="text" value="{{ $reco_item->supplier->company_name }}" class="form-control form-control-sm w-75" readonly>
                                @endforeach
                            </div>
                            <div class="col-xl-3">
                                @foreach($abstract_form->abstract_parent->recommendation->recommendation_items as $reco_item)
                                    <input type="text" value="{{ $reco_item->pr_item_nos }}" class="form-control form-control-sm w-25" readonly>
                                @endforeach
                            </div>
                            <div class="col-xl-3">
                                @foreach($abstract_form->abstract_parent->recommendation->recommendation_items as $reco_item)
                                    <input type="text" value="&#8369; {{ number_format( $reco_item->subtotal, 2, '.', ',') }}" class="form-control form-control-sm w-75" readonly>
                                @endforeach
                            </div>
                            <div class="col-xl-3">
                                @foreach($abstract_form->abstract_parent->recommendation->recommendation_items as $reco_item)
                                    <input type="text" value="{{ $reco_item->remarks }}" class="form-control form-control-sm w-75" readonly>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="{{ $colspan }}">
                    <div class="row">
                        <div class="col-xl-2 offset-xl-4 text-right">
                            <p><strong>TOTAL:</strong></p>
                        </div>
                        <div class="col-xl-3">
                            {{ Form::text('reco_total', "&#8369; ". number_format($abstract_form->abstract_parent->recommendation->total, 2, '.', ','), ['class' => 'form-control form-control-sm border border-info w-50', 'readonly']) }}
                        </div>
                    </div>
                </td>
            </tr>
            @if($abstract_form->abstract_parent->type === 'AOB')
                @include('abstract.signatories-show-aob')
            @else
                @include('abstract.signatories-show')
            @endif
        </tbody>
    </table>
</div>