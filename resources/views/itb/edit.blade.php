@extends('layouts.base')
@section('content')
<link rel="stylesheet" href="{{ asset('css/jquery.datetimepicker.css') }}">
@if ($itb->invite->status == 'FOR_REVISION')
<div class="alert alert-info">
    {{$itb->invite->rejects_remarks}}
</div>
@endif
{{ Form::open(['url' => url('/itb/'.$itb->id), 'method' => 'put']) }}
<div class="row frm" id="itb_form">
    <div class="col-xl-12">
        <h1 class="text-center">INVITATION TO BID FOR</h1>
    </div>
    <div class="col-xl-4 mx-auto">
        <input value="{{ $itb->name_of_project }}" name="name_of_project" type="text" required placeholder="Insert name of project" class="form-control form-control-sm border border-info">
    </div>
    <div class="col-xl-12 form-inline mt-5">
        <ol>
            <li class="mb-4"><span>The Philippine Competition Commission, through the General Appropriations Act of {{ Form::select('year', $yearsArray, $itb->year, ['class' => 'form-control form-control-sm border border-info']) }} intends to apply the sum of <input value="{{ $itb->abc }}" type="text" placeholder="insert the approved budget for the contract" style="width: 27%;" class="form-control form-control-sm border border-info" name="abc"> being the Approved Budget for the Contract(ABC) to payments under the contract for <input required type="text" value="{{ $itb->invite->type }}#: {{ $itb->invite->form_no }}" readonly class="form-control form-control-sm border border-info" name="itb_no">. Bids received in excess of the ABC shall be automatically rejected at bid opening.</span></li>

            <li class="mb-4"><span>The Philippine Competition Commission now invites bids for <input style="width: 29%;" placeholder="insert brief description of goods to be procured" type="text" required class="w-50 form-control form-control-sm border border-info" name="description_of_goods" value="{{ $itb->description_of_goods }}">. Delivery of the Goods is required <input placeholder="insert the required delivery date or expected contract duration" style="width: 38%;" id="delivery_date" required type="text" class="form-control form-control-sm border border-info" name="delivery_date" value="{{ $itb->delivery_date }}">. Bidders should have completed, within <input placeholder="insert relevant period" type="text" required class="form-control form-control-sm border border-info" name="relevant_period" value="{{ $itb->relevant_period }}"> from the date of submission and receipt of bids, a contract similar to the Project. The description of an eligible bidder is contained in the Bidding Documents, particularly in Section II. Instructions to Bidders.</span></li>

            <li class="mb-4">
                <p>Bidding will be conducted through open competitive bidding procedures using a non-discretionary "pass/fail" criterion as specified in the 2016 Revised Implementing Rules and Regulations (IRR) of Republic Act (RA) 9184, otherwise known as the "Government Procurement Reform Act".</p>
                <p class="mt-4">Bidding is restricted to Filipino citizens/sole proprietorships, partnerships, or organizations with atl east sixy percent (60%) interset or oustanding capital stock belonging to citizens of the Philippines, adn to citizens or organizations of a country the laws or regulations of which grant similar rights or privileges to Filipino citizens, pursuant to RA 5183.</p>
            </li>

            <li class="mb-4">
                <span>Interested bidders may obtain further information from Philippine Competition Commission and inspect the Bidding Documents at the address given below during Office Hours from 8:00 A.M. to 5:00 P.M. (Monday through Friday).</span>
            </li>

            <li class="mb-4">
                <span>A complete set of Bidding Documents may be acquired by interested Bidders on <input type="text" placeholder="insert date of availability of Bidding Documents" id="acquireDocuments" class="form-control form-control-sm border border-info" required name="date_of_availability" value="{{ $itb->date_of_availability }}"> from the address below and upon payment of the appliacble fee for the bidding Documents, pursuant to the latest Guidelines issued by the GPPB, in the amount of &#8369;<input type="text" required name="bidding_documents_fee" placeholder="insert amount in Pesos" class="form-control form-control-sm border border-info" style="width: 36%" value="{{ $itb->bidding_documents_fee }}"></span>
            </li>

            <li class="mb-4">
            <span>The Philippine Competition Commission will hold a Pre-Bid Conference on <input type="text" required name="prebid_conf_date" id="prebid_conf_date" placeholder="insert time and date" class="form-control form-control-sm border border-info" value="{{ $itb->prebid_conf_date }}"> at <input type="text" placeholder="insert address for Pre-Bid Conference, if applicable" name="prebid_conf_address" value="{{ $itb->prebid_conf_address }}" class="form-control form-control-sm border border-info">, which shall be open to prospective bidders.</span>
            </li>

            <li class="mb-4">
                <p><span>Bids must be duly received by the BAC Secretariat at the address below on or before <input value="{{ $itb->bids_deadline }}" type="text" required name="bids_deadline" id="bids_deadline" class="form-control form-control-sm border border-info">. All Bids must be accompanied by a bid security in any  of the acceptable forms and in the amount state in <strong>ITB</strong> Clause 18.</span></p>

                <p class="mt-4"><span>Bid opening shall be on <input type="text" placeholder="insert time and date" name="bid_opening_datetime" value="{{ $itb->bid_opening_datetime }}" id="bid_opening_datetime" class="form-control form-control-sm border border-info"> at <input type="text" class="form-control form-control-sm border border-info" value="{{ $itb->bid_opening_address }}" required name="bid_opening_address" placeholder="insert address for bid opening" style="width: 30%">. Bids will be opened in the presence of the bidders' representatives who choose to attend at the address below. Late bids shall not be accepted.</span></p>
            </li>

            <li class="mb-4">
                <textarea name="other_info" rows="10" class="form-control border border-info w-100" placeholder="insert such other necessary information deemed relevant by the Procuring Entity.">{{ $itb->other_info }}</textarea>
            </li>

            <li class="mb-4">
                <p>The Philippine Competition Commission reserves the right to reject any and all bids, declare a failure of bidding, or not award the contract at any time prior to contract award in accordance with Section 41 of RA 9184 and its IRR, without thereby incurring any liability to the affected bidder or bidders.</p>
            </li>

            <li class="mb-4">
                <p>For further information, please refer to:</p>
                <p class="mt-3"><i>The Secretariat</i></p>
                <p><i>Bids and Awards Commitee (BAC)</i></p>
                <p><i>Philippine Competition Commission (PCC)</i></p>
                <p><i>25/F Vertis North Corporate Center 1</i></p>
                <p><i>North Avenue, Quezon City, 1105</i></p>
                <p><i>Tel. No.: (02) 771-9722 / (02) 771-9757</i></p>
                <p><i>Email: procurement@phcc.gov.ph or gsd@phcc.gov.ph</i></p>
                <p><i>Website: www.phcc.gov.ph</i></p>
            </li>
        </ol>
    </div>
    <div class="col-xl-12">
        <p><input required name="chairperson_signatory" type="text" class="form-control form-control-sm border border-info w-25 float-right" value="{{ $itb->chairperson_signatory }}"></p>
    </div>
    <div class="col-xl-12 text-right">
        <p>Chairperson</p>
    </div>
    <div class="col-xl-12 text-right">
        <p><i>PCC Bids and Awards Committee</i></p>
    </div>
    <input type="submit" value="Update and Submit" class="btn btn-success">
</div>
{{ Form::close() }}
<script src="{{ asset('js/utils/jquery-ui.min.js') }}"></script>
<script src="{{ asset('js/utils/jquery.datetimepicker.js') }}"></script>
@endsection