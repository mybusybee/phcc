@extends('layouts.base')
@section('content')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
@if ($itb->invite->status == 'FOR_REVISION' || ($itb->invite->status == 'FOR_REVIEW/APPROVAL' && ($itb->invite->rejects_remarks)))
<div class="alert alert-info">
    {{$itb->invite->rejects_remarks}}
</div>
@endif
<div class="row frm" id="itb_form">
    <div class="col-xl-12">
        <h1 class="text-center">INVITATION TO BID FOR</h1>
    </div>
    <div class="col-xl-4 mx-auto">
        <input value="{{ $itb->name_of_project }}" readonly name="name_of_project" type="text" required class="form-control form-control-sm">
    </div>
    <div class="col-xl-12 form-inline mt-5">
        <ol>
        <li class="mb-4"><span>The Philippine Competition Commission, through the General Appropriations Act of <input value="{{ $itb->year }}" readonly required name="year" type="text" class="form-control form-control-sm"> intends to apply the sum of <input value="{{ $itb->abc }}" readonly type="text" class="form-control form-control-sm" name="abc"> being the Approved Budget for the Contract(ABC) to payments under the contract for <input readonly requried type="text" value="{{ $itb->invite->type }}#: {{ $itb->invite->form_no }}" class="form-control form-control-sm" name="itb_no">. Bids received in excess of the ABC shall be automatically rejected at bid opening.</span></li>

            <li class="mb-4"><span>The Philippine Competition Commission now invites bids for <input readonly type="text" required class="form-control form-control-sm" name="description_of_goods" style="width: 60%" value="{{ $itb->description_of_goods }}">. Delivery of the Goods is required <input readonly id="delivery_date" required type="text" class="form-control form-control-sm" name="delivery_date" value="{{ $itb->delivery_date }}">. Bidders should have completed, within <input readonly type="text" required class="form-control form-control-sm" name="relevant_period" value="{{ $itb->relevant_period }}"> from the date of submission and receipt of bids, a contract similar to the Project. The description of an eligible bidder is contained in the Bidding Documents, particularly in Section II. Instructions to Bidders.</span></li>

            <li class="mb-4">
                <p>Bidding will be conducted through open competitive bidding procedures using a non-discretionary "pass/fail" criterion as specified in the 2016 Revised Implementing Rules and Regulations (IRR) of Republic Act (RA) 9184, otherwise known as the "Government Procurement Reform Act".</p>
                <p class="mt-4">Bidding is restricted to Filipino citizens/sole proprietorships, partnerships, or organizations with at least sixty percent (60%) interset or oustanding capital stock belonging to citizens of the Philippines, adn to citizens or organizations of a country the laws or regulations of which grant similar rights or privileges to Filipino citizens, pursuant to RA 5183.</p>
            </li>

            <li class="mb-4">
                <span>Interested bidders may obtain further information from Philippine Competition Commission and inspect the Bidding Documents at the address given below during Office Hours from 8:00 A.M. to 5:00 P.M. (Monday through Friday).</span>
            </li>

            <li class="mb-4">
                <span>A complete set of Bidding Documents may be acquired by interested Bidders on <input readonly type="text" class="form-control form-control-sm" required name="date_of_availability" value="{{ $itb->date_of_availability }}"> from the address below and upon payment of the appliacble fee for the bidding Documents, pursuant to the latest Guidelines issued by the GPPB, in the amount of &#8369;<input readonly type="text" required name="bidding_documents_fee" class="form-control form-control-sm" value="{{ $itb->bidding_documents_fee }}"></span>
            </li>

            <li class="mb-4">
                <span>The Philippine Competition Commission will hold a Pre-Bid Conference on <input readonly type="text" required name="prebid_conf_date" id="prebid_conf_date" class="form-control form-control-sm" value="{{ $itb->prebid_conf_date }}"> at <input readonly type="text" name="prebid_conf_address" value="{{ $itb->prebid_conf_address }}" class="form-control form-control-sm">, which shall be open to prospective bidders.</span>
            </li>

            <li class="mb-4">
                <p><span>Bids must be duly received by the BAC Secretariat at the address below on or before <input readonly value="{{ $itb->bids_deadline }}" type="text" required name="bids_deadline" id="bids_deadline" class="form-control form-control-sm">. All Bids must be accompanied by a bid security in any  of the acceptable forms and in the amount state in <strong>ITB</strong> Clause 18.</span></p>

                <p class="mt-4"><span>Bid opening shall be on <input readonly type="text" name="bid_opening_datetime" value="{{ $itb->bid_opening_datetime }}" id="bid_opening_datetime" class="form-control form-control-sm"> at <input readonly type="text" class="form-control form-control-sm" value="{{ $itb->bid_opening_address }}" required name="bid_opening_address">. Bids will be opened in the presence of the bidders' representatives who choose to attend at the address below. Late bids shall not be accepted.</span></p>
            </li>

            <li class="mb-4">
                <textarea readonly name="other_info" rows="10" class="form-control w-100">{{ $itb->other_info }}</textarea>
            </li>

            <li class="mb-4">
                <p>The Philippine Competition Commission reserves the right to reject any and all bids, declare a failure of bidding, or not award the contract at any time prior to contract award in accordance with Section 41 of RA 9184 and its IRR, without thereby incurring any liability to the affected bidder or bidders.</p>
            </li>

            <li class="mb-4">
                <p>For further information, please refer to:</p>
                <p class="mt-3"><i>The Secretariat</i></p>
                <p><i>Bids and Awards Commitee (BAC)</i></p>
                <p><i>Philippine Competition Commission (PCC)</i></p>
                <p><i>25/F Vertis North Corporate Center 1</i></p>
                <p><i>North Avenue, Quezon City, 1105</i></p>
                <p><i>Tel. No.: (02) 771-9722 / (02) 771-9757</i></p>
                <p><i>Email: procurement@phcc.gov.ph or gsd@phcc.gov.ph</i></p>
                <p><i>Website: www.phcc.gov.ph</i></p>
            </li>
        </ol>
    </div>
    <div class="col-xl-12 mb-3">
        @if(Auth::user()->user_role === 5 && $itb->invite->status === 'FOR_EXPORT' && $itb->signed === 0)
            <a href="{{ url('itb/signed/'.$itb->id) }}" class="btn btn-info float-right">Signed</a>
        @elseif(Auth::user()->user_role === 5 && $itb->invite->status === 'FOR_EXPORT' && $itb->signed === 1)
            <button class="btn btn-warning float-right" type="button" disabled>Signed</button>
        @endif
    </div>
    <div class="row">
        <div class="col-xl-12">
            <h3>Attached Files:</h3>
            <ul class="list-group">
                @forelse ($itb->invite->files as $file)
                    <a style="color: #000 !important;" href="{{ url('invitation/download/'.$file->id) }}" class="list-group-item list-group-item-action list-group-item-primary">
                        <i class="fa fa-download"></i><span>  {{ $file->filename }}</span>
                    </a>
                @empty
                    <li class="list-group-item group-item-action list-group-item-primary">No file/s attached</li>
                @endforelse
            </ul>
        </div>
    </div>
    <div class="col-xl-12">
        <p><input readonly required name="chairperson_signatory" type="text" class="form-control form-control-sm w-25 float-right" value="{{ $itb->chairperson_signatory }}"></p>
    </div>
    <div class="col-xl-12 text-right">
        <p>Chairperson</p>
    </div>
    <div class="col-xl-12 text-right">
        <p><i>PCC Bids and Awards Committee</i></p>
    </div>
    @if ($itb->pr->personal_pmr_item->status == 'Cancelled' || $itb->pr->personal_pmr_item->status == 'Failed')
    
    @else
        @if(Auth::user()->user_role === 3)
            <div class="form-group">
                <a href="{{ url('/invitations/approve/'.$itb->invite->id) }}" class="btn btn-success">Approve</a>
                <button class="btn btn-danger" data-toggle="modal" data-target="#rejectITBModal">Reject</button>
                <button class="btn btn-danger" data-toggle="modal" data-target="#cancelITBModal">Cancel ITB</button>
            </div>
            <div class="modal fade" id="rejectITBModal" tabindex="-1" role="dialog" aria-hidden="true">
                {{ Form::open(['url' => url('/invitations/reject/'.$itb->invite->id), 'method' => 'put']) }}
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Reason for rejection</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-md-12">
                                            {{ Form::textarea('reject_remarks', null, ['class' => 'form-control']) }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </div>
                {{ Form::close() }}
            </div>
        @elseif(Auth::user()->user_role === 5 && $itb->invite->status === 'FOR_EXPORT')
            <div class="col-xl-4">
                <a href="{{ url('/export/docx/itb/'.$itb->id) }}" class="btn btn-warning">Export</a>
            </div>
            {{ Form::open(['url' => '/invite-suppliers' , 'method' => 'post', 'style' => 'width: 100%;']) }}
                    <div class="table-responsive form-group" id="suppliersContainer">
                        <h5>Supplier List:</h5>
                        <table id="suppliers-datatable" class="table">
                            <thead class="table-primary">
                                <tr>
                                    <th><i class="fa fa-check-circle"></i></th>
                                    <th>Company Name</th>
                                    <th>Authorized Personnel</th>
                                    <th>Category</th>
                                    <th>Address</th>
                                    <th>Telephone</th>
                                    <th>Email</th>
                                    <th>TIN</th>
                                    <th>Mayor's Permit Expiry Date</th>
                                    <th>Philgeps Number</th>
                                </tr>
                            </thead>
                            <tbody>
                                <div id="supplierList">
                                    @foreach($suppliers as $supplier)
                                        @if(in_array($supplier->id, $selected_suppliers))
                                            <input type="hidden" name="selectedSuppliers[]" class="form-control form-control-sm d-inline-block w-25 supplier-id-class" value="{{ $supplier->id }}">
                                        @endif
                                    @endforeach
                                </div>
                                @foreach ($suppliers as $supplier)
                                    <tr>
                                        <td>
                                            @if (in_array($supplier->id, $selected_suppliers))
                                                <input type="checkbox" name="" id="" class="sup_id" value="{{ $supplier->id }}" checked>
                                            @else
                                                <input type="checkbox" name="" id="" class="sup_id" value="{{ $supplier->id }}">
                                            @endif
                                        </td>
                                        <td>{{ $supplier->company_name }}</td>
                                        <td>{{ $supplier->authorized_personnel }}</td>
                                        <td>{{ $supplier->category }}</td>
                                        <td>{{ $supplier->address }}</td>
                                        <td>{{ $supplier->telephone }}</td>
                                        <td>{{ $supplier->email }}</td>
                                        <td>{{ $supplier->tin_number }}</td>
                                        <td>{{ $supplier->mayors_permit }}</td>
                                        <td>{{ $supplier->philgeps_number }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row form-group">
                        <div class="col-xl-3">
                            <label for="preselect"><input required type="radio" id="preselect" value="preselect" name="submitType">Preselect</label>
                        </div>
                        <div class="col-xl-3">
                            <label for="sendEmails"><input required type="radio" id="sendEmails" value="sendEmails" name="submitType">Send Emails</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success submit" type="submit" id="submitBtn" disabled>Save/Send</button>
                        <button class="btn btn-danger" data-toggle="modal" data-target="#cancelITBModal">Cancel ITB</button>
                    </div>
                    <input type="hidden" name="invitation_id" value="{{ $itb->invite->id }}">
                {{ Form::close() }}
            @elseif ($itb->invite->status === 'FARMED_OUT')
                <div class="col-xl-12">
                    <h1>Selected Suppliers</h1>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead class="table-primary">
                            <tr>
                                <th>Company Name</th>
                                <th>Authorized Personnel</th>
                                <th>Category</th>
                                <th>Address</th>
                                <th>Telephone</th>
                                <th>Email</th>
                                <th>TIN</th>
                                <th>Mayor's Permit Expiry Date</th>
                                <th>Philgeps Number</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($itb->invite->suppliers as $supplier)
                                <tr>
                                    <td>{{ $supplier->company_name }}</td>
                                    <td>{{ $supplier->authorized_personnel }}</td>
                                    <td>{{ $supplier->category }}</td>
                                    <td>{{ $supplier->address }}</td>
                                    <td>{{ $supplier->telephone }}</td>
                                    <td>{{ $supplier->email }}</td>
                                    <td>{{ $supplier->tin_number }}</td>
                                    <td>{{ $supplier->mayors_permit }}</td>
                                    <td>{{ $supplier->philgeps_number }}</td>
                                    @if($supplier->pivot->email_status === 'EMAIL_SENT')
                                        <td>{{ $supplier->pivot->email_status }} {{date('M d, Y h:i:s a', strtotime($itb->invite->updated_at))}}</td>
                                    @else
                                        <td><a href="#" class="btn btn-warning">Send Email</a></td>
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
        @elseif ($itb->invite->status !== 'CANCELLED')
            <div class="form-group">
                <button class="btn btn-danger" data-toggle="modal" data-target="#cancelITBModal">Cancel ITB</button>
            </div>
        @endif
    @endif
    
</div>
<div class="modal fade" id="cancelITBModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Cancel ITB</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <h3>Are you sure?</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
                    <a href="{{ url('/invitations/cancel/'.$itb->invite->id) }}" class="btn btn-danger">Yes</a>
                </div>
            </div>
        </div>
    </div>
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="{{ asset('js/itb/show.js') }}"></script>
@endsection