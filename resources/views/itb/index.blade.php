@extends('layouts.base')
@section('content')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <div class="frm">
        <div class="row">
            <div class="col-xl-12">
                <h1>Invitation to Bids List</h1>
            </div>
            <div class="col-xl-12">
                <table id="itbTable" class="table table-striped table-hover">
                    <thead class="table-primary">
                        <tr>
                            <th>ITB No.</th>
                            <th>PR No.</th>
                            <th>Name of Project</th>
                            <th>Year</th>
                            <th>ABC</th>
                            <th>Chairperson</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($itbs as $itb)
                            <tr>
                                <td>{{ $itb->itb_no }}</td>
                                <td>{{ $itb->pr->pr_no }}</td>
                                <td>{{ $itb->name_of_project }}</td>
                                <td>{{ $itb->year }}</td>
                                <td>{{ $itb->abc }}</td>
                                <td>{{ $itb->chairperson_signatory }}</td>
                                <td>
                                    <a href="{{ url('/itb/'.$itb->id) }}" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i></a>
                                    <a href="{{ url('/itb/'.$itb->id.'/edit') }}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a>
                                    <a href="{{ url('/itb/'.$itb->id.'/delete') }}" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script>
        $('#itbTable').DataTable();
    </script>
@endsection