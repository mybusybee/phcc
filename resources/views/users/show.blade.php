@extends('layouts.base')

@section('content')
<div class="container-fluid">
	<div class="row frm col-md-5 mx-auto">
		<div class="col-md-12">
			<label for="">First Name: </label>
			<span>{{ $user->first_name }}</span>
		</div>
		<div class="col-md-12">
			<label for="">Last Name: </label>
			<span>{{ $user->last_name }}</span>
		</div>
		<div class="col-md-12">
			<label for="">Email: </label>
			<span>{{ $user->email }}</span>
		</div>
		<div class="col-md-12">
			<label for="">User Role: </label>
			<span>{{ $user->role->user_role }}</span>
		</div>
		<div class="col-md-12">
			<label for="">Office: </label>
			<span>{{ $user->office->office }}</span>
		</div>
	</div>
</div>
@endsection