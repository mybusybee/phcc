@extends('layouts.base')

@section('content')
<div class="container-fluid">
	<div class="row col-md-4 frm center-div">
		<h1>Create User</h1>
		{{ Form::open(['url' => url('/user'), 'method' => 'post', 'class' => 'w-100']) }}
		<div class="form-group">
			<label for="email">First Name:</label>
			{{ Form::text('fname', null, ['class' => 'form-control', 'id' => 'fname', 'required']) }}
		</div>
		<div class="form-group">
			<label for="email">Last Name:</label>
			{{ Form::text('lname', null, ['class' => 'form-control', 'id' => 'lname', 'required']) }}
		</div>
		<div class="form-group">
			<label for="email">Email Address:</label>
			@if($errors->has('email'))
				{{ Form::email('email', null, ['class' => 'form-control border border-danger', 'id' => 'email', 'required']) }}
			@else
				{{ Form::email('email', null, ['class' => 'form-control', 'id' => 'email', 'required']) }}
			@endif
		</div>
		<div class="form-group">
			<label for="pwd">Password:</label>
			{{ Form::password('password', ['class' => 'form-control', 'id' => 'pwd', 'required']) }}
		</div>
		<div class="form-group">
			<label for="pwd">Confirm Password:</label>
			{{ Form::password('password_confirmation', ['class' => 'form-control', 'id' => 'confirm_pwd', 'required']) }}
		</div>
		<div class="form-group">
			<label for="user_role">User Role / Type:</label>
			<select name="user_role" id="user_role" class="form-control" required>
				@foreach($roles as $role)
				<option value="{{ $role->id }}">{{ $role->user_role }}</option>
				@endforeach
			</select>
		</div>
		<div class="form-group">
			<label for="office">Office:</label>
			{{ Form::select('office_id', $offices, null, ['class' => 'form-control', 'id' => 'office', 'required']) }}
		</div>
		<div class="form-group text-center">
			{{ Form::submit('Submit', ['class' => 'btn btn-success col-md-4', 'id' => 'submit-btn', 'disabled']) }}
		</div>
		{{ Form::close() }}
	</div>
	
</div>
<script src="{{ asset('js/createuser.js') }}"></script>
@endsection