@extends('layouts.base')
@section('content')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<div class="container-fluid">
	<div class="row">
		<h1>Users List</h1>
		<div class="col-xl-4 offset-xl-8 form-group text-right">
			<a href="{{ url('/user/create') }}" class="btn btn-success ">Create new...</a>
		</div>
		<div class="col-md-12 mx-auto table-responsive">
			<table class="table table-bordered text-center" id="userIndexTable">
				<thead class="table-primary">
					<th>ID</th>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Email</th>
					<th>User Role</th>
					<th>Office</th>
					<th>Active</th>
					@if (session()->get('user_role') == 1)
					<th>Action</th>
					@endif
				</thead>
				<tbody>
					@foreach($users as $user)
					<tr>
						<td>{{ $user->id }}</td>
						<td>{{ $user->first_name }}</td>
						<td>{{ $user->last_name }}</td>
						<td>{{ $user->email }}</td>
						<td>{{ $user->role->user_role }}</td>
						<td>{{ $user->office->office }}</td>
						<td>{{ $user->is_active }}</td>
						@if (session()->get('user_role') == 1)
						<td class="action-buttons">
							{{-- <a href="{{ url('/user/'.$user->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a> --}}
							
							<a href="{{ url('/user/'.$user->id.'/edit') }}" class="btn btn-info"><i class="fa fa-edit"></i></a>
							@if ($user->is_active == 1)
								<a href="{{ url('/user/'.$user->id.'/deactivate') }}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
							@else
								<a href="{{ url('/user/'.$user->id.'/activate') }}" class="btn btn-info"><i class="fa fa-toggle-on"></i></a>
							@endif
						</td>
						@endif
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
	$('#userIndexTable').DataTable();
</script>
@endsection