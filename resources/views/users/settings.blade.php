@extends('layouts.base')

@section('content')
	<div class="container-fluid">
		
		<div class="row col-md-6 frm center-div">
			<h2>Super Admin Controls</h2>
			{{ Form::open(['url' => url('/settings/update'), 'method' => 'post']) }}
			@foreach ($settings as $setting)
			<div class="form-group">
				<h3>{{$setting->control}}</h3>
				@if ($setting->is_active)
					<label class="radio">
				      <input type="radio" name="ppmp_control" value="on" checked>On
				    </label>
				    <label class="radio">
				      <input type="radio" name="ppmp_control" value="off">Off
				    </label>
				@else
					<label class="radio">
				      <input type="radio" name="ppmp_control" value="on">On
				    </label>
				    <label class="radio">
				      <input type="radio" name="ppmp_control" value="off" checked>Off
				    </label>
				@endif
			</div>	
			@endforeach
			<div class="form-group ctr-text">
				{{ Form::submit('Update Settings', ['class' => 'btn btn-success submit-50', 'id' => 'submit-btn']) }}
			</div>
			{{ Form::close() }}
		</div>
	</div>
@endsection
