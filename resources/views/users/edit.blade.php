@extends('layouts.base')

@section('content')
<div class="container-fluid">
	<div class="row ctr-30 frm">
		{{ Form::open(['url' => url('/user/' . $user->id), 'method' => 'put']) }}
		<div class="form-group">
			<label for="email">First Name:</label>
			{{ Form::text('fname', $user->first_name, ['class' => 'form-control', 'id' => 'fname', 'required']) }}
		</div>
		<div class="form-group">
			<label for="email">Last Name:</label>
			{{ Form::text('lname', $user->last_name, ['class' => 'form-control', 'id' => 'lname', 'required']) }}
		</div>
		<div class="form-group">
			<label for="email">Email Address:</label>
			{{ Form::email('email', $user->email, ['class' => 'form-control', 'id' => 'email', 'required']) }}
		</div>
		<div class="form-group">
			<label for="user_role">User Role / Type:</label>
			{{ Form::select('user_role', $roles, $user->user_role, ['class' => 'form-control', 'id' => 'user_role', 'required']) }}
		</div>
		<div class="form-group">
			<label for="office">Office:</label>
			{{ Form::select('office', $offices, $user->office_id, ['class' => 'form-control', 'id' => 'office', 'required']) }}
		</div>
		<div class="form-group ctr-text">
			{{ Form::submit('Submit', ['class' => 'btn btn-success submit-50', 'id' => 'submit-btn']) }}
		</div>
		{{ Form::close() }}
	</div>
</div>
<script>
	
</script>
@endsection