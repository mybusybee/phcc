@extends('layouts.base')

@section('content')
    <div id="uploaded_pmr" class="frm">
        <div class="row">
            <div class="col-xl-12">
                <h4 class="">Philippine Competition Commission Procurement Monitoring Report as of {{ Form::select('', [null => 'Please select one...', '1' => '1st Semester', '2' => '2nd Semester'], null, ['class' => 'form-control form-control-sm w-25 d-inline-block' , '@change' => 'getItems', 'v-model' => 'semester']) }} of {{ Form::select('', [null => 'Please select one...'], null, ['class' => 'form-control form-control-sm w-25 d-inline-block', 'id' => 'year', '@change' => 'getItems', 'v-model' => 'year']) }}</h4>
            </div>
        </div>
        <div class="table-responsive table-wrapper">
            <table class="table table-striped table-hover custom-table-bordered" id="pmrTbl">
                <thead class="table-primary">
                    <tr class="text-center">
                        <th style="vertical-align: middle;" class="resize-min-width-half" rowspan="2">P.R. No.</th>
                        <th style="vertical-align: middle;" rowspan="2">Code (UACS/PAP)</th>
                        <th style="vertical-align: middle;" class="resize-min-width" rowspan="2">Procurement Program/Project</th>
                        <th style="vertical-align: middle;" class="resize-min-width" rowspan="2">PMO/End-User</th>
                        <th style="vertical-align: middle;" class="resize-min-width" rowspan="2">Mode of Procurement</th>
                        <th style="vertical-align: middle;" class="resize-min-width-half" rowspan="2">P.R. Date Received</th>
                        <th style="vertical-align: middle;" colspan="12">Actual Procurement Activity</th>
                        <th style="vertical-align: middle;" rowspan="2">Source of Funds</th>
                        <th style="vertical-align: middle;" colspan="3">ABC (PhP)</th>
                        <th style="vertical-align: middle;" colspan="3">Contract Cost (PhP)</th>
                        <th style="vertical-align: middle;" rowspan="2">List of Invited Observers</th>
                        <th style="vertical-align: middle;" colspan="6">Date of Receipt of Invitation</th>
                        <th style="vertical-align: middle;" rowspan="2" class="resize-min-width">Remarks (Explaining changes from the APP)</th>
                        <th style="vertical-align: middle;" rowspan="2" class="resize-min-width">Status</th>
                    </tr>
                    <tr class="text-center">
                        <th style="vertical-align: middle;" class="resize-min-width-half">Pre-Proc Conference</th>
                        <th style="vertical-align: middle;" class="resize-min-width-half">Ads/Post of IB</th>
                        <th style="vertical-align: middle;" class="resize-min-width-half">Pre-bid Conf</th>
                        <th style="vertical-align: middle;" class="resize-min-width-half">Eligibility Check</th>
                        <th style="vertical-align: middle;" class="resize-min-width-half">Sub/Open of Bids</th>
                        <th style="vertical-align: middle;" class="resize-min-width-half">Bid Evaluation</th>
                        <th style="vertical-align: middle;" class="resize-min-width-half">Post Qual</th>
                        <th style="vertical-align: middle;" class="resize-min-width-half">Notice of Award</th>
                        <th style="vertical-align: middle;" class="resize-min-width-half">Contract Signing</th>
                        <th style="vertical-align: middle;" class="resize-min-width-half">Notice to Proceed</th>
                        <th style="vertical-align: middle;" class="resize-min-width-half">Delivery/Completion</th>
                        <th style="vertical-align: middle;" class="resize-min-width-half">Inspection & Acceptance</th>
                        <th style="vertical-align: middle;" class="resize-min-width-half">Total</th>
                        <th style="vertical-align: middle;" class="resize-min-width-half">MOOE</th>
                        <th style="vertical-align: middle;" class="resize-min-width-half">CO</th>
                        <th style="vertical-align: middle;" class="resize-min-width-half">Total</th>
                        <th style="vertical-align: middle;" class="resize-min-width-half">MOOE</th>
                        <th style="vertical-align: middle;" class="resize-min-width-half">CO</th>
                        <th style="vertical-align: middle;" class="resize-min-width-half">Pre-bid Conf</th>
                        <th style="vertical-align: middle;" class="resize-min-width-half">Eligiblity Check</th>
                        <th style="vertical-align: middle;" class="resize-min-width-half">Sub/Open of Bids</th>
                        <th style="vertical-align: middle;" class="resize-min-width-half">Bid Evaluation</th>
                        <th style="vertical-align: middle;" class="resize-min-width-half">Post Qual</th>
                        <th style="vertical-align: middle;" class="resize-min-width-half">Delivery/Completion/Acceptance(if applicable)</th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="table-primary">
                        <th colspan="99">COMPLETED PROCUREMENT ACTIVITIES</th>
                    </tr>
                    @include('pmr.uploaded.show.completed')
                    <tr class="table-primary">
                        <th colspan="99">ONGOING PROCUREMENT ACTIVITIES</th>
                    </tr>
                    @include('pmr.uploaded.show.ongoing')
                </tbody>
            </table>
        </div>
    </div>
    @push('scripts')
        <script src="{{ asset('js/pmr/uploaded/show.js') }}"></script>
    @endpush
@endsection