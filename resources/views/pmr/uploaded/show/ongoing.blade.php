<tr v-if="!getOngoing.length">
    <th colspan="99" style="font-size: 30px;">@{{ tableText }}</th>
</tr>
<tr v-else v-for="ongoing in getOngoing" :key="ongoing.id">
    <td class="d-none"><input type="hidden" name="" :value="ongoing.id"></td>
    <td>@{{ ongoing.pr_no }}</td>
    <td>@{{ ongoing.code }}</td>
    <td>@{{ ongoing.program_project }}</td>
    <td>@{{ ongoing.end_user.office }}</td>
    <td>@{{ ongoing.procurement_mode.mode }}</td>
    <td>@{{ ongoing.pr_date_received }}</td>
    <td>@{{ ongoing.apa__pre_proc_conference }}</td>
    <td>@{{ ongoing.apa__ads_post_of_ib }}</td>
    <td>@{{ ongoing.apa__pre_bid_conf }}</td>
    <td>@{{ ongoing.apa__eligibility_check }}</td>
    <td>@{{ ongoing.apa__sub_open_of_bids }}</td>
    <td>@{{ ongoing.apa__bid_evaluation }}</td>
    <td>@{{ ongoing.apa__post_qual }}</td>
    <td>@{{ ongoing.apa__notice_of_award }}</td>
    <td>@{{ ongoing.apa__contract_signing }}</td>
    <td>@{{ ongoing.apa__notice_to_proceed }}</td>
    <td>@{{ ongoing.apa__delivery_completion }}</td>
    <td>@{{ ongoing.apa__inspection_and_acceptance }}</td>
    <td>@{{ ongoing.source_of_funds }}</td>
    <td>@{{ ongoing.abc_total }}</td>
    <td>@{{ ongoing.abc_mooe }}</td>
    <td>@{{ ongoing.abc_co }}</td>
    <td>@{{ ongoing.contract_cost_total }}</td>
    <td>@{{ ongoing.contract_cost_mooe }}</td>
    <td>@{{ ongoing.contract_cost_co }}</td>
    <td>@{{ ongoing.list_of_invited_observers }}</td>
    <td>@{{ ongoing.dri__pre_bid_conf }}</td>
    <td>@{{ ongoing.dri__eligibility_check }}</td>
    <td>@{{ ongoing.dri__sub_open_of_bids }}</td>
    <td>@{{ ongoing.dri__bid_evaluation }}</td>
    <td>@{{ ongoing.dri__post_qual }}</td>
    <td>@{{ ongoing.dri__delivery_completion_acceptance }}</td>
    <td>@{{ ongoing.remarks }}</td>
    <td>@{{ ongoing.status }}</td>
</tr>
<tr>
    <th colspan="19" class="text-right table-primary">Total Alloted Budget of On-going Procurement Activities</th>
    <th v-if="getPMR">&#8369; @{{ getPMR.pmrDetails.ongoing_total_budget }}</th>
    <th v-else>N/A</th>
</tr>