<tr v-if="!getCompleted.length">
    <th colspan="99" style="font-size: 30px;">@{{ tableText }}</th>
</tr>
<tr v-else v-for="completed in getCompleted" :key="completed.id">
    <td class="d-none"><input type="hidden" name="" :value="completed.id"></td>
    <td>@{{ completed.pr_no }}</td>
    <td>@{{ completed.code }}</td>
    <td>@{{ completed.program_project }}</td>
    <td>@{{ completed.end_user.office }}</td>
    <td>@{{ completed.procurement_mode.mode }}</td>
    <td>@{{ completed.pr_date_received }}</td>
    <td>@{{ completed.apa__pre_proc_conference }}</td>
    <td>@{{ completed.apa__ads_post_of_ib }}</td>
    <td>@{{ completed.apa__pre_bid_conf }}</td>
    <td>@{{ completed.apa__eligibility_check }}</td>
    <td>@{{ completed.apa__sub_open_of_bids }}</td>
    <td>@{{ completed.apa__bid_evaluation }}</td>
    <td>@{{ completed.apa__post_qual }}</td>
    <td>@{{ completed.apa__notice_of_award }}</td>
    <td>@{{ completed.apa__contract_signing }}</td>
    <td>@{{ completed.apa__notice_to_proceed }}</td>
    <td>@{{ completed.apa__delivery_completion }}</td>
    <td>@{{ completed.apa__inspection_and_acceptance }}</td>
    <td>@{{ completed.source_of_funds }}</td>
    <td>@{{ completed.abc_total }}</td>
    <td>@{{ completed.abc_mooe }}</td>
    <td>@{{ completed.abc_co }}</td>
    <td>@{{ completed.contract_cost_total }}</td>
    <td>@{{ completed.contract_cost_mooe }}</td>
    <td>@{{ completed.contract_cost_co }}</td>
    <td>@{{ completed.list_of_invited_observers }}</td>
    <td>@{{ completed.dri__pre_bid_conf }}</td>
    <td>@{{ completed.dri__eligibility_check }}</td>
    <td>@{{ completed.dri__sub_open_of_bids }}</td>
    <td>@{{ completed.dri__bid_evaluation }}</td>
    <td>@{{ completed.dri__post_qual }}</td>
    <td>@{{ completed.dri__delivery_completion_acceptance }}</td>
    <td>@{{ completed.remarks }}</td>
    <td>@{{ completed.status }}</td>
</tr>
<tr>
    <th colspan="19" class="text-right table-primary">Total Allotted Budget of Procurement Activities</th>
    {{-- <th v-if="getPMR">&#8369; @{{ getPMR.total_completed_alloted_budget.toLocaleString() }}</th> --}}
    <th v-if="getPMR">&#8369; @{{ getPMR.pmrDetails.completed.total_alloted_budget }}</th>
    <th v-else>N/A</th>
</tr>
<tr>
    <th colspan="19" class="text-right table-primary">Total Contract Price of Procurement Activities Conducted</th>
    {{-- <th v-if="getPMR">&#8369; @{{ getPMR.total_completed_contract_cost.toLocaleString() }}</th> --}}
    <th v-if="getPMR">&#8369; @{{ getPMR.pmrDetails.completed.total_contract_price }}</th>
    <th v-else>N/A</th>
</tr>
<tr>
    <th colspan="19" class="text-right table-primary">Total Savings (Total Allotted Budget - Total Contract Price)</th>
    {{-- <th v-if="getPMR">&#8369; @{{ getPMR.total_completed_savings.toLocaleString() }}</th> --}}
    <th v-if="getPMR">&#8369; @{{ getPMR.pmrDetails.completed.total_savings }}</th>
    <th v-else>N/A</th>
</tr>