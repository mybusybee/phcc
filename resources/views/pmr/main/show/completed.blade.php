<tr v-if="!getCompleted.length">
        <th colspan="99" style="font-size: 30px;">@{{ tableText }}</th>
    </tr>
    <tr v-else v-for="completed in getCompleted" :key="completed.id">
        <td class="d-none"><input type="hidden" name="" :value="completed.id"></td>
        <td>@{{ completed.pr_no }}</td>
        <td>@{{ completed.code }}</td>
        <td>
            <ol>
                <li v-for="pp in completed.program_project">@{{ pp }}</li>
            </ol>
        </td>
        <td>@{{ completed.end_user }}</td>
        <td>@{{ completed.procurement_mode }}</td>
        <td>@{{ completed.pr_received_date }}</td>
        <td>@{{ completed.pre_proc_conference }}</td>
        <td>@{{ completed.ads_post_of_ib }}</td>
        <td>@{{ completed.pre_bid_conf }}</td>
        <td>@{{ completed.eligibility_check }}</td>
        <td>@{{ completed.sub_open_of_bids }}</td>
        <td>@{{ completed.bid_evaluation }}</td>
        <td>@{{ completed.post_qual }}</td>
        <td>@{{ completed.noa }}</td>
        <td>@{{ completed.contract_signing }}</td>
        <td>@{{ completed.ntp }}</td>
        <td>@{{ completed.delivery_completion }}</td>
        <td>@{{ completed.inspection_acceptance }}</td>
        <td>@{{ completed.source_of_funds }}</td>
        <td>@{{ completed.abc_total }}</td>
        <td>@{{ completed.abc_mooe }}</td>
        <td>@{{ completed.abc_co }}</td>
        <td>@{{ completed.contract_cost_total }}</td>
        <td>@{{ completed.contract_cost_mooe }}</td>
        <td>@{{ completed.contract_cost_co }}</td>
        <td>@{{ completed.list_of_invited_observers }}</td>
        <td>@{{ completed.receipt_pre_bid_conf }}</td>
        <td>@{{ completed.receipt_eligibility_check }}</td>
        <td>@{{ completed.receipt_sub_open_of_bids }}</td>
        <td>@{{ completed.receipt_bid_evaluation }}</td>
        <td>@{{ completed.receipt_post_qual }}</td>
        <td>@{{ completed.receipt_delivery_completion }}</td>
        <td>@{{ completed.remarks }}</td>
        <td>@{{ completed.status }}</td>
    </tr>
    <tr>
        <th colspan="19" class="text-right table-primary">Total Allotted Budget of Procurement Activities</th>
        <th v-if="getPMR">&#8369; @{{ getPMR.pmrDetails.completed.total_alloted_budget.toLocaleString() }}</th>
        <th v-else>N/A</th>
    </tr>
    <tr>
        <th colspan="19" class="text-right table-primary">Total Contract Price of Procurement Activities Conducted</th>
        <th v-if="getPMR">&#8369; @{{ getPMR.pmrDetails.completed.total_contract_price.toLocaleString() }}</th>
        <th v-else>N/A</th>
    </tr>
    <tr>
        <th colspan="19" class="text-right table-primary">Total Savings (Total Allotted Budget - Total Contract Price)</th>
        <th v-if="getPMR">&#8369; @{{ (getPMR.pmrDetails.completed.total_alloted_budget - getPMR.pmrDetails.completed.total_contract_price).toLocaleString() }}</th>
        <th v-else>N/A</th>
    </tr>