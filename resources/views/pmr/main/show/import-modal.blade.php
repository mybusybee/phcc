<div class="modal fade" id="uploadPMRModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        {{ Form::open(['url' => url('upload-pmr'), 'method' => 'post', 'files' => true]) }}
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="label">Upload Procurement Monitoring Report</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    Upload PMR (xlsx/xls/csv files only): <input type="file" name="file" class="form-control" required accept=".xlsx,.xls,.csv">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary submitAPP w-100">Upload</button>
                    <a href="{{ url('/pmr/template/download') }}" class="btn btn-info"><i class="fas fa-download"></i> Download Template</a>
                </div>
            </div>
        {{ Form::close() }}
    </div>
</div>