@foreach($pmrModifiedObj->ongoing as $item)
    <tr>
        @include('pmr.main.export.ongoing-items')
    </tr>
@endforeach
<tr>
    <th colspan="19" style="text-align: right;">Total Allotted Budget of On-going Procurement Activities</th>
    <th colspan="6">&#8369; {{ number_format($pmrModifiedObj->pmrDetails['ongoing_total_budget'], 2, '.', ',') }}</th>
</tr>