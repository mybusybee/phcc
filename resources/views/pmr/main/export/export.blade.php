<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/pmrtable.css') }}">
</head>
<body>
    <br><br><br><br><br><br>
    <table class="custom-table-bordered">
        <thead>
            <tr>
                <th colspan="34" style="border: 0 !important;">Philippine Competition Commission Procurement Monitoring Report as of {{ $pmrModifiedObj->semester }} Semester of {{ $pmrModifiedObj->year }}</th>
            </tr>
            <tr>
                <th colspan="34" style="border-top: 0 !important; border-right: 0 !important; border-left: 0 !important;"></th>
            </tr>
            <tr>
                <th colspan="6"></th>
                <th colspan="12" style="text-align: center;">Actual Procurement Activity</th>
                <th></th>
                <th colspan="3">ABC (PhP)</th>
                <th colspan="3">Contract Cost (PhP)</th>
                <th></th>
                <th colspan="6">Date of Receipt of Invitation</th>
                <th colspan="2"></th>
            </tr>
            <tr>
                <th>P.R. No.</th>
                <th>Code (UACS/PAP)</th>
                <th>Procurement Program/Project</th>
                <th>PMO/End-User</th>
                <th>Mode of Procurement</th>
                <th>P.R. Date Received</th>
                <th>Pre-Proc Conference</th>
                <th>Ads/Post of IB</th>
                <th>Pre-bid Conf</th>
                <th>Eligibility Check</th>
                <th>Sub/Open of Bids</th>
                <th>Bid Evaluation</th>
                <th>Post Qual</th>
                <th>Notice of Award</th>
                <th>Contract Signing</th>
                <th>Notice to Proceed</th>
                <th>Delivery/Completion</th>
                <th>Inspection & Acceptance</th>
                <th>Source of Funds</th>
                <th>Total</th>
                <th>MOOE</th>
                <th>CO</th>
                <th>Total</th>
                <th>MOOE</th>
                <th>CO</th>
                <th>List of Invited Observers</th>
                <th>Pre-bid Conf</th>
                <th>Eligibility Check</th>
                <th>Sub/Open of Bids</th>
                <th>Bid Evaluation</th>
                <th>Post Qual</th>
                <th>Delivery/Completion/Acceptance(if applicable)</th>
                <th>Remarks(Explaining changes from the APP)</th>
                <th>Status</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th colspan="34" style="text-align: left;">COMPLETED PROCUREMENT ACTIVITIES</th>
            </tr>
            @include('pmr.main.export.completed')
            <tr>
                <th colspan="34" style="text-align: left;">ONGOING PROCUREMENT ACTIVITIES</th>
            </tr>
            @include('pmr.main.export.ongoing')
        </tbody>
    </table>
</body>
</html>