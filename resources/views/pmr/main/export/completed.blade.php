@foreach($pmrModifiedObj->completed as $item)
    <tr>
        @include('pmr.main.export.completed-items')
    </tr>
@endforeach
<tr>
    <th colspan="19" style="text-align: right;">Total Allotted Budget of Procurement Activities</th>
    <th colspan="6">&#8369; {{ number_format($pmrModifiedObj->pmrDetails['completed']['total_alloted_budget'], 2, '.', ',') }}</th>
</tr>
<tr>
    <th colspan="19" style="text-align: right;">Total Contract Price of Procurement Activities Conducted</th>
    <th colspan="6">&#8369; {{ number_format($pmrModifiedObj->pmrDetails['completed']['total_contract_price'], 2, '.', ',') }}</th>
</tr>
<tr>
    <th colspan="19" style="text-align: right;">Total Savings (Total Alloted Budget - Total Contract Price</th>
    <th colspan="6">&#8369; {{ number_format($pmrModifiedObj->pmrDetails['completed']['total_alloted_budget'] - $pmrModifiedObj->pmrDetails['completed']['total_contract_price'], 2, '.', ',') }}</th>
</tr>