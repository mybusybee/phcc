@extends('layouts.base')

@section('content')
    <div class="frm col-xl-3 mx-auto">
        <h3>Personal PMR List</h3>
        <div class="table-responsive">
            <table class="table table-striped table-hover text-center">
                <thead class="table-primary">
                    <tr>
                        <th>Year</th>
                        <th>Semester</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($pmrs as $pmr)
                        <tr>
                            <td>{{ $pmr->year }}</td>
                            <td>{{ $pmr->semester === 1 ? '1st' : '2nd' }}</td>
                            <td><a href="{{ url('/personal-pmr/'.$pmr->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection