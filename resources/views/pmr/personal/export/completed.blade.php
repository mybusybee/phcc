@foreach($pmr->pmr_items->where('completed', 1) as $item)
    <tr>
        @include('pmr.personal.export.completed-items')
    </tr>
@endforeach
<tr>
    <th colspan="19" style="text-align: right;">Total Allotted Budget of Procurement Activities</th>
    <th colspan="6">&#8369; {{ number_format($pmr->total_completed_alloted_budget, 2, '.', ',') }}</th>
</tr>
<tr>
    <th colspan="19" style="text-align: right;">Total Contract Price of Procurement Activities Conducted</th>
    <th colspan="6">&#8369; {{ number_format($pmr->total_completed_contract_cost, 2, '.', ',') }}</th>
</tr>
<tr>
    <th colspan="19" style="text-align: right;">Total Savings (Total Alloted Budget - Total Contract Price</th>
    <th colspan="6">&#8369; {{ number_format($pmr->total_completed_alloted_budget - $pmr->total_completed_contract_cost, 2, '.', ',') }}</th>
</tr>