@foreach($pmr->pmr_items->where('completed', 0) as $item)
    <tr>
        @include('pmr.personal.export.ongoing-items')
    </tr>
@endforeach
<tr>
    <th colspan="19" style="text-align: right;">Total Allotted Budget of On-going Procurement Activities</th>
    <th colspan="6">&#8369; {{ number_format($pmr->total_ongoing_abc, 2, '.', ',') }}</th>
</tr>