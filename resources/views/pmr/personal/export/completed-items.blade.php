<td>{{ $item->pr->pr_no }}</td>
<td>{{ $item->code }}</td>
<td>
    <ol>
        @foreach($item->program_projects as $pp)
            <li>{{ $pp }}</li>
        @endforeach
    </ol>
</td>
<td>{{ $item->end_user }}</td>
<td>{{ $item->procurement_mode }}</td>
<td>{{ $item->pr->date_received === null ? 'Not yet received.' : $item->pr->date_received->format('F d, Y - h:i:s A') }}</td>
@if($item->pre_proc_conference !== null)
    <td>{{ $item->pre_proc_conference }}</td>
@else
    <td>N/A</td>
@endif
@if($item->ads_post_of_ib !== null)
    <td>{{ $item->ads_post_of_ib }}</td>
@else
    <td>N/A</td>
@endif
@if($item->pre_bid_conf !== null)
    <td>{{ $item->pre_bid_conf }}</td>
@else
    <td>N/A</td>
@endif
@if($item->eligibility_check !== null)
    <td>{{ $item->eligibility_check }}</td>
@else
    <td>N/A</td>
@endif
@if($item->sub_open_of_bids !== null)
    <td>{{ $item->sub_open_of_bids }}</td>
@else
    <td>N/A</td>
@endif
@if($item->bid_evaluation !== null)
    <td>{{ $item->bid_evaluation }}</td>
@else
    <td>N/A</td>
@endif
@if($item->post_qual !== null)
    <td>{{ $item->post_qual }}</td>
@else
    <td>N/A</td>
@endif
@if($item->noa !== null)
    <td>{{ $item->noa }}</td>
@else
    <td>N/A</td>
@endif
@if($item->contract_signing !== null)
    <td>{{ $item->contract_signing }}</td>
@else
    <td>N/A</td>
@endif
@if($item->ntp !== null)
    <td>{{ $item->ntp }}</td>
@else
    <td>N/A</td>
@endif
@if($item->delivery_completion !== null)
    <td>{{ $item->delivery_completion }}</td>
@else
    <td>N/A</td>
@endif
@if($item->inspection_acceptance !== null)
    <td>{{ $item->inspection_acceptance }}</td>
@else
    <td>N/A</td>
@endif
<td>{{ $item->source_of_funds }}</td>
<td>&#8369; {{ $item->pr->pr_item_total->total_estimate }}</td>
@if($item->abc_mooe !== null)
    <td>&#8369; {{ $item->abc_mooe }}</td>
@else
<td>N/A</td>
@endif
@if($item->abc_co !== null)
    <td>&#8369; {{ $item->abc_co }}</td>
@else
<td>N/A</td>
@endif
<td>&#8369; {{ $item->total_contract_cost }}</td>
@if($item->contract_cost_mooe !== null)
    <td>&#8369; {{ $item->contract_cost_mooe }}</td>
@else
<td>N/A</td>
@endif
@if($item->contract_cost_co !== null)
    <td>&#8369; {{ $item->contract_cost_co }}</td>
@else
<td>N/A</td>
@endif
@if($item->list_of_invited_observers !== null)
    <td>{{ $item->list_of_invited_observers }}</td>
@else
<td>N/A</td>
@endif
@if($item->receipt_pre_bid_conf !== null)
    <td>{{ $item->receipt_pre_bid_conf }}</td>
@else
<td>N/A</td>
@endif
@if($item->receipt_eligibility_check !== null)
    <td>{{ $item->receipt_eligibility_check }}</td>
@else
<td>N/A</td>
@endif
@if($item->receipt_sub_open_of_bids !== null)
    <td>{{ $item->receipt_sub_open_of_bids }}</td>
@else
<td>N/A</td>
@endif
@if($item->receipt_bid_evaluation !== null)
    <td>{{ $item->receipt_bid_evaluation }}</td>
@else
<td>N/A</td>
@endif
@if($item->receipt_post_qual !== null)
    <td>{{ $item->receipt_post_qual }}</td>
@else
<td>N/A</td>
@endif
@if($item->receipt_delivery_completion !== null)
    <td>{{ $item->receipt_delivery_completion }}</td>
@else
<td>N/A</td>
@endif
@if($item->remarks !== null)
    <td>{{ $item->remarks }}</td>
@else
<td>N/A</td>
@endif
<td>
    {{ $item->status }}
</td>