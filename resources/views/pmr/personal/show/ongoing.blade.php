<tr v-if="!getOngoing.length">
    <th colspan="99" style="font-size: 30px;">@{{ tableText }}</th>
</tr>
<tr v-else v-for="ongoing in getOngoing" :key="ongoing.id">
    <td class="d-none"><input type="hidden" name="" :value="ongoing.id"></td>
    <td>@{{ ongoing.pr_no }}</td>
    <td>@{{ ongoing.code }}</td>
    <td>
        <ol>
            <li v-for="pp in ongoing.program_project">@{{ pp }}</li>
        </ol>
    </td>
    <td>@{{ ongoing.end_user }}</td>
    <td>@{{ ongoing.procurement_mode }}</td>
    <td>@{{ ongoing.pr_received_date }}</td>
    <td>@{{ ongoing.pre_proc_conference }}</td>
    <td>@{{ ongoing.ads_post_of_ib }}</td>
    <td>@{{ ongoing.pre_bid_conf }}</td>
    <td>@{{ ongoing.eligibility_check }}</td>
    <td>@{{ ongoing.sub_open_of_bids }}</td>
    <td>@{{ ongoing.bid_evaluation }}</td>
    <td>@{{ ongoing.post_qual }}</td>
    <td>@{{ ongoing.noa }}</td>
    <td>@{{ ongoing.contract_signing }}</td>
    <td>@{{ ongoing.ntp }}</td>
    <td>@{{ ongoing.delivery_completion }}</td>
    <td>@{{ ongoing.inspection_acceptance }}</td>
    <td>@{{ ongoing.source_of_funds }}</td>
    <td>&#8369; @{{ ongoing.abc_total }}</td>
    <td>@{{ ongoing.abc_mooe }}</td>
    <td>@{{ ongoing.abc_co }}</td>
    {{-- <td>&#8369; @{{ ongoing.contract_cost_total.toLocaleString() }}</td> --}}
    <td>&#8369; @{{ ongoing.contract_cost_total }}</td>
    <td>@{{ ongoing.contract_cost_mooe }}</td>
    <td>@{{ ongoing.contract_cost_co }}</td>
    <td>@{{ ongoing.list_of_invited_observers }}</td>
    <td>@{{ ongoing.receipt_pre_bid_conf }}</td>
    <td>@{{ ongoing.receipt_eligibility_check }}</td>
    <td>@{{ ongoing.receipt_sub_open_of_bids }}</td>
    <td>@{{ ongoing.receipt_bid_evaluation }}</td>
    <td>@{{ ongoing.receipt_post_qual }}</td>
    <td>@{{ ongoing.receipt_delivery_completion }}</td>
    <td>@{{ ongoing.remarks }}</td>
    <td>@{{ ongoing.status }}</td>
</tr>
<tr>
    <th colspan="19" class="text-right table-primary">Total Alloted Budget of On-going Procurement Activities</th>

    {{-- <th v-if="getPMR">&#8369; @{{ getPMR.total_ongoing_abc.toLocaleString() }}</th> --}}
    <th v-if="getPMR">&#8369; @{{ getPMR.total_ongoing_abc }}</th>
    <th v-else>N/A</th>

    {{-- <th v-if="getPMR">&#8369; @{{ getPMR.total_ongoing_abc_mooe.toLocaleString() }}</th> --}}
    <th v-if="getPMR">&#8369; @{{ getPMR.total_ongoing_abc_mooe }}</th>
    <th v-else>N/A</th>

    {{-- <th v-if="getPMR">&#8369; @{{ getPMR.total_ongoing_abc_co.toLocaleString() }}</th> --}}
    <th v-if="getPMR">&#8369; @{{ getPMR.total_ongoing_abc_co }}</th>
    <th v-else>N/A</th>
    
    {{-- <th v-if="getPMR">&#8369; @{{ getPMR.total_ongoing_contract_cost.toLocaleString() }}</th> --}}
    <th v-if="getPMR">&#8369; @{{ getPMR.total_ongoing_contract_cost }}</th>
    <th v-else>N/A</th>

    {{-- <th v-if="getPMR">&#8369; @{{ getPMR.total_ongoing_contract_cost_mooe.toLocaleString() }}
    </th> --}}
    <th v-if="getPMR">&#8369; @{{ getPMR.total_ongoing_contract_cost_mooe }}
    <th v-else>N/A</th>

    {{-- <th v-if="getPMR">&#8369; @{{ getPMR.total_ongoing_contract_cost_co.toLocaleString() }}</th> --}}
    <th v-if="getPMR">&#8369; @{{ getPMR.total_ongoing_contract_cost_co }}</th>
    <th v-else>N/A</th>
    
</tr>