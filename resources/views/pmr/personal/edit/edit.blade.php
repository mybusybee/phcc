@extends('layouts.base')

@section('content')
<link rel="stylesheet" href="{{ asset('css/abstract/table.css') }}">
{{ Form::open(['url' => url('/personal-pmr/'.$pmr->id), 'method' => 'put']) }}
<div class="frm" id="pmr">
    <input type="hidden" ref="uid" value="{{ Auth::id() }}">
    <div class="row">
        <div class="col-xl-12">
            <h4 class="">Philippine Competition Commission Procurement Monitoring Report for {{ $pmr->sem }} of {{ $pmr->year }}</h4>
        </div>
    </div>
    <div class="table-responsive table-wrapper mb-3">
        <table class="table table-striped table-hover custom-table-bordered">
            <thead class="table-primary">
                <tr class="text-center">
                    <th style="vertical-align: middle;" class="resize-min-width-half" rowspan="2">P.R. No.</th>
                    <th style="vertical-align: middle;" rowspan="2">Code (UACS/PAP)</th>
                    <th style="vertical-align: middle;" class="resize-min-width" rowspan="2">Procurement Program/Project</th>
                    <th style="vertical-align: middle;" class="resize-min-width" rowspan="2">PMO/End-User</th>
                    <th style="vertical-align: middle;" class="resize-min-width" rowspan="2">Mode of Procurement</th>
                    <th style="vertical-align: middle;" class="resize-min-width-half" rowspan="2">P.R. Date Received</th>
                    <th style="vertical-align: middle;" colspan="12">Actual Procurement Activity</th>
                    <th style="vertical-align: middle;" rowspan="2">Source of Funds</th>
                    <th style="vertical-align: middle;" colspan="3">ABC (PhP)</th>
                    <th style="vertical-align: middle;" colspan="3">Contract Cost (PhP)</th>
                    <th style="vertical-align: middle;" rowspan="2">List of Invited Observers</th>
                    <th style="vertical-align: middle;" colspan="6">Date of Receipt of Invitation</th>
                    <th style="vertical-align: middle;" rowspan="2" class="resize-min-width">Remarks (Explaining changes from the APP)</th>
                    <th style="vertical-align: middle;" rowspan="2" class="resize-min-width">Status</th>
                </tr>
                <tr class="text-center">
                    <th style="vertical-align: middle;" class="resize-min-width-half">Pre-Proc Conference</th>
                    <th style="vertical-align: middle;" class="resize-min-width-half">Ads/Post of IB</th>
                    <th style="vertical-align: middle;" class="resize-min-width-half">Pre-bid Conf</th>
                    <th style="vertical-align: middle;" class="resize-min-width-half">Eligiiblity Check</th>
                    <th style="vertical-align: middle;" class="resize-min-width-half">Sub/Open of Bids</th>
                    <th style="vertical-align: middle;" class="resize-min-width-half">Bid Evaluation</th>
                    <th style="vertical-align: middle;" class="resize-min-width-half">Post Qual</th>
                    <th style="vertical-align: middle;" class="resize-min-width-half">Notice of Award</th>
                    <th style="vertical-align: middle;" class="resize-min-width-half">Contract Signing</th>
                    <th style="vertical-align: middle;" class="resize-min-width-half">Notice to Proceed</th>
                    <th style="vertical-align: middle;" class="resize-min-width-half">Delivery/Completion</th>
                    <th style="vertical-align: middle;" class="resize-min-width-half">Inspection & Acceptance</th>
                    <th style="vertical-align: middle;" class="resize-min-width-half">Total</th>
                    <th style="vertical-align: middle;" class="resize-min-width-half">MOOE</th>
                    <th style="vertical-align: middle;" class="resize-min-width-half">CO</th>
                    <th style="vertical-align: middle;" class="resize-min-width-half">Total</th>
                    <th style="vertical-align: middle;" class="resize-min-width-half">MOOE</th>
                    <th style="vertical-align: middle;" class="resize-min-width-half">CO</th>
                    <th style="vertical-align: middle;" class="resize-min-width-half">Pre-bid Conf</th>
                    <th style="vertical-align: middle;" class="resize-min-width-half">Eligiblity Check</th>
                    <th style="vertical-align: middle;" class="resize-min-width-half">Sub/Open of Bids</th>
                    <th style="vertical-align: middle;" class="resize-min-width-half">Bid Evaluation</th>
                    <th style="vertical-align: middle;" class="resize-min-width-half">Post Qual</th>
                    <th style="vertical-align: middle;" class="resize-min-width-half">Delivery/Completion/Acceptance(if applicable)</th>
                </tr>
            </thead>
            <tbody>
                <tr class="table-primary">
                    <th colspan="99">COMPLETED PROCUREMENT ACTIVITIES</th>
                </tr>
                @include('pmr.personal.edit.completed')
                <tr class="table-primary">
                    <th colspan="99">ONGOING PROCUREMENT ACTIVITIES</th>
                </tr>
                @include('pmr.personal.edit.ongoing')
            </tbody>
        </table>
    </div>
    <div class="d-none">
        <div class="row">
            <div class="col-xl-4 text-center">
                <p><strong>Prepared By:</strong></p>
            </div>
            <div class="col-xl-4 text-center">
                <p><strong>Recommended for Approval By:</strong></p>
            </div>
            <div class="col-xl-4 text-center">
                <p><strong>APPROVED:</strong></p>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-4 text-center">
                {{ Form::text('prepared_by', $pmr->prepared_by, ['class' => 'form-control form-control-sm', 'placeholder' => 'insert name']) }}
            </div>
            <div class="col-xl-4">
                {{ Form::text('recommended_approval', $pmr->recommended_approval, ['class' => 'form-control form-control-sm', 'placeholder' => 'insert name']) }}
            </div>
            <div class="col-xl-4">
                {{ Form::text('approved_by', $pmr->approved_by, ['class' => 'form-control form-control-sm', 'placeholder' => 'insert name']) }}
            </div>
        </div>
        <div class="row">
            <div class="col-xl-4 text-center">
                <p>BAC Secretariat</p>
            </div>
            <div class="col-xl-4 text-center">
                <p>BAC Chairperson</p>
            </div>
            <div class="col-xl-4 text-center">
                <p>Head of the Procuring Entity:</p>
            </div>
        </div>
    </div>
    <div class="form-group mt-3">
        <input type="submit" value="Save" class="btn btn-success">
    </div>
</div>
{{ Form::close() }}
<script src="{{ asset('js/pmr/edit.js') }}"></script>
@endsection