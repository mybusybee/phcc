<td>{{ $item->end_user }}</td>
<td>{{ $item->procurement_mode }}</td>
<td>{{ $item->pr->date_received === null ? 'Not yet received.' : $item->pr->date_received }}</td>
@if($item->pre_proc_conference !== null)
    <td>{{ Form::text('c_pre_proc_conference[]', $item->pre_proc_conference, ['class' => 'form-control form-control-sm', 'placeholder' => 'insert date', 'readonly']) }}</td>
@else
    {{ Form::hidden('c_pre_proc_conference[]', $item->pre_proc_conference) }}
    <td>N/A</td>
@endif
@if($item->ads_post_of_ib !== null)
    <td>{{ Form::text('c_ads_post_of_ib[]', $item->ads_post_of_ib, ['class' => 'form-control form-control-sm', 'placeholder' => 'insert date', 'readonly']) }}</td>
@else
    {{ Form::hidden('c_ads_post_of_ib[]', $item->ads_post_of_ib) }}
    <td>N/A</td>
@endif
@if($item->pre_bid_conf !== null)
    <td>{{ Form::text('c_pre_bid_conf[]', $item->pre_bid_conf, ['class' => 'form-control form-control-sm', 'placeholder' => 'insert date', 'readonly']) }}</td>
@else
    {{ Form::hidden('c_pre_bid_conf[]', $item->pre_bid_conf) }}
    <td>N/A</td>
@endif
@if($item->eligibility_check !== null)
    <td>{{ Form::text('c_eligibility_check[]', $item->eligibility_check, ['class' => 'form-control form-control-sm', 'placeholder' => 'insert date', 'readonly']) }}</td>
@else
    {{ Form::hidden('c_eligibility_check[]', $item->eligibility_check) }}
    <td>N/A</td>
@endif
@if($item->sub_open_of_bids !== null)
    <td>{{ Form::text('c_sub_open_of_bids[]', $item->sub_open_of_bids, ['class' => 'form-control form-control-sm', 'placeholder' => 'insert date', 'readonly']) }}</td>
@else
    {{ Form::hidden('c_sub_open_of_bids[]', $item->sub_open_of_bids) }}
    <td>N/A</td>
@endif
@if($item->bid_evaluation !== null)
    <td>{{ Form::text('c_bid_evaluation[]', $item->bid_evaluation, ['class' => 'form-control form-control-sm', 'placeholder' => 'insert date', 'readonly']) }}</td>
@else
    {{ Form::hidden('c_bid_evaluation[]', $item->bid_evaluation) }}
    <td>N/A</td>
@endif
@if($item->post_qual !== null)
    <td>{{ Form::text('c_post_qual[]', $item->post_qual, ['class' => 'form-control form-control-sm', 'placeholder' => 'insert date', 'readonly']) }}</td>
@else
    {{ Form::hidden('c_post_qual[]', $item->post_qual) }}
    <td>N/A</td>
@endif
@if($item->noa !== null)
    <td>{{ Form::text('c_noa[]', $item->noa, ['class' => 'form-control form-control-sm', 'placeholder' => 'insert date', 'readonly']) }}</td>
@else
    {{ Form::hidden('c_noa[]', $item->noa) }}
    <td>N/A</td>
@endif
@if($item->contract_signing !== null)
    <td>{{ Form::text('c_contract_signing[]', $item->contract_signing, ['class' => 'form-control form-control-sm', 'placeholder' => 'insert date', 'readonly']) }}</td>
@else
    {{ Form::hidden('c_contract_signing[]', $item->contract_signing) }}
    <td>N/A</td>
@endif
@if($item->ntp !== null)
    <td>{{ Form::text('c_ntp[]', $item->ntp, ['class' => 'form-control form-control-sm', 'placeholder' => 'insert date', 'readonly']) }}</td>
@else
    {{ Form::hidden('c_ntp[]', $item->ntp) }}
    <td>N/A</td>
@endif
@if($item->delivery_completion !== null)
    <td>{{ Form::text('c_delivery_completion[]', $item->delivery_completion, ['class' => 'form-control form-control-sm', 'placeholder' => 'insert date', 'readonly']) }}</td>
@else
    {{ Form::hidden('c_delivery_completion[]', $item->delivery_completion) }}
    <td>N/A</td>
@endif
@if($item->inspection_acceptance !== null)
    <td>{{ Form::text('c_inspection_acceptance[]', $item->inspection_acceptance, ['class' => 'form-control form-control-sm', 'placeholder' => 'insert date', 'readonly']) }}</td>
@else
    {{ Form::hidden('c_inspection_acceptance[]', $item->inspection_acceptance) }}
    <td>N/A</td>
@endif
<td>{{ $item->source_of_funds }}</td>
<td>{{ $item->pr->pr_item_total->total_estimate }}</td>
@if($item->abc_mooe !== null)
<td>{{ Form::text('c_mooe[]', $item->abc_mooe, ['class' => 'form-control form-control-sm', 'placeholder' => 'insert amount', 'readonly']) }}</td>
@else
{{ Form::hidden('c_mooe[]', $item->abc_mooe) }}
<td>N/A</td>
@endif
@if($item->abc_co !== null)
<td>{{ Form::text('c_co[]', $item->abc_co, ['class' => 'form-control form-control-sm', 'placeholder' => 'insert amount', 'readonly']) }}</td>
@else
{{ Form::hidden('c_co[]', $item->abc_co) }}
<td>N/A</td>
@endif
<td>{{ $item->total_contract_cost }}</td>
@if($item->contract_cost_mooe !== null)
<td>{{ Form::text('c_contract_cost_mooe[]', $item->contract_cost_mooe, ['class' => 'form-control form-control-sm', 'placeholder' => 'insert amount', 'readonly']) }}</td>
@else
{{ Form::hidden('c_contract_cost_mooe[]', $item->contract_cost_mooe) }}
<td>N/A</td>
@endif
@if($item->contract_cost_co !== null)
<td>{{ Form::text('c_contract_cost_co[]', $item->contract_cost_co, ['class' => 'form-control form-control-sm', 'placeholder' => 'insert amount', 'readonly']) }}</td>
@else
{{ Form::hidden('c_contract_cost_co[]', $item->contract_cost_co) }}
<td>N/A</td>
@endif
@if($item->list_of_invited_observers !== null)
<td>{{ Form::text('c_list_of_invited_observers[]', $item->list_of_invited_observers, ['class' => 'form-control form-control-sm', 'readonly']) }}</td>
@else
{{ Form::hidden('c_list_of_invited_observers[]') }}
<td>N/A</td>
@endif
@if($item->receipt_pre_bid_conf !== null)
<td>{{ Form::text('c_receipt_pre_bid_conf[]', $item->receipt_pre_bid_conf, ['class' => 'form-control form-control-sm', 'placeholder' => 'insert date', 'readonly']) }}</td>
@else
{{ Form::hidden('c_receipt_pre_bid_conf[]', $item->receipt_pre_bid_conf) }}
<td>N/A</td>
@endif
@if($item->receipt_eligibility_check !== null)
<td>{{ Form::text('c_receipt_eligibility_check[]', $item->receipt_eligibility_check, ['class' => 'form-control form-control-sm', 'placeholder' => 'insert date', 'readonly']) }}</td>
@else
{{ Form::hidden('c_receipt_eligibility_check[]', $item->receipt_eligibility_check) }}
<td>N/A</td>
@endif
@if($item->receipt_sub_open_of_bids !== null)
<td>{{ Form::text('c_receipt_sub_open_of_bids[]', $item->receipt_sub_open_of_bids, ['class' => 'form-control form-control-sm', 'placeholder' => 'insert date', 'readonly']) }}</td>
@else
{{ Form::hidden('c_receipt_sub_open_of_bids[]', $item->receipt_sub_open_of_bids) }}
<td>N/A</td>
@endif
@if($item->receipt_bid_evaluation !== null)
<td>{{ Form::text('c_receipt_bid_evaluation[]', $item->receipt_bid_evaluation, ['class' => 'form-control form-control-sm', 'placeholder' => 'insert date', 'readonly']) }}</td>
@else
{{ Form::hidden('c_receipt_bid_evaluation[]', $item->receipt_bid_evaluation) }}
<td>N/A</td>
@endif
@if($item->receipt_post_qual !== null)
<td>{{ Form::text('c_receipt_post_qual[]', $item->receipt_post_qual, ['class' => 'form-control form-control-sm', 'placeholder' => 'insert date', 'readonly']) }}</td>
@else
{{ Form::hidden('c_receipt_post_qual[]', $item->receipt_post_qual) }}
<td>N/A</td>
@endif
@if($item->receipt_delivery_completion !== null)
<td>{{ Form::text('c_receipt_delivery_completion[]', $item->receipt_delivery_completion, ['class' => 'form-control form-control-sm', 'placeholder' => 'insert date', 'readonly']) }}</td>
@else
{{ Form::hidden('c_receipt_delivery_completion[]', $item->receipt_delivery_completion) }}
<td>N/A</td>
@endif
@if($item->remarks !== null)
<td>{{ Form::text('c_remarks[]', $item->remarks, ['class' => 'form-control form-control-sm', 'placeholder' => 'insert remarks', 'readonly']) }}</td>
@else
{{ Form::hidden('c_remarks[]', $item->remarks) }}
<td>N/A</td>
@endif
<td>
    {{ Form::select('c_status[]', ["" => "Please select one...", "For Pre-procurement" => "For Pre-procurement", "For Pre-bidding Conference" => "For Pre-bidding Conference", "For Posting" => "For Posting", "For TWG Review" => "For TWG Review", "For Farm Out / Canvass" => "For Farm Out / Canvass", "For Award" => "For Award", "For Post Qual" => "For Post Qual", "For Contract Signing" => "For Contract Signing", "For Delivery" => "For Delivery", "For Contract Implementation" => "For Contract Implementation", "For Inspection & Acceptance" => "For Inspection & Acceptance", "For Payment" => "For Payment", "Cancelled" => "Cancelled", "Failed" => "Failed"], $item->status, ['class' => 'form-control form-control-sm']) }}
</td>