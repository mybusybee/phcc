@php
    $row_ctr = 0;
@endphp
@foreach($pmr->pmr_items->where('completed', 0) as $item)
    <tr id="{{'ongoing-row-'.$row_ctr}}">
        <td class="d-none"><input type="hidden" name="ongoing[]" value="{{ $item->id }}"></td>
        <td>{{ $item->pr->pr_no }}</td>
        <td>{{ $item->code }}</td>
        <td>
            <ol>
                @foreach($item->program_projects as $pp)
                    <li>{{ $pp }}</li>
                @endforeach
            </ol>
        </td>
        <td>{{ $item->end_user }}</td>
        <td>{{ $item->procurement_mode }}</td>
        <td>{{ $item->pr->date_received === null ? 'Not yet received.' : $item->pr->date_received }}</td>
        <td>{{ Form::text('o_pre_proc_conference[]', $item->pre_proc_conference, ['class' => 'form-control form-control-sm date', 'placeholder' => 'Insert date']) }}</td>
        <td>{{ Form::text('o_ads_post_of_ib[]', $item->ads_post_of_ib, ['class' => 'form-control form-control-sm date', 'placeholder' => 'Insert date']) }}</td>
        <td>{{ Form::text('o_pre_bid_conf[]', $item->pre_bid_conf, ['class' => 'form-control form-control-sm date', 'placeholder' => 'Insert date']) }}</td>
        <td>{{ Form::text('o_eligibility_check[]', $item->eligibility_check, ['class' => 'form-control form-control-sm date', 'placeholder' => 'Insert date']) }}</td>
        <td>{{ Form::text('o_sub_open_of_bids[]', $item->sub_open_of_bids, ['class' => 'form-control form-control-sm date', 'placeholder' => 'Insert date']) }}</td>
        <td>{{ Form::text('o_bid_evaluation[]', $item->bid_evaluation, ['class' => 'form-control form-control-sm date', 'placeholder' => 'Insert date']) }}</td>
        <td>{{ Form::text('o_post_qual[]', $item->post_qual, ['class' => 'form-control form-control-sm date', 'placeholder' => 'Insert date']) }}</td>
        <td>{{ Form::text('o_noa[]', $item->noa, ['class' => 'form-control form-control-sm date', 'placeholder' => 'Insert date']) }}</td>
        <td>{{ Form::text('o_contract_signing[]', $item->contract_signing, ['class' => 'form-control form-control-sm date', 'placeholder' => 'Insert date']) }}</td>
        <td>{{ Form::text('o_ntp[]', $item->ntp, ['class' => 'form-control form-control-sm date', 'placeholder' => 'Insert date']) }}</td>
        <td>{{ Form::text('o_delivery_completion[]', $item->delivery_completion, ['class' => 'form-control form-control-sm date', 'placeholder' => 'Insert date']) }}</td>
        <td>{{ Form::text('o_inspection_acceptance[]', $item->inspection_acceptance, ['class' => 'form-control form-control-sm date', 'placeholder' => 'Insert date']) }}</td>
        <td>{{ $item->source_of_funds }}</td>
        <td id="{{'total_estimate_'.$row_ctr}}">{{ $item->pr->pr_item_total->total_estimate }}</td>
        <td>{{ Form::text('o_mooe[]', $item->abc_mooe, ['class' => 'pr-mooe form-control form-control-sm', 'placeholder' => 'insert amount', 'id' => 'pr-mooe-'.$row_ctr]) }}</td>
        <td>{{ Form::text('o_co[]', $item->abc_co, ['class' => 'pr-co form-control form-control-sm', 'placeholder' => 'insert amount', 'id' => 'pr-co-'.$row_ctr]) }}</td>
        <td id="{{'contract_cost_'.$row_ctr}}">{{ $item->total_contract_cost }}</td>
        @if ($item->total_contract_cost == null)
        <td>{{ Form::text('o_contract_cost_mooe[]', $item->contract_cost_mooe, ['class' => 'contract-mooe form-control form-control-sm', 'placeholder' => 'insert amount', 'id' => 'contract-mooe-'.$row_ctr, 'readonly']) }}</td>
        <td>{{ Form::text('o_contract_cost_co[]', $item->contract_cost_co, ['class' => 'contract-co form-control form-control-sm', 'placeholder' => 'insert amount', 'id' => 'contract-co-'.$row_ctr, 'readonly']) }}</td>
        @else
        <td>{{ Form::text('o_contract_cost_mooe[]', $item->contract_cost_mooe, ['class' => 'contract-mooe form-control form-control-sm', 'placeholder' => 'insert amount', 'id' => 'contract-mooe-'.$row_ctr]) }}</td>
        <td>{{ Form::text('o_contract_cost_co[]', $item->contract_cost_co, ['class' => 'contract-co form-control form-control-sm', 'placeholder' => 'insert amount', 'id' => 'contract-co-'.$row_ctr]) }}</td>
        @endif
        
        <td>{{ Form::text('o_list_of_invited_observers[]', $item->list_of_invited_observers, ['class' => 'form-control form-control-sm']) }}</td>
        <td>{{ Form::text('o_receipt_pre_bid_conf[]', $item->receipt_pre_bid_conf, ['class' => 'form-control form-control-sm date', 'placeholder' => 'insert date']) }}</td>
        <td>{{ Form::text('o_receipt_eligibility_check[]', $item->receipt_eligibility_check, ['class' => 'form-control form-control-sm date', 'placeholder' => 'insert date']) }}</td>
        <td>{{ Form::text('o_receipt_sub_open_of_bids[]', $item->receipt_sub_open_of_bids, ['class' => 'form-control form-control-sm date', 'placeholder' => 'insert date']) }}</td>
        <td>{{ Form::text('o_receipt_bid_evaluation[]', $item->receipt_bid_evaluation, ['class' => 'form-control form-control-sm date', 'placeholder' => 'insert date']) }}</td>
        <td>{{ Form::text('o_receipt_post_qual[]', $item->receipt_post_qual, ['class' => 'form-control form-control-sm date', 'placeholder' => 'insert date']) }}</td>
        <td>{{ Form::text('o_receipt_delivery_completion[]', $item->receipt_delivery_completion, ['class' => 'form-control form-control-sm date', 'placeholder' => 'insert date']) }}</td>
        <td>{{ Form::text('o_remarks[]', $item->remarks, ['class' => 'form-control form-control-sm', 'placeholder insert remarks']) }}</td>
        <td>
            {{ Form::select('o_status[]', ['' => 'Please select one...', 'For Pre-procurement' => 'For Pre-procurement','For Pre-bidding Conference' => 'For Pre-bidding Conference','For Posting' => 'For Posting','For TWG Review' => 'For TWG Review','For Farm Out / Canvass' => 'For Farm Out / Canvass','For Award' => 'For Award','For Post Qual' => 'For Post Qual','For Contract Signing' => 'For Contract Signing','For Delivery' => 'For Delivery','For Contract Implementation' => 'For Contract Implementation','For Inspection & Acceptance' => 'For Inspection & Acceptance','For Payment' => 'For Payment','Cancelled' => 'Cancelled','Failed' => 'Failed'], $item->status, ['class' => 'form-control form-control-sm']) }}
        </td>
    </tr>
    @php
        $row_ctr++;
    @endphp
@endforeach
<tr>
    <th colspan="19" class="text-right table-primary">Total Allotted Budget of On-going Procurement Activities</th>
    <th colspan="6">{{ number_format($pmr->total_ongoing_abc, 2, '.', ',') }}</th>
</tr>