<td>{{ $item->end_user }}</td>
<td>{{ $item->procurement_mode }}</td>
<td>{{ $item->pr->date_received === null ? 'Not yet received.' : $item->pr->date_received }}</td>
<td>{{ Form::text('c_pre_proc_conference[]', $item->pre_proc_conference, ['class' => 'form-control form-control-sm date', 'placeholder' => 'insert date']) }}</td>
<td>{{ Form::text('c_ads_post_of_ib[]', $item->ads_post_of_ib, ['class' => 'form-control form-control-sm date', 'placeholder' => 'insert date']) }}</td>
<td>{{ Form::text('c_pre_bid_conf[]', $item->pre_bid_conf, ['class' => 'form-control form-control-sm date', 'placeholder' => 'insert date']) }}</td>
<td>{{ Form::text('c_eligibility_check[]', $item->eligibility_check, ['class' => 'form-control form-control-sm date', 'placeholder' => 'insert date']) }}</td>
<td>{{ Form::text('c_sub_open_of_bids[]', $item->sub_open_of_bids, ['class' => 'form-control form-control-sm date', 'placeholder' => 'insert date']) }}</td>
<td>{{ Form::text('c_bid_evaluation[]', $item->bid_evaluation, ['class' => 'form-control form-control-sm date', 'placeholder' => 'insert date']) }}</td>
<td>{{ Form::text('c_post_qual[]', $item->post_qual, ['class' => 'form-control form-control-sm date', 'placeholder' => 'insert date']) }}</td>
<td>{{ Form::text('c_noa[]', $item->noa, ['class' => 'form-control form-control-sm date', 'placeholder' => 'insert date']) }}</td>
<td>{{ Form::text('c_contract_signing[]', $item->contract_signing, ['class' => 'form-control form-control-sm date', 'placeholder' => 'insert date']) }}</td>
<td>{{ Form::text('c_ntp[]', $item->ntp, ['class' => 'form-control form-control-sm date', 'placeholder' => 'insert date']) }}</td>
<td>{{ Form::text('c_delivery_completion[]', $item->delivery_completion, ['class' => 'form-control form-control-sm date', 'placeholder' => 'insert date']) }}</td>
<td>{{ Form::text('c_inspection_acceptance[]', $item->inspection_acceptance, ['class' => 'form-control form-control-sm date', 'placeholder' => 'insert date']) }}</td>
<td>{{ $item->source_of_funds }}</td>
<td id="{{'completed_total_estimate_'.$row_ctr}}">{{ $item->pr->pr_item_total->total_estimate }}</td>
<td>{{ Form::text('c_mooe[]', $item->abc_mooe, ['class' => 'pr-com-mooe form-control form-control-sm', 'placeholder' => 'insert amount', 'id' => 'pr-com-mooe-'.$row_ctr]) }}</td>
<td>{{ Form::text('c_co[]', $item->abc_co, ['class' => 'pr-com-co form-control form-control-sm', 'placeholder' => 'insert amount', 'id' => 'pr-com-co-'.$row_ctr]) }}</td>
<td id="{{'completed_contract_cost_'.$row_ctr}}">{{ $item->total_contract_cost }}</td>
@if ($item->total_contract_cost == null)
<td>{{ Form::text('c_contract_cost_mooe[]', $item->contract_cost_mooe, ['class' => 'form-control form-control-sm', 'placeholder' => 'insert amount', 'readonly']) }}</td>
<td>{{ Form::text('c_contract_cost_co[]', $item->contract_cost_co, ['class' => 'form-control form-control-sm', 'placeholder' => 'insert amount', 'readonly']) }}</td>
@else
<td>{{ Form::text('c_contract_cost_mooe[]', $item->contract_cost_mooe, ['class' => 'com-contract-mooe form-control form-control-sm', 'placeholder' => 'insert amount', 'id' => 'com-contract-mooe-'.$row_ctr]) }}</td>
<td>{{ Form::text('c_contract_cost_co[]', $item->contract_cost_co, ['class' => 'com-contract-co form-control form-control-sm', 'placeholder' => 'insert amount', 'id' => 'com-contract-co-'.$row_ctr]) }}</td>
@endif
<td>{{ Form::text('c_list_of_invited_observers[]', $item->list_of_invited_observers, ['class' => 'form-control form-control-sm']) }}</td>
<td>{{ Form::text('c_receipt_pre_bid_conf[]', $item->receipt_pre_bid_conf, ['class' => 'form-control form-control-sm date', 'placeholder' => 'insert date']) }}</td>
<td>{{ Form::text('c_receipt_eligibility_check[]', $item->receipt_eligibility_check, ['class' => 'form-control form-control-sm date', 'placeholder' => 'insert date']) }}</td>
<td>{{ Form::text('c_receipt_sub_open_of_bids[]', $item->receipt_sub_open_of_bids, ['class' => 'form-control form-control-sm date', 'placeholder' => 'insert date']) }}</td>
<td>{{ Form::text('c_receipt_bid_evaluation[]', $item->receipt_bid_evaluation, ['class' => 'form-control form-control-sm date', 'placeholder' => 'insert date']) }}</td>
<td>{{ Form::text('c_receipt_post_qual[]', $item->receipt_post_qual, ['class' => 'form-control form-control-sm date', 'placeholder' => 'insert date']) }}</td>
<td>{{ Form::text('c_receipt_delivery_completion[]', $item->receipt_delivery_completion, ['class' => 'form-control form-control-sm date', 'placeholder' => 'insert date']) }}</td>
<td>{{ Form::text('c_remarks[]', $item->remarks, ['class' => 'form-control form-control-sm', 'placeholder' => 'insert remarks']) }}</td>
<td>
    {{ Form::select('c_status[]', ["" => "Please select one...", "For Pre-procurement" => "For Pre-procurement", "For Pre-bidding Conference" => "For Pre-bidding Conference", "For Posting" => "For Posting", "For TWG Review" => "For TWG Review", "For Farm Out / Canvass" => "For Farm Out / Canvass", "For Award" => "For Award", "For Post Qual" => "For Post Qual", "For Contract Signing" => "For Contract Signing", "For Delivery" => "For Delivery", "For Contract Implementation" => "For Contract Implementation", "For Inspection & Acceptance" => "For Inspection & Acceptance", "For Payment" => "For Payment", "Cancelled" => "Cancelled", "Failed" => "Failed"], $item->status, ['class' => 'form-control form-control-sm']) }}
</td>