@php
    $row_ctr = 0;
@endphp
@foreach($pmr->pmr_items->where('completed', 1) as $item)
    <tr id="{{'completed-row-'.$row_ctr}}">
        <td class="d-none"><input type="hidden" name="completed[]" value="{{ $item->id }}"></td>
        <td>{{ $item->pr->pr_no }}</td>
        <td>{{ $item->code }}</td>
        <td>
            <ol>
                @foreach($item->program_projects as $pp)
                    <li>{{ $pp }}</li>
                @endforeach
            </ol>
        </td>
        @if($item->status === 'Cancelled')
            @include('pmr.personal.edit.completed-N-A')
        @else
            @include('pmr.personal.edit.completed-not-n-a')
        @endif
    </tr>
@endforeach
<tr>
    <th colspan="19" class="text-right table-primary">Total Allotted Budget of Procurement Activities</th>
    <th colspan="6">{{ number_format($pmr->total_completed_alloted_budget, 2, '.', ',') }}</th>
</tr>
<tr>
    <th colspan="19" class="text-right table-primary">Total Contract Price of Procurement Activities Conducted</th>
    <th colspan="6">{{ number_format($pmr->total_completed_contract_cost, 2, '.', ',') }}</th>
</tr>
<tr>
    <th colspan="19" class="text-right table-primary">Total Savings (Total Alloted Budget - Total Contract Price</th>
    <th colspan="6">{{ number_format($pmr->total_completed_alloted_budget - $pmr->total_completed_contract_cost, 2, '.', ',') }}</th>
</tr>