@extends('layouts.base')

@section('content')
    <div class="container-fluid frm">
        <h1>Request For Quotations List</h1>
        <div class="table-responsive">
            <table class="table table-hover table-striped text-center">
                <thead class="table-primary">
                    <tr>
                        <th>PR No.</th>
                        <th>PR Date</th>
                        <th>RFQ No.</th>
                        <th>RFQ Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rfqs as $rfq)
                        <tr>
                            <td>{{ $rfq->pr->pr_no }}</td>
                            <td>{{ $rfq->pr->pr_prep_date }}</td>
                            <td>{{ $rfq->rfq_no }}</td>
                            <td>{{ $rfq->rfq_prep_date }}</td>
                            <td>
                                <a href="{{ url('/rfq/'.$rfq->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                                @if(!$rfq->aoq)
                                    <a href="{{ url('/rfq/'.$rfq->id.'/edit') }}" class="btn btn-info"><i class="fas fa-pencil-alt"></i></a>
                                    <a href="{{ url('/aoq/create/rfq/'.$rfq->id) }}" class="btn btn-warning">Generate AOQ</a>
                                @else
                                    <button class="btn btn-danger" disabled><i class="fas fa-pencil-alt"></i></button>
                                    <button class="btn btn-warning" disabled>Generate AOQ</button>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection