@extends('layouts.base')

@section('content')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.16/cr-1.4.1/datatables.min.css"/>
<link rel="stylesheet" href="{{ asset('css/jquery.datetimepicker.css') }}">
@if ($rfq->invite->status == 'FOR_REVISION')
<div class="alert alert-info">
    {{$rfq->invite->rejects_remarks}}
</div>
@endif
    {{ Form::open(['url' => url('rfq/'.$rfq->id.'/update'), 'method' => 'put']) }}
    <div class="container-fluid frm">
        <div class="row">
            <div class="col-xl-12 text-center">
                <h2>REQUEST FOR QUOTATION</h2>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-xl-6">
                <strong>{{ Form::label('prnodate', 'PR No. / Date: ') }}</strong>
                <div class="row">
                    <div class="col-xl-4">
                        {{ Form::text('pr_no', $rfq->pr->pr_no, ['class' => 'form-control form-control-sm', 'readonly']) }}
                    </div>
                    <div class="col-xl-4">
                        {{ Form::text('pr_prep_date', $rfq->pr->pr_prep_date, ['class' => 'form-control form-control-sm', 'readonly']) }}
                    </div>
                </div>
            </div>
            <div class="col-xl-6">
                <strong>{{ Form::label('rfqnodate', 'RFQ No. / Date: ') }}</strong>
                <div class="row">
                    <div class="col-xl-4">
                        {{ Form::text('rfq_no', $rfq->invite->form_no, ['class' => 'form-control form-control-sm', 'readonly']) }}
                    </div>
                    <div class="col-xl-4">
                        {{ Form::text('rfq_prep_date', date('m/d/Y'), ['class' => 'form-control form-control-sm', 'readonly']) }}
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-xl-12">
                <strong><p>Sir / Madam:</p></strong>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12">
                <span>Please quote your lowest price/s for the item/s listed below including the total amount in legible style (preferably typewritten) and return this duly signed by the company's authorized signatory/ies {{ Form::select('submission_type', ['' => 'Please select one...', 'signed and sealed envelope' => 'signed and sealed envelope', 'emailed quotation' => 'emailed quotation'], $rfq->submission_type, ['class' => 'form-control form-control-sm border border-info d-inline-block w-25', 'required']) }} to the General Services Division, Administrative Office c/o {{ Form::text('rfq_receiver', $rfq->rfq_receiver, ['class' => 'form-control form-control-sm border border-info d-inline-block w-25', 'placeholder' => 'insert name']) }}, 25/F of the above address or e-mail to <i>procurement@phcc.gov.ph</i> on or before {{ Form::text('submission_date_time', $rfq->submission_date_time, ['class' => 'form-control form-control-sm border border-info d-inline-block w-25', 'id' => 'submission_date_time', 'required', 'placeholder' => 'insert date and time']) }}</span>
            </div>
        </div>
        <hr>
        <div class="form-group">
            <div class="col-xl-12">
                <p>Your participation to this bidding shall be subjected to the requirements as identified below:</p>
            </div>
            <div class="col-xl-12">
                <ul class="rfq-list list-group">
                    <li class="list-group-item"><label for="philgeps"><input type="checkbox" name="philgeps" id="philgeps" {{ $rfq->invite->requirement->philgeps_reg_no == 1 ? 'checked' : null }}>PhilGEPS Registration Number</label></li>
                    <li class="list-group-item"><label for="permit"><input type="checkbox" name="business_permit" id="permit" {{ $rfq->invite->requirement->mayors_business_permit_bir_cor == 1 ? 'checked' : null }}>Mayor's Business Permit / BIR Certificate of Registration in case of individual</label></li>
                    <li class="list-group-item"><label for="latest_income"><input type="checkbox" name="latest_income" id="latest_income" {{ $rfq->invite->requirement->income_business_tax_return == 1 ? 'checked' : null }} >Latest Income / Business Tax Return</label></li>
                    <li class="list-group-item"><label for="license_cv"><input type="checkbox" name="cert_of_exclusivity" id="license_cv" {{ $rfq->invite->requirement->certificate_of_exclusivity == 1 ? 'checked' : null }} >Certificate of Exclusivity</label></li>
                    <li class="list-group-item"><label for="omnibus"><input type="checkbox" name="omnibus" id="omnibus" {{ $rfq->invite->requirement->omnibus_sworn_statement == 1 ? 'checked' : null }} >Omnibus Sworn Statement</label></li>
                    <li class="list-group-item"><label for="tor"><input type="checkbox" name="tor" id="tor" {{ $rfq->invite->requirement->signed_terms_of_reference == 1 ? 'checked' : null }} >Signed Terms of Reference</label></li>
                    <li class="list-group-item"><label for="tax_inclusive"><input type="checkbox" name="tax_inclusive" id="tax_inclusive" {{ $rfq->invite->requirement->govt_tax_inclusive == 1 ? 'checked' : null }} >Proposal must be inclusive of all applicable government taxes and subject to 5% R-VAT and 1% (PO) or 2% (JO) deductions.</label></li>
                    <li class="list-group-item"><label for="used_form"><input type="checkbox" name="used_form" id="used_form" {{ $rfq->invite->requirement->used_form == 1 ? 'checked' : null }} >Bids must be submitted using this form. Supplemental information using your company stationery may be attached to reflect the complete specification of bid e.g. brand name, model, pictures / brochures / literature, menu, etc.</label></li>
                    <li class="list-group-item"><label for="not_exceeding_abc"><input type="checkbox" name="not_exceeding_abc" id="not_exceeding_abc" {{ $rfq->invite->requirement->not_exceeding_abc == 1 ? 'checked' : null }} >Bids should not exceed the Approved Budget for the Contract (ABC) in the total amount of </label> {{ Form::text('abc_amount', $rfq->invite->requirement->abc_amount, ['class' => 'form-control form-control-sm d-inline-block w-25', 'readonly']) }}</li>
                    <li class="list-group-item"><label for="by_lot"><input type="checkbox" name="by_lot" id="by_lot" {{ $rfq->invite->requirement->award_by_lot == 1 ? 'checked' : null }} >Award shall be made by lot(please bid for all items to avoid disqualification of bid) or</label></li>
                    <li class="list-group-item"><label for="by_line_item"><input type="checkbox" name="by_line_item" id="by_line_item" {{ $rfq->invite->requirement->award_by_line_item == 1 ? 'checked' : null }} >Award shall be made by line item</label></li>
                    <li class="list-group-item"><label for="validity"><input type="checkbox" name="validity" id="validity" {{ $rfq->invite->requirement->one_month_validity == 1 ? 'checked' : null }} >Bids should be valid for a <strong>minimum of one (1) month</strong> from deadline of submission of bids as indicated above.</label></li>
                    <li class="list-group-item"><label for="delivery"><input type="checkbox" name="delivery" id="delivery" {{ $rfq->invite->requirement->delivered_to_pcc == 1 ? 'checked' : null }} required>Procured items shall be delivered to <input type="text" class="form-control form-control-sm" required name="delivery_place" value="{{ $rfq->invite->requirement->delivery_place }}"></label></li>
                    <li class="list-group-item"><label for="paymentTerms"><input type="checkbox" name="payment_terms_cb" id="paymentTerms" {{ $rfq->invite->requirement->payment_terms_cb == 1 ? 'checked' : null }} required>Payment Terms:</label> {{ Form::text('payment_terms', $rfq->invite->requirement->payment_terms, ['class' => 'form-control form-control-sm w-25 d-inline-block'], 'required') }}</li>
                    <li class="list-group-item"><label for="refusal"><input type="checkbox" name="refusal" id="refusal" {{ $rfq->invite->requirement->signatory_refusal == 1 ? 'checked' : null }} >Refusal to sign and accept an Award / Purchase Order / Job Order or enter into contract without justifiable reason, maybe ground for imposition of administrative sanctions under Rule XXIII of the Revised IRR of RA 9184.</label></li>
                    <li class="list-group-item"><label for="others"><strong>Others:</strong></label> {{ Form::text('others', $rfq->invite->requirement->others, ['class' => 'form-control form-control-sm']) }}</li>
                </ul>
            </div>
            <hr>
            <div class="col-xl-12">
                <p>In case you do not receive any communication from PCC {{ Form::text('not_awarded', $rfq->not_awarded_notification, ['class' => 'form-control form-control-sm w-25 d-inline-block']) }} days / months from the deadline indicated above, it will mean that the award was not made in your favor. With the end in view of obtaining the contract most advantageous to the government, PCC reserves the right to: 1.) accept or reject any or all the quotations / bids; 2.) award the contract on a per item / lot basis; and 3.) to annul the bidding process and to reject all quotations / bids at any time prior to contract award, without thereby incurring any liability to the affected bidder or bidders. Tie bids shall be resolved by draw lots.</p>
                <p>For clarifications, please contact PCC-AI-GSD - c/o Mr. Jeson Q. de la Torre, in the above address / telephone numbers.</p>
            </div>
            <hr>
        </div>
        <div class="form-group">
            <div class="col-xl-12">
                <p>Very truly yours,</p>
            </div>
            <div class="col-xl-4 mt-5">
                {{ Form::text('rfq_sender', $rfq->rfq_sender, ['class' => 'form-control form-control-sm border border-info', 'required', 'placeholder' => 'insert name']) }}
            </div>
            <div class="col-xl-4">
                {{ Form::text('rfq_sender_designation', $rfq->rfq_sender_designation, ['class' => 'form-control form-control-sm border border-info', 'placeholder' => 'insert designation']) }}
            </div>
        </div>
        <hr>
        <div class="table-responsive text-center">
            <table class="table table-hover table-striped">
                <thead class="table-primary">
                    <tr>
                        <th colspan="4"></th>
                        <th>BRAND / MODEL / OTHERS</th>
                        <th>UNIT PRICE</th>
                        <th></th>
                    </tr>
                    <tr>
                        <th>Item No.</th>
                        <th>Unit</th>
                        <th>QTY</th>
                        <th>Item Description</th>
                        <th colspan="2">(To be filled up by the supplier)</th>
                        <th>Total (PHP)</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rfq->pr->pr_items as $item)
                        <tr>
                            <td>{{ $item->item_no }}</td>
                            <td>{{ $item->unit }}</td>
                            <td>{{ $item->quantity }}</td>
                            <td>{{ $item->description }}</td>
                            <td></td>
                            <td></td>
                            <td>{{ count($item->pr_sub_items) ? '' : $item->est_cost_total }}</td>
                        </tr>
                        @foreach($item->pr_sub_items as $sub)
                            <tr>
                                <td>{{ $sub->item_no }}</td>
                                <td>{{ $sub->unit }}</td>
                                <td>{{ $sub->quantity }}</td>
                                <td>{{ $sub->description }}</td>
                                <td></td>
                                <td></td>
                                <td>{{ $sub->est_cost_total }}</td>
                            </tr>
                        @endforeach
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="5"></td>
                        <td class="text-center"><strong>TOTAL:</strong></td>
                        <td>{{ $rfq->pr->pr_item_total->total_estimate }}</td>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div class="row">
            <div class="col-xl-12 form-group">
                <label for="">Notes:</label>
                {{ Form::textarea('notes', $rfq->notes, ['class' => 'form-control', 'maxlength' => 3000]) }}
            </div>
        </div>
        {{ Form::submit('Update', ['class' => 'btn btn-success']) }}
    </div>
    {{ Form::close() }}
    <script src="{{ asset('js/rfq/edit.js') }}"></script>
@endsection