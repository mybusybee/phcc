<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('css/rfq-pdf.css') }}">
    <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="w-50">
                <img src="{{ asset('img/pcc-logo-pdf.png') }}" height="80">
            </div>
            <div class="w-50 text-right">
                <img src="{{ asset('img/pcc-contact-pdf.png') }}" height="80">
            </div>
        </div>
        <hr>
        <div class="row text-center">
            <h2>REQUEST FOR QUOTATION / PROPOSAL</h2>
        </div>
        <hr>
        <div class="row">
            <div class="w-50">
                <strong><span>PR No. / Date:</span></strong>
                <p>{{ $rfq->pr->pr_no . ' - ' .  $rfq->pr->pr_prep_date }}</p>
            </div>
            <div class="w-50 text-right">
                <span><strong>RFQ No. / Date:</strong></span>
                <p>{{ $rfq->rfq_no . ' - ' . $rfq->rfq_prep_date }}</p>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="w-100">
                <span><strong>Sir/Madam:</strong></span>
            </div>
        </div>
        <div class="row">
            <div class="w-100 text-justify">
                <p>Please quote your lowest price/s for the item/s listed below including the total amount in legible style (preferably typewritten) and return this duly signed by the company's authorized signatories to the General Services Division, Administrative Office c/o Mr. Jeson Q. de la Torre, 2/F of the above address or e-mail to <i>procurement@phcc.gov.ph</i> on or before <span style="text-decoration:underline; font-weight: bold;"> {{ $rfq->submission_date_time }}</span></p>
            </div>
        </div>
        <hr>
        <div class="form-group">
            <div class="col-xs-12">
                <p>Your participation to this bidding shall be subjected to the requirements as identified below:</p>
            </div>
            <div class="col-xs-12">
                <ul class="rfq-list list-group">
                    <li class="list-group-item">
                        @if ($rfq->rfq_req->philgeps_reg_no == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        PhilGEPS Registration Number
                    </li>
                    <li class="list-group-item">
                        @if ($rfq->rfq_req->mayors_business_permit_bir_cor == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        Mayor's Business Permit / BIR Certificate of Registration in case of individual
                    </li>
                    <li class="list-group-item">
                        @if ($rfq->rfq_req->income_business_tax_return == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        Latest Income / Business Tax Return
                    </li>
                    <li class="list-group-item">
                        @if ($rfq->rfq_req->professional_license_cv == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        Professional License / Curriculum Vitae
                    </li>
                    <li class="list-group-item">
                        @if ($rfq->rfq_req->omnibus_sworn_statement == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        Omnibus Sworn Statement
                    </li>
                    <li class="list-group-item">
                        @if ($rfq->rfq_req->signed_terms_of_reference == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        Signed Terms of Reference
                    </li>
                    <li class="list-group-item">
                        @if ($rfq->rfq_req->govt_tax_inclusive == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        Proposal must be inclusive of all applicable government taxes and subject to 5% R-VAT and 1% (PO) or 2% (JO) deductions.
                    </li>
                    <li class="list-group-item">
                        @if ($rfq->rfq_req->used_form == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        Bids must be submitted using this form. Supplemental information using your company stationery may be attached to reflect the complete specification of bid e.g. brand name, model, pictures / brochures / literature, menu, etc.
                    </li>
                    <li class="list-group-item">
                        @if ($rfq->rfq_req->not_exceeding_abc == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        Bids should not exceed the Approved Budget for the Contract (ABC) in the total amount of <span style="text-decoration: underline; font-weight: bold;">{{ $rfq->rfq_req->abc_amount }}</span>
                    </li>
                    <li class="list-group-item">
                        @if ($rfq->rfq_req->award_by_lot == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        Award shall be made by lot(please bid for all items to avoid disqualification of bid) or
                    </li>
                    <li class="list-group-item">
                        @if ($rfq->rfq_req->award_by_line_item == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        Award shall be made by line item
                    </li>
                    <li class="list-group-item">
                        @if ($rfq->rfq_req->one_month_validity == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        Bids should be valid for a <strong>minimum of one (1) month</strong> from deadline of submission of bids as indicated above.
                    </li>
                    <li class="list-group-item">
                        @if ($rfq->rfq_req->delivered_to_pcc == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        Procured items shall be delivered to PCC Office.
                    </li>
                    <li class="list-group-item">
                        <i class="fa fa-check-circle" style="color: green;"></i>
                        Payment Terms: <span style="text-decoration: underline; font-weight: bold;">{{ $rfq->rfq_req->payment_terms }}</span>
                    </li>
                    <li class="list-group-item">
                        @if ($rfq->rfq_req->signatory_refusal == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        Refusal to sign and accept an Award / Purchase Order / Job Order or enter into contract without justifiable reason, maybe ground for imposition of administrative sanctions under Rule XXIII of the Revised IRR of RA 9184.
                    </li>
                    <li class="list-group-item">
                        <label for="others"><strong>Others:</strong></label>
                        <u>{{ $rfq->rfq_req->others }}</u>
                    </li>
                </ul>
            </div>
            <hr>
            <div class="w-100 text-justify" style="margin-top: 30%">
                <p>In case you do not receive any communication from PCC <span style="text-decoration: underline; font-weight: bold;">{{ $rfq->not_awarded_notification }}</span> days / months from the deadline indicated above, it will mean that the award was not made in your favor. With the end in view of obtaining the contract most advantageous to the government, PCC reserves the right to: 1.) accept or reject any or all the quotations / bids; 2.) award the contract on a per item / lot basis; and 3.) to annul the bidding process and to reject all quotations / bids at any time prior to contract award, without thereby incurring any liability to the affected bidder or bidders. Tie bids shall be resolved by draw lots.</p>
                <p>For clarifications, please contact PCC-AI-GSD - c/o Mr. Jeson Q. de la Torre, in the above address / telephone numbers.</p>
            </div>
            <hr>
        </div>
        <div class="form-group">
            <div class="col-xs-12">
                <p>Very truly yours,</p>
            </div>
            <div class="col-xs-12 mt-5">
                <p><strong>JESON Q. DE LA TORRE</strong></p>
            </div>
            <div class="col-xs-12">
                <p>Head, PBAC Secretariat</p>
            </div>
        </div>
        <hr>
        <div class="table-responsive text-center">
            <table class="table table-hover table-striped text-center">
                <thead class="table-primary">
                    <tr>
                        <th colspan="5"></th>
                        <th>BRAND / MODEL</th>
                        <th>UNIT PRICE</th>
                        <th></th>
                    </tr>
                    <tr>
                        <th>Item No.</th>
                        <th>QTY</th>
                        <th>Unit</th>
                        <th>Item Description</th>
                        <th>Total Budget per line item</th>
                        <th colspan="2">(To be filled up by the supplier)</th>
                        <th>Total (PHP)</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rfq->pr->pr_items as $item)
                    <tr>
                        <td>{{ $item->item_no }}</td>
                        <td>{{ $item->quantity }}</td>
                        <td>{{ $item->unit }}</td>
                        <td>{{ $item->description }}</td>
                        <td>{{ $item->est_cost_total }}</td>
                        <td></td>
                        <td></td>
                        <td class="item_cost_total">{{ $item->est_cost_total }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <div class="row">
                <div class="w-75 text-justify">
                    <p><strong>This procurement shall be subject to the salient provisions of the IRR of RA 9184 - Liquidated Damages - Section 69 and the delivery schedule shall be completed within ____ calendar days from the receipt of the Purchase / Job Order.</strong></p>
                </div>
                <div class="w-25 text-right">
                    <label for=""><strong>TOTAL:</strong></label>
                    <span id="total_cost"></span>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="w-100 form-group">
                <label for="">Notes:</label>
                {{ Form::textarea('notes', $rfq->notes, ['class' => 'form-control', 'readonly', 'rows' => 4]) }}
            </div>
        </div>
    </div>
</body>
<script>
    $(function(){
        var x = 0;
        $('.item_cost_total').each(function(){
            x += parseInt($(this).text());
        });
        $('#total_cost').text(x);
    })
</script>
</html>