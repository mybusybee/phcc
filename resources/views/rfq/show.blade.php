@extends('layouts.base')

@section('content')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    @if ($rfq->invite->status == 'FOR_REVISION' || ($rfq->invite->status == 'FOR_REVIEW/APPROVAL' && ($rfq->invite->rejects_remarks)))
    <div class="alert alert-info">
        {{$rfq->invite->rejects_remarks}}
    </div>
    @endif
    <div class="container-fluid frm">
        <div class="row">
            <div class="col-xl-12 text-center">
                <h2>REQUEST FOR QUOTATION</h2>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-xl-6">
                <strong>{{ Form::label('prnodate', 'PR No. / Date: ') }}</strong>
                <div class="row">
                    <div class="col-xl-4">
                        {{ Form::text('pr_no', $rfq->pr->pr_no, ['class' => 'form-control form-control-sm', 'readonly']) }}
                    </div>
                    <div class="col-xl-4">
                        {{ Form::text('pr_prep_date', $rfq->pr->pr_prep_date, ['class' => 'form-control form-control-sm', 'readonly']) }}
                    </div>
                </div>
            </div>
            <div class="col-xl-6">
                <strong>{{ Form::label('rfqnodate', 'RFQ No. / Date: ') }}</strong>
                <div class="row">
                    <div class="col-xl-4">
                        {{ Form::text('rfq_no', $rfq->invite->form_no, ['class' => 'form-control form-control-sm', 'readonly']) }}
                    </div>
                    <div class="col-xl-4">
                        {{ Form::text('rfq_prep_date', $rfq->rfq_prep_date, ['class' => 'form-control form-control-sm', 'readonly']) }}
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-xl-12">
                <strong><p>Sir / Madam:</p></strong>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12">
                <span>Please quote your lowest price/s for the item/s listed below including the total amount in legible style (preferably typewritten) and return this duly signed by the company's authorized signatory/ies in a {{ Form::text('', $rfq->submission_type, ['class' => 'form-control form-control-sm d-inline-block w-25', 'readonly']) }} to the General Services Division, Administrative Office c/o {{ Form::text('', $rfq->rfq_receiver, ['class' => 'form-control form-control-sm d-inline-block w-25', 'readonly']) }}, 25/F of the above address or e-mail to <i>procurement@phcc.gov.ph</i> on or before {{ Form::text('submission_date_from', $rfq->submission_date_time, ['class' => 'form-control form-control-sm d-inline-block w-25', 'readonly']) }}</span>
            </div>
        </div>
        <hr>
        <div class="form-group">
            <div class="col-xl-12">
                <p>Your participation to this bidding shall be subjected to the requirements as identified below:</p>
            </div>
            <div class="col-xl-12">
                <ul class="rfq-list list-group">
                    <li class="list-group-item">
                        @if ($rfq->invite->requirement->philgeps_reg_no == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        PhilGEPS Registration Number
                    </li>
                    <li class="list-group-item">
                        @if ($rfq->invite->requirement->mayors_business_permit_bir_cor == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        Mayor's Business Permit / BIR Certificate of Registration in case of individual
                    </li>
                    <li class="list-group-item">
                        @if ($rfq->invite->requirement->income_business_tax_return == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        Latest Income / Business Tax Return
                    </li>
                    <li class="list-group-item">
                        @if ($rfq->invite->requirement->certificate_of_exclusivity == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        Certificate of Exclusivity (for Direct Contracting)
                    </li>
                    <li class="list-group-item">
                        @if ($rfq->invite->requirement->omnibus_sworn_statement == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        Omnibus Sworn Statement
                    </li>
                    <li class="list-group-item">
                        @if ($rfq->invite->requirement->signed_terms_of_reference == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        Signed Terms of Reference
                    </li>
                    <li class="list-group-item">
                        @if ($rfq->invite->requirement->govt_tax_inclusive == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        Proposal must be inclusive of all applicable government taxes and subject to 5% R-VAT and 1% (PO) or 2% (JO) deductions.
                    </li>
                    <li class="list-group-item">
                        @if ($rfq->invite->requirement->used_form == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        Bids must be submitted using this form. Supplemental information using your company stationery may be attached to reflect the complete specification of bid e.g. brand name, model, pictures / brochures / literature, menu, etc.
                    </li>
                    <li class="list-group-item">
                        @if ($rfq->invite->requirement->not_exceeding_abc == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        Bids should not exceed the Approved Budget for the Contract (ABC) in the total amount of </label> {{ Form::text('abc_amount', $rfq->invite->requirement->abc_amount, ['class' => 'form-control form-control-sm d-inline-block w-25', 'readonly']) }}
                    </li>
                    <li class="list-group-item">
                        @if ($rfq->invite->requirement->award_by_lot == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        Award shall be made by lot(please bid for all items to avoid disqualification of bid) or
                    </li>
                    <li class="list-group-item">
                        @if ($rfq->invite->requirement->award_by_line_item == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        Award shall be made by line item
                    </li>
                    <li class="list-group-item">
                        @if ($rfq->invite->requirement->one_month_validity == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        Bids should be valid for a <strong>minimum of one (1) month</strong> from deadline of submission of bids as indicated above.
                    </li>
                    <li class="list-group-item">
                        @if ($rfq->invite->requirement->delivered_to_pcc == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        Procured items shall be delivered to <strong>{{ $rfq->invite->requirement->delivery_place }}</strong>.
                    </li>
                    <li class="list-group-item">
                        <i class="fa fa-check-circle" style="color: green;"></i>
                        Payment Terms: {{ Form::text('payment_terms', $rfq->invite->requirement->payment_terms, ['class' => 'form-control form-control-sm w-25 d-inline-block', 'readonly']) }}
                    </li>
                    <li class="list-group-item">
                        @if ($rfq->invite->requirement->signatory_refusal == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        Refusal to sign and accept an Award / Purchase Order / Job Order or enter into contract without justifiable reason, maybe ground for imposition of administrative sanctions under Rule XXIII of the Revised IRR of RA 9184.
                    </li>
                    <li class="list-group-item">
                        <label for="others"><strong>Others:</strong></label>
                        {{ Form::text('others', $rfq->invite->requirement->others, ['class' => 'form-control form-control-sm', 'readonly']) }}
                    </li>
                </ul>
            </div>
            <hr>
            <div class="col-xl-12">
                <p>In case you do not receive any communication from PCC {{ Form::text('not_awarded', $rfq->not_awarded_notification, ['class' => 'form-control form-control-sm w-25 d-inline-block', 'readonly']) }} days / months from the deadline indicated above, it will mean that the award was not made in your favor. With the end in view of obtaining the contract most advantageous to the government, PCC reserves the right to: 1.) accept or reject any or all the quotations / bids; 2.) award the contract on a per item / lot basis; and 3.) to annul the bidding process and to reject all quotations / bids at any time prior to contract award, without thereby incurring any liability to the affected bidder or bidders. Tie bids shall be resolved by draw lots.</p>
                <p>For clarifications, please contact PCC Bids and Awards Committee in the above address/telephone numbers.</p>
            </div>
            <hr>
        </div>
        <div class="form-group">
            @if(Auth::user()->user_role === 5 && $rfq->invite->status === 'FOR_EXPORT' && $rfq->signed === 0)
                <div class="col-xl-4">
                    <a href="{{ url('rfq/signed/'.$rfq->id) }}" class="btn btn-info">Signed</a>
                </div>
            @elseif(Auth::user()->user_role === 5 && $rfq->invite->status === 'FOR_EXPORT' && $rfq->signed === 1)
                <div class="col-xl-4">
                    <button class="btn btn-warning" type="button" disabled>Signed</button>
                </div>
            @endif
            <div class="col-xl-12">
                <p>Very truly yours,</p>
            </div>
            <div class="col-xl-4 mt-5">
                {{ Form::text('', $rfq->rfq_sender, ['class' => 'form-control form-control-sm', 'readonly']) }}
            </div>
            <div class="col-xl-4">
                {{ Form::text('', $rfq->rfq_sender_designation, ['class' => 'form-control form-control-sm', 'readonly']) }}
            </div>
        </div>
        <hr>
        <div class="table-responsive text-center">
            <table class="table table-hover table-striped">
                <thead class="table-primary">
                    <tr>
                        <th colspan="4"></th>
                        <th>BRAND / MODEL / OTHERS</th>
                        <th>UNIT PRICE</th>
                        <th></th>
                    </tr>
                    <tr>
                        <th>Item No.</th>
                        <th>Unit</th>
                        <th>QTY</th>
                        <th>Item Description</th>
                        <th colspan="2">(To be filled up by the supplier)</th>
                        <th>Total (PHP)</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rfq->pr->pr_items as $item)
                        <tr>
                            <td>{{ $item->item_no }}</td>
                            <td>{{ $item->unit }}</td>
                            <td>{{ $item->quantity }}</td>
                            <td>{{ $item->description }}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        @foreach($item->pr_sub_items as $sub)
                            <tr>
                                <td>{{ $sub->item_no }}</td>
                                <td>{{ $sub->unit }}</td>
                                <td>{{ $sub->quantity }}</td>
                                <td>{{ $sub->description }}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        @endforeach
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="5"></td>
                        <td class="text-center"><strong>TOTAL:</strong></td>
                        <td></td>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div class="form-group">
            <h4>File Attachments:</h4>
            <div class="row">
                <div class="col-xl-4">
                    <div class="list-group">
                        @forelse ($rfq->invite->files as $file)
                            <a style="color: #000 !important;" href="{{ url('invitation/download/'.$file->id) }}" class="list-group-item list-group-item-action list-group-item-primary">
                                <i class="fa fa-download"></i><span>  {{ $file->filename }}</span>
                            </a>
                        @empty
                            <li style="color: #000 !important;" class="list-group-item list-group-item-danger">No file/s attached.</li>
                        @endforelse
                    </div>
                </div>
            </div>
        </div>
        @if($rfq->notes != null)
            <div class="row">
                <div class="col-xl-12 form-group">
                    <label for="">Notes:</label>
                    {{ Form::textarea('notes', $rfq->notes, ['class' => 'form-control', 'readonly']) }}
                </div>
            </div>
        @endif
        @if ($rfq->pr->personal_pmr_item->status == 'Cancelled' || $rfq->pr->personal_pmr_item->status == 'Failed')

        @else
            @if(Auth::user()->user_role === 3 && $rfq->invite->status === 'FOR_REVIEW/APPROVAL')
                <div class="form-group">
                    <a href="{{ url('/invitations/approve/'.$rfq->invite->id) }}" class="btn btn-success">Approve</a>
                    <button class="btn btn-danger" data-toggle="modal" data-target="#rejectITBModal">Reject</button>
                    <button class="btn btn-danger" data-toggle="modal" data-target="#cancelRFQModal">Cancel RFQ</button>
                </div>
                <div class="modal fade" id="rejectITBModal" tabindex="-1" role="dialog" aria-hidden="true">
                    {{ Form::open(['url' => url('/invitations/reject/'.$rfq->invite->id), 'method' => 'put']) }}
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Reason for rejection</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-md-12">
                                                {{ Form::textarea('reject_remarks', null, ['class' => 'form-control']) }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
            @elseif(Auth::user()->user_role === 5 && $rfq->invite->status === 'FOR_EXPORT')
                <div class="col-xl-4">
                    <a href="{{ url('/export/xls/rfq/'.$rfq->id) }}" class="btn btn-warning">Export</a>
                </div>
                {{ Form::open(['url' => '/invite-suppliers' , 'method' => 'post', 'style' => 'width: 100%;']) }}
                    <div class="table-responsive form-group" id="suppliersContainer">
                        <h5>Supplier List:</h5>
                        <table id="suppliers-datatable" class="table">
                            <thead class="table-primary">
                                <tr>
                                    <th><i class="fa fa-check-circle"></i></th>
                                    <th>Company Name</th>
                                    <th>Authorized Personnel</th>
                                    <th>Category</th>
                                    <th>Address</th>
                                    <th>Telephone</th>
                                    <th>Email</th>
                                    <th>TIN</th>
                                    <th>Mayor's Permit Expiry Date</th>
                                    <th>Philgeps Number</th>
                                </tr>
                            </thead>
                            <tbody>
                                <div id="supplierList">
                                    @foreach($suppliers as $supplier)
                                        @if(in_array($supplier->id, $selected_suppliers))
                                            <input type="hidden" name="selectedSuppliers[]" class="form-control form-control-sm d-inline-block w-25 supplier-id-class" value="{{ $supplier->id }}">
                                        @endif
                                    @endforeach
                                </div>
                                @foreach ($suppliers as $supplier)
                                    <tr>
                                        <td>
                                            @if (in_array($supplier->id, $selected_suppliers))
                                                <input type="checkbox" name="" id="" class="sup_id" value="{{ $supplier->id }}" checked>
                                            @else
                                            <input type="checkbox" name="" id="" class="sup_id" value="{{ $supplier->id }}">
                                            @endif
                                        </td>
                                        <td>{{ $supplier->company_name }}</td>
                                        <td>{{ $supplier->authorized_personnel }}</td>
                                        <td>{{ $supplier->category }}</td>
                                        <td>{{ $supplier->address }}</td>
                                        <td>{{ $supplier->telephone }}</td>
                                        <td>{{ $supplier->email }}</td>
                                        <td>{{ $supplier->tin_number }}</td>
                                        <td>{{ $supplier->mayors_permit }}</td>
                                        <td>{{ $supplier->philgeps_number }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row form-group">
                        <div class="col-xl-3">
                            <label for="preselect"><input required type="radio" id="preselect" value="preselect" name="submitType">Preselect</label>
                        </div>
                        <div class="col-xl-3">
                            <label for="sendEmails"><input required type="radio" id="sendEmails" value="sendEmails" name="submitType">Send Emails</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success submit" type="submit" id="submitBtn" disabled>Save/Send</button>
                        <button class="btn btn-danger" data-toggle="modal" data-target="#cancelRFQModal">Cancel RFQ</button>
                    </div>
                    <input type="hidden" name="invitation_id" value="{{ $rfq->invite->id }}">
                {{ Form::close() }}
            @elseif ($rfq->invite->status === 'FARMED_OUT')
                <div class="col-xl-12">
                    <h1>Selected Suppliers</h1>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead class="table-primary">
                            <tr>
                                <th>Company Name</th>
                                <th>Authorized Personnel</th>
                                <th>Category</th>
                                <th>Address</th>
                                <th>Telephone</th>
                                <th>Email</th>
                                <th>TIN</th>
                                <th>Mayor's Permit Expiry Date</th>
                                <th>Philgeps Number</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($rfq->invite->suppliers as $supplier)
                                <tr>
                                    <td>{{ $supplier->company_name }}</td>
                                    <td>{{ $supplier->authorized_personnel }}</td>
                                    <td>{{ $supplier->category }}</td>
                                    <td>{{ $supplier->address }}</td>
                                    <td>{{ $supplier->telephone }}</td>
                                    <td>{{ $supplier->email }}</td>
                                    <td>{{ $supplier->tin_number }}</td>
                                    <td>{{ $supplier->mayors_permit }}</td>
                                    <td>{{ $supplier->philgeps_number }}</td>
                                    @if($supplier->pivot->email_status === 'EMAIL_SENT')
                                        <td>{{ $supplier->pivot->email_status }} {{date('M d, Y h:i:s a', strtotime($rfq->invite->updated_at))}}</td>
                                    @else
                                        <td><a href="#" class="btn btn-warning">Send Email</a></td>
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @elseif ($rfq->invite->status !== 'CANCELLED')
                <div class="form-group">
                    <button class="btn btn-danger" data-toggle="modal" data-target="#cancelRFQModal">Cancel RFQ</button>
                </div>
            @endif
        @endif
        
    </div>
    <div class="modal fade" id="cancelRFQModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Cancel RFQ</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <h3>Are you sure?</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
                    <a href="{{ url('/invitations/cancel/'.$rfq->invite->id) }}" class="btn btn-danger">Yes</a>
                </div>
            </div>
        </div>
    </div>
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('js/rfq/show.js') }}"></script>
@endsection