@extends('layouts.base')

@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/datatables.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('css/abstract/table.css') }}">
<link rel="stylesheet" href="{{ asset('css/form-validation.css') }}">
<div id="aobForm" class="frm">
    {{ Form::open(['url' => url('/aop/store'), 'method' => 'post', 'class' => 'abstract-form']) }}
        <input type="hidden" ref="inviteID" name="invite_id" value="{{ $invitation->invite->id }}">
        <div class="row">
            <div class="col-xl-12">
                <h4 class="text-center">ABSTRACT OF PROPOSAL</h4>
            </div>
        </div>
        <input type="hidden" id="abstract_co" value="{{ $abstract_parent_count }}">
        <div class="row mt-3">
            <div class="col-xl-1">
                <p>PR No.:</p>
            </div>
            <div class="col-xl-3">
                {{ Form::text('', $invitation->pr->pr_no, ['class' => 'form-control form-control-sm', 'readonly']) }}
            </div>
            <div class="col-xl-1 offset-xl-4">
                <p class="text-right">AOP No.:</p>
            </div>
        
            <div class="col-xl-3">
                {{ Form::text('form_no', null, ['class' => 'form-control form-control-sm border border-info', 'readonly', 'id' => 'aop_no']) }}
            </div>
        </div>
        <div class="row mt-1">
            <div class="col-xl-1">
                <p>PR Date:</p>
            </div>
            <div class="col-xl-3">
                {{ Form::text('', $invitation->pr->pr_prep_date, ['class' => 'form-control form-control-sm', 'readonly']) }}
            </div>
            <div class="col-xl-2 offset-xl-3">
                <p class="text-right">Opening Date:</p>
            </div>
            <div class="col-xl-3">
                {{ Form::text('opening_date', null, ['class' => 'form-control form-control-sm border border-info', 'id' => 'openingDate', 'required']) }}
            </div>
        </div>
        <div class="row mt-1">
            <div class="col-xl-1">
                <p>ABC:</p>
            </div>
            <div class="col-xl-3">
                {{ Form::text('', '&#8369;'.$invitation->pr->pr_item_total->total_estimate, ['class' => 'form-control form-control-sm', 'readonly']) }}
            </div>
            <div class="col-xl-2 offset-xl-3">
                <p class="text-right">Mode of Procurement:</p>
            </div>
            <div class="col-xl-3">
                <select name="proc_mode_type" id="proc_mode_type" required class="form-control form-control-sm border border-info">
                    <option value="" disabled selected>Please select one...</option>
                    @foreach ($proc_modes as $proc_mode)
                        <option value="{{$proc_mode->id}}">{{$proc_mode->mode}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        @include('abstract.table')
        <div class="form-group">
            <input type="submit" value="Save" class="btn btn-success">
        </div>
    {{ Form::close() }}
    @include('abstract/new-supplier-modal')
    @include('abstract/new-registered-supplier-modal')
</div>
<script type="text/javascript" src="{{ asset('js/utils/datatables.min.js') }}"></script>
<script src="{{ asset('js/aop/create.js') }}"></script>
<script src="{{ asset('js/aob/add-reco-row.js') }}"></script>
<script src="{{ asset('js/aob/remove-reco-row.js') }}"></script>
<script src="{{ asset('js/aob/remove-doc-row.js') }}"></script>
<script src="{{ asset('js/aob/add-supplier-column.js') }}"></script>
<script src="{{ asset('js/abstracts/total-cost-calculator.js') }}"></script>
<script src="{{ asset('js/abstracts/sub-total-calculator.js') }}"></script>
<script src="{{ asset('js/aop/compute-technical-proposal.js') }}"></script>
<script src="{{ asset('js/abstracts/filter-per-item-supplier-by-bid.js') }}"></script>
@endsection