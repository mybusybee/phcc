@extends('layouts.base')

@section('content')
    <div id="dashboard" class="frm">
        <div class="row">
            <div class="col-xl-6">
                <div class="card text-center" style="border-top: 3px solid #adadad">
                    <div class="card-header dashboard bg-primary-lt">
                        <p>Purchase Requests PCC-Wide</p>
                    </div>
                    <div class="card-body">
                        <input type="hidden" name="" id="graphData1" value="{{ $graphData }}">
                        <canvas id="canvasChart1" height="100"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-xl-6">
                <div class="card text-center" style="border-top: 3px solid #adadad">
                    <div class="card-header dashboard bg-primary-lt">
                        <p>Purchase Requests Office-Wide</p>
                    </div>
                    <div class="card-body">
                        <input type="hidden" name="" id="graphData2" value="{{ $graphDataOffice }}">
                        <canvas id="canvasChart2" height="100"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-xl-6">
                <div class="card text-center" style="border-top: 3px solid red;">
                    <div class="card-header dashboard bg-primary-lt">
                        <p>Purchase Requests Current Status</p>
                    </div>
                    <div class="card-body">
                        <input type="hidden" name="" id="prStatusGraph1" value="{{ $pr_status_data }}">
                        <canvas id="prStatusCanvasChart" height="100"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-xl-6">
                <div class="card text-center" style="border-top: 3px solid magenta;">
                    <div class="card-header dashboard bg-primary-lt">
                        <p>Procurement Monitoring Report</p>
                    </div>
                    <div class="card-body">
                        {{-- <input type="hidden" name="" id="officeTotalBudget" value="100,150,300,750,500,350,728,1098,924">
                        <canvas id="officeTotalBudgetCanvasChart" height="100"></canvas> --}}
                        <div id="carousel" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img src="{{ asset('img/dashboard/slides/1.png') }}" class="d-block w-100">
                                </div>
                                @for ($i = 2; $i <= 15; $i++)
                                    <div class="carousel-item">
                                        <img src="{{ asset('img/dashboard/slides/'.$i.'.png') }}" class="d-block w-100">
                                    </div>
                                @endfor
                            </div>
                            <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-xl-6">
                <div class="card text-center" style="border-top: 3px solid green;">
                    <div class="card-header dashboard bg-primary-lt">
                        <p>Philippine Competition Commission Website</p>
                    </div>
                    <div class="card-body">
                        <a href="https://phcc.gov.ph/">
                            <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/8/89/Philippine_Competition_Commission_%28PCC%29.svg/490px-Philippine_Competition_Commission_%28PCC%29.svg.png" width="22%" alt="" class="img-fluid img-thumbnail mx-auto d-block">
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-xl-6">
                <div class="card text-center" style="border-top: 3px solid #3e00fb;">
                    <div class="card-header dashboard bg-primary-lt">
                        <p>PhilGEPS Website</p>
                    </div>
                    <div class="card-body">
                        <a href="https://www.philgeps.gov.ph/">
                            <img src="https://engrjhez.files.wordpress.com/2012/03/philgeps_logo.jpg" width="22%" alt="" class="img-fluid img-thumbnail mx-auto d-block">
                        </a>
                    </div>
                </div>

            </div>
        </div>
        @php $referencesUsersArray = [1, 3]; @endphp
        <div class="row mt-3">
            <div class="col-xl-6">
                <div class="card text-center" style="border-top: 3px solid #2dccd4">
                    <div class="card-header dashboard bg-primary-lt">
                        <i class="fa fa-bell"></i>  References
                    </div>
                    <div class="card-body">
                        <div class="list-group list-group-flush">
                            <div class="list-group list-group-flush">
                                @forelse ($references as $reference)
                                    <a href="{{ url('get-reference/'.$reference->id) }}" class="list-group-item list-group-item-action list-group-item-secondary text-dark">
                                        {{ $reference->filename }}
                                    </a>
                                @empty
                                    <a href="#" class="list-group-item list-group-item-action list-group-item-danger text-dark">
                                        No references/manuals yet.
                                    </a>
                                @endforelse
                            </div>
                            @if (in_array(Auth::user()->user_role, $referencesUsersArray))
                                <button class="btn btn-xs btn-primary" data-toggle="modal" data-target="#referencesModal">Add new</button>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-6">
                <div class="card text-center" style="border-top: 3px solid #47ef6b">
                    <div class="card-header dashboard bg-primary-lt">
                        <i class="fa fa-bell"></i>  Announcements
                    </div>
                    <div class="card-body">
                        <div class="list-group list-group-flush">
                            @forelse ($announcements as $announcement)
                                <a href="{{ url('announcement/'.$announcement->id) }}" class="list-group-item list-group-item-action list-group-item-secondary text-dark">
                                    <span class="badge badge-secondary badge-pill">{{ $announcement->created_at->diffForHumans() }}</span>
                                    {{ $announcement->title }}
                                </a>
                            @empty
                                <a href="#" class="list-group-item list-group-item-action list-group-item-danger text-dark">
                                    No announcement yet.
                                </a>
                            @endforelse
                            <a href="{{ url('announcement') }}" class="btn btn-xs btn-info">See all</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if (in_array(Auth::user()->user_role, $referencesUsersArray))
        <div class="modal fade" id="referencesModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    {{ Form::open(['url' => url('/reference'), 'method' => 'post', 'files' => 'true']) }}
                        <div class="modal-header">
                            <h5 class="modal-title">Upload References / Manuals</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <div class="container-fluid">
                                {{-- <div class="row">
                                    <button type="button" class="btn btn-success btn-xs addNewFile"><i class="fa fa-plus"></i> Add</button>
                                </div> --}}
                                <div class="row" id="filesSection">
                                    <div class="col-xl-12 file-row">
                                        <div class="input-group input-group-sm">
                                            <input type="file" multiple name="references[]" accept=".pdf" class="form-control form-control-sm" />
                                            {{-- <div class="input-group-append">
                                                <button type="button" class="btn btn-xs btn-danger removeRow"><i class="fa fa-minus"></i></button>
                                            </div> --}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-secondary btn-xs">Upload File/s</button>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    @endif
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.3/Chart.bundle.min.js"></script>
    <script src="{{ asset('js/dashboard/chart-data.js') }}"></script>
    <script src="{{ asset('js/dashboard/chart-data-2.js') }}"></script>
    <script src="{{ asset('js/dashboard/references.js') }}"></script>
    <script src="{{ asset('js/dashboard/prstatusgraph.js') }}"></script>
    @push('scripts')
        <script>
            $('.carousel').carousel({
                interval: 4000
            })
        </script>
    @endpush
    {{-- <script src="{{ asset('js/dashboard/officetotalbudget.js') }}"></script> --}}
    {{-- <script src ="{{ asset('js/dashboard/index.js') }}"></script> --}}
@endsection