@extends('layouts.base')

@section('content')
<div class="container-fluid">
	@if (session()->get('user_role') == 1 || session()->get('user_role') == 3 || session()->get('user_role') == 5 || session()->get('user_role') == 8)
	<div class="row form-group">
		<div class="col-xl-12">
			<div class="py-3 px-5 summary-container">
				<div class="row">
					<div class="summary-item col-xl-2">
						<a href="{{ url('/pr') }}">
							<div>
								<p class="count">{{ $counts->pr }}</p>
								<p class="title">Purchase Requests</p>
							</div>
						</a>
					</div>
					<div class="col-half-offset summary-item col-xl-2">
						@if (session()->get('user_role') == 1 || session()->get('user_role') == 3 || session()->get('user_role') == 5)
							<a href="{{ url('/invitations') }}">
						@else
							<a href="{{ url('/forbidden-access') }}">
						@endif
							<div>
								<p class="count">{{ $counts->invitations }}</p>
								<p class="title">IBs/RFQs/RFPs/REIs</p>
							</div>
						</a>
					</div>
					<div class="col-half-offset summary-item col-xl-2">
						@if (session()->get('user_role') == 1 || session()->get('user_role') == 3 || session()->get('user_role') == 5)
							<a href="{{ url('/abstracts') }}">
						@else
							<a href="{{ url('/forbidden-access') }}">
						@endif
							<div>
								<p class="count">{{ $counts->abstract_parents }}</p>
								<p class="title">AOBs/AOPs/AOQs</p>
							</div>
						</a>
					</div>
					<div class="col-half-offset summary-item col-xl-2">
						@if (session()->get('user_role') == 1 || session()->get('user_role') == 3 || session()->get('user_role') == 5)
							<a href="{{ url('/orders/all') }}">
						@else
							<a href="{{ url('/forbidden-access') }}">
						@endif
							<div>
								<p class="count">{{ $counts->order_parents }}</p>
								<p class="title">Purchase / Job Orders / Contracts</p>
							</div>
						</a>
					</div>
					<div class="col-half-offset summary-item col-xl-2">
						@if (session()->get('user_role') == 1 || session()->get('user_role') == 3 || session()->get('user_role') == 5)
							<a href="{{ url('/suppliers') }}">
						@else
							<a href="{{ url('/forbidden-access') }}">
						@endif
							<div>
								<p class="count">{{ $counts->supplier }}</p>
								<p class="title">Total Suppliers</p>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	@endif
	<div class="row form-group">
		<div class="col-xl-6">
			<div class="card">
				<div class="card-header bg-primary text-center" style="color:#fff"><strong>FAQs(Frequently Asked Questions)</strong></div>
				<div class="card-body">
					<ul class="list-group">
						<a href="{{ url('/download/ppmp') }}"><li class="list-group-item list-group-item-action list-group-item-secondary"><span><i class="fas fa-download"></i></span> How to prepare a PPMP?</li></a>
						<a href="{{ url('/download/pr') }}"><li class="list-group-item list-group-item-action list-group-item-secondary"><span><i class="fas fa-download"></i></span> How to prepare a PR?</li></a>
						<a href="{{ url('/download/rfq') }}"><li class="list-group-item list-group-item-action list-group-item-secondary"><span><i class="fas fa-download"></i></span> How to prepare an RFQ?</li></a>
						<a href="{{ url('/download/aoq') }}"><li class="list-group-item list-group-item-action list-group-item-secondary"><span><i class="fas fa-download"></i></span> How to prepare an AOQ?</li></a>
						<a href="{{ url('/download/pojo') }}"><li class="list-group-item list-group-item-action list-group-item-secondary"><span><i class="fas fa-download"></i></span> How to prepare a PO/JO?</li></a>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-xl-6">
			<div class="card">
				<div class="card-header bg-primary text-center" style="color:#fff"><strong>Procurement Manual</strong></div>
				<div class="card-body">
					<p class="mb-3">
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent viverra, ante ut lacinia cursus, dui mauris eleifend nulla, at rhoncus lorem neque ac nunc. Donec a sagittis tortor. Nullam vel massa eget magna tincidunt aliquet. Duis quis diam pharetra, pulvinar odio id, congue sapien. Sed quam urna, tincidunt et mollis ut, tempus quis justo. Etiam euismod sodales dolor, nec placerat neque sagittis nec. Fusce justo velit, dapibus ac fermentum in, ultrices sed turpis. Aliquam aliquam, sem vitae faucibus porta, urna diam posuere sem, ac laoreet dolor diam eu dolor. Donec ac consequat nisi, id varius ligula. Nullam posuere turpis at nisl pretium, sit amet varius magna maximus. Cras id laoreet mi, tristique pretium neque. Nunc id lacus metus. Cras enim sapien, blandit at ornare a, eleifend et eros. Sed convallis quam sed eleifend venenatis.
					</p>
					<div class="btn-group">
						<a href="{{ url('download/procurement-manual') }}"><button type="button" class="btn btn-outline-dark"><span><i class="fas fa-download"></i></span> Download Procurement Manual</button></a>
						@if((Auth::user()->role->id == 1) ||(Auth::user()->role->id == 3))
							<button type="button" class="btn btn-outline-dark dropdown-toggle dropdown-toggle-split" data-toggle="dropdown">
								<span class="caret"></span>
							</button>
							<div class="dropdown-menu dropdown-menu-right">
								<div class="dropdown-item" href="#">
									<div class="input-group">
										<button data-toggle="modal" data-target="#uploadProcManual" class="btn dropdown-item"><span><i class="fas fa-upload"></i></span> Upload new Manual</button>
									</div>
								</div>
							</div>
						@endif
					</div> 
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modals -->
<div class="modal fade" id="uploadProcManual" tabindex="-1" role="dialog" aria-labelledby="uploadProcManual" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			{{ Form::open(['url' => url('/upload/proc-manual'), 'method' => 'post', 'files' => true]) }}
				<div class="modal-header">
					<h5 class="modal-title">Upload new Procurement Manual</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="input-group">
						{{ Form::file('proc_file', ['class' => '', 'id' => 'uploadProcMFile']) }}
						{{ Form::submit('Upload', ['class' => 'btn btn-success']) }}
					</div>
				</div>
			{{ Form::close() }}
		</div>
	</div>
</div>
@endsection