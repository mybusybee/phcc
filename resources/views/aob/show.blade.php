@extends('layouts.base')

@section('content')
<link rel="stylesheet" href="{{ asset('css/abstract/table.css') }}">
<link rel="stylesheet" href="{{ asset('css/form-validation.css') }}">
<div id="aobForm" class="frm">
    <div class="row">
        <div class="col-xl-12">
            <h4 class="text-center">ABSTRACT OF BIDS</h4>
        </div>
    </div>
    <div class="row">
        <div class="col-xl-2 mx-auto mb-2">
            {{ Form::text('', $abstract_form->bid_type, ['class' => 'form-control form-control-sm', 'readonly']) }}
        </div>
    </div>
    <div class="row">
        <div class="col-xl-3 mx-auto">
            {{ Form::text('', $abstract_form->pb_type, ['class' => 'form-control form-control-sm', 'readonly']) }}
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-xl-1">
            <p>PR No.:</p>
        </div>
        <div class="col-xl-3">
            {{ Form::text('', $abstract_form->pr->pr_no, ['class' => 'form-control form-control-sm', 'readonly']) }}
        </div>
        <div class="col-xl-1 offset-xl-4">
            <p class="text-right">AOB No.:</p>
        </div>
    
        <div class="col-xl-3">
            {{ Form::text('aob_no', $abstract_form->abstract_parent->form_no, ['class' => 'form-control form-control-sm border border-info', 'readonly', 'id' => 'aob_no']) }}
        </div>
    </div>
    <div class="row mt-1">
        <div class="col-xl-1">
            <p>PR Date:</p>
        </div>
        <div class="col-xl-3">
            {{ Form::text('', $abstract_form->pr->pr_prep_date, ['class' => 'form-control form-control-sm', 'readonly']) }}
        </div>
        <div class="col-xl-2 offset-xl-3">
            <p class="text-right">Opening Date:</p>
        </div>
        <div class="col-xl-3">
            {{ Form::text('opening_date', $abstract_form->opening_date, ['class' => 'form-control form-control-sm border border-info', 'readonly']) }}
        </div>
    </div>
    <div class="row mt-1">
        <div class="col-xl-1">
            <p>ABC:</p>
        </div>
        <div class="col-xl-3">
            {{ Form::text('', '&#8369;'.$abstract_form->pr->pr_item_total->total_estimate, ['class' => 'form-control form-control-sm', 'readonly']) }}
        </div>
        <div class="col-xl-2 offset-xl-3">
            <p class="text-right">Mode of Procurement:</p>
        </div>
        <div class="col-xl-3">
            {{ Form::text('', $abstract_form->procurement_mode->mode, ['class' => 'form-control form-control-sm ', 'readonly']) }}
        </div>
    </div>
    @include('abstract.table-show')
    @if($abstract_form->abstract_parent->status === 'SAVED')
        <div class="form-group mt-3">
            <a href="{{ url('/export/aob/'.$abstract_form->id) }}" class="btn btn-warning">Export</a>
            <input type="button" value="Cancel AOB" class="btn btn-danger" data-toggle="modal" data-target="#cancelAOBModal">
        </div>
        @include('abstract.cancel-modal')
    @endif
</div>
@endsection