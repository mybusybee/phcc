<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/pmrtable.css') }}">
</head>
<body>
    <div class="table-responsive">
        <table class="table">
            <!-- Image header here -->
            <thead>
                <tr style="width: 8.43;"></tr>
                <tr></tr>
                <tr></tr>
                <tr></tr>
                <tr>
                    <th class="text-center" colspan="{{ 9 + ( count($aob->abstract_parent->suppliers) * 2 ) }}">ABSTRACT OF BIDS</th>
                </tr>
                <tr>
                    <th class="text-center" colspan="{{ 9 + ( count($aob->abstract_parent->suppliers) * 2 ) }}">({{ $aob->bid_type }})</th>
                </tr>
                <tr></tr>
                <tr>
                    <td></td>
                    <th colspan="2">PR No.:</th>
                    <td colspan="4" style="border-bottom: 1px solid #000;">{{ $aob->pr->pr_no }}</td>
                    @for ($i = 1; $i <= ( ( count($aob->abstract_parent->suppliers) * 2 ) - 3 ); $i++)
                        <td></td>
                    @endfor
                    <th colspan="2">AOB No.:</th>
                    <td colspan="2" style="border-bottom: 1px solid #000;">{{ $aob->abstract_parent->form_no }}</td>
                </tr>
                <tr>
                    <td></td>
                    <th colspan="2">PR Date:</th>
                    <td colspan="4" style="border-bottom: 1px solid #000;">{{ $aob->pr->pr_prep_date }}</td>
                    @for ($i = 1; $i <= ( ( count($aob->abstract_parent->suppliers) * 2 ) - 3 ); $i++)
                        <td></td>
                    @endfor
                    <th colspan="2">Opening Date:</td>
                    <td colspan="2" style="border-bottom: 1px solid #000;">{{ $aob->opening_date }}</td>
                </tr>
                <tr>
                    <td></td>
                    <th colspan="2">ABC:</th>
                    <td colspan="4" style="border-bottom: 1px solid #000;">&#8369; {{ $aob->pr->pr_item_total->total_estimate }}</td>
                    @for ($i = 1; $i <= ( ( count($aob->abstract_parent->suppliers) * 2 ) - 3 ); $i++)
                        <td></td>
                    @endfor
                    <th colspan="2" style="width: 14;">Mode of Procurement:</td>
                    <td colspan="2" style="border-bottom: 1px solid #000;">{{ $aob->procurement_mode->mode }}</td>
                </tr>
            </thead>
        </table>
    </div>
    <table class="custom-table-bordered-2">
        <tbody>
            <tr>
                <th class="text-center" style="width: 6; height: 53; wrap-text: true;">Item No.</th>
                <th class="text-center" style="width: 10;">Quantity</th>
                <th class="text-center" style="width: 10;">Unit</th>
                <th class="text-center" colspan="4">Description</th>
                <th class="text-right" colspan="2">Supplier's Name</th>
                @foreach($aob->abstract_parent->suppliers as $supplier)
                    <td colspan="2">{{ $supplier->company_name }}</td>
                @endforeach
            </tr>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th colspan="4"></th>
                <th colspan="2" class="text-right" style="height: 57;">Location Address</th>
                @foreach($aob->abstract_parent->suppliers as $supplier)
                    <td colspan="2" style="wrap-text: true;">{{ $supplier->address }}</td>
                @endforeach
            </tr>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th colspan="4"></td>
                <th colspan="2" class="text-right">Contact No.:</th>
                @foreach($aob->abstract_parent->suppliers as $supplier)
                    <td colspan="2">{{ $supplier->telephone }}</td>
                @endforeach
            </tr>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th colspan="4"></t>
                <th colspan="2" class="text-right">T.I.N.</th>
                @foreach($aob->abstract_parent->suppliers as $supplier)
                    <td colspan="2">{{ $supplier->tin_number }}</td>
                @endforeach
            </tr>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th colspan="4"></t>
                <th colspan="2" class="text-center">Estimated</th>
                @foreach($aob->abstract_parent->suppliers as $supplier)
                    <th colspan="2"></th>
                @endforeach
            </tr>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th colspan="4"></t>
                <th class="text-center" style="width: 17">Unit Cost</th>
                <th class="text-center" style="width: 17">Total</th>
                @foreach($aob->abstract_parent->suppliers as $supplier)
                    <th style="width: 17;" class="text-center">Unit Cost</th>
                    <th style="width: 17;" class="text-center">Total</th>
                @endforeach
            </tr>
            @foreach($aob->pr->pr_items as $pr_item)
                <tr>
                    <td>{{ $pr_item->item_no }}</td>
                    <td>{{ $pr_item->quantity }}</td>
                    <td>{{ $pr_item->unit }}</td>
                    <td colspan="4">{{ $pr_item->description }}</td>
                    @if ($pr_item->est_cost_unit != " ")
                        <td>&#8369; {{ number_format($pr_item->est_cost_unit, 2, '.', ',') }}</td>
                    @else
                        <td></td>
                    @endif
                    @if (!count($pr_item->pr_sub_items))
                        <td>&#8369; {{ $pr_item->est_cost_total }}</td>
                    @else
                        <td></td>
                    @endif
                    @if (count($pr_item->suppliers->where('pivot.abstract_id', $aob->abstract_parent->id)))
                         @foreach($pr_item->suppliers->where('pivot.abstract_id', $aob->abstract_parent->id) as $item_supplier)
                            @if($item_supplier->pivot->unit_cost === 'n/a')
                                <td>{{ $item_supplier->pivot->unit_cost }}</td>
                            @else
                                <td>&#8369; {{ number_format($item_supplier->pivot->unit_cost, 2, '.', ',') }}</td>
                            @endif
                            @if($item_supplier->pivot->total !== 'n/a')
                                @if($item_supplier->pivot->total > $pr_item->est_cost_total)
                                    <td>&#8369; {{ number_format($item_supplier->pivot->total, 2,'.', ',') }}</td>
                                @else
                                    <td>&#8369; {{ number_format($item_supplier->pivot->total, 2, '.', ',') }}</td>
                                @endif
                            @else
                                <td>n/a</td>
                            @endif
                        @endforeach
                    @else
                        @foreach ($aob->abstract_parent->suppliers as $supplier)
                            @if(count($pr_item->pr_sub_items) && ($aob->pr->pr_item_total->is_lot_purchase))
                                <td></td>
                                <td></td>
                            @else
                                <td colspan="2"></td>
                            @endif
                        @endforeach
                    @endif
                </tr>
                @if (($aob->pr->pr_item_total->is_per_item_purchase) && (!count($pr_item->pr_sub_items)))
                    <tr>
                        <td colspan="9" rowspan="1" class="text-center" id="" style="vertical-align: middle;">
                            <center><strong><p>RANKING</p></strong></center>
                        </td>
                        @foreach($pr_item->suppliers->where('pivot.abstract_id', $aob->abstract_parent->id) as $item_supplier)
                        <th colspan="2" class="text-center">{{$item_supplier->pivot->per_item_ranking}}</td>
                        @endforeach
                    </tr>
                @endif
                @if(count($pr_item->pr_sub_items))
                    @foreach($pr_item->pr_sub_items as $subItem)
                        <tr>
                            <td>{{ $subItem->item_no }}</td>
                            <td>{{ $subItem->quantity }}</td>
                            <td>{{ $subItem->unit }}</td>
                            <td colspan="4">{{ $subItem->description }}</td>
                            <td>&#8369; {{ number_format(str_replace(',', '', $subItem->est_cost_unit), 2, '.', ',') }}</td>
                            <td>&#8369; {{ number_format(str_replace(',', '', $subItem->est_cost_total), 2, '.', ',') }}</td>
                            @foreach($subItem->suppliers->where('pivot.abstract_id', $aob->abstract_parent->id) as $subItem_supplier)
                                @if($subItem_supplier->pivot->unit_cost === 'n/a')
                                    <td>{{ $subItem_supplier->pivot->unit_cost }}</td>
                                @else
                                    <td>&#8369; {{ number_format($subItem_supplier->pivot->unit_cost, 2, '.', ',') }}</td>
                                @endif
                                @if($subItem_supplier->pivot->total !== 'n/a')
                                    <td>&#8369; {{ number_format($subItem_supplier->pivot->total, 2, '.', ',') }}</td>
                                @else
                                    <td>n/a</td>
                                @endif
                            @endforeach
                        </tr>
                        @if($aob->pr->pr_item_total->is_per_item_purchase)
                            <tr>
                                <td colspan="9" rowspan="1" class="text-center" id="" style="vertical-align: middle;">
                                    <center><strong><p>RANKING</p></strong></center>
                                </td>
                                @foreach($subItem->suppliers->where('pivot.abstract_id', $aob->abstract_parent->id) as $subItem_supplier)
                                    <th colspan="2" class="text-center">{{$subItem_supplier->pivot->per_item_ranking}}</td>
                                @endforeach
                            </tr>
                        @endif
                    @endforeach
                @endif
            @endforeach
            @if($aob->pr->pr_item_total->is_lot_purchase)
                <tr>
                    <th colspan="9" class="text-center">RANKING</th>
                    @foreach($aob->abstract_parent->suppliers as $supplier)
                        <th colspan="2" class="text-center">{{ $supplier->pivot->ranking }}</th>
                    @endforeach
                </tr>
            @endif
            <tr>
                <th colspan="3" style="wrap-text: true;">ELIGIBILITY REQUIREMENT</th>
                <td colspan="6">Class "A" - Legal Documents:</td>
                @foreach ($aob->abstract_parent->suppliers as $supplier)
                    <td colspan="2"></td>
                @endforeach
            </tr>
            @foreach ($aob->abstract_parent->docs as $doc)
                @if ($doc->doc_class == 'other')
                <tr>
                    <td colspan="3"></td>
                    <td colspan="6" style="text-indent: 3px;">{{$doc->document_name}}</td>
                    @foreach ($doc->supplier_eligibilities as $supplier)
                        <th colspan="2" class="text-center">{{$supplier->eligibility}}</th>
                    @endforeach
                </tr>
                @endif
            @endforeach
            <tr>
                <td colspan="3" style="wrap-text: true;"></td>
                <td colspan="6">Class "A" - Technical Documents:</td>
                @foreach ($aob->abstract_parent->suppliers as $supplier)
                    <td colspan="2"></th>
                @endforeach
            </tr>
            @foreach ($aob->abstract_parent->docs as $doc)
                @if ($doc->doc_class == 'technical')
                <tr>
                    <td colspan="3"></td>
                    <td colspan="6" style="text-indent: 3px;">{{$doc->document_name}}</td>
                    @foreach ($doc->supplier_eligibilities as $supplier)
                        <th colspan="2" class="text-center">{{$supplier->eligibility}}</th>
                    @endforeach
                </tr>
                @endif
            @endforeach
            <tr>
                <td colspan="3" style="wrap-text: true;"></td>
                <td colspan="6">Class "A" - Financial Documents:</td>
                @foreach ($aob->abstract_parent->suppliers as $supplier)
                    <td colspan="2"></td>
                @endforeach
            </tr>
            @foreach ($aob->abstract_parent->docs as $doc)
                @if ($doc->doc_class == 'financial')
                <tr>
                    <td colspan="3"></td>
                    <td colspan="6" style="text-indent: 3px;">{{$doc->document_name}}</td>
                    @foreach ($doc->supplier_eligibilities as $supplier)
                        <th colspan="2" class="text-center">{{$supplier->eligibility}}</th>
                    @endforeach
                </tr>
                @endif
            @endforeach
            <tr>
                <td colspan="3" style="wrap-text: true;"></td>
                <td colspan="6">Class "B" Document:</td>
                @foreach ($aob->abstract_parent->suppliers as $supplier)
                    <td colspan="2"></td>
                @endforeach
            </tr>
            @foreach ($aob->abstract_parent->docs as $doc)
                @if ($doc->doc_class == 'classb')
                <tr>
                    <td colspan="3"></td>
                    <td colspan="6" style="text-indent: 3px;">{{$doc->document_name}}</td>
                    @foreach ($doc->supplier_eligibilities as $supplier)
                        <th colspan="2" class="text-center">{{$supplier->eligibility}}</th>
                    @endforeach
                </tr>
                @endif
            @endforeach
            <tr>
                <td colspan="3" style="wrap-text: true;"></td>
                <td colspan="6">Other Technical Documents:</td>
                @foreach ($aob->abstract_parent->suppliers as $supplier)
                    <td colspan="2"></td>
                @endforeach
            </tr>
            @foreach ($aob->abstract_parent->docs as $doc)
                @if ($doc->doc_class == 'other')
                <tr>
                    <td colspan="3"></td>
                    <td colspan="6" style="text-indent: 3px;">{{$doc->document_name}}</td>
                    @foreach ($doc->supplier_eligibilities as $supplier)
                        <th colspan="2" class="text-center">{{$supplier->eligibility}}</th>
                    @endforeach
                </tr>
                @endif
            @endforeach
            <tr>
                <th colspan="9" class="text-center">RESULT</th>
                @foreach($aob->abstract_parent->suppliers as $supplier)
                    <th colspan="2" class="text-center">{{ $supplier->pivot->compliance }}</th>
                @endforeach
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <th style="border: 1px solid #000;" colspan="{{ 9 + ( count($aob->abstract_parent->suppliers) * 2 ) }}">Recommendations:</th>
            </tr>
            <tr>
                <th colspan="{{ 9 + ( count($aob->abstract_parent->suppliers) * 2 ) }}" style="border-left: 1px solid #000; border-right: 1px solid #000;"></th>
            </tr>
            <tr>
                @php $recommendationColspan = ( 9 + ( count($aob->abstract_parent->suppliers) * 2 ) ) / 4; @endphp
                @php $wholeRow = 9 + ( count($aob->abstract_parent->suppliers) * 2 ); @endphp
                @if($aob->bid_type === 'As Read')
                    @if($aob->pb_type === 'For Consulting Services')
                        <td colspan="{{ (int)$recommendationColspan + 2 }}" class="text-center" style="border-left: 1px solid #000;">Single / Highest Rated Bidder:</td>
                    @else
                        <td colspan="{{ (int)$recommendationColspan + 2 }}" class="text-center" style="border-left: 1px solid #000;">Single / Lowest Calculated Bidder:</td>
                    @endif
                @else
                    @if($aob->pb_type === 'For Consulting Services')
                        <td colspan="{{ (int)$recommendationColspan + 2 }}" class="text-center" style="border-left: 1px solid #000;">Single / Highest Rated and Responsive Bidder:</td>
                    @else
                        <td colspan="{{ (int)$recommendationColspan + 2 }}" class="text-center" style="border-left: 1px solid #000;">Single / Lowest Calculated and Responsive Bidder:</td>
                    @endif
                @endif
                <td colspan="{{ (int)$recommendationColspan - 1 }}" class="text-center">for Item/Lot #:</td>
                <td colspan="{{ (int)$recommendationColspan }}" class="text-center">Sub-Total Amount:</td>
                <td colspan="{{ ( $wholeRow -  ((int)$recommendationColspan * 3) ) - 1 }}" class="text-center" style="border-right: 1px solid #000;">Remarks:</td>
            </tr>
            @foreach($aob->abstract_parent->recommendation->recommendation_items as $reco_item)
                <tr>
                    <td colspan="{{ (int)$recommendationColspan + 2 }}" style="border-left: 1px solid #000;" class="text-center">{{ $reco_item->supplier->company_name }}</td>
                    <td class="text-center" colspan="{{ (int)$recommendationColspan - 1 }}">{{ $reco_item->pr_item_nos }}</td>
                    <td class="text-center" colspan="{{ (int)$recommendationColspan }}">&#8369; {{ number_format($reco_item->subtotal, 2, '.', ',') }}</td>
                    <td colspan="{{ ( $wholeRow -  ((int)$recommendationColspan * 3) ) - 1 }}" style="border-right: 1px solid #000;" class="text-center">{{ $reco_item->remarks }}</td>
                </tr>
            @endforeach
            <tr>
                <th colspan="{{ 9 + ( count($aob->abstract_parent->suppliers) * 2 ) }}" style="border-left: 1px solid #000; border-right: 1px solid #000;"></th>
            </tr>
            <tr>
                <td colspan="{{ (int)$recommendationColspan + 2 }}" class="text-center" style="border-left: 1px solid #000; border-top: 1px solid #000;"></td>
                <td colspan="{{ (int)$recommendationColspan - 1 }}" class="text-center" style="border-top: 1px solid #000;"></td>
                <td colspan="{{ (int)$recommendationColspan }}" class="text-center" style="border-top: 1px solid #000;">TOTAL: &#8369; {{ number_format($aob->abstract_parent->recommendation->total, 2, '.', ',') }}</td>
                <td colspan="{{ ( $wholeRow -  ((int)$recommendationColspan * 3) ) - 1 }}" class="text-center" style="border-right: 1px solid #000; border-top: 1px solid #000;"></td>
            </tr>
            @php $by_3 = ( 9 + ( count($aob->abstract_parent->suppliers) * 2 ) ) / 3; @endphp
            @php $by_2 = ( 9 + ( count($aob->abstract_parent->suppliers) * 2 ) ) / 2; @endphp
            @if($aob->bid_type === 'As Calculated')
                <tr>
                    <th colspan="{{ 9 + ( count($aob->abstract_parent->suppliers) * 2 ) }}" style="border: 1px solid #000; background: #ccc">Technical Working Group:</th>
                </tr>
                <tr>
                    <th colspan="{{ 9 + ( count($aob->abstract_parent->suppliers) * 2 ) }}" style="border-left: 1px solid #000; border-right: 1px solid #000;"></th>
                </tr>
                <tr>
                    <th colspan="{{ (int)$by_3 }}" style="border-left: 1px solid #000;" class="text-center">{{ $aob->twg_chairman }}</th>
                    <th colspan="{{ (int)$by_3 }}" class="text-center">{{ $aob->twg_member_1 }}</th>
                    <th colspan="{{ ( $wholeRow -  ((int)$by_3 * 2) ) }}" style="border-right:1px solid #000;" class="text-center">{{ $aob->twg_member_2 }}</th>
                </tr>
                <tr>
                    <th colspan="{{ (int)$by_3 }}" style="border-left: 1px solid #000;" class="text-center">TWG Chairperson</th>
                    <th colspan="{{ (int)$by_3 }}" class="text-center">TWG Member</th>
                    <th colspan="{{ ( $wholeRow -  ((int)$by_3 * 2) ) }}" style="border-right: 1px solid #000;" class="text-center">TWG Member</th>
                </tr>
                <tr>
                    <th colspan="{{ 9 + ( count($aob->abstract_parent->suppliers) * 2 ) }}" style="border-left: 1px solid #000; border-right: 1px solid #000; border-bottom: 1px solid #000;"></th>
                </tr>
            @endif
            <tr>
                <th colspan="{{ 9 + ( count($aob->abstract_parent->suppliers) * 2 ) }}" style="border: 1px solid #000; background: #ccc;">PCC Bids and Awards Committee</th>
            </tr>
            <tr>
                <th colspan="{{ (int)$by_2 }}" style="border-left: 1px solid #000;" class="text-center">{{ $aob->pbac_chairperson }}</th>
                <th colspan="{{ ( $wholeRow -  ((int)$by_2 * 1) ) }}" style="border-right: 1px solid #000;" class="text-center">{{ $aob->pbac_vice_chairperson }}</th>
            </tr>
            <tr>
                <th colspan="{{ (int)$by_2 }}" style="border-left: 1px solid #000;" class="text-center">PBAC Chairperson</th>
                <th colspan="{{ ( $wholeRow -  ((int)$by_2 * 1) ) }}" style="border-right: 1px solid #000;" class="text-center">PBAC Vice Chairperson</th>
            </tr>
            <tr>
                <td colspan="{{ 9 + ( count($aob->abstract_parent->suppliers) * 2 ) }}" style="border-left: 1px solid #000; border-right: 1px solid #000;"></td>
            </tr>
            <tr>
                <th colspan="{{ (int)$by_3 }}" style="border-left: 1px solid #000;" class="text-center">{{ $aob->pbac_member_1 }}</th>
                <th colspan="{{ (int)$by_3 }}" class="text-center">{{ $aob->pbac_member_1 }}</th>
                <th colspan="{{ ( $wholeRow -  ((int)$by_3 * 2) ) }}" style="border-right: 1px solid #000;" class="text-center">{{ $aob->provisional_member }}</th>
            </tr>
            <tr>
                <th colspan="{{ (int)$by_3 }}" style="border-left: 1px solid #000; border-bottom: 1px solid #000;" class="text-center">PBAC Member</th>
                <th colspan="{{ (int)$by_3 }}" style="border-bottom: 1px solid #000;" class="text-center">PBAC Member</th>
                <th colspan="{{ ( $wholeRow -  ((int)$by_3 * 2) ) }}" style="border-right: 1px solid #000; border-bottom: 1px solid #000;" class="text-center">Provisional Member</th>
            </tr>
            @if($aob->bid_type === 'As Read')
                <tr>
                    <th colspan="{{ 9 + ( count($aob->abstract_parent->suppliers) * 2 ) }}" style="border: 1px solid #000; background: #ccc;">Observers:</th>
                </tr>
                <tr>
                    <td colspan="{{ 9 + ( count($aob->abstract_parent->suppliers) * 2 ) }}" style="border-left: 1px solid #000; border-right: 1px solid #000;"></td>
                </tr>
                <tr>
                    <th colspan="{{ (int)$by_3 }}" style="border-left: 1px solid #000;" class="text-center">{{ $aob->coa_representative }}</th>
                    <th colspan="{{ (int)$by_3 }}" class="text-center">{{ $aob->private_observer_1 }}</th>
                    <th colspan="{{ ( $wholeRow -  ((int)$by_3 * 2) ) }}" style="border-right: 1px solid #000;" class="text-center">{{ $aob->private_observer_2 }}</th>
                </tr>
                <tr>
                    <th colspan="{{ (int)$by_3 }}" style="border-left: 1px solid #000;" class="text-center">COA Representative</th>
                    <th colspan="{{ (int)$by_3 }}" class="text-center">Private Observer 1</th>
                    <th colspan="{{ ( $wholeRow -  ((int)$by_3 * 2) ) }}" style="border-right: 1px solid #000;" class="text-center">Private Observer 2</th>
                </tr>
                <tr>
                    <td colspan="{{ 9 + ( count($aob->abstract_parent->suppliers) * 2 ) }}" style="border-left: 1px solid #000; border-right: 1px solid #000; border-bottom: 1px solid #000;"></td>
                </tr>
            @endif
            <tr></tr>
            <tr></tr>
            <tr>
                <td colspan="{{ $wholeRow }}"><i>Generated as of {{ $aob->dateToday }}</i></td>
            </tr>
        </tfoot>
    </table>
</html>