@extends('layouts.base')

@section('content')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<div class="frm">
    <div class="row">
        <div class="col-md-12 text-center">
            <h2 class="d-inline">Annual Procurement Plan for Year {{ $app_year }} (Approved Budget)</h2>
            @php $importUser = [1,5,8]; @endphp
            @if (in_array(Auth::user()->user_role, $importUser))
                <button data-toggle="modal" data-target="#importMovingAppModal" type="button" class="btn btn-info float-right"><i class="fas fa-upload"></i> Import Deducted APP</button>
                @include('app.import-moving')
            @endif
        </div>
    </div>
    <div>
        <div class="table-responsive">
            <table class="table table-striped table-hover text-center" id="movingAppTable">
                <thead class="table-primary">
                    <tr>
                        <th style="vertical-align: middle;">Code(PAP)</th>
                        <th style="vertical-align: middle;">Program Project</th>
                        <th style="vertical-align: middle;">Category Code</th>
                        <th style="vertical-align: middle;">PMO/End User</th>
                        <th style="vertical-align: middle;">Mode of Procurement</th>
                        <th style="vertical-align: middle;">Remaining Budget(PhP)</th>
                        <th style="vertical-align: middle;">Remarks(brief description of Program/Activity/Project)</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($moving_apps as $app_row)
                    <tr style="font-size: 12px;">
                        <td style="vertical-align: middle;">{{ $app_row->code }}</td>
                        <td style="vertical-align: middle;">{{ $app_row->program_project }}</td>
                        <td style="vertical-align: middle;">{{ $app_row->pap->category_code_field->category_code }}</td>
                        <td style="vertical-align: middle;">{{ $app_row->office->office }}</td>
                        <td style="vertical-align: middle;">{{ $app_row->procurementMode->mode }}</td>
                        <td style="white-space: nowrap; vertical-align: middle;">&#8369; {{ $app_row->total_estimated_budget }}</td>
                        <td style="vertical-align: middle;"></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    {{-- <a href="{{ url('/export/app/'.$app_year) }}" class="btn btn-success">Export APP</a> --}}
</div>
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $('#movingAppTable').DataTable();
</script>
@endsection