@extends('layouts.base')

@section('content')
    <div id="appTable" class="frm">
        <div class="row">
            <div class="col-xl-12">
                <h4>Annual Procurement Plan for the Year of 
                    <select name="" id="appYear" class="form-control form-control-sm d-inline-block w-25 appYear" @change="getAPP($event)">
                        <option value="" selected disabled>Please select one...</option>
                    </select>
                </h4>
            </div>
        </div>
        <div class="table-responsive">
            <table class="table table-hover table-striped">
                <thead class="table-primary">
                    <tr>
                        <th rowspan="2" style="vertical-align: middle;">Code(PAP)</th>
                        <th rowspan="2" style="vertical-align: middle;">Program Project</th>
                        <th rowspan="2" style="vertical-align: middle;">Category Code</th>
                        <th rowspan="2" style="vertical-align: middle;">PMO/End User</th>
                        <th rowspan="2" style="vertical-align: middle;">Mode of Procurement</th>
                        <th colspan='4'>Schedule for Each Procurement Activity</th>
                        <th style="vertical-align: middle;" rowspan="2">Source of Funds</th>
                        <th style="vertical-align: middle;" colspan="3">Estimated Budget(PhP)</th>
                        <th style="vertical-align: middle;" rowspan="2">Remarks(brief description of Program/Activity/Project)</th>
                    </tr>
                    <tr>
                        <th style="vertical-align: middle;">Advertisement/Posting of IB/REI</th>
                        <th style="vertical-align: middle;">Submission/Opening of Bids</th>
                        <th style="vertical-align: middle;">Notice of Award</th>
                        <th style="vertical-align: middle;">Contract Signing</th>
                        <th style="vertical-align: middle;">Total</th>
                        <th style="vertical-align: middle;">MOOE</th>
                        <th style="vertical-align: middle;">CO</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-if="!getItems.length">
                        <td colspan="99">
                            <h3>No items available.</h3>
                        </td>
                    </tr>
                    <tr v-else v-for="item in getItems" style="font-size: 12px;" class="text-center">
                        <td style="vertical-align: middle;">@{{ item.code }}</td>
                        <td style="vertical-align: middle;">@{{ item.program_project }}</td>
                        <td style="vertical-align: middle;">@{{ item.category_code }}</td>
                        <td style="vertical-align: middle;">@{{ item.pmo_end_user }}</td>
                        <td style="vertical-align: middle;">@{{ item.mode_of_procurement }}</td>
                        <td style="font-size: 12px; vertical-align: middle;">@{{ item.ads }}</td>
                        <td style="font-size: 12px; vertical-align: middle;">@{{ item.sub }}</td>
                        <td style="font-size: 12px; vertical-align: middle;">@{{ item.noa }}</td>
                        <td style="font-size: 12px; vertical-align: middle;">@{{ item.contract }}</td>
                        <td style="vertical-align: middle;">@{{ item.source_of_funds }}</td>
                        <td style="white-space: nowrap; vertical-align: middle;">&#8369; @{{ item.total_estimated_budget }}</td>
                        <td style="white-space: nowrap; vertical-align: middle;">&#8369; @{{ item.mooe ? item.mooe : '0.00' }}</td>
                        <td style="white-space: nowrap; vertical-align: middle;">&#8369; @{{ item.co ? item.co : '0.00' }}</td>
                        <td style="vertical-align: middle;">@{{ item.remarks }}</td>
                    </tr>
                </tbody>
                <tfoot class="table-secondary">
                    <tr>
                        <th colspan="10" style="text-align: right;">Total</th>
                        <th style="white-space: nowrap;">&#8369; @{{ getTotals.totalBudget ? getTotals.totalBudget.toLocaleString() : '0.00' }}</th>
                        <th style="white-space: nowrap;">&#8369; @{{ getTotals.totalmooe ? getTotals.totalmooe.toLocaleString() : '0.00' }}</th>
                        <th style="white-space: nowrap;">&#8369; @{{ getTotals.totalco ? getTotals.totalco.toLocaleString() : '0.00' }}</th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div class="my-3">
            @php $appEditUsers = [1,3,4,8]; @endphp
            @if(in_array(Auth::user()->user_role, $appEditUsers))
                <a :href="editLink" class="btn btn-info"><i class="fa fa-edit"></i> Edit</a>
            @endif
            <a :href="exportLink" class="btn btn-warning"><i class="fa fa-download"></i> Export</a>
            @php $upload_app_users = [1, 5, 8]; @endphp
            @if (in_array(Auth::user()->user_role, $upload_app_users))
                <button type="button" class="btn btn-success" data-toggle="modal" data-target="#importAppModal"><i class="fas fa-upload"></i> Import</button>
                @include('app.import-modal')
            @endif
        </div>
    </div>
    <script src="{{ asset('js/app/show.js') }}"></script>
@endsection