@extends('layouts.base')

@section('content')
<link rel="stylesheet" href="{{ asset('css/abstract/table.css') }}">
{{ Form::open(['url' => url("/app/update/.$year"), 'method' => 'put']) }}
<div class="container-fluid">
	<div class="row frm">
		<div class="col-md-12">
			<h1 class="text-center">Annual Procurement Plan for FY {{ $year }}</h1>
		</div>
		<div class="table-responsive table-wrapper col-md-12">
			<table class="table table-striped table-hover text-center custom-table-bordered">
				<thead class="text-center table-primary">
					<tr>
						<th colspan="5"></th>
						<th colspan='4'>Schedule for Each Procurement Activity</th>
						<th colspan="2"></th>
					</tr>
					<tr>
						<th>Code (PAP)</th>
						<th class="resize-min-width" rowspan="2">Procurement Program/Project</th>
						<th>Category Code</th>
						<th>PMO/End-User</th>
						<th>Mode of Procurement</th>
						<th>Source of Funds</th>
						<th class="resize-min-width-half" rowspan="2">Advertisement</th>
						<th class="resize-min-width-half" rowspan="2">Submission</th>
						<th class="resize-min-width-half" rowspan="2">Notice of Award</th>
						<th class="resize-min-width-half" rowspan="2">Contract Signing</th>
						<th class="resize-min-width-half" rowspan="2">Total Est. Budget</th>
						<th class="resize-min-width" rowspan="2">Remarks</th>
					</tr>
				</thead>
				<tbody>
					<?php $i = 1; ?>
					@foreach($app as $app_row)
					<tr>
						<td class="d-none">{{ Form::hidden('row_counter[]', 'x', ['class' => 'form-control']) }}</td>
						<td class="d-none">{{ Form::hidden('app_id'.$i, $app_row->id, ['class' => 'form-control']) }}</td>
						{{-- <td>{{ Form::text('app_code'.$i, $app_row->code, ['class' => 'form-control']) }}</td> --}}
						<td>{{ $app_row->code }}</td>
						<td>{{ $app_row->program_project }}</td>
						<td>{{ $app_row->pap->category_code_field->category_code }}</td>
						<td>{{ $app_row->office->office }}</td>
						<td>{{ $app_row->mode_of_procurement }}</td>
						<td>{{ $app_row->source_of_funds }}</td>
						<td>{{ Form::text('advertisement'.$i, $app_row->app_schedule->advertisement, ['class' => 'form-control form-control-sm border border-info date']) }}</td>
						<td>{{ Form::text('submission'.$i, $app_row->app_schedule->submission, ['class' => 'form-control form-control-sm border border-info date']) }}</td>
						<td>{{ Form::text('notice_of_award'.$i, $app_row->app_schedule->notice_of_award, ['class' => 'form-control form-control-sm border border-info date']) }}</td>
						<td>{{ Form::text('contract_signing'.$i, $app_row->app_schedule->contract_signing, ['class' => 'form-control form-control-sm border border-info date']) }}</td>
						<td>{{ $app_row->total_estimated_budget }}</td>
						<td>{{ Form::textarea('app_remarks'.$i, $app_row->remarks, ['class' => 'form-control', 'col' => '5', 'row' => '2', 'style' => 'height: 90px !important;']) }}</td>
					</tr>
					<?php $i++; ?>
					@endforeach
				</tbody>
			</table>
		</div>
		<div class="col-xl-3">
			<input type="submit" value="Update" class="btn btn-success" />
		</div>
	</div>
</div>
{{ Form::close() }}
<script src="{{ asset('js/ppmp/annual-proc-plan.js') }}"></script>
@endsection