<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/pmrtable.css') }}">
</head>
<body>
    {{-- <div class="row">
        <div style="text-align: left; width: 48%; display: inline-block;">
            <img src="{{ public_path('img/logo.png') }}" style="width:250;height:70px;" alt="" class="img-responsive">
        </div>
        <div class="col-md-6 text-right" style="text-align: right; width: 48%; display: inline-block;">
            <img src="{{ public_path('img/logo.png') }}" style="width:250;height:70px;" alt="" class="img-responsive">
        </div>
    </div> --}}
    <br><br><br><br><br><br>
    <div class="row">
        <div class="col-md-12 text-center">
            <h1>Philippine Competition Commission Annual Procurement Plan for FY 2018</h1>
        </div>
    </div>
    <br><br>
    <div class="table-responsive">
        <table class="table table-striped table-hover text-center custom-table-bordered">
            <thead class="table-primary">
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th colspan="4">Schedule for Each Procurement Activity</th>
                    <th></th>
                    <th colspan="3">Estimated Budget (PhP)</th>
                    <th></th>
                </tr>
                <tr>
                    <th>Code(PAP)</th>
                    <th>Program/Project</th>
                    <th>PMO/End-User</th>
                    <th>Mode of Procurement</th>
                    <th>Advertisement/Posting of IB/REI</th>
                    <th>Submission/Opening of Bids</th>
                    <th>Notice of Award</th>
                    <th>Contract Signing</th>
                    <th>Source of Funds</th>
                    <th>Total</th>
                    <th>MOOE</th>
                    <th>CO</th>
                    <th>Remarks(brief description of Program/Activity/Project)</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $tM = 0;
                    $tC = 0;
                    $tE = 0;
                @endphp
                @foreach($app as $app_row)
                <tr>
                    <td>{{ $app_row->code }}</td>
                    <td>{{ $app_row->program_project }}</td>
                    <td>{{ $app_row->office->office }}</td>
                    <td>{{ $app_row->procurementMode->mode }}</td>
                    <td>{{ $app_row->app_schedule->advertisement }}</td>
                    <td>{{ $app_row->app_schedule->submission }}</td>
                    <td>{{ $app_row->app_schedule->notice_of_award }}</td>
                    <td>{{ $app_row->app_schedule->contract_signing }}</td>
                    <td>{{ $app_row->source_of_funds }}</td>
                    <td>&#8369; {{ $app_row->total_estimated_budget }}</td>
                    <td>&#8369; {{ $app_row->mooe == '' ? '0' : $app_row->mooe }}</td>
                    <td>&#8369; {{ $app_row->co == '' ? '0' : $app_row->co }}</td>
                    <td>{{ $app_row->remarks }}</td>
                    @php
                        $tE += floatval(str_replace(',', '', $app_row->total_estimated_budget));
                        $tM += floatval(str_replace(',', '', $app_row->mooe));
                        $tC += floatval(str_replace(',', '', $app_row->co));
                    @endphp
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th colspan="9" style="text-align: right;">Total</th>
                    <th>&#8369; {{ number_format($tE, 2, '.', ',') }} </th>
                    <th>&#8369; {{ number_format($tM, 2, '.', ',') }} </th>
                    <th>&#8369; {{ number_format($tC, 2, '.', ',') }} </th>
                </tr>
            </tfoot>
        </table>
    </div>
    <div class="table-responsive">
        <table class="table">
            <tr>
                <td>
                    <p>Note:</p>
                </td>
            </tr>
            <tr>
                <td>__________________________________________________________________________</td>
            </tr>
        </table>
    </div>
    <br>
    <div class="table-responsive">
        <table class="table">
            <tr>
                <th colspan="4">Prepared By:</th>
                <th colspan="4">Reviewed By:</th>
                <th colspan="4">Recommending Approval:</th>
                <th colspan="4">Approved:</th>
            </tr>
            <tr></tr>
            <tr></tr>
            <tr>
                <td colspan="4">___________________</td>
                <td colspan="4">___________________</td>
                <td colspan="4">___________________</td>
                <td colspan="4">___________________</td>
            </tr>
        </table>
    </div>
    <br><br>
    <div class="table-responsive">
        <table class="table">
            <tr>
                <td><i>Generated as of {{ $app->dateToday }}</i></td>
            </tr>
        </table>
    </div>
</body>
</html>