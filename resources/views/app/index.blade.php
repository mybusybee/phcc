@extends('layouts.base')

@section('content')
<div class="row">
	<div class="col-md-12">
		<h1>Annual Procurement Plan List</h1>
	</div>
</div>
<div class="row">
	<div class="col-md-4 table-responsive">
		<table class="table table-hover table-striped text-center">
			<thead class="table-primary">
				<th>Year</th>
				<th>Action</th>
			</thead>
			<tbody>
				@foreach($apps as $app)
				<tr>
					<td>{{ $app->app_year }}</td>
					<td>
						<a href="{{ url('/app/view/'.$app->app_year) }}" class="btn btn-info"><i class="fa fa-eye"></i></a>
						@if (session()->get('user_role') == 8)
						<a href="{{ url('/app/edit/'.$app->app_year) }}" class="btn btn-primary"><i class="fa fa-edit"></i></a>
						@endif
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@endsection