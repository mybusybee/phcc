@extends('layouts.base')

@section('content')
<div class="frm">
    <div class="row">
        <div class="col-md-12 text-center">
            <h1>Annual Procurement Plan for Year {{ $app_year }}</h1>
        </div>
    </div>
    <div>
        <div class="table-responsive">
            <table class="table table-striped table-hover text-center custom-table-bordered">
                <thead class="table-primary">
                    <tr>
                        <th rowspan="2" style="vertical-align: middle;">Code(PAP)</th>
                        <th rowspan="2" style="vertical-align: middle;">Program Project</th>
                        <th rowspan="2" style="vertical-align: middle;">Category Code</th>
                        <th rowspan="2" style="vertical-align: middle;">PMO/End User</th>
                        <th rowspan="2" style="vertical-align: middle;">Mode of Procurement</th>
                        <th colspan='4'>Schedule for Each Procurement Activity</th>
                        <th style="vertical-align: middle;" rowspan="2">Source of Funds</th>
                        <th style="vertical-align: middle;" colspan="3">Estimated Budget(PhP)</th>
                        <th style="vertical-align: middle;" rowspan="2">Remarks(brief description of Program/Activity/Project)</th>
                    </tr>
                    <tr>
                        <th style="vertical-align: middle;">Advertisement/Posting of IB/REI</th>
                        <th style="vertical-align: middle;">Submission/Opening of Bids</th>
                        <th style="vertical-align: middle;">Notice of Award</th>
                        <th style="vertical-align: middle;">Contract Signing</th>
                        <th style="vertical-align: middle;">Total</th>
                        <th style="vertical-align: middle;">MOOE</th>
                        <th style="vertical-align: middle;">CO</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                        $tE = 0;
                        $tC = 0;
                        $tM = 0;
                    @endphp
                    @foreach($app as $app_row)
                        <tr style="font-size: 12px;">
                            <td style="vertical-align: middle;">{{ $app_row->code }}</td>
                            <td style="vertical-align: middle;">{{ $app_row->program_project }}</td>
                            <td style="vertical-align: middle;">{{ $app_row->pap->category_code_field->category_code }}</td>
                            <td style="vertical-align: middle;">{{ $app_row->office->office }}</td>
                            <td style="vertical-align: middle;">{{ $app_row->mode_of_procurement }}</td>
                            <td style="font-size: 12px; vertical-align: middle;">{{ $app_row->app_schedule->advertisement }}</td>
                            <td style="font-size: 12px; vertical-align: middle;">{{ $app_row->app_schedule->submission }}</td>
                            <td style="font-size: 12px; vertical-align: middle;">{{ $app_row->app_schedule->notice_of_award }}</td>
                            <td style="font-size: 12px; vertical-align: middle;">{{ $app_row->app_schedule->contract_signing }}</td>
                            <td style="vertical-align: middle;">{{ $app_row->source_of_funds }}</td>
                            <td style="white-space: nowrap; vertical-align: middle;">&#8369; {{ $app_row->total_estimated_budget }}</td>
                            <td style="white-space: nowrap; vertical-align: middle;">&#8369; {{ $app_row->mooe == '' ? '0.00' : $app_row->mooe }}</td>
                            <td style="white-space: nowrap; vertical-align: middle;">&#8369; {{ $app_row->co == '' ? '0.00' : $app_row->co }}</td>
                            <td style="vertical-align: middle;">{{ $app_row->remarks }}</td>
                        </tr>
                        @php
                            $tE += floatval(str_replace(',', '', $app_row->total_estimated_budget));
                            $tM += floatval(str_replace(',', '', $app_row->mooe));
                            $tC += floatval(str_replace(',', '', $app_row->co));
                        @endphp
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="10" style="text-align: right;">Total</th>
                        <th style="white-space: nowrap;">&#8369; {{ number_format($tE, 2, '.', ',') }} </th>
                        <th style="white-space: nowrap;">&#8369; {{ number_format($tM, 2, '.', ',') }} </th>
                        <th style="white-space: nowrap;">&#8369; {{ number_format($tC, 2, '.', ',') }} </th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <a href="{{ url('/export/app/'.$app_year) }}" class="btn btn-success">Export APP</a>
</div>
@endsection