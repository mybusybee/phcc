<div class="modal fade" id="importAppModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        {{ Form::open(['url' => url('upload-app'), 'method' => 'post', 'files' => true]) }}
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="label">Upload Annual Procurement Plan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="year">Year:</label>
                        <select name="year" id="" class="form-control appYear" required>
                            <option value="" disabled selected>Please select one...</option>
                        </select>
                    </div>
                    Upload APP (xlsx/xls/csv files only): <input type="file" name="file" class="form-control" required accept=".xlsx,.xls,.csv">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary submitAPP w-100">Upload</button>
                    <a href="{{ url('/app/template-1') }}" class="btn btn-info"><i class="fas fa-download"></i> Download Original Template</a>
                </div>
            </div>
        {{ Form::close() }}
    </div>
</div>