<div class="frm">
	<div class="row">
		<div class="col-md-12 table-responsive">
			<table class="table table-striped table-hover text-center custom-table-bordered">
				<thead class="table-primary">
					<tr>
						<th colspan="5"></th>
						<th colspan='4'>Schedule for Each Procurement Activity</th>
						<th colspan="2"></th>
					</tr>
					<tr>
						<th>Code</th>
						<th>Program Project</th>
						<th>PMO/End User</th>
						<th>Mode of Procurement</th>
						<th>Source of Funds</th>
						<th>Advertisement</th>
						<th>Submission</th>
						<th>Notice of Award</th>
						<th>Contract Signing</th>
						<th>Total Estimated Budget</th>
						<th>Remarks</th>
					</tr>
				</thead>
				<tbody>
					@foreach($app as $app_row)
					<tr>
						<td>{{ $app_row->code }}</td>
						<td>{{ $app_row->program_project }}</td>
						<td>{{ $app_row->office->office }}</td>
						<td>{{ $app_row->mode_of_procurement }}</td>
						<td>{{ $app_row->source_of_funds }}</td>
						<td>{{ $app_row->app_schedule->advertisement }}</td>
						<td>{{ $app_row->app_schedule->submission }}</td>
						<td>{{ $app_row->app_schedule->notice_of_award }}</td>
						<td>{{ $app_row->app_schedule->contract_signing }}</td>
						<td>{{ $app_row->total_estimated_budget }}</td>
						<td>{{ $app_row->remarks }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>