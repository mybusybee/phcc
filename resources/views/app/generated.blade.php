@extends('layouts.base')

@section('content')
<div class="container-fluid">
	<div class="row frm">
		<div class="col-md-12">
			<h1 class="text-center">Annual Procurement Plan for FY {{ $fy }}</h1>
		</div>
		<div class="table-responsive col-md-12">
			<table class="table table-striped table-hover text-center custom-table-borderedd">
				<thead class="table-primary">
					<tr>
						<th rowspan="2" style="vertical-align: middle;">Code</th>
						<th rowspan="2" style="vertical-align: middle;">Program Project</th>
						<th rowspan="2" style="vertical-align: middle;">PMO/End User</th>
						<th rowspan="2" style="vertical-align: middle;">Mode of Procurement</th>
						<th colspan='4'>Schedule for Each Procurement Activity</th>
						<th style="vertical-align: middle;" rowspan="2">Source of Funds</th>
						<th style="vertical-align: middle;" colspan="3">Estimated Budget(PhP)</th>
						<th style="vertical-align: middle;" rowspan="2">Remarks</th>
					</tr>
					<tr>
						<th style="vertical-align: middle;">Advertisement</th>
						<th style="vertical-align: middle;">Submission</th>
						<th style="vertical-align: middle;">Notice of Award</th>
						<th style="vertical-align: middle;">Contract Signing</th>
						<th style="vertical-align: middle;">Total</th>
						<th style="vertical-align: middle;">MOOE</th>
						<th style="vertical-align: middle;">CO</th>
					</tr>
				</thead>
				<tbody>
					@foreach($app_rows as $app_row)
					<tr style="font-size: 12px;">
						<td>{{ $app_row->code }}</td>
						<td>{{ $app_row->program_project }}</td>
						<td>{{ $app_row->office->office }}</td>
						<td>{{ $app_row->procurementMode->mode }}</td>
						<td style="font-size: 12px;">{{ $app_row->app_schedule->advertisement }}</td>
						<td style="font-size: 12px;">{{ $app_row->app_schedule->submission }}</td>
						<td style="font-size: 12px;">{{ $app_row->app_schedule->notice_of_award }}</td>
						<td style="font-size: 12px;">{{ $app_row->app_schedule->contract_signing }}</td>
						<td>{{ $app_row->source_of_funds }}</td>
						<td>{{ number_format($app_row->total_estimated_budget, 2, '.', ',') }}</td>
						<td>{{ number_format($app_row->mooe, 2, '.', ',') }}</td>
						<td>{{ number_format($app_row->co, 2, '.', ',') }}</td>
						<td>{{ '-do-' }}</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection