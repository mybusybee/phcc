<?php

use App\SystemNotification;

$unread = SystemNotification::where([
    'user_id' => Auth::id(),
    'is_read' => 0
])->latest()->get();

$read = SystemNotification::where([
    'user_id' => Auth::id(),
    'is_read' => 1
])->limit(5)->latest()->get();

$total = count($unread) + count($read);

?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">
<nav class="navbar navbar-expand-lg navbar-light drop-shadow">
    <div class="menu-icon">
        <span class="line"></span>
        <span class="line"></span>
        <span class="line"></span>
    </div>
<a class="navbar-brand logo-container" href="{{ url('/dashboard') }}">
        <img src="{{ asset('img/pcc-logo-pdf.png') }}" alt="" class="logo-image">
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#toolbar" aria-controls="toolbar" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="toolbar" style="align-items: center; justify-content: flex-end;">
        <ul class="navbar-nav mr-5 mt-2 mt-lg-0">
            <div class="bd-highlight mr-lg-5">
                <div class="btn-group" style="position: relative;">
                    <button type="button" class="btn btn-raised btn-xs {{ count($unread) > 0 ? 'btn-danger animated delay-1s tada infinite' : 'btn-info' }} dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-bell"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" style="overflow-y: scroll; max-height: 50vh; min-width: 350px; background-color: #b2c5fd;">
                        <h5 class="dropdown-item-text ml-2">Notifications</h5>
                        @forelse ($unread as $unread_notif)
                            <a class="dropdown-item" href="{{ $unread_notif->link."?n=".$unread_notif->id }}" style="background-color: #a7a7a7; padding: 0.5rem; border-bottom: 1px solid #fff; white-space: normal;">
                                <div class="row">
                                    <div class="col-xl-1"><i class="fas fa-bell" style="vertical-align: middle;"></i></div>
                                    <div class="col-xl-10" style="font-size: 0.9rem;">{{ $unread_notif->message }}</div>
                                </div>
                            </a>
                        @empty
                            <h6 class="dropdown-item-text ml-2">No new notifications</h6>
                        @endforelse
                        {{-- <div style="border-bottom: 1px solid #b5b5b5;"></div> --}}
                        {{-- @if(count($read))
                            @foreach ($read as $read_notif)
                                <a class="dropdown-item" href="{{ $read_notif->link }}" style="padding: 1rem 0.5rem; border-bottom: 1px solid #b5b5b5;"><i class="fas fa-bell" style="margin-right: 1%;"></i>{{ $read_notif->message }}</a>
                            @endforeach
                        @endif --}}
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item text-center" href="{{ url('notifications') }}">See all</a>
                    </div>
                </div>
            </div>
        </ul>
        <form class="form-inline my-2 my-lg-0">
            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ Auth::user()->fullName }}
                        <i>({{ Auth::user()->role->user_role }})</i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="{{ url('/logout') }}">Logout</a>
                    </div>
                </li>
            </ul>
        </form>
    </div>
</nav>