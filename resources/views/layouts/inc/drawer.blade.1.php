<div class="menu-list">
        <ul>
            <li>
                <a href="{{ url('/dashboard') }}">
                    <div class="{{ Request::is(['dashboard/*', 'dashboard']) ? 'menu-active' : '' }} hvr-bounce-to-right">
                        <div class="icon">
                            <img src="{{ asset('img/dashboard.PNG') }}" alt="">
                        </div>
                        <span>Dashboard</span>
                    </div>
                </a>
            </li>
            <li class="has-submenu">
                <a href="{{ url('/ppmps') }}">
                    <div class="hvr-bounce-to-right">
                        <div class="icon">
                            <img src="{{ asset('img/pr.png') }}" alt="">
                        </div>
                        <span>PPMP</span>
                    </div>
                </a>
                <ul class="submenu">
                    @if(session()->get('user_role') == 8)
                    <li>
                        <a href="{{ url('/ppmps/approved') }}">
                            <div class="hvr-bounce-to-right">
                                <div class="icon">
                                    <img src="{{ asset('img/pr.png') }}" alt="">
                                </div>
                                <span>View All Approved PPMPs</span>
                            </div>
                        </a>
                    </li>
                    @endif
                    @if(session()->get('user_role') == 1 || session()->get('user_role') == 4 || session()->get('user_role') == 7)
                    <li>
                        <a href="{{ url('/ppmps/create') }}">
                            <div class="hvr-bounce-to-right">
                                <div class="icon">
                                <img src="{{ asset('img/create.png') }}" alt="">
                                </div>
                                <span>Create New PPMP</span>
                            </div>
                        </a>
                    </li>
                    @endif
                    <li>
                        <a href="{{ url('/ppmps/moving') }}">
                            <div class="hvr-bounce-to-right">
                                <div class="icon">
                                <img src="{{ asset('img/create.png') }}" alt="">
                                </div>
                                <span>Moving PPMP</span>
                            </div>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="has-submenu">
                <a href="{{ url('/app/all') }}">
                    <div class="hvr-bounce-to-right">
                        <div class="icon">
                            <img src="{{ asset('img/procurement.PNG') }}" alt="">
                        </div>
                        <span>APP</span>
                    </div>
                </a>
                <ul class="submenu">
                    <li>
                        <a href="{{ url('/app/moving') }}">
                            <div class="hvr-bounce-to-right">
                                <div class="icon">
                                <img src="{{ asset('img/create.png') }}" alt="">
                                </div>
                                <span>Moving APP</span>
                            </div>
                        </a>
                    </li>
                </ul>
            </li>
            @if (session()->get('user_role') != 8)
            <li class="has-submenu">
                <a href="{{ url('/pr') }}">
                    <div class="hvr-bounce-to-right">
                        <div class="icon">
                            <img src="{{ asset('img/pr.png') }}" alt="">
                        </div>
                        <span>Purchase Request</span>
                    </div>
                </a>
                @if(count(Auth::user()->office->app))
                    <ul class="submenu">
                        <li>
                            <a href="{{ url('/pr/create') }}">
                                <div class="hvr-bounce-to-right">
                                    <div class="icon">
                                        <img src="{{asset('img/create.png')}}" alt="">
                                    </div>
                                    <span>Create New PR</span>
                                </div>
                            </a>
                        </li>
                    </ul>
                @endif
            </li>   
            @endif
            @if (session()->get('user_role') == 1 || session()->get('user_role') == 3 || session()->get('user_role') == 5)
            <li>
                <a href="{{ url('/invitations') }}">
                    <div class="hvr-bounce-to-right">
                        <div class="icon">
                            <img src="{{ asset('img/pr.png') }}" alt="">
                        </div>
                        <span>IBs/RFQs/RFPs/REIs</span>
                    </div>
                </a>
            </li>
            <li>
                <a href="{{ url('/abstracts') }}">
                    <div class="hvr-bounce-to-right">
                        <div class="icon">
                            <img src="{{ asset('img/pr.png') }}" alt="">
                        </div>
                        <span>AOBs/AOPs/AOQs</span>
                    </div>
                </a>
            </li>
            <li>
                <a href="{{ url('/orders/all') }}">
                    <div class="hvr-bounce-to-right">
                        <div class="icon">
                            <img src="{{ asset('img/pr.png') }}" alt="">
                        </div>
                        <span>JOs/POs/Contracts</span>
                    </div>
                </a>
            </li>
            @endif
            <li class="has-submenu">
                <a href="#">
                    <div class="{{ Request::is(['user/*', 'user']) ? 'menu-active' : '' }} hvr-bounce-to-right">
                        <div class="icon">
                            <img src="{{ asset('img/pr.PNG') }}" alt="">
                        </div>
                        <span>PMR</span>
                    </div>
                </a>
                <ul class="submenu">
                    <li>
                        <a href="{{ url('/main-pmr') }}">
                            <div class="hvr-bounce-to-right">
                                <div class="icon">
                                    <img src="{{ asset('img/pr.png') }}" alt="">
                                </div>
                                <span>Main PMR</span>
                            </div>
                        </a>
                    </li>
                    <li>
                        <a href="{{ url('/personal-pmr') }}">
                            <div class="hvr-bounce-to-right">
                                <div class="icon">
                                    <img src="{{ asset('img/pr.png') }}" alt="">
                                </div>
                                <span>Personal PMRs</span>
                            </div>
                        </a>
                    </li>
                </ul>
            </li>
            @if (session()->get('user_role') == 3 || session()->get('user_role') == 1 || session()->get('user_role') == 5 || session()->get('user_role') == 8)
            <li class="has-submenu">
                <a href="{{ url('/user') }}">
                    <div class="{{ Request::is(['user/*', 'user']) ? 'menu-active' : '' }} hvr-bounce-to-right">
                        <div class="icon">
                            <img src="{{ asset('img/users.PNG') }}" alt="">
                        </div>
                        <span>Users List</span>
                    </div>
                </a>
                @if (session()->get('user_role') != 8)
                    <ul class="submenu">
                        <li>
                            <a href="{{ url('/user/create') }}">
                                <div class="hvr-bounce-to-right">
                                    <div class="icon">
                                    <img src="{{ asset('img/create.png') }}" />
                                    </div>
                                    <span>Create New User</span>
                                </div>
                            </a>
                        </li>
                    </ul>
                @endif
            </li>
            @endif
            @if (session()->get('user_role') == 3 || session()->get('user_role') == 1 || session()->get('user_role') == 5 || session()->get('user_role') == 8)
            <li class="has-submenu">
                <a href="{{ url('/supplier') }}">
                    <div class="hvr-bounce-to-right">
                        <div class="icon">
                            <img src="{{ asset('img/suppliers.PNG') }}" alt="">
                        </div>
                        <span>Suppliers List</span>
                    </div>
                </a>
                <ul class="submenu">
                    <li>
                        <a href="{{ url('/supplier/create') }}">
                            <div class="hvr-bounce-to-right">
                                <div class="icon">
                                <img src="{{ asset('img/create.png') }}" alt="">
                                </div>
                                <span>Create New Supplier</span>
                            </div>
                        </a>
                    </li>
                </ul>
            </li>
            @endif
            @if(Auth::user()->role->id == 1)
                <li class="has-submenu">
                    <a href="">
                        <div class="hvr-bounce-to-right">
                            <div class="icon">
                                <img src="{{ asset('img/settings.PNG') }}" alt="">
                            </div>
                            <span>Settings</span>
                        </div>
                    </a>
                    <ul class="submenu">
                        <li>
                            <a href="{{ url('/procurement-modes/edit') }}">
                                <div class="hvr-bounce-to-right">
                                    <div class="icon">
                                    <img src="{{ asset('img/edit.png') }}" alt="">
                                    </div>
                                    <span>Modify Procurement Modes</span>
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('/logs') }}">
                                <div class="hvr-bounce-to-right">
                                    <div class="icon">
                                        <img src="{{ asset('img/edit.png') }}" alt="">
                                    </div>
                                    <span>View Activity Logs</span>
                                </div>
                            </a>
                        </li>
                    </ul>
                </li>
            @else
            <li>
                <a href="{{ url('/logs') }}">
                    <div class="hvr-bounce-to-right">
                        <div class="icon">
                            <img src="{{ asset('img/edit.png') }}" alt="">
                        </div>
                        <span>View Activity Logs</span>
                    </div>
                </a>
            </li>
            @endif
        </ul>
        <div class="f1l_3r"></div>
    </div>