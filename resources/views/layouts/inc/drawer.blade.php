<link rel="stylesheet" href="{{ asset('css/drawer.css') }}">
            <!-- menu vertical -->
<nav id="menu" class="left sidebar" style="overflow-y: auto;">
    <ul>
        <li>
            <a href="{{ url('/dashboard') }}" class="{{ Request::is(['dashboard/*', 'dashboard']) ? 'active' : '' }}"><i class="fas fa-th"></i> Dashboard</a>
        </li>
        @if(Auth::user()->user_role === 5 || Auth::user()->user_role === 1)
            <li>
                <a href="{{ url('/announcement') }}" class="{{ Request::is(['announcement/*', 'announcement']) ? 'active' : '' }}"><i class="fas fa-bullhorn"></i>Announcements</a>
            </li>
        @endif
        <li>
            <a href="#" class="{{ Request::is(['ppmps/*', 'ppmps']) ? 'active' : '' }}"><i class="fas fa-columns"></i> PPMP <i class="fa fa-caret-down"></i></a>
            <ul>
                <li><a href="{{ url('/ppmps') }}">PPMP List</a></li>
                @if(session()->get('user_role') == 8 || Auth::user()->user_role === 1 || Auth::user()->user_role === 3)
                    <li><a href="{{ url('/ppmps/approved') }}">View All Approved PPMPs</a></li>
                @endif
                @if(session()->get('user_role') == 1 || session()->get('user_role') == 7)
                    <li><a href="{{ url('/ppmps/create') }}">Create New PPMP</a></li>
                @endif
                <li><a href="{{ url('/ppmps/moving') }}">Moving PPMP</a></li>
            </ul>
        </li>
        <li>
            <a href="#" class="{{ Request::is(['app/*', 'app']) ? 'active' : '' }}"><i class="fas fa-calendar-check"></i> APP <i class="fa fa-caret-down"></i></a>
            <ul>
                {{-- <li><a href="{{ url('/app/all') }}">APP List</a></li> --}}
                <li><a href="{{ url('/app') }}">APP List</a></li>
                <li><a href="{{ url('/app/moving') }}">Moving APP</a></li>
            </ul>
        </li>
        {{-- @if (session()->get('user_role') != 8) --}}
        <li>
            <a href="#"><i class="fas fa-credit-card"></i> Purchase Request <i class="fa fa-caret-down"></i></a>
            <ul>
                <li><a href="{{ url('/pr') }}">PR List</a></li>
                @if(count(Auth::user()->office->app) && Auth::user()->user_role === 7)
                    <li><a href="{{ url('/pr/create') }}">Create New PR</a></li>
                @endif
            </ul>
        </li>
        {{-- @endif --}}
        @if (session()->get('user_role') == 1 || session()->get('user_role') == 3 || session()->get('user_role') == 5)
            <li><a href="{{ url('/invitations') }}"><i class="fa fa-file-upload"></i>IBs/RFQs/RFPs/REIs</a></li>
            <li><a href="{{ url('/abstracts') }}"><i class="fas fa-file-invoice"></i> AOBs/AOPs/AOQs</a></li>
            <li><a href="{{ url('/orders/all') }}"><i class="fas fa-file-contract"></i> JOs/POs/Contracts</a></li>
        @endif
        <li>
            <a href="#"><i class="fas fa-sitemap"></i> PMR <i class="fa fa-caret-down"></i></a>
            <ul>
                <li><a href="{{ url('/main-pmr') }}">Main PMR</a></li>
                @php $pmr_access_array = [1,3,5,8]; @endphp
                @if(in_array(Auth::user()->user_role, $pmr_access_array))
                    <li><a href="{{ url('/uploaded-pmr') }}">Uploaded PMR</a></li>
                    <li><a href="{{ url('/personal-pmr') }}">Personal PMR</a></li>
                @endif
            </ul>
        </li>
        @if (session()->get('user_role') == 3 || session()->get('user_role') == 1 || session()->get('user_role') == 5 || session()->get('user_role') == 8)
            <li>
                <a href="#"><i class="fas fa-user-tie"></i> Users List <i class="fa fa-caret-down"></i></a>
                <ul>
                    <li><a href="{{ url('/user') }}">All Users</a></li>
                    @if (session()->get('user_role') != 8)
                        <li><a href="{{ url('/user/create') }}">Create New User</a></li>
                    @endif
                </ul>
            </li>
        @endif
        @if (session()->get('user_role') == 3 || session()->get('user_role') == 1 || session()->get('user_role') == 5 || session()->get('user_role') == 8)
            <li>
                <a href="#"><i class="fas fa-people-carry"></i> Suppliers List <i class="fa fa-caret-down"></i></a>
                <ul>
                    <li><a href="{{ url('/supplier') }}">All Suppliers</a></li>
                    <li><a href="{{ url('/supplier/create') }}">Create New Supplier</a></li>
                </ul>
            </li>
        @endif
        @if(Auth::user()->role->id == 1)
            <li>
                <a href="#"><i class="fas fa-cogs"></i> Settings <i class="fa fa-caret-down"></i></a>
                <ul>
                    <li><a href="{{ url('/procurement-modes/edit') }}">Modify Procurement Modes</a></li>
                    <li><a href="{{ url('/settings') }}">Modify Global Settings</a></li>
                    <li><a href="{{ url('/logs') }}">View Activity Logs</a></li>
                </ul>
            </li>
        @else
        <li><a href="{{ url('/logs') }}"><i class="fa fa-phone"></i>View Activity Logs</a></li>
        @endif
        {{-- <li>
            <a href="#"><i class="fa fa-laptop"></i>Servicios <i class="fa fa-caret-down"></i></a>
            <ul>
                <li><a href="#">Servicio</a></li>
                <li><a href="#">Servicio</a></li>
                <li><a href="#">Servicio</a></li>
            </ul>
        </li>
        <li><a href="#"><i class="fa fa-phone"></i>Contacto</a></li> --}}
    </ul>
</nav>
              <!-- /menu vertical -->
        
<script>
    $("#showmenu").click(function(e){
        $("#menu").toggleClass("show");
    });
    $("#menu a").click(function(event){
        if($(this).next('ul').length){
    event.preventDefault();
            $(this).next().toggle('fast');
            $(this).children('i:last-child').toggleClass('fa-caret-down fa-caret-left');
        }
    });
</script>