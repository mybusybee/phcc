<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>{{ env('APP_NAME') }}</title>
	<script src="{{ asset('js/utils/jquery.min.js') }}"></script>
	{{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> --}}
	<link rel="stylesheet" href="{{ asset('css/jquery-ui-cupertino.min.css') }}">
	<link rel="stylesheet" href="{{ asset('css/bootstrap4-wizardry.min.css') }}
	">

	<!-- FOR MDBS (files already included) --->
	{{-- <!-- Bootstrap core CSS -->
	<link href="{{ asset('css/mdbs/css/bootstrap.min.css') }}" rel="stylesheet">
	<!-- Material Design Bootstrap -->
	<link href="{{ asset('css/mdbs/css/mdb.min.css') }}" rel="stylesheet"> --}}

	<!-- FOR MATERIAL DESIGN FOR BOOTSTRAP -->
	{{-- <link rel="stylesheet" href="https://unpkg.com/bootstrap-material-design@4.1.1/dist/css/bootstrap-material-design.min.css" integrity="sha384-wXznGJNEXNG1NFsbm0ugrLFMQPWswR3lds2VeinahP8N0zJw9VWSopbjv2x7WCvX" crossorigin="anonymous"> --}}

	<link rel="stylesheet" href="{{ asset('css/multiselect.css') }}">
	<link rel="stylesheet" href="{{ asset('css/styles.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/datatables.min.css') }}"/>
	<link rel="stylesheet" href="{{ asset('css/form-validation.css') }}">
	<!-- select2.org -->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
	<script src="{{ asset('js/jquery.mask.min.js') }}"></script>
	<link rel="stylesheet" href="{{ asset('css/footer.css') }}">
	<link rel="stylesheet" href="{{ asset('css/ripple.min.css') }}">
</head>
<body>
	{{-- <div class="navbar">
		<div class="left-nav">
			<div class="menu-icon">
				<span class="line"></span>
				<span class="line"></span>
				<span class="line"></span>
			</div>
			<div class="logo">
				<img src="{{ asset('img/logo.PNG') }}" alt="">
			</div>
		</div>
		<div class="right-nav">
			<div class="profile">
				<p class="profile-name">{{ session()->get('name') }}</p>
			</div>
			<div class="hbp">
				<button class="hamburger">&#9776;</button>
				<button class="cross">&#735;</button>
				<div class="menu">
					<ul>
						<a href="#"><li>My Account</li></a>
						<a href="{{ url('/logout') }}"><li>Logout</li></a>
					</ul>
				</div> 
			</div>
		</div>
	</div> --}}

	<?php
		// var_dump('For leftover section in abstract when removing supplier after adding eligibility/ies: add new attribute or class when adding row then manually delete it using jquery onclick of remove supplier')
	?>
	<div class="body">

		@include('layouts.inc.top-nav')

		@include('layouts.inc.drawer')
				
		@include('layouts.inc.main-content')

		@include('layouts.inc.footer')

	</div>

{{-- <div class="frm" style="position:fixed; right: 0; bottom: 0; z-index: 999;" id="time-container">
	<p class="navbar-text" id="ct"></p>
</div> --}}

	<script src='{{ asset("js/slidebars.min.js") }}'></script>
	<script src="{{ asset('js/layout.js') }}"></script>
	<script src="{{ asset('js/multiselect.js') }}"></script>
	<script src="{{ asset('js/ppmp/create.js') }}"></script>
	<script type="text/javascript" src="{{ asset('js/utils/datatables.min.js') }}"></script>
	<script src="{{ asset('js/utils/jquery-ui.min.js') }}"></script>
	<script src="{{ asset('js/utils/jquery.datetimepicker.js') }}"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
	
	<!-- FOR MDBS (files already included) --> 
	{{-- <script type="text/javascript" src="{{ asset('js/mdbs/js/mdb.min.js') }}"></script> --}}

	<!-- FOR MATERIAL DESIGN FOR BOOTSTRAP -->
	{{-- <script src="https://unpkg.com/bootstrap-material-design@4.1.1/dist/js/bootstrap-material-design.js" integrity="sha384-CauSuKpEqAFajSpkdjv3z9t8E7RlpJ1UP0lKM/+NdtSarroVKu069AlsRPKkFBz9" crossorigin="anonymous"></script> --}}
	{{-- <script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script> --}}


	<!-- select2.org -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
	@stack('scripts')
</body>
</html>