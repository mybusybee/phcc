<div class="modal fade" id="importSuppliers" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {{ Form::open(['url' => url('import-suppliers'), 'method' => 'post', 'files' => true]) }}
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Import suppliers list</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-left">
                    <p style="font-style: italic; color: red;">*(csv, xlsx, xls files only)</p>
                    <p style="font-style: italic bold; color: red;">*sheet name must be 'suppliers_list' without quotes, otherwise, errors will surely occur.</p>
                    <input type="file" name="suppliers" id="" accept=".xlsx,.xls,.csv" required>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            {{ Form::close() }}
        </div>
    </div>
</div>