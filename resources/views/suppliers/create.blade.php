@extends('layouts.base')

@section('content')
    <div class="container frm">
        {{ Form::open(['url' => url('/supplier'), 'method' => 'post', 'enctype' => 'multipart/form-data']) }}
        <div class="row form-group">
            <div class="col-xl-12">
                {{ Form::label('company_name', 'Company Name: ') }}
                {{ Form::text('company_name', null, ['class' => 'form-control form-control-sm']) }}
            </div>
            <div class="col-xl-12">
                {{ Form::label('authorized_personnel', 'Authorized Personnel: ') }}
                {{ Form::text('authorized_personnel', null, ['class' => 'form-control form-control-sm']) }}
            </div>
            <div class="col-xl-12">
                {{ Form::label('address', 'Address: ') }}
                {{ Form::text('address', null, ['class' => 'form-control form-control-sm']) }}
            </div>
            <div class="col-xl-12">
                {{ Form::label('telephone', 'Telephone: ') }}
                {{ Form::text('telephone', null, ['class' => 'form-control form-control-sm']) }}
            </div>
            <div class="col-xl-12" id="emailSection">
                {{ Form::label('email', 'Email Address: ') }}
                <div class="input-group">
                    {{ Form::email('email[]', null, ['class' => 'form-control form-control-sm email-rows']) }}
                    <div class="input-group-append">
                        <button type="button" id="addNewEmail" class="btn btn-info"><i class="fa fa-plus"></i></button>
                    </div>
                </div>
            </div>
            <div class="col-xl-12">
                {{ Form::label('tin_number', 'TIN: ') }}
                {{ Form::text('tin_number', null, ['class' => 'form-control form-control-sm']) }}
            </div>
            <div class="col-xl-12">
                {{ Form::label('category', 'Category: ') }}
                {{ Form::text('category', null, ['class' => 'form-control form-control-sm']) }}
            </div>
            <div class="col-xl-12">
                {{ Form::label('mayors_permit', 'Mayor\'s Permit Expiry Date: ') }}
                {{ Form::text('mayors_permit', null, ['class' => 'form-control form-control-sm']) }}
            </div>
            <div class="col-xl-12">
                {{ Form::label('philgeps_number', 'Philgeps Number') }}
                {{ Form::text('philgeps_number', null, ['class' => 'form-control form-control-sm']) }}
            </div>
        </div>
        <div class="form-group" id="files_section">
            <h4>Upload Supplier Documents</h4><span style="font-size: 0.9em;"><i>PDF files only, maximum of 10 attachments.</i></span>
            <div class="input-group file_row">
                <input type="file" name="supplier_docs[]" class="form-control sup-docs" accept=".pdf">
                <div class="input-group-append">
                    <button class="btn btn-success add_new_file" type="button"><i class="fa fa-plus"></i> Add</button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-4">
                {{ Form::submit('Save Supplier Profile', ['class' => 'btn btn-success']) }}
            </div>
        </div>
        {{ Form::close() }}
    </div>
    <script src="{{ asset('js/supplier/create.js') }}"></script>
@endsection