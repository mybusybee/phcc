@extends('layouts.base')

@section('content')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<div class="container-fluid frm">
    <h1>Suppliers List</h1>
    <div class="col-xl-4 offset-xl-8 form-group text-right">
        @php $roles = [1, 2, 3, 5, 8]; @endphp
        @if(in_array(Auth::user()->user_role, $roles))
            <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#importSuppliers">Import data</button>
            @include('suppliers.import-suppliers-modal')
        @endif
        <a href="{{ url('/supplier/create') }}" class="btn btn-success ">Create new...</a>
    </div>
    <div class="table-responsive text-center">
        <table class="table table-striped table-hover" id="suppliers_table">
            <thead class="table-primary">
                <th>Company Name</th>
                <th>Authorized Personnel</th>
                <th>Address</th>
                <th>Telephone Number</th>
                <th>Email Address</th>
                <th>T.I.N.</th>
                <th>Mayor's Permit Expiry Date</th>
                <th>Philgeps Number</th>
                <th>Action</th>
            </thead>
            <tbody>
                @foreach ($suppliers as $supplier)
                    <tr>
                        <td>{{ $supplier->company_name }}</td>
                        <td>{{ $supplier->authorized_personnel }}</td>
                        <td>{{ $supplier->address }}</td>
                        <td>{{ $supplier->telephone }}</td>
                        <td>{{ $supplier->email }}</td>
                        <td>{{ $supplier->tin_number }}</td>
                        <td>{{ $supplier->mayors_permit }}</td>
                        <td>{{ $supplier->philgeps_number }}</td>
                        <td>
                            <a href="{{ url('/supplier/'.$supplier->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                            <a href="{{ url('/supplier/'.$supplier->id.'/edit') }}" class="btn btn-info"><i class="fa fa-edit"></i></a>
                            @if(session()->get('user_role') == 1)
                            <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#deleteSupplierModal"><i class="fa fa-trash"></i>
                            </a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="modal fade" id="deleteSupplierModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Delete Supplier</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <h3>Are you sure?</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
                    <a href="{{ url('/supplier/'.$supplier->id.'/delete') }}" class="btn btn-danger">Yes</a>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $(document).ready( function () {
        $('#suppliers_table').DataTable();
    } );
</script>
@endsection