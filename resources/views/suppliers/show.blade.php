@extends('layouts.base')

@section('content')
    <div class="container-fluid frm">
        <div class="row">
            <div class="col-xl-12">
                <strong>{{ Form::label('authorized_personnel', 'Authorized Personnel: ') }}</strong>
                <u>{{ $supplier->authorized_personnel }}</u>
            </div>
            <div class="col-xl-12">
                <strong>{{ Form::label('company_name', 'Company Name: ') }}</strong>
                <u>{{ $supplier->company_name }}</u>
            </div>
            <div class="col-xl-12">
                <strong>{{ Form::label('address', 'Address: ') }}</strong>
                <u>{{ $supplier->address }}</u>
            </div>
            <div class="col-xl-12">
                <strong>{{ Form::label('telephone', 'Telephone No.: ') }}</strong>
                <u>{{ $supplier->telephone }}</u>
            </div>
            <div class="col-xl-12">
                <strong>{{ Form::label('email', 'Email Address: ') }}</strong>
                <u>{{ $supplier->email }}</u>
            </div>
            <div class="col-xl-12">
                <strong>{{ Form::label('tin_number', 'TIN: ') }}</strong>
                <u>{{ $supplier->tin_number }}</u>
            </div>
            <div class="col-xl-12">
                <strong>{{ Form::label('tin_number', 'Mayor\'s Permit Expiry Date: ') }}</strong>
                <u>{{ $supplier->mayors_permit }}</u>
            </div>
            <div class="col-xl-12">
                <strong>{{ Form::label('tin_number', 'Philgeps Number: ') }}</strong>
                <u>{{ $supplier->philgeps_number }}</u>
            </div>
        </div>
        <div class="form-group">
            <h4>File Attachments:</h4>
            @if(count($supplier->docs))
                <div class="row">
                    <div class="col-xl-4">
                        <div class="list-group">
                            @foreach($supplier->docs as $file)
                                <a style="color: #000 !important;" href="{{ url('invitation/download/'.$file->id) }}" class="list-group-item list-group-item-action list-group-item-primary">
                                    <i class="fa fa-download"></i><span>  {{ $file->filename }}</span>
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            @else
                <div class="row">
                    <div class="col-xl-4">
                        <ul class="list-group">
                            <li style="color: #000 !important;" class="list-group-item list-group-item-danger">No file/s attached.</li>
                        </li>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection