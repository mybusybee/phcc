@extends('layouts.base')

@section('content')
    <div class="container-fluid frm">
        {{ Form::open(['url' => url('/supplier/'.$supplier->id), 'method' => 'put', 'enctype' => 'multipart/form-data']) }}
            <div class="row form-group">
                <div class="col-xl-12">
                    {{ Form::label('company_name', 'Company Name: ') }}
                    {{ Form::text('company_name', $supplier->company_name, ['class' => 'form-control form-control-sm']) }}
                </div>
                <div class="col-xl-12">
                    {{ Form::label('authorized_personnel', 'Authorized Personnel') }}
                    {{ Form::text('authorized_personnel', $supplier->authorized_personnel, ['class' => 'form-control form-control-sm']) }}
                </div>
                <div class="col-xl-12">
                    {{ Form::label('address', 'Address: ') }}
                    {{ Form::text('address' , $supplier->address, ['class' => 'form-control form-control-sm']) }}
                </div>
                <div class="col-xl-12">
                    {{ Form::label('telephone', 'Telephone No.: ') }}
                    {{ Form::text('telephone', $supplier->telephone, ['class' => 'form-control form-control-sm']) }}
                </div>
                <div class="col-xl-12">
                    {{ Form::label('tin_number', 'TIN: ') }}
                    {{ Form::text('tin_number', $supplier->tin_number, ['class' => 'form-control form-control-sm']) }}
                </div>
                <div class="col-xl-12">
                    {{ Form::label('Category', 'Category: ') }}
                    {{ Form::text('category', $supplier->category, ['class' => 'form-control form-control-sm']) }}
                </div>
                <div class="col-xl-12" id="emailSection">
                    {{ Form::label('email', 'Email Address: ') }}
                    @php $exploded_emails = explode(', ', $supplier->email) @endphp
                    @foreach($exploded_emails as $key => $email)
                        @if($key === 0)
                            <div class="input-group">
                                {{ Form::email('email[]', $email, ['class' => 'form-control form-control-sm']) }}
                                <div class="input-group-append">
                                    <button type="button" id="addNewEmail" class="btn btn-info"><i class="fa fa-plus"></i></button>
                                </div>
                            </div>
                        @else
                            <div class="input-group">
                                {{ Form::email('email[]', $email, ['class' => 'form-control form-control-sm']) }}
                                <div class="input-group-append">
                                    <button type="button" id="removeEmailRow" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
                <div class="col-xl-12">
                    {{ Form::label('mayors_permit', 'Mayor\'s Permit Expiry Date: ') }}
                    {{ Form::text('mayors_permit', $supplier->mayors_permit, ['class' => 'form-control form-control-sm']) }}
                </div>
                <div class="col-xl-12">
                    {{ Form::label('philgeps_number', 'Philgeps Number') }}
                    {{ Form::text('philgeps_number', $supplier->philgeps_number, ['class' => 'form-control form-control-sm']) }}
                </div>
            </div>
            <div class="form-group files_section">
                <h4>File Attachments:</h4>
                @if(count($supplier->docs))
                    <div class="row">
                        <div class="col-xl-4">
                            <div class="list-group">
                                @foreach($supplier->docs as $file)
                                    <a style="color: #000 !important;" href="{{ url('invitation/download/'.$file->id) }}" class="list-group-item list-group-item-action list-group-item-primary">
                                        <i class="fa fa-download"></i><span>  {{ $file->filename }}</span>
                                    </a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                @else
                    <h4>Upload Supplier Documents</h4><span style="font-size: 0.9em;"><i>PDF files only, maximum of 5 attachments.</i></span>
                    <div class="input-group file_row">
                        <input type="file" name="supplier_docs[]" class="form-control sup-docs" accept=".pdf">
                        <div class="input-group-append">
                            <button class="btn btn-success add_new_file" type="button"><i class="fa fa-plus"></i> Add</button>
                        </div>
                    </div>
                @endif
            </div>
            <div class="form-group">
                {{ Form::submit('Update', ['class' => 'btn btn-success']) }}
            </div>
        {{ Form::close() }}
    </div>
<script src="{{ asset('js/supplier/create.js') }}"></script>
@endsection