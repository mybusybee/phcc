<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="{{ asset('css/rfq-pdf.css') }}">
    <script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
</head>
<body>
    <div class="container-fluid">
        <div class="row">
            <div class="w-50">
                <img src="{{ asset('img/pcc-logo-pdf.png') }}" height="80">
            </div>
            <div class="w-50 text-right">
                <img src="{{ asset('img/pcc-contact-pdf.png') }}" height="80">
            </div>
        </div>
        <hr>
        <div class="row text-center">
            <h2>NOTICE TO PROCEED</h2>
        </div>
        <hr>
        
    </div>
</body>

</html>