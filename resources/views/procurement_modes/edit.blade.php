@extends('layouts.base')

@section('content')
{{ Form::open(['url' => url('/procurement-modes/update'), 'method' => 'put']) }}
<div class="container">
	<div class="row">
		<div class="col-xl-6">
			<h1>Edit Procurement Modes</h1>
		</div>
        <div class="col-xl-6 text-right">
            <a href="{{ asset('/procurement-modes/create') }}" class="btn btn-success"><i class="fas fa-plus"></i> Add New</a>
        </div>
    </div>
	<div class="row frm">
		<div class="table-responsive">
			<table class="table table-striped table-hover">
				<thead class="table-primary">
					<th>Mode of Procurement</th>
					<th>Minimum Value</th>
					<th>Maximum Value</th>
				</thead>
				<tbody>
					<?php foreach($modes as $mode): ?>
						<tr>
							<td class="d-none">{{ Form::hidden('id[]', $mode->id, ['class' => 'form-control']) }}</td>
							<td>{{ Form::text('mode[]', $mode->mode, ['class' => 'form-control']) }}</td>
							<td>
								{{ Form::text('min_value[]', $mode->min_value, ['class' => 'form-control']) }}
							</td>
							<td>{{ Form::text('max_value[]', $mode->max_value, ['class' => 'form-control']) }}</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>
			<div class="form-group">
				<input type="submit" value="Save" class="btn btn-success" />
			</div>
		</div>
	</div>
</div>
{{ Form::close() }}
@endsection