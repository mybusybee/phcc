@extends('layouts.base')

@section('content')
{{ Form::open(['url' => url('/procurement-modes/store'), 'method' => 'post']) }}
<div class="container">
    <h1>Add New Procurement Mode</h1>
	<div class="row frm">
		<div class="table-responsive">
			<table class="table table-striped table-hover">
				<thead class="table-primary">
					<th>Mode of Procurement</th>
					<th>Minimum Value</th>
					<th>Maximum Value</th>
				</thead>
				<tbody>
                    <tr>
                        <td>{{ Form::text('mode', null, ['class' => 'form-control', 'required']) }}</td>
                        <td>
                            {{ Form::text('min_value', null, ['class' => 'form-control value']) }}
                        </td>
                        <td>{{ Form::text('max_value', null, ['class' => 'form-control value']) }}</td>
                    </tr>
				</tbody>
			</table>
			<div class="form-group">
				<input type="submit" value="Save" class="btn btn-success" />
			</div>
		</div>
	</div>
</div>
{{ Form::close() }}
<script>
    $('.value').mask('#############');
</script>
@endsection