@extends('layouts.base')

@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/datatables.min.css') }}"/>
<link rel="stylesheet" href="{{ asset('css/abstract/table.css') }}">
<link rel="stylesheet" href="{{ asset('css/form-validation.css') }}">
<div id="aobForm" class="frm">
    <input type="hidden" ref="abstract_parent_id" value="{{ $abstract_form->abstract_parent->id }}">
    {{ Form::open(['url' => url('/aoq/abstract/'.$abstract_form->abstract_parent->id), 'method' => 'put', 'class' => 'abstract-form']) }}
        <div class="row">
            <div class="col-xl-12">
                <h4 class="text-center">ABSTRACT OF QUOTATION</h4>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-xl-1">
                <p>PR No.:</p>
            </div>
            <div class="col-xl-3">
                {{ Form::text('', $abstract_form->pr->pr_no, ['class' => 'form-control form-control-sm', 'readonly']) }}
            </div>
            <div class="col-xl-1 offset-xl-4">
                <p class="text-right">AOQ No.:</p>
            </div>
        
            <div class="col-xl-3">
                {{ Form::text('aoq_no', $abstract_form->abstract_parent->form_no, ['class' => 'form-control form-control-sm border border-info', 'readonly']) }}
            </div>
        </div>
        <div class="row mt-1">
            <div class="col-xl-1">
                <p>PR Date:</p>
            </div>
            <div class="col-xl-3">
                {{ Form::text('', $abstract_form->pr->pr_prep_date, ['class' => 'form-control form-control-sm', 'readonly']) }}
            </div>
            <div class="col-xl-2 offset-xl-3">
                <p class="text-right">Opening Date:</p>
            </div>
            <div class="col-xl-3">
                {{ Form::text('opening_date', $abstract_form->opening_date, ['class' => 'form-control form-control-sm border border-info', 'id' => 'openingDate', 'required']) }}
            </div>
        </div>
        <div class="row mt-1">
            <div class="col-xl-1">
                <p>ABC:</p>
            </div>
            <div class="col-xl-3">
                {{ Form::text('', '&#8369;'.$abstract_form->pr->pr_item_total->total_estimate, ['class' => 'form-control form-control-sm', 'readonly']) }}
            </div>
            <div class="col-xl-2 offset-xl-3">
                <p class="text-right">Mode of Procurement:</p>
            </div>
            <div class="col-xl-3">
                {{ Form::select('proc_mode_type', $proc_modes, $abstract_form->procurement_mode_id, ['class' => 'form-control form-control-sm border border-info'])}}
            </div>
        </div>
        @include('abstract.table-edit')
        <div class="form-group">
            <input type="submit" value="Update" class="btn btn-success">
        </div>
    {{ Form::close() }}
    @include('abstract/new-supplier-modal')
    @include('abstract/new-registered-supplier-modal')
</div>
<script type="text/javascript" src="{{ asset('js/utils/datatables.min.js') }}"></script>
<script src="{{ asset('js/aoq/create.js') }}"></script>
<script src="{{ asset('js/aob/add-reco-row.js') }}"></script>
<script src="{{ asset('js/aob/remove-reco-row.js') }}"></script>
<script src="{{ asset('js/aob/remove-doc-row.js') }}"></script>
<script src="{{ asset('js/abstracts/abstract-column-edit.js') }}"></script>
<script src="{{ asset('js/abstracts/total-cost-calculator.js') }}"></script>
<script src="{{ asset('js/abstracts/sub-total-calculator.js') }}"></script>
<script src="{{ asset('js/abstracts/filter-per-item-supplier-by-bid.js') }}"></script>
@endsection