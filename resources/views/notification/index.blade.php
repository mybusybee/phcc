@extends('layouts.base')

@section('content')
    <div class="frm col-xl-6 mx-auto">
        <h2>Recent Notifications</h2>
        <div class="row">
            <div class="col-xl-12">
                <div class="list-group list-group-flush">
                    @if(count($notifications))
                        @foreach ($notifications as $notification)
                            <a href="{{ $notification->link }}" class="list-group-item list-group-item-action {{ $notification->is_read === 0 ? 'list-group-item-secondary' : null }} text-dark">
                                <span class="badge badge-secondary badge-pill">{{ $notification->created_at->diffForHumans() }}</span>
                                {{ $notification->message }}
                            </a>
                        @endforeach
                    @else
                        <a href="#" class="list-group-item list-group-item-action list-group-item-danger text-dark">
                            No notification yet.
                        </a>
                    @endif
                </div>
            </div>
        </div>
        {{ $notifications->links() }}
    </div>
@endsection