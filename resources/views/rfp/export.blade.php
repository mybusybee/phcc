<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/pmrtable.css') }}">
</head>
<body>
    <br><br><br><br><br><br>
    <table>
        <tr>
            <th colspan="14" style="text-align: center">REQUEST FOR PROPOSAL</th>
        </tr>
        <tr></tr>
        <tr>
            <th style="">P.R. No/Date:</th>
            <th colspan="5" style="">{{ $rfp->pr->pr_no }} / {{ $rfp->pr->pr_prep_date }}</th>
            <td colspan="4"></td>
            <th colspan="2" style="text-align: right;">RFP No./Date:</th>
            <th colspan="2">{{ $rfp->invite->form_no }} / {{ $rfp->rfp_prep_date }}</th>
        </tr>
        <tr></tr>
        <tr><th colspan="14">Sir / Madam:</th></tr>
    </table>
    <table>
        <tr>
            <td colspan="14" style="wrap-text: true; height: 46.50;">Please quote your lowest price/s for the item/s listed below including the total amount in legible style (preferably typewritten) and return this duly signed by the company's authorized signatory/ies in a <span style="font-weight: bold;">{{ $rfp->submission_type }}</span> to the PCC Bids and Awards Committee c/o <span style="font-weight: bold;">{{ $rfp->rfp_receiver }}</span> 25/F of the above address or email to <span style="font-weight: bold;">procurement@phcc.gov.ph</span> on or before {{ $rfp->submission_date_time }}</td>
        </tr>
        <tr>
            <td colspan="14">Your participation to this bidding shall be subject to the requirements as identified below:</td>
        </tr>
    </table>
    <table>
        <tr>
            <td></td>
            <td style=" border: 1px solid #000; text-align: center;">{{ $rfp->invite->requirement->philgeps_reg_no === 1 ? 'x' : ''}}</td>
            <td colspan="12">PhilGEPS Registration Number.</td>
        </tr>
        <tr style="height: 4.25;"></tr>
        <tr>
            <td></td>
            <td style=" border: 1px solid #000; text-align: center;">{{ $rfp->invite->requirement->mayors_business_permit_bir_cor === 1 ? 'x' : ''}}</td>
            <td colspan="12">Mayor's/Business Permit / BIR Certificate of Registration incase of individual or equivalent.</td>
        </tr>
        <tr style="height: 4.25;"></tr>
        <tr>
            <td></td>
            <td style=" border: 1px solid #000; text-align: center;">{{ $rfp->invite->requirement->income_business_tax_return === 1 ? 'x' : ''}}</td>
            <td colspan="12">Latest income/Business Tax Return or equivalent.</td>
        </tr>
        <tr style="height: 4.25;"></tr>
        <tr>
            <td></td>
            <td style=" border: 1px solid #000; text-align: center;">{{ $rfp->invite->requirement->professional_license_cv === 1 ? 'x' : ''}}</td>
            <td colspan="12">Professional License / Curriculum Vitae <i>(for Consulting Services)</i>.</td>
        </tr>
        <tr style="height: 4.25;"></tr>
        <tr>
            <td></td>
            <td style=" border: 1px solid #000; text-align: center;">{{ $rfp->invite->requirement->omnibus_sworn_statement === 1 ? 'x' : ''}}</td>
            <td colspan="12">Notarized Omnibus Sworn Statement.</td>
        </tr>
        <tr style="height: 4.25;"></tr>
        <tr>
            <td></td>
            <td style=" border: 1px solid #000; text-align: center;">{{ $rfp->invite->requirement->signed_terms_of_reference === 1 ? 'x' : ''}}</td>
            <td colspan="12">Signed Terms of Reference or Conformed Technical Specifications.</td>
        </tr>
        <tr style="height: 4.25;"></tr>
        <tr>
            <td></td>
            <td style=" border: 1px solid #000; text-align: center;">{{ $rfp->invite->requirement->govt_tax_inclusive === 1 ? 'x' : ''}}</td>
            <td colspan="12">Quotation must be inclusive of all applicable governemnt taxes and subject to 5% R-VAT and 1% (PO) or 2% (JO) deductions.</td>
        </tr>
        <tr style="height: 4.25;"></tr>
        <tr>
            <td></td>
            <td style=" border: 1px solid #000; text-align: center;">{{ $rfp->invite->requirement->used_form === 1 ? 'x' : ''}}</td>
            <td colspan="12">Bids must be submitted using this form. Supplemental information using your company astationery may be attached to</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td colspan="12">reflect the complete specification of bid e.g., brand name, model, pictures/brochures/literature, menu, etc.</td>
        </tr>
        <tr style="height: 4.25;"></tr>
        <tr>
            <td></td>
            <td style=" border: 1px solid #000; text-align: center;">{{ $rfp->invite->requirement->not_exceeding_abc === 1 ? 'x' : ''}}</td>
            <td colspan="10">Bids should not exceed the Approved Budget for the Contract (ABC) in the total amount of</td>
            <th colspan="2"><i><u>PhP {{ $rfp->invite->requirement->abc_amount }}</u></i></th>
        </tr>
        <tr style="height: 4.25;"></tr>
        <tr>
            <td></td>
            <td style=" border: 1px solid #000; text-align: center;">{{ $rfp->invite->requirement->award_by_lot === 1 ? 'x' : ''}}</td>
            <td colspan="12">Award shall be made by lot (please bid for all items to avoid disqualification of bid) or</td>
        </tr>
        <tr style="height: 4.25;"></tr>
        <tr>
            <td></td>
            <td style=" border: 1px solid #000; text-align: center;">{{ $rfp->invite->requirement->award_by_line_item === 1 ? 'x' : ''}}</td>
            <td colspan="12">Award shall be made by line item.</td>
        </tr>
        <tr style="height: 4.25;"></tr>
        <tr>
            <td></td>
            <td style=" border: 1px solid #000; text-align: center;">{{ $rfp->invite->requirement->one_month_validitiy === 1 ? 'x' : ''}}</td>
            <td colspan="12">Bids should be valid for a <strong>minimum of one (1) month</strong> from deadline of submission of bids as indicated above</td>
        </tr>
        <tr style="height: 4.25;"></tr>
        <tr>
            <td></td>
            <td style=" border: 1px solid #000; text-align: center;">{{ $rfp->invite->requirement->delivered_to_pcc === 1 ? 'x' : ''}}</td>
            <td colspan="12">Procured items shall be delivered to {{ $rfp->invite->requirement->delivery_place }}</td>
        </tr>
        <tr style="height: 4.25;"></tr>
        <tr>
            <td></td>
            <td style=" border: 1px solid #000; text-align: center;">x</td>
            <td colspan="12">Payment Terms: <strong><u><i>{{ $rfp->invite->requirement->payment_terms }}</i></u></strong></td>
        </tr>
        <tr style="height: 4.25;"></tr>
        <tr>
            <td></td>
            <td style=" border: 1px solid #000; text-align: center;">{{ $rfp->invite->requirement->signatory_refusal === 1 ? 'x' : ''}}</td>
            <td colspan="12">Refusal to sign and accept an Award/Purchase Order/Job Order or enter into contract without justifiable reason, maybe</td>
        </tr>
        <tr>
            <td></td>
            <td></td>
            <td colspan="12">ground for imposition of administrative sanctions under RUle XXIII of the Revised IRR of RA 9184.</td>
        </tr>
    </table>
    <table>
        <tr>
            <td style="wrap-text: true; height: 62" colspan="14">In case you do not receive any communication from PCC <u> {{ $rfp->not_awarded_notification }}</u> days/months from the deadline indicated above, it will mean that the award was not made in your favor. With the end in view of obtaining the contract most advantageous to the government, PCC reserves the right to: 1) accept or reject any or all the quotations/bids; 2) award the contract on a per item/lot basis; and 3) to annul the bidding process and to reject all quotations/bids at any time prior to contract award, without thereby incurring any liability to the affected bidder or bidders.</td>
        </tr>
        <tr>
            <td colspan="12">For clarifications, please contact PCC Bids and Awards Committee in the above address/telephone numbers.</td>
        </tr>
        <tr></tr>
        <tr>
            <td>Very truly yours,</td>
        </tr>
        <tr>
            <td><i>{{ $rfp->signed === 1 ? 'Sgd.' : '' }}</i></td>
        </tr>
        <tr>
            <td colspan="12">{{ $rfp->rfp_sender }}</td>
        </tr>
        <tr>
            <td colspan="12">{{ $rfp->rfp_sender_designation }}</td>
        </tr>
    </table>
    <table class="custom-table-bordered">
        <thead>
            <tr>
                <td colspan="11"></td>
                <th>BRAND/MODEL/OTHERS</th>
                <th>UNIT PRICE</th>
                <th>TOTAL (PhP)</th>
            </tr>
            <tr>
                <th>Item No.</th>
                <th colspan="2">QTY</th>
                <th colspan="2">UNIT</th>
                <th colspan="6">ITEM/DESCRIPTION</th>
                <th colspan="2">(To be filled-up by the supplier)</th>
                <th>TOTAL (PhP)</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($rfp->pr->pr_items as $item)
                <tr>
                    <td>{{ $item->item_no }}</td>
                    <td colspan="2">{{ $item->quantity }}</td>
                    <td colspan="2">{{ $item->unit }}</td>
                    <td colspan="6">{{ $item->description }}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                @foreach($item->pr_sub_items as $sub)
                    <tr>
                        <td>{{ $sub->item_no }}</td>
                        <td colspan="2">{{ $sub->quantity }}</td>
                        <td colspan="2">{{ $sub->unit }}</td>
                        <td colspan="6">{{ $sub->description }}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                @endforeach
            @endforeach
            <tr>
                <td></td>
                <td colspan="2"></td>
                <td colspan="2"></td>
                <td colspan="6"></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            @if($rfp->notes !== null)
                <tr>
                    <td></td>
                    <td colspan="2"></td>
                    <td colspan="2"></td>
                    <td colspan="6" style="wrap-text: true;">{{ $rfp->notes }}</td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            @endif
            <tr>
                <td></td>
                <td colspan="2"></td>
                <td colspan="2"></td>
                <td colspan="6"></td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="10" style="border-bottom: 0 !important; text-align: left;"><i>This procurement shall be subject to the salient provisions of the IRR of RA 9184 - Liquidated Damages - Section 68.</i></td>
                <td></td>
                <td></td>
                <th></th>
                <td></td>
            </tr>
            <tr>
                <td colspan="10" style="border-top: 0 !important;"><i></i></td>
                <td></td>
                <td></td>
                <th>TOTAL:</th>
                <td></td>
            </tr>
        </tbody>
    </table>
    <table>
        <tr>
            <td colspan="12" style="text-align: right;">(Bidders, please provide complete information below)</td>
        </tr>
    </table>
    <table>
        <tr>
            <td colspan="10" style="text-align: right;">Signature:</td>
            <td></td>
            <td colspan="3" style="border-bottom: 1px solid #000"></td>
        </tr>
        <tr>
            <td colspan="10" style="text-align: right;">Name/Designation:</td>
            <td></td>
            <td colspan="3" style="border-bottom: 1px solid #000"></td>
        </tr>
        <tr>
            <td colspan="10" style="text-align: right;">Name of Company:</td>
            <td></td>
            <td colspan="3" style="border-bottom: 1px solid #000"></td>
        </tr>
        <tr>
            <td colspan="10" style="text-align: right;">Address:</td>
            <td></td>
            <td colspan="3" style="border-bottom: 1px solid #000"></td>
        </tr>
        <tr>
            <td colspan="10" style="text-align: right;">Telephone/Fax:</td>
            <td></td>
            <td colspan="3" style="border-bottom: 1px solid #000"></td>
        </tr>
        <tr>
            <td colspan="10" style="text-align: right;">E-mail Address:</td>
            <td></td>
            <td colspan="3" style="border-bottom: 1px solid #000"></td>
        </tr>
        <tr>
            <td colspan="10" style="text-align: right;">TIN:</td>
            <td></td>
            <td colspan="3" style="border-bottom: 1px solid #000"></td>
        </tr>
    </table>
    <table>
        <tr>
            <td colspan="12"><i>Generated as of {{ $rfp->dateToday }}</i></td>
        </tr>
    </table>
</body>
</html>