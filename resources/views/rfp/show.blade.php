@extends('layouts.base')

@section('content')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    @if ($rfp->invite->status == 'FOR_REVISION' || ($rfp->invite->status == 'FOR_REVIEW/APPROVAL' && ($rfp->invite->rejects_remarks)))
    <div class="alert alert-info">
        {{$rfp->invite->rejects_remarks}}
    </div>
    @endif
    <div class="container-fluid frm">
        <div class="row">
            <div class="col-xl-12 text-center">
                <h2>REQUEST FOR PROPOSAL</h2>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-xl-6">
                <strong>{{ Form::label('prnodate', 'PR No. / Date: ') }}</strong>
                <div class="row">
                    <div class="col-xl-4">
                        {{ Form::text('pr_no', $rfp->pr->pr_no, ['class' => 'form-control form-control-sm', 'readonly']) }}
                    </div>
                    <div class="col-xl-4">
                        {{ Form::text('pr_prep_date', $rfp->pr->pr_prep_date, ['class' => 'form-control form-control-sm', 'readonly']) }}
                    </div>
                </div>
            </div>
            <div class="col-xl-6">
                <strong>{{ Form::label('rfpnodate', 'RFP No. / Date: ') }}</strong>
                <div class="row">
                    <div class="col-xl-4">
                        {{ Form::text('rfp_no', $rfp->invite->form_no, ['class' => 'form-control form-control-sm', 'readonly']) }}
                    </div>
                    <div class="col-xl-4">
                        {{ Form::text('rfp_prep_date', $rfp->rfp_prep_date, ['class' => 'form-control form-control-sm', 'readonly']) }}
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-xl-12">
                <strong><p>Sir / Madam:</p></strong>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12">
                <span>Please quote your lowest price/s for the item/s listed below including the total amount in legible style (preferably typewritten) and return this duly signed by the company's authorized signatory/ies in a {{ Form::text('', $rfp->submission_type, ['class' => 'form-control form-control-sm d-inline-block w-25', 'readonly']) }} to the General Services Division, Administrative Office c/o {{ Form::text('', $rfp->rfp_receiver, ['class' => 'form-control form-control-sm d-inline-block w-25', 'readonly']) }}, 25/F of the above address or e-mail to <i>procurement@phcc.gov.ph</i> on or before {{ Form::text('submission_date_from', $rfp->submission_date_time, ['class' => 'form-control form-control-sm d-inline-block w-25', 'readonly']) }}</span>
            </div>
        </div>
        <hr>
        <div class="form-group">
            <div class="col-xl-12">
                <p>Your participation to this bidding shall be subjected to the requirements as identified below:</p>
            </div>
            <div class="col-xl-12">
                <ul class="rfq-list list-group">
                    <li class="list-group-item">
                        @if ($rfp->invite->requirement->philgeps_reg_no == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        PhilGEPS Registration Number
                    </li>
                    <li class="list-group-item">
                        @if ($rfp->invite->requirement->mayors_business_permit_bir_cor == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        Mayor's Business Permit / BIR Certificate of Registration in case of individual
                    </li>
                    <li class="list-group-item">
                        @if ($rfp->invite->requirement->income_business_tax_return == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        Latest Income / Business Tax Return
                    </li>
                    <li class="list-group-item">
                        @if ($rfp->invite->requirement->professional_license_cv == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        Professional License / Curriculum Vitae (for Consulting Services)
                    </li>
                    <li class="list-group-item">
                        @if ($rfp->invite->requirement->omnibus_sworn_statement == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        Omnibus Sworn Statement
                    </li>
                    <li class="list-group-item">
                        @if ($rfp->invite->requirement->signed_terms_of_reference == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        Signed Terms of Reference
                    </li>
                    <li class="list-group-item">
                        @if ($rfp->invite->requirement->govt_tax_inclusive == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        Proposal must be inclusive of all applicable government taxes and subject to 5% R-VAT and 1% (PO) or 2% (JO) deductions.
                    </li>
                    <li class="list-group-item">
                        @if ($rfp->invite->requirement->used_form == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        Bids must be submitted using this form. Supplemental information using your company stationery may be attached to reflect the complete specification of bid e.g. brand name, model, pictures / brochures / literature, menu, etc.
                    </li>
                    <li class="list-group-item">
                        @if ($rfp->invite->requirement->not_exceeding_abc == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        Bids should not exceed the Approved Budget for the Contract (ABC) in the total amount of </label> {{ Form::text('abc_amount', $rfp->invite->requirement->abc_amount, ['class' => 'form-control form-control-sm d-inline-block w-25', 'readonly']) }}
                    </li>
                    <li class="list-group-item">
                        @if ($rfp->invite->requirement->award_by_lot == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        Award shall be made by lot(please bid for all items to avoid disqualification of bid) or
                    </li>
                    <li class="list-group-item">
                        @if ($rfp->invite->requirement->award_by_line_item == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        Award shall be made by line item
                    </li>
                    <li class="list-group-item">
                        @if ($rfp->invite->requirement->one_month_validity == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        Bids should be valid for a <strong>minimum of one (1) month</strong> from deadline of submission of bids as indicated above.
                    </li>
                    <li class="list-group-item">
                        @if ($rfp->invite->requirement->delivered_to_pcc == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        Procured items shall be delivered to <strong>{{ $rfp->invite->requirement->delivery_place }}</strong>.
                    </li>
                    <li class="list-group-item">
                        <i class="fa fa-check-circle" style="color: green;"></i>
                        Payment Terms: {{ Form::text('payment_terms', $rfp->invite->requirement->payment_terms, ['class' => 'form-control form-control-sm w-25 d-inline-block', 'readonly']) }}
                    </li>
                    <li class="list-group-item">
                        @if ($rfp->invite->requirement->signatory_refusal == 1)
                            <i class="fa fa-check-circle" style="color: green;"></i>
                        @else
                            <i class="fas fa-times-circle" style="color: red;"></i>
                        @endif
                        Refusal to sign and accept an Award / Purchase Order / Job Order or enter into contract without justifiable reason, maybe ground for imposition of administrative sanctions under Rule XXIII of the Revised IRR of RA 9184.
                    </li>
                    <li class="list-group-item">
                        <label for="others"><strong>Others:</strong></label>
                        {{ Form::text('others', $rfp->invite->requirement->others, ['class' => 'form-control form-control-sm', 'readonly']) }}
                    </li>
                </ul>
            </div>
            <hr>
            <div class="col-xl-12">
                <p>In case you do not receive any communication from PCC {{ Form::text('not_awarded', $rfp->not_awarded_notification, ['class' => 'form-control form-control-sm w-25 d-inline-block', 'readonly']) }} days / months from the deadline indicated above, it will mean that the award was not made in your favor. With the end in view of obtaining the contract most advantageous to the government, PCC reserves the right to: 1.) accept or reject any or all the quotations / bids; 2.) award the contract on a per item / lot basis; and 3.) to annul the bidding process and to reject all quotations / bids at any time prior to contract award, without thereby incurring any liability to the affected bidder or bidders. Tie bids shall be resolved by draw lots.</p>
                <p>For clarifications, please contact PCC Bids and Awards Committee in the above address/telephone numbers.</p>
            </div>
            <hr>
        </div>
        <div class="form-group">
            @if(Auth::user()->user_role === 5 && $rfp->invite->status === 'FOR_EXPORT' && $rfp->signed === 0)
                <div class="col-xl-4">
                    <a href="{{ url('rfp/signed/'.$rfp->id) }}" class="btn btn-info">Signed</a>
                </div>
            @elseif(Auth::user()->user_role === 5 && $rfp->invite->status === 'FOR_EXPORT' && $rfp->signed === 1)
                <div class="col-xl-4">
                    <button class="btn btn-warning" type="button" disabled>Signed</button>
                </div>
            @endif
            <div class="col-xl-12">
                <p>Very truly yours,</p>
            </div>
            <div class="col-xl-3 mt-5">
                {{ Form::text('', $rfp->rfp_sender, ['class' => 'form-control form-control-sm', 'readonly']) }}
            </div>
            <div class="col-xl-3">
                {{ Form::text('', $rfp->rfp_sender_designation, ['class' => 'form-control form-control-sm', 'readonly']) }}
            </div>
        </div>
        <hr>
        <div class="table-responsive text-center">
            <table class="table table-hover table-striped">
                <thead class="table-primary">
                    <tr>
                        <th colspan="4"></th>
                        <th>BRAND / MODEL</th>
                        <th>UNIT PRICE</th>
                        <th></th>
                    </tr>
                    <tr>
                        <th>Item No.</th>
                        <th>QTY</th>
                        <th>Unit</th>
                        <th>Item Description</th>
                        {{-- <th>Total Budget per line item</th> --}}
                        <th colspan="2">(To be filled up by the supplier)</th>
                        <th>Total (PHP)</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rfp->pr->pr_items as $item)
                    <tr>
                        <td>{{ $item->item_no }}</td>
                        <td>{{ $item->quantity }}</td>
                        <td>{{ $item->unit }}</td>
                        <?php if($item->specs == null): ?>
                            <td>{{ $item->description }}</td>
                        <?php else: ?>
                            <td>{{ $item->description }} - ({{ $item->specs  }}) </td>
                        <?php endif; ?>
                        {{-- <td>{{ $item->est_cost_total }}</td> --}}
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="form-group">
            <h4>File Attachments:</h4>
            @if(count($rfp->invite->files))
                <div class="row">
                    <div class="col-xl-4">
                        <div class="list-group">
                            @foreach($rfp->invite->files as $file)
                                <a style="color: #000 !important;" href="{{ url('invitation/download/'.$file->id) }}" class="list-group-item list-group-item-action list-group-item-primary">
                                    <i class="fa fa-download"></i><span>  {{ $file->filename }}</span>
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            @else
                <div class="row">
                    <div class="col-xl-4">
                        <ul class="list-group">
                            <li style="color: #000 !important;" class="list-group-item list-group-item-danger">No file/s attached.</li>
                        </li>
                    </div>
                </div>
            @endif
        </div>
        @if($rfp->notes != null)
            <div class="row">
                <div class="col-xl-12 form-group">
                    <label for="">Notes:</label>
                    {{ Form::textarea('notes', $rfp->notes, ['class' => 'form-control', 'readonly']) }}
                </div>
            </div>
        @endif
        @if ($rfp->pr->personal_pmr_item->status == 'Cancelled' || $rfp->pr->personal_pmr_item->status == 'Failed')
        
        @else
            @if(Auth::user()->user_role === 3 && $rfp->invite->status === 'FOR_REVIEW/APPROVAL')
                <div class="form-group">
                    <a href="{{ url('/invitations/approve/'.$rfp->invite->id) }}" class="btn btn-success">Approve</a>
                    <button class="btn btn-danger" data-toggle="modal" data-target="#rejectRFPModal">Reject</button>
                    <button class="btn btn-danger" data-toggle="modal" data-target="#cancelRFPModal">Cancel RFP</button>
                </div>
                <div class="modal fade" id="rejectRFPModal" tabindex="-1" role="dialog" aria-hidden="true">
                    {{ Form::open(['url' => url('/invitations/reject/'.$rfp->invite->id), 'method' => 'put']) }}
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Reason for rejection</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-md-12">
                                                {{ Form::textarea('reject_remarks', null, ['class' => 'form-control']) }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
            @elseif(Auth::user()->user_role === 5 && $rfp->invite->status === 'FOR_EXPORT')
                <div class="col-xl-4">
                    <a href="{{ url('/export/xls/rfp/'.$rfp->id) }}" class="btn btn-warning">Export</a>
                </div>
                {{ Form::open(['url' => '/invite-suppliers' , 'method' => 'post', 'style' => 'width: 100%;']) }}
                    <div class="table-responsive form-group" id="suppliersContainer">
                        <h5>Supplier List:</h5>
                        <table id="suppliers-datatable" class="table">
                            <thead class="table-primary">
                                <tr>
                                    <th><i class="fa fa-check-circle"></i></th>
                                    <th>Company Name</th>
                                    <th>Authorized Personnel</th>
                                    <th>Category</th>
                                    <th>Address</th>
                                    <th>Telephone</th>
                                    <th>Email</th>
                                    <th>TIN</th>
                                    <th>Mayor's Permit Expiry Date</th>
                                    <th>Philgeps Number</th>
                                </tr>
                            </thead>
                            <tbody>
                                <div id="supplierList">
                                    @foreach($suppliers as $supplier)
                                        @if(in_array($supplier->id, $selected_suppliers))
                                            <input type="hidden" name="selectedSuppliers[]" class="form-control form-control-sm d-inline-block w-25 supplier-id-class" value="{{ $supplier->id }}">
                                        @endif
                                    @endforeach
                                </div>
                                @foreach ($suppliers as $supplier)
                                    <tr>
                                        <td>
                                            @if (in_array($supplier->id, $selected_suppliers))
                                                <input type="checkbox" name="" id="" class="sup_id" value="{{ $supplier->id }}" checked>
                                            @else
                                            <input type="checkbox" name="" id="" class="sup_id" value="{{ $supplier->id }}">
                                            @endif
                                        </td>
                                        <td>{{ $supplier->company_name }}</td>
                                        <td>{{ $supplier->authorized_personnel }}</td>
                                        <td>{{ $supplier->category }}</td>
                                        <td>{{ $supplier->address }}</td>
                                        <td>{{ $supplier->telephone }}</td>
                                        <td>{{ $supplier->email }}</td>
                                        <td>{{ $supplier->tin_number }}</td>
                                        <td>{{ $supplier->mayors_permit }}</td>
                                        <td>{{ $supplier->philgeps_number }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row form-group">
                        <div class="col-xl-3">
                            <label for="preselect"><input required type="radio" id="preselect" value="preselect" name="submitType">Preselect</label>
                        </div>
                        <div class="col-xl-3">
                            <label for="sendEmails"><input required type="radio" id="sendEmails" value="sendEmails" name="submitType">Send Emails</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success submit" type="submit" id="submitBtn" disabled>Save/Send</button>
                        <button class="btn btn-danger" data-toggle="modal" data-target="#cancelRFPModal">Cancel RFP</button>
                    </div>
                    <input type="hidden" name="invitation_id" value="{{ $rfp->invite->id }}">
                {{ Form::close() }}
            @elseif ($rfp->invite->status === 'FARMED_OUT')
                <div class="col-xl-12">
                    <h1>Selected Suppliers</h1>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead class="table-primary">
                            <tr>
                                <th>Company Name</th>
                                <th>Authorized Personnel</th>
                                <th>Category</th>
                                <th>Address</th>
                                <th>Telephone</th>
                                <th>Email</th>
                                <th>TIN</th>
                                <th>Mayor's Permit</th>
                                <th>Philgeps Number</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($rfp->invite->suppliers as $supplier)
                                <tr>
                                    <td>{{ $supplier->company_name }}</td>
                                    <td>{{ $supplier->authorized_personnel }}</td>
                                    <td>{{ $supplier->category }}</td>
                                    <td>{{ $supplier->address }}</td>
                                    <td>{{ $supplier->telephone }}</td>
                                    <td>{{ $supplier->email }}</td>
                                    <td>{{ $supplier->tin_number }}</td>
                                    <td>{{ $supplier->mayors_permit }}</td>
                                    <td>{{ $supplier->philgeps_number }}</td>
                                    @if($supplier->pivot->email_status === 'EMAIL_SENT')
                                        <td>{{ $supplier->pivot->email_status }} {{date('M d, Y h:i:s a', strtotime($rfp->invite->updated_at))}}</td>
                                    @else
                                        <td><a href="#" class="btn btn-warning">Send Email</a></td>
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @elseif ($rfp->invite->status !== 'CANCELLED')
                <div class="form-group">
                    <button class="btn btn-danger" data-toggle="modal" data-target="#cancelRFPModal">Cancel RFP</button>
                </div>
            @endif

        @endif
    </div>
    <div class="modal fade" id="cancelRFPModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Cancel RFP</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <h3>Are you sure?</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
                    <a href="{{ url('/invitations/cancel/'.$rfp->invite->id) }}" class="btn btn-danger">Yes</a>
                </div>
            </div>
        </div>
    </div>
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('js/rfp/show.js') }}"></script>
@endsection