<div class="modal fade" id="importPrModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Upload Purchase Request</h5>
                <button type="button"class="close" data-dismiss="modal" >
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            {{ Form::open(['url' => url('/import/pr'), 'method' => 'post', 'files' => true]) }}
                <div class="modal-body text-left">
                    {{-- <div class="form-group">
                        <label for="pr_no">PR No.:</label>
                        <input type="text" name="pr_no" id="pr_no" required class="form-control form-control-sm" placeholder="e.g. 201903-0001">
                        <span class="small"><em>Dash is added automatically.</em></span>
                    </div>
                    <div class="form-group">
                        <label for="date">Date:</label>
                        <input type="text" name="date" class="form-control form-control-sm" id="date" required placeholder="Date">
                    </div>
                    <div class="form-group">
                        <label for="office">Office:</label>
                        <select name="office" id="" class="form-control form-control-sm" required>
                            <option value="" disabled selected>Please select one...</option>
                            @foreach ($offices as $office)
                                <option value="{{ $office->id }}">{{ $office->office }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="rcc">Responsibility Center Code:</label>
                        <select name="rcc" id="" class="form-control form-control-sm" required>
                            <option value="" disabled selected>Please select one...</option>
                            @foreach ($roles as $role)
                                <option value="{{ $role->id }}">{{ $role->user_role }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="purchase_type">Purchase Type:</label>
                        <select name="purchase_type" id="" class="form-control form-control-sm" required>
                            <option value="" selected disabled>Please select one...</option>
                            <option value="lot">Lot Purchase</option>
                            <option value="per">Per Item Purchase</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="purpose">Purpose:</label>
                        <input type="text" required placeholder="Purpose" name="purpose" class="form-control form-control-sm">
                    </div>
                    <div class="form-group" style="border: 1px solid #cecece; padding-top: 9px;">
                        <input type="radio" name="in_app" id="1_in_app" value="1" required>
                        <label for="1_in_app">Included in the APP</label>
                        <input type="radio" name="in_app" id="0_in_app" value="0">
                        <label for="0_in_app">Not included in the APP</label>
                    </div>
                    <div class="form-group" style="border: 1px solid #cecece; padding-top: 9px;">
                        <input type="radio" name="in_ps_dbm" required id="1_dbm" value="1">
                        <label for="1_dbm">Available in PS DBM</label>
                        <input type="radio" name="in_ps_dbm" id="0_dbm" value="0">
                        <label for="0_dbm">Not available in PS DBM</label>
                    </div>
                    <div class="form-group">
                        <label for="aa_no">AA#:</label>
                        <input type="text" class="form-control form-control-sm" required name="aa_no" placeholder="AA#" id="aa_no">
                    </div>
                    <div class="form-group">
                        <label for="valid_from">Valid From:</label>
                        <input type="text" class="form-control form-control-sm" placeholder="Date" required name="valid_from" id="valid_from">
                    </div>
                    <div class="form-group">
                        <label for="valid_to">Valid Until:</label>
                        <input type="text" class="form-control form-control-sm" placeholder="Date" required name="valid_to" id="valid_to">
                    </div>
                    <div class="form-group">
                        <label for="cy">CY:</label>
                        <select name="cy" id="cy" class="form-control form-control-sm" required>
                            <option value="" disabled selected>Please select one...</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="requested_by">Requested By:</label>
                        <select name="requested_by" id="" class="form-control form-control-sm">
                            <option value="" selected disabled>Please select one...</option>
                            @foreach ($end_users as $user)
                                <option value="{{ $user->id }}">{{ $user->fullName }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="approved_by">Approved By:</label>
                        <input type="text" required name="approved_by" placeholder="Approved by" class="form-control form-control-sm">
                    </div>
                    <div class="form-group">
                        <label for="proc_user_assigned">Assigned Procurement User:</label>
                        <select name="proc_user" id="proc_user_assigned" class="form-control form-control-sm" required>
                            <option value="" disabled selected>Please select one...</option>
                            @foreach ($proc_users as $user)
                                <option value="{{ $user->id }}">{{ $user->fullName }}</option>
                            @endforeach
                        </select>
                    </div> --}}
                    <div class="form-group" style="padding-top: 5px; border: 1px solid #cecece;">
                        <label for="items">Upload file:</label>
                        <input type="file" name="items" id="" required accept=".xlsx,.xls,.csv">
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="{{ route('download-pr-template') }}" class="href btn btn-warning"><i class="fa fa-download"></i> Download Sample Template</a>
                    <button type="submit" class="btn btn-primary">Upload</button>
                </div>
            {{ Form::close() }}
        </div>
    </div>
</div>

<script>
    $(function() {
        $('#pr_no').mask('000000-0000');
        $('#date, #valid_to, #valid_from').datepicker({
            dateFormat: 'MM dd, yy'
        });

        for (i = new Date().getFullYear(); i <= new Date().getFullYear() + 10; i++)
        {
            $('#cy').append($('<option />').val(i).html(i));
        }
    })
</script>