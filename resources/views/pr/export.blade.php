<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="{{ asset('css/pmrtable.css') }}">
</head>
<body>
    <br><br><br><br><br><br>
    <div class="row">
        
    </div>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr colspan="11" class="text-center">
                    <th>PURCHASE REQUEST</th>
                </tr>
            </thead>
        </table>
    </div>
    <div class="table-responsive">
        <table class="table">
            <thead>
                <tr>
                    <th colspan="11">1</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Office:</td>
                    <td colspan="3">{{ $pr->office->office }}</td>
                    <td colspan="3"></td>
                    <td style="text-align: right;">PR No:</td>
                    <td>{{ $pr->pr_no }}</td>
                    <td>Date:</td>
                    <td>{{ $pr->pr_prep_date }}</td>
                </tr>
                <tr>
                    <td>Division:</td>
                    <td colspan="3"></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="table-responsive" style="border: 2px solid #000">
        <table class="table custom-table-bordered">
            <thead>
                <tr>
                    <th></th>
                    <th></th>
                    <th colspan="6"></th>
                    <th></th>
                    <th colspan="2">Estimated Cost</th>
                </tr>
                <tr>
                    <th>Item No.</th>
                    <th style="width: 19.14;">Unit</th>
                    <th colspan="6">Item Description</th>
                    <th>Quantity</th>
                    <th>Unit</th>
                    <th>Total</th>
                </tr>
            </thead>
            <tbody>
                @foreach($pr->pr_items as $item)
                    <tr>
                        <td>{{ $item->item_no }}</td>
                        <td>{{ $item->unit }}</td>
                        <td colspan="6" style="text-align: left;">{{ $item->description }}</td>
                        <td>{{ $item->quantity }}</td>
                        @if($item->est_cost_unit === ' ')
                            <td style="text-align: right">{{ $item->est_cost_unit }}</td>
                        @else
                            <td style="text-align: right">&#8369; {{ $item->est_cost_unit }}</td>
                        @endif
                        @if(count($item->pr_sub_items))
                            <td style="text-align: right"></td>
                        @else
                            <td style="text-align: right">&#8369; {{ $item->est_cost_total }}</td>
                        @endif
                    </tr>
                    @foreach($item->pr_sub_items as $subItem)
                        <tr>
                            <td>{{ $subItem->item_no }}</td>
                            <td>{{ $subItem->unit }}</td>
                            <td style="text-align: left;" colspan="6">{{ $subItem->description }}</td>
                            <td>{{ $subItem->quantity }}</td>
                            <td style="text-align: right">&#8369; {{ $subItem->est_cost_unit }}</td>
                            <td style="text-align: right">&#8369; {{ $subItem->est_cost_total }}</td>
                        </tr>
                    @endforeach
                @endforeach
                <tr>
                    <td></td>
                    <td></td>
                    <th colspan="6">xxxx Nothing Follows xxxx</th>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </div>
    <div>
        <table>
            <tr>
                <th style="text-align: left;" colspan="2"></th>
                @if($pr->pr_item_total->is_lot_purchase === 1)
                    <td style="background-color: #000; width: 3.14;"></td>
                @else
                    <td style="border: 1px solid #000; width: 3.14;"></td>
                @endif
                <td colspan="2" style="text-align: left; width: 23.1;">Lot Purchase</td>
                @if($pr->pr_item_total->is_per_item_purchase === 1)
                    <td style="background-color: #000; width: 3.14;"></td>
                @else
                    <td style="border: 1px solid #000; width: 3.14;"></td>
                @endif
                <td colspan="2" style="text-align: left">Per Item Purchase</td>
                <th colspan="2" style="text-align: right">Total Estimate:</td>
                <th style="text-align: right">&#8369; {{ $pr->pr_item_total->total_estimate }}</td>
            </tr>
        </table>
    </div>
    <div>
        <table>
            <tbody>
                <tr>
                    <th style="border-top: 1px solid #000; border-right: 1px solid #000; border-left: 1px solid #000;" colspan="11">2</th>
                </tr>
                <tr>
                    <td colspan="11" style="border-right: 1px solid #000; border-bottom: 1px solid #000;"><strong>Purpose: </strong> {{ $pr->purpose }}</td> 
                </tr>
                <tr>
                    <td colspan="2" style="border-left: 1px solid #000;">The aforementioned supplies are:</td>
                    @if($pr->is_included_in_app === 1)
                        <td style="background-color: #000; width: 3.14;"></td>
                    @else
                        <td style="border: 1px solid #000; width: 3.14;"></td>
                    @endif
                    <td colspan="8" style="border-right: 1px solid #000;">INCLUDED in the Annual Procurement Plan submitted to Administrative and Legal Office</td>
                </tr>
                <tr>
                    <td colspan="11" style="border-left: 1px solid #000; border-right: 1px solid #000"></td>
                </tr>
                <tr>
                    <td colspan="2" style="border-bottom: 1px solid #000; border-left: 1px solid #000;"></td>
                    @if($pr->is_pm_dbm_available === 1)
                        <td style="border-bottom: 1px solid #000; background-color: #000; width: 3.14;"></td>
                        <td colspan="2" style="border-bottom: 1px solid #000; text-align: left;">Available at PS DBM</td>
                        <td style="border-bottom: 1px solid #000; border: 1px solid #000; width: 3.14;"></td>
                        <td colspan="3" style="border-bottom: 1px solid #000; text-align: left;">Not Available at PS DBM</td>
                        <td colspan="2" style="border-bottom: 1px solid #000; border-right: 1px solid #000;"></td>
                    @else
                        <td style="border-bottom: 1px solid #000; border: 1px solid #000; width: 3.14;"></td>
                        <td colspan="2" style="border-bottom: 1px solid #000; text-align: left;">Available at PS DBM</td>
                        <td style="border-bottom: 1px solid #000; background-color: #000; width: 3.14;"></td>
                        <td colspan="3" style="border-bottom: 1px solid #000; text-align: left;">Not Available at PS DBM</td>
                        <td colspan="2" style="border-bottom: 1px solid #000; border-right: 1px solid #000;"></td>
                    @endif
                </tr>
            </tbody>
        </table>
    </div>
    <div>
        <table>
            <tr>
                <td colspan="11" style="border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000; word-wrap: break-word;"><strong>3</strong></td>
            </tr>
            <tr>
                <td colspan="11" style="border-right: 1px solid #000; border-left: 1px solid #000; text-indent: 2em;">
                    The Availability of Allotment  (AA#{{ $pr->pr_budget->aa_no }}) in the amount of  &#8369;{{ $pr->pr_budget->amount }} is valid from {{ $pr->pr_budget->date_from }} to {{ $pr->pr_budget->date_to }}.
                </td>
            </tr>
            <tr>
                <td colspan="11" style="border-right: 1px solid #000; border-left: 1px solid #000;"></td>
            </tr>
            <tr>
                <td colspan="11" style="border-left: 1px solid #000; border-right: 1px solid #000; wrap-text: true; text-indent: 2em;">
                    The amount of  &#8369;{{ $pr->pr_budget->amount }} has been earmarked in our CY {{ $pr->pr_budget->current_year }} pursuant to Sec 5, Rule 1 Section 17, Rule VI of RA 9184 and its IRR.
                </td>
            </tr>
            <tr>
                <td colspan="9" style="border-left: 1px solid #000;"></td>
                <td colspan="2" style="text-align: center; border-right: 1px solid #000;"></td>
            </tr>
            <tr>
                <td colspan="9" style="border-left: 1px solid #000;"></td>
                <th colspan="2" style="text-align: center; border-right: 1px solid #000;"></th>
            </tr>
            <tr>
                <td colspan="9" style="border-left: 1px solid #000; border-bottom: 1px solid #000;      "></td>
                <th colspan="2" style="text-align: center; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000;">Budget Division</th>
            </tr>
        </table>
    </div>
    <div>
        <table>
            <tr>
                <th colspan="3" style="border: 1px solid #000; text-align: left;">4</th>
                <th colspan="4" style="border: 1px solid #000; text-align: center;">Requested By:</th>
                <th colspan="4" style="border: 1px solid #000; text-align: center;">Approved/Certified By:</th>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center; border-left: 1px solid #000; border-right: 1px solid #000">Signature:</td>
                <td colspan='4' style="border-left: 1px solid #000; border-right: 1px solid #000"></td>
                <td colspan="4" style="border-left: 1px solid #000; border-right: 1px solid #000"></td>
            </tr>
            <tr>
                <td colspan="3" style="text-align: center; border-left: 1px solid #000; border-right: 1px solid #000">Printed Name:</td>
                <td colspan='4' style="border-left: 1px solid #000; border-right: 1px solid #000"></td>
                <td colspan="4" style="border-left: 1px solid #000; border-right: 1px solid #000"></td>
            </tr>
            <tr>
                <td colspan="3" style="border-bottom: 1px solid #000; text-align: center; border-left: 1px solid #000; border-right: 1px solid #000">Designation:</td>
                <td colspan='4' style="border-bottom: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000"></td>
                <td colspan="4" style="border-bottom: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000"></td>
            </tr>
            <tr>
                <td colspan="3"></td>
                <th colspan="4">Date:</th>
                <th colspan="4">Date:</th>
            </tr>
        </table>
    </div>
    <div>
        <table>
            <tr>
                <td colspan="11"><i>Generated as of: {{ $pr->dateToday }}</i></td>
            </tr>
        </table>
    </div>
</body>
</html>