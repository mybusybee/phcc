<link rel="stylesheet" href="{{ asset('css/bootstrap4-charming.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/pr-pdf.css') }}">

<div class="container-fluid">
    <div class="row">
        <div class="w-50">
            <img src="{{ asset('img/pcc-logo-pdf.png') }}" height="100">
        </div>
        <div class="w-50 text-right">
            <img src="{{ asset('img/pcc-contact-pdf.png') }}" height="100">
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="w-100 text-center">
            <h4>PURCHASE REQUEST</h4>
        </div>
    </div>
    <div class="row">
        <div class="w-100 text-right">
            <strong>{{ Form::label('fund_cluster', 'Fund Cluster: ') }}</strong>
            <u>_____________________________</u>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="w-50">
            <strong>{{ Form::label('office_name', 'Office: ') }}</strong>
            <u>{{ $pr->office->office }}</u>
        </div>
        <div class="w-25 text-right">
            <strong>{{ Form::label('pr_no', 'PR No: ') }}</strong>
            <u>{{ $pr->pr_no }}</u>
        </div>
        <div class="w-25 text-right">
            <strong>{{ Form::label('pr_date', 'Date: ') }}</strong>
            <u>{{ $pr->pr_prep_date }}</u>
        </div>
    </div>
    <div class="row">
        <div class="w-50">
            <span style="font-weight: bold;">Responsibility Center Code: </span>
            <span style="text-decoration: underline;">{{ $pr->division->user_role }}</span>
        </div>
        <div class="w-25 text-right">
            <strong>{{ Form::label('sai_no', 'SAI No: ') }}</strong>
            <span>_______________</span>
        </div>
        <div class="w-25 text-right">
            <strong>{{ Form::label('pr_date', 'Date: ') }}</strong>
            <u>{{ $pr->pr_prep_date }}</u>
        </div>
    </div>
    <div class="row form-group">
        <div class="w-25">
            <strong><label for="">Source of Funds:</label></strong>
            <u>{{ $pr->source_of_fund }}</u>
        </div>
    </div>
    <div class="table-responsive text-center">
        <table class="table table-striped table-hover custom-table-bordered text-center">
            <thead class="table-primary">
                <tr>
                    <th rowspan="2" style="width: 7%; vertical-align: middle;">Item No</th>
                    <th rowspan="2" style="width: 7%; vertical-align: middle;">Stock No.</th>
                    <th rowspan="2" style="width: 8%; vertical-align: middle;">Unit</th>
                    <th rowspan="2" style="width: 50%; vertical-align: middle;">Item Description</th>
                    <th rowspan="2" style="width: 7%; vertical-align: middle;">Quantity</th>
                    <th colspan="2">Total Estimate</th>
                </tr>
                <tr>
                    <th style="width: 7%;">Unit</th>
                    <th style="width: 7%;">Total</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($pr->pr_items as $item)
                    <tr>
                        <td>{{ $item->item_no }}</td>
                        <td>{{ $item->stock_no }}</td>
                        <td>{{ $item->unit }}</td>
                        <td>{{ $item->description }}</td>
                        <td>{{ $item->quantity }}</td>
                        <td>{{ $item->est_cost_unit }}</td>
                        <td>{{ $item->est_cost_total }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="row">
        <div class="w-50 text-right ml-5">
            @if ($pr->pr_item_total->is_lot_purchase == 1)
                <div class="select-box highlight"></div>
                <span>Lot Purchase</span>
                <div class="select-box"></div>
                <span>Per Item Purchase</span>
            @else
                <div class="select-box"></div>
                <span>Lot Purchase</span>
                <div class="select-box highlight"></div>
                <span>Per Item Purchase</span>
            @endif
        </div>
        <div class="w-25 text-right">
            <span><strong>Total Estimate: </strong></span>
            <u>{{ $pr->pr_item_total->total_estimate }}</u>
        </div>
    </div>
    <div class="row form-group">
        <div class="w-10 text-right">
            <strong>Purpose: </strong>
        </div>
        <div class="w-85 border-bottom border-dark ml-5">
            <span>{{ $pr->purpose }}</span>
        </div>
    </div>
    <div class="row form-group">
        <div class="w-10 text-right">
            <strong>End User(s): </strong>
        </div>
        <div class="w-85 border-bottom border-dark ml-5">
            <span>{{ $pr->end_user }}</span>
        </div>
    </div>
    <div class="two-column-row form-group">
        <div class="left-column">
            <div class="label-container">
                <span><strong>Name of Property (in case of repair): </strong></span>
            </div>
            <div class="label-content-container">
                <span>{{ $pr->property_name }}</span>
            </div>
        </div>
        <div class="right-column">
            <div class="label-container">
                <span><strong>Plate No. / Serial No. :</strong></span>
            </div>
            <div class="label-content-container">
                <span>{{ $pr->property_name }}</span>
            </div>
        </div>
    </div>
    <div class="two-column-row form-group">
        <div class="left-column">
            <div class="label-container">
                <span><strong>Mode of Procurement per APP: </strong></span>
            </div>
            <div class="label-content-container">
                <span>{{ $pr->procurement_mode->mode }}</span>
            </div>
        </div>
        <div class="right-column">
            <div class="label-container">
                <span><strong>Page No.: </strong></span>
            </div>
            <div class="label-content-container">
                <span>{{ $pr->app_page }}</span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-4">
            <strong>The aforementioned supplies are: </strong>
        </div>
        <div class="row form-group">
            <div class="col-xs-12">
                <ul>
                @if ($pr->is_included_in_app == 1)
                    <div class="select-box highlight"></div>
                    <span>INCLUDED in the Annual Procurement Plan submitted to Administrative and Legal Office</span>
                @else
                    <div class="select-box"></div>
                    <span>INCLUDED in the Annual Procurement Plan submitted to Administrative and Legal Office</span>
                @endif
                </ul>
                <ul>
                @if ($pr->is_pm_dbm_available == 1)
                    <div class="select-box highlight"></div>
                    <span>Available at PS DBM</span>
                    <div class="select-box"></div>
                    <span>Not Available at PS DBM</span>
                @else
                <div class="select-box"></div>
                <span>Available at PS DBM</span>
                <div class="select-box highlight"></div>
                <span>Not Available at PS DBM</span>
                @endif
                </ul>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="w-100">
            <p style="font-weight: bold; text-indent: 5%;">The Availability of Allotment (AA#{{ $pr->pr_budget->aa_no }}) in the amount of {{ $pr->pr_budget->amount }} is valid from {{ $pr->pr_budget->date_from }} to {{ $pr->pr_budget->date_to }}</p>
        </div>
    </div>
    <div class="row">
        <div class=" w-100">
            <span style="font-weight: bold;">The amount of <strong><u>{{ $pr->pr_budget->amount }}</u></strong> has been earmarked in our CY {{ $pr->pr_budget->current_year }} Budget pursuant to Sec 5, Rule 1 Section 17, Rule VI of RA 9184 and its IRR.</span>
        </div>
    </div>
    <div class="row form-group">
        <div class="w-100 text-right px-5 mt-5">
            <p>______________________________</p>
            <div style="margin-right: 4%">
                <strong>Budget Division</strong>
            </div>
        </div>
    </div>

    <table class="table custom-table-bordered">
        <thead>
            <tr class="text-center">
                <th></th>
                <th>Requested By:</th>
                <th>Approved/Certified By:</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th>
                    <p>Signature</p>
                    <p>Printed Name</p>
                    <p>Designation</p>
                </th>
                <td></td>
                <td></td>
            </tr>
        </tbody>
        <tfoot>
            <tr class="text-left">
                <td></td>
                <th>Date:</th>
                <th>Date:</th>
            </tr>
        </tfoot>
    </table>

{{-- 
    <div class="row" style="margin-top: 20%">
        <div class="w-50">
            <strong>{{ Form::label('requested_by', 'Requested By:') }}</strong>
            <span>{{ $pr->user_request->first_name . ' ' . $pr->user_request->last_name }}</span>
        </div>
        <div class="w-50 text-right">
            <strong>{{ Form::label('approved_by', 'Approved By:') }}</strong>
            <span>{{ $pr->user_approved->first_name.' '.$pr->user_approved->last_name }}</span>
        </div>
    </div> --}}
    <br>
</div>