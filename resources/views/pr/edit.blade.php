@extends('layouts.base') 
@section('content')
<link rel="stylesheet" href="{{ asset('css/easy-autocomplete.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/easy-autocomplete.themes.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/pr/form.css') }}">
@if(Auth::user()->user_role === 7 && $pr->revision_remarks !== null && $pr->revision_last_modifier !== null)
<div class="container-fluid">
    <div class="alert alert-info">
        <h4>Purchase Request for Revision</h4>
        <p>{{ $pr->revision_remarks }}</p>
        <hr>
        <p><strong>- {{ $pr->revision_requestor->fullName }}</strong></p>
    </div>
</div>
@endif
<div class="container-fluid frm">
    {{ Form::open(['url' => url('/pr/'.$pr->id), 'method' => 'put', 'enctype' => 'multipart/form-data']) }}
        <div class="col-xl-12 text-center">
            <h2>Purchase Request</h2>
            <hr>
        </div>
        <div class="row form-group">
            <div class="col-xl-7">
                {{ Form::label('office_name', 'Office: ') }}
                {{ Form::text('', $pr->office->office, ['class' => 'form-control form-control-sm', 'disabled']) }}
            </div>
            <div class="col-xl-2 offset-xl-1">
                {{ Form::label('pr_no', 'PR No: ') }}
                {{ Form::text('', $pr->pr_no, ['class' => 'form-control form-control-sm', 'disabled']) }}
            </div>
            <div class="col-xl-2">
                {{ Form::label('pr_date', 'Date: ') }}
                {{ Form::text('',$pr->pr_prep_date, ['class' => 'form-control form-control-sm', 'disabled']) }}
            </div>
        </div>
        <div class="row form-group">
            <div class="col-xl-7">
                {{ Form::label('rc_code', 'Responsibility Center Code: ') }}
                {{ Form::text('', $pr->division->user_role, ['class' => 'form-control form-control-sm', 'disabled']) }}
            </div>
        </div>
        <div class="form-group">
            @if(Auth::user()->user_role === 7 || Auth::user()->user_role === 8)
                @include('pr/pr-table/editable-pr-table')
            @else
                @include('pr/pr-table/uneditable-pr-table')
            @endif
        </div>
        <div class="row form-group">
            @if(Auth::user()->user_role === 7 || Auth::user()->user_role === 8)
                <div class="col-xl-3 offset-xl-3">
                    <label for="lot_purchase"><input required type="radio" name="purchase_type" value="lot" id="lot_purchase" {{ $pr->pr_item_total->is_lot_purchase == 1 ? 'checked' : '' }}> - Lot Purchase</label>
                </div>
                <div class="col-xl-3">
                    <label for="per_item"><input required type="radio" name="purchase_type" id="per_item" value="per" {{ $pr->pr_item_total->is_per_item_purchase == 1 ? 'checked' : '' }}> - Per Item Purchase</label>
                </div>
            @else
                @if($pr->pr_item_total->is_lot_purchase === 1)
                    <div class="col-xl-3 offset-xl-3">
                        <label for="lot_purchase"><input required type="radio" name="purchase_type" value="lot" id="lot_purchase" checked> - Lot Purchase</label>
                    </div>
                    <div class="col-xl-3">
                        <label for="per_item"><input required type="radio" name="purchase_type" id="per_item" value="per" disabled> - Per Item Purchase</label>
                    </div>
                @else
                    <div class="col-xl-3 offset-xl-3">
                        <label for="lot_purchase"><input required type="radio" name="purchase_type" value="lot" id="lot_purchase" disabled> - Lot Purchase</label>
                    </div>
                    <div class="col-xl-3">
                        <label for="per_item"><input required type="radio" name="purchase_type" id="per_item" value="per" checked> - Per Item Purchase</label>
                    </div>
                @endif
            @endif
            <div class="col-xl-3">
                {{ Form::label('total_estimate', 'Total Estimate: ') }}
                <div class="input-group">
                    <div class="input-group-append">
                        <span class="input-group-text">&#8369;</span>
                    </div>
                    {{ Form::text('total_estimate', $pr->pr_item_total->total_estimate, ['class' => 'form-control form-control-sm', 'id' => 'total_estimate', 'readonly']) }}
                </div>
            </div>
        </div>
        <hr>
        <div class="row form-group">
            <div class="col-xl-12">
                {{ Form::label('purpose', 'Purpose: ') }}
                {{ Form::text('purpose', $pr->purpose, ['class' => 'form-control form-control-sm', 'id' => 'purpose', Auth::user()->user_role === 7 ? '' : 'readonly', 'required']) }}
            </div>
        </div>
        <div class="form-group">
            <h4>File Attachments:</h4>
            @if(Auth::user()->user_role === 7)
                <button class="btn btn-success form-group add_new_file" type="button"><i class="fa fa-plus"></i> Add New File</button>
            @endif
            <div id="files_section">
                @if(count($pr->files))
                    <div class="row">
                        <div class="col-xl-4">
                            @foreach($pr->files as $file)
                                <div class="input-group pr-files">
                                    <a style="color: #000 !important;" href="{{ url('pr/download/'.$file->id) }}" class="btn btn-outline-primary">
                                        <i class="fa fa-download"></i><span>  {{ $file->filename }}</span>
                                    </a>
                                    @if(Auth::user()->user_role === 4 || Auth::user()->user_role === 7)
                                        <div class="input-group-append">
                                            <a href="{{ url('/pr/delete-attachment/'.$file->id) }}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                        </div>
                                    @endif
                                </div>
                            @endforeach
                        </div>
                    </div>
                @else
                    <div class="row">
                        <div class="col-xl-4">
                            <ul class="list-group">
                                <li style="color: #000 !important;" class="list-group-item list-group-item-danger">No file/s attached.</li>
                            </li>
                        </div>
                    </div>
                @endif
            </div>
        </div>
        @if(Auth::user()->role->id == 8)
        <div class="row form-group">
            <div class="col-xl-4">
                {{ Form::label('supplies_confirmation', 'The aforementioned supplies are: ') }}
            </div>
            <div class="col-xl-8">
                <div class="row">
                    <div class="col-xl-12">
                        <label for="in_app"><input required type="checkbox" name="in_app" id="in_app" value="1">Included in the Annual Procurement Plan submitted to Administrative and Legal Office</label>
                    </div>
                    <div class="col-xl-3">
                        <label for="in_ps_dbm"><input type="radio" required name="ps_dbm_confirm" id="in_ps_dbm" value="1">Available at PS DBM</label>
                    </div>
                    <div class="col-xl-3">
                        <label for="not_in_ps_dbm"><input type="radio" required name="ps_dbm_confirm" id="not_in_ps_dbm" value="0">Not Available at PS DBM</label>
                    </div>
                </div>
            </div>
        </div>
        @endif
        <hr>
        @if (Auth::user()->user_role == 6)
            <div class="form-group form-inline">
                <span style="font-weight: bold; text-indent: 5%;">The Availability of Allotment (AA#{{ Form::text('aa_no', $pr->pr_budget->aa_no, ['class' => 'form-control form-control-sm', Auth::user()->user_role == 6 ? 'required' : null]) }}) in the amount of &#8369;{{ Form::text('aa_amount', $pr->pr_item_total->total_estimate, ['class' => 'form-control form-control-sm', Auth::user()->user_role == 6 ? 'required' : null, 'readonly']) }} is valid from {{ Form::text('valid_from', $pr->pr_budget->date_from, ['class' => 'form-control form-control-sm', Auth::user()->user_role == 6 ? 'required' : null, 'id' => 'valid_from']) }} to {{ Form::text('valid_to', $pr->pr_budget->date_to, ['class' => 'form-control form-control-sm', 'id' => 'valid_to', 'required']) }}</span>
            </div>
            <hr>
            <div class="form-group form-inline">
                <span style="font-weight: bold; text-indent: 5%;">The amount of &#8369;{{ Form::text('budget_amount', $pr->pr_item_total->total_estimate, ['class' => 'form-control form-control-sm', Auth::user()->user_role == 6 ? 'required' : null, 'readonly']) }} has been earmarked in our CY <select name="cy" class="form-control form-control-sm" id="cy"></select> Budget pursuant to Sec 5, Rule 1 Section 17, Rule VI of RA 9184 and its IRR.</span>
            </div>
            <hr>
        @endif
        <div class="row form-group">
            <div class="col-xl-6">
                {{ Form::label('requested_by', 'Requested By:') }}
                {{ Form::text('requested_by', $pr->user_request->fullName, ['class' => 'form-control form-control-sm', 'readonly']) }}
            </div>
            <div class="col-xl-6">
                {{ Form::label('approved_by', 'Approved By:') }}
                {{ Form::text('approved_by', $pr->approved_by, ['class' => 'form-control form-control-sm', 'readonly', 'required']) }}
            </div>
        </div>
        @if(Auth::user()->user_role === 6)
            <div class="form-group">
                <input type="submit" value="Allocate" class="btn btn-success">
                <button class="btn btn-danger" data-toggle="modal" data-target="#rejectPRModal" type="button">Reject</button>
            </div>
        @elseif(Auth::user()->user_role === 8)
            <div class="form-group">
                <input type="submit" value="Validate" class="btn btn-success">
                <button class="btn btn-danger" data-toggle="modal" data-target="#rejectPRModal" type="button">Reject</button>
            </div>
        @elseif(Auth::user()->user_role === 7)
            <div class="form-group">
                <input type="submit" value="Update and Submit" class="btn btn-success">
            </div>
        @endif
    {{ Form::close() }}
    @php $officesRejectArray = [5, 6, 8] @endphp
    @if(in_array(Auth::user()->user_role, $officesRejectArray))
        <div class="modal fade" id="rejectPRModal" tabindex="-1" role="dialog" aria-hidden="true">
            {{ Form::open(['url' => url('/pr/reject/'.$pr->id), 'method' => 'put']) }}
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Reason for rejection</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    {{ Form::textarea('pr_reject_remarks', null, ['class' => 'form-control']) }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>
            {{ Form::close() }}
        </div>
    @endif
</div>
<input type="hidden" id="pr_form_type" value="edit">
<script src="{{ asset('js/pr/pr_create.js') }}"></script>
<script src="{{ asset('js/pr/add-sub-item.js') }}"></script>
<script src="{{ asset('js/pr/remove-sub-item.js') }}"></script>
<script src="{{ asset('js/pr/form-validation.js') }}"></script>
<script src="{{ asset('js/pr/edit.js') }}"></script>
<script src="{{ asset('js/pr/add-new-file-input.js') }}"></script>
<script src="{{ asset('js/pr/item-numbering.js') }}"></script>
@endsection