@extends('layouts.base')

@section('content')
    <link rel="stylesheet" href="{{ asset('css/jquery.datetimepicker.css') }}">
    @if(Auth::user()->user_role === 7 && $pr->revision_remarks !== null && $pr->revision_last_modifier !== null)
    <div class="container-fluid">
        <div class="alert alert-info">
            <h4>Purchase Request for Revision</h4>
            <p>{{ $pr->revision_remarks }}</p>
            <hr>
            <p><strong>- {{ $pr->revision_requestor->fullName }}</strong></p>
        </div>
    </div>
    @endif
    <div class="container-fluid frm">
        <div class="row">
            <div class="col-xl-12 text-center">
                <h2>Purchase Request</h2>
            </div>
        </div>
        {{-- <div class="row form-group">
            <div class="col-xl-5 offset-xl-8">
                <strong>{{ Form::label('fund_cluster', 'Fund Cluster: ') }}</strong>
                <u></u>
            </div>
        </div> --}}
        <div class="row form-group">
            <div class="col-xl-7">
                <strong>{{ Form::label('office_name', 'Office: ') }}</strong>
                <u>{{ $pr->office->office }}</u>
            </div>
            <div class="col-xl-2 offset-xl-1">
                <strong>{{ Form::label('pr_no', 'PR No: ') }}</strong>
                <u>{{ $pr->pr_no }}</u>
            </div>
            <div class="col-xl-2">
                <strong>{{ Form::label('pr_date', 'Date: ') }}</strong>
                <u>{{ $pr->pr_prep_date }}</u>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-xl-7">
                <strong>{{ Form::label('rc_code', 'Responsibility Center Code: ') }}</strong>
                <u>{{ $pr->division->user_role }}</u>
            </div>{{-- 
            <div class="col-xl-2 offset-xl-1">
                <strong>{{ Form::label('sai_no', 'SAI No: ') }}</strong>
                <u></u>
            </div>
            <div class="col-xl-2">
                <strong>{{ Form::label('sai_date', 'Date: ') }}</strong>
                <u></u>
            </div> --}}
        </div>
        {{-- <div class="row form-group">
            <div class="col-xl-3 offset-xl-8">
                <strong>{{ Form::label('source_of_funds', 'Source of Funds:') }}</strong>
                <u>{{ $pr->source_of_fund }}</u>
            </div>
        </div> --}}
        <hr>
        <div class="table-responsive text-center">
            <table class="table table-striped table-hover custom-table-bordered">
                <thead class="table-primary">
                    <tr>
                        <th rowspan="2" style="width: 7%; vertical-align: middle;">Item No</th>
                        <th rowspan="2" style="width: 8%; vertical-align: middle;">Unit</th>
                        <th rowspan="2" style="width: 50%; vertical-align: middle;">Item Description</th>
                        <th rowspan="2" style="width: 7%; vertical-align: middle;">Quantity</th>
                        <th colspan="2">Estimated Cost</th>
                    </tr>
                    <tr>
                        <th style="width: 7%;">Unit</th>
                        <th style="width: 7%;">Total</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($pr->pr_items as $item)
                        <tr>
                            <td>{{ $item->item_no }}</td>
                            <td>{{ $item->unit }}</td>
                            <td>{{ $item->description }}</td>
                            <td>{{ $item->quantity }}</td>
                            <td>
                                @if($item->est_cost_unit === ' ')
                                    {{ $item->est_cost_unit }}
                                @else
                                    &#8369;{{ $item->est_cost_unit }}
                                @endif
                            </td>
                            <td>
                                @if(!count($item->pr_sub_items))
                                    &#8369;{{ $item->est_cost_total }}
                                @else
                                    &#8369;{{ $apps->where('id', $item->app_id)->first()->total_budget }}
                                @endif
                            </td>
                        </tr>
                        @foreach($item->pr_sub_items as $sub_item)
                        <tr>
                            <td>{{ $sub_item->item_no }}</td>
                            <td>{{ $sub_item->unit }}</td>
                            <td>{{ $sub_item->description }}</td>
                            <td>{{ $sub_item->quantity }}</td>
                            <td>&#8369;{{ $sub_item->est_cost_unit }}</td>
                            <td>&#8369;{{ $sub_item->est_cost_total }}</td>
                        </tr>
                        @endforeach
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <td></td>
                        <td></td>
                        <td class="text-center">
                            <strong>Purchase Type:</strong> 
                            @if ($pr->pr_item_total->is_lot_purchase == 1)
                                <u>Lot Purchase</u>
                            @else
                                <u>Per Item Purchase</u>
                            @endif
                        </td>
                        <th class="text-right" colspan="3">Total Estimate: &#8369;{{$pr->pr_item_total->total_estimate}}</th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <hr>
        <div class="row form-group">
            <div class="col-xl-12">
                <strong>{{ Form::label('purpose', 'Purpose: ') }}</strong>
                <p><u>{{ $pr->purpose }}</u></p>
            </div>
        </div>
        {{-- <div class="row form-group">
            <div class="col-xl-12">
                <strong>{{ Form::label('end_user', 'End User(s): ') }}</strong>
                <u>{{ $pr->end_user }}</u>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-xl-6">
                <strong>{{ Form::label('name_of_property', 'Name of Property (in case of repair): ') }}</strong>
                <u>{{ $pr->property_name }}</u>
            </div>
            <div class="col-xl-6">
                <strong>{{ Form::label('plate_serial_no', 'Plate No./Serial No.: ') }}</strong>
                <u>{{ $pr->property_serial_no }}</u>
            </div>
        </div>
        <div class="row form-group">
            <div class="col-xl-6">
                <strong>{{ Form::label('mode_of_procurement', 'Mode of Procurement per APP: ') }}</strong>
                <u>{{ $pr->procurement_mode->mode }}</u>
            </div>
            <div class="col-xl-3">
                <strong>{{ Form::label('page_no', 'Page No.: ') }}</strong>
                <u>{{ $pr->app_page }}</u>
            </div>
        </div> --}}
        @if ($pr->status == 'PR_FOR_BUDGET_ALLOCATION' || $pr->status == 'PR_FOR_SUBMISSION' || $pr->status == 'PR_ASSIGNED')
         <div class="row form-group">
            <div class="col-xl-4">
                <strong>{{ Form::label('supplies_confirmation', 'The aforementioned supplies are: ') }}</strong>
            </div>
            <div class="col-xl-8">
                <div class="row">
                    <div class="col-xl-12">
                        <ul>
                        @if ($pr->is_included_in_app == 1)
                            <li><p><strong><u>INCLUDED</u></strong> in the Annual Procurement Plan submitted to Administrative and Legal Office</p></li>
                        @else
                            <li><p><strong><u>NOT INCLUDED</u></strong> in the Annual Procurement Plan submitted to Administrative and Legal Office</p></li>
                        @endif
                        </ul>
                    </div>
                    <div class="col-xl-3">
                        <ul>
                        @if ($pr->is_pm_dbm_available == 1)
                            <li><strong><u>AVAILABLE</u></strong> at PS DBM</li>
                        @else
                            <li><strong><u>NOT AVAILABLE</u></strong> at PS DBM</li>
                        @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        @endif
        <hr>
        @if ($pr->status == 'PR_FOR_SUBMISSION' || $pr->status == 'PR_ASSIGNED')
        <div class="form-group form-inline">
            <span style="font-weight: bold; text-indent: 5%">The Availability of Allotment (AA#{{ Form::text('aa_no', $pr->pr_budget->aa_no, ['class' => 'form-control form-control-sm', 'readonly']) }}) in the amount of &#8369;{{ Form::text('aa_amount', $pr->pr_budget->amount, ['class' => 'form-control form-control-sm', 'readonly']) }} is valid from {{ Form::text('valid_from', $pr->pr_budget->date_from, ['class' => 'form-control form-control-sm', 'readonly']) }} to {{ Form::text('valid_to', $pr->pr_budget->date_to, ['class' => 'form-control form-control-sm', 'readonly']) }}</span>
        </div>
        <div class="form-group form-inline">
            <span style="font-weight: bold; text-indent: 5%;">The amount of &#8369;{{ Form::text('budget_amount', $pr->pr_budget->amount, ['class' => 'form-control form-control-sm', 'readonly']) }} has been earmarked in our CY {{ Form::text('cy', $pr->pr_budget->current_year, ['class' => 'form-control form-control-sm', 'readonly']) }} Budget pursuant to Sec 5, Rule 1 Section 17, Rule VI of RA 9184 and its IRR.</span>
        </div>
        @endif
        <div class="form-group">
            <h4>File Attachments:</h4>
            @if(count($pr->files))
                <div class="row">
                    <div class="col-xl-4">
                        <div class="list-group">
                            @foreach($pr->files as $file)
                                <a style="color: #000 !important;" href="{{ url('pr/download/'.$file->id) }}" class="list-group-item list-group-item-action list-group-item-primary">
                                    <i class="fa fa-download"></i><span>  {{ $file->filename }}</span>
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            @else
                <div class="row">
                    <div class="col-xl-4">
                        <ul class="list-group">
                            <li style="color: #000 !important;" class="list-group-item list-group-item-danger">No file/s attached.</li>
                        </li>
                    </div>
                </div>
            @endif
        </div>
        <div class="row form-group">
            <div class="col-xl-6">
                {{ Form::label('requested_by', 'Requested By:') }}
                {{ Form::text('requested_by', $pr->user_request->first_name . ' ' . $pr->user_request->last_name, ['class' => 'form-control form-control-sm', 'readonly']) }}
            </div>
            <div class="col-xl-6">
                {{ Form::label('approved_by', 'Approved By:') }}
                {{ Form::text('', $pr->approved_by, ['class' => 'form-control form-control-sm', 'readonly']) }}
            </div>
        </div>
        <hr>
        @if (Auth::user()->role->id == 7 && $pr->status == 'PR_FOR_SUBMISSION')
            <div class="form-group">
                <a href="{{ url('/export/xls/pr/'.$pr->id) }}" class="btn btn-success">Export</a>
            </div>
        @elseif ((Auth::user()->role->id == 4 && ($pr->status == 'PR_FOR_DIRECTOR_APPROVAL')))
            <div class="form-group">
                <a href="{{ url('/pr/approve/'.$pr->id) }}" class="btn btn-success">Approve</a>
                <button class="btn btn-danger" data-toggle="modal" data-target="#rejectPRModal" type="button">Reject</button>
            </div>
        @elseif(Auth::user()->role->id == 5 && $pr->status == 'PR_ASSIGNED')
            <div class="row">
                <div class="col-xl-4">
                    @if($pr->date_received === null && $pr->is_hardcopy_received === 1)
                        {{ Form::open(['url' => url('pr/datereceived/'.$pr->id), 'method' => 'post']) }}
                            <div class="input-group">
                                <input name="received" type="text" class="datetimepicker form-control form-control-sm" placeholder="Select Date Received" required>
                                <div class="input-group-append">
                                    <input type="submit" class="btn btn-info" value="Save">
                                </div>
                            </div>
                        {{ Form::close() }}
                    @else
                    <div class="input-group">
                        <label for="date_of_receipt"><strong>Date Received: </strong></label>
                        <input name="date_of_receipt" type="text" class="form-control form-control-sm" placeholder="Select Date Received" readonly value="{{ $pr->date_received->format('M d, Y h:i:s A') }}">
                    </div>
                    @endif
                </div>    
            </div>
        @elseif(Auth::user()->role->id == 3 && $pr->status == 'PR_FOR_SUBMISSION')
            <div class="row">
                @if($pr->date_received === null && $pr->is_hardcopy_received === 0)
                    <div class="col-xl-4">
                        <a href="{{ url('/pr/hardcopyreceived/'.$pr->id) }}" class="btn btn-primary">Hard Copy Received?</a>
                    </div>
                @else
                    <div class="col-xl-4">
                        <button class="btn btn-warning" type="button">Hard Copy Received</button>
                    </div>
                @endif
                @if($pr->is_hardcopy_received === 1)
                    <div class="col-xl-4 offset-xl-4 float-right">
                        {{ Form::open(['url' => url('/pr/assign/'.$pr->id), 'method' => 'POST']) }}
                            <div class="input-group mb-3">
                                <select name="proc_admin" id="proc_admin" class="form-control">
                                    <option value="" selected disabled>Please select one...</option>
                                    @foreach($proc_admins as $key => $value)
                                        <option value="{{ $key }}">{{ $value }}</option>
                                    @endforeach
                                </select>
                                <div class="input-group-append">
                                    <input type="submit" value="Assign" class="btn btn-success" id="assignProcurementUserBtn" disabled>
                                </div>
                            </div>
                        {{ Form::close() }}
                    </div>
                @endif
            </div>
        @endif
        @if(session()->get('user_role') == 4)
		<div class="modal fade" id="rejectPRModal" tabindex="-1" role="dialog" aria-hidden="true">
			{{ Form::open(['url' => url('/pr/reject/'.$pr->id), 'method' => 'put']) }}
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Reason for rejection</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="container-fluid">
							<div class="row">
								<div class="col-md-12">
									{{ Form::textarea('pr_reject_remarks', null, ['class' => 'form-control']) }}
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</div>
			</div>
			{{ Form::close() }}
		</div>
		@endif
    </div>
    <script src="{{ asset('js/utils/jquery.datetimepicker.js') }}"></script>
    <script src="{{ asset('js/pr/show.js') }}"></script>
@endsection