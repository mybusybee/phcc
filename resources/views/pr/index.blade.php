@extends('layouts.base')

@section('content')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <div class="container-fluid frm">
        <div class="row">
            <div class="col-xl-6">
                <h1>Purchase Requests</h1>
            </div>
            <div class="col-xl-6">
                @php $importUserRoles = [3, 5, 8]; @endphp
                <div class="text-right">
                    @if (in_array(Auth::user()->user_role, $importUserRoles))
                        <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#importPrModal">Import PR</button>
                        @include('pr.import-modal')
                    @endif
                    @if (Auth::user()->user_role === 7 || Auth::user()->user_role === 1)
                        <a href="{{ url('pr/create') }}" class="btn btn-success">Create new...</a>
                    @endif
                </div>
            </div>
        </div>
        {{ Form::open(['url' => url('/export/consolidate/prs'), 'method' => 'post']) }}
        <div class="table-responsive">
            <table class="table table-hover table-striped pr-table" id="prTable">
                <thead class="table-primary text-center">
                    <tr>
                        {{-- <th><i class="far fa-check-square"></i></th> --}}
                        <td class="d-none">ID</td>
                        <th>PR No.</th>
                        <th>Office</th>
                        <th>PR Date</th>
                        <th>Status</th>
                        @if(Auth::user()->user_role === 7 || Auth::user()->user_role === 3 || Auth::user()->user_role === 5)
                            <th>Assigned To</th>
                            <th>Date Assigned</th>
                        @endif
                        @if(Auth::user()->user_role === 3 || Auth::user()->user_role === 5)
                            <th>Date Received</th>
                        @endif
                        <th>Mode of Procurement</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody class="text-center">
                    @foreach ($prs as $pr)
                        <tr>
                            <td class="d-none"><input class="pr_id" type="hidden" value="{{ $pr->id }}">{{ $pr->id }}</td>
                            <td>{{ $pr->pr_no }}</td>
                            <td>{{ $pr->office->office }}</td>
                            <td>{{ $pr->pr_prep_date }}</td>
                            @if($pr->status == 'PR_FOR_DIRECTOR_APPROVAL')
							    <td class="status-pr-for-director-approval">{{ $pr->status }}</td>
							@elseif($pr->status == 'PR_FOR_PROCUREMENT_VALIDATION')
							    <td class="status-pr-for-procurement-validation">{{ $pr->status }}</td>
                            @elseif($pr->status == 'PR_FOR_BUDGET_ALLOCATION')
                                <td class="status-pr-for-budget-validation">{{ $pr->status }}</td>
							@elseif($pr->status == 'PR_FOR_REVISION')
							    <td class="status-pr-for-revision">{{ $pr->status }}</td>
							@elseif($pr->status == 'PR_FOR_SUBMISSION' && (Auth::user()->user_role === 7 || Auth::user()->user_role === 4 || Auth::user()->user_role === 1 || Auth::user()->user_role === 8))
                                <td class="status-pr-for-submission">{{ $pr->status }}</td>
                            @elseif($pr->status == 'PR_FOR_SUBMISSION' && Auth::user()->user_role === 3)
                                <td class="status-pr-for-assignment">PR_FOR_ASSIGNMENT</td>
                            @elseif($pr->status == 'PR_FOR_SUBMISSION' && Auth::user()->user_role === 6)
                                <td class="status-pr-for-assignment">ALLOCATED</td>
							@elseif($pr->status == 'PR_ASSIGNED')
                                @if (Auth::user()->user_role === 6)
                                    <td class="status-pr-for-assignment">ALLOCATED</td>
                                @else
                                    <td class="status-pr-assigned">{{ $pr->status }}</td>
                                @endif
                            {{-- @elseif($pr->status == 'PR_FOR_ASSIGNMENT')
                                <td class="status-pr-for-assignment">{{ $pr->status }}</td> --}}
                            @endif

                            @if(Auth::user()->user_role === 7 || Auth::user()->user_role === 3 || Auth::user()->user_role === 5)
                                <td>
                                    @if($pr->proc_user_assigned === null)
                                        Not yet assigned.
                                    @else
                                        {{ $pr->proc_user->fullname }}
                                    @endif
                                </td>
                                <td>
                                    @if($pr->date_assigned === null)
                                        Not yet assigned.
                                    @else
                                        {{ $pr->date_assigned->format('F d, Y h:i:s A') }}
                                    @endif
                                </td>
                            @endif
                            @if(Auth::user()->user_role === 3 || Auth::user()->user_role === 5)
                                @if($pr->date_received !== null)
                                    <td>{{ $pr->date_received->format('F d, Y h:i:s A') }}</td>
                                @else
                                    <td>Not yet received.</td>
                                @endif
                            @endif
                            <td>{{ $pr->procurement_mode}}</td>
                            <td>
                                <a href="{{ url('/pr/'.$pr->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                                @if ((Auth::user()->role->id == 1 || (Auth::user()->role->id == 7) && $pr->status == 'PR_FOR_REVISION') || (Auth::user()->user_role == 6 && $pr->status == 'PR_FOR_BUDGET_ALLOCATION') || (Auth::user()->user_role == 8 && $pr->status == 'PR_FOR_PROCUREMENT_VALIDATION'))
                                    <a href="{{ url('/pr/'.$pr->id.'/edit') }}" class="btn btn-info"><i class="fa fa-edit"></i></a>
                                @endif
                                @if(Auth::user()->role->id == 5 && $pr->status == 'PR_ASSIGNED' && $pr->date_received !== null)
                                    <div class="input-group">
                                        <select name="" class="form-control formSelector">
                                            <option value="" selected disabled>Please select one...</option>
                                            <option value="itb">IB</option>
                                            <option value="rei">REI</option>
                                            <option value="rfq">RFQ</option>
                                            <option value="rfp">RFP</option>
                                        </select>
                                        <div class="input-group-append">
                                            <a href="" class="btn btn-info generateFormBtn">Generate</a>
                                        </div>
                                    </div>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div id="pr_ids_container">

        </div>
        {{-- <div class="row form-group">
            <div class="col-xl-12">
                <input type="submit" value="Consolidate PRs" class="btn btn-success">
            </div>
        </div> --}}
        {{ Form::close() }}
        <div class="row">
            <div class="col-xl-4 col-lg-7 col-xl-8">
                <div class="card">
                    <div class="card-header bg-primary">
                        Status Column Legend:
                    </div>
                    <div class="card-body bg-light">
                        <div class="col-xl-12 d-flex align-items-center">
                            <div class="legend-box status-pr-for-revision"></div>
                            <span>PR_FOR_REVISION</span>
                        </div>
                        <div class="col-xl-12 d-flex align-items-center">
                            <div class="legend-box status-pr-for-director-approval"></div>
                            <span>PR_FOR_DIRECTOR_APPROVAL</span>
                        </div>
                        <div class="col-xl-12 d-flex align-items-center">
                            <div class="legend-box status-pr-for-procurement-validation"></div>
                            <span>PR_FOR_PROCUREMENT_VALIDATION</span>
                        </div>
                        <div class="col-xl-12 d-flex align-items-center">
                            <div class="legend-box status-pr-for-budget-validation"></div>
                            <span>PR_FOR_BUDGET_ALLOCATION</span>
                        </div>
                        <div class="col-xl-12 d-flex align-items-center">
                            <div class="legend-box status-pr-for-submission"></div>
                            <span>PR_FOR_SUBMISSION</span>
                        </div>
                        <div class="col-xl-12 d-flex align-items-center">
                            <div class="legend-box status-pr-for-assignment"></div>
                            <span>PR_FOR_ASSIGNMENT</span>
                        </div>
                        <div class="col-xl-12 d-flex align-items-center">
                            <div class="legend-box status-pr-assigned"></div>
                            <span>PR_ASSIGNED</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('js/pr/index.js') }}"></script>
@endsection