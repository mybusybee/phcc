@extends('layouts.base') 
@section('content')
<link rel="stylesheet" href="{{ asset('css/easy-autocomplete.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/easy-autocomplete.themes.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/pr/form.css') }}">
<div class="container-fluid frm">
    {{ Form::open(['url' => url('/pr'), 'method' => 'post', 'enctype' => 'multipart/form-data']) }}
        <div class="col-xl-12 text-center">
            <h2>Purchase Request</h2>
            <hr>
        </div>
        <div class="row form-group">
            <div class="col-xl-7">
                {{ Form::label('office_name', 'Office: ') }}
                {{ Form::text('office_name', Auth::user()->office->office, ['class' => 'form-control form-control-sm', 'id' => 'office_name']) }}
            </div>
            <div class="col-xl-2 offset-xl-1">
                {{ Form::label('pr_no', 'PR No: ') }}
                {{ Form::text('pr_no', null, ['class' => 'form-control form-control-sm', 'id' => 'pr_no']) }}
            </div>
            <div class="col-xl-2">
                {{ Form::label('pr_date', 'Date: ') }}
                {{ Form::text('pr_date', date('F d, Y'), ['class' => 'form-control form-control-sm', 'id' => 'pr_date']) }}
            </div>
        </div>
        <div class="row form-group">
            <div class="col-xl-7">
                {{ Form::label('rc_code', 'Responsibility Center Code: ') }}
                {{ Form::text('rc_code', Auth::user()->role->user_role, ['class' => 'form-control form-control-sm', 'id' => 'rc_code']) }}
            </div>
        </div>
        <div class="form-group">
            <table class="table table-striped table-hover table-bordered text-center custom-table-bordered" id="pr_table">
                <thead class="table-primary">
                    <tr>
                        <th colspan="4"></th>
                        <th colspan="2">Estimated Cost</th>
                        <th></th>
                    </tr>
                    <tr>
                        <th style="width: 7%;">Item No.</th>
                        <th style="width: 8%;">Unit</th>
                        <th style="width: 50%;">Item Description</th>
                        <th style="width: 7%;">Quantity</th>
                        <th style="width: 7%;">Unit</th>
                        <th style="width: 7%;">Total</th>
                        <th style="width: 4%"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr class="d-none">
                        <td>
                            <select name="" id="description_controller" class="description">
                                <option value="" disabled selected>Select one...</option>
                                @foreach ($apps as $app)
                                    <option data-app-id="{{ $app->id }}" data-quantity={{ $app->pap->size }} data-budget="{{ $app->total_budget }}" value="{{ $app->pap->general_description }}" data-procurement-mode="{{ $app->mode_of_procurement }}" data-allotment-type="{{ $app->pap->allotment_type }}">{{ $app->pap->general_description }} - {{ $app->pap->allotment_type }}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                    <tr id="all_units_row" class="d-none">
                        <td>
                            <select name="" id="all_units" class="form-control form-control-sm">
                                @foreach ($units as $unit)
                                    <option value="{{ $unit->unit }}">{{ $unit->unit }}</option>
                                @endforeach
                            </select>
                        </td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="8" class="text-right"><button @click="test" type="button" class="add-pr-row btn btn-success btn-xs">+</button></td>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div class="row form-group">
            <div class="col-xl-3 offset-xl-3">
                <label for="lot_purchase"><input required type="radio" name="purchase_type" value="lot" id="lot_purchase"> - Lot Purchase</label>
            </div>
            <div class="col-xl-3">
                <label for="per_item"><input required type="radio" name="purchase_type" id="per_item" value="per"> - Per Item Purchase</label>
            </div>
            <div class="col-xl-3">
                {{ Form::label('total_estimate', 'Total Estimate: ') }}
                <div class="input-group input-group-sm">
                    <div class="input-group-append">
                        <span class="input-group-text">&#8369;</span>
                    </div>
                    {{ Form::text('total_estimate', null, ['class' => 'form-control form-control-sm', 'id' => 'total_estimate', 'readonly']) }}
                </div>
            </div>
        </div>
        <hr>
        <div class="row form-group">
            <div class="col-xl-12">
                {{ Form::label('purpose', 'Purpose: ') }}
                {{ Form::text('purpose', null, ['class' => 'form-control form-control-sm', 'id' => 'purpose', 'required']) }}
            </div>
        </div>
        @if(Auth::user()->role->id == 5)
        <div class="row form-group">
            <div class="col-xl-4">
                {{ Form::label('supplies_confirmation', 'The aforementioned supplies are: ') }}
            </div>
            <div class="col-xl-8">
                <div class="row">
                    <div class="col-xl-12">
                        <label for="in_app"><input required type="checkbox" name="in_app" id="in_app" value="1">Included in the Annual Procurement Plan submitted to Administrative and Legal Office</label>
                    </div>
                    <div class="col-xl-3">
                        <label for="in_ps_dbm"><input type="radio" required name="ps_dbm_confirm" id="in_ps_dbm" value="1">Available at PS DBM</label>
                    </div>
                    <div class="col-xl-3">
                        <label for="not_in_ps_dbm"><input type="radio" required name="ps_dbm_confirm" id="not_in_ps_dbm" value="0">Not Available at PS DBM</label>
                    </div>
                </div>
            </div>
        </div>
        @endif
        <hr>
        <div class="form-group" id="files_section">
            <h4>Upload Required Files</h4><span style="font-size: 0.9em;"><i>PDF files only, maximum of 5 attachments.</i></span>
            <button class="btn btn-success add_new_file" type="button"><i class="fa fa-plus"></i> Add</button>
            <div class="input-group file_row">
            </div>
        </div>
        <hr>
        <div class="row form-group">
            <div class="col-xl-6">
                {{ Form::label('requested_by', 'Requested By:') }}
                {{ Form::text('requested_by', Auth::user()->first_name . ' ' . Auth::user()->last_name, ['class' => 'form-control form-control-sm']) }}
            </div>
            <div class="col-xl-6">
                {{ Form::label('approved_by', 'Approved By:') }}
                {{ Form::text('approved_by', null, ['class' => 'form-control form-control-sm', 'required']) }}
            </div>
        </div>
        <div class="form-group">
            <input type="submit" value="Submit" class="btn btn-success" id="prSubmit">
        </div>
    {{ Form::close() }}
</div>
<input type="hidden" value="{{ $pr_count }}" id="pr_count">
<input type="hidden" value="{{ $item_no_count }}" id="item_no_count">
<script src="{{ asset('js/pr/pr_create.js') }}"></script>
<script src="{{ asset('js/pr/add-sub-item.js') }}"></script>
<script src="{{ asset('js/pr/remove-sub-item.js') }}"></script>
<script src="{{ asset('js/pr/form-validation.js') }}"></script>
<script src="{{ asset('js/pr/add-new-file-input.js') }}"></script>
<script src="{{ asset('js/pr/item-numbering.js') }}"></script>
@endsection