<table class="table table-striped table-hover custom-table-bordered text-center">
    <thead class="table-primary">
        <tr>
            <th rowspan="2" style="width: 7%; vertical-align: middle;">Item No</th>
            <th rowspan="2" style="width: 8%; vertical-align: middle;">Unit</th>
            <th rowspan="2" style="width: 50%; vertical-align: middle;">Item Description</th>
            <th rowspan="2" style="width: 7%; vertical-align: middle;">Quantity</th>
            <th colspan="2">Estimated Cost</th>
        </tr>
        <tr>
            <th style="width: 7%;">Unit</th>
            <th style="width: 7%;">Total</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($pr->pr_items as $item)
            <tr>
                <td>{{ $item->item_no }}</td>
                <td>{{ $item->unit }}</td>
                <td>{{ $item->description }}</td>
                <td>{{ $item->quantity }}</td>
                <td>
                    @if($item->est_cost_unit === ' ')
                        {{ $item->est_cost_unit }}
                    @else
                        &#8369;{{ $item->est_cost_unit }}
                    @endif
                </td>
                @if(!count($item->pr_sub_items))
                    <td>
                        &#8369;{{ $item->est_cost_total }}
                    </td>
                @else
                    <td>
                        &#8369;{{ $apps->where('id', $item->app_id)->first()->total_budget }}
                    </td>
                @endif
            </tr>
            @foreach($item->pr_sub_items as $sub_item)
            <tr>
                <td>{{ $sub_item->item_no }}</td>
                <td>{{ $sub_item->unit }}</td>
                <td>{{ $sub_item->description }}</td>
                <td>{{ $sub_item->quantity }}</td>
                <td>&#8369;{{ $sub_item->est_cost_unit }}</td>
                <td>&#8369;{{ $sub_item->est_cost_total }}</td>
            </tr>
            @endforeach
        @endforeach
    </tbody>
</table>