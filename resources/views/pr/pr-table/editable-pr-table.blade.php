<table class="table table-striped table-hover table-bordered text-center custom-table-bordered" id="pr_table">
    <thead class="table-primary">
        <tr>
            <th colspan="4"></th>
            <th colspan="2">Estimated Cost</th>
            <th></th>
        </tr>
        <tr>
            <th style="width: 7%;">Item No.</th>
            <th style="width: 8%;">Unit</th>
            <th style="width: 50%;">Item Description</th>
            <th style="width: 7%;">Quantity</th>
            <th style="width: 7%;">Unit</th>
            <th style="width: 7%;">Total</th>
            <th style="width: 4%"></th>
        </tr>
    </thead>
    <tbody>
        <tr class="d-none">
            <td>
                <select name="" id="description_controller">
                    <option value="" disabled selected>Select one...</option>
                    @foreach ($apps as $app)
                        <option data-app-id="{{ $app->id }}" data-quantity={{ $app->pap->size }} data-budget={{ $app->pap->estimated_budget }} value="{{ $app->pap->general_description }}">{{ $app->pap->general_description }} - {{ $app->pap->allotment_type }}</option>
                    @endforeach
                </select>
            </td>
        </tr>
        <tr id="all_units_row" class="d-none">
            <td>
                <select name="" id="all_units" class="form-control form-control-sm">
                    @foreach ($units as $unit)
                        <option value="{{ $unit->unit }}">{{ $unit->unit }}</option>
                    @endforeach
                </select>
            </td>
        </tr>
        @php $row_counter = 1; @endphp
        @foreach($pr->pr_items as $prItem)
            <tr id="{{ $row_counter }}" class="pr-item">
                <td class="d-none"><input type="hidden" name="pr_item_row_counter[]" value="{{ $prItem->app_id }}"></td>
                <td class="item-no-container">
                    {{ Form::text('item_no_'.$row_counter, $prItem->item_no, ['class' => 'form-control form-control-sm item_no parent_item_no', 'readonly']) }}
                    @php $sub_row_counter = 1; @endphp
                    @if(count($prItem->pr_sub_items))
                        @foreach($prItem->pr_sub_items as $subItem)
                            <div class="input-group sub-item">
                                {{ Form::text('itemNoSub'.$row_counter.'[]', $subItem->item_no, ['class' => 'form-control form-control-sm sub-item-no', 'data-parent' => $row_counter, 'data-sub-row' => $sub_row_counter, 'readonly']) }}
                            </div>
                            @php $sub_row_counter++; @endphp
                        @endforeach
                    @endif
                </td>
                <td class="unit-container">
                    <input type="hidden"  name="{{ 'unit_no_h_'.$row_counter }}" class="hidden_unit form-control" value="{{ $prItem->unit }}">
                    {{ Form::select('unit_no_'.$row_counter, $units_array, $prItem->unit, ['class' => 'form-control form-control-sm unit_no', 'required']) }}
                    @if(count($prItem->pr_sub_items))
                        @php $sub_row_counter = 1; @endphp
                        @foreach($prItem->pr_sub_items as $subItem)
                            <div class="input-group">
                                {{ Form::select('unitSub'.$row_counter.'[]', $units_array, $subItem->unit, ['class' => 'form-control form-control-sm', 'data-parent' => $row_counter, 'data-sub-row' => $sub_row_counter]) }}
                            </div>
                            @php $sub_row_counter++; @endphp
                        @endforeach
                    @endif
                </td>
                <td class="description-container">
                    <div class="input-group">
                        <select name="{{ 'description_'.$row_counter }}" class="form-control form-control-sm description">
                            @foreach($apps as $app)
                                <option data-app-id="{{ $app->id }}" data-quantity={{ $app->pap->size }} data-budget={{ $app->total_budget }} value="{{ $app->pap->general_description }}">{{ $app->pap->general_description }}</option>
                            @endforeach
                        </select>
                        <input type="hidden" value="{{ $prItem->app_id }}">
                        <div class="input-group-append">
                            <button type="button" class="btn btn-primary btn-xs add-sub-item">+</button>
                        </div>
                    </div>
                    @if(count($prItem->pr_sub_items))
                        @php $sub_row_counter = 1; @endphp
                        @foreach($prItem->pr_sub_items as $subItem)
                            <div class="input-group" data-parent="{{ $row_counter }}" data-sub-row="{{ $sub_row_counter }}">
                            <input type="text" class="form-control form-control-sm" name="{{ 'descriptionSub'.$row_counter.'[]' }}" value="{{ $subItem->description }}">
                                <div class="input-group-append">
                                    <button type="button" class="btn btn-danger btn-xs remove-sub-item-row">-</button>
                                </div>
                            </div>
                            @php $sub_row_counter++; @endphp
                        @endforeach
                    @endif
                </td>
                <td class="quantity-container">
                    {{ Form::text('quantity_'.$row_counter, $prItem->quantity, ['class' => 'form-control form-control-sm quantity', count($prItem->pr_sub_items) ? 'readonly' : '']) }}
                    @php $sub_row_counter = 1; @endphp
                    @if(count($prItem->pr_sub_items))
                        @foreach($prItem->pr_sub_items as $subItem)
                            <div class="input-group">
                                {{ Form::text('quantitySub'.$row_counter.'[]', $subItem->quantity, ['class' => 'form-control form-control-sm subQuantity', 'data-parent' => $row_counter, 'data-sub-row' => $sub_row_counter]) }}
                            </div>
                            @php $sub_row_counter++; @endphp
                        @endforeach
                    @endif
                </td>
                <td class="unit-cost-container">
                    <div class="input-group form-text-extend input-group-sm">
                        <div class="input-group-prepend">
                            <span class="input-group-text">&#8369;</span>
                        </div>
                        {{ Form::text('unit_cost_'.$row_counter, $prItem->est_cost_unit, ['class' => 'form-control form-control-sm unit_cost', count($prItem->pr_sub_items) ? 'readonly' : '']) }}
                        @php $sub_row_counter = 1; @endphp
                        @if(count($prItem->pr_sub_items))
                            @foreach($prItem->pr_sub_items as $subItem)
                                <div class="input-group form-text-extend input-group-sm">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">&#8369;</span>
                                    </div>
                                    {{ Form::text('unitCostSub'.$row_counter.'[]', $subItem->est_cost_unit, ['class' => 'form-control form-control-sm subUnitCost' ,'data-parent' => $row_counter, 'data-sub-row' => $sub_row_counter]) }}
                                </div>
                                @php $sub_row_counter++; @endphp
                            @endforeach
                        @endif
                    </div>
                </td>
                <td class="total-cost-container">
                    <div class="input-group form-text-extend input-group-sm">
                        <div class="input-group-prepend">
                            <span class="input-group-text">&#8369;</span>
                        </div>
                        @if(count($prItem->pr_sub_items))
                            {{ Form::text('total_cost_'.$row_counter, $apps->where('id', $prItem->app_id)->first()->total_budget, ['class' => 'form-control form-control-sm total_cost', 'readonly']) }}
                        @else
                            {{ Form::text('total_cost_'.$row_counter, $prItem->est_cost_total, ['class' => 'form-control form-control-sm total_cost', 'readonly']) }}
                        @endif
                    </div>
                    @php $sub_row_counter = 1; @endphp
                    @if(count($prItem->pr_sub_items))
                        @foreach($prItem->pr_sub_items as $subItem)
                            <div class="input-group form-text-extend input-group-sm">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">&#8369;</span>
                                </div>
                                {{ Form::text('totalCostSub'.$row_counter.'[]', $subItem->est_cost_total, ['class' => 'form-control form-control-sm subTotalCost', 'data-parent' => $row_counter, 'data-sub-row' => $sub_row_counter, 'readonly']) }}
                            </div>
                            @php $sub_row_counter++; @endphp
                        @endforeach
                    @endif
                </td>
                <td>
                    <button type="button" class="btn btn-danger btn-xs remove-pr-row">-</button>
                </td>
            </tr>
            @php $row_counter++; @endphp
        @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td colspan="6"></td>
            <td class="text-center"><button type="button" class="add-pr-row btn btn-success btn-xs">+</button></td>
        </tr>
    </tfoot>
</table>