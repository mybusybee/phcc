@extends('layouts.base')

@section('content')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <div class="container-fluid frm">
        <h1>IB/RFQ/REI/RFP List</h1>
        <div class="table-responsive">
            <table class="table table-hover table-striped text-center" id="invitesTable">
                <thead class="table-primary">
                    <tr>
                        <th>Number</th>
                        <th>Date</th>
                        <th>PR No.</th>
                        <th>PR Date</th>
                        @if(Auth::user()->user_role === 3)
                            <th>Assigned To</th>
                        @endif
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($invites as $invite)
                        <tr id="{{ $invite->id }}">
                            <td>{{$invite->type}}#: {{ $invite->form_no }}</td>
                            <td>{{ $invite->created_at->format('F d, Y') }}</td>
                            <td>{{ $invite->pr->pr_no }}</td>
                            <td>{{ $invite->pr->pr_prep_date }}</td>
                            @if(Auth::user()->user_role === 3)
                                <td>{{ $invite->pr->proc_user->full_name }}</td>
                            @endif
                            @if ($invite->pr->personal_pmr_item->status == 'Cancelled' || $invite->pr->personal_pmr_item->status == 'Failed')
                                <td>CANCELLED</td>
                            @else
                                <td>{{ $invite->status }}</td>
                            @endif
                            <td>
                                
                                @if ($invite->type == 'RFQ')
                                    <a href="{{ url('/rfq/'.$invite->rfq->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                                @elseif ($invite->type == 'RFP')
                                    <a href="{{ url('/rfp/'.$invite->rfp->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                                @elseif ($invite->type == 'IB')
                                    <a href="{{ url('/itb/'.$invite->itb->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                                @elseif ($invite->type == 'REI')
                                    <a href="{{ url('/rei/'.$invite->rei->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                                @endif

                                @if ($invite->pr->personal_pmr_item->status == 'Cancelled' || $invite->pr->personal_pmr_item->status == 'Failed')

                                @else
                                    @if ($invite->type == 'RFQ' && $invite->status === 'FOR_REVISION' && Auth::user()->user_role === 5)
                                        <a href="{{ url('/rfq/'.$invite->rfq->id.'/edit') }}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a>
                                    @elseif ($invite->type == 'RFP' && $invite->status === 'FOR_REVISION' && Auth::user()->user_role === 5)
                                        <a href="{{ url('/rfp/'.$invite->rfp->id.'/edit') }}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a>
                                    @elseif ($invite->type == 'IB' && $invite->status === 'FOR_REVISION' && Auth::user()->user_role === 5)
                                        <a href="{{ url('/itb/'.$invite->itb->id.'/edit') }}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a>
                                    @elseif ($invite->type == 'REI' && $invite->status === 'FOR_REVISION' && Auth::user()->user_role === 5)
                                        <a href="{{ url('/rei/'.$invite->rei->id.'/edit') }}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a>
                                    @endif

                                    @if ($invite->type == 'RFQ' && $invite->status === 'FOR_REVIEW/APPROVAL')
                                        <a href="{{ url('/rfq/'.$invite->rfq->id.'/edit') }}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a>
                                    @elseif ($invite->type == 'RFP' && $invite->status === 'FOR_REVIEW/APPROVAL')
                                        <a href="{{ url('/rfp/'.$invite->rfp->id.'/edit') }}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a>
                                    @elseif ($invite->type == 'IB' && $invite->status === 'FOR_REVIEW/APPROVAL')
                                        <a href="{{ url('/itb/'.$invite->itb->id.'/edit') }}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a>
                                    @elseif ($invite->type == 'REI' && $invite->status === 'FOR_REVIEW/APPROVAL')
                                        <a href="{{ url('/rei/'.$invite->rei->id.'/edit') }}" class="btn btn-sm btn-info"><i class="fa fa-edit"></i></a>
                                    @endif

                                    @if(Auth::user()->user_role === 5 && $invite->status === 'FARMED_OUT')
                                        <div class="input-group">
                                            <select name="" class="form-control formSelector">
                                                <option value="" selected disabled>Select abstract</option>
                                                <option value="aob">AOB</option>
                                                <option value="aop">AOP</option>
                                                <option value="aoq">AOQ</option>
                                            </select>
                                            <div class="input-group-append">
                                                <a href="{{ url('/invitations') }}" class="btn btn-info generateBtn">Generate</a>
                                            </div>
                                        </div>
                                    @endif
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('js/invitations/index.js') }}"></script>
@endsection