@extends('layouts.base')

@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/ppmp/ppmp.css') }}">
<div class="container-fluid">
	<div class="frm center-div">
		<div class="row">
			<div class="col-md-4 form-group">
				<label for="">END-USER/UNIT:</label>
				<input type="text" value="{{ $ppmp->office->office }}" readonly class="form-control">
			</div>
			<div class="col-md-2 offset-md-1" id="ppmp_year_container">
				<label for="end_user">PPMP for Year:</label>
				{{ Form::text('', $ppmp->for_year, ['class' => 'form-control', 'readonly']) }}
			</div>
		</div>
		<div class="">
			<div class="col-md-12 form-group">
				<div class="justify-content-center">
					<div class="col-md-12 form-group table-responsive" style="max-height: 500px; overflow: auto;">
						<table class="table table-hover table-striped text-center ppmp-tbl table-bordered">
							<thead class="table-primary">
								<tr>
									<th colspan="7"></th>
									<th colspan="12">Schedule/Milestones of Activities</th>
									<tr>
										<th style="min-width: 150px;">Code</th>
										<th>General Description</th>
										<th>Qty/Size</th>
										<th>UACS Object</th>
										<th>Allotment Type</th>
										<th>Est. Budget</th>
										<th>Mode of Procurement</th>
										<th>Jan</th>
										<th>Feb</th>
										<th>Mar</th>
										<th>Apr</th>
										<th>May</th>
										<th>Jun</th>
										<th>Jul</th>
										<th>Aug</th>
										<th>Sept</th>
										<th>Oct</th>
										<th>Nov</th>
										<th>Dec</th>
									</tr>
								</tr>
							</thead>
							<tbody class="pap-body">
								<td style="display: none;">
									<input type="text" class="form-control" value="Procurement mode here" readonly>
								</td>
								<?php $i = 1; foreach($ppmp->paps as $pap): ?>
								<?php
								$modes = [];
								foreach($pap->procurement_modes as $pap_mode){
									foreach($procurement_modes as $procurement_mode){
										if($pap_mode->id == $procurement_mode->id){
											$modes[] = $procurement_mode->mode;
										}
									}
								}
								?>
								<tr>
									<td style="display: none;">
										<input type="hidden" name="row_counter[]" value="<?php echo $i; ?>" />
									</td>
									<td>
										<p>{{ $pap->code }}</p>
									</td>
									<td>
										{{ $pap->general_description }}
									</td>
									<td>
										<p>{{ $pap->size }}</p>
									</td>
									<td>
										<p>{{ $pap->uacs_object->uacs_object_name }}</p>
									</td>
									<td>
										<p>{{ $pap->allotment_type }}</p>
									</td>
									<td style="white-space: nowrap;">
										@if ($pap->moving_app)
											<p>&#8369; {{ number_format($pap->moving_app->actual_budget, 2, '.', ',') }}</p>
										@else
											<p>&#8369; {{ $pap->estimated_budget }}</p>
										@endif
									</td>
									<td style="height: 100px;">
										<div style="overflow: auto;width: 100%; height: inherit;">
											<ul>
												<?php foreach($modes as $mode): ?>
													<li><?php echo $mode; ?></li>
												<?php endforeach; ?>
											</ul>
										</div>
									</td>
									@foreach($pap->pap_schedules as $sched)
										<td style="white-space: nowrap;">
											<p>&#8369; {{ $sched->allocation }}</p>
										</td>
									@endforeach
								</tr>
								<?php $i++; endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3 form-group">
				<label for="">Total Approved Budget:</label>
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text">&#8369;</span>
					</div>
					<input type="text" class="form-control" value="{{ number_format($ppmp->actual_budget, 2, '.', ',') }}" readonly>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 form-group">
				<label for="">Prepared By: </label>
				<input type="text" class="form-control" value="{{ $ppmp->prepared_by }}" readonly>
			</div>
			<div class="col-md-6 form-group">
				<label for="">Approved By: </label>
				<input type="text" class="form-control" value="{{ $ppmp->approved_by }}" readonly>
			</div>
		</div>
	</div>
</div>
@endsection