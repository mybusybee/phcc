
@extends('layouts.base')

@section('content')

<link rel="stylesheet" type="text/css" href="{{ asset('css/ppmp/ppmp.css') }}">
<div class="container-fluid">
	<div class="row frm">
		<div class="col-md-12">
			<h1>Create Project Procurement Management Plan</h1>
			{{ Form::open(['url' => url('/ppmps'), 'method' => 'post']) }}
			<input type="hidden" name="" id="formType" value="create">
			<div class="row">
				<div class="col-md-4">
					<div class="form-group">
						<label for="end_user">END-USER/UNIT:</label>
						{{ Form::text('end_user', Auth::user()->office->office, ['class' => 'form-control', 'id' => 'end_user', 'required', 'readonly']) }}
					</div>
				</div>
				<div class="col-md-2 offset-md-1">
					<label for="end_user">PPMP for Year:</label>
					{{ Form::select('ppmp_year', ['' => 'Year'], null, ['class' => 'form-control', 'id' => 'ppmp_year', 'required']) }}
				</div>
				<div class="col-md-4 offset-md-1">
					<label for="end_user" style="display: none;">Assigned Director:</label>
					{{ Form::select('assigned_director', $directors, null, ['class' => 'form-control', 'style' => 'display:none' ,'required']) }}
				</div>
			</div>
			<input type="hidden" name="" id="user_role" value="{{ Auth::user()->user_role }}">
			<div class="row justify-content-center">
				<div class="col-md-12 form-group table-responsive" style="overflow-y: hidden;">
				{{-- <div class="col-md-12 form-group table-responsive" style="max-height: 500px; overflow: auto;"> --}}
					<table class="table table-hover table-striped text-center ppmp-tbl table-bordered">
						<thead class="table-primary">
							<tr>
								<th colspan="7"></th>
								<th colspan="12">Schedule/Milestones of Activities</th>
								<th class="table-danger"></th>
								<tr>
									<th style="min-width: 270px;">Code</th>
									<th>General Description</th>
									<th>Qty/Size</th>
									<th style="min-width: 380px;">UACS Object</th>
									<th>Allotment Type</th>
									<th>Est. Budget</th>
									<th>Mode of Procurement</th>
									<th>Jan</th>
									<th>Feb</th>
									<th>Mar</th>
									<th>Apr</th>
									<th>May</th>
									<th>Jun</th>
									<th>Jul</th>
									<th>Aug</th>
									<th>Sept</th>
									<th>Oct</th>
									<th>Nov</th>
									<th>Dec</th>
									<th class="table-danger"><button class="btn btn-primary invisible" type="button"> - </button></th>
								</tr>
							</tr>
						</thead>
						<tbody class="pap-body">
							<tr style="display: none;">
								<td style="display: none;">
									<select name="pap_mode_of_procurement[]" class="hidden-procurement-modes" >
										<option value="" disabled selected>Please select one</option>
										@foreach($procurement_modes as $mode)
										<option value="{{ $mode->id }}" data-min-value="{{ $mode->min_value != null ? $mode->min_value : 0 }}" data-max-value="{{ $mode->max_value != null ? $mode->max_value : 0 }}">{{ $mode->mode }}</option>
										@endforeach
									</select>
								</td>
								<td class="d-none">
									<select name="pap_uacs_object" id="">
										<option value="" disabled selected>Please select one</option>
										@foreach ($objects as $object)
											<option value="{{ $object->id }}" data-type="{{ $object->type }}">{{ $object->uacs_object_name }}</option>
										@endforeach
									</select>
								</td>
							</tr>
						</tbody>
						<tfoot>
							<tr>
								<td colspan="20" class="text-left"><button type="button" class="btn btn-raised btn-success add-pap">+</button></td>
							</tr>
						</tfoot>
					</table>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 col-sm-6 form-group">
					<label for="total_estimated_budget">Total Estimated Budget:</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text">&#8369;</span>
						</div>
						{{ Form::text('total_estimated_budget', 0, ['class' => 'form-control', 'id' => 'total_estimated_budget', 'required', 'readonly' => 'true']) }}
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 form-group">
					<label for="prepared_by">Prepared By:</label>
					<input type="text" required name="prepared_by" class="form-control" value="">
				</div>
				<div class="col-md-4 form-group">
					<label for="approved_by">Approved By:</label>
					{{ Form::text('approved_by', null, ['class' => 'form-control form-control-sm', 'required']) }}
				</div>
			</div>
			<div class="row">
				<div class="col-xl-2">
					<label for="draftBtn"><input type="radio" name="saveType" id="draftBtn" value="draft" required>Save as Draft</label>
				</div>
				<div class="col-xl-2">
					<label for="submitBtnRadio"><input type="radio" name="saveType" id="submitBtnRadio" value="submit" required>Submit</label>
				</div>
			</div>
			<div class="form-group">
				{{ Form::submit('Submit / Save as Draft', ['class' => 'btn btn-success btn-raised', 'id' => 'submit-btn', 'disabled' => true]) }}
			</div>
		</div>
		{{ Form::close() }}
	</div>
</div>
<script src="{{ asset('js/ppmp/allocation-computation.js') }}"></script>
<script src="{{ asset('js/ppmp/form-validation.js') }}"></script>
<script src="{{ asset('js/ppmp/currency-auto-format.js') }}"></script>
@endsection