@extends('layouts.base')

@section('content')
<link rel="stylesheet" type="text/css" href="{{ asset('css/ppmp/ppmp.css') }}">
<div class="container-fluid">
	<div class="frm center-div">
		@if($ppmp->remarks != null && session()->get('user_role') == 7)
		<div class="col-md-12">
			<div class="alert alert-danger show" role="alert">
				<div><p>" {{ $ppmp->remarks }} "</p></div>
				<footer class="mt-4">
					<cite> -- {{ $ppmp->remarks_author }}</cite>
				</footer>
			</div>
		</div>
		@elseif($ppmp->remarks != null && (session()->get('user_role') != 7 || session()->get('user_role') != 2))
		<div class="col-md-12">
			<div class="alert alert-info show" role="alert">
				<div><p>" {{ $ppmp->remarks }} "</p></div>
				<footer class="mt-4">
					<cite> -- {{ $ppmp->remarks_author }}</cite>
				</footer>
			</div>
		</div>
		@endif
		<div class="row">
			<div class="col-md-4 form-group">
				<label for="">END-USER/UNIT:</label>
				<input type="text" value="{{ $ppmp->office->office }}" readonly class="form-control">
			</div>
			<div class="col-md-2 offset-md-1" id="ppmp_year_container">
				<label for="end_user">PPMP for Year:</label>
				{{ Form::text('', $ppmp->for_year, ['class' => 'form-control', 'readonly']) }}
			</div>
		</div>
		<div class="">
			<div class="col-md-12 form-group">
				<div class="justify-content-center">
					<div class="col-md-12 form-group table-responsive" style="max-height: 500px; overflow: auto;">
						<table class="table table-hover table-striped text-center ppmp-tbl table-bordered">
							<thead class="table-primary">
								<tr>
									<th colspan="{{ Auth::user()->user_role === 6 ? '9' : '8' }}"></th>
									<th colspan="12">Schedule/Milestones of Activities</th>
									<tr>
										<th style="min-width: 150px;">Code</th>
										<th>General Description</th>
										@php $procBacUsers = [5,8]; @endphp
										@if(in_array(Auth::user()->user_role, $procBacUsers))
											<th>Category Code</th>
										@endif
										<th>Qty/Size</th>
										<th>UACS Object</th>
										<th>Allotment Type</th>
										<th>Est. Budget</th>
										<th>Mode of Procurement</th>
										<th>Jan</th>
										<th>Feb</th>
										<th>Mar</th>
										<th>Apr</th>
										<th>May</th>
										<th>Jun</th>
										<th>Jul</th>
										<th>Aug</th>
										<th>Sept</th>
										<th>Oct</th>
										<th>Nov</th>
										<th>Dec</th>
										@if(Auth::user()->user_role === 6)
											<th>Remarks</th>
										@endif
									</tr>
								</tr>
							</thead>
							<tbody class="pap-body">
								<td style="display: none;">
									<input type="text" class="form-control" value="Procurement mode here" readonly>
								</td>
								<?php $i = 1; foreach($ppmp->paps as $pap): ?>
								<?php
								$modes = [];
								foreach($pap->procurement_modes as $pap_mode){
									foreach($procurement_modes as $procurement_mode){
										if($pap_mode->id == $procurement_mode->id){
											$modes[] = $procurement_mode->mode;
										}
									}
								}
								?>
								<tr>
									<td style="display: none;">
										<input type="hidden" name="row_counter[]" value="<?php echo $i; ?>" />
									</td>
									<td>
										<p>{{ $pap->code }}</p>
									</td>
									<td>
										{{ $pap->general_description }}
									</td>
									@if (in_array(Auth::user()->user_role, $procBacUsers))
										<td>
											{{ $pap->category_code_field->category_code }}
										</td>
									@endif
									<td>
										<p>{{ $pap->size }}</p>
									</td>
									<td>
										<p>{{ $pap->uacs_object->uacs_object_name }}</p>
									</td>
									<td>
										<p>{{ strtoupper($pap->allotment_type) }}</p>
									</td>
									<td style="white-space: nowrap;">
										<p>&#8369; {{ $pap->estimated_budget }}</p>
									</td>
									<td style="height: 100px;">
										<div style="overflow: auto;width: 100%; height: inherit;">
											<ul>
												<?php foreach($modes as $mode): ?>
													<li><?php echo $mode; ?></li>
												<?php endforeach; ?>
											</ul>
										</div>
									</td>
									@foreach($pap->pap_schedules as $sched)
										<td style="white-space: nowrap;">
											<p>&#8369; {{ $sched->allocation }}</p>
										</td>
									@endforeach
									@if (Auth::user()->user_role === 6)
										<td>{{ $pap->budget_remarks }}</td>
									@endif
								</tr>
								<?php $i++; endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3 form-group">
				<label for="">Total Estimated Budget:</label>
				<div class="input-group">
					<div class="input-group-prepend">
						<span class="input-group-text">&#8369;</span>
					</div>
					<input type="text" class="form-control" value="{{ $ppmp->total_estimated_budget }}" readonly>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 form-group">
				<label for="">Prepared By: </label>
				<input type="text" class="form-control" value="{{ $ppmp->prepared_by }}" readonly>
			</div>
			<div class="col-md-6 form-group">
				<label for="">Approved By: </label>
				<input type="text" class="form-control" value="{{ $ppmp->approved_by }}" readonly>
			</div>
		</div>
		@if(
		(Auth::user()->user_role === 4 && ($ppmp->status === 'FOR_DIRECTOR_APPROVAL' || $ppmp->status === 'FOR_DIRECTOR_APPROVAL_FINAL'))
		)
				<div class="row">
					<div class="col-md-1">
						<div class="form-group">
						<a href="{{ url('/ppmps/'.$ppmp->id.'/approve') }}" class="btn btn-success">Approve</a>
						</div>
					</div>
					<div class="col-md-1">
						<div class="form-group">
							<button class="btn btn-danger w-100" type="button" data-toggle="modal" data-target="#rejectPPMPModal">Reject</button>
						</div>
					</div>
				</div>
				<div class="modal fade" id="rejectPPMPModal" tabindex="-1" role="dialog" aria-hidden="true">
					{{ Form::open(['url' => url('/ppmps/'.$ppmp->id.'/reject'), 'method' => 'put']) }}
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title">Reason for rejection</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<div class="container-fluid">
									<div class="row">
										<div class="col-md-12">
											{{ Form::textarea('ppmp_reject_remarks', null, ['class' => 'form-control']) }}
										</div>
									</div>
								</div>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
								<button type="submit" class="btn btn-primary">Submit</button>
							</div>
						</div>
					</div>
					{{ Form::close() }}
				</div>
		@endif
		@if(
			($ppmp->status === 'PPMP_APPROVED' && Auth::user()->user_role === 7) ||
			(Auth::user()->user_role === 6)
		)
			<a class="btn btn-success" href="{{ url('/export/ppmp/'.$ppmp->id) }}">Export PPMP</a>
		@endif
	</div>
</div>
@endsection