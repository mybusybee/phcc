<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/pmrtable.css') }}">
</head>
<body>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <div class="table-responsive">
        <table class="table">
            <tr>
                <th>{{ $ppmp->year }} PROJECT PROCUREMENT MANAGEMENT PLAN (PPMP)</th>
            </tr>
            <tr>
                <td><i>END-USER/UNIT: </i> <strong><u>{{ $ppmp->office->office }}</u></strong></td>
            </tr>
            <tr>
                <th><strong>Charged to GAA</strong></th>
            </tr>
            <tr>
                <td><i>Projects, Programs and Activities (PAPs)</i></td>
            </tr>
        </table>
    </div>
    <div class="table-responsive">
        <table class="table custom-table-bordered">
            <thead>
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th colspan="12">Schedule / Milestones of Activities</th>
                </tr>
                <tr>
                    <th style="width: 25;">Code</th>
                    <th>General Description</th>
                    <th>Qty/Size</th>
                    <th>Est.Budget</th>
                    <th>Mode of Procurement</th>
                    <th>Jan</th>
                    <th>Feb</th>
                    <th>Mar</th>
                    <th>Apr</th>
                    <th>May</th>
                    <th>Jun</th>
                    <th>Jul</th>
                    <th>Aug</th>
                    <th>Sept</th>
                    <th>Oct</th>
                    <th>Nov</th>
                    <th>Dec</th>
                </tr>
            </thead>
            <tbody>
                @foreach($ppmp->paps as $pap)
                    <tr>
                        <td>{{ $pap->code }}</td>
                        <td style="text-align: left;">{{ $pap->general_description }}</td>
                        <td style="text-align: right;">{{ $pap->size }}</td>
                        <td style="text-align: right;">&#8369; {{ $pap->estimated_budget }}</td>
                        @foreach($pap->procurement_modes as $mode)
                            <td style="text-align: left;">{{ $mode->mode }}</td>
                        @endforeach
                        @foreach($pap->pap_schedules as $sched)
                            <td style="text-align: right;">&#8369; {{ $sched->allocation }}</td>
                        @endforeach
                    </tr>
                @endforeach
                <tr>
                    <th colspan="2">TOTAL ESTIMATED BUDGET</th>
                    <th colspan="2" style="text-align: right;">&#8369; {{ $ppmp->total_estimated_budget }}</th>
                </tr>
            </tbody>
        </table>
    </div>
    <br>
    <div>
        <p><i>NOTE: Technical Specifications for each Item/Project being proposed shall be submitted as part of the PPMP.</i></p>
    </div>
    <div class="table-responsive">
        <table class="table">
            <tr>
                <th colspan="3">Prepared By:</th>
                <th colspan="3">Approved By:</th>
                <th colspan="3">Certified Funds Available:</th>
            </tr>
            <tr></tr>
            <tr></tr>
            <tr>
                <td colspan="3">_________________________</td>
                <td colspan="3">_________________________</td>
                <td colspan="3">_________________________</td>
            </tr>
        </table>
    </div>
    <br>
    <div class="table-responsive">
        <table class="table">
            <tr>
                <td><i>Generated as of {{ $ppmp->dateToday }}</i></td>
            </tr>
        </table>
    </div>
</body>
</html>