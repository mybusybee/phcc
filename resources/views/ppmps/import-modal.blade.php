{{ Form::button('Upload PPMP', ['class' => 'btn btn-success', 'data-toggle' => 'modal', 'data-target' => '#uploadModal']) }}
<div class="modal fade" tabindex="-1" role="dialog" id="uploadModal">
    <div class="modal-dialog" role="document">
        {{ Form::open(['url' => url('/import/ppmp'), 'method' => 'post', 'files' => true]) }}
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Upload new PPMP</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-left">
                    {{-- <div class="form-group">
                        {{ Form::label('End User: ') }}
                        <select name="end_user" id="" class="form-control" required>
                            @foreach ($offices as $office)
                                <option value="{{ $office->id }}">{{ $office->office }}</option>
                            @endforeach
                        </select>
                    </div> --}}
                    {{-- <div class="form-group">
                        {{ Form::label('Total Estimated Budget: ') }}
                        {{ Form::text('total_estimated_budget', null, ['class' => 'form-control total_estimated_budget', 'required']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('For Year: ') }}
                        {{ Form::select('for_year',[null => 'Please select one...'] , null, ['class' => 'form-control uploadForYear', 'required']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('Status: ') }}
                        {{ Form::select('status', [
                            null => 'Please select one...',
                            'FOR_END_USER_REVISION' => 'FOR_END_USER_REVISION',
                            'FOR_DIRECTOR_APPROVAL' => 'FOR_DIRECTOR_APPROVAL',
                            'FOR_BUDGET_VALIDATION' => 'FOR_BUDGET_VALIDATION',
                            'FOR_PROCUREMENT_VALIDATION' => 'FOR_PROCUREMENT_VALIDATION',
                            'FOR_DIRECTOR_APPROVAL_FINAL' => 'FOR_DIRECTOR_APPROVAL_FINAL',
                            'PPMP_APPROVED' => 'PPMP_APPROVED'
                        ], null, ['class' => 'form-control', 'required']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('Prepared By: ') }}
                        {{ Form::text('prepared_by', null, ['class' => 'form-control']) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('Approved By: ') }}
                        {{ Form::text('approved_by', null, ['class' => 'form-control', 'required']) }}
                    </div> --}}
                    Upload PPMP (xlsx/xls/csv files only): <input type="file" name="file" class="form-control" required accept=".xlsx,.xls,.csv">
                </div>
                <div class="modal-footer">
                    <a href="{{ url('/download-ppmp-template') }}" class="btn btn-info"><i class="fas fa-download"></i> Download Template</a>
                    {{ Form::submit('Import', ['class' => 'btn btn-success']) }}
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        {{ Form::close() }}
    </div>
</div>