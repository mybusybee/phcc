@extends('layouts.base')

@section('content')
<div class="container-fluid">
	<div class="row frm">
		<div class="col-xl-12">
			<div class="row">
				<div class="col-xl-10">
					<h1>Project Procurement Management Plan List</h1>
				</div>
				<div class="col-xl-2 d-flex align-items-center">
					@if (session()->get('user_role') == 7)
						<a href="{{ url('/ppmps/create') }}" class="btn btn-success pull-right w-100">Create new...</a>
					@endif
				</div>
			</div>
			@php $uploadPPMPUsers = [1, 5, 8]; @endphp
			@if (in_array(Auth::user()->user_role, $uploadPPMPUsers))
				<div class="row">
					<div class="col-xl-12 text-right">
						@include('ppmps.import-modal')
					</div>
				</div>
			@endif
			<div class="table-responsive col-xl-12 mt-3">
				<table class="table table-striped table-hover text-center table-bordered">
					<thead class="text-center table-primary">
						<th>Office</th>
						<th>Year</th>
						<th>Total Approved Budget</th>
						<th>Status</th>
						<th>Prepared By</th>
						<th>Approved By</th>
						<th>Action</th>
					</thead>
					<tbody>
						@foreach($ppmps as $ppmp)
						<tr class='clickable-row' data-href="{{ URL::to('ppmps/' . $ppmp->id)}}" style="cursor: pointer;">
							<td>{{ $ppmp->office->office }}</td>
							<td>{{ $ppmp->for_year }}</td>
							<td>&#8369;{{ $ppmp->total_estimated_budget }}</td>
							@if($ppmp->status == 'FOR_DIRECTOR_APPROVAL')
								<td class="status-for-director-approval">{{ $ppmp->status }}</td>
							@elseif($ppmp->status == 'FOR_BUDGET_VALIDATION')
								<td class="status-for-budget-approval">{{ $ppmp->status }}</td>
							@elseif($ppmp->status == 'FOR_END_USER_REVISION')
								<td class="status-for-end-user-revision">{{ $ppmp->status }}</td>
							@elseif($ppmp->status == 'FOR_DIRECTOR_APPROVAL_FINAL')
								<td class="status-for-director-approval-final">{{ $ppmp->status }}</td>
							@elseif($ppmp->status == 'FOR_PROCUREMENT_VALIDATION')
								<td class="status-for-procurement-approval">{{ $ppmp->status }}</td>
							@elseif($ppmp->status == 'PPMP_APPROVED')
								<td class="status-ppmp-approved">{{ $ppmp->status }}</td>
							@else
								<td class="">{{ $ppmp->status }}</td>
							@endif
							<td>{{ $ppmp->prepared_by }}</td>
							<td>{{ $ppmp->approved_by }}</td>
							<td>
								<!-- if user is admin or god -->
								@if(session()->get('user_role') == 1)
									<a href="{{ url('/ppmps/'.$ppmp->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
									<a href="{{ url('/ppmps/'.$ppmp->id.'/edit') }}" class="btn btn-info"><i class="fa fa-edit"></i></a>
									{{ Form::open(array('url' => url('/ppmps/' . $ppmp->id), 'style' => 'display: inline')) }}
									{{ Form::hidden('_method', 'DELETE') }}
									<button type="submit" class="btn btn-danger" id="delete_btn"><i class="fa fa-trash"></i></button>
									{{ Form::close() }}

								@elseif(session()->get('user_role') == 3)
									<a href="{{ url('/ppmps/'.$ppmp->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
	
								<!-- if user is end user -->
								@elseif(session()->get('user_role') == 7)

									<!-- if status is for director approval and for user revision-->
									@if($ppmp->status == 'FOR_DIRECTOR_APPROVAL' || $ppmp->status == 'FOR_END_USER_REVISION' || $ppmp->status == 'DRAFT')
										<a href="{{ url('/ppmps/'.$ppmp->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
										{{-- @if( (Auth::user()->user_role === 1) )
											<a href="{{ url('/ppmps/'.$ppmp->id.'/edit') }}" class="btn btn-info"><i class="fa fa-edit"></i></a>
										@endif --}}

										<button class="btn btn-danger" data-toggle="modal" data-target="#deletePPMPModal" ><i class="fa fa-trash"></i></button>
									<!-- if not for director approval-->
									@else
										<a href="{{ url('/ppmps/'.$ppmp->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
									@endif

								<!-- if user is procurement -->
								{{-- @elseif(session()->get('user_role') == 5)
									<a href="{{ url('/ppmps/'.$ppmp->id.'/edit') }}" class="btn btn-info"><i class="fa fa-edit"></i></a> --}}
								@elseif(session()->get('user_role') == 5)
									<a href="{{ url('/ppmps/'.$ppmp->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>

								@elseif(session()->get('user_role') == 6)
									@if ($ppmp->status == 'FOR_BUDGET_VALIDATION')
										<a href="{{ url('/ppmps/'.$ppmp->id.'/edit') }}" class="btn btn-info"><i class="fa fa-edit"></i></a>
										<a href="{{ url('/ppmps/'.$ppmp->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
									@else
										<a href="{{ url('/ppmps/'.$ppmp->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
									@endif
								
								@elseif(session()->get('user_role') == 8)
									<a href="{{ url('/ppmps/'.$ppmp->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
									@if ($ppmp->status == 'FOR_PROCUREMENT_VALIDATION')
										<a href="{{ url('/ppmps/'.$ppmp->id.'/edit') }}" class="btn btn-info"><i class="fa fa-edit"></i></a>
									@endif
								<!-- if other user roles other than admin, god and end user -->
								@else
									<a href="{{ url('/ppmps/'.$ppmp->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
								@endif
							</td>
							<div class="modal fade" id="deletePPMPModal" tabindex="-1" role="dialog" aria-hidden="true">
						        <div class="modal-dialog" role="document">
						            <div class="modal-content">
						                <div class="modal-header">
						                    <h5 class="modal-title">Delete PPMP</h5>
						                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
						                        <span aria-hidden="true">&times;</span>
						                    </button>
						                </div>
						                <div class="modal-body">
						                    <div class="container-fluid">
						                        <div class="row">
						                            <div class="col-md-12">
						                                <h3>Are you sure?</h3>
						                            </div>
						                        </div>
						                    </div>
						                </div>
						                <div class="modal-footer">
						                    <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
						                    <a href="{{ url('/ppmps/'.$ppmp->id.'/delete') }}" class="btn btn-danger">Yes</a>
						                </div>
						            </div>
						        </div>
						    </div>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="col-xl-4 col-lg-7 col-xl-5">
				<div class="card">
					<div class="card-header bg-primary">
						Status Column Legend:
					</div>
					<div class="card-body bg-light">
						<div class="col-xl-12 d-flex align-items-center">
							<div class="legend-box status-for-end-user-revision"></div>
							<span>FOR_END_USER_REVISION</span>
						</div>
						<div class="col-xl-12 d-flex align-items-center">
							<div class="legend-box status-for-director-approval"></div>
							<span>FOR_DIRECTOR_APPROVAL</span>
						</div>
						<div class="col-xl-12 d-flex align-items-center">
							<div class="legend-box status-for-budget-approval"></div>
							<span>FOR_BUDGET_VALIDATION</span>
						</div>
						<div class="col-xl-12 d-flex align-items-center">
							<div class="legend-box status-for-procurement-approval"></div>
							<span>FOR_PROCUREMENT_VALIDATION</span>
						</div>
					<div class="col-xl-12 d-flex align-items-center">
						<div class="legend-box status-for-director-approval-final"></div>
						<span>FOR_DIRECTOR_APPROVAL_FINAL</span>
					</div>
						<div class="col-xl-12 d-flex align-items-center">
							<div class="legend-box status-ppmp-approved"></div>
							<span>PPMP_APPROVED</span>
						</div>
					</div>
				</div>
			</div>
		</div>
		{{-- <nav class="fab-container"> 
			<a href="#" class="buttons btn btn-success" tooltip="Create new">Create new PPMP</a>
			<a href="#" class="buttons btn btn-success" tooltip="Upload">Upload PPMP</a>
			<a class="buttons" tooltip="Settings" href="#"></a>
		</nav> --}}
	</div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="{{ asset('js/ppmpindex.js') }}"></script>

@endsection