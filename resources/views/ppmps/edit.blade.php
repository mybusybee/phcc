@extends('layouts.base')

@section('content')
<link rel="stylesheet" href="{{ asset('css/multiselect.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/ppmp/ppmp.css') }}">
<div class="container-fluid">
	<div class="row frm">
		@if($ppmp->remarks != null && session()->get('user_role') == 7)
			<div class="col-md-12">
				<div class="alert alert-danger show" role="alert">
					<div><p>" {{ $ppmp->remarks }} "</p></div>
					<footer class="mt-4">
						<cite> -- {{ $ppmp->remarks_author }}</cite>
					</footer>
				</div>
			</div>
		@elseif($ppmp->remarks != null && (session()->get('user_role') == 1 || session()->get('user_role') == 3))
			<div class="col-md-12">
				<div class="alert alert-info show" role="alert">
					<div><p>" {{ $ppmp->remarks }} "</p></div>
					<footer class="mt-4">
						<cite> -- {{ $ppmp->remarks_author }}</cite>
					</footer>
				</div>
			</div>
		@endif
		<div class="col-md-12">
			{{ Form::open(['url' => url('/ppmps/'.$ppmp->id), 'method' => 'put', 'id' => 'ppmpform']) }}
			<div class="row">
				<div class="col-md-4">
					@if(session()->get('user_role') == 7)
					<div class="form-group">
						<label for="end_user">END-USER/UNIT:</label>
						{{ Form::text('end_user', $ppmp->office->office, ['class' => 'form-control', 'id' => 'end_user', 'required', 'readonly']) }}
					</div>
					@elseif(session()->get('user_role') == 8 || session()->get('user_role') == 6)
					<div class="form-group">
						<label for="end_user">END-USER/UNIT:</label>
						{{ Form::text('end_user', $ppmp->office->office, ['class' => 'form-control', 'id' => 'end_user', 'required', 'readonly']) }}
					</div>
					@endif
				</div>
				<div class="col-md-2 offset-md-1" id="ppmp_year_container">
					<label for="end_user">PPMP for Year:</label>
					@if(session()->get('user_role') == 7)
					{{ Form::select('ppmp_year', ['' => 'Year'], $ppmp->for_year, ['class' => 'form-control', 'id' => 'ppmp_year', 'required']) }}
					<input type="hidden" class="form-control" value="{{ $ppmp->for_year }}" readonly id="ppmp_year_hidden">
					@elseif(session()->get('user_role') == 8 || session()->get('user_role') == 6)
					{{ Form::text('ppmp_year', $ppmp->for_year, ['class' => 'form-control', 'readonly']) }}
					@endif
				</div>
			</div>
			
			<div class="justify-content-center">
				<div class="col-md-12 form-group table-responsive" style="max-height: 500px; overflow: auto;">
					<table class="table table-hover table-striped text-center ppmp-tbl table-bordered">
						<thead class="table-primary">
							<tr>
								<th colspan="9"></th>
								<th colspan="{{ Auth::user()->user_role == 6 ? 14 : 12 }}">Schedule/Milestones of Activities</th>
								@if(session()->get('user_role') == 7)
								<th class="table-danger"></th>
								@endif
								<tr>
									<th style="min-width: 270px;">Code</th>
									<th>General Description</th>
									@php $procBacUsers = [5,8]; @endphp
									@if(in_array(Auth::user()->user_role, $procBacUsers))
										<th style="min-width: 350px;">Category Code</th>
									@endif
									<th>Qty/Size</th>
									<th style="min-width: 380px;">UACS Object</th>
									<th>Allotment Type</th>
									<th>Est. Budget</th>
									<th style="min-width: 300px;">Mode of Procurement</th>
									<th>Jan</th>
									<th>Feb</th>
									<th>Mar</th>
									<th>Apr</th>
									<th>May</th>
									<th>Jun</th>
									<th>Jul</th>
									<th>Aug</th>
									<th>Sept</th>
									<th>Oct</th>
									<th>Nov</th>
									<th>Dec</th>
									<th style="min-width: 500px;">Budget Remarks</th>
									<th style="min-width: 500px;">Procurement Remarks</th>
									@if(session()->get('user_role') == 7)
										<th class="table-danger"><button class="btn btn-primary invisible" type="button"> - </button></th>
									@endif
								</tr>
							</tr>
						</thead>
						<tbody class="pap-body">
							<td class="d-none">
								<select name="pap_mode_of_procurement[]" class="hidden-procurement-modes" >
									<option value="" disabled selected>Please select one</option>
									@foreach($procurement_modes as $mode)
									<option value="{{ $mode->id }}" data-min-value="{{ $mode->min_value != null ? $mode->min_value : 0 }}" data-max-value="{{ $mode->max_value != null ? $mode->max_value : 0 }}">{{ $mode->mode }}</option>
									@endforeach
								</select>
							</td>
							<td class="d-none">
								<select name="pap_uacs_object" id="">
									<option value="" disabled selected>Please select one</option>
									@foreach ($objects as $object)
										<option value="{{ $object->id }}" data-type="{{ $object->type }}">{{ $object->uacs_object_name }}</option>
									@endforeach
								</select>
							</td>
							<?php $i = 1; foreach($ppmp->paps as $pap): ?>
							<?php
								$mode_name = [];
								$selected_modes = [];
								foreach($pap->procurement_modes as $modes){
									$selected_modes[] = $modes->id;
									$mode_names[] = $modes->mode;
								}
							?>
							<tr style="position: relative;" id="{{$i}}">
								<td class="d-none">
									<input type="text" name="row_counter[]" class="row_counter" value="<?php echo $i; ?>" />
								</td>
								<td class="d-none">
									<input type="hidden" value="{{ $pap->id }}" name="prev_pap_id[]">
								</td>
								<td class="d-none">
									<input type="hidden" value="{{ $pap->id }}" name="id[]">
								</td>
								<td>
									@if(session()->get('user_role') == 6 || session()->get('user_role') == 8)
										{{ Form::text('pap_code'.$i, $pap->code, ['class' => 'form-control pap_code', 'readonly', 'required']) }}
									@elseif(Auth::user()->user_role === 7)
										{{ Form::text('pap_code'.$i, $pap->code, ['class' => 'form-control pap_code', 'required']) }}
									@endif
								</td>
								<td>
									@if(session()->get('user_role') == 7 || Auth::user()->user_role === 8)
										{{ Form::textarea('pap_gen_desc'.$i, $pap->general_description, ['class' => 'form-control pap_gen_desc gen-desc', 'rows' => 2, 'required']) }}
									@elseif(session()->get('user_role') == 6)
										{{ Form::textarea('pap_gen_desc'.$i, $pap->general_description, ['class' => 'form-control pap_gen_desc gen-desc', 'rows' => 2, 'readonly', 'required']) }}
									@endif
								</td>
								@if(in_array(Auth::user()->user_role, $procBacUsers))
									<td>
										<select name="{{ 'category_code'.$i }}" id="" class="form-control category_code">
											@foreach ($category_codes as $cc)
												<option value="{{ $cc->id }}" {{ $pap->category_code === $cc->id ? 'selected' : null }}>{{ $cc->category_code }}</option>
											@endforeach
										</select>
									</td>
								@endif
								<td>
									@if(session()->get('user_role') == 7)
										{{ Form::text('pap_qty'.$i, $pap->size, ['class' => 'form-control pap_qty', 'required']) }}
									@elseif(session()->get('user_role') == 8 || session()->get('user_role') == 6)
										{{ Form::text('pap_qty'.$i, $pap->size, ['class' => 'form-control pap_qty', 'readonly', 'required']) }}
									@endif									
								</td>
								<td>
									@if (Auth::user()->user_role === 7)
										<select name="pap_uacs_object{{ $i }}" id="" class="form-control pap-uacs-object" required>
											@foreach($objects as $object)
												<option value="{{ $object->id }}" data-type="{{ $object->type }}" {{ $pap->uacs_object_id === $object->id ? 'selected' : '' }}>{{ $object->uacs_object_name }}</option>
											@endforeach
										</select>
									@else
										<input type="hidden" name="pap_uacs_object{{ $i }}" value="{{ $pap->uacs_object->id }}">
										<input type="text" class="form-control" value="{{ $pap->uacs_object->uacs_object_name }}" readonly>
									@endif
								</td>
								<td>
									{{ Form::text('allotment_type'.$i, $pap->allotment_type, ['class' => 'form-control', 'readonly', 'required']) }}
								</td>
								<td>
									@if(session()->get('user_role') == 7)
									<div class="input-group pap_est_budget_container">
										<div class="input-group-prepend">
											<span class="input-group-text">&#8369;</span>
										</div>
										{{ Form::text('pap_est_budget'.$i, $pap->estimated_budget, ['class' => 'form-control pap_est_budget', 'readonly', 'required']) }}
									</div>
									@elseif(session()->get('user_role') == 8 || session()->get('user_role') == 6)
										<div class="input-group pap_est_budget_container">
											<div class="input-group-prepend">
												<span class="input-group-text">&#8369;</span>
											</div>
											{{ Form::text('pap_est_budget'.$i, $pap->estimated_budget, ['class' => 'form-control pap_est_budget', 'readonly', 'required']) }}
										</div>
									@endif
								</td>
								@if(session()->get('user_role') == 8 || session()->get('user_role') == 7)
									@if(count($selected_modes) > 1)
									<td class="table-danger" data-toggle="tooltip" data-placement="right" title="More than 1 procurement mode has been selected by the end user.">
										{!! Form::select('pap_mode_of_procurement'.$i, $procurement_modes, $selected_modes, ['class' => 'form-control procurement_modes_select d-none', 'required'] ) !!}
									</td>
									@else
									<td>
										<select name="pap_mode_of_procurement{{ $i }}" id="" class="form-control procurement_modes_select" required>
											@foreach($procurement_modes as $mode)
												@if($selected_modes[0] == $mode->id)
													<option value="{{ $mode->id }}" data-min-value="{{ $mode->min_value != null ? $mode->min_value : 0 }}" data-max-value="{{ $mode->max_value != null ? $mode->max_value : 0 }}" selected>{{ $mode->mode }}</option>
												@else
													<option value="{{ $mode->id }}" data-min-value="{{ $mode->min_value != null ? $mode->min_value : 0 }}" data-max-value="{{ $mode->max_value != null ? $mode->max_value : 0 }}">{{ $mode->mode }}</option>
												@endif
											@endforeach
										</select>
									</td>
									@endif
								@else
								<td style="height: 100px;">
										<div style="overflow: auto;width: 100%; height: inherit;">
											<ul>
												<input type="hidden" id="hidden_session" value="{{ session()->get('user_role') }}">
												<input type="hidden" name='pap_mode_of_procurement{{$i}}' value="{{$selected_modes[0]}}">
												<li><?php echo $mode_names[$i-1]; ?></li>
											</ul>
										</div>
									</td>
								@endif

								@foreach($pap->pap_schedules as $sched)
									<td>
										<div class="input-group pap_est_budget_container">
											<div class="input-group-prepend">
												<span class="input-group-text">&#8369;</span>
											</div>
											{{ Form::text('allocation'.$i.'[]', $sched->allocation, ['class' => 'form-control allocation-month alloc-month-'.$i, Auth::user()->user_role == 7 ? '' : 'readonly', 'required']) }}
										</div>
									</td>
								@endforeach

								<td>
									@if (Auth::user()->user_role === 6)
										{{ Form::textarea('budget_remarks[]', $pap->budget_remarks, ['class' => 'form-control remarks', 'rows' => 3]) }}
									@else
										{{ Form::textarea('budget_remarks[]', $pap->budget_remarks, ['class' => 'form-control remarks', 'readonly', 'rows' => 3]) }}
									@endif
								</td>
								<td>
									@if (Auth::user()->user_role === 8)
										{{ Form::textarea('procurement_remarks[]', $pap->procurement_remarks, ['class' => 'form-control procurement_remarks', 'rows' => 3]) }}
									@else
										{{ Form::textarea('procurement_remarks[]', $pap->procurement_remarks, ['class' => 'form-control procurement_remarks', 'readonly', 'rows' => 3]) }}
									@endif
								</td>

								@if(session()->get('user_role') == 7)
									<td><button type="button" class="btn btn-danger remove-pap-row"> - </button></td>
								@endif

							</tr>
							<?php $i++; endforeach; ?>
						</tbody>
						@if(session()->get('user_role') == 7)
							<tfoot>
								<tr>
									<td colspan="20" class="text-left"><button type="button" class="btn btn-success add-pap">+</button></td>
								</tr>
							</tfoot>
						@endif
					</table>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 col-sm-6 form-group">
					<label for="total_estimated_budget">Total Estimated Budget:</label>
					<div class="input-group">
						<div class="input-group-prepend">
							<span class="input-group-text">&#8369;</span>
						</div>
						{{ Form::text('total_estimated_budget', $ppmp->total_estimated_budget, ['class' => 'form-control', 'id' => 'total_estimated_budget', 'required', 'readonly' => 'true']) }}
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 form-group">
					<label for="prepared_by">Prepared By:</label>
					<input type="text" name="prepared_by" class="form-control" {{ Auth::user()->user_role !== 7 ? 'readonly' : null }} value="{{ $ppmp->prepared_by }}" required>
				</div>
				<div class="col-md-4 form-group">
					<label for="approved_by">Approved By:</label>
					@if(session()->get('user_role') == 7)
						{!! Form::text('approved_by', $ppmp->approved_by, ['class' => 'form-control', 'id' => 'approved_by']) !!}
					@elseif(session()->get('user_role') == 8 || session()->get('user_role') == 6)
						{!! Form::text('approved_by2', $ppmp->approved_by, ['class' => 'form-control', 'id' => 'approved_by', 'disabled']) !!}
						<input type="hidden" name="approved_by" value="{{ $ppmp->approved_by }}">
					@endif
				</div>
			</div>
			@if(Auth::user()->user_role === 7 && $ppmp->status === 'DRAFT')
				<div class="row">
					<div class="col-xl-2">
						<label for="draftBtn"><input type="radio" name="saveType" id="draftBtn" value="draft" required>Update Draft</label>
					</div>
					<div class="col-xl-2">
						<label for="submitBtnRadio"><input type="radio" name="saveType" id="submitBtnRadio" value="submit" required>Submit</label>
					</div>
				</div>
				<div class="row">
					<div class="col-xl-12">
						{{ Form::submit('Update', ['class' => 'btn btn-success', 'id' => 'submit-btn']) }}
					</div>
				</div>
			@endif
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						@if(session()->get('user_role') == 8)
							{{ Form::submit('Validate', ['class' => 'btn btn-success procurementValidate', 'id' => 'submit-btn']) }}
						@elseif(session()->get('user_role') == 6)
							{{ Form::button('Within WFP', ['class' => 'btn btn-success within-wfp-submit', 'id' => 'submit-btn']) }}
							<a class="btn btn-success" href="{{ url('/export/ppmp/'.$ppmp->id) }}">Export PPMP</a>
						@else
							@if($ppmp->status !== 'DRAFT')
								<label for="draftBtn"><input type="radio" name="saveType" id="draftBtn" value="draft" required checked>Update Draft</label>
								<label for="submitBtnRadio"><input type="radio" name="saveType" id="submitBtnRadio" value="submit" required>Submit</label>
								{{ Form::submit('Update', ['class' => 'btn btn-success', 'id' => 'submit-btn']) }}
							@endif
						@endif
					</div>
				</div>
				@if(session()->get('user_role') == 8)
					<div class="col-md-1">
						<div class="form-group">
							<button class="btn btn-danger w-100" id="procRejectBtn" type="button" disabled>Reject</button>
						</div>
					</div>
					<input type="hidden" name="procValidation" value="" id="hiddenProcValidation">
				@elseif(Auth::user()->user_role === 6)
					<div class="col-md-1">
						<div class="form-group">
							<button class="btn btn-danger w-100" type="button" disabled id="budgetRejectBtn">Reject</button>
						</div>
					</div>
					<input type="hidden" name="budgetValidation" value="" id="hiddenBudgetValidation">
				@endif
			</div>
			{{ Form::close() }}
		</div>
		@if(session()->get('user_role') == 8 || session()->get('user_role') == 6)
		<div class="modal fade" id="rejectPPMPModal" tabindex="-1" role="dialog" aria-hidden="true">
			{{ Form::open(['url' => url('/ppmps/'.$ppmp->id.'/reject'), 'method' => 'put']) }}
			@foreach ($pap_ids_array as $pap_id)
				<input type="hidden" name="hidden_pap_id[]" value="{{ $pap_id }}">
			@endforeach
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title">Reason for rejection</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="container-fluid">
							<div class="row">
								<div class="col-md-12">
									{{ Form::textarea('ppmp_reject_remarks', null, ['class' => 'form-control', 'required']) }}
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Submit</button>
					</div>
				</div>
			</div>
			{{ Form::close() }}
		</div>
		@endif
	</div>
</div>
<script src="{{ asset('js/ppmp/allocation-computation.js') }}"></script>
<script src="{{ asset('js/ppmp/form-validation.js') }}"></script>
<script src="{{ asset('js/ppmp/edit.js') }}"></script>
@endsection