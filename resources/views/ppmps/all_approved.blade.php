@extends('layouts.base')

@section('content')
<link rel="stylesheet" href="{{ asset('css/ppmp/ppmp_approved.css') }}">
<div class="container-fluid">
	<div class="row frm">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-4">
					<h1>PPMP List</h1>
				</div>
				<div class="col-md-2 offset-md-6 d-flex align-items-center">
					<a href="{{ url('/ppmps/create') }}" class="btn btn-success pull-right w-100">Create new...</a>
				</div>
			</div>
			{{ Form::open(['url' => url("generate-app"), 'method' => 'get']) }}
			<div class="table-responsive col-md-12">
				<table class="table table-striped table-hover text-center table-bordered approved_ppmps">
					<thead class="text-center table-primary">
						<th><i class="fa fa-check-square-o" aria-hidden="true"></i></th>
						<th>End User</th>
						<th>Year</th>
						<th>Total Estimated Budget</th>
						<th>Status</th>
						<th>Prepared By</th>
						<th>Action</th>
					</thead>
					<tbody>
						@foreach($ppmps as $ppmp)
						<tr class='all-ppmp-clickable-row' data-href="{{ URL::to('ppmps/' . $ppmp->id)}}" style="cursor: pointer;">
							<td><input type="checkbox" class="generate_app_cb" name="p_id[]" value="{{ $ppmp->id }}"/></td>
							<td>{{ $ppmp->office->office }}</td>
							<td class="year-cell">{{ $ppmp->for_year }}</td>
							<td>{{ $ppmp->total_estimated_budget }}</td>
							@if($ppmp->status == 'FOR_DIRECTOR_APPROVAL')
								<td class="status-for-director-approval">{{ $ppmp->status }}</td>
							@elseif($ppmp->status == 'FOR_BUDGET_APPROVAL')
								<td class="status-for-budget-approval">{{ $ppmp->status }}</td>
							@elseif($ppmp->status == 'FOR_END_USER_REVISION')
								<td class="status-for-end-user-revision">{{ $ppmp->status }}</td>
							@elseif($ppmp->status == 'FOR_DIRECTOR_APPROVAL_FINAL')
								<td class="status-for-director-approval-final">{{ $ppmp->status }}</td>
							@elseif($ppmp->status == 'FOR_PROCUREMENT_APPROVAL')
								<td class="status-for-procurement-approval">{{ $ppmp->status }}</td>
							@elseif($ppmp->status == 'PPMP_APPROVED')
								<td class="status-ppmp-approved">{{ $ppmp->status }}</td>
							@endif
							<td>{{ $ppmp->created_by }}</td>
							<td>							
								<!-- if user is admin or god -->
								@if(session()->get('user_role') == 1 || session()->get('user_role') == 3)
								<a href="{{ url('/ppmps/'.$ppmp->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
								<a href="{{ url('/ppmps/'.$ppmp->id.'/edit') }}" class="btn btn-info"><i class="fa fa-edit"></i></a>
								{{ Form::open(array('url' => url('/ppmps') . $ppmp->id, 'style' => 'display: inline')) }}
								{{ Form::hidden('_method', 'DELETE') }}
								<button type="submit" class="btn btn-danger" id="delete_btn"><i class="fa fa-trash"></i></button>
								{{ Form::close() }}

								<!-- if user is end user -->
								@elseif(session()->get('user_role') == 7)

								<!-- if status is for director approval and for user revision-->
								@if($ppmp->status == 'FOR_DIRECTOR_APPROVAL' || $ppmp->status == 'FOR_END_USER_REVISION')
									<a href="{{ url('/ppmps/'.$ppmp->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
									<a href="{{ url('/ppmps/'.$ppmp->id.'/edit') }}" class="btn btn-info"><i class="fa fa-edit"></i></a>
								<!-- if not for director approval-->
								@else
									<a href="{{ url('/ppmps/'.$ppmp->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
								@endif

								<!-- if user is procurement -->
								@elseif(session()->get('user_role') == 5)
									<a href="{{ url('/ppmps/'.$ppmp->id.'/edit') }}" class="btn btn-info"><i class="fa fa-edit"></i></a>

								<!-- if other user roles other than admin, god and end user -->
								@else
									<a href="{{ url('/ppmps/'.$ppmp->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
								@endif
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="col-xl-2 form-group">
				@if( ($apps === 0))
					{{ Form::submit('Generate APP', ['class' => 'btn btn-success w-100']) }}
				@endif
			</div>
			{{ Form::close() }}
			<div class="col-xl-4 col-lg-7 col-md-8">
				<div class="card">
					<div class="card-header bg-primary">
						Status Column Legend:
					</div>
					<div class="card-body bg-light">
						<div class="col-md-12 d-flex align-items-center">
							<div class="legend-box status-for-end-user-revision"></div>
							<span>FOR_END_USER_REVISION</span>
						</div>
						<div class="col-md-12 d-flex align-items-center">
							<div class="legend-box status-for-director-approval"></div>
							<span>FOR_DIRECTOR_APPROVAL</span>
						</div>
						<div class="col-md-12 d-flex align-items-center">
							<div class="legend-box status-for-budget-approval"></div>
							<span>FOR_BUDGET_VALIDATION</span>
						</div>
						<div class="col-md-12 d-flex align-items-center">
							<div class="legend-box status-for-director-approval-final"></div>
							<span>FOR_DIRECTOR_APPROVAL_FINAL</span>
						</div>
						<div class="col-md-12 d-flex align-items-center">
							<div class="legend-box status-for-procurement-approval"></div>
							<span>FOR_PROCUREMENT_APPROVAL</span>
						</div>
						<div class="col-md-12 d-flex align-items-center">
							<div class="legend-box status-ppmp-approved"></div>
							<span>PPMP_APPROVED</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="{{ asset('js/approved_ppmps.js') }}"></script>
@push('scripts')
	<script>
		$("input[type='submit']").on('click', event => {
			$(this).closest('form').submit()
		})
	</script>
@endpush
@endsection