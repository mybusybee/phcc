@extends('layouts.base')

@section('content')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<div class="container-fluid">
	<div class="row frm">
		<div class="col-xl-12">
			<div class="row">
				<div class="col-xl-10">
					<h1>Project Procurement Management Plan List (Approved Budget)</h1>
				</div>
			</div>
			<div class="table-responsive col-xl-12">
				<table class="table table-striped table-hover text-center table-bordered" id="movingPPMPTable">
					<thead class="text-center table-primary">
						<th>End User</th>
						<th>Year</th>
						<th>Total Approved Budget</th>
						<th>Prepared By</th>
						<th>Approved By</th>
						<th>Action</th>
					</thead>
					<tbody>
						@foreach($ppmps as $ppmp)
						<tr class='clickable-row' data-href="{{ URL::to('ppmps/' . $ppmp->id)}}" style="cursor: pointer;">
							<td>{{ $ppmp->office->office }}</td>
							<td>{{ $ppmp->for_year }}</td>
							<td>&#8369;{{ number_format($ppmp->actual_budget, 2, '.', ',')}}</td>
							<td>{{ $ppmp->prepared_by }}</td>
							<td>{{ $ppmp->approved_by }}</td>
							<td>
								<a href="{{ url('/ppmps/'.$ppmp->id.'/show-moving') }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
<script src="{{ asset('js/ppmpindex.js') }}"></script>
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
    $('#movingPPMPTable').DataTable();
</script>
@endsection