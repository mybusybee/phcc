@extends('layouts.base')

@section('content')
    <div class="frm" style="min-height: 80vh;">
        <div class="row mb-5">
            <div class="col-xl-6">
                <h3><input type="text" readonly value="{{ $announcement->title }}" class="form-control"></h3>
            </div>
            <div class="col-xl-6 text-right">
                @if (Auth::user()->user_role === 1 || Auth::user()->user_role === 5)
                    <a href="{{ url('announcement/'.$announcement->id.'/edit') }}" class="btn btn-info"><i class="fa fa-edit"></i></a>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12">
                <textarea name="" id="" class="form-control" readonly>{{ $announcement->content }}</textarea>
            </div>
        </div>
    </div>
@endsection