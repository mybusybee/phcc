@extends('layouts.base')

@section('content')
    <div class="frm">
        <h4>Create new announcement</h4>
        {{ Form::open(['url' => url('/announcement'), 'method' => 'post']) }}
            <div class="row">
                <div class="col-xl-12 mb-5">
                    <h3>Title:</h3>
                    {{ Form::text('title', null, ['class' => 'form-control form-control-sm border border-info', 'required']) }}
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12 mb-3">
                    <h3>Content:</h3>
                    {{ Form::textarea('content', null, ['class' => 'form-control form-control-sm border border-info', 'required']) }}
                </div>
            </div>
            <input type="submit" value="Save" class="btn btn-success">
        {{ Form::close() }}
    </div>
@endsection