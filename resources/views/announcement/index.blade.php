@extends('layouts.base')

@section('content')
    <div class="frm">
        <div class="row">
            <div class="col-xl-6">
                <h3>Announcements</h3>
            </div>
            <div class="col-xl-6 text-right">
                @if (Auth::user()->user_role === 1 || Auth::user()->user_role === 5)
                    <a href="{{ url('announcement/create') }}" class="btn btn-success">Create new</a>
                @endif
            </div>
        </div>
        <hr>
        <div class="row">
            @if(count($announcements))
                @foreach($announcements as $announcement)
                    <div class="col-xl-4 mb-5">
                        <div class="card text-center">
                            <div class="card-header bg-primary">
                                {{ $announcement->title }}
                            </div>
                            <div class="card-body">
                                <p class="card-text">{{ $announcement->content = (strlen($announcement->content) > 225) ? substr($announcement->content,0,225).'...' : $announcement->content
                                    }}</p>
                                <a href="{{ url('announcement/'.$announcement->id) }}" class="btn btn-primary btn-xs mt-3">See more...</a>
                            </div>
                            <div class="card-footer text-muted">
                                <p>Created {{ $announcement->created_at->diffForHumans() }}</p>
                                <p>Last Updated {{ $announcement->updated_at->diffForHumans() }}</p>
                                <p>By: {{ $announcement->user->fullName }}</p>
                            </div>
                        </div>
                    </div>
                @endforeach
            @else
                <div class="col-xl-12">
                    <h5>No announcement yet.</h5>
                    @if(Auth::user()->user_role === 1 || Auth::user()->user_role === 5)
                        <a href="{{ url('announcement/create') }}" class="text-success">Create one?</a>
                    @endif
                </div>
            @endif
        </div>
        {{ $announcements->links() }}
    </div>
@endsection