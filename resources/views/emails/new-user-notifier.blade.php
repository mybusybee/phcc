<h2>Welcome to the PCC Procurement Monitoring System.</h2>
<p>	Here is your system credentials: </p><br>
<p>Email: <b><i>{{$user_email}}</i></b></p>
<p>Password: <i><b>{{$user_password}}</b></i></p>
<p>You can access the system through this <a href="{{$url}}">link</a></p>