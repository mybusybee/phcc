<p>Dear Sir/Madam:</p> 

<p>Good day!</p>
<p>The Philippine Competition Commission (PCC) is pleased to invite {{ $supplier }} to submit your lowest price quotation for the {{ $title }} with the approved budget of PhP {{ $total }}.</p> 

<p>Please see instructions/details in the attached Request for Quotation (RFQ) and Terms of Reference (TOR). </p>
<p>Kindly submit your quotation in together with all documentary requirements on/before {{ $deadline }}.Please acknowledge receipt of this email. Thank you.</p>