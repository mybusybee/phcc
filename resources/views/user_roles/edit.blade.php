@extends('layouts.base')

@section('content')
<div class="container-fluid">
	<div class="row ctr-30 frm">
		<div class="col-md-12 text-center">
			<h1>Manage User Roles</h1>
		</div>
		{{Form::open(['url' => url('/user-roles/post'), 'method' => 'put'])}}
		<div class="role-container">
			@foreach($roles as $role)
			<div class="row role-div">
				<div class="col-md-10">
					<div class="form-group">
						{{ Form::hidden('role_id[]', $role->id, ['class' => 'role_id']) }}
						{{ Form::text('role[]', $role->user_role, ['class' => 'form-control text-center role']) }}
					</div>
				</div>
				<div class="col-md-1">
					<div class="form-group">
						<button type="button" class="btn btn-danger remove-role-btn">-</button>
					</div>
				</div>
			</div>	
			@endforeach
		</div>
		<div class="row">
			<div class="col-md-10">
				<div class="form-group text-center">
					{{ Form::submit('Save', ['class' => 'btn btn-success center-block']) }}
				</div>
			</div>
			<div class="col-md-1">
				<div class="form-group">
					<button type="button" class="btn btn-success add-role-btn">+</button>
				</div>
			</div>
		</div>
		{{Form::close()}}
	</div>
</div>

<script src="{{ asset('js/userroles.js') }}"></script>
@endsection