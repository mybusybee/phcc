@extends('layouts.base')

@section('content')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<div class="container frm" id="log-table">
    <h1>Activity Logs</h1>
    <table class="table table-bordered" id="logsTable">
        <thead class="table-primary">
            <tr>
                {{-- <th>ID</th> --}}
                <th>Date & Time</th>
                <th>Name</th>
                <th>Office</th>
                <th>Activity</th>
                
            </tr>
        </thead>
        <tbody>
            @foreach ($logs as $log)
                <tr>
                    {{-- <td>{{ $log->id }}</td> --}}
                    <td>{{ $log->created_at }}</td>
                    <td>{{ $log->user_name }}</td>
                    <td>{{ $log->user_office }}</td>
                    <td>{{ $log->log }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script>
    $('#logsTable').DataTable({
        "order": [[ 0, "desc" ]]
    });
</script>
@endsection