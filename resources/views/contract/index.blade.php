@extends('layouts.base')

@section('content')
    <div class="frm">
        <h1>Contracts List</h1>
        <div class="table-responsive">
            <table class="table table-striped table-hover">
                <thead class="table-primary">
                    <th>P.R. No.</th>
                    <th>P.R. Date</th>
                    <th>Contract Date</th>
                    <th>Action</th>
                </thead>
                <tbody>
                    @foreach ($contracts as $contract)
                        <tr>
                            <td>{{ $contract->pr->pr_no }}</td>
                            <td>{{ $contract->pr->pr_prep_date }}</td>
                            <td>{{ $contract->agreement_date }}</td>
                            <td><a href="{{ url('/contract/'.$contract->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection