@extends('layouts.base')

@section('content')
    <div class="frm">
        <div class="row">
            <div class="col-xl-3 offset-xl-9">
                <div class="form-group">
                    <label for="formNo">Contract Number:</label>
                    <input type="text" id="formNo" class="form-control form-control-sm" value="{{ $contract->order->form_no }}" readonly name="form_no">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12">
                <h3 class="text-center">Contract Agreement Form</h3>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-xl-12">
                <p style="text-indent: 40px">THIS AGREEMENT made the {{ Form::text('', $contract->agreement_day, ['class' => 'form-control form-control-sm d-inline-block', 'id' => 'agreementDate', 'style' => 'width: 5%;', 'readonly']) }} day of {{ Form::text('', $contract->agreement_month, ['class' => 'form-control form-control-sm d-inline-block', 'readonly', 'style' => 'width: 12%;']) }} {{ Form::text('', $contract->agreement_year, ['class' => 'form-control form-control-sm d-inline-block', 'readonly', 'style' => 'width: 12%;']) }} between {{ Form::text('procuring_entity', $contract->procurement_entity_name, ['class' => 'form-control form-control-sm d-inline-block w-25', 'placeholder' => 'insert name of procuring entity', 'readonly']) }} of the Philippines (hereinafter called "the Entity") of the one part and
                {{ Form::text('', $contract->supplier->company_name, ['class' => 'form-control form-control-sm d-inline-block w-25', 'readonly', 'id' => 'supplierAddress']) }}
                of {{ Form::text('', $contract->supplier->address, ['class' => 'form-control form-control-sm d-inline-block w-25', 'readonly', 'id' => 'supplierAddress']) }} (hereinafter called "the Supplier") of the other part:</p>
            </div>
            <div class="col-xl-12 mt-4">
                <p style="text-indent: 40px">WHEREAS the Entity invited Bids for certain goods and ancillary services, viz., {{ Form::text('brief_description', $contract->brief_description, ['class' => 'form-control form-control-sm d-inline-block w-25', 'placeholder' => 'brief description of goods and services', 'readonly']) }} and has accepted a Bid by the Supplier for the supply of those goodsd and services in the sum of {{ Form::text('contract_price', $contract->contract_price, ['class' => 'form-control form-control-sm d-inline-block w-25', 'placeholder' => 'contract price in words and figures', 'readonly']) }} (hereinafter called "the Contract Price").</p>
            </div>
            <div class="col-xl-12 mt-4">
                <p style="text-indent: 40px;">NOW THIS AGREEMENT WITNESSETH AS FOLLOWS:</p>
            </div>
            <div class="col-xl-12 mt-4">
                <p class="my-3">1.    In This Agreement words and expressions shall have the same meanings as are respectively assigned to them in the Conditions of Contract referred to.</p>
                <p class="my-3">2.   The following documents shall be deemed to form and be read and construed as part of this Agreement, viz.:</p>
            </div>
            
            <div class="col-xl-12">
                <ol type="a">
                    <li class="my-3">the Supplier's Bid, including the Technical and Financial Proposals, and all other doucments/statements submitted (e.g. bidder's response to clarifications on the bid), including corrections to the bid resulting from the Procuring Entity's bid Evaluation;</li>
                    <li class="my-3">the Schedule of Requirements;</li>
                    <li class="my-3">the Technical Specifications;</li>
                    <li class="my-3">the General Conditions of Contract;</li>
                    <li class="my-3">the Special Conditions of Contract;</li>
                    <li class="my-3">the Performance Security; and</li>
                    <li class="my-3">the Entity's Notice of Award.</li>
                </ol>
            </div>

            <div class="col-xl-12 mt-4">
                <p class="my-3">3.    In consideration of the payments to be made by the Entity to the Supplier as hereinafter mentioned, the Supplier hereby covenants with the Entity to provide the goods and services and to remedy defects therein in conformity in all respects with the provisions of the Contract.</p>
                <p class="my-3">4.   The Entity hereby covenants to pay the Supplier in consideration of the provision of the goods and services and the remedying of defects therein, the Contract Price or such other sum as may become payable under the provisions of the contract at the time and in the manner prescribed by the contract.</p>
                <p class="my-3" style="text-indent: 40px">IN WITNESS whereof the parties hereto have caused this Agreement to be executed in accordance with the laws of the Rpeublic of the Philippines on the day and year first above written.</p>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-xl-6">
                <p>For the <strong>Philippine Competition Commission</strong></p>
            </div>
            <div class="col-xl-6">
                <span>For the {{ Form::text('', $contract->supplier->company_name, ['class' => 'd-inline-block w-75 form-control form-control-sm', 'readonly', 'id' => 'supplierCompanyName2']) }}</span>
            </div>
        </div>
        <div class="row mt-3">
            <div class="col-xl-6">
                <p>By:</p>
            </div>
            <div class="col-xl-6">
                <p>By:</p>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-6">
                {{ Form::text('pcc_signatory', $contract->pcc_signatory, ['class' => 'form-control form-control-sm', 'required', 'readonly']) }}
                {{ Form::text('pcc_signatory', $contract->pcc_signatory_designation, ['class' => 'form-control form-control-sm', 'required', 'readonly']) }}

            </div>
            <div class="col-xl-6">
                {{ Form::text('supplier_signatory', $contract->supplier_signatory, ['class' => 'form-control form-control-sm', 'readonly']) }}
                {{ Form::text('supplier_signatory', $contract->supplier_signatory_designation, ['class' => 'form-control form-control-sm', 'readonly']) }}

            </div>
        </div>
        <div class="row mt-3">
            <div class="col-xl-12">
                <p class="text-center">Witnesses:</p>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-6">
                {{ Form::text('witness_1', $contract->witness_1, ['class' => 'form-control form-control-sm', 'readonly']) }}
                {{ Form::text('witness_1', $contract->witness_1_designation, ['class' => 'form-control form-control-sm', 'readonly']) }}

            </div>
            <div class="col-xl-6">
                {{ Form::text('witness_2', $contract->witness_2, ['class' => 'form-control form-control-sm', 'readonly']) }}
                {{ Form::text('witness_2', $contract->witness_2_designation, ['class' => 'form-control form-control-sm', 'readonly']) }}

            </div>
        </div>
        @if(count($contract->order->attachments))
            {{ Form::open(['url' => url('/attach-files/order/'.$contract->order->id), 'method' => 'post', 'files' => true]) }}
                <div id="filesSection">
                    <div class="row">
                        <div class="col-xl-12">
                            <h3>Attached Files:</h3>
                            <button class="btn btn-success add-file" type="button"><i class="fas fa-plus"></i> Add</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-4">
                            @foreach($contract->order->attachments as $file)
                                <div class="input-group pr-files">
                                    <a style="color: #000 !important;" href="{{ url('/download-files/order/'.$file->id) }}" class="btn btn-outline-primary file">
                                        <i class="fa fa-download"></i><span>  {{ $file->original_filename }}</span>
                                    </a>
                                    <div class="input-group-append">
                                        <a href="{{ url('delete-files/order/'.$file->id) }}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="row">
                    </div>
                </div>
                <div class="form-group">
                    <input type="submit" value="Attach Files" class="btn btn-success">
                </div>
            {{ Form::close() }}
        @else
            {{ Form::open(['url' => url('/attach-files/order/'.$contract->order->id), 'method' => 'post', 'files' => true]) }}
                <div class="form-group" id="files_section">
                    <h4>Attach Files:</h4><span style="font-size: 0.9em;"><i>PDF files only, maximum of 5 attachments.</i></span>
                    <button class="btn btn-success add-file" type="button"><i class="fa fa-plus"></i> Add</button>
                    <div class="input-group file_row"></div>
                </div>
                <div class="form-group">
                    <input type="submit" value="Attach Files" class="btn btn-success" id="attachFiles" disabled>
                </div>
            {{ Form::close() }}
        @endif
        <div class="form-group mt-3">
            <a href="{{ url('/export/docx/contract/'.$contract->id) }}" class="btn btn-warning">Export</a>
            @if(count($contract->order->attachments))
                @if ($contract->order->status !== 'NOTICE_SENT')
                    <a href="{{ url('send-notice/'.$contract->order->id) }}" class="btn btn-info">Send Notice</a>
                @else
                    <button class="btn btn-warning" type="button" disabled>Notice Sent</button>
                @endif
            @endif
        </div>
    </div>
    <script src="{{ asset('js/orders-parent/show.js') }}"></script>
@endsection