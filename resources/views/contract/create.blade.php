@extends('layouts.base')

@section('content')
    {{ Form::open(['url' => url('/contract/store'), 'method' => 'post']) }}
        <input type="hidden" value="{{ $ordersCount }}" id="ordersCount">
        <div class="frm">
            <input type="hidden" name="abstract_id" value="{{ $abstract->id }}">
            <div class="row">
                <div class="col-xl-3 offset-xl-9">
                    <div class="form-group">
                        <label for="formNo">Contract Number:</label>
                        <input type="text" id="formNo" class="form-control form-control-sm" readonly name="form_no">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12">
                    <h3 class="text-center">Contract Agreement Form</h3>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-xl-12">
                    <p style="text-indent: 40px">THIS AGREEMENT made the {{ Form::select('agreement_day', [ null => 'Please select one...'] + $date->ordinalDays, null, ['class' => 'form-control form-control-sm d-inline-block', 'style' => 'width: 6%;', 'required']) }} day of {{ Form::select('agreement_month', [ null => 'Please select one...'] + $date->months, null, ['class' => 'form-control form-control-sm d-inline-block', 'style' => 'width: 12%;', 'required']) }} {{ Form::select('agreement_year', [ null => 'Please select one...'] + $date->years, null, ['class' => 'form-control form-control-sm d-inline-block', 'style' => 'width: 12%;', 'required']) }} between {{ Form::text('procuring_entity', null, ['class' => 'form-control form-control-sm d-inline-block w-25', 'placeholder' => 'insert name of procuring entity', 'required']) }} of the Philippines (hereinafter called "the Entity") of the one part and
                    <select name="supplier_id" id="supplierSelector" required class="form-control form-control-sm d-inline-block w-25">
                        <option value="" disabled selected>Please select one...</option>
                        @foreach ($abstract->recommendation->recommendation_items as $item)
                            <option value="{{ $item->supplier_id }}" data-address="{{ $item->supplier->address }}">{{ $item->supplier->company_name }}</option>
                        @endforeach
                    </select>
                    of {{ Form::text('', null, ['class' => 'form-control form-control-sm d-inline-block w-25', 'readonly', 'id' => 'supplierAddress', 'required']) }} (hereinafter called "the Supplier") of the other part:</p>
                </div>
                <div class="col-xl-12 mt-4">
                    <p style="text-indent: 40px">WHEREAS the Entity invited Bids for certain goods and ancillary services, viz., {{ Form::text('brief_description', null, ['class' => 'form-control form-control-sm d-inline-block w-25', 'placeholder' => 'brief description of goods and services', 'required']) }} and has accepted a Bid by the Supplier for the supply of those goodsd and services in the sum of {{ Form::text('contract_price', null, ['class' => 'form-control form-control-sm d-inline-block w-25', 'placeholder' => 'contract price in words and figures','required']) }} (hereinafter called "the Contract Price").</p>
                </div>
                <div class="col-xl-12 mt-4">
                    <p style="text-indent: 40px;">NOW THIS AGREEMENT WITNESSETH AS FOLLOWS:</p>
                </div>
                <div class="col-xl-12 mt-4">
                    <p class="my-3">1.    In This Agreement words and expressions shall have the same meanings as are respectively assigned to them in the Conditions of Contract referred to.</p>
                    <p class="my-3">2.   The following documents shall be deemed to form and be read and construed as part of this Agreement, viz.:</p>
                </div>
                
                <div class="col-xl-12">
                    <ol type="a">
                        <li class="my-3">the Supplier's Bid, including the Technical and Financial Proposals, and all other doucments/statements submitted (e.g. bidder's response to clarifications on the bid), including corrections to the bid resulting from the Procuring Entity's bid Evaluation;</li>
                        <li class="my-3">the Schedule of Requirements;</li>
                        <li class="my-3">the Technical Specifications;</li>
                        <li class="my-3">the General Conditions of Contract;</li>
                        <li class="my-3">the Special Conditions of Contract;</li>
                        <li class="my-3">the Performance Security; and</li>
                        <li class="my-3">the Entity's Notice of Award.</li>
                    </ol>
                </div>

                <div class="col-xl-12 mt-4">
                    <p class="my-3">3.    In consideration of the payments to be made by the Entity to the Supplier as hereinafter mentioned, the Supplier hereby covenants with the Entity to provide the goods and services and to remedy defects therein in conformity in all respects with the provisions of the Contract.</p>
                    <p class="my-3">4.   The Entity hereby covenants to pay the Supplier in consideration of the provision of the goods and services and the remedying of defects therein, the Contract Price or such other sum as may become payable under the provisions of the contract at the time and in the manner prescribed by the contract.</p>
                    <p class="my-3" style="text-indent: 40px">IN WITNESS whereof the parties hereto have caused this Agreement to be executed in accordance with the laws of the Rpeublic of the Philippines on the day and year first above written.</p>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-xl-6">
                    <p>For the <strong>Philippine Competition Commission</strong></p>
                </div>
                <div class="col-xl-6">
                    <span>For the {{ Form::text('', null, ['class' => 'd-inline-block w-75 form-control form-control-sm', 'readonly', 'id' => 'supplierCompanyName2']) }}</span>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-xl-6">
                    <p>By:</p>
                </div>
                <div class="col-xl-6">
                    <p>By:</p>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-6">
                    {{ Form::text('pcc_signatory', null, ['class' => 'form-control form-control-sm', 'required', 'placeholder' => 'insert name']) }}
                    {{ Form::text('pcc_signatory_designation', null, ['class' => 'form-control form-control-sm', 'required', 'placeholder' => 'insert designation']) }}
                </div>
                <div class="col-xl-6">
                    {{ Form::text('supplier_signatory', null, ['class' => 'form-control form-control-sm', 'required', 'placeholder' => 'insert name']) }}
                    {{ Form::text('supplier_signatory_designation', null, ['class' => 'form-control form-control-sm', 'required', 'placeholder' => 'insert designation']) }}
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-xl-12">
                    <p class="text-center">Witnesses:</p>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-6">
                    {{ Form::text('witness_1', null, ['class' => 'form-control form-control-sm', 'required', 'placeholder' => 'insert name']) }}
                    {{ Form::text('witness_1_designation', null, ['class' => 'form-control form-control-sm', 'required', 'placeholder' => 'insert designation']) }}
                </div>
                <div class="col-xl-6">
                    {{ Form::text('witness_2', null, ['class' => 'form-control form-control-sm', 'required', 'placeholder' => 'insert name']) }}
                    {{ Form::text('witness_2_designation', null, ['class' => 'form-control form-control-sm', 'required', 'placeholder' => 'insert designation']) }}
                </div>
            </div>
            <div class="form-group mt-3">
                <input type="submit" value="Save" class="btn btn-success">
            </div>
        </div>
    {{ Form::close() }}
    <script src="{{ asset('js/contract/create.js') }}"></script>
@endsection