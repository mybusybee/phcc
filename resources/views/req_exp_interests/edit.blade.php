@extends('layouts.base')
@section('content')
@if ($rei->invite->status == 'FOR_REVISION')
<div class="alert alert-info">
    {{$rei->invite->rejects_remarks}}
</div>
@endif
<link rel="stylesheet" href="{{ asset('css/jquery.datetimepicker.css') }}">
{{ Form::open(['url' => url('/rei/'.$rei->id.'/update/'), 'method' => 'put']) }}
    <div class="row frm" id="rei_form">
        <div class="col-xl-12">
            <h1 class="text-center">REQUEST FOR EXPRESSION OF INTEREST FOR</h1>
        </div>
        <div class="col-xl-4 mx-auto">
            <input name="project_name" type="text" required class="form-control form-control-sm" value="{{ $rei->project_name}}" placeholder="Insert name of Project">
        </div>
        <div class="col-xl-12 form-inline mt-5">
            <ol>
                <li class="mb-4"><span>The Philippine Competition Commission, through the General Appropriations Act of {{ Form::select('year', $yearsArray, $rei->year, ['class' => 'form-control form-control-sm border border-info']) }} intends to apply the sum of <input type="text" class="form-control form-control-sm" name="abc" value="{{$rei->abc}}" placeholder="Insert the approved budget for the contract"> being the Approved Budget for the Contract(ABC) to payments under the contract for <input required type="text" class="form-control form-control-sm" name="rei_no" readonly value="{{ $rei->invite->type }}#: {{ $rei->invite->form_no }}">. Bids received in excess of the ABC shall be automatically rejected at bid opening.</span></li>

                <li class="mb-4"><span>The Philippine Competition Commission now calls for the submission of eligibility documents for <input type="text" required class="form-control form-control-sm w-50" name="brief_desc" value="{{$rei->brief_desc}}" placeholder="Insert brief description of services to be procured">. Eligibility documents of interested consultants must be duly received by the BAC Secretariat on or before <input id="opening_date" required type="text" class="form-control form-control-sm" name="opening_date" value="{{$rei->opening_date}}" placeholder="insert date and time of the opening of eligibility documents"> at 25/F Vertis North Corporate Center 1, North Avenue, Quezon City. Applications for eligibility will evaluated based on a non-discretionary "pass/fail" criterion.</span></li>

                <li class="mb-4">
                    <span>Interested bidders may obtain further information from Philippine Competition Commission and inspect the Bidding Documents at the address given below during Office Hours from 8:00 A.M. to 5:00 P.M. (Monday through Friday).</span>
                </li>

                <li class="mb-4">
                    <span>A complete set of Bidding Documents may be acquired by interested Bidders on <input type="text" class="form-control form-control-sm" required name="availability_date" id="availability_date" value="{{$rei->availability_date}}" placeholder="Insert date of availability of Bidding Documents"> from the address below and upon payment of the applicable fee for the bidding Documents, pursuant to the latest Guidelines issued by the GPPB, in the amount of <input type="text" required name="amount_in_peso" class="form-control form-control-sm" value="{{$rei->amount_in_peso}}" placeholder="insert amount in pesos" style="width: 36%;"></span>

                    <p class="mt-4"><span>It may also be downloaded free of charge from the website of the Philippine Government Electronic Procurement System (PhilGEPS) and the website of the Procuring Entity, provided that Bidders shall pay the applicable fee for the Bidding Documents not later than the submission of their bids.</span></p>
                </li>

                <li class="mb-4">
                    <span>The BAC shall draw up the short list of consultants from those who have submitted Expression of Interest, including the eligibility documents, and have been determined as eligible in accordance with the provisions of Republic Act 9184 (RA 9184), otherwise known as the "Government Procurement Reform Act", and its Implementing Rules and Regulations (IRR). The short list shall consists of <input type="number" required name="shortlist_allowed" class="form-control form-control-sm" value="{{$rei->shortlist_allowed}}" placeholder="insert number of short list allowed"> prospective bidders who will be entitled to submit bids. The criteria and rating system for short listing are: </span>

                    <p class="mt-4"><textarea name="shortlist_criteria" rows="10" class="form-control w-100" placeholder="Insert here a general statement on the criteria and rating system to be used for the short listing">{{$rei->shortlist_criteria}}</textarea></span></p>
                </li>

                <li class="mb-4">
                    <span>Bidding will be conducted through open competitive bidding procedures using non-discretionary "pass/fail" criterion as specified in the IRR of RA 9184. Bidding is restricted to Filipino citizens/sole proprietorships, ccoperatives and partnerships or organizations with at least sixty percent (60%) interest or outstanding capital stock belonging to citizens of the Philippines.</span>
                </li>

                <li class="mb-4">
                    <span>The Procuring Entity shall evaluate bids using the 
                    	{{ Form::select('procedure', ['QBE/QBS' => 'Quality Based Evaluation/Selection (QBE/QBS)', 'QCBE/QCBS' => 'Quality-Cost Based Evaluation/Selection (QCBE/QCBS)', 'FBS' => 'Fixed Budget Selection', 'LCS' => 'Least-Cost Selection'], $rei->procedure, ['class' => 'form-control form-control-sm procedure', 'id' => 'procedure']) }} procedure. <p id="ifqcbe" hidden>The Procuring Entity shall indicate the weights to be allocated for the Technical and Financial Proposals.</p> The criteria and rating system for the evaluation of bids shall be provided in the Instructions to Bidders.</span>
                </li>

                <li class="mb-4">
                <span>The contract shall be completed within <input type="text" required name="completed_within" class="form-control form-control-sm" value="{{ $rei->completed_within }}" placeholder="insert the expected contract duration in days or months" id="completedWithin"></span>
                </li>

                <li class="mb-4">
                    <p>The Philippine Competition Commission reserves the right to reject any and all bids, declare a failure of bidding, or not award the contract at any time prior to contract award in accordance with Section 41 of RA 9184 and its IRR, without thereby incurring any liability to the affected bidder or bidders.</p>
                </li>

                <li class="mb-4">
                    <p>For further information, please refer to:</p>
                    <p class="mt-3"><i>The Secretariat</i></p>
                    <p><i>Bids and Awards Commitee (BAC)</i></p>
                    <p><i>Philippine Competition Commission (PCC)</i></p>
                    <p><i>25/F Vertis North Corporate Center 1</i></p>
                    <p><i>North Avenue, Quezon City, 1105</i></p>
                    <p><i>Tel. No.: (02) 771-9722 / (02) 771-9757</i></p>
                    <p><i>Email: procurement@phcc.gov.ph or gsd@phcc.gov.ph</i></p>
                    <p><i>Website: www.phcc.gov.ph</i></p>
                </li>
            </ol>
        </div>
        <div class="col-xl-12">
    <p><input required name="chairperson_name" type="text" class="form-control form-control-sm w-25 float-right" value="{{ $rei->chairperson_name }}" placeholder="Insert Name"></p>
        </div>
        <div class="col-xl-12 text-right">
            <p>Chairperson</p>
        </div>
        <div class="col-xl-12 text-right">
            <p><i>PCC Bids and Awards Committee</i></p>
        </div>
        <div class="">
            <input type="submit" value="Submit" class="btn btn-success">
        </div>
    </div>
{{ Form::close() }}
<script src="{{ asset('js/utils/jquery-ui.min.js') }}"></script>
<script src="{{ asset('js/utils/jquery.datetimepicker.js') }}"></script>
<script src="{{ asset('js/rei/create.js') }}"></script>
@endsection