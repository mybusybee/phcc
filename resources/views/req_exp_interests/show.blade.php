@extends('layouts.base')
@section('content')
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    @if ($rei->invite->status == 'FOR_REVISION' || ($rei->invite->status == 'FOR_REVIEW/APPROVAL' && ($rei->invite->rejects_remarks)))
    <div class="alert alert-info">
        {{$rei->invite->rejects_remarks}}
    </div>
    @endif
    <div class="row frm">
        <div class="col-xl-12">
            <h1 class="text-center">REQUEST FOR EXPRESSION OF INTEREST FOR</h1>
        </div>
        <div class="col-xl-4 mx-auto">
            <input value="{{ $rei->project_name }}" readonly name="project_name" type="text" required class="form-control form-control-sm">
        </div>
        <div class="col-xl-12 form-inline mt-5">
            <ol>
                <li class="mb-4"><span>The Philippine Competition Commission, through the General Appropriations Act of <input value="{{ $rei->year }}" readonly required name="year" type="text" class="form-control form-control-sm"> intends to apply the sum of <input readonly type="text" class="form-control form-control-sm" value="{{ $rei->abc }}" name="abc"> being the Approved Budget for the Contract(ABC) to payments under the contract for <input readonly required type="text" class="form-control form-control-sm" value="{{ $rei->invite->type }}#: {{ $rei->invite->form_no }}" name="rei_no">. Bids received in excess of the ABC shall be automatically rejected at bid opening.</span></li>

                <li class="mb-4"><span>The Philippine Competition Commission now calls for the submission of eligibility documents for <input readonly type="text" required class="form-control form-control-sm" value="{{ $rei->brief_desc }}" name="brief_desc">. Eligibility documents of interested consultants must be duly received by the BAC Secretariat on or before <input readonly id="opening_date" value="{{ $rei->opening_date }}" required type="text" class="form-control form-control-sm" name="opening_date"> at 25/F Vertis North Corporate Center 1, North Avenue, Quezon City. Applications for eligibility will evaluated based on a non-discretionary "pass/fail" criterion.</span></li>

                <li class="mb-4">
                    <span>Interested bidders may obtain further information from Philippine Competition Commission and inspect the Bidding Documents at the address given below during Office Hours from 8:00 A.M. to 5:00 P.M. (Monday through Friday).</span>
                </li>

                <li class="mb-4">
                    <span>A complete set of Bidding Documents may be acquired by interested Bidders on <input readonly type="text" class="form-control form-control-sm" required name="availability_date" value="{{ $rei->availability_date }}"> from the address below and upon payment of the applicable fee for the bidding Documents, pursuant to the latest Guidelines issued by the GPPB, in the amount of &#8369;<input readonly type="text" required name="amount_in_peso" value="{{ $rei->amount_in_peso }}" class="form-control form-control-sm"></span>

                    <p class="mt-4"><span>It may also be downloaded free of charge from the website of the Philippine Government Electronic Procurement System (PhilGEPS) and the website of the Procuring Entity, provided that Bidders shall pay the applicable fee for the Bidding Documents not later than the submission of their bids.</span></p>
                </li>

                <li class="mb-4">
                    <span>The BAC shall draw up the short list of consultants from those who have submitted Expression of Interest, including the eligibility documents, and have been determined as eligible in accordance with the provisions of Republic Act 9184 (RA 9184), otherwise known as the "Government Procurement Reform Act", and its Implementing Rules and Regulations (IRR). The short list shall consists of <input value="{{ $rei->shortlist_allowed }}" readonly type="number" required name="shortlist_allowed" class="form-control form-control-sm"> prospective bidders who will be entitled to submit bids. The criteria and rating system for short listing are: </span>

                    <p class="mt-4"><textarea readonly name="shortlist_criteria" rows="10" class="form-control w-100">{{ $rei->shortlist_criteria }}</textarea></span></p>
                </li>

                <li class="mb-4">
                    <span>Bidding will be conducted through open competitive bidding procedures using non-discretionary "pass/fail" criterion as specified in the IRR of RA 9184. Bidding is restricted to Filipino citizens/sole proprietorships, ccoperatives and partnerships or organizations with at least sixty percent (60%) interest or outstanding capital stock belonging to citizens of the Philippines.</span>
                </li>

                <li class="mb-4">
                    <span>The Procuring Entity shall evaluate bids using the <input type="text" value="{{ $rei->procedure }}" class="form-control form-control-sm" readonly> procedure. <p id="ifqcbe" hidden>The Procuring Entity shall indicate the weights to be allocated for the Technical and Financial Proposals.</p> The criteria and rating system for the evaluation of bids shall be provided in the Instructions to Bidders.</span>
                </li>

                <li class="mb-4">
                    <span>The contract shall be completed within <input value="{{ $rei->completed_within }}" readonly type="text" required name="completed_within" class="form-control form-control-sm"></span>
                </li>

                <li class="mb-4">
                    <p>The Philippine Competition Commission reserves the right to reject any and all bids, declare a failure of bidding, or not award the contract at any time prior to contract award in accordance with Section 41 of RA 9184 and its IRR, without thereby incurring any liability to the affected bidder or bidders.</p>
                </li>

                <li class="mb-4">
                    <p>For further information, please refer to:</p>
                    <p class="mt-3"><i>The Secretariat</i></p>
                    <p><i>Bids and Awards Commitee (BAC)</i></p>
                    <p><i>Philippine Competition Commission (PCC)</i></p>
                    <p><i>25/F Vertis North Corporate Center 1</i></p>
                    <p><i>North Avenue, Quezon City, 1105</i></p>
                    <p><i>Tel. No.: (02) 771-9722 / (02) 771-9757</i></p>
                    <p><i>Email: procurement@phcc.gov.ph or gsd@phcc.gov.ph</i></p>
                    <p><i>Website: www.phcc.gov.ph</i></p>
                </li>
            </ol>
        </div>
        <div class="col-xl-12 mb-3">
            @if(Auth::user()->user_role === 5 && $rei->invite->status === 'FOR_EXPORT' && $rei->signed === 0)
                <a href="{{ url('rei/signed/'.$rei->id) }}" class="btn btn-info float-right">Signed</a>
            @elseif(Auth::user()->user_role === 5 && $rei->invite->status === 'FOR_EXPORT' && $rei->signed === 1)
                <button class="btn btn-warning float-right" type="button" disabled>Signed</button>
        @endif
        </div>
        <div class="col-xl-12">
            <h3>Attached Files:</h3>
            <ul class="list-group">
                @forelse ($rei->invite->files as $file)
                    <a style="color: #000 !important;" href="{{ url('invitation/download/'.$file->id) }}" class="list-group-item list-group-item-action list-group-item-primary">
                        <i class="fa fa-download"></i><span>  {{ $file->filename }}</span>
                    </a>
                @empty
                    <li class="list-group-item group-item-action list-group-item-primary">No file/s attached</li>
                @endforelse
            </ul>
        </div>
        <div class="col-xl-12">
            <p><input readonly required name="chairperson_name" type="text" class="form-control form-control-sm w-25 float-right" value="{{ $rei->chairperson_name }}"></p>
        </div>
        <div class="col-xl-12 text-right">
            <p>Chairperson</p>
        </div>
        <div class="col-xl-12 text-right">
            <p><i>PCC Bids and Awards Committee</i></p>
        </div>
        <hr>
        @if ($rei->pr->personal_pmr_item->status == 'Cancelled' || $rei->pr->personal_pmr_item->status == 'Failed')
        
        @else
            @if ((Auth::user()->role->id == 3) && ($rei->invite->status == 'FOR_REVIEW/APPROVAL'))
                <div class="form-group">
                    <a href="{{ url('/invitations/approve/'.$rei->invite->id) }}" class="btn btn-success">Approve</a>
                    <button class="btn btn-danger" data-toggle="modal" data-target="#rejectREIModal">Reject</button>
                    <button class="btn btn-danger" data-toggle="modal" data-target="#cancelREIModal">Cancel REI</button>
                </div>
                <div class="modal fade" id="rejectREIModal" tabindex="-1" role="dialog" aria-hidden="true">
                    {{ Form::open(['url' => url('/invitations/reject/'.$rei->invite->id), 'method' => 'put']) }}
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title">Reason for rejection</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-md-12">
                                                {{ Form::textarea('reject_remarks', null, ['class' => 'form-control']) }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
            @elseif((Auth::user()->user_role === 5) && ($rei->invite->status === 'FOR_EXPORT'))
                <div class="col-xl-4">
                    <a href="{{ url('/export/docx/rei/'.$rei->id) }}" class="btn btn-warning">Export</a>
                </div>
                {{ Form::open(['url' => '/invite-suppliers' , 'method' => 'post', 'style' => 'width: 100%;']) }}
                    <div class="table-responsive form-group" id="suppliersContainer">
                        <h5>Supplier List:</h5>
                        <table id="suppliers-datatable" class="table">
                            <thead class="table-primary">
                                <tr>
                                    <th><i class="fa fa-check-circle"></i></th>
                                    <th>Company Name</th>
                                    <th>Authorized Personnel</th>
                                    <th>Category</th>
                                    <th>Address</th>
                                    <th>Telephone</th>
                                    <th>Email</th>
                                    <th>TIN</th>
                                    <th>Mayor's Permit Expiry Date</th>
                                    <th>Philgeps Number</th>
                                </tr>
                            </thead>
                            <tbody>
                                <div id="supplierList">
                                    @foreach($suppliers as $supplier)
                                        @if(in_array($supplier->id, $selected_suppliers))
                                            <input type="hidden" name="selectedSuppliers[]" class="form-control form-control-sm d-inline-block w-25 supplier-id-class" value="{{ $supplier->id }}">
                                        @endif
                                    @endforeach
                                </div>
                                @foreach ($suppliers as $supplier)
                                    <tr>
                                        <td>
                                            @if (in_array($supplier->id, $selected_suppliers))
                                                <input type="checkbox" name="" id="" class="sup_id" value="{{ $supplier->id }}" checked>
                                            @else
                                            <input type="checkbox" name="" id="" class="sup_id" value="{{ $supplier->id }}">
                                            @endif
                                        </td>
                                        <td>{{ $supplier->company_name }}</td>
                                        <td>{{ $supplier->authorized_personnel }}</td>
                                        <td>{{ $supplier->category }}</td>
                                        <td>{{ $supplier->address }}</td>
                                        <td>{{ $supplier->telephone }}</td>
                                        <td>{{ $supplier->email }}</td>
                                        <td>{{ $supplier->tin_number }}</td>
                                        <td>{{ $supplier->mayors_permit }}</td>
                                        <td>{{ $supplier->philgeps_number }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="row form-group">
                        <div class="col-xl-3">
                            <label for="preselect"><input required type="radio" id="preselect" value="preselect" name="submitType">Preselect</label>
                        </div>
                        <div class="col-xl-3">
                            <label for="sendEmails"><input required type="radio" id="sendEmails" value="sendEmails" name="submitType">Send Emails</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <button class="btn btn-success submit" type="submit" id="submitBtn" disabled>Save/Send</button>
                        <button class="btn btn-danger" data-toggle="modal" data-target="#cancelREIModal">Cancel REI</button>
                    </div>
                    <input type="hidden" name="invitation_id" value="{{ $rei->invite->id }}">
                {{ Form::close() }}
            @elseif ($rei->invite->status === 'FARMED_OUT')
                <div class="col-xl-12">
                    <h1>Selected Suppliers</h1>
                </div>
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead class="table-primary">
                            <tr>
                                <th>Company Name</th>
                                <th>Authorized Personnel</th>
                                <th>Category</th>
                                <th>Address</th>
                                <th>Telephone</th>
                                <th>Email</th>
                                <th>TIN</th>
                                <th>Mayor's Permit Expiry Date</th>
                                <th>Philgeps Number</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($rei->invite->suppliers as $supplier)
                                <tr>
                                    <td>{{ $supplier->company_name }}</td>
                                    <td>{{ $supplier->authorized_personnel }}</td>
                                    <td>{{ $supplier->category }}</td>
                                    <td>{{ $supplier->address }}</td>
                                    <td>{{ $supplier->telephone }}</td>
                                    <td>{{ $supplier->email }}</td>
                                    <td>{{ $supplier->tin_number }}</td>
                                    <td>{{ $supplier->mayors_permit }}</td>
                                    <td>{{ $supplier->philgeps_number }}</td>
                                    @if($supplier->pivot->email_status === 'EMAIL_SENT')
                                        <td>{{ $supplier->pivot->email_status }} {{date('M d, Y h:i:s a', strtotime($rei->invite->updated_at))}}</td>
                                    @else
                                        <td><a href="#" class="btn btn-warning">Send Email</a></td>
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @elseif ($rei->invite->status !== 'CANCELLED')
                <div class="form-group">
                    <button class="btn btn-danger" data-toggle="modal" data-target="#cancelREIModal">Cancel REI</button>
                </div>
            @endif

        @endif
        
    </div>
    <div class="modal fade" id="cancelREIModal" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Cancel REI</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-md-12">
                                <h3>Are you sure?</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-dark" data-dismiss="modal">Close</button>
                    <a href="{{ url('/invitations/cancel/'.$rei->invite->id) }}" class="btn btn-danger">Yes</a>
                </div>
            </div>
        </div>
    </div>
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="{{ asset('js/rei/show.js') }}"></script>
@endsection