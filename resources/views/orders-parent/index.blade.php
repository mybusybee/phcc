@extends('layouts.base')

@section('content')
<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <div class="frm">
        <h2>Purchase / Job Orders / Contracts List</h2>
        <div class="table-responsive">
            <table class="table table-striped table-hover" id="ordersTable">
                <thead class="table-primary">
                    {{-- <th>ID</th> --}}
                    <th>Number</th>
                    <th>Date</th>
                    <th>P.R. No.</th>
                    <th>P.R. Date</th>
                    <th>Invitation No.</th>
                    <th>Abstract No.</th>
                    <th>Status</th>
                    <th>Action</th>
                </thead>
                <tbody>
                    @foreach ($orders as $order)
                        <tr>
                            {{-- <td>{{ $order->id }}</td> --}}
                            <td>{{ $order->type }}#: {{ $order->form_no }}</td>
                            <td>{{ $order->created_at->format('F d, Y - h:i:s A') }}</td>
                            <td>{{ $order->pr->pr_no }}</td>
                            <td>{{ $order->pr->pr_prep_date }}</td>
                            <td>{{ $order->abstract->invite->type }}#: {{ $order->abstract->invite->form_no }}</td>
                            <td>{{ $order->abstract->type }}#: {{ $order->abstract->form_no }}</td>
                            @if ($order->pr->personal_pmr_item->status == 'Cancelled' || $order->pr->personal_pmr_item->status == 'Failed')
                                <td>CANCELLED</td>
                                @if($order->type === 'PO')
                                    <td>
                                        <a href="{{ url('/po/'.$order->po->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                                    </td>
                                @elseif($order->type === 'JO')
                                    <td>
                                        <a href="{{ url('/jo/'.$order->jo->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                                    </td>
                                @else
                                    <td>
                                        <a href="{{ url('/contract/'.$order->contract->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                                    </td>
                                @endif
                            @else
                                <td>{{ $order->status }}</td>
                                @if($order->type === 'PO')
                                    <td>
                                        <a href="{{ url('/po/'.$order->po->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                                        <a href="{{ url('/po/'.$order->po->id.'/edit') }}" class="btn btn-info"><i class="fa fa-edit"></i></a>
                                    </td>
                                @elseif($order->type === 'JO')
                                    <td>
                                        <a href="{{ url('/jo/'.$order->jo->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                                        <a href="{{ url('jo/'.$order->jo->id.'/edit') }}" class="btn btn-info"><i class="fa fa-edit"></i></a>
                                    </td>
                                @else
                                    <td>
                                        <a href="{{ url('/contract/'.$order->contract->id) }}" class="btn btn-primary"><i class="fa fa-eye"></i></a>
                                    <a href="{{ url('contracts/'.$order->contract->id.'/edit') }}" class="btn btn-info"><i class="fa fa-edit"></i></a>
                                    </td>
                                @endif
                            @endif
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script>
        $(function(){
            $('#ordersTable').DataTable({
                "aaSorting": [],
            })
        })
    </script>
@endsection