@extends('layouts.base')

@section('content')
    <div class="frm" id="orderForm">
        <div class="row form-group">
            <div class="col-xl-12">
                <h1 class="text-center">PURCHASE ORDER</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-2">
                <h5>Supplier/Provider</h5>
            </div>
            <div class="col-xl-4">
                {{ Form::text('', $po->supplier->company_name, ['class' => 'form-control form-control-sm', 'readonly']) }}
            </div>
            <div class="col-xl-2">
                <h5>P.O. Number</h5>
            </div>
            <div class="col-xl-4">
                {{ Form::text('form_no', $po->ordersParent->form_no, ['class' => 'form-control form-control-sm', 'readonly']) }}
            </div>
        </div>
        <div class="row">
            <div class="col-xl-2 offset-xl-6">
                <h5>Date</h5>
            </div>
            <div class="col-xl-4">
                {{ Form::text('po_date', $po->po_date, ['class' => 'form-control form-control-sm', 'id' => 'formDate', 'readonly']) }}
            </div>
        </div>
        <div class="row">
            <div class="col-xl-2">
                <h5>Address</h5>
            </div>
            <div class="col-xl-4">
                {{ Form::text('', $po->supplier->address, ['class' => 'form-control form-control-sm', 'readonly', 'v-model' => 'supplier.address']) }}
            </div>
            <div class="col-xl-2">
                <h5>P.R. No.</h5>
            </div>
            <div class="col-xl-4">
                {{ Form::text('', $po->pr->pr_no, ['class' => 'form-control form-control-sm', 'readonly']) }}
            </div>
        </div>
        <div class="row">
            <div class="col-xl-2">
                <h5>Tel./Fax No.</h5>
            </div>
            <div class="col-xl-4">
                {{ Form::text('', $po->supplier->telephone, ['class' => 'form-control form-control-sm', 'readonly', 'v-model' => 'supplier.telephone']) }}
            </div>
            <div class="col-xl-2">
                <h5>Date</h5>
            </div>
            <div class="col-xl-4">
                {{ Form::text('', $po->pr->pr_prep_date, ['class' => 'form-control form-control-sm', 'readonly']) }}
            </div>
        </div>
        <div class="row">
            <div class="col-xl-2">
                <h5>TIN</h5>
            </div>
            <div class="col-xl-4">
                {{ Form::text('', $po->supplier->tin_number, ['class' => 'form-control form-control-sm', 'readonly', 'v-model' => 'supplier.tin']) }}
            </div>
            <div class="col-xl-2">
                <h5>Mode of Procurement</h5>
            </div>
            <div class="col-xl-4">
                {{ Form::text('', $po->procurement_mode, ['class' => 'form-control form-control-sm', 'readonly']) }}
            </div>
        </div>
        <hr>
        <div class="table-responsive">
            <table class="table table-hover table-striped">
                <thead class="table-primary">
                    <tr>
                        <th colspan="4"></th>
                        <th class="text-center" colspan="2">Cost</th>
                    </tr>
                    <tr>
                        <th class="text-center">Item No.</th>
                        <th class="text-center">Qty</th>
                        <th class="text-center">Unit</th>
                        <th class="text-center">Item Description</th>
                        <th class="text-center">Unit</th>
                        <th class="text-center">Total</th>
                    </tr>
                </thead>
                <tbody>
                    @php $total = 0; @endphp
                    @foreach($po->items as $item)
                        <tr v-for="item in getAllItems">
                            <td class="text-center">{{ $item->item_no }}</td>
                            <td class="text-center">{{ $item->quantity }}</td>
                            <td class="text-center">{{ $item->unit }}</td>
                            <td class="text-center">{{ $item->description }}</td>
                            <td class="text-center">{{ $item->type === 'parent' ? '' : number_format($item->pivot->unit_cost, 2, '.', ',') }}</td>
                            <td class="text-center">{{ $item->type === 'parent' ? '' : number_format($item->pivot->total, 2, '.', ',') }}</td>
                            @php $total += $item->type === 'parent' ? 0 : $item->pivot->total; @endphp
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <hr>
        <div class="row">
            <div class="col-xl-6">
                <p>Amount in Words:</p>
                {{ Form::text('', $po->amount_in_words, ['class' => 'form-control form-control-sm', 'readonly']) }}
            </div>
            <div class="col-xl-3 offset-xl-3">
                <p>Total:</p>
                <div class="input-group input-group-sm">
                    <div class="input-group-append">
                        <span class="input-group-text">&#8369;</span>
                    </div>
                    {{ Form::text('', $total, ['class' => 'form-control form-control-sm', 'readonly']) }}
                </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="form-group col-xl-6">
                <label for="deliveryPlace" class="w-100">Place of Delivery:</span>
                {{ Form::text('', $po->place_of_delivery, ['class' => 'form-control form-control-sm', 'readonly']) }}
            </div>
            <div class="col-xl-6">
                <span>Delivery Term:</span><br>
                @if($po->delivery_term === 'pickup')
                    {{ Form::text('', 'Pick Up', ['class' => 'form-control form-control-sm', 'readonly']) }}
                @else
                    <span>Deliver w/in {{ Form::text('', $po->delivery_term, ['class' => 'form-control form-control-sm d-inline-block', 'readonly', 'style' => 'width: 15%;']) }} calendar days from receipt of JO</span>
                @endif
            </div>
        </div>
        <div class="row">
            <div class="form-group col-xl-6">
                <label for="dateOfDeliveryCompletion" class="w-100">Date of Delivery/Completion:</span>
                    {{ Form::text('', $po->date_of_delivery, ['class' => 'form-control form-control-sm', 'readonly']) }}
            </div>
            <div class="col-xl-6">
                <span>Payment Term:</span><br>
                @if($po->payment_term === 'cod')
                    {{ Form::text('', 'COD', ['class' => 'form-control form-control-sm', 'readonly']) }}
                @else
                    <span>w/in {{ Form::text('', $po->payment_term, ['class' => 'form-control form-control-sm d-inline-block', 'readonly', 'style' => 'width: 15%;']) }} days after delivery.</span>
                @endif
            </div>
        </div>
        <hr>
        @if(count($po->ordersParent->attachments))
            {{ Form::open(['url' => url('/attach-files/order/'.$po->ordersParent->id), 'method' => 'post', 'files' => true]) }}
                <div class="form-group" id="files_section">
                    <h4>Attach Files:</h4><span style="font-size: 0.9em;"><i>PDF files only, maximum of 5 attachments.</i></span>
                    <button class="btn btn-success add-file" type="button"><i class="fa fa-plus"></i> Add</button>
                    <div class="row">
                        <div class="col-xl-4">
                            @foreach($po->ordersParent->attachments as $file)
                                <div class="input-group pr-files">
                                    <a style="color: #000 !important;" href="{{ url('/download-files/order/'.$file->id) }}" class="btn btn-outline-primary file">
                                        <i class="fa fa-download"></i><span>  {{ $file->original_filename }}</span>
                                    </a>
                                    <div class="input-group-append">
                                        <a href="{{ url('delete-files/order/'.$file->id) }}" class="btn btn-danger"><i class="fa fa-trash"></i></a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <input type="submit" value="Attach Files" class="btn btn-success">
                </div>
            {{ Form::close() }}
        @else
            {{ Form::open(['url' => url('/attach-files/order/'.$po->ordersParent->id), 'method' => 'post', 'files' => true]) }}
                <div class="form-group" id="files_section">
                    <h4>Attach Files:</h4><span style="font-size: 0.9em;"><i>PDF files only, maximum of 5 attachments.</i></span>
                    <button class="btn btn-success add-file" type="button"><i class="fa fa-plus"></i> Add</button>
                    <div class="input-group file_row"></div>
                </div>
                <div class="form-group">
                    <input type="submit" value="Attach Files" class="btn btn-success" id="attachFiles" disabled>
                </div>
            {{ Form::close() }}
        @endif
        <hr>
        @if ($po->pr->personal_pmr_item->status == 'Cancelled' || $po->pr->personal_pmr_item->status == 'Failed')
            
        @else
            <div class="form-group">
                <a href="{{ url('/export/xls/po/'.$po->id) }}" class="btn btn-warning">Export</a>
                @if(count($po->ordersParent->attachments))
                    <a href="{{ url('send-notice/'.$po->ordersParent->id) }}" class="btn btn-info">Send Notice</a>
                @endif
            </div>
        @endif
    </div>
    <script src="{{ asset('js/orders-parent/show.js') }}"></script>
@endsection