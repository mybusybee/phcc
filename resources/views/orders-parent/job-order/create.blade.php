@extends('layouts.base')

@section('content')
    <div class="frm" id="orderForm">
        {{ Form::open(['url' => url('jo/store'), 'method' => 'post']) }}
            <input type="hidden" name="abstractID" ref="abstractID" value="{{ $abstract->abstract_parent->id }}">
            <input type="hidden" name="" ref="ordersCount" value="{{ $ordersCount }}">
            <div class="row form-group">
                <div class="col-xl-12">
                    <h1 class="text-center">JOB ORDER</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-2">
                    <h5>Supplier/Provider</h5>
                </div>  
                <div class="col-xl-4">
                    <input type="hidden" name="" ref="formType" value="create">
                    <select name="supplier_id" id="" required class="form-control form-control-sm" @change="supplierChanged($event)">
                        <option value="" disabled selected>Please select one...</option>
                        @foreach($abstract->abstract_parent->recommendation->recommendation_items as $recommended)
                            <option value="{{ $recommended->supplier->id }}">{{ $recommended->supplier->company_name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-xl-2">
                    <h5>J.O. Number</h5>
                </div>
                <div class="col-xl-4">
                    {{ Form::text('form_no', null, ['class' => 'form-control form-control-sm', 'readonly', 'v-model' => 'formNumber']) }}
                </div>
            </div>
            <div class="row">
                <div class="col-xl-2 offset-xl-6">
                    <h5>Date</h5>
                </div>
                <div class="col-xl-4">
                    {{ Form::text('jo_date', null, ['class' => 'form-control form-control-sm', 'id' => 'formDate', 'required']) }}
                </div>
            </div>
            <div class="row">
                <div class="col-xl-2">
                    <h5>Address</h5>
                </div>
                <div class="col-xl-4">
                    {{ Form::text('', null, ['class' => 'form-control form-control-sm', 'readonly', 'v-model' => 'supplier.address']) }}
                </div>
                <div class="col-xl-2">
                    <h5>P.R. No.</h5>
                </div>
                <div class="col-xl-4">
                    {{ Form::text('', $abstract->pr->pr_no, ['class' => 'form-control form-control-sm', 'readonly']) }}
                </div>
            </div>
            <div class="row">
                <div class="col-xl-2">
                    <h5>Tel./Fax No.</h5>
                </div>
                <div class="col-xl-4">
                    {{ Form::text('', null, ['class' => 'form-control form-control-sm', 'readonly', 'v-model' => 'supplier.telephone']) }}
                </div>
                <div class="col-xl-2">
                    <h5>Date</h5>
                </div>
                <div class="col-xl-4">
                    {{ Form::text('', $abstract->pr->pr_prep_date, ['class' => 'form-control form-control-sm', 'readonly']) }}
                </div>
            </div>
            <div class="row">
                <div class="col-xl-2">
                    <h5>TIN</h5>
                </div>
                <div class="col-xl-4">
                    {{ Form::text('', null, ['class' => 'form-control form-control-sm', 'readonly', 'v-model' => 'supplier.tin']) }}
                </div>
                <div class="col-xl-2">
                    <h5>Mode of Procurement</h5>
                </div>
                <div class="col-xl-4">
                    {{ Form::text('', $abstract->procurement_mode->mode, ['class' => 'form-control form-control-sm', 'readonly']) }}
                </div>
            </div>
            <hr>
            <div class="table-responsive">
                <table class="table table-hover table-striped">
                    <thead class="table-primary">
                        <tr>
                            <th colspan="4"></th>
                            <th class="text-center" colspan="2">Cost</th>
                        </tr>
                        <tr>
                            <th class="text-center">Item No.</th>
                            <th class="text-center">Qty</th>
                            <th class="text-center">Unit</th>
                            <th class="text-center">Item Description</th>
                            <th class="text-center">Unit</th>
                            <th class="text-center">Total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <template v-if="getAllItems === null">
                            <th colspan="6" class="text-center">@{{ emptyItemText }}</th>
                        </template>
                        <template v-else>
                            <tr v-for="item in getAllItems">
                                <td class="text-center">@{{ item.item_no }}</td>
                                <td class="text-center">@{{ item.quantity }}</td>
                                <td class="text-center">@{{ item.unit }}</td>
                                <td class="text-center">@{{ item.description }}</td>
                                <td class="text-center">@{{ item.type === 'parent' ? '' : `&#8369; ${item.pivot.unit_cost.toLocaleString()}` }}</td>
                                <td class="text-center">@{{ item.type === 'parent' ? '' : `&#8369; ${item.pivot.total.toLocaleString()}` }}</td>
                            </tr>
                        </template>
                    </tbody>
                </table>
            </div>
            <hr>
            <div class="row">
                <div class="col-xl-6">
                    <p>Amount in Words:</p>
                    <input type="text" class="form-control form-control-sm" name="amount_in_words" required>
                </div>
                <div class="col-xl-3 offset-xl-3">
                    <p>Total:</p>
                    <div class="input-group input-group-sm">
                        <div class="input-group-append">
                            <span class="input-group-text">&#8369;</span>
                        </div>
                        <input readonly type="text" class="form-control form-control-sm" v-model="getTotalCost">
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="form-group col-xl-6">
                    <label for="deliveryPlace" class="w-100">Place of Delivery:</span>
                    <input id="deliveryPlace" required type="text" class="form-control form-control-sm d-inline-block" style="width: 80%;" name="place_of_delivery">
                </div>
                <div class="col-xl-3">
                    <span>Delivery Term:</span>
                    <label for="pickUp"><input type="radio" required name="delivery_term" id="pickUp" class="ml-3" value="pickup">Pick Up</label>
                </div>
                <div class="col-xl-1" style="text-align: right; padding: 0; margin-top: 4px;">
                    <input type="radio" name="delivery_term" id="cDays" class="ml-3" value="days" required>
                </div>
                <div class="col-xl-2">
                    <label for="cDays">Deliver w/in <input type="text" class="form-control form-control-sm d-inline-block" style="width: 15%;" name="calendar_days"> calendar days from receipt of JO</label>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-xl-6">
                    <label for="dateOfDeliveryCompletion" class="w-100">Date of Delivery/Completion:</span>
                    <input id="dateOfDeliveryCompletion" required type="text" class="form-control form-control-sm d-inline-block" style="width: 67%;" name="date_of_delivery">
                </div>
                <div class="col-xl-3">
                    <span>Payment Term:</span>
                    <label for="paymentTerm1"><input type="radio" required name="payment_term" id="paymentTerm1" class="ml-3" value="cod">COD</label>
                </div>
                <div class="col-xl-1" style="text-align: right; padding: 0; margin-top: 4px;">
                    <input type="radio" required name="payment_term" id="paymentDaysAfter" class="ml-3" value="days after delivery">
                </div>
                <div class="col-xl-2">
                    <label for="paymentDaysAfter">w/in <input type="text" class="form-control form-control-sm d-inline-block" style="width: 15%;" name="payment_after_delivery"> days after delivery</label>
                </div>
            </div>
            <div class="form-group mt-5">
                <input type="submit" value="Save" class="btn btn-success">
            </div>
        {{ Form::close() }}
    </div>
<script src="{{ asset('js/orders-parent/create.js') }}"></script>
@endsection