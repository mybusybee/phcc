<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="{{ asset('css/pmrtable.css') }}">
</head>
<body>
    <br><br><br><br><br>
    <table>
        <thead>
            <tr><td></td></tr>
            <tr>
                <th colspan="12" style="text-align: center; font-size: 16px">JOB ORDER</th>
            </tr>
        </thead>
    </table>
    <table>
        <tbody>
            <tr>
                <th colspan="7" style="border-top: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000; text-align: left;">1</th>
                <th colspan="5" style="border-top: 1px solid #000; border-right: 1px solid #000; text-align: left;">2</th>
            </tr>
            <tr>
                <td colspan="2" style="border-left: 1px solid #000;">Supplier/Provider</td>
                <td colspan="5" style="border-right: 1px solid #000; border-bottom: 1px solid #000">{{ $jo->supplier->company_name }}</td>
                <td colspan="2">J.O. Number</td>
                <td colspan="3" style="border-bottom: 1px solid #000; border-right: 1px solid #000">{{ $jo->ordersParent->form_no }}</td>
            </tr>
            <tr>
                <td colspan="2" style="border-left: 1px solid #000;"></td>
                <td colspan="5" style="border-right: 1px solid #000; border-bottom: 1px solid #000"></td>
                <td colspan="2">Date</td>
                <td colspan="3" style="border-bottom: 1px solid #000; border-right: 1px solid #000">{{ $jo->po_date }}</td>
            </tr>
            <tr>
                <td colspan="2" style="border-left: 1px solid #000;">Address</td>
                <td colspan="5" style="border-right: 1px solid #000; border-bottom: 1px solid #000">{{ $jo->supplier->address }}</td>
                <td colspan="2">P.R. No.</td>
                <td colspan="3" style="border-bottom: 1px solid #000; border-right: 1px solid #000">{{ $jo->pr->pr_no }}</td>
            </tr>
            <tr>
                <td colspan="2" style="border-left: 1px solid #000;">Tel./Fax No.</td>
                <td colspan="5" style="border-right: 1px solid #000; border-bottom: 1px solid #000">{{ $jo->supplier->telephone }}</td>
                <td colspan="2">Date</td>
                <td colspan="3" style="border-bottom: 1px solid #000; border-right: 1px solid #000">{{ $jo->pr->pr_prep_date }}</td>
            </tr>
            <tr>
                <td colspan="2" style="border-left: 1px solid #000;">TIN</td>
                <td colspan="5" style="border-right: 1px solid #000; border-bottom: 1px solid #000">{{ $jo->supplier->tin_number }}</td>
                <td colspan="2">Mode of Procurement</td>
                <td colspan="3" style="border-bottom: 1px solid #000; border-right: 1px solid #000">{{ $jo->procurement_mode }}</td>
            </tr>
            <tr>
                <td colspan="7" style="border-bottom: 1px solid #000; border-right: 1px solid #000; border-left: 1px solid #000;"></td>
                <td colspan="5" style="border-bottom: 1px solid #000; border-right: 1px solid #000;"></td>
            </tr>
            <tr>
                <th colspan="12" style="border-left: 1px solid #000; border-right: 1px solid #000; text-align: left;">3</th>
            </tr>
            <tr>
                <td colspan="12" style="border-left: 1px solid #000; border-right: 1px solid #000;">Gentlemen:</td>
            </tr>
            <tr>
                <td colspan="12" style="border-left: 1px solid #000; border-right: 1px solid #000; text-indent: 3px; border-bottom: 1px solid #000;">Please furnish this Office the following articles subject to the terms and conditions contained herein.</td>
            </tr>
            <tr>
                <th style="text-align: center; border-right: 1px solid #000; border-left: 1px solid #000; font-size: 10px;">Item</th>
                <th style="text-align: center; border-right: 1px solid #000; border-left: 1px solid #000;"></th>
                <th style="text-align: center; border-right: 1px solid #000; border-left: 1px solid #000;"></th>
                <th style="text-align: center; border-right: 1px solid #000; border-left: 1px solid #000;" colspan="7"></th>
                <th colspan="2" style="text-align: center; border-right: 1px solid #000; border-left: 1px solid #000; border-bottom: 1px solid #000;">Cost</th>
            </tr>
            <tr>
                <th style="text-align: center; border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000;">No.</th>
                <th style="text-align: center; border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000;">Qty</th>
                <th style="text-align: center; border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000;">Unit</th>
                <th colspan="7" style="text-align: center; border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000;">Description</th>
                <th style="text-align: center; border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000;">Unit</th>
                <th style="text-align: center; border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000;">Total</th>
            </tr>
            @php $total = 0; @endphp
            @foreach($jo->items as $item)
                <tr>
                    <td style="border: 1px solid #000;" class="text-center">{{ $item->item_no }}</td>
                    <td style="border: 1px solid #000;" class="text-center">{{ $item->quantity }}</td>
                    <td style="border: 1px solid #000;" class="text-center">{{ $item->unit }}</td>
                    <td style="border: 1px solid #000;" class="text-center" colspan="7">{{ $item->description }}</td>
                    <td style="border: 1px solid #000;" class="text-center">{{ $item->type === 'parent' ? '' : $item->pivot->unit_cost }}</td>
                    <td style="border: 1px solid #000;" class="text-center">{{ $item->type === 'parent' ? '' : $item->pivot->total }}</td>
                    @php $total += $item->type === 'parent' ? 0 : $item->pivot->total; @endphp
                </tr>
            @endforeach
            <tr>
                <td colspan="7" style="border-left: 1px solid #000; border-right: 1px solid #000;">Amount in Words (Gross):</td>
                <td colspan="3">Total:</td>
                <td style="text-align: right;">***</td>
                <td style="border-right: 1px solid #000;"></td>
            </tr>
            <tr>
            <td colspan="7" style="border-left: 1px solid #000; border-right: 1px solid #000;">{{ $jo->amount_in_words }}</td>
                <td colspan="4">(***Subject to all applicable taxes)</td>
                <td style="border-right: 1px solid #000;">{{ $total }}</td>
            </tr>
            <tr>
                <td colspan="7" style="border-left: 1px solid #000; border-right: 1px solid #000; border-bottom: 1px solid #000"></td>
                <td colspan="5" style="border-left: 1px solid #000; border-right: 1px solid #000; border-bottom: 1px solid #000"></td>
            </tr>
            <tr>
                <th style="border-left: 1px solid #000; text-align: left;">4</th>
                <td colspan="2">Place of Delivery:</td>
                <td colspan="2" style="border-bottom: 1px solid #000;">{{ $jo->place_of_delivery }}</td>
                <td colspan="2" style="text-align: right;">Delivery Term:  </td>
                @if($jo->delivery_term === 'pickup')
                    <td style="border: 1px solid #000; background-color: #000;"></td>
                @else
                    <td style="border: 1px solid #000"></td>
                @endif
                <td> - Pick Up</td>
                @if($jo->delivery_term !== 'pickup')
                    <td style="border: 1px solid #000; background-color: #000;"></td>
                    <td colspan="2" style="text-align: right; border-right: 1px solid #000;">- Deliver w/in {{ $jo->delivery_term }} calendar</td>
                @else
                    <td style="border: 1px solid #000;"></td>
                    <td colspan="2" style="text-align: right; border-right: 1px solid #000;">- Deliver w/in ____ calendar</td>
                @endif
            </tr>
            <tr>
                <td colspan="12" style="text-align: right; border-right: 1px solid #000; border-left: 1px solid #000;">days from receipt of JO</td>
            </tr>
            <tr>
                <td colspan="3" style="border-bottom: 1px solid #000; border-left: 1px solid #000;">Date of Delivery/Completion</td>
                <td colspan="2" style="border-bottom: 1px solid #000;">{{ $jo->date_of_delivery }}</td>
                <td colspan="2" style="text-align: right; border-bottom: 1px solid #000;">Payment Term:  </td>
                @if ($jo->payment_term === 'cod')
                    <td style="border: 1px solid #000; background-color: #000;"></td>
                @else
                    <td style="border: 1px solid #000"></td>
                @endif
                <td style="border-bottom: 1px solid #000;"> - COD</td>
                @if ($jo->payment_term !== 'cod')
                    <td style="border: 1px solid #000; background-color: #000;"></td>
                    <td colspan="2" style="text-align: right; border-bottom: 1px solid #000; border-right: 1px solid #000;">- w/in {{ $jo->payment_term }} days after delivery</td>
                @else
                    <td style="border: 1px solid #000;"></td>
                    <td colspan="2" style="text-align: right; border-bottom: 1px solid #000; border-right: 1px solid #000;">- w/in ___ days after delivery</td>
                @endif
            </tr>
            <tr>
                <td colspan="12" style="border: 1px solid #000;"></td>
            </tr>
            <tr>
                <td style="border-left: 1px solid #000;">Note:</td>
                <td colspan="10">All materials replaced during the repair job shall be surrendered upon delivery of equipment to facilitate processing of</td>
                <td style="border-right: 1px solid #000;"></td>
            </tr>
            <tr>
                <td style="border-left: 1px solid #000; border-bottom: 1px solid #000;"></td>
                <td colspan="10" style="border-bottom: 1px solid #000;">payment. Unless otherwise indicated, the above terms and conditions are demed accepted and form part thereof.</td>
                <td style="border-right: 1px solid #000; border-bottom: 1px solid #000;"></td>
            </tr>
            <tr>
                <th style="text-align: left; border-left: 1px solid #000;">5</th>
                <td colspan="11" style="border-right: 1px solid #000;">Penalty Provision:</td>
            </tr>
            <tr>
                <td style="border-left: 1px solid #000;"></td>
                <td colspan="11" style="text-indent: 5px; border-right: 1px solid #000;">In case of failure to make the full delivery within the time specified above, a penalty of one-tenth (1/10) of one percent</td>
            </tr>
            <tr>
                <td style="border-left: 1px solid #000;"></td>
                <td colspan="11" style="border-right: 1px solid #000;">for every day of delay shall be imposed.</td>
            </tr>
            <tr>
                <td colspan="12" style="text-align: center; border-left: 1px solid #000; border-right: 1px solid #000;">Very truly yours,</td>
            </tr>
            <tr>
                <td colspan="12" style="border-left: 1px solid #000; border-right: 1px solid #000;"></td>
            </tr>
            <tr>
                <td colspan="12" style="text-align: left; border-left: 1px solid #000; border-right: 1px solid #000;">Conforme:</td>
            </tr>
            <tr>
                <td style="border-left: 1px solid #000;"></td>
                <td colspan="3" style="border-bottom: 1px solid #000;"></td>
                <td></td>
                <td></td>
                <td></td>
                <td colspan="3" style="border-bottom: 1px solid #000;"></td>
                <td colspan="2" style="border-right: 1px solid #000;"></td>
            </tr>
            <tr>
                <td style="border-left: 1px solid #000;"></td>
                <td colspan="3" style="text-align: center;">Supplier Signature over printed name</td>
                <td></td>
                <td></td>
                <td></td>
                <td colspan="3" style="text-align: center;">Executive Director</td>
                <td colspan="2" style="border-right: 1px solid #000;"></td>
            </tr>
            <tr>
                <td style="border-left: 1px solid #000;"></td>
                <td style="text-align: right;">Date:</td>
                <td colspan="2" style="border-bottom: 1px solid #000;"></td>
                <td></td>
                <td></td>
                <td></td>
                <td style="text-align: right;">Date:</td>
                <td colspan="2" style="border-bottom: 1px solid #000;"></td>
                <td colspan="2" style="border-right: 1px solid #000;"></td>
            </tr>
            <tr>
                <td colspan="12" style="border-left: 1px solid #000; border-right: 1px solid #000; border-bottom: 1px solid #000;"></td>
            </tr>
            <tr>
                <th colspan="6" style="border-left: 1px solid #000; border-right: 1px solid #000;">6</th>
                <th colspan="6" style="border-right: 1px solid #000;"></th>
            </tr>
            <tr>
                <td colspan="6" style="border-left: 1px solid #000; border-right: 1px solid #000;">Funds Available:</td>
                <td>Amount:</td>
                <td colspan="2" style="border-bottom: 1px solid #000;"></td>
                <td colspan="2" style="text-align: right;">ALOBS No.:</td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;"></td>
                td
            </tr>
            <tr>
                <td colspan="6" style="border-left: 1px solid #000; border-right: 1px solid #000;"></td>
                <td>Date:</td>
                <td colspan="2" style="border-bottom: 1px solid #000;"></td>
                <td colspan="3" style="border-right: 1px solid #000;"></td>
            </tr>
            <tr>
                <td style="border-left: 1px solid #000;"></td>
                <td colspan="4" style="border-bottom: 1px solid #000;"></td>
                <td style="border-right: 1px solid #000;"></td>
                <td colspan="6" style="border-right: 1px solid #000;"></td>
            </tr>
            <tr>
                <td colspan="6" style="border-left: 1px solid #000; border-right: 1px solid #000; border-bottom: 1px solid #000;"></td>
                <td colspan="6" style="border-left: 1px solid #000; border-right: 1px solid #000; border-bottom: 1px solid #000;"></td>
            </tr>
            <tr></tr>
            <tr>
                <td>Generated as of {{ $jo->dateToday }}</td>
            </tr>
        </tbody>
    </table>
</body>
</html>