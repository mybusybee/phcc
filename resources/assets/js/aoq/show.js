$(function(){
    var req_count = $('.req_row').length;

    $('th#elig_req').attr('rowspan', req_count + 3);
});

$(function(){
    $('.fp-box').each(function(){
        var val = $(this).data('val');
        var max = $(this).closest('tr').data('max');

        console.log(val);

        val > max ? $(this).addClass('error') : $(this).removeClass('error');
    })

    $('.sup_total_bid').each(function(){
        var val = $(this).data('total-bid');
        var max = $(this).closest('tr').data('total-estimate');

        val > max ? $(this).addClass('error') : $(this).removeClass('error');
    })
})