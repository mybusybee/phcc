import { updateCompliantSupplier } from '../abstracts/total-cost-calculator'

$(function(){

    $('input[name="opening_date"]').datepicker({
        dateFormat:'MM d, yy',
        minDate: 0,
    });


    var aoq_counter = $('#abstract_co').val();

    var year = String(new Date().getFullYear());
    var month = String(new Date().getMonth() + 1);
    month = month <= 9 ? "0" + month : month;
    var unique;

    if(aoq_counter == 0 ){
        unique = '0001';
    } else {
        var new_aoq_counter = parseInt(aoq_counter) + 1;
        if(new_aoq_counter.toString().length == 1) {
            unique = '000' + parseInt(new_aoq_counter);
        } else if(new_aoq_counter.toString().length == 2) {
            unique = '00' + parseInt(new_aoq_counter);
        } else if(new_aoq_counter.toString().length == 3) {
            unique = '0' + parseInt(new_aoq_counter);
        } else {
            unique = parseInt(new_aoq_counter);
        }
    }

    var aoq_no = year + month + '-' +unique;

    $('#aoq_no').val(aoq_no);
});

//this will dynamically add a text input below the dropdown and remove it on change
$(document).on('change', '.document-select', function(){
    let document_type = $(this).val();
    let doc_container_id = $(this).closest('td').attr('id');
    let doc_container_id_tokens = doc_container_id.split("_");
    let doc_container_row_no = doc_container_id_tokens[doc_container_id_tokens.length - 1];

    if (document_type == 'f9'){
        let other_doc_input = "";

        other_doc_input = $(`<input type="text" name="eligibility_docs_extra_${doc_container_row_no}" id="additional_doc" class="additional_doc form-control form-control-sm border border-info mt-2" style="width:95%; float:right">`);

        other_doc_input.appendTo('#' + doc_container_id);
    }else{
        $(this).next('.additional_doc').remove();
    }
})

$(document).on('change', '.compliance-select', function(){
    // debugger
    let compliance_cell_id = $(this).attr('id');
    let compliance_cell_id_tokens = compliance_cell_id.split("_");
    let compliance_suffix = compliance_cell_id_tokens[compliance_cell_id_tokens.length - 1];

    let supplier_compliance_array = document.getElementsByClassName("compliance_"+compliance_suffix);

    let compliance = true;
    for (let sup_ctr = 0; sup_ctr < supplier_compliance_array.length; sup_ctr++){
        let supplier_compliance = supplier_compliance_array[sup_ctr].value;
        if (supplier_compliance == "Not Comply"){
            compliance = false;
        }
    }

    if (compliance){
        $('#result_compliance_' + compliance_suffix).val('COMPLIANT');
    }else{
        $('#result_compliance_' + compliance_suffix).val('NON-COMPLIANT');
    }

    updateCompliantSupplier();
})

// function updateCompliantSupplier(){
//     let current_suppliers = $('select#reco_supplier option').clone();

//     let updated_supplier_select = $('select#reco_supplier_updated option').remove();
    
//     for(let ctr = 0; ctr < current_suppliers.length; ctr++){
//         let sup_val = current_suppliers[ctr].value;

//         let sup_compliance = $('.result_compliance_supplier_id_'+sup_val).val();
//         let ranking = $('#supplier_ranking'+ctr).val();

//         if ((sup_compliance === 'COMPLIANT') && (ranking !== 'Bid Over ABC' && ranking !== 'N/A')){
//             $('select#reco_supplier_updated').append(current_suppliers[ctr]);
//         }
//     }
// }