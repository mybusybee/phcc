import Vue from 'vue'
import App from '../components/auth/base'
import router from './router'
import axios from 'axios'
import { baseURL } from './baseURL'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
Vue.use(Vuetify)
axios.defaults.baseURL = baseURL();

window.Event = new Vue()

new Vue({
    router,
    render: a => a(App)
}).$mount('#auth')