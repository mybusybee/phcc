import Vue from 'vue'
import Router from 'vue-router'
import PasswordReset from '../components/password-reset'
import Login from '../components/auth/login'
import PasswordResetForm from '../components/password-reset-form'

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/phcc/public/login',
            name: 'Login',
            component: Login
        },
        {
            path: 'password-reset',
            name: 'Password Reset',
            component: PasswordReset
        },
        {
            path: '/password/reset/:token',
            name: 'Password Reset Form',
            component: PasswordResetForm
        }
    ]
})