for (i = new Date().getFullYear() + 10; i >= new Date().getFullYear(); i--)
{
    $('#year').append($('<option />').val(i).html(i));
}

$('#delivery_date, #acquireDocuments').datepicker({
    dateFormat: 'MM dd, yy',
    minDate: 0,
})

$('#prebid_conf_date, #bids_deadline, #bid_opening_datetime').datetimepicker({
    format:'F d, Y h:i A',
    step: 30,
    formatTime: 'h:i A',
    validateOnBlur: false,
    minDate: 0,
});