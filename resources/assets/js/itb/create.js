import Vue from 'vue'

new Vue({
    data() {
        return {
            itbNo: null,
        }
    },
    methods: {
        initators(){
            $('#acquireDocuments').datepicker({
                dateFormat: 'MM dd, yy',
                minDate: 0,
            })
            $('#delivery_date').datepicker({
                constrainInput: false,
                dateFormat: 'MM dd, yy',
                minDate: 0,
            })

            $('#prebid_conf_date, #bids_deadline, #bid_opening_datetime').datetimepicker({
                format:'F d, Y h:i A',
                step: 30,
                formatTime: 'h:i A',
                validateOnBlur: false,
                minDate: 0,
            });

            $('input[name="bidding_documents_fee"]').mask('Z##,###,###,###,###.00', {
                reverse: true,
                translation: {
                    'Z': {
                        pattern: /[1-9]/
                    },
                    'Y': {
                        pattern: /[0-9]/
                    }
                }
            });
        },
        itbNumberFiller(){
            let year = String(new Date().getFullYear());
            let month = String(new Date().getMonth() + 1);
            let itbCounter = $('#itbCount').val()
            month = month <= 9 ? "0" + month : month;
            let unique
            itbCounter++
            if(itbCounter.toString().length == 1) {
                unique = `000${itbCounter}`
            } else if (itbCounter.toString().length == 2) {
                unique = `00${itbCounter}`
            } else if (itbCounter.toString().length == 3) {
                unique = `0${itbCounter}`
            } else {
                unique = itbCounter
            }
            this.itbNo = `${year}${month}-${unique}`
        }
    },
    mounted() {
        this.initators()
        this.itbNumberFiller()
    }
}).$mount('#itb_form')

$(document).on('click', '.add_new_file', function(){
    let prFilesCounter = $('.rfq-files').length

    if(prFilesCounter < 5){
        let newFileRow = $(`
            <div class="input-group">
                <input type="file" name="invite_requirements[]" class="form-control rfq-files" accept=".pdf">
                <div class="input-group-append">
                    <button class="btn btn-danger remove_file_row" type="button"><i class="fa fa-trash"></i></button>
                </div>
            </div>
            `)

        newFileRow.appendTo('div#files_section')
    } else {
        alert('Attached files must not be more than 5!')
    }
})

$(document).on('click', '.remove_file_row', function(){
    $(this).closest('div.input-group').remove()
})