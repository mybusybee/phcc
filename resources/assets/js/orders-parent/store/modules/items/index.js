import * as getters from './getters'
import * as actions from './actions'
import * as mutations from './mutations'

export default {
    state: {
        items: null,
        totalCost: 0,
    },
    getters, actions, mutations
}