import axios from 'axios'
export const setItems = ({commit}, payload) => {
    axios.get(`/api/abstract/${payload.abstractID}/recommendations/supplier/${payload.supplierID}`)
        .then(res => {
            commit('setItems', res.data)
            commit('setTotalCost', totalCost(res.data))
        })
        .catch(err => console.log(err))
}

export const clearItems = ({commit}) => {
    commit('clearItems')
}

let totalCost = (data) => {
    let total = 0
    for(let i = 0; i < data.length; i++){
        total += parseFloat(data[i].pivot ? data[i].pivot.total : 0)
    }
    return total
}