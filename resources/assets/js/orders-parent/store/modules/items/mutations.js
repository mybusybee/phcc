export const setItems = (state, payload) => {
    state.items = payload
}

export const clearItems = (state) => {
    state.items = null
}

export const setTotalCost = (state, payload) => {
    state.totalCost = payload
}