export const getAllItems = state => {
    return state.items
}

export const getTotalCost = state => {
    return state.totalCost.toLocaleString()
}