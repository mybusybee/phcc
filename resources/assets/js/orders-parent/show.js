$('.add-file').on('click', function(){
    console.log('add')
    let rowsCount = $('.file').length;
    if(rowsCount < 5){
        let row = $(`
            <div class="input-group">
                <input type="file" name="attachments[]" class="form-control form-control-sm file" id="" accept=".pdf" required>
                <div class="input-group-append">
                    <button class="btn btn-danger remove-file" type="button"><i class="fas fa-trash"></i> Remove</button>
                </div>
            </div>
        `)
        row.appendTo('div#files_section')
        $('#attachFiles').attr('disabled', false)
    } else {
        alert('Maximum of 5 files only!')
    }
})

$(document).on('click', '.remove-file', function(){
    $(this).closest('div.input-group').remove();
    let rowsCount = $('.file').length;
    if(rowsCount === 0) {
        $('#attachFiles').attr('disabled', true)
    }
})