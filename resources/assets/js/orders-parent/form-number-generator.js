export const generateFormNumber = count => {
    let year = String(new Date().getFullYear());
    let month = String(new Date().getMonth() + 1);
    month = month <= 9 ? "0" + month : month;
    let unique;

    if(count == 0 ){
        unique = '0001';
    } else {
        let new_abs_count = parseInt(count) + 1;
        if(new_abs_count.toString().length == 1) {
            unique = '000' + parseInt(new_abs_count);
        } else if(new_abs_count.toString().length == 2) {
            unique = '00' + parseInt(new_abs_count);
        } else if(new_abs_count.toString().length == 3) {
            unique = '0' + parseInt(new_abs_count);
        } else {
            unique = parseInt(new_abs_count);
        }
    }

    let formNumber = year + month + '-' +unique;

    return formNumber;
}