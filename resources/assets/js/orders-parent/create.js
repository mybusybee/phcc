import Vue from 'vue'
import store from './store'
import { mapActions, mapGetters } from 'vuex';
import axios from 'axios'
import { generateFormNumber } from './form-number-generator'
import { baseURL } from '../auth/baseURL'

axios.defaults.baseURL = baseURL()

new Vue({
    store,
    data() {
        return {
            emptyItemText: 'No Items Available',
            supplier: {
                address: null,
                telephone: null,
                tin: null,
            },
            formNumber: null,
        }
    },
    methods :{
        ...mapActions([
            'setItems', 'clearItems'
        ]),
        supplierChanged(event){
            this.clearItems()
            this.emptyItemText = 'Loading...'
            let data = {
                abstractID: this.$refs.abstractID.value,
                supplierID: event.srcElement.value
            }
            this.setItems(data)
            this.setSupplierData(data.supplierID)
        },
        setSupplierData(id) {
            axios.get(`/api/supplier/${id}`)
                .then(res => {
                    this.supplier.address = res.data.address
                    this.supplier.telephone = res.data.telephone
                    this.supplier.tin = res.data.tin_number
                })
                .catch(err => console.log(err))
        }
    },
    computed: {
        ...mapGetters([
            'getAllItems', 'getTotalCost'
        ])
    },
    mounted() {
        if(this.$refs.formType.value === 'edit'){
            let selectedSupplierID = this.$refs.selectedSupplier.value
            let data = {
                abstractID: this.$refs.abstractID.value,
                supplierID: selectedSupplierID
            }
            this.setItems(data)
            this.setSupplierData(selectedSupplierID)
        } else {
            this.formNumber = generateFormNumber(this.$refs.ordersCount.value)
        }
    }
}).$mount('#orderForm')

$(function() {
    $('#formDate, #dateOfDeliveryCompletion').datepicker({
        dateFormat:'MM d, yy',
        minDate: 0,
    });

    $('input[name="delivery_term"]').on('change', function(){
        let val = $(this).val()

        if(val === 'days') {
            $('input[name="calendar_days"]').attr('required', true);
        } else {
            $('input[name="calendar_days"]').attr('required', false);
        }
    })

    $('input[name="payment_term"]').on('change', function() {
        let val = $(this).val();

        if (val === 'days after delivery') {
            $('input[name="payment_after_delivery"]').attr('required', true)
        } else {
            $('input[name="payment_after_delivery"]').attr('required', false)
        }
    })

    let supplierID = $('#selectedSupplier').val()
    $('#supplierList').find(`option[value="${supplierID}"]`).attr('selected', true)
})