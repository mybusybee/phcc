//this is for the email
$(document).on('click', '#addNewEmail', function(){
    let emailAddressCounter = $('.email-rows').length

    if (emailAddressCounter < 10) {
        const row = $(`
            <div class="input-group">
                <input class="form-control form-control-sm email-rows" name="email[]" type="email">
                <div class="input-group-append">
                    <button type="button" id="removeEmailRow" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                </div>
            </div>
        `)

        row.appendTo('#emailSection')    
    }else{
        alert('Attached files must not be more than 10!')
    }
    
})

$(document).on('click', '#removeEmailRow', function(){
    $(this).closest('div.input-group').remove()
})

//this is for the file attachments
$(document).on('click', '.add_new_file', function(){
    let supplierDocsCounter = $('.sup-docs').length

    if(supplierDocsCounter < 10){
        let newFileRow = $(`
            <div class="input-group">
                <input type="file" name="supplier_docs[]" class="form-control sup-docs" accept=".pdf">
                <div class="input-group-append">
                    <button class="btn btn-danger remove_file_row" type="button"><i class="fa fa-trash"></i></button>
                </div>
            </div>
            `)

        newFileRow.appendTo('div#files_section')
    } else {
        alert('Attached files must not be more than 10!')
    }
})

$(document).on('click', '.remove_file_row', function(){
    $(this).closest('div.input-group').remove()
})