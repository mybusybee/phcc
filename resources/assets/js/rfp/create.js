$(function(){
    $('#suppliers-datatable').DataTable({
        colReorder: true,
        responsive: true,
        "lengthMenu": [ [ 5, 10, 20, 50, -1 ], [5, 10, 20, 50 , 'All'] ],
        'order': [],
        columnDefs: [{
            targets: [0],
            orderable: false
        }]
    });

    $('input[name="submission_date_time"]').datetimepicker({
        format:'F d, Y h:i A',
        step: 30,
        formatTime: 'h:i A',
        minDate: 0,
        validateOnBlur: false,
    });

     /**
     * 
     * RFP Number Generator
     * 
     */
    let rfp_counter = $('#rfp_co').val();

	let year = String(new Date().getFullYear());
	let month = String(new Date().getMonth() + 1);
	month = month <= 9 ? "0" + month : month;
	let unique;
	
	if(rfp_counter == 0 ){
		unique = '0001';
	} else {
		let new_rfp_count = parseInt(rfp_counter) + 1;
		if(new_rfp_count.toString().length == 1) {
			unique = '000' + parseInt(new_rfp_count);
		} else if(new_rfp_count.toString().length == 2) {
			unique = '00' + parseInt(new_rfp_count);
		} else if(new_rfp_count.toString().length == 3) {
			unique = '0' + parseInt(new_rfp_count);
		} else {
			unique = parseInt(new_rfp_count);
		}
	}

    let rfp_no = year + month + '-' +unique;
    
    $('#rfp_no').val(rfp_no);
    
});

$('.sup_id').on('change', function(){
    var id = $(this).val();

    if($(this).is(':checked')){
        var row = $(`
            <input type="hidden" class="form-control form-control-sm" value="`+id+`" name="supplier_id[]">
        `);

        row.appendTo('.sup_ids_container');
    } else {
        $('div.sup_ids_container').find('input[value="'+id+'"]').remove();
    }
});

$(document).on('click', '.add_new_file', function(){
    let prFilesCounter = $('.rfp-files').length

    if(prFilesCounter < 5){
        let newFileRow = $(`
            <div class="input-group">
                <input type="file" name="invite_requirements[]" class="form-control rfp-files" accept=".pdf">
                <div class="input-group-append">
                    <button class="btn btn-danger remove_file_row" type="button"><i class="fa fa-trash"></i></button>
                </div>
            </div>
            `)

        newFileRow.appendTo('div#files_section')
    } else {
        alert('Attached files must not be more than 5!')
    }
})

$(document).on('click', '.remove_file_row', function(){
    $(this).closest('div.input-group').remove()
})

$('#delivery').on('click', function(){
    if($(this).is(':checked')) {
        $('#deliveryPlace').attr('readonly', false)
    } else {
        $('#deliveryPlace').attr('readonly', true)
    }
})

$('#paymentTerms').on('click', function(){
    if($(this).is(':checked')) {
        $('input[name="payment_terms"]').attr('readonly', false)
    } else {
        $('input[name="payment_terms"]').attr('readonly', true)
    }
})