let IDArray = [];
$(function(){
    $('.supplier-id-class').each(function() {
        let id = $(this).val()
        IDArray.push(id)
    })
})

$(document).on('change', '.sup_id', function(){
    let supplierID = $(this).val()
    if(IDArray.indexOf(supplierID) >= 0) {
        IDArray.splice(IDArray.indexOf(supplierID), 1)
        $(`input[name="selectedSuppliers[]"][value="${supplierID}"]`).remove()
    } else {
        IDArray.push(supplierID)
        insertNewRow(supplierID)
    }
})


let insertNewRow = id => {
    let row = $(`
        <input type="hidden" name="selectedSuppliers[]" class="form-control form-control-sm d-inline-block w-25 supplier-id-class" value="${id}">
    `)

    row.appendTo('div#supplierList')
}
$('#suppliers-datatable').DataTable()

$('#submitBtn').on('click', function(){
    let checked = $("input[type=checkbox]:checked").length

    if(!checked) {
        alert("You must check at least one checkbox.");
        return false;
    }
})

$(document).on('change', 'input[type="radio"][name="submitType"]', function(){
    let val = $(this).val()

    if (val === 'preselect') {
        $('#submitBtn').html('Save')
        $('#submitBtn').attr('disabled', false)
    } else {
        $('#submitBtn').html('Send')
        $('#submitBtn').attr('disabled', false)
    }
})