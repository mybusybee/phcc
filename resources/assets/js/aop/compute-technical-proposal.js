import { updateCompliantSupplier } from '../abstracts/total-cost-calculator'

$(document).on('change', '.tech_proposals', function(){
	 debugger
	let tech_proposal_base = parseFloat($('.tech_proposal_base').val());
	let tech_proposal = parseFloat($(this).val());
	let tech_proposal_id = $(this).attr('id');
	let tech_proposal_id_token = tech_proposal_id.split('_');
	let tech_proposal_ctr = tech_proposal_id_token[tech_proposal_id_token.length-1];

	if (tech_proposal < tech_proposal_base){
		$(this).parent().parent().addClass('with-error');
		$('#tech_proposal_res_'+tech_proposal_ctr).val('FAILED');
	}else{
		$(this).parent().parent().removeClass('with-error');
		$('#tech_proposal_res_'+tech_proposal_ctr).val('PASSED');
	}

	updateCompliantSupplier();
})

$(document).on('change', '.tech_proposal_base', function(){
	// debugger
	let tech_proposal_base = parseFloat($(this).val());

	let tech_proposals = document.getElementsByClassName("tech_proposals");
	for (let tp_ctr = 0; tp_ctr < tech_proposals.length; tp_ctr++){
		let tech_proposal = parseFloat(tech_proposals[tp_ctr].value);
		let tech_proposal_id = $('#tech_proposal_'+tp_ctr).attr('id');
		let tech_proposal_id_token = tech_proposal_id.split('_');
		let tech_proposal_ctr = tech_proposal_id_token[tech_proposal_id_token.length-1];

		if (tech_proposal < tech_proposal_base){
			$('#tech_proposal_'+tp_ctr).parent().parent().addClass('with-error');
			$('#tech_proposal_res_'+tech_proposal_ctr).val('FAILED');
		}else{
			$('#tech_proposal_'+tp_ctr).parent().removeClass('with-error');
			$('#tech_proposal_res_'+tech_proposal_ctr).val('PASSED');
		}

		updateCompliantSupplier();
	}

	
})