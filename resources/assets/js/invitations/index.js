import { baseURL } from '../auth/baseURL'
$(function(){
    $('#invitesTable').DataTable({
        "aaSorting": [],
    })
})

$(document).on('change', '.formSelector', function(){
    let val = $(this).val()
    let button = $(this).closest('tr').find('.generateBtn')
    let id = $(this).closest('tr').attr('id')

    if(val === 'aob'){
        $(button).attr('href', `${baseURL()}aob/create/${id}`)
    } else if (val === 'aop') {
        $(button).attr('href', `${baseURL()}aop/create/${id}`)
    } else if (val === 'aoq') {
        $(button).attr('href', `${baseURL()}aoq/create/${id}`)
    } else {
        $(button).attr('href', `/`)
    }
})