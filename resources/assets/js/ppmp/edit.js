$(document).on('keyup', '.pap_mooe, .pap_co',function() {
    let row_counter = $(this).closest('tr').find('.row_counter').val()
    let total_allocation = 0
    let total_estimated_budget = $(this).closest('tr').find('.pap_est_budget').val()
    total_estimated_budget = total_estimated_budget.replace(/\,/g,'')

    let total_budget_category = 0;

    let mooe = $(this).closest('tr').find('.pap_mooe').val()
    mooe = mooe.replace(/\,/g,'')
    mooe = parseFloat(mooe);
    if (isNaN(mooe)){
        mooe = 0;
    }
    total_budget_category += mooe;

    let co = $(this).closest('tr').find('.pap_co').val()
    co = co.replace(/\,/g,'')
    co = parseFloat(co);
    if (isNaN(co)){
        co = 0;
    }
    total_budget_category += co;


    if(total_budget_category != parseFloat(total_estimated_budget)){
        $(this).closest('table.table').find('.add-pap').attr('disabled', true)
        $(this).closest('tr').addClass('est-budget-error')
    } else {
        $(this).closest('table.table').find('.add-pap').attr('disabled', false)
        $(this).closest('tr').removeClass('est-budget-error')
    }
})

$(document).on('keyup', '.pap_mooe', function() {
    let row = $(this).closest('tr').attr('id')
    let mooe = $(`tr#${row}`).find('.pap_mooe').val()
    let mooe_float = mooe
    if (mooe != "0.00"){
        mooe=mooe.replace(/\,/g,'')
        mooe_float = parseFloat(mooe)
    }
    let mooe_formatted = mooe_float.toLocaleString()
    if (mooe.charAt(mooe.length - 1) == "."){
        mooe_formatted += "."
    }

    if (isNaN(mooe_float)){
        $(`tr#${row}`).find('.pap_mooe').val("0.00")
    }else{
        $(`tr#${row}`).find('.pap_mooe').val(mooe_formatted)    
    }
})

$(document).on('keyup', '.pap_co', function() {
    let row = $(this).closest('tr').attr('id')
    let co = $(`tr#${row}`).find('.pap_co').val()
    let co_float = co
    if (co != "0.00"){
        co=co.replace(/\,/g,'')
        co_float = parseFloat(co)
    }
    let co_formatted = co_float.toLocaleString()
    if (co.charAt(co.length - 1) == "."){
        co_formatted += "."
    }

    if (isNaN(co_float)){
        $(`tr#${row}`).find('.pap_co').val("0.00")        
    }else{
        $(`tr#${row}`).find('.pap_co').val(co_formatted)    
    }
    
})

// $(document).on('change',  function(){
//     let error_classes = document.getElementsByClassName("est-budget-error")

//     $('table.ppmp-tbl').find('tr').each(function(){
//         let mooe = $(this).find('.pap_mooe').val();
//         let co = $(this).find('.pap_co').val();

//         if($(this).hasClass('est-budget-error') || (mooe == "0.00" && co == "0.00")){
//             $('#submit-btn').attr("disabled", true)
//             return false
//         } else {
//             $('#submit-btn').attr("disabled", false)
//         }
//     })
// })

$(function(){
    let inputType = $('.mooe-co').attr('type')

    if(inputType !== 'hidden') {
        $('.mooe-co').each(function(){
            $(this).closest('tr').addClass('est-budget-error')
        })
    }
})

$(function(){
    $('#openingDate').datepicker({
        dateFormat:'MM d, yy',
    });
})

$(function(){
    $('.procurement_modes_select').on('change', function() {
 
		let budget = $(this).closest('tr').find('.pap_est_budget').val()
		budget=budget.replace(/\,/g,'')
		var budget_value = parseFloat(budget);


		max_value = $(this).find(`:selected`).data('max-value');
		min_value = $(this).find(`:selected`).data('min-value');

		if((min_value !== 0) && (max_value !== 0)){
			if(budget_value < min_value) {
				alert('Budget too low for selected mode of procurement!');
				$(this).val("").trigger('change');
			}
			if(budget_value > max_value){
				alert('Budget too high for selected mode of procurement!');
				$(this).val("").trigger('change');
			}
		} else if ((min_value === 0) && (max_value !== 0)){
			if(budget_value < min_value) {
				alert('Budget too low for selected mode of procurement!');
				$(this).val("").trigger('change');
			}
			if(budget_value > max_value){
				alert('Budget too high for selected mode of procurement!');
				$(this).val("").trigger('change');
			}
		}
	})
})

//delete row
$('table.ppmp-tbl').on('click', '.remove-pap-row', function(){

	max_value = 0
	min_value = 0

	$(this).closest('tr').nextAll().find(".pap_code").each(function(){
		var name = $(this).attr('name');
		var i = name.substring(8).slice(0);
		i -= 1;
		$(this).attr('name', 'pap_code' + i);
	});

	$(this).closest('tr').nextAll().find(".pap_gen_desc").each(function(){
		var name = $(this).attr('name');
		var i = name.substring(12).slice(0);
		i -= 1;
		$(this).attr('name', 'pap_gen_desc' + i);
	});

	$(this).closest('tr').nextAll().find(".allotment_type").each(function(){
		var name = $(this).attr('name');
		var i = name.substring(14).slice(0);
		i -= 1;
		$(this).attr('name', 'allotment_type' + i);
	});

	$(this).closest('tr').nextAll().find(".pap-uacs-object").each(function(){
		var name = $(this).attr('name');
		var i = name.substring(15).slice(0);
		i -= 1;
		$(this).attr('name', 'pap-uacs-object' + i);
	});

	$(this).closest('tr').nextAll().find(".pap_qty").each(function(){
		var name = $(this).attr('name');
		var i = name.substring(7).slice(0);
		i -= 1;
		$(this).attr('name', 'pap_qty' + i);
	});

	$(this).closest('tr').nextAll().find(".pap_est_budget").each(function(){
		var name = $(this).attr('name');
		var i = name.substring(14).slice(0);
		i -= 1;
		$(this).attr('name', 'pap_est_budget' + i);
	});

	$(this).closest('tr').nextAll().find(".procurement_modes_select").each(function(){
		var name = $(this).attr('name');
		var count = name.substring(23);
		var i = count.slice(0, count.length - 2 );
		i -= 1;
		$(this).attr('name', 'pap_mode_of_procurement' + i +'[]');
	});

	$(this).closest('tr').nextAll().find(".allocation-month").each(function(){
		var name = $(this).attr('name');
		var count = name.substring(10);
		var i = count.slice(0, count.length - 2 );
		i -= 1;
		$(this).attr('name', 'allocation' + i +'[]');
	});

	$(this).closest('tr.pap-row').nextAll().each(function(){
		var id = $(this).attr('id');
		var count = id.substring(0);
		count -= 1;
		$(this).attr('id', count);
	});

	$(this).parent().parent().remove();

	//compute budget, inflation, contingency, total_est_budget
	compute_budgets();
	row_counter -= 1;
});

let compute_budgets = () => {
	let total_budget = 0
	$('.pap_est_budget').each(function() {
		let budgetValue = $(this).val()
        budgetValue=budgetValue.replace(/\,/g,'')
		total_budget += parseFloat(budgetValue)
	})
	if(isNaN(total_budget)){
		total_budget = '0.00'
	}else{
		total_budget = total_budget.toLocaleString()
	}

	$('#total_estimated_budget').val(total_budget)
}

$(document).on('keyup', '.remarks', function(){
    $('.remarks').each(function(){
        if($(this).val() !== '') {
			$('#submit-btn').attr('disabled', true)
			$('#budgetRejectBtn').attr('disabled', false)
            return false
        } else {
			$('#submit-btn').attr('disabled', false)
			$('#budgetRejectBtn').attr('disabled', true)
        }
    })
})

$(document).on('keyup', '.procurement_remarks', function(){
    $('.procurement_remarks').each(function(){
        if($(this).val() !== '') {
			$('#submit-btn').attr('disabled', true)
			$('#procRejectBtn').attr('disabled', false)
            return false
        } else {
			$('#submit-btn').attr('disabled', false)
			$('#procRejectBtn').attr('disabled', true)
        }
    })
})

$(function(){
	$('.allocation-month').mask('Z##,###,###,###,###.00', {
		reverse: true,
		translation: {
			'Z': {
				pattern: /[1-9]/
			},
			'Y': {
				pattern: /[0-9]/
			}
		}
	});

	$('.pap_qty').mask('#0000000')
})

$(function() {
	$('.within-wfp-submit').on('click', function() {
		$('#hiddenBudgetValidation').val('1');
		$('#ppmpform').submit();
	})
	$('#budgetRejectBtn').on('click', function() {
		$('#hiddenBudgetValidation').val('0');
		$('#ppmpform').submit();
	})
})

$(function() {
	$('.procurementValidate').on('click', function() {
		$('#hiddenProcValidation').val('1');
		$('#ppmpform').submit();
	})
	$('#procRejectBtn').on('click', function() {
		$('#hiddenProcValidation').val('0');
		$('#ppmpform').submit();
	})
})