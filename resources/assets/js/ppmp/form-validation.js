

let validated = row => {
    let code = $(`tr#${row}`).find('.pap_code').val()
    let desc = $(`tr#${row}`).find('.pap_gen_desc').val()
    let qty = $(`tr#${row}`).find('.pap_qty').val()
    let budget = $(`tr#${row}`).find('.pap_est_budget').val()
    let mode = $(`tr#${row}`).find('.procurement_modes_select').val()
    let allocation = false
    $(`tr#${row}`).find('.allocation-month').each(function(){
        let allocationValue = $(this).val()
        if(allocationValue != ''){
            allocation = true
        } else {
            allocation = false
            return false
        }
    })

    if (
        (desc != '') &&
        (budget != '') &&
        (mode != null) &&
        (allocation == true)
        ) {
            return true
        } else {
            return false
        }
}

// $(document).on('keypress, blur', '.pap_code, .pap_gen_desc, .pap_qty, .pap_est_budget, .allocation-month', function() {
//     let row = $(this).closest('tr').attr('id')
//     if(validated(row)){
//         $('.add-pap').attr('disabled', false)
//         // $('#submit-btn').attr('disabled', false)
//         console.log('validated')
//     } else {
//         $('.add-pap').attr('disabled', true)
//         // $('#submit-btn').attr('disabled', true)
//         console.log('not validated')
//     }
// })

$(document).on('change', '.procurement_modes_select', function(){
    let row = $(this).closest('tr').attr('id')
    validated(row)
})

$(document).on('keyup', '.pap_est_budget', function() {
    let row = $(this).closest('tr').attr('id')
    let budget = $(`tr#${row}`).find('.pap_est_budget').val()
    let budget_float = budget
    if (budget != "0.00"){
        budget=budget.replace(/\,/g,'')
        budget_float = parseFloat(budget)
    }
    let budget_formatted = budget_float.toLocaleString()
    if (budget.charAt(budget.length - 1) == "."){
        budget_formatted += "."
    }

    if (isNaN(budget_float)){
        $(`tr#${row}`).find('.pap_est_budget').val("0.00")
    }else{
        $(`tr#${row}`).find('.pap_est_budget').val(budget_formatted)
    }
    
})

// $(document).on('keyup', '.allocation-month', function() {
//     let row = $(this).closest('tr').attr('id')
//     $(`tr#${row}`).find('.allocation-month').each(function(){
//         let allocationValue = $(this).val()
//         let allocationValue_float = allocationValue
//         if (allocationValue != "0.00"){
//             allocationValue=allocationValue.replace(/\,/g,'')
//             allocationValue_float = parseFloat(allocationValue)
//         }
//         let allocationValue_formatted = allocationValue_float.toLocaleString()
//         if (allocationValue.charAt(allocationValue.length - 1) == "."){
//             allocationValue_formatted += "."
//         }

//         if (isNaN(allocationValue_float)){
//             $(this).val("0.00")
//         }else{
//             $(this).val(allocationValue_formatted)
//         }
        
//     })
// })

//procurement mode price validation on change of allocation price per month
$(document).on('change', '.allocation-month', function(){
    let totalAllocation = parseFloat(removeCommas($(this).closest('tr').find('input.pap_est_budget').val()))
    let procModeMax = $(this).closest('tr').find('select.procurement_modes_select').find(':selected').data('max-value')
    let procModeMin = $(this).closest('tr').find('select.procurement_modes_select').find(':selected').data('min-value')

    if( (totalAllocation > procModeMax) &&
        ( procModeMax !== 0 )
    ) {
        alert('Total allocation exceeded the maximum amount!')
        $(this).val('0.00')
        let row_id = $(this).closest('tr').attr('id')
        recalculateTotalAllocation(row_id)
    } else if ( (totalAllocation < procModeMin) &&
                ( procModeMin !== 0 )
    ) {
        alert('Total allocation is below the minimum amount!')
        $(this).val('0.00')
        let row_id = $(this).closest('tr').attr('id')
        recalculateTotalAllocation(row_id)
    }
})

$(document).on('change', 'select.procurement_modes_select', function(){
    let procModeMax = $(this).find(':selected').data('max-value')
    let procModeMin = $(this).find(':selected').data('min-value')
    let totalAllocation = $(this).closest('tr').find('.pap_est_budget').val()

    if( (parseFloat(removeCommas(totalAllocation)) > procModeMax) &&
        (procModeMax !== 0)
    ) {
        alert('Selected mode of procurement\'s maximum amount exceeds the current total budget!')
        $(this).val("").trigger('change');
    } else if ( (parseFloat(removeCommas(totalAllocation)) < procModeMin) &&
                ( procModeMin !== 0 ) &&
                ( parseFloat(removeCommas(totalAllocation)) !== 0 )
    ) {
        alert('Selected mode of procurement\'s minimum amount is lower than the current total budget!')
        $(this).val("").trigger('change');
    }
})

let recalculateTotalAllocation = row_id => {
    let totalAllocation = 0
    $(`tr#${row_id}`).find('.allocation-month').each(function(){
        let allocation = $(this).val()
        totalAllocation += parseFloat(removeCommas(allocation))
    })
    $(`tr#${row_id}`).find('.pap_est_budget').val(currencyConvert(totalAllocation))
}

const removeCommas = num => {
    return num.replace(/\,/g,'')
}

const currencyConvert = num => {
    return num.toLocaleString(undefined, {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
    })
}