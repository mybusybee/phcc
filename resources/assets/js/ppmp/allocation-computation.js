$(document).on('keyup', '.allocation-month',function() {

    //Prompt alert if user entered value in allocation first before total estimated budget
    if($(this).closest('tr').find('.pap_est_budget').val() == '') {
        //alert('Estimated budget is empty!')
        $(this).val('0.00')
    }

    let row_counter = $(this).closest('tr').find('.row_counter').val()
    let total_allocation = 0
    let total_estimated_budget = $(this).closest('tr').find('.pap_est_budget').val()
    total_estimated_budget = total_estimated_budget.replace(/\,/g,'')

    $(`.alloc-month-${row_counter}`).each(function() {
        let allocationValue = $(this).val()
        allocationValue=allocationValue.replace(/\,/g,'')
        total_allocation += parseFloat(allocationValue)
    })

    // if(total_allocation != parseFloat(total_estimated_budget)){
    //     $(this).closest('table.table').find('.add-pap').attr('disabled', true)
    //     $(this).closest('tr').addClass('est-budget-error')

    // } else {
    //     $(this).closest('table.table').find('.add-pap').attr('disabled', false)
    //     $(this).closest('tr').removeClass('est-budget-error')
    // }
    let row = $(this).closest('tr').attr('id')
    computeTotalAllocation(row)
})

$(document).on('keyup', '.pap_est_budget',function() {
    //Prompt alert if user entered value in allocation first before total estimated budget
    if($(this).closest('tr').find('.pap_est_budget').val() == '') {
        //alert('Estimated budget is empty!')
        $(this).val('0.00')
    }

    let row_counter = $(this).closest('tr').find('.row_counter').val()
    let total_allocation = 0
    let total_estimated_budget = $(this).closest('tr').find('.pap_est_budget').val()
    total_estimated_budget = total_estimated_budget.replace(/\,/g,'')

    $(`.alloc-month-${row_counter}`).each(function() {
        let allocationValue = $(this).val()
        allocationValue=allocationValue.replace(/\,/g,'')
        total_allocation += parseFloat(allocationValue)
    })

    // if(total_allocation != parseFloat(total_estimated_budget)){
    //     $(this).closest('table.table').find('.add-pap').attr('disabled', true)
    //     $(this).closest('tr').addClass('est-budget-error')

    // } else {
    //     $(this).closest('table.table').find('.add-pap').attr('disabled', false)
    //     $(this).closest('tr').removeClass('est-budget-error')

    // }
})


let computeTotalAllocation = row => {
    let totalAllocation = 0
    $(`tr#${row}`).find('.allocation-month').each(function(){
        let allocation = $(this).val()
        totalAllocation += parseFloat(removeCommas(allocation))
    })
    let formattedValue = currencyConvert(totalAllocation)
    $(`tr#${row}`).find('.pap_est_budget').val(formattedValue)
}

$(document).on('blur', '.allocation-month', function() {
    let val = $(this).val();
    if(val === '') {
        $(this).val('0.00')
    }
})

let removeCommas = num => {
    return num.replace(/\,/g,'')
}

let currencyConvert = num => {
    return num.toLocaleString(undefined, {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
    })
}