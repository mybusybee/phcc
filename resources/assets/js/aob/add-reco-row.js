/*
*
*
Add Recommendation Row
*
*
*/
let reco_row_counter = $('div.reco-section').length;
$(document).on('click', 'button.add-recommendation-row', function(){

	if (reco_row_counter < 30){
		let reco_row = $(`
			<div class="row reco-row mb-2" id="reco_row_${reco_row_counter}">
				<input type="hidden" name="reco_row_count[]" id="reco_row_count">
				<div class="col-xl-3">
					<select name="reco_supplier_${reco_row_counter}" id="reco_supplier_${reco_row_counter}" required class="reco_suppliers form-control form-control-sm border border-info w-50">
						<option value="" disabled selected>Please select one...</option>
					</select>
				</div>
				<div class="col-xl-2">
					<select name="reco_item_no_${reco_row_counter}[]" id="reco_item_no_${reco_row_counter}" required class="reco_item_nos form-control form-control-sm border border-info w-50" multiple>
						<option value="0">Please select the items</option>
					</select>
				</div>
				<div class="col-xl-3">
					<input type="text" name="reco_sub_total_${reco_row_counter}" id="reco_sub_total_${reco_row_counter}" class="reco_sub_totals form-control form-control-sm border border-info w-50" value="0" readonly>
				</div>
				<div class="col-xl-3">
					<input type="text" name="reco_remarks_${reco_row_counter}" class="form-control form-control-sm border border-info" required>
				</div>
				<div class="col-xl-1">
					<button type="button" class="remove-reco-row btn btn-danger btn-xs" style="float: right;">-</button>
				</div>
			</div>
			`);

		reco_row.appendTo('div.reco-section');

		$('select#reco_supplier_updated option').clone().appendTo('select[name="reco_supplier_'+reco_row_counter+'"]');
		$('select#reco_item_no option').clone().appendTo('select[name="reco_item_no_'+reco_row_counter+'[]"]');

		reco_row_counter++;
	} else {
        alert('Cannot add more than 30 recommendations!');
    }
	
});