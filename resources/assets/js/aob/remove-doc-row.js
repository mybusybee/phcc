$(document).on('click', 'button.remove-doc-row', function(){
	let eli_title_rowspan = document.getElementById("eli_req_title").rowSpan;

	$(this).closest('tr').remove();

	document.getElementById("eli_req_title").rowSpan = eli_title_rowspan - 1;

	//this will remove the Compliant and Non-Compliant text in result row
    let supplier_compliance_array = document.getElementsByClassName("document-select");
    if (supplier_compliance_array.length == 0){
    	let supplier_count = $("input[name='supplierList[]']").length;
        for (let i = 0; i < supplier_count; i++) {
        	$('#result_compliance_supplier' + i).text('');
        }	
    }
});