let other_docs_counter
$(function(){
	other_docs_counter = $('tr.other_docs').length - 1;
})

export const addOtherDocs = supplierCount => {
	let supplier_count = parseInt(supplierCount);
	let eli_title_rowspan = document.getElementById("eli_req_title").rowSpan;
	
	other_docs_counter++;

	let other_docs_row = $(`
		<tr>
			<td colspan="3" class="other_docs_cell" id="other_docs_cell_${other_docs_counter}">
				<input type="hidden" name="other_docs_count[]" id="legal_docs_count">
				<button type="button" class="remove-doc-row btn btn-danger btn-xs" style="float: left;">-</button>
	            <select name="other_docs_${other_docs_counter}" id="" required class="document-select form-control form-control-sm border border-info" style="width: 95%; float: right;">
	                <option value="" disabled selected>Select One</option>
	                <option value="e1">Bid Security</option>
	                <option value="e2">Omnibus Sworn Statement</option>
	                <option value="e3">Technical Specifications (for Goods)</option>
	                <option value="e4">Project Requirements (for Infra)</option>
	                <option value="e5">Corporate Secretary Certificate / Board Resolution for Authorized Representative</option>
	                <option value="e9">Others</option>
	            </select>
	        </td>
	        ${Array(supplier_count).join(0).split(0).map((item, i) => `
		    <td colspan="2">
                <select name="other_docs_${other_docs_counter}_supplier${i}" id="other_docs_${other_docs_counter}_supplier${i}" required class="compliance-select compliance_supplier${i} form-control form-control-sm border border-info">
                    <option value="" disabled selected>Select One</option>
                    <option value="Comply">Comply</option>
                    <option value="Not Comply">Not Comply</option>
                </select>
            </td>
		  	`).join('')}
	    </tr>
	`);

	$('.add-other-docs').closest('tr').after(other_docs_row);

	document.getElementById("eli_req_title").rowSpan = eli_title_rowspan + 1;
}