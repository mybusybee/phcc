import { rankSuppliers } from '../abstracts/total-cost-calculator'

$(function(){
	// let pr_item_unit_costs = document.getElementsByClassName("pr-item-unit-cost");
	// alert(pr_item_unit_costs.length);

	$('#openingDate').datepicker({
		dateFormat:'MM d, yy',
		minDate: 0,
    });
    $('#aobProcurementType').on('change', function(){
		let pb_type = $(this).val();
		
		$('#proc_mode_type').val(pb_type);

		let bid_type = $('#aobType').val();

		sentenceChanger(bid_type, pb_type);
	})

	$('#aobType').on('change', function(){
		let bid_type = $(this).val();
		let pb_type = $('#aobProcurementType').val();

		sentenceChanger(bid_type, pb_type);

		if (bid_type === 'As Read'){
			let obs_header = document.getElementById('obs-header');
			let obs_content = document.getElementById('obs-content');
			obs_header.classList.remove('d-none');
			obs_content.classList.remove('d-none');

			let twg_header = document.getElementById('twg-header');
			let twg_content = document.getElementById('twg-content');
			twg_header.classList.add("d-none");
			twg_content.classList.add("d-none");

		}else if(bid_type === 'As Calculated'){
			let obs_header = document.getElementById('obs-header');
			let obs_content = document.getElementById('obs-content');
			obs_header.classList.add('d-none');
			obs_content.classList.add('d-none');

			let twg_header = document.getElementById('twg-header');
			let twg_content = document.getElementById('twg-content');
			twg_header.classList.remove("d-none");
			twg_content.classList.remove("d-none");
		}
	})
})

function sentenceChanger(bid_type, pb_type){
	if (bid_type === 'As Read'){
		if (pb_type === "1" || pb_type === "2")
			$('#bidder_grammar').text('Single/Lowest Calculated Bidder');
		else if (pb_type === "3")
			$('#bidder_grammar').text('Single/Highest Rated Bidder');
	}else if (bid_type === 'As Calculated'){
		if (pb_type === "1" || pb_type === "2")
			$('#bidder_grammar').text('Single/Lowest Calculated and Responsive Bidder');
		else if (pb_type === "3")
			$('#bidder_grammar').text('Single/Highest Rated and Responsive Bidder');
	}
}

//this will dynamically add a text input below the dropdown and remove it on change
$(document).on('change', '.document-select', function(){
	let document_type = $(this).val();
	let doc_container_id = $(this).closest('td').attr('id');
	let doc_container_id_tokens = doc_container_id.split("_");
	let doc_container_row_no = doc_container_id_tokens[doc_container_id_tokens.length - 1];

	let extra_document_values = ['a9', 'b9', 'c9', 'd9', 'e9'];

	if (extra_document_values.indexOf(document_type) != -1){
		let other_doc_input = "";

		if (document_type == 'a9'){
			other_doc_input = $(`<input type="text" name="legal_docs_extra_${doc_container_row_no}" id="additional_doc" class="additional_doc form-control form-control-sm border border-info mt-2" style="width:95%; float:right">`);
		}else if (document_type == 'b9'){
			other_doc_input = $(`<input type="text" name="technical_docs_extra_${doc_container_row_no}" id="additional_doc" class="additional_doc form-control form-control-sm border border-info mt-2" style="width:95%; float:right">`);
		}else if (document_type == 'c9'){
			other_doc_input = $(`<input type="text" name="financial_docs_extra_${doc_container_row_no}" id="additional_doc" class="additional_doc form-control form-control-sm border border-info mt-2" style="width:95%; float:right">`);
		}else if (document_type == 'd9'){
			other_doc_input = $(`<input type="text" name="classb_docs_extra_${doc_container_row_no}" id="additional_doc" class="additional_doc form-control form-control-sm border border-info mt-2" style="width:95%; float:right">`);
		}else if (document_type == 'e9'){
			other_doc_input = $(`<input type="text" name="other_docs_extra_${doc_container_row_no}" id="additional_doc" class="additional_doc form-control form-control-sm border border-info mt-2" style="width:95%; float:right">`);
		}

		other_doc_input.appendTo('#' + doc_container_id);
	}else{
		$(this).next('.additional_doc').remove();
	}

	let supplier_count = $("input[name='supplierList[]']").length;
	if (document_type == 'd2'){
		for (let i = 0; i < supplier_count; i++) {
			$('#classb_docs_'+doc_container_row_no+'_supplier'+i).val("N/A");
		}
	}
})

$(document).on('change', '.compliance-select', function(){

	let compliance_cell_id = $(this).attr('id');
	let compliance_cell_id_tokens = compliance_cell_id.split("_");
	let compliance_suffix = compliance_cell_id_tokens[compliance_cell_id_tokens.length - 1];

	let supplier_compliance_array = document.getElementsByClassName("compliance_"+compliance_suffix);

	let compliance = true;
	for (let sup_ctr = 0; sup_ctr < supplier_compliance_array.length; sup_ctr++){
		let supplier_compliance = supplier_compliance_array[sup_ctr].value;
		if (supplier_compliance == "Not Comply"){
			compliance = false;
		}
	}
	
	let compliance_suffix_tokens = compliance_suffix.split('supplier');
	let supplier_index = compliance_suffix_tokens[1];
	let pr_item_unit_costs = document.getElementsByClassName("pr-item-unit-cost" + supplier_index);
	let pr_sub_item_unit_costs = document.getElementsByClassName("pr-sub-item-unit-cost" + supplier_index);

	if (compliance){
		$('#result_compliance_' + compliance_suffix).val('PASSED');
		// debugger

		let pr_item_total_costs = document.getElementsByClassName("pr-item-total-cost" + supplier_index);
		for(let pr_total_ctr = 0; pr_total_ctr < pr_item_total_costs.length; pr_total_ctr++){
			if (pr_item_total_costs[pr_total_ctr].value == "0.00" || pr_item_total_costs[pr_total_ctr].value == "n/a")
				pr_item_total_costs[pr_total_ctr].value = "0.00";
			else
				break;
		}

		for(let pr_item_ctr = 0; pr_item_ctr < pr_item_unit_costs.length; pr_item_ctr++){

			if (pr_item_unit_costs[pr_item_ctr].value == "n/a"){
				pr_item_unit_costs[pr_item_ctr].value = "";
				pr_item_unit_costs[pr_item_ctr].readOnly = false;	
			}
			
		}

		let pr_sub_item_total_costs = document.getElementsByClassName("pr-sub-item-total-cost" + supplier_index);
		for(let pr_sub_total_ctr = 0; pr_sub_total_ctr < pr_sub_item_total_costs.length; pr_sub_total_ctr++){
			if (pr_sub_item_total_costs[pr_sub_total_ctr].value == "0.00" || pr_sub_item_total_costs[pr_sub_total_ctr].value == "n/a")
				pr_sub_item_total_costs[pr_sub_total_ctr].value = "0.00";
			else
				break;
		}

		for(let pr_sub_item_ctr = 0; pr_sub_item_ctr < pr_sub_item_unit_costs.length; pr_sub_item_ctr++){

			if (pr_sub_item_unit_costs[pr_sub_item_ctr].value == "n/a"){
				pr_sub_item_unit_costs[pr_sub_item_ctr].value = "";
				pr_sub_item_unit_costs[pr_sub_item_ctr].readOnly = false;	
			}
		}

		$('#supplier_ranking'+supplier_index).val("-");

	}else{
		$('#result_compliance_' + compliance_suffix).val('FAILED');

		let pr_item_total_costs = document.getElementsByClassName("pr-item-total-cost" + supplier_index);
		for(let pr_total_ctr = 0; pr_total_ctr < pr_item_total_costs.length; pr_total_ctr++){
			pr_item_total_costs[pr_total_ctr].value = "n/a";		
		}

		for(let pr_item_ctr = 0; pr_item_ctr < pr_item_unit_costs.length; pr_item_ctr++){

			pr_item_unit_costs[pr_item_ctr].value = "n/a";
			pr_item_unit_costs[pr_item_ctr].readOnly = true;
		}

		$('.total_cost_total_supplier'+supplier_index).text('0.00');

		$('#supplier_ranking'+supplier_index).val("N/A");
		
	}

	rankSuppliers();
})

function updateCompliantSupplier(){
	let current_suppliers = $('select#reco_supplier option').clone();

	let updated_supplier_select = $('select#reco_supplier_updated option').remove();
	
	for(let ctr = 0; ctr < current_suppliers.length; ctr++){
		let sup_val = current_suppliers[ctr].value;

		let sup_compliance = $('.result_compliance_supplier_id_'+sup_val).val();
		let ranking = $('#supplier_ranking'+ctr).val();

		if ((sup_compliance === 'PASSED') && (ranking !== 'Bid Over ABC' && ranking !== 'N/A')){
			$('select#reco_supplier_updated').append(current_suppliers[ctr]);
		}
	}
}