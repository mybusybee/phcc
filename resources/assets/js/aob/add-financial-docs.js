let financial_docs_counter
$(function(){
	financial_docs_counter = $('tr.financial_docs').length - 1;
})
export const addFinancialDocs = supplierCount => {
	let supplier_count = parseInt(supplierCount);
	let eli_title_rowspan = document.getElementById("eli_req_title").rowSpan;
	
	financial_docs_counter++;

	let fin_docs_row = $(`
		<tr>
			<td colspan="3" class="financial_docs_cell" id="financial_docs_cell_${financial_docs_counter}">
				<input type="hidden" name="financial_docs_count[]" id="legal_docs_count">
				<button type="button" class="remove-doc-row btn btn-danger btn-xs" style="float: left;">-</button>
	            <select name="financial_docs_${financial_docs_counter}" id="" required class="document-select form-control form-control-sm border border-info" style="width: 95%; float: right;">
	                <option value="" disabled selected>Select One</option>
	                <option value="c1">Audited Financial Statement (AFS)</option>
	                <option value="c2">Net Financial Contracting Capacity (NFCC) / Committed Line of Credit</option>
	                <option value="c9">Others</option>
	            </select>
	        </td>
	        ${Array(supplier_count).join(0).split(0).map((item, i) => `
		    <td colspan="2">
                <select name="financial_docs_${financial_docs_counter}_supplier${i}" id="financial_docs_${financial_docs_counter}_supplier${i}" required class="compliance-select compliance_supplier${i} form-control form-control-sm border border-info">
                    <option value="" disabled selected>Select One</option>
                    <option value="Comply">Comply</option>
                    <option value="Not Comply">Not Comply</option>
                </select>
            </td>
		  	`).join('')}
	    </tr>
	`);

	$('.add-finan-docs').closest('tr').after(fin_docs_row);

	document.getElementById("eli_req_title").rowSpan = eli_title_rowspan + 1;
}