let technical_docs_counter
$(function(){
	technical_docs_counter = $('tr.tech_docs').length - 1;
})
export const addTechDocs = supplierCount => {
	let supplier_count = parseInt(supplierCount);
	let eli_title_rowspan = document.getElementById("eli_req_title").rowSpan;
	
	technical_docs_counter++;

	let tech_docs_row = $(`
		<tr>
			<td colspan="3" class="technical_docs_cell" id="technical_docs_cell_${technical_docs_counter}">
				<input type="hidden" name="tech_docs_count[]" id="legal_docs_count">
				<button type="button" class="remove-doc-row btn btn-danger btn-xs" style="float: left;">-</button>
	            <select name="technical_docs_${technical_docs_counter}" id="" required class="document-select form-control form-control-sm border border-info" style="width: 95%; float: right;">
	                <option value="" disabled selected>Select One</option>
	                <option value="b1">Statement of all Ongoing Government and Private Contracts, including awarded but not yet started</option>
	                <option value="b2">Statement of Single Largest Completed Contract (SLCC) similar to the contract to be bid</option>
	                <option value="b3">PBAC License (Infra.)</option>
	                <option value="b9">Others</option>
	            </select>
	        </td>
	        ${Array(supplier_count).join(0).split(0).map((item, i) => `
		    <td colspan="2">
                <select name="technical_docs_${technical_docs_counter}_supplier${i}" id="technical_docs_${technical_docs_counter}_supplier${i}" required class="compliance-select compliance_supplier${i} form-control form-control-sm border border-info">
                    <option value="" disabled selected>Select One</option>
                    <option value="Comply">Comply</option>
                    <option value="Not Comply">Not Comply</option>
                </select>
            </td>
		  	`).join('')}
	    </tr>
	`);

	$('.add-technical-docs').closest('tr').after(tech_docs_row);

	document.getElementById("eli_req_title").rowSpan = eli_title_rowspan + 1;
}