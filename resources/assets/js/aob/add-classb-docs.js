let classb_docs_counter
$(function(){
	classb_docs_counter = $('tr.classb_docs').length - 1;
})

export const addClassBDocs = supplierCount => {
	let supplier_count = parseInt(supplierCount);
	let eli_title_rowspan = document.getElementById("eli_req_title").rowSpan;
	
	classb_docs_counter++;

	let classb_docs_row = $(`
		<tr>
			<td colspan="3" class="classsb_docs_cell" id="classb_docs_cell_${classb_docs_counter}">
				<input type="hidden" name="classb_docs_count[]" id="legal_docs_count" value="0">
				<button type="button" class="remove-doc-row btn btn-danger btn-xs" style="float: left;">-</button>
	            <select name="classb_docs_${classb_docs_counter}" id="" required class="document-select form-control form-control-sm border border-info" style="width: 95%; float: right;">
	                <option value="d0" disabled selected>Select One</option>
	                <option value="d1">Valid Joint Venture Agreement (JVA)</option>
	                <option value="d2">N/A</option>
	                <option value="d9">Others</option>
	            </select>
	        </td>
	        ${Array(supplier_count).join(0).split(0).map((item, i) => `
		    <td colspan="2">
                <select name="classb_docs_${classb_docs_counter}_supplier${i}" id="classb_docs_${classb_docs_counter}_supplier${i}" required class="compliance-select compliance_supplier${i} form-control form-control-sm border border-info">
                    <option value="" disabled selected>Select One</option>
                    <option value="Comply">Comply</option>
                    <option value="Not Comply">Not Comply</option>
                    <option value="N/A">N/A</option>
                </select>
            </td>
		  	`).join('')}
	    </tr>
	`);

	$('.add-classb-docs').closest('tr').after(classb_docs_row);

	document.getElementById("eli_req_title").rowSpan = eli_title_rowspan + 1;
}