let legal_docs_counter
$(function(){
	legal_docs_counter = $('tr.legal_docs').length-1;
})

export const addLegalDocs = (supplierCount, className) => {
	let supplier_count = parseInt(supplierCount);

	let eli_title_rowspan = document.getElementById("eli_req_title").rowSpan;
	legal_docs_counter++;

	let legal_docs_row = $(`
		<tr>
			<td colspan="3" class="legal_docs_cell" id="legal_docs_cell_${legal_docs_counter}">
				<input type="hidden" name="legal_docs_count[]" id="legal_docs_count">
				<button type="button" class="remove-doc-row btn btn-danger btn-xs" style="float: left;">-</button>
				<select name="legal_docs_${legal_docs_counter}" id="" required class="document-select form-control form-control-sm border border-info" style="width: 95%; float: right;">
					<option value="" disabled selected>Select One</option>
					<option value="a1">SEC Registration/DTI Registration/CDA</option>
					<option value="a2">Mayor's/Business Permit</option>
					<option value="a3">Tax Clearance</option>
					<option value="a4">PhilGEPS Certificate of Registration & Membership (Platinum)</option>
					<option value="a9">Others</option>
				</select>
			</td>
			${Array(supplier_count).join(0).split(0).map((item, i) => `
			<td colspan="2">
				<select name="legal_docs_${legal_docs_counter}_supplier${i}" id="legal_docs_${legal_docs_counter}_supplier${i}" required class="compliance-select compliance_supplier${i} form-control form-control-sm border border-info">
					<option value="" disabled selected>Select One</option>
					<option value="Comply">Comply</option>
					<option value="Not Comply">Not Comply</option>
				</select>
			</td>
			`).join('')}
		</tr>
	`);

	$(`.add-legal-docs`).closest('tr').after(legal_docs_row);

	document.getElementById("eli_req_title").rowSpan = eli_title_rowspan + 1;
}