import Vue from 'vue'
import store from '../vuex-store'
import { mapActions, mapGetters } from 'vuex';
import axios from 'axios'
import { addLegalDocs } from './add-legal-docs'
import { addClassBDocs } from './add-classb-docs'
import { addFinancialDocs } from './add-financial-docs'
import { addOtherDocs } from './add-other-docs'
import { addTechDocs } from './add-technical-docs'
import { addEligibilityDocs } from '../abstracts/add-eligibility-docs'
import { initiators } from './initiators'
import { baseURL } from '../auth/baseURL'

axios.defaults.baseURL = baseURL()

let app = new Vue({
    store,
    data() {
        return {
            supplier: {
                company_name: null,
                authorized_personnel: null,
                address: null,
                telephone: null,
                email: null,
                tin_number: null,
                category: null,
                mayors_permit: null,
                philgeps: null
            },
            submitButton: {
                classNames: 'btn btn-success',
                value: 'Save',
            },
        }
    },
    methods: {
        ...mapActions([
            'reloadSuppliers', 'reloadUninvitedSuppliers', 'pushNewUninvitedSupplier', 'removeSupplierFromArray'
        ]),
        submitNewSupplier() {
            this.submitButtonToggle('Saving...', 'btn btn-warning')
            let formData = new FormData()
            formData.append('company_name', this.supplier.company_name)
            formData.append('authorized_personnel', this.supplier.authorized_personnel)
            formData.append('address', this.supplier.address)
            formData.append('telephone', this.supplier.telephone)
            formData.append('email', this.supplier.email)
            formData.append('tin_number', this.supplier.tin_number)
            formData.append('category', this.supplier.category)
            formData.append('mayors_permit', this.supplier.mayors_permit)
            formData.append('philgeps', this.supplier.philgeps)
            axios.post(`/api/new-supplier/invite/`, formData)
                .then(res => {
                    this.pushNewUninvitedSupplier(res.data)
                    this.resetFields()
                    this.submitButtonToggle('Saved!', 'btn btn-info')
                    setTimeout(() => {
                        $('#newSupplierModal').modal('toggle');
                        this.submitButtonToggle('Save', 'btn btn-success')
                        setTimeout(() => {
                            this.scrollToRight()
                        }, 1000)
                    },800)
                })
                .catch(err => console.log(err))
        },
        addLegalBtn(event){
            addLegalDocs(this.getSuppliersCount, event.srcElement.className)
        },
        addTechDocsBtn() {
            addTechDocs(this.getSuppliersCount)
        },
        addFinancialDocsBtn() {
            addFinancialDocs(this.getSuppliersCount)
        },
        addClassBDocsBtn() {
            addClassBDocs(this.getSuppliersCount)
        },
        addOthersBtn() {
            addOtherDocs(this.getSuppliersCount)
        },
        addEligibilityBtn(){
            addEligibilityDocs(this.getSuppliersCount)
        },
        resetFields(){
            this.supplier = {
                companyName: null,
                authorizedPersonnel: null,
                address: null,
                telephone: null,
                email: null,
                tin: null,
                category: null,
                mayorsPermit: null,
                philgeps: null
            }
        },
        scrollToRight() {
            let leftPos = $('.aob-table').scrollLeft();
            $(".aob-table").animate({scrollLeft: leftPos + 1000}, 1500);
        },
        submitButtonToggle(value, classNames) {
            this.submitButton.value = value
            this.submitButton.classNames = classNames
        },
        addUninvitedSupplier(supplier, event) {
            event.srcElement.textContent = 'Added...'
            event.srcElement.className = 'btn btn-warning'
            event.srcElement.disabled = true
            this.pushNewUninvitedSupplier(supplier)
            this.scrollToRight()
        },
        removeSupplier(index, supplier_id) {
            this.removeSupplierFromArray(index)
            console.log(supplier_id)
            $(`.compliance_supplier${supplier_id}`).parent().remove()
        }
    },
    computed: {
        ...mapGetters([
            'getSuppliers', 'getSuppliersCount', 'getUninvitedSuppliers'
        ])
    },
    mounted() {
        let payload = {
            inviteID: this.$refs.inviteID.value,
            formType: 'create'
        }
        this.reloadSuppliers(payload)
        this.reloadUninvitedSuppliers(payload)
        initiators()
    }
}).$mount('#aobForm')