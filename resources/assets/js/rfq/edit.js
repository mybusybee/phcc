$(function(){
    $('#suppliers-datatable').DataTable({
        colReorder: true,
        responsive: true,
        "lengthMenu": [ [ 5, 10, 20, 50, -1 ], [5, 10, 20, 50 , 'All'] ],
        'order': [],
        columnDefs: [{
            targets: [0],
            orderable: false
        }]
    });

    $('#submission_date_time').datetimepicker({
        format:'F d, Y h:i A',
        step: 30,
        formatTime: 'h:i A',
        minDate: 0,
        validateOnBlur: false,
    });
});