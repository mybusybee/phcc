import axios from 'axios'

export default {
    data() {
        return {
            newPassword: '',
            confirmNewPassword: '',
            email: '',
            formData: new FormData(),
            loading: false
        }
    },
    methods: {
        resetPasswordSubmit() {
            this.loading = true
            this.formData.append('email', this.email)
            this.formData.append('password', this.newPassword)
            this.formData.append('password_confirmation', this.confirmNewPassword)
            this.formData.append('token', this.$route.params.token)
            axios.post('password/reset', this.formData)
                .then(response => {
                    this.loading = false
                    this.$router.go('/dashboard')
                    console.log('Success')
                    console.log(response)
                })
                .catch(err => {
                    this.loading = false
                    console.log(err)
                    console.log('Error')
                })
        }
    }
}