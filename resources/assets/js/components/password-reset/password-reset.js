import axios from 'axios'
export default {
    data() {
        return {
            email: '',
            formData: new FormData(),
            loading: false,
            alert: {
                type: 'success',
                message: '',
                hasAlert: false
            }
        }
    },
    methods: {
        submitForm() {
            this.loading = true
            this.formData.append('email', this.email)
            axios.post('password/email', this.formData)
                .then(response => {
                    console.log(response)
                    this.loading = false
                    this.alert.type = "success"
                    this.alert.message = "A password reset link has been sent to your email!"
                    this.alert.hasAlert = true
                })
                .catch(err => {
                    console.log('something went wrong')
                    console.log(err)
                    this.loading = false
                    this.alert.type = "error"
                    this.alert.message = err
                    this.alert.hasAlert = true
                })
        }
    }
}