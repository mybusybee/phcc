import { baseURL } from '../../../auth/baseURL'
import axios from 'axios'
export default {
    data() {
        return {
            loginLoading: false,
            userCredentials: {
                email: '',
                password: '',
            },
            alert: {
                hasAlert: false,
                alertType: '',
                alertMsg: '',
            },
            passwordResetLink: `${baseURL()}password/reset`
        }
    },
    methods: {
        submit() {
            this.loginLoading = true;
            this.alert.hasAlert = true
            this.alert.alertMsg = 'Logging in...'
            this.alert.alertType = 'info'
            axios.post('/login', this.userCredentials)
                 .then(res => {
                     if(res.request.responseURL == `${ baseURL() }dashboard`){
                         //successful login
                         this.alert.hasAlert = true
                         this.alert.alertMsg = "Successful log in! Redirecting to dashboard"
                         this.alert.alertType = 'success'
                         setTimeout(() => {
                             window.location = res.request.responseURL;
                         },1500)
                     } else {
                         //unauthenticated
                         this.alert.hasAlert = true
                         this.alert.alertMsg = "Invalid username or password! or User has been Deactivated!"
                         this.alert.alertType = 'error'
                         this.resetFields()
                     }
                 })
                 .catch(err => {
                     console.log(err)
                 })
        },
        resetFields() {
            this.userCredentials.email = ''
            this.userCredentials.password = ''
            this.loginLoading = false;
        },
    }
}