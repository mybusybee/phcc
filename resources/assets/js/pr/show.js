$(document).on('change', '#proc_admin', function(){
    let value = $(this).val()

    if(value !== null){
        $('#assignProcurementUserBtn').attr('disabled', false)
    } else {
        $('#assignProcurementUserBtn').attr('disabled', true)
    }
})

$('.datetimepicker').datetimepicker({
    format:'F d, Y h:i A',
    formatTime: 'h:i A',
    step: 30,
    validateOnBlur: false,
});