
$(document).on('click', '.add_new_file', function(){
    let prFilesCounter = $('.pr-files').length

    if(prFilesCounter < 5){
        let newFileRow = $(`
            <div class="input-group">
                <input type="file" name="pr_requirements[]" class="form-control pr-files" accept=".pdf">
                <div class="input-group-append">
                    <button class="btn btn-danger remove_file_row" type="button"><i class="fa fa-trash"></i></button>
                </div>
            </div>
            `)

        newFileRow.appendTo('div#files_section')
    } else {
        alert('Attached files must not be more than 5!')
    }
})

$(document).on('click', '.remove_file_row', function(){
    $(this).closest('div.input-group').remove()
})