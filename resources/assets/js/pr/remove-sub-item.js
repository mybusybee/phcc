import { computeTotalBudgetOfAllItems } from './form-validation'
import { reCountSubItemNo } from './item-numbering'
$(document).on('click', '.remove-sub-item-row', function(){
    let subRow = $(this).parent().parent().data('sub-row')
    let parentRow = $(this).parent().parent().data('parent')

    let rowNumber = $(this).closest('tr').attr('id')
    let row = $(this).closest('tr')
    let subRowNumber = (($(this).closest('tr').find(`[data-parent="${rowNumber}"]`).length) / 5) - 1

    if(subRowNumber === 0){
        $(row).find(`select[name="unit_no_${rowNumber}"]`).attr('disabled', false)
        $(row).find(`input[name="quantity_${rowNumber}"]`).attr('readonly', false)
        $(row).find(`input[name="unit_cost_${rowNumber}"]`).attr('readonly', false)
        $(this).closest('tr').find('input.total_cost').val('')
    }

    $(this).closest('tr').find(`[data-parent="${parentRow}"][data-sub-row="${subRow}"]`).each(function(){
        $(this).closest('div.input-group').remove()
    })

    computeParentTotalCost(rowNumber)
    computeTotalBudgetOfAllItems()

    let subItemCount = $('div.sub-item').length
    if(subItemCount === 0){
        $('.add-pr-row').attr('disabled', false)
        $(`tr#${rowNumber}`).find(`input[name="quantity_${rowNumber}"]`).attr('readonly', false)
        $(`tr#${rowNumber}`).find(`input[name="unit_cost_${rowNumber}"]`).attr('readonly', false)
        $(`tr#${rowNumber}`).find(`input[name="total_cost_${rowNumber}"]`).val('')
    }

    reCountSubItemNo()
    
})

let computeParentTotalCost = rowNumber => {

    let totalBudget = $(`tr#${rowNumber}`).find('select.description').find(':selected').data('budget')

    if(~String(totalBudget).indexOf(',')) {
        totalBudget = parseFloat(String(totalBudget).replace(/,/g, ''))
    }

    let parentRowTotalCost = 0
    $(`input.subTotalCost[data-parent="${rowNumber}"]`).each(function(){
        parentRowTotalCost += parseFloat($(this).val().replace(/,/g, ''))
    })

    if(parentRowTotalCost > totalBudget) {
        $(`tr#${rowNumber}`).find('td.total-cost-container').addClass('form-has-error')
    } else {
        $(`tr#${rowNumber}`).find('td.total-cost-container').removeClass('form-has-error')
    }
}