//Sub Quantity
$(document).on('keyup', '.subQuantity', function(){
    let rowNumber = $(this).closest('tr').attr('id')
    let subRowNumber = $(this).data('sub-row')
    let value = $(this).val()
    let el = $(this)


    computeSubUnitQuantityTotal(el, rowNumber, subRowNumber)
})

//Unit Cost
$(document).on('keyup', '.subUnitCost', function(){
    
    let rowNumber = $(this).closest('tr').attr('id')
    let subRowNumber = $(this).data('sub-row')
    let value = $(this).val()
    let el = $(this)

    computeSubUnitQuantityTotal(el, rowNumber, subRowNumber)
})

let computeSubUnitQuantityTotal = (el, rowNumber, subRowNumber) => {
    let totalCost = 0
    let quantity = 0
    let unit = 0

    if($(el).hasClass('subQuantity')){
        quantity = $(el).val()
        unit = parseFloat($(el).closest('tr').find(`input.subUnitCost[data-parent="${rowNumber}"][data-sub-row="${subRowNumber}"]`).val().replace(/,/g, ''))
        totalCost = (parseFloat(quantity)) * parseFloat(unit)
    } else {
        unit = parseFloat($(el).val().replace(/,/g, ''));
        quantity = $(el).closest('tr').find(`input.subQuantity[data-parent="${rowNumber}"][data-sub-row="${subRowNumber}"]`).val()
        totalCost = quantity * unit
    }

    isNaN(totalCost) ? totalCost = 0 : totalCost

    $(el).closest('tr').find(`input.subTotalCost[data-parent="${rowNumber}"][data-sub-row="${subRowNumber}"]`).val(totalCost.toLocaleString())

    let parentRowTotalCost = computeParentTotalCost(rowNumber)

    checkIfBudgetExceeds($(el), rowNumber, parentRowTotalCost)

    computeTotalBudgetOfAllItems()
}

let checkIfBudgetExceeds = (el, rowNumber, parentRowTotalCost) => {

    let totalBudget = $(el).closest('tr').find(`select[name="description_${rowNumber}"]`).find(':selected').data('budget').toLocaleString().replace(/,/g, '')

    parentRowTotalCost = parseFloat(parentRowTotalCost)

    if(parentRowTotalCost > totalBudget) {
        $(el).closest('tr').find('td.total-cost-container').addClass('form-has-error')
    } else {
        $(el).closest('tr').find('td.total-cost-container').removeClass('form-has-error')
    }
}

let computeParentTotalCost = rowNumber => {
    let total = 0
    $(`input.subTotalCost[data-parent="${rowNumber}"]`).each(function(){
        total += parseFloat($(this).val().replace(/,/g, ''))
    })
    return total
}

export let computeTotalBudgetOfAllItems = () => {
    let totalEstimate = 0
    $('table#pr_table').find('tr.pr-item').each(function(){
        let rowNumber = $(this).attr('id')
        let subRowNumber = 0;
        $(this).find(`*[data-parent="${rowNumber}"][data-sub-row]`).each(function(){
            subRowNumber++
        })

        if(subRowNumber != 0){
            $(this).find(`input.subTotalCost[data-parent="${rowNumber}"]`).each(function(){
                totalEstimate += parseFloat($(this).val().replace(/,/g, ''))
            })
        } else {
            totalEstimate += parseFloat($(`input[name="total_cost_${rowNumber}"]`).val().replace(/,/g, ''))
        }
    })

    if(isNaN(totalEstimate)){
        totalEstimate = 0
    }

    $('#total_estimate').val(totalEstimate.toLocaleString())
}

/**
 * 
 *  ON CHANGE OF SELECTED DESCRIPTION, REMOVE QUANTITIES AND COSTS FOR EACH SUBITEM
 * 
 */

  //cant add sub item if description is null
$(document).on('change', 'select.description', function(){
    let val = $(this).val()
    if(val != "") {
        $(this).closest('tr').find('.add-sub-item').attr('disabled', false)
    }
})

//auto fill total budget field and remove readonly of quantity and unit cost of parent item
$(document).on('change', '.description', function(){
	let appId = $(this).find(':selected').data('app-id');
	$(this).closest('tr').find('.app_id').val(appId)

	let rowNumber = $(this).closest('tr').attr('id')

	let subRowNumber = (($(this).closest('tr').find(`[data-parent="${rowNumber}"]`).length) / 5)

	if(subRowNumber !== 0){
		let totalBudget = $(this).find(':selected').data('budget')
        $(this).closest('tr').find(`input[name="total_cost_${rowNumber}"]`).val(totalBudget)
    }
    $(this).closest('tr').find(`input[name="unit_cost_${rowNumber}"]`).attr('readonly', false)
    $(this).closest('tr').find(`input[name="quantity_${rowNumber}"]`).attr('readonly', false)
});

$(document).on('keyup', '.quantity:not([readonly]), .unit_cost:not([readonly])', function(){
    let el = $(this)
    let rowNumber = $(this).closest('tr').attr('id')
    let quantity = 0
    let unit = 0
    let total = 0

    if($(el).hasClass('quantity')){
        quantity = parseFloat($(this).val().replace(/,/g, ''))
        unit = parseFloat($(`tr#${rowNumber}`).find(`input[name="unit_cost_${rowNumber}"]`).val().replace(/,/g, ''))
    } else {
        unit = parseFloat($(this).val().replace(/,/g, ''))
        quantity = parseFloat($(`tr#${rowNumber}`).find(`input[name="quantity_${rowNumber}"]`).val().replace(/,/g, ''))
    }
    total = unit * quantity
    if(isNaN(total)){
        total = 0
    }

    $(`tr#${rowNumber}`).find(`input[name="total_cost_${rowNumber}"]`).val(total.toLocaleString())

    computeTotalBudgetOfAllItems()

    totalCostValidation(el, total)
})

let totalCostValidation = (el, total) => {
    let budgetAlloted = parseFloat($(el).closest('tr').find('select.description').find(':selected').data('budget').toLocaleString().replace(/,/g, ''))

    if(total > budgetAlloted) {
        $(el).closest('tr').find('.total-cost-container').addClass('form-has-error')
    } else {
        $(el).closest('tr').find('.total-cost-container').removeClass('form-has-error')
    }
}

$(document).on('keyup','.unit_cost, .quantity, .subQuantity, .subUnitCost', function(){
    let errorCount = $('table#pr_table tr.pr-item td.total-cost-container.form-has-error').length
    if(errorCount > 0){
        $('#prSubmit').attr('disabled', true)
    } else {
        $('#prSubmit').attr('disabled', false)
    }
})