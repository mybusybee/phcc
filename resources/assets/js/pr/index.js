import { baseURL } from '../auth/baseURL'
$(function(){
    $('#prTable').DataTable({
        "order": [[ 0, "desc" ]]
    });
});

$('td.consolidate-cb-container').on('click', function(){
    var checkbox = $(this).children('input.consolidate-cb');
    var id = $(checkbox).val();

    if($(checkbox).is(':checked')){
        //add hidden textbox with checkbox value
        var row = $(`
            <input type="text" class="form-control form-control-sm" value="`+id+`" name="consolidate[]">
        `);

        row.appendTo('div#pr_ids_container');
    } else {
        console.log('not checked');
        //remove
        $('div#pr_ids_container').find('input[value="'+id+'"]').remove();
    }

    $(checkbox).click();
});

$('input.consolidate-cb').click(function(e){
    e.stopPropagation();

    var checkbox = $(this);
    var id = $(this).val();

    if($(checkbox).is(':checked')){
        console.log('checked');
        //add hidden textbox with checkbox value
        var row = $(`
            <input type="hidden" class="form-control form-control-sm" value="`+id+`" name="consolidate[]">
        `);

        row.appendTo('div#pr_ids_container');
    } else {
        console.log('not checked');
        //remove
        $('div#pr_ids_container').find('input[value="'+id+'"]').remove();
    }
});

$('.formSelector').on('change', function(){
    let val = $(this).val()
    let id = $(this).closest('tr').find('.pr_id').val()
    if(val === 'rfp'){
        $(this).closest('tr').find('.generateFormBtn').attr('href', `${baseURL()}rfp/create/${id}`)
    } else if (val === 'rfq') {
        $(this).closest('tr').find('.generateFormBtn').attr('href', `${baseURL()}rfq/create/${id}`)
    } else if (val === 'rei') {
        $(this).closest('tr').find('.generateFormBtn').attr('href', `${baseURL()}rei/create/${id}`)
    } else if (val === 'itb') {
        $(this).closest('tr').find('.generateFormBtn').attr('href', `${baseURL()}itb/create/pr/${id}`)
    } else {
        $(this).closest('tr').find('.generateFormBtn').attr('href', `/`)
    }
})
