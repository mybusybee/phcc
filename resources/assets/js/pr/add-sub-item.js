import { computeTotalBudgetOfAllItems } from './form-validation'
//add subrow counter then +1 immediately then pass to element
$(document).on('click', '.add-sub-item', function(){
    $('.add-pr-row').attr('disabled', true)
    let rowNumber = $(this).closest('tr').attr('id')
    let row = $(this).closest('tr')

    let subRowNumber = (($(this).closest('tr').find(`[data-parent="${rowNumber}"]`).length) / 5) + 1

    let newItemNoSub = $(`
        <div class="input-group sub-item">
            <input class="form-control form-control-sm sub-item-no" readonly data-parent="${rowNumber}" data-sub-row="${subRowNumber}" name="itemNoSub${rowNumber}[]" value="${checkPurchaseType()}">
        </div>
    `)
    newItemNoSub.appendTo(`table#pr_table tbody tr#${rowNumber} td.item-no-container`)

    let newUnitSub = $(`
        <div class="input-group">
            <select required class="form-control form-control-sm" data-parent="${rowNumber}" data-sub-row="${subRowNumber}" name="unitSub${rowNumber}[]"></select>
        </div>
    `)
    newUnitSub.appendTo(`table#pr_table tbody tr#${rowNumber} td.unit-container`)
    $('select#all_units option').clone().appendTo(`select[data-parent=${rowNumber}][data-sub-row="${subRowNumber}"]`);

    let newDescriptionSub = $(`
        <div class="input-group" data-parent="${rowNumber}" data-sub-row="${subRowNumber}">
            <input type="text" required class="form-control form-control-sm" name="descriptionSub${rowNumber}[]">
            <div class="input-group-append">
                <button type="button" class="btn btn-danger btn-xs remove-sub-item-row">-</button>
            </div>
        </div>
    `)
    newDescriptionSub.appendTo(`table#pr_table tbody tr#${rowNumber} td.description-container`)

    let newQuantitySub = $(`
        <div class="input-group">
            <input required type="text" class="form-control form-control-sm subQuantity" data-parent="${rowNumber}" data-sub-row="${subRowNumber}" name="quantitySub${rowNumber}[]" value="0">
        </div>
    `)
    newQuantitySub.appendTo(`table#pr_table tbody tr#${rowNumber} td.quantity-container`)

    let newUnitCostSub = $(`
        <div class="input-group form-text-extend input-group-sm">
            <div class="input-group-prepend">
                <span class="input-group-text">&#8369;</span>
            </div>
            <input required type="text" class="form-control form-control-sm subUnitCost" data-parent="${rowNumber}" data-sub-row="${subRowNumber}" name="unitCostSub${rowNumber}[]" value="0">
        </div>
    `)
    newUnitCostSub.appendTo(`table#pr_table tbody tr#${rowNumber} td.unit-cost-container`)

    let newTotalCostSub = $(`
        <div class="input-group form-text-extend input-group-sm">
            <div class="input-group-prepend">
                <span class="input-group-text">&#8369;</span>
            </div>
            <input required readonly type="text" class="form-control form-control-sm subTotalCost" data-parent="${rowNumber}" data-sub-row="${subRowNumber}" name="totalCostSub${rowNumber}[]" value="0">
        </div>
    `)
    newTotalCostSub.appendTo(`table#pr_table tbody tr#${rowNumber} td.total-cost-container`)

    subRowNumber++

    if(subRowNumber !== 0){
        $(row).find(`select[name="unit_no_${rowNumber}"]`).attr('disabled', true)
        $(row).find(`input[name="quantity_${rowNumber}"]`).attr('readonly', true).val('')
        $(row).find(`input[name="unit_cost_${rowNumber}"]`).attr('readonly', true).val('')

        let totalBudget = $(row).find(`select[name="description_${rowNumber}"]`).find(':selected').data('budget')

        $(row).find(`input[name="total_cost_${rowNumber}"]`).attr('readonly', true).val(totalBudget)
    }

    computeTotalBudgetOfAllItems()
    
    $('.subUnitCost').mask('Z##,###,###,###,###.00', {
		reverse: true,
		translation: {
			'Z': {
				pattern: /[1-9]/
			}
		}
	});

	$('.subQuantity').mask('#0000000')
})

function checkPurchaseType(){
    let purchaseType = $('input[name="purchase_type"]:checked').val()
    if(purchaseType === 'lot'){
        return ''
    } else if (purchaseType === 'per') {
        let subItemCount = $('div.sub-item').length
        return subItemCount + 1
    } else {
        return ''
    }
}