$('input[name="purchase_type"]').on('change', function(){
    let purchaseType = $(this).val()
    applyNumbers(purchaseType)
})

export const applyNumbers = purchaseType => {
    if(purchaseType === 'lot') {
        let i = 1
        $('.parent_item_no').each(function(){
            $(this).val(i)
            i++
        })
        $('.sub-item-no').each(function(){
            $(this).val('')
        })
    } else if(purchaseType === 'per') {
        let subItemCount = $('.sub-item-no').length
        if(subItemCount > 0) {
            let i = 1
            $('.parent_item_no').each(function(){
                $(this).val('')
            })
            $('.sub-item-no').each(function(){
                $(this).val(i)
                i++
            })
        } else {
            let i = 1
            $('.parent_item_no').each(function(){
                $(this).val(i)
                i++
            })
            $('.sub-item-no').each(function(){
                $(this).val('')
            })
        }
    }
}

export const reCountSubItemNo = () => {
    let i = 1
    $('.sub-item-no').each(function(){
        $(this).val(i)
        i++
    })
}