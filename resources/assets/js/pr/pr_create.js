/**
 * 
 * Add new PR Row
 * 
 */
import { computeTotalBudgetOfAllItems } from './form-validation'
import { applyNumbers } from './item-numbering'

var row_counter = $('tr.pr-item').length;
var item_no_counter = parseInt($('#item_no_count').val());
var item_id;
$('table#pr_table').on('click', 'button.add-pr-row', function(){

	row_counter += 1;

	var row = $(`
		<tr id="${row_counter}" class="pr-item">
			<td class="d-none"><input type="hidden" class="app_id" name="pr_item_row_counter[]" value="x"></td>
			<td class="item-no-container">
				<input required type="text" readonly class="form-control form-control-sm item_no parent_item_no" name="item_no_`+row_counter+`" value="">
			</td>
			<td class="unit-container">
				<input required type="hidden" value="Ampule" class="form-control hidden_unit" name="unit_no_h_${row_counter}" />
				<select class="form-control form-control-sm unit_no" required name="unit_no_`+row_counter+`" id="unit_list" ></select>
			</td>
			<td class="description-container">
				<div class="input-group">
					<select required class="form-control form-control-sm description" name="description_`+row_counter+`" ></select>
					<div class="input-group-append">
						<button type="button" class="btn btn-primary btn-xs add-sub-item" disabled>+</button>
					</div>
				</div>
			</td>
			<td class="quantity-container">
				<input readonly type="text" class="form-control form-control-sm quantity" name="quantity_`+row_counter+`">
			</td>
			<td class="unit-cost-container">
				<div class="input-group form-text-extend input-group-sm">
					<div class="input-group-prepend">
						<span class="input-group-text">&#8369;</span>
					</div>
					<input readonly type="text" class="form-control form-control-sm unit_cost" name="unit_cost_`+row_counter+`">
				</div>
			</td>
			<td class="total-cost-container">
				<div class="input-group form-text-extend input-group-sm">
					<div class="input-group-prepend">
						<span class="input-group-text">&#8369;</span>
					</div>
					<input readonly type="text" class="form-control form-control-sm total_cost" name="total_cost_`+row_counter+`">
				</div>
			</td>
			<td>
				<button type="button" class="btn btn-danger btn-xs remove-pr-row">-</button>
			</td>
		</tr>
	`);

	row.appendTo('table#pr_table tbody');

	$('select#description_controller option').clone().appendTo('select[name="description_'+row_counter+'"]');
	$('select#all_units option').clone().appendTo(`select[name='unit_no_`+ row_counter +`'`);

	let purchaseType = $('input[name="purchase_type"]:checked').val();
	applyNumbers(purchaseType)

	$('.unit_cost').mask('Z##,###,###,###,###.00', {
		reverse: true,
		translation: {
			'Z': {
				pattern: /[1-9]/
			},
			'Y': {
				pattern: /[0-9]/
			}
		}
	});

	$('.quantity').mask('#0000000')
});

/**
 * 
 * Remove PR Row
 * 
 */

$('table#pr_table').on('click', 'button.remove-pr-row', function(){
	row_counter -= 1;
	

	$(this).closest('tr').nextAll().find(".item_no").each(function(){
		var name = $(this).attr('name');
		var i = name.substring(8).slice(0);
		i -= 1;
		$(this).attr('name', 'item_no_' + i);

		/** Item No. Value Recount **/
		var item_no = parseInt($(this).val());
		item_no -= 1;
		$(this).val(item_no);
	});

	$(this).closest('tr').nextAll().find(".unit_no").each(function(){
		var name = $(this).attr('name');
		var i = name.substring(8).slice(0);
		i -= 1;
		$(this).attr('name', 'unit_no_' + i);
	});

	$(this).closest('tr').nextAll().find(".hidden_unit").each(function(){
		var name = $(this).attr('name');
		var i = name.substring(10).slice(0);
		i -= 1;
		$(this).attr('name', 'unit_no_h_' + i);
	});

	$(this).closest('tr').nextAll().find(".description").each(function(){
		var name = $(this).attr('name');
		var i = name.substring(12).slice(0);
		i -= 1;
		$(this).attr('name', 'description_' + i);
	});

	$(this).closest('tr').nextAll().find(".quantity").each(function(){
		var name = $(this).attr('name');
		var i = name.substring(9).slice(0);
		i -= 1;
		$(this).attr('name', 'quantity_' + i);
	});

	$(this).closest('tr').nextAll().find(".unit_cost").each(function(){
		var name = $(this).attr('name');
		var i = name.substring(10).slice(0);
		i -= 1;
		$(this).attr('name', 'unit_cost_' + i);
	});

	$(this).closest('tr').nextAll().find(".total_cost").each(function(){
		var name = $(this).attr('name');
		var i = name.substring(11).slice(0);
		i -= 1;
		$(this).attr('name', 'total_cost_' + i);
	});

	$(this).closest('tr').nextAll().each(function(){
		let id = $(this).attr('id')
		let i = id.substring(0).slice(0);
		i -= 1
		$(this).attr('id', i)
	})


	$(this).closest('tr').remove();

	item_no_counter -= 1;
	$('#item_no_count').val(item_no_counter);
	computeTotalBudgetOfAllItems()

	if(row_counter === 0) {
		resetHiddenDescriptionSelect()
	}
	
});

let resetHiddenDescriptionSelect = () => {
    $('select.description').each(function() {
        $(this).find(`option`).each(function() {
            $(this).show()
        })
    })
}

/**
 * 
 * Disable keydown events on
 * several elements
 * 
 */
 $(document).on('keydown', '#pr_no, #office_name, #sai_no, #rc_code, #pr_date, #end_user, .item_no, .total_cost, #requested_by, input[name=cy], #mode_of_procurement', function(){
	 return false; 
 });

 /**
  * 
  * PR Number Generator
  * 
  */
$(document).ready(function(){
	var pr_counter = $('#pr_count').val();

	var month = String(new Date().getMonth() + 1);
	var year = String(new Date().getFullYear());
	month = month <= 9 ? "0" + month : month;
	var unique;
	
	if(pr_counter == 0 ){
		unique = '0001';
	} else {
		var new_pr_count = parseInt(pr_counter) + 1;
		if(new_pr_count.toString().length == 1) {
			unique = '000' + parseInt(new_pr_count);
		} else if(new_pr_count.toString().length == 2) {
			unique = '00' + parseInt(new_pr_count);
		} else if(new_pr_count.toString().length == 3) {
			unique = '0' + parseInt(new_pr_count);
		} else {
			unique = parseInt(new_pr_count);
		}
	}

	var pr_no = year + month + '-' +unique;
	$('#pr_no').val(pr_no);
});

 //allow numeric inputs only
$(document).on('keydown', '.quantity, .unit_cost', function(e){
	if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) || (e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true)) || (e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true)) || (e.keyCode >= 35 && e.keyCode <= 39)) {
		return;
	}
	if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
		e.preventDefault();
	}
});

//populate hidden unit field (for disabled select workaround)
$(document).on('change', 'select.unit_no', function(){
	let value = $(this).val()
	$(this).closest('tr').find('input.hidden_unit').val(value)
})

//for edit form
$(function(){
	let formType = $('#pr_form_type').val()
	if(formType === 'edit'){
		$('tr.pr-item').each(function(){
			let rowNumber = $(this).attr('id')
			let appId = $(this).find(`input[data-parent="${rowNumber}"]`).val()
			$(`select.description[data-parent="${rowNumber}"]`).find(`option[value="${appId}"]`).attr('selected', true)
		})
	}
})

// $(document).on('change', '.description', function(){
// 	let mode = $(this).find(':selected').data('procurement-mode')

// 	$('.description').each(function(){
// 		$(this).find('option').each(function(){
// 			let optionMode = $(this).data('procurement-mode')
// 			if(optionMode !== mode) {
// 				$(this).hide()
// 			} else {
// 				$(this).show()
// 			}
// 		})
// 	})
// })

$(document).on('change', 'select.description', function() {
	let allotmentType = $(this).find(':selected').data('allotment-type')
	$('select.description').find(`option`).each(function() {
		if($(this).data('allotment-type') !== allotmentType) {
			$(this).hide()
		}
	})
})

$(document).on('click change', function() {
	let parentItemCount = $('.pr-item').length
	if(parentItemCount > 1) {
		$('.add-sub-item').attr('disabled', true)
	} else {
		$('.add-sub-item').attr('disabled', false)
	}
});