$(function(){
	let year = new Date().getFullYear()
    $('#valid_from, #valid_to').datepicker({
		dateFormat: 'MM dd, yy',
		minDate: 0,
		maxDate: new Date(year, 11, 31)
	})

    for (i = new Date().getFullYear(); i <= new Date().getFullYear() + 10; i++)
	{
		$('#cy').append($('<option />').val(i).html(i));
	}

	let subItemCount = $('div.sub-item').length

	if(subItemCount !== 0) {
		$('.add-pr-row').attr('disabled', true)
	}
})

//auto compute 3 months on validity
// $(function() {
// 	$('#valid_from, #valid_to').on('change', function(){
// 		let id = $(this).attr('id')
// 		let date = $(this).val()

// 		if(id === 'valid_from') {
// 			let validTo = new Date(date)
// 			validTo.setMonth(validTo.getMonth() + 3)
// 			$('#valid_to').datepicker('setDate', validTo)
// 		} else {
// 			let validFrom = new Date(date)
// 			validFrom.setMonth(validFrom.getMonth() - 3)
// 			$('#valid_from').datepicker('setDate', validFrom)
// 		}
// 	})
// })