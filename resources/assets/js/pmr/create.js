import Vue from 'vue'
import store from './store'
import { mapActions, mapGetters } from 'vuex';
window.Event = new Vue()
new Vue({
	store,
	data() {
		return {
			tableText: 'No data available'
		}
	},
	methods: {
		...mapActions([
			'setItems', 'clearItems'
		]),
		getItems(event){
			this.clearItems()
			this.tableText = 'Loading...'
			this.setItems(event.srcElement.value)
		}
	},
	computed: {
		...mapGetters([
			'getCompleted', 'getOngoing'
		])
	},
	mounted() {
		Event.$on('setItemsComplete', () => {
			setTimeout(() => {
				$('.datepicker').datepicker({
					dateFormat:'MM d, yy',
				})
			},300)
		}),
		Event.$on('noData', () => {
			this.tableText = 'No available data for the selected year.'
		})
	},
	beforeDestroy(){
		Event.$off('setItemsComplete')
		Event.$off('noData')
	}
}).$mount('#pmr')

$(function(){
	for (let i = new Date().getFullYear(); i <= new Date().getFullYear() + 10; i++)
	{
		$('#year').append($('<option />').val(i).html(i));
	}
})