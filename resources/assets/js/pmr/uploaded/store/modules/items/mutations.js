export const setCompleted = (state, payload) => {
    state.completed = payload
}

export const setOngoing = (state, payload) => {
    state.ongoing = payload
}

export const setPMRDetails = (state, payload) => {
    state.pmr.pmrDetails = payload
}