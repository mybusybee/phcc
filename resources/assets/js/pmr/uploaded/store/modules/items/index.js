import * as getters from './getters'
import * as mutations from './mutations'
import * as actions from './actions'

export default {
    state: {
        completed: {},
        ongoing: {},
        pmr: {
            prepared_by: '',
            recommended_approval: '',
            approved_by: '',
            year: '',
            semester: '',
            pmrDetails: {
                completed: {
                    total_alloted_budget: 0,
                    total_contract_price: 0,
                    total_savings: 0,
                },
                ongoing: {
                    total_alloted_budget: 0,
                }
            }
        },
    },
    mutations, actions, getters
}