import axios from 'axios'

export const setCompleted = ({commit}, data) => {
    axios.get(`api/uploaded-pmrs/year/${data.year}/semester/${data.semester}`).then(res => {
        let completed_items = []
        let ongoing_items = []
        let pmr_data = {
            completed: {
                total_alloted_budget: 0,
                total_contract_price: 0,
                total_savings: 0,
            },
            ongoing: {
                total_alloted_budget: 0,
            }
        }
        res.data.data.forEach(pmr => {
            pmr.items.forEach(item => {
                if(item.completed === 1) {
                    completed_items.push(item)
                    pmr_data.completed.total_alloted_budget += parseFloat(item.abc_total)
                    pmr_data.completed.total_contract_price += parseFloat(item.contract_cost_total)
                    pmr_data.completed.total_savings += (parseFloat(item.abc_total) - parseFloat(item.contract_cost_total))
                } else {
                    ongoing_items.push(item)
                    pmr_data.ongoing.total_alloted_budget += item.abc_total
                }
            })
        })
        commit('setPMRDetails', pmr_data)
        commit('setCompleted', completed_items)
        commit('setOngoing', ongoing_items)
    })
    .catch(err => console.log(err.response.data))
}

