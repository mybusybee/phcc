import Vue from 'vue'
import store from './store'
import { mapGetters, mapActions } from 'vuex';

new Vue({
    store,
    data() {
        return {
            year: '',
            semester: '',
            tableText: 'No data available',
        }
    },
    methods: {
        ...mapActions([
            'setCompleted'
        ]),
        getItems() {
            if(this.year && this.semester !== '') {
                const data = {
                    year: this.year,
                    semester: this.semester
                }
                this.setCompleted(data)
            }
        },
    },
    computed: {
        ...mapGetters([
            'getCompleted', 'getOngoing', 'getPMR'
        ])
    }
}).$mount('#uploaded_pmr')

$(function(){
	for (let i = new Date().getFullYear(); i <= new Date().getFullYear() + 10; i++)
	{
		$('#year').append($('<option />').val(i).html(i));
	}
})