$(function(){
    $('.date').datepicker({
        dateFormat:'MM d, yy',
    })
})

$(document).on('change', '.pr-mooe', function(){

	let pr_mooe = parseFloat($(this).val())
	let pr_mooe_id = $(this).attr('id')
	let pr_mooe_id_tokens = pr_mooe_id.split('-')
	let row_num = pr_mooe_id_tokens[pr_mooe_id_tokens.length - 1]

	let total_estimate = parseFloat($('#total_estimate_'+row_num).text().replace(/,/g, ''))
	let pr_co = parseFloat($('#pr-co-'+row_num).val().replace(/,/g, ''))

	if(total_estimate == pr_mooe + pr_co){
		$('#ongoing-row-'+row_num).removeClass('with-error');
		$('input[type="submit"]').attr('disabled', false)
	}else{
		$('#ongoing-row-'+row_num).addClass('with-error');
		$('input[type="submit"]').attr('disabled', true)
	}
})

$(document).on('change', '.pr-co', function(){
	let pr_co = parseFloat($(this).val())
	let pr_co_id = $(this).attr('id')
	let pr_co_id_tokens = pr_co_id.split('-')
	let row_num = pr_co_id_tokens[pr_co_id_tokens.length - 1]

	let total_estimate = parseFloat($('#total_estimate_'+row_num).text().replace(/,/g, ''))
	let pr_mooe = parseFloat($('#pr-mooe-'+row_num).val().replace(/,/g, ''))

	if(total_estimate == pr_mooe + pr_co){
		$('#ongoing-row-'+row_num).removeClass('with-error');
		$('input[type="submit"]').attr('disabled', false)
	}else{
		$('#ongoing-row-'+row_num).addClass('with-error');
		$('input[type="submit"]').attr('disabled', true)
	}
})

$(document).on('change', '.contract-mooe', function(){
	let contract_mooe = parseFloat($(this).val())
	let contract_mooe_id = $(this).attr('id')
	let contract_mooe_id_tokens = contract_mooe_id.split('-')
	let row_num = contract_mooe_id_tokens[contract_mooe_id_tokens.length - 1]

	let contract_cost = parseFloat($('#contract_cost_'+row_num).text().replace(/,/g, ''))
	let contract_co = parseFloat($('#contract-co-'+row_num).val().replace(/,/g, ''))

	if(contract_cost == contract_mooe + contract_co){
		$('#ongoing-row-'+row_num).removeClass('with-error');
		$('input[type="submit"]').attr('disabled', false)
	}else{
		$('#ongoing-row-'+row_num).addClass('with-error');
		$('input[type="submit"]').attr('disabled', true)
	}
})

$(document).on('change', '.contract-co', function(){
	let contract_co = parseFloat($(this).val())
	let contract_co_id = $(this).attr('id')
	let contract_co_id_tokens = contract_co_id.split('-')
	let row_num = contract_co_id_tokens[contract_co_id_tokens.length - 1]

	let contract_cost = parseFloat($('#contract_cost_'+row_num).text().replace(/,/g, ''))
	let contract_mooe = parseFloat($('#contract-mooe-'+row_num).val().replace(/,/g, ''))

	if(contract_cost == contract_mooe + contract_co){
		$('#ongoing-row-'+row_num).removeClass('with-error');
		$('input[type="submit"]').attr('disabled', false)
	}else{
		$('#ongoing-row-'+row_num).addClass('with-error');
		$('input[type="submit"]').attr('disabled', true)
	}
})

/*********************FOR COMPLETED PRs*********************/

$(document).on('change', '.pr-com-mooe', function(){
	let pr_mooe = parseFloat($(this).val())
	let pr_mooe_id = $(this).attr('id')
	let pr_mooe_id_tokens = pr_mooe_id.split('-')
	let row_num = pr_mooe_id_tokens[pr_mooe_id_tokens.length - 1]

	let total_estimate = parseFloat($('#completed_total_estimate_'+row_num).text().replace(/,/g, ''))
	let pr_co = parseFloat($('#pr-com-co-'+row_num).val().replace(/,/g, ''))

	if(total_estimate == pr_mooe + pr_co){
		$('#completed-row-'+row_num).removeClass('with-error');
		$('input[type="submit"]').attr('disabled', false)
	}else{
		$('#completed-row-'+row_num).addClass('with-error');
		$('input[type="submit"]').attr('disabled', true)
	}
})

$(document).on('change', '.pr-com-co', function(){
	debugger
	let pr_co = parseFloat($(this).val())
	let pr_co_id = $(this).attr('id')
	let pr_co_id_tokens = pr_co_id.split('-')
	let row_num = pr_co_id_tokens[pr_co_id_tokens.length - 1]

	let total_estimate = parseFloat($('#completed_total_estimate_'+row_num).text().replace(/,/g, ''))
	let pr_mooe = parseFloat($('#pr-com-mooe-'+row_num).val().replace(/,/g, ''))

	if(total_estimate == pr_mooe + pr_co){
		$('#completed-row-'+row_num).removeClass('with-error');
		$('input[type="submit"]').attr('disabled', false)
	}else{
		$('#completed-row-'+row_num).addClass('with-error');
		$('input[type="submit"]').attr('disabled', true)
	}
})

$(document).on('change', '.com-contract-mooe', function(){
	let contract_mooe = parseFloat($(this).val())
	let contract_mooe_id = $(this).attr('id')
	let contract_mooe_id_tokens = contract_mooe_id.split('-')
	let row_num = contract_mooe_id_tokens[contract_mooe_id_tokens.length - 1]

	let contract_cost = parseFloat($('#completed_contract_cost_'+row_num).text().replace(/,/g, ''))
	let contract_co = parseFloat($('#com-contract-co-'+row_num).val().replace(/,/g, ''))

	if(contract_cost == contract_mooe + contract_co){
		$('#completed-row-'+row_num).removeClass('with-error');
		$('input[type="submit"]').attr('disabled', false)
	}else{
		$('#completed-row-'+row_num).addClass('with-error');
		$('input[type="submit"]').attr('disabled', true)
	}
})

$(document).on('change', '.com-contract-co', function(){
	let contract_co = parseFloat($(this).val())
	let contract_co_id = $(this).attr('id')
	let contract_co_id_tokens = contract_co_id.split('-')
	let row_num = contract_co_id_tokens[contract_co_id_tokens.length - 1]

	let contract_cost = parseFloat($('#completed_contract_cost_'+row_num).text().replace(/,/g, ''))
	let contract_mooe = parseFloat($('#com-contract-mooe-'+row_num).val().replace(/,/g, ''))

	if(contract_cost == contract_mooe + contract_co){
		$('#completed-row-'+row_num).removeClass('with-error');
		$('input[type="submit"]').attr('disabled', false)
	}else{
		$('#completed-row-'+row_num).addClass('with-error');
		$('input[type="submit"]').attr('disabled', true)
	}
})