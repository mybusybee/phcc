import Vue from 'vue'
import store from './store'
import { mapActions, mapGetters } from 'vuex';
import { baseURL } from '../auth/baseURL'
window.Event = new Vue()
new Vue({
	store,
	data() {
		return {
            tableText: 'No data available',
            semester: '',
			year: '',
			editLink: '#',
			exportable: false,
			exportLink: `#`,
		}
	},
	methods: {
		...mapActions([
			'setItems', 'clearItems'
		]),
		getItems(event){
            if(this.semester !== '' && this.year !== ''){
				this.editLink = `#`
				this.exportable = false
				this.exportLink = `#`
                this.clearItems()
                this.tableText = 'Loading...'
                let data = {
                    semester: this.semester,
                    year: this.year,
                    uid: this.$refs.uid.value
				}
                this.setItems(data)
            }
		},
	},
	computed: {
		...mapGetters([
			'getCompleted', 'getOngoing', 'getPMR'
		])
	},
	mounted() {
		Event.$on('setItemsComplete', () => {
			this.editLink = `${baseURL()}personal-pmr/edit/semester/${this.semester}/year/${this.year}`
			this.exportable = true
			this.exportLink = `${baseURL()}export/pmr/personal/${this.getPMR.id}`
		}),
		Event.$on('noData', () => {
			this.editLink = `#`
			this.exportable = false
			this.exportLink = `#`
		})
		Event.$on('noDataText', () => {
			this.tableText = 'No available data for the selected year and/or semester.'
		})
	},
	beforeDestroy(){
		Event.$off('setItemsComplete')
		Event.$off('noData')
		Event.$off('noDataText')
	}
}).$mount('#pmr')

$(function(){
	for (let i = new Date().getFullYear(); i <= new Date().getFullYear() + 10; i++)
	{
		$('#year').append($('<option />').val(i).html(i));
	}
})