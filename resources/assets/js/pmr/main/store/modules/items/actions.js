import axios from 'axios'

export const setItems = ({commit}, data) => {
    axios.get(`api/main-pmr/semester/${data.semester}/year/${data.year}`)
        .then(res => {
            commit('setPMRData', res.data.pmr)
            commit('setCompleted', res.data.completed.data)
            commit('setOngoing', res.data.ongoing.data)
            if(res.data.completed.length !== 0 || res.data.ongoing.length !== 0){
                Event.$emit('setItemsComplete')
                Event.$emit('noDataText')
            }
        })
        .catch(() => {
            Event.$emit('noData')
            Event.$emit('noDataText')
        })
}

export const clearItems = ({commit}) => {
    commit('clearItems')
}