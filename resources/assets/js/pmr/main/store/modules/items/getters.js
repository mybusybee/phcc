export const getCompleted = state => {
    return state.completed
}

export const getOngoing = state => {
    return state.ongoing
}

export const getPMR = state => {
    return state.pmr
}