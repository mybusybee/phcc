export const setCompleted = (state, payload) => {
    state.completed = payload
}

export const setOngoing = (state, payload) => {
    state.ongoing = payload
}

export const setPMRData = (state, payload) => {
    state.pmr = payload
}

export const clearItems = state => {
    state.completed = {}
    state.ongoing = {}
    state.pmr = null
}