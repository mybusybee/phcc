let supplier_id = 0;
let purchase_type = $('.purchase_type').val();

$(function(){
	$('.abstract-form').submit(function() {
	    $('select.reco_item_nos').each(function(){
			let options = $(this).find("option")

			$(options).each(function(){
				$(this).attr("disabled", false)
			})
		});	
	});
})

$(document).on('keyup', '.pr-item-unit-cost', function(){

	let supplier_unit_cost_str = $(this).val();
	let supplier_unit_cost = parseFloat(supplier_unit_cost_str);
	let supplier_unit_cost_id = $(this).attr('id');
	let supplier_unit_cost_id_tokens = supplier_unit_cost_id.split("_");
	let item_id = supplier_unit_cost_id_tokens[supplier_unit_cost_id_tokens.length - 1];
	let item_quantity = parseFloat($("#item_quantity_" + item_id).text());
	let supplier_total_cost = supplier_unit_cost * item_quantity;

	let supplier_id_tokens = supplier_unit_cost_id.split("unit_");
	let total_cost_id = "#total_"+supplier_id_tokens[1];

	if (supplier_unit_cost_str === 'n/a'){
		$(total_cost_id).val('n/a');
	}else if(supplier_unit_cost_str === 'no bid'){
		$(total_cost_id).val('no bid');
	}else if (supplier_unit_cost_str == ''){
		$(total_cost_id).val('0.00');
	}else{
		if (supplier_total_cost != null){
			$(total_cost_id).val(supplier_total_cost);
		}else{
			$(total_cost_id).val('0.00');
		}

		//make border red for a visual indicator
		if (purchase_type == "per item"){
			let purchase_total_cost = parseFloat($("#item_cost_total_" + item_id).text());

			if (supplier_total_cost > purchase_total_cost){
				$(total_cost_id).parent().addClass('with-error');
			}else{
				$(total_cost_id).parent().removeClass('with-error');
			}
		}
		
	}

	//pr-item-total-cost
	supplier_id = supplier_unit_cost_id_tokens[3];
})

$(document).on('keyup', '.pr-sub-item-unit-cost', function(){
	// debugger
	let supplier_unit_cost_str = $(this).val();
	let supplier_unit_cost = parseFloat(supplier_unit_cost_str);
	let supplier_unit_cost_id = $(this).attr('id');
	let supplier_unit_cost_id_tokens = supplier_unit_cost_id.split("_");
	let sub_item_id = supplier_unit_cost_id_tokens[supplier_unit_cost_id_tokens.length - 1];
	let item_quantity = parseFloat($("#sub_item_quantity_" + sub_item_id).text());
	let supplier_total_cost = supplier_unit_cost * item_quantity;

	let supplier_id_tokens = supplier_unit_cost_id.split("sub_unit_");
	let total_cost_id = "#sub_total_"+supplier_id_tokens[1];

	if (supplier_unit_cost_str === 'n/a'){
		$(total_cost_id).val('n/a');
	}else if(supplier_unit_cost_str === 'no bid'){
		$(total_cost_id).val('no bid');
	}else if (supplier_unit_cost_str == ''){
		$(total_cost_id).val('0.00');
	}else{
		if (supplier_total_cost != null){
			$(total_cost_id).val(supplier_total_cost);
		}else{
			$(total_cost_id).val('0.00');
		}

		//make border red for a visual indicator
		if (purchase_type == "per item"){
			let purchase_total_cost = parseFloat($("#sub_item_cost_total_" + sub_item_id).text().replace(/,/g, ''));
			if (supplier_total_cost > purchase_total_cost){
				$(total_cost_id).parent().addClass('with-error');
			}else{
				$(total_cost_id).parent().removeClass('with-error');
			}
		}
	}

	//pr-item-total-cost
	supplier_id = supplier_unit_cost_id_tokens[4];
})

$(document).on('change', '.pr-item-unit-cost', function(){
	if (purchase_type == 'per lot'){
		let item_ids = document.getElementsByClassName("item_ids");
		let amount_total = 0;
		for (let item_ctr = 0; item_ctr < item_ids.length; item_ctr++){
			let item_id = item_ids[item_ctr].innerHTML;

			let purchase_total_cost = parseFloat($("#item_cost_total_" + item_id).text());
			let item_total_cost_str = $("#total_cost_supplier_" + supplier_id + "_pr_item_" + item_id).val();

			if (item_total_cost_str !== 'n/a'){
				if (item_total_cost_str !== 'no bid'){
					let item_total_cost = parseFloat(item_total_cost_str);

					amount_total += item_total_cost;
				}else{
					amount_total = 'no bid';
					break;
				}
			}else{
				amount_total = 'n/a';
				break;
			}
			
		}
		$("#total_cost_total_supplier" + supplier_id).val(amount_total);
		rankSuppliers();
	}else if (purchase_type == 'per item'){
		let item_cell_id = $(this).attr('id');
		let item_cell_id_tokens = item_cell_id.split('_');
		let pr_id = item_cell_id_tokens[item_cell_id_tokens.length-1];
		//debugger
		rankSuppliers(pr_id);
	}
})

$(document).on('change', '.pr-sub-item-unit-cost', function(){
	if (purchase_type == 'per lot'){
		let sub_item_ids = document.getElementsByClassName("sub_item_ids");
		let amount_total = 0;
		for (let sub_item_ctr = 0; sub_item_ctr < sub_item_ids.length; sub_item_ctr++){
			let sub_item_id = sub_item_ids[sub_item_ctr].innerHTML;

			let purchase_total_cost = parseFloat($("#sub_item_cost_total_" + sub_item_id).text().replace(/,/g, ''));
			let sub_item_total_cost_str = $("#sub_total_cost_supplier_" + supplier_id + "_pr_sub_item_" + sub_item_id).val();

			if (sub_item_total_cost_str !== 'n/a'){
				if (sub_item_total_cost_str !== 'no bid'){
					let item_total_cost = parseFloat(sub_item_total_cost_str);

					amount_total += item_total_cost;
				}else{
					amount_total = 'no bid';
					break;
				}
			}else{
				amount_total = 'n/a';
				break;
			}
			
		}
		$("#total_cost_total_supplier" + supplier_id).val(amount_total);
		rankSuppliers();
	}else if (purchase_type == 'per item'){
		let item_cell_id = $(this).attr('id');
		let item_cell_id_tokens = item_cell_id.split('_');
		let pr_id = item_cell_id_tokens[item_cell_id_tokens.length-1];
		// debugger
		rankSuppliers(pr_id);
	}
})

export const rankSuppliers = (pr_id) => {
	let supplier_amount_arr = [];

	if (purchase_type == "per lot" && pr_id === undefined){
		
		let total_estimate = $('#total_estimate').text();
		total_estimate = parseFloat(total_estimate.slice(1, total_estimate.length).replace(/,/g, ''));
		
		let supplier_amounts = document.getElementsByClassName("total_cost_total_suppliers");
		
		for (let amt_ctr = 0; amt_ctr < supplier_amounts.length; amt_ctr++){
			let supplier_column_id = ".total_cost_total_supplier" + amt_ctr;
			// let amount_value_str = $(supplier_column_id).text();
			let amount_value_str = $(supplier_column_id).val();
			let amount_value = parseFloat(amount_value_str);

			if (amount_value_str != 'n/a'){
				if(amount_value_str != 'no bid'){
					if (!isNaN(amount_value)){
						if((amount_value < total_estimate && amount_value > 0) || amount_value == total_estimate){
							supplier_amount_arr.push({
								supplier_col_index: amt_ctr,
								amount: amount_value
							})	
						}else if (amount_value > total_estimate){
							$("#supplier_ranking" + amt_ctr).val("Bid Over ABC");
						}
					}
				}else{
					$("#supplier_ranking" + amt_ctr).val("NO BID");	
				}
			}else{
				$("#supplier_ranking" + amt_ctr).val("N/A");
			}
		}

		supplier_amount_arr.sort(function(a,b) {
		    return a.amount - b.amount;
		});
		 //debugger

		let final_ranking_arr = [];
		for (let sorted_ctr = 0; sorted_ctr < supplier_amount_arr.length; sorted_ctr++){
			if (sorted_ctr == 0){
				final_ranking_arr.push({
					rank: 1,
					supplier_col_index: supplier_amount_arr[sorted_ctr].supplier_col_index,
					amount: supplier_amount_arr[sorted_ctr].amount
				})
			}else{
				let base_entry = final_ranking_arr[sorted_ctr - 1];
				let base_amount = base_entry.amount;

				if (supplier_amount_arr[sorted_ctr].amount == base_amount){
					final_ranking_arr.push({
						rank: base_entry.rank,
						supplier_col_index: supplier_amount_arr[sorted_ctr].supplier_col_index,
						amount: supplier_amount_arr[sorted_ctr].amount
					})
				}else{
					final_ranking_arr.push({
						rank: base_entry.rank + 1,
						supplier_col_index: supplier_amount_arr[sorted_ctr].supplier_col_index,
						amount: supplier_amount_arr[sorted_ctr].amount
					})
				}
			}
		}

		for(let final_arr_ctr = 0; final_arr_ctr < final_ranking_arr.length; final_arr_ctr++){
			$("#supplier_ranking" + final_ranking_arr[final_arr_ctr].supplier_col_index).val(final_ranking_arr[final_arr_ctr].rank);
		}
	}else if(purchase_type == "per item" && pr_id !== undefined){
		let sub_item_count = $('#sub_item_count').val();
		if (sub_item_count == undefined)
			sub_item_count = "0"

		let total_estimate = $('#item_cost_total_'+pr_id).text();
		// debugger
		if (sub_item_count != "0")
			total_estimate = $('#sub_item_cost_total_'+pr_id).text();
		// total_estimate = parseFloat(total_estimate.slice(1, total_estimate.length).replace(/,/g, ''));
		total_estimate = parseFloat(total_estimate.replace(/,/g, ''));
		
		let row_total_costs = document.getElementsByClassName('pr-item_'+pr_id+'_total-cost');
		if (sub_item_count != "0")
			row_total_costs = document.getElementsByClassName('pr-sub-item_'+pr_id+'_total-cost');

		for(let item_ctr = 0; item_ctr < row_total_costs.length; item_ctr++){
			let total_supplier_cost = row_total_costs[item_ctr].value;
			let amount_value = parseFloat(total_supplier_cost);

			let total_supplier_cost_id = row_total_costs[item_ctr].id;
			let total_supplier_cost_tokens = total_supplier_cost_id.split('_');
			let item_supplier_id = total_supplier_cost_tokens[3];
			if (sub_item_count != "0")
				item_supplier_id = total_supplier_cost_tokens[4];
			
			if (total_supplier_cost != 'n/a'){
				if(total_supplier_cost != 'no bid'){
					if(total_supplier_cost != '0'){
						if (!isNaN(amount_value)){
							if((amount_value < total_estimate && amount_value > 0) || amount_value == total_estimate){
								supplier_amount_arr.push({
									supplier_col_index: item_ctr,
									supplier_id: item_supplier_id,
									amount: amount_value
								})	
							}else if (amount_value > total_estimate){
								$("#ranking_supplier_"+item_supplier_id+"_pr_item_"+pr_id+"_col_"+item_ctr).val("Bid Over ABC");
								if (sub_item_count != "0")
									$("#ranking_supplier_"+item_supplier_id+"_pr_sub_item_"+pr_id+"_col_"+item_ctr).val("Bid Over ABC");
							}
						}
					}else{
						$("#ranking_supplier_"+item_supplier_id+"_pr_item_"+pr_id+"_col_"+item_ctr).val("FREE");
						if (sub_item_count != "0")
							$("#ranking_supplier_"+item_supplier_id+"_pr_sub_item_"+pr_id+"_col_"+item_ctr).val("FREE");
					}
				}else{
					$("#ranking_supplier_"+item_supplier_id+"_pr_item_"+pr_id+"_col_"+item_ctr).val("NO BID");
					if (sub_item_count != "0")
						$("#ranking_supplier_"+item_supplier_id+"_pr_sub_item_"+pr_id+"_col_"+item_ctr).val("NO BID");
				}
			}else{
				$("#ranking_supplier_"+item_supplier_id+"_pr_item_"+pr_id+"_col_"+item_ctr).val("N/A");
				if (sub_item_count != "0")
					$("#ranking_supplier_"+item_supplier_id+"_pr_sub_item_"+pr_id+"_col_"+item_ctr).val("N/A");
			}
		}

		supplier_amount_arr.sort(function(a,b) {
		    return a.amount - b.amount;
		});

		let final_ranking_arr = [];
		for (let sorted_ctr = 0; sorted_ctr < supplier_amount_arr.length; sorted_ctr++){
			if (sorted_ctr == 0){
				final_ranking_arr.push({
					rank: 1,
					supplier_id: supplier_amount_arr[sorted_ctr].supplier_id,
					supplier_col_index: supplier_amount_arr[sorted_ctr].supplier_col_index,
					amount: supplier_amount_arr[sorted_ctr].amount
				})
			}else{
				let base_entry = final_ranking_arr[sorted_ctr - 1];
				let base_amount = base_entry.amount;

				if (supplier_amount_arr[sorted_ctr].amount == base_amount){
					final_ranking_arr.push({
						rank: base_entry.rank,
						supplier_id: supplier_amount_arr[sorted_ctr].supplier_id,
						supplier_col_index: supplier_amount_arr[sorted_ctr].supplier_col_index,
						amount: supplier_amount_arr[sorted_ctr].amount
					})
				}else{
					final_ranking_arr.push({
						rank: base_entry.rank + 1,
						supplier_id: supplier_amount_arr[sorted_ctr].supplier_id,
						supplier_col_index: supplier_amount_arr[sorted_ctr].supplier_col_index,
						amount: supplier_amount_arr[sorted_ctr].amount
					})
				}
			}
		}

		for(let final_arr_ctr = 0; final_arr_ctr < final_ranking_arr.length; final_arr_ctr++){
			$("#ranking_supplier_" + final_ranking_arr[final_arr_ctr].supplier_id +"_pr_item_" + pr_id + "_col_" + final_ranking_arr[final_arr_ctr].supplier_col_index).val(final_ranking_arr[final_arr_ctr].rank);
			if (sub_item_count != "0")
				$("#ranking_supplier_" + final_ranking_arr[final_arr_ctr].supplier_id +"_pr_sub_item_" + pr_id + "_col_" + final_ranking_arr[final_arr_ctr].supplier_col_index).val(final_ranking_arr[final_arr_ctr].rank);
		}

		// for (let sorted_ctr = 0; sorted_ctr < supplier_amount_arr.length; sorted_ctr++){
		// 	$("#ranking_supplier_" + supplier_amount_arr[sorted_ctr].supplier_id +"_pr_item_" + pr_id + "_col_" + supplier_amount_arr[sorted_ctr].supplier_col_index).val(sorted_ctr + 1);
		// 	if (sub_item_count != "0")
		// 		$("#ranking_supplier_"+supplier_amount_arr[sorted_ctr].supplier_id+"_pr_sub_item_"+pr_id+"_col_"+supplier_amount_arr[sorted_ctr].supplier_col_index).val(sorted_ctr + 1);
		// }
	}

	updateCompliantSupplier();
}

export const updateCompliantSupplier = () => {
	//get the based supplier options from vue state
	let current_suppliers = $('select#reco_supplier option').clone();

	//clear the new dropdown of old options
	let updated_supplier_select = $('select#reco_supplier_updated option').remove();

	//get the abstract type
	let abstract_type = $('#abstract_type').val()

	//loop through the based suppliers
	for(let ctr = 0; ctr < current_suppliers.length; ctr++){
		let sup_val = current_suppliers[ctr].value;
		let sup_compliance = $('.result_compliance_supplier_id_'+sup_val).val();

		if (purchase_type == "per lot"){
			let ranking = $('#supplier_ranking'+ctr).val();

			if (abstract_type == 'aob'){
				if ((sup_compliance === 'PASSED') && (ranking !== 'Bid Over ABC' && ranking !== 'N/A' && ranking !== 'NO BID')){
					$('select#reco_supplier_updated').append(current_suppliers[ctr]);
				}	
			}else if(abstract_type == 'aop'){
				//debugger
				let tech_proposal_res = $('#tech_proposal_res_'+ctr).val();
				if ((sup_compliance === 'COMPLIANT') && (ranking !== 'Bid Over ABC' && ranking !== 'N/A' && ranking !== 'NO BID') && (tech_proposal_res == "PASSED")){
					$('select#reco_supplier_updated').append(current_suppliers[ctr]);
				}
			}else{
				if ((sup_compliance === 'COMPLIANT') && (ranking !== 'Bid Over ABC' && ranking !== 'N/A' && ranking !== 'NO BID')){
					$('select#reco_supplier_updated').append(current_suppliers[ctr]);
				}
			}
		}else if(purchase_type == "per item"){
			// debugger
			if (abstract_type == 'aob'){
				if (sup_compliance === 'PASSED'){
					$('select#reco_supplier_updated').append(current_suppliers[ctr]);
				}	
			}else if(abstract_type == 'aop'){
				//debugger
				let tech_proposal_res = $('#tech_proposal_res_'+ctr).val();
				if (sup_compliance === 'COMPLIANT' && tech_proposal_res == "PASSED"){
					$('select#reco_supplier_updated').append(current_suppliers[ctr]);
				}
			}else{
				if (sup_compliance === 'COMPLIANT'){
					$('select#reco_supplier_updated').append(current_suppliers[ctr]);
				}
			}
		}
	}
}
