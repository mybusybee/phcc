import { baseURL } from '../auth/baseURL'
$(function(){
    $('#abstractsTable').DataTable({
        "aaSorting": [],
    })
})

$(document).on('change', '.formSelector', function(){
    let val = $(this).val()
    let button = $(this).closest('tr').find('.generateBtn')
    let id = $(this).closest('tr').attr('id')

    if(val === 'JO'){
        $(button).attr('href', `${baseURL()}jo/create/${id}`)
    } else if (val === 'PO') {
        $(button).attr('href', `${baseURL()}po/create/${id}`)
    } else if (val === 'contract') {
        $(button).attr('href', `${baseURL()}contract/create/${id}`)
    } else {
        $(button).attr('href', `/`)
    }
})