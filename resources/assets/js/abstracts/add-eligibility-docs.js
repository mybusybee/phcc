let eligibility_docs_counter
$(function(){
	eligibility_docs_counter = $('tr.eligibility_docs').length - 1;
})

export const addEligibilityDocs = (supplierCount) => {
	let supplier_count = parseInt(supplierCount);

	let eli_title_rowspan = document.getElementById("eli_req_title").rowSpan;
	eligibility_docs_counter++;

	let eligibility_docs_row = $(`
		<tr>
			<td colspan="3" class="eligibility_docs_cell" id="eligibility_docs_cell_${eligibility_docs_counter}">
				<input type="hidden" name="eligibility_docs_count[]" id="eligibility_docs_count">
				<button type="button" class="remove-doc-row btn btn-danger btn-xs" style="float: left;">-</button>
				<select name="eligibility_docs_${eligibility_docs_counter}" id="" required class="document-select form-control form-control-sm border border-info" style="width: 95%; float: right;">
					<option value="" disabled selected>Select One</option>
					<option value="f1">Mayor's/Business Permit</option>
					<option value="f2">BIR Certificate of Registration</option>
					<option value="f3">Professional License / Curriculum Vitae (Consulting Services)</option>
					<option value="f4">PhilGEPS Registration Number</option>
					<option value="f5">PCAB License(Infra.)</option>
					<option value="f6">NFCC(Infra.)</option>
					<option value="f7">Income / Business Tax Return</option>
					<option value="f8">Notarized Omnnibus Sworn Statement</option>
					<option value="f9">Others</option>
				</select>
			</td>
			${Array(supplier_count).join(0).split(0).map((item, i) => `
			<td colspan="2">
				<select name="eligibility_docs_${eligibility_docs_counter}_supplier${i}" id="legal_docs_${eligibility_docs_counter}_supplier${i}" required class="compliance-select compliance_supplier${i} form-control form-control-sm border border-info">
					<option value="" disabled selected>Select One</option>
					<option value="Comply">Comply</option>
					<option value="Not Comply">Not Comply</option>
				</select>
			</td>
			`).join('')}
		</tr>
	`);

	$(`.add-eligibility-docs`).closest('tr').after(eligibility_docs_row);

	document.getElementById("eli_req_title").rowSpan = eli_title_rowspan + 1;
}