let purchase_type = $('.purchase_type').val();
let selected_items_arr = [];

$(document).on('change', '.reco_suppliers', function(){
	// debugger
	let sub_item_count = $('#sub_item_count').val();
	if (sub_item_count == undefined)
			sub_item_count = "0"

	if (purchase_type == "per item"){
		let supplier_id = $(this).val()
		let supplier_elem_id = $(this).attr('id')
		let supplier_elem_id_tokens = supplier_elem_id.split("_")
		let row_no = supplier_elem_id_tokens[supplier_elem_id_tokens.length - 1]

		let supplier_count = $("input[name='supplierList[]']").length;

		let base_item_nos = $('select#reco_item_no option').clone();
		//debugger
		$('#reco_sub_total_'+row_no).val(0);
		$('#reco_total').val(0);

		let row_item_no_dropdown = $('select#reco_item_no_'+row_no+' option').remove();
		$('select#reco_item_no_'+row_no).append('<option value="0">Please select the items</option>');

		for(let supplier_ctr = 0; supplier_ctr < supplier_count; supplier_ctr++){
			
			for(let base_itm_ctr = 0; base_itm_ctr < base_item_nos.length; base_itm_ctr++){
				let item_id = base_item_nos[base_itm_ctr].value;
				let compliance_cell = $('#ranking_supplier_'+supplier_id+'_pr_item_'+item_id+'_col_'+supplier_ctr)
				if (sub_item_count != "0")
					compliance_cell = $('#ranking_supplier_'+supplier_id+'_pr_sub_item_'+item_id+'_col_'+supplier_ctr)

				if(compliance_cell.length > 0){
					let item_compliance = $('#ranking_supplier_'+supplier_id+'_pr_item_'+item_id+'_col_'+supplier_ctr).val();
					if (sub_item_count != "0")
						item_compliance = $('#ranking_supplier_'+supplier_id+'_pr_sub_item_'+item_id+'_col_'+supplier_ctr).val();
					
					if (item_compliance == "Bid Over ABC" || item_compliance == "N/A" || item_compliance == "-" || item_compliance == "NO BID"){
						
					}else{
						$('select#reco_item_no_'+row_no).append(base_item_nos[base_itm_ctr]);
					}	
				}
			}
		}
	}
})

$(document).on('change', '.reco_suppliers', function(){
	//kunin ung selected values
	//add sa array if wala
	//
	//iterate sa lahat ng reco_items para hanapin ung idisable

	let reco_row_count = $('.reco-row').length;
	if (reco_row_count > 1){
		let supplier_elem_id = $(this).attr('id')
		let supplier_elem_id_tokens = supplier_elem_id.split("_")
		let row_no = supplier_elem_id_tokens[supplier_elem_id_tokens.length - 1]

		$('select#reco_item_no_'+row_no).each(function(){
			for(let sel_ctr = 0; sel_ctr < selected_items_arr.length; sel_ctr++){
				$(this).find("option[value = "+selected_items_arr[sel_ctr]+"]").attr("disabled", true)	
			}
		});	
	}	
})

$(document).on('change', '.reco_item_nos', function(){
	selected_items_arr = [];

	$('select.reco_item_nos').each(function(){
		$(this).find(":selected").each(function(){
			let selected_id = $(this).val();
			if (selected_id != '0')
				selected_items_arr.push(selected_id)
		})	
	})

	selected_items_arr = selected_items_arr.filter((v, i, a) => a.indexOf(v) === i)

	let reco_row_count = $('.reco-row').length;
	if (reco_row_count > 1){
		$('select.reco_item_nos').each(function(){
			let options = $(this).find("option")
			// for(let opt_ctr = 0; opt_ctr < options.length; opt_ctr++){
			// 	options[opt_ctr].attr("disabled", false)	
			// }

			$(options).each(function(){
				$(this).attr("disabled", false)
				// debugger
			})
			
			for(let sel_ctr = 0; sel_ctr < selected_items_arr.length; sel_ctr++){
				// debugger
				$(this).find("option[value = "+selected_items_arr[sel_ctr]+"]").attr("disabled", true)	
			}
		});	
	}
})