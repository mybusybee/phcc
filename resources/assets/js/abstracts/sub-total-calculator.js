$(document).on('change', '.reco_item_nos', function(){
	let subtotal = 0;
	let sub_item_count = $('#sub_item_count').val();
	let purchase_type = $('#purchase_type').val();
	debugger
	if (sub_item_count == undefined){
		sub_item_count = 0;
	}

	let reco_item_nos = $(this).val();
	let reco_row_id = $(this).closest('.reco-row').attr('id');
	let reco_row_id_tokens = reco_row_id.split("_");
	let reco_row_no = reco_row_id_tokens[reco_row_id_tokens.length - 1];
	let supplier_id = $('#reco_supplier_'+reco_row_no).val();

	if (purchase_type == 'per lot'){
		subtotal = $('#total_cost_total_supplier'+supplier_id).val();
	}else{
		reco_item_nos.forEach(function(item, index){
			let supplier_total_cost = 0
			if(item != "0"){
				supplier_total_cost = parseFloat($('#total_cost_supplier_'+supplier_id+'_pr_item_'+item).val());
				if (sub_item_count != "0" || sub_item_count != 0){
					supplier_total_cost = parseFloat($('#sub_total_cost_supplier_'+supplier_id+'_pr_sub_item_'+item).val());
				}
			}
			subtotal += supplier_total_cost;
		})
	}

	$("#reco_sub_total_"+reco_row_no).val(subtotal);

	getRecoTotal();
})

$(document).on('change', '.reco_suppliers', function(){
	let subtotal = 0;
	let sub_item_count = $('#sub_item_count').val();
	let purchase_type = $('#purchase_type').val();
	// debugger
	if (sub_item_count == undefined){
		sub_item_count = 0;
	}

	let supplier_id = $(this).val();
	let reco_row_id = $(this).closest('.reco-row').attr('id');
	let reco_row_id_tokens = reco_row_id.split("_");
	let reco_row_no = reco_row_id_tokens[reco_row_id_tokens.length - 1];
	let reco_item_nos = $('#reco_item_no_'+reco_row_no).val();

	// reco_item_nos.forEach(function(item, index){
	// 	let supplier_total_cost = parseFloat($('#total_cost_supplier_'+supplier_id+'_pr_item_'+item).val());
	// 	subtotal += supplier_total_cost;
	// })

	if (purchase_type == 'per lot'){
		subtotal = $('#total_cost_total_supplier'+supplier_id).val();
	}else{
		reco_item_nos.forEach(function(item, index){
			let supplier_total_cost = 0;
			if (item != "0"){
				supplier_total_cost = parseFloat($('#total_cost_supplier_'+supplier_id+'_pr_item_'+item).val());
				if (sub_item_count != "0" || sub_item_count != 0){
					supplier_total_cost = parseFloat($('#sub_total_cost_supplier_'+supplier_id+'_pr_sub_item_'+item).val());
				}
			}
			subtotal += supplier_total_cost;
		})
	}

	$("#reco_sub_total_"+reco_row_no).val(subtotal);

	getRecoTotal();
})

function getRecoTotal(){
	let reco_total = 0.00;

	let reco_sub_totals = document.getElementsByClassName("reco_sub_totals");

	for (let i = 0; i < reco_sub_totals.length; i++){
		let reco_row_subtotal = parseFloat(reco_sub_totals[i].value);
		reco_total += reco_row_subtotal;
	}

	$("#reco_total").val(reco_total);
}