import Vue from 'vue'
import axios from 'axios'
import { baseURL } from '../auth/baseURL'
import store from './store'
import { mapActions, mapGetters } from 'vuex';

axios.defaults.baseURL = baseURL()

window.Event = new Vue()

new Vue({
    store,
    data() {
        return {
            editLink: '#',
            exportLink: '#'
        }
    },
    methods :{
        ...mapActions([
            'getAPPData'
        ]),
        getAPP(e){
            this.getAPPData(e.srcElement.value)
        }
    },
    computed: {
        ...mapGetters([
            'getItems', 'getTotals'
        ]),
    },
    mounted() {
        Event.$on('successGetData', year => {
            this.editLink = `${baseURL()}app/edit/${year}`
            this.exportLink = `${baseURL()}export/app/${year}`
        })
    },
    beforeDestroy(){
        Event.$off('successGetData')
    }
}).$mount('#appTable')

$(function(){
    for (let i = new Date().getFullYear() - 3; i <= new Date().getFullYear() + 10; i++)
	{
		$('.appYear').append($('<option />').val(i).html(i));
	}
})

$(function() {
	$('form').on('submit', function(){
        console.log('form submitted')
		$('button[type="submit"]').attr('disabled', true)
	})
})