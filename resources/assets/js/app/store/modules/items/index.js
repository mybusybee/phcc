import * as actions from './actions'
import * as getters from './getters'
import * as mutations from './mutations'

export default {
    state: {
        items: [],
        totals: {},
    },
    mutations, actions, getters
}