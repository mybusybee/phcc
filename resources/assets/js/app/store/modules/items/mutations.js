export const setAPPData = (state, payload) => {
    state.items = payload
    let total = {
        totalBudget: 0,
        totalmooe: 0,
        totalco: 0
    }
    for(let i = 0; i < payload.length; i++) {
        total.totalBudget += payload[i].total_estimated_budget ? parseFloat(payload[i].total_estimated_budget.replace(/,/g, '')) : 0
        total.totalmooe += payload[i].mooe ? parseFloat(payload[i].mooe.replace(/,/g, '')) : 0
        total.totalco += payload[i].co ? parseFloat(payload[i].co.replace(/,/g, '')) : 0
    }

    state.totals = total
}