import axios from 'axios'
import { baseURL } from '../../../../auth/baseURL'

axios.defaults.baseURL = baseURL()

export const getAPPData = ({commit}, payload) => {
    axios.get(`/api/app/${payload}`)
        .then(res => {
            commit('setAPPData', res.data)
            Event.$emit('successGetData', payload)
        })
        .catch(err => {
            console.log(err)
        })
}