
// $(function () {
// 	let proce_elem = document.getElementById("procedure");
// 	let proce_val = proce_elem.options[proce_elem.selectedIndex].value;

// 	if (proce_val == "QCBE/QCBS"){

// 	}
// })

$(document).on('change', '.procedure', function(e){
	let proce_elem = document.getElementById("procedure");
	let proce_val = proce_elem.options[proce_elem.selectedIndex].value;

	if (proce_val == "QCBE/QCBS")
		$('#ifqcbe').attr('hidden', false);
	else
		$('#ifqcbe').attr('hidden', true);
});

import Vue from 'vue'

new Vue({
    data() {
        return {
            reiNo: null,
        }
    },
    methods: {
        initiators(){
            $('#opening_date').datetimepicker({
                format:'F d, Y h:i A',
                step: 30,
                formatTime: 'h:i A',
                minDate: 0,
                validateOnBlur: false,
            })

            $('#availability_date').datepicker({
                dateFormat: 'MM dd, yy',
                minDate: 0,
            })

            $('#completedWithin').datepicker({
                constrainInput: false,
                dateFormat: 'MM dd, yy',
                minDate: 0,
            })
        },
        reiNumberFiller(){
            let year = String(new Date().getFullYear());
            let month = String(new Date().getMonth() + 1);
            let reiCounter = $('#reiCount').val()
            month = month <= 9 ? "0" + month : month;
            let unique
            reiCounter++
            if(reiCounter.toString().length == 1) {
                unique = `000${reiCounter}`
            } else if (reiCounter.toString().length == 2) {
                unique = `00${reiCounter}`
            } else if (reiCounter.toString().length == 3) {
                unique = `0${reiCounter}`
            } else {
                unique = reiCounter
            }
            this.reiNo = `${year}${month}-${unique}`
        }
    },
    mounted() {
        this.initiators()
        this.reiNumberFiller()
        let proce_elem = document.getElementById("procedure");
        let proce_val = proce_elem.options[proce_elem.selectedIndex].value;
    
        if (proce_val == "QCBE/QCBS")
            $('#ifqcbe').attr('hidden', false);
        else
            $('#ifqcbe').attr('hidden', true);
    }
}).$mount('#rei_form')

$(document).on('click', '.add_new_file', function(){
    let prFilesCounter = $('.rfq-files').length

    if(prFilesCounter < 5){
        let newFileRow = $(`
            <div class="input-group">
                <input type="file" name="invite_requirements[]" class="form-control rfq-files" accept=".pdf">
                <div class="input-group-append">
                    <button class="btn btn-danger remove_file_row" type="button"><i class="fa fa-trash"></i></button>
                </div>
            </div>
            `)

        newFileRow.appendTo('div#files_section')
    } else {
        alert('Attached files must not be more than 5!')
    }
})

$(document).on('click', '.remove_file_row', function(){
    $(this).closest('div.input-group').remove()
})