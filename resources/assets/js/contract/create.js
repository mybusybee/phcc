import { generateFormNumber } from '../orders-parent/form-number-generator'

$(function() {
    $('#agreementDate').datepicker({
        dateFormat:'MM d, yy',
        minDate: 0,
    });
})

$('#supplierSelector').on('change', function(){
    let address = $(this).find(':selected').data('address')
    let companyName = $(this).find(':selected').text()

    $('#supplierAddress').val(address)
    $('#supplierCompanyName2').val(companyName)
})

$(function(){
    let count = $('#ordersCount').val()
    let formNo = generateFormNumber(count)
    $('#formNo').val(formNo)
})

$(function(){
    let supplierID = $('#selectedSupplier').val()
    $('#supplierSelector').find(`option[value="${supplierID}"]`).attr('selected', true)
})