export const getSuppliers = state => {
    return state.suppliers
}

export const getSuppliersCount = state => {
    return state.suppliersCount
}

export const getUninvitedSuppliers = state => {
    return state.uninvitedSuppliers
}