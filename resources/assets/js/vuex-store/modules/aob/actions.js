import axios from'axios'
import { baseURL } from '../../../auth/baseURL'
axios.defaults.baseURL = baseURL()

export const reloadSuppliers = ({commit}, payload) => {
    if(payload.formType === 'create'){
        axios.get(`/api/invited-suppliers/invite/${payload.inviteID}`)
        .then(res => {
            commit('reloadSuppliers', res.data)
        })
        .catch(err => console.log(err))
    } else {
        axios.get(`/api/abstract/${payload.abstractParentID}`)
        .then(res => {
            commit('reloadSuppliers', res.data)
        })
        .catch(err => console.log(err))
    }
}

export const reloadUninvitedSuppliers = ({commit}, payload) => {
    if(payload.formType === 'create') {
        axios.get(`/api/uninvited-suppliers/invitation/${payload.abstractParentID}`)
        .then(res => {
            commit('reloadUninvitedSuppliers', res.data)
        })
        .catch(err => console.log(err))
    } else {
        axios.get(`/api/not-attached/abstract/${payload.abstractParentID}`)
            .then(res => {
                commit('reloadUninvitedSuppliers', res.data)
            })
            .catch(err => console.log(err))
    }
}

export const pushNewUninvitedSupplier = ({commit}, payload) => {
    commit('pushNewUninvitedSupplier', payload)
}

export const removeSupplierFromArray = ({commit}, payload) => {
    commit('removeSupplierFromArray', payload)
}