import * as getters from './getters'
import * as actions from './actions'
import * as mutations from './mutations'

export default {
    state: {
        suppliers: {},
        suppliersCount: 0,
        uninvitedSuppliers: {},
    },
    getters, actions, mutations
}