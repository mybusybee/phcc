export const reloadSuppliers = (state, payload) => {
    state.suppliers = payload
    state.suppliersCount = payload.length
}

export const reloadUninvitedSuppliers = (state, payload) => {
    state.uninvitedSuppliers = payload
    setTimeout(() => {
        $('#suppliers-datatable').DataTable()
    }, 800)
}

export const pushNewUninvitedSupplier = (state, payload) => {
    state.suppliers.push(payload)
    state.suppliersCount += 1
}

export const removeSupplierFromArray = (state, payload) => {
    state.suppliers.splice(payload, 1)
    state.suppliersCount -= 1
}