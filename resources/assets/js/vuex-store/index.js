import Vue from 'vue'
import Vuex from 'vuex'
import aob from './modules/aob'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        test: 'hehe'
    },
    modules: {
        aob
    }
})