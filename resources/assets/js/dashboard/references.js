$('.addNewFile').on('click', function() {
    let row = $(`
        <div class="col-xl-12 file-row">
            <div class="input-group input-group-sm">
                <input type="file" name="files[]" accept=".pdf" class="form-control form-control-sm" />
                <div class="input-group-append">
                    <button type="button" class="btn btn-xs btn-danger removeRow"><i class="fa fa-minus"></i></button>
                </div>
            </div>
        </div>
    `)

    row.appendTo('#filesSection')
})

$(document).on('click', '.removeRow', function(){
    $(this).closest('div.file-row').remove();
})