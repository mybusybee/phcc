import Vue from 'vue'
import Dashboard from '../components/dashboard/base'

new Vue({
    render: a => a(Dashboard)
}).$mount('#dashboard')