<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryCode extends Model
{
    public function paps() {
        return $this->hasMany('App\ProjectsProgramsActivities', 'category_code');
    }
}
