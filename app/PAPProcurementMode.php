<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PAPProcurementMode extends Model
{
    protected $table = 'paps_procurement_modes';
}
