<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AoqPrItemsSupplier extends Model
{
    protected $table = 'aoq_pr_items_suppliers';

    public function pr_item(){
        return $this->belongsTo('App\PurchaseRequestItem');
    }

    public function item_supplier(){
        return $this->belongsTo('App\Supplier', 'supplier_id');
    }
}
