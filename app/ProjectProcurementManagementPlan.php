<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectProcurementManagementPlan extends Model
{
    protected $table = 'ppmps';

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
     protected $fillable = [
        'end_user', 
        'total_budget', 
        'provision_for_inflation', 
        'contingency',
        'total_estimated_budget',
        'prepared_by',
        'submitted_by' 
    ];

    public function paps(){
    	return $this->hasMany('App\ProjectsProgramsActivities', 'ppmp_id');
    }

    public function office() {
        return $this->belongsTo('App\Office', 'end_user');
    }

    public function director(){
        return $this->belongsTo('App\User', 'assigned_director');
    }

    public function prepared_by_user() {
        return $this->belongsTo('App\User', 'created_by');
    }

    public function notifications() {
        return $this->hasMany('App\SystemNotification');
    }
}
