<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pmr extends Model
{
    
    public function pmr_item(){
        return $this->hasMany('App\PmrItem', 'pmr_id');
    }

    public function pr(){
        return $this->hasOne('App\PurchaseRequest', 'pr_id');
    }

    public function office(){
        return $this->belongsTo('App\Office', 'office_id');
    }
}
