<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AbstractParent extends Model
{
    protected $table = 'abstracts';
    
    public function aob(){
    	return $this->hasOne('App\AbstractOfBids', 'abstract_id');
    }

    public function aoq(){
    	return $this->hasOne('App\AbstractOfQuotation', 'abstract_id');
    }

    public function aop(){
    	return $this->hasOne('App\AbstractOfProposal', 'abstract_id');
    }

    public function invite(){
        return $this->belongsTo('App\Invitation', 'invite_id');
    }

    public function recommendation(){
        return $this->hasOne('App\AbstractRecommendation', 'abstract_id');
    }

    public function docs(){
        return $this->hasMany('App\AbstractDocument', 'abstract_id');
    }

    public function suppliers() {
        return $this->belongsToMany('App\Supplier', 'abstracts_suppliers', 'abstract_id', 'supplier_id')->withPivot('ranking', 'compliance', 'technical_proposal');
    }

    public function orders() {
        return $this->hasMany('App\OrdersParent', 'abstract_id');
    }

    public function notifications() {
        return $this->hasMany('App\SystemNotification');
    }
}
