<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use App\AnnualProcurementPlan;
use App\ProcurementMode;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;

class Exports implements FromView
{
	public function __construct($year){
		$this->app_year = $year;
	}

	public function view(): View
	{
		$app = AnnualProcurementPlan::where('app_year', $this->app_year)->get();
		$procurement_modes = ProcurementMode::all();

		foreach($procurement_modes as $procurement_mode){
			foreach($app as $app_row){
				if($app_row->mode_of_procurement == $procurement_mode->id){
					$app_row->mode_of_procurement = $procurement_mode->mode;
				}
			}
		}

		return view('app.export_to_csv', [
			'app' => $app
		]);
	}
}