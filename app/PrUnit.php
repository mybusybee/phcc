<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrUnit extends Model
{
    public $table = 'pr_units';
}
