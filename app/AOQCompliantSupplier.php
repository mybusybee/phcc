<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AOQCompliantSupplier extends Model
{
    protected $table = 'aoq_compliant_suppliers'; 
}
