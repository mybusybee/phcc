<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use App\Pmr;

class PmrExport implements FromView
{
	public function __construct($id){
		$this->pmr_id = $id;
	}

	public function view(): View
	{
        $pmr = Pmr::find($this->pmr_id);
        
		return view('pmr.export', [
			'pmr' => $pmr
		]);
	}
}