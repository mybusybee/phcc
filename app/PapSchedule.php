<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PapSchedule extends Model
{
	public $table = 'pap_schedule';
	
    public function pap(){
    	return $this->belongsTo('App\ProjectsProgramsActivities', 'pap_id');
    }
}
