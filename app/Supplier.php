<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Supplier extends Model
{
    public function rfqs(){
        return $this->belongsToMany('App\RequestForQuotation', 'suppliers_rfqs', 'supplier_id', 'rfq_id');
    }

    public function aoq(){
        return $this->belongsToMany('App\AbstractOfQuotation', 'aoq_compliant_suppliers', 'supplier_id', 'aoq_id');
    }

    public function aoq_items(){
        return $this->belongsToMany('App\PurchaseRequestItem', 'aoq_pr_items_suppliers', 'supplier_id', 'pr_item_id')
        ->withPivot('financial_proposal', 'ranking', 'aoq_id');
    }

    public function prospective_aoq(){
        return $this->belongsToMany('App\AbstractOfQuotation', 'aoq_prospective_consultants', 'supplier_id', 'aoq_id');
    }

    public function po_jo(){
        return $this->hasMany('App\PurchaseJobOrder', 'supplier_id');
    }

    public function aoq_pr_items_supplier(){
        return $this->hasOne('App\AoqPrItemsSupplier', 'supplier_id');
    }

    public function invitationForms(){
        return $this->belongsToMany('App\Invitation', 'suppliers_invitations', 'supplier_id', 'invitation_id')->withPivot('email_status');
    }

    public function docs(){
        return $this->hasMany('App\SupplierDoc', 'supplier_id');
    }

    public function pr_items(){
        return $this->belongsToMany('App\PurchaseRequestItem', 'suppliers_pr_items', 'supplier_id', 'pr_item_id')->withPivot('abstract_id', 'unit_cost', 'total', 'per_item_ranking');
    }

    public function pr_sub_items(){
        return $this->belongsToMany('App\PurchaseRequestSubItem', 'suppliers_pr_sub_items', 'supplier_id', 'pr_sub_item_id')->withPivot('total', 'per_item_ranking', 'id', 'abstract_id', 'unit_cost');
    }

    public function recommendation_items(){
        return $this->hasMany('App\AbstractRecoItem', 'supplier_id');
    }

    public function supplier_eligibilities(){
        return $this->hasMany('App\AbstractSupplierEligibility', 'supplier_id');
    }

    public function abstracts() {
        return $this->belongsToMany('App\AbstractParent', 'abstracts_suppliers', 'supplier_id', 'abstract_id');
    }

    public function po(){
        return $this->hasMany('App\PurchaseOrder', 'supplier_id');
    }
}
