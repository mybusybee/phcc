<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PmrItem extends Model
{
    protected $fillable = ['pr_id', 'pjo_id', 'aoq_id', 'rfq_id', 'prescribed_turnaround_time', 'actual_turnaround_time', 'completed'];

    public function pmr(){
        return $this->belongsTo('App\Pmr', 'pmr_id');
    }

    public function app() {
        return $this->belongsTo('App\AnnualProcurementPlan', 'app_id');
    }

    public function item_details(){
        return $this->hasMany('App\PmrItemDetail');
    }
}