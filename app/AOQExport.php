<?php

namespace App;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use App\AbstractOfQuotation as AOQ;

class AOQExport implements FromView
{
    public function __construct($id){
		$this->id = $id;
    }
    
    public function view(): View
    {
        $aoq = AOQ::find($this->id);

		return view('aoq.pdf', [
			'aoq' => $aoq,
		]);
    }
}

?>