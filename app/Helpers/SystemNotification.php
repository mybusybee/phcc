<?php

namespace App\Helpers;

use App\SystemNotification as SysNotif;

class SystemNotification {
    

    public function __construct(){
        
    }

    /**
     * 
     * $user = Instance of App\User
     * $obj = data Object
     * $type = String type of form
     * $form_object = instance of passed form
     * 
     */
    public static function add($user, $obj, $type=null, $form_object=null) {
        $notif = new SysNotif;
        $notif->user()->associate($user);
        $notif->message = '(From:' . \Auth::user()->office->office . ')-' . $obj->message;
        $notif->link = $obj->link;
        if($type === 'ppmp') {
            $notif->ppmp()->associate($form_object);
        } else if ($type === 'pr') {
            $notif->pr()->associate($form_object);
        } else if ($type === 'inv') {
            $notif->invitation()->associate($form_object);
        } else if ($type === 'abstract') {
            $notif->abstract()->associate($form_object);
        } else if ($type === 'order') {
            $notif->order()->associate($form_object);
        }
        $notif->save();
        
    }

    public static function markAsRead($id) {
        $notif = SysNotif::find($id);
        $notif->is_read = 1;
        $notif->save();
    }

    public static function checkURL(){
        if($_GET) {
            SystemNotification::markAsRead($_GET['n']);
        }
    }

}

?>