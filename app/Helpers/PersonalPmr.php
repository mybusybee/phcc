<?php

namespace App\Helpers;
use Illuminate\Http\Request;
use App\PurchaseRequest;
use App\ProcurementUserPmr;
use Carbon\Carbon;
use App\ProcurementUserPmrItem as PmrItem;

class PersonalPmr
{
    public static function add(PurchaseRequest $pr) {
        $pmr = PersonalPmr::checkIfPmrExists($pr->proc_user_assigned, PersonalPmr::getYearAndMonth($pr->pr_prep_date));
        if($pmr){
            PersonalPmr::update($pr, $pmr);
        } else {
            PersonalPmr::create($pr);
        }
    }

    private static function checkSemester($month){
        $first = [1,2,3,4,5,6];

        if(in_array($month, $first)){
            return 1;
        } else {
            return 2;
        }
    }

    private static function update($pr, $pmr){
        PersonalPmr::newPmrItem($pr, $pmr);
    }

    private static function create($pr){
        $new_pmr = new ProcurementUserPmr;
        $new_pmr->year = PersonalPmr::getYearAndMonth($pr->pr_prep_date)->year;
        $new_pmr->procurement_user_id = $pr->proc_user_assigned;
        $new_pmr->semester = PersonalPmr::checkSemester(PersonalPmr::getYearAndMonth($pr->pr_prep_date)->month);
        $new_pmr->save();
        PersonalPmr::newPmrItem($pr, $new_pmr);
    }

    private static function newPmrItem($pr, $pmr){
        $new_pmr_item = new PmrItem;
        $new_pmr_item->pr()->associate($pr);
        $new_pmr_item->procurement_user_pmr()->associate($pmr);
        $new_pmr_item->save();
    }

    private static function checkIfPmrExists($proc_user_id, $dateObj){
        $pmr = ProcurementUserPmr::where([
            'procurement_user_id' => $proc_user_id,
            'year' => $dateObj->year,
            'semester' => PersonalPmr::checkSemester($dateObj->month)
        ])->first();
        
        if($pmr) {
            return $pmr;
        } else {
            return false;
        }
    }

    private static function getYearAndMonth($date){
        $dateObj = new \stdClass();
        $dateObj->year = Carbon::parse($date)->year;
        $dateObj->month = Carbon::parse($date)->month;
        return $dateObj;
    }
}

?>