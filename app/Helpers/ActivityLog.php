<?php

namespace App\Helpers;
use App\Log;

class ActivityLog 
{
    public static function add($activity_arr){

        $log = new Log;
        $log->user_name = $activity_arr['user_name'];
        $log->user_office = $activity_arr['user_office'];
        $log->log = $activity_arr['activity'];
        $log->save();
    }
}
?>