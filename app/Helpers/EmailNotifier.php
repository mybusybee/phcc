<?php

namespace App\Helpers;
use Mail;

class EmailNotifier
{
    public static function sendMail($user_email, $user_pw){

        Mail::send('emails.new-user-notifier', ['user_email' => $user_email, 'user_password' => $user_pw, 'url' => 'http://beezymail.com/phcc'], function ($message) use ($user_email){

            $message->from('procurement@phcc.gov.ph', 'Philippine Competition Commission');
            $message->subject('Your PCC PMS Account Credentials');

            $message->to($user_email);
            
        });
    }

    public static function sendAttachments($form){
        $orders = ['PO', 'JO', 'CONTRACT'];
        $invitations = ['RFQ', 'RFP', 'REI', 'IB'];

        //if form is po/jo/contract
        if(in_array($form->type, $orders)){
            if($form->type === 'PO') {
                $supplier_emails[] = explode(', ', $form->supplier->email);
                foreach($supplier_emails as $supplier_email) {
                    Mail::send('emails.attachments', ['title' => $title, 'content' => 'Please see attached files.'], function ($message)
                    {
                        $message->from('info@phcc.gov.ph', 'Philippine Competition Commission');
                        $message->to($supplier_email);
                        if(count($form->attachments)) {
                            foreach($form->attachments as $attachment) {
                                $message->attach(storage_path($attachment->filepath));
                            }
                        }
                    });
                }
            } else {
                $supplier_emails[] = explode(', ', $form->supplier->email);
                foreach($supplier_emails as $supplier_email) {
                    Mail::send('emails.attachments', ['title' => 'Dear ' . $supplier->company_name, 'content' => 'Please see attached files.'], function ($message)
                    {
                        $message->from('info@phcc.gov.ph', 'Philippine Competition Commission');
                        $message->to($supplier_email);
                        if(count($form->attachments)) {
                            foreach($form->attachments as $attachment) {
                                $message->attach(storage_path($attachment->filepath));
                            }
                        }
                    });
                }
            }
            foreach($form->attachments as $file) {

            }
        } else {
            foreach($form->files as $file){
                
            }
        }
    }
}
?>