<?php

namespace App\Helpers;
use App\Pmr;
use App\PurchaseRequest as PR;
use App\RequestForQuotation as RFQ;
use App\AbstractOfQuotation as AOQ;
use App\PurchaseJobOrder as PJO;
use App\PmrItem;

class PmrHelper 
{
    public static function addPr($pr_id){

        /** Find newly created PR **/
        $pr = PR::find($pr_id);

        $pmr = Pmr::where([
            'year' => date('Y', strtotime($pr->created_at)),
        ])->first();

        $prs = PR::where('pr_prep_date', 'like', '%'.date('Y', strtotime($pr->created_at)))->get();

        //if not found, create new
        if(!$pmr){
            $pmr = new Pmr;
        }

        $pmr->year = date('Y', strtotime($pr->created_at));

        /** Total ABC **/
        $total_abc = 0;
        foreach($prs as $pr){
            $total_abc += $pr->pr_item_total->total_estimate;
        }
        $pmr->total_abc = $total_abc;

        /**
         * 
         * Total Contract Amount
         * All Financial Proposals under that aoq 
         * 
         */
        foreach($prs as $pr){
            //lot purchase
            if($pr->pr_item_total->is_lot_purchase == 1){
                
                $total_contract_amount = 0;
                foreach($pr->rfq as $pr_rfq){
                    foreach($pr_rfq->aoq->compliant_supplier as $cs){
                        if($cs->pivot->ranking == 1){
                            $total_contract_amount += $cs->pivot->total_bid;
                        }
                    }
                }
            } else {
                //per item purchase
                $total_contract_amount = 0;
                foreach($pr->rfq as $pr_rfq){
                    foreach($pr_rfq->aoq->aoq_pr_items as $aoq_pr_item){
                        if($aoq_pr_item->pivot->ranking == 1){
                            $total_contract_amount += $aoq_pr_item->pivot->financial_proposal;
                        }
                    }
                }
            }
        }
        $pmr->total_contract_amt = $total_contract_amount;
        $pmr->total_savings = $total_abc - $total_contract_amount;

        $pmr_item = new PmrItem;
        $pmr_item->pr_id = $pr_id;

        $pmr->save();
        $pmr->pmr_item()->save($pmr_item);
    }

    public static function addRfq($rfq_id){
        $rfq = RFQ::find($rfq_id);

        $pmr = Pmr::where([
            'year' => date('Y', strtotime($rfq->pr->pr_prep_date)),
        ])->first();
        
        $pmr_item = PmrItem::where([
            'pr_id' => $rfq->pr->id,
            'pmr_id' => $pmr->id,
        ])->first();

        $pmr_item->rfq_id = $rfq_id;
        $pmr_item->save();
    }

    public static function addAoq($aoq_id){
        $aoq = AOQ::find($aoq_id);

        $pmr = Pmr::where([
            'year' => date('Y', strtotime($aoq->rfq->pr->pr_prep_date)),
        ])->first();

        //lot purchase
        if($aoq->rfq->pr->pr_item_total->is_lot_purchase == 1){

            $total_contract_amount = 0;
            foreach($aoq->compliant_supplier as $cs){
                if($cs->pivot->ranking == 1){
                    $total_contract_amount += $cs->pivot->total_bid;
                }
            }
        } else {
            //per item purchase
            $total_contract_amount = 0;
            foreach($aoq->aoq_pr_items as $aoq_pr_item){
                if($aoq_pr_item->pivot->ranking == 1){
                    $total_contract_amount += $aoq_pr_item->pivot->financial_proposal;
                }
            }
        }

        $pmr->total_contract_amt = $total_contract_amount;
        $pmr->total_savings = $pmr->total_abc - $total_contract_amount;
        
        $pmr_item = PmrItem::where([
            'pr_id' => $aoq->rfq->pr->id,
            'pmr_id' => $pmr->id,
        ])->first();

        $pmr_item->aoq_id = $aoq_id;

        $pmr->save();
        $pmr_item->save();
    }

    public static function addPoJo($pojo_id){

    }
}
?>