<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AbstractRecoItem extends Model
{
    public function recommendation(){
    	return $this->belongsTo('App\AbstractRecommendation', 'reco_id');
    }

    public function supplier(){
    	return $this->belongsTo('App\Supplier', 'supplier_id');	
    }
}
