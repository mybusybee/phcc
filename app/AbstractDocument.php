<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AbstractDocument extends Model
{
    public function abstract_parent(){
        return $this->belongsTo('App\AbstractParent', 'abstract_id');
    }

    public function supplier_eligibilities(){
    	return $this->hasMany('App\AbstractSupplierEligibility', 'abstract_docs_id');
    }
}
