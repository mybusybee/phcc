<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UacsObject extends Model
{
    public function paps() {
        return $this->hasMany('App\ProjectsProgramsActivities', 'uacs_object_id');
    }
}
