<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProcurementMode extends Model
{
    public function paps(){
    	return $this->belongsToMany('ProjectsProgramsActivities', 'paps_procurement_modes', 'procurement_mode_id', 'pap_id');
    }

    public function pr(){
        return $this->hasMany('App\PurchaseRequest');
    }

    public function app_row() {
        return $this->hasOne('App\AnnualProcurementPlan', 'mode_of_procurement');
    }

    public function aob(){
    	return $this->hasOne('App\AbstractOfBids', 'procurement_mode_id');
    }

    public function aop(){
    	return $this->hasOne('App\AbstractOfProposal', 'procurement_mode_id');
    }

    public function aoq(){
    	return $this->hasOne('App\AbstractOfQuotation', 'procurement_mode_id');
    }

    public function uploaded_pmr_items() {
        return $this->hasMany('App\UploadedPmrItem', 'procurement_mode_id');
    }
}
