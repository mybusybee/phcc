<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestForQuotation extends Model
{
    public $table = 'rfqs';
    
    public function pr(){
        return $this->belongsTo('App\PurchaseRequest', 'pr_id');
    }

    public function rfq_req(){
        return $this->hasOne('App\RequestRequirement', 'rfq_id');
    }

    public function suppliers(){
        return $this->belongsToMany('App\Supplier', 'suppliers_rfqs', 'rfq_id', 'supplier_id');
    }

    public function aoq(){
        return $this->hasOne('App\AbstractOfQuotation', 'rfq_id');
    }

    public function invite(){
        return $this->belongsTo('App\Invitation', 'invite_id');
    }
}
