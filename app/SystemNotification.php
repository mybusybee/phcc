<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SystemNotification extends Model
{
    public function user(){
        return $this->belongsTo('App\User', 'user_id');
    }

    public function ppmp() {
        return $this->belongsTo('App\ProjectProcurementManagementPlan');
    }

    public function pr() {
        return $this->belongsTo('App\PurchaseRequest');
    }

    public function invitation() {
        return $this->belongsTo('App\Invitation');
    }

    public function abstract() {
        return $this->belongsTo('App\AbstractParent');
    }

    public function order() {
        return $this->belongsTo('App\OrdersParent');
    }
}
