<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProcurementUserPmrItem extends Model
{
    public function procurement_user_pmr(){
        return $this->belongsTo('App\ProcurementUserPmr', 'procurement_user_pmr_id');
    }

    public function pr(){
        return $this->belongsTo('App\PurchaseRequest', 'pr_id');
    }
}
