<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvitationAttachment extends Model
{
    protected $fillable = ['filepath', 'filename'];

    public function invite(){
        return $this->belongsTo('App\Invitation', 'invite_id');
    }
}
