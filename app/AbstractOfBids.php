<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AbstractOfBids extends Model
{
    // public function invite(){
    //     return $this->belongsTo('App\Invitation', 'invite_id');
    // }

    public function abstract_parent(){
        return $this->belongsTo('App\AbstractParent', 'abstract_id');
    }

    public function procurement_mode(){
    	return $this->belongsTo('App\ProcurementMode', 'procurement_mode_id');
    }
}
