<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupplierRfq extends Model
{
    public $table = 'suppliers_rfqs';
}
