<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AbstractRecommendation extends Model
{
    public function abstract_parent(){
    	return $this->belongsTo('App\AbstractParent', 'abstract_id');
    }

    public function recommendation_items(){
    	return $this->hasMany('App\AbstractRecoItem', 'reco_id');
    }
}
