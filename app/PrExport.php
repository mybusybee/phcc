<?php

namespace App;
use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;
use App\PurchaseRequest;

class PrExport implements FromView
{
	public function __construct($id){
		$this->pr_id = $id;
	}

	public function view(): View
	{
        $pr = PurchaseRequest::find($this->pr_id);
        
		return view('pr.export', [
			'pr' => $pr
		]);
	}
}