<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SupplierDoc extends Model
{
    protected $fillable = ['filepath', 'filename'];

    public function supplier(){
        return $this->belongsTo('App\Supplier', 'supplier_id');
    }
}
