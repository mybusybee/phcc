<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AbstractSupplierEligibility extends Model
{
    public function abstract_doc(){
        return $this->belongsTo('App\AbstractDocument', 'abstract_docs_id');
    }

    public function supplier(){
        return $this->belongsTo('App\Supplier', 'supplier_id');
    }
}
