<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderAttachment extends Model
{
    protected $fillable = ['original_filename', 'filepath'];

    public function order_parent(){
        return $this->belongsTo('App\OrdersParent', 'orders_parent_id');
    }
}
