<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseRequestSubItem extends Model
{
    protected $fillable = ['unit', 'description', 'quantity', 'est_cost_unit', 'est_cost_total'];

    public function pr_item() {
        return $this->belongsTo('App\PurchaseRequestItem', 'pr_item_id');
    }
    
    public function suppliers(){
        return $this->belongsToMany('App\Supplier', 'suppliers_pr_sub_items', 'pr_sub_item_id', 'supplier_id')->withPivot('abstract_id', 'unit_cost', 'total', 'per_item_ranking');
    }
}
