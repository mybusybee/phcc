<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class APPSchedule extends Model
{
    protected $table = 'app_schedules';

    protected $fillable = ['id', 'advertisement', 'submission', 'notice_of_award', 'contract_signing'];

    public function app(){
    	return $this->belongsTo('App\AnnualProcurementPlan');
    }
}
