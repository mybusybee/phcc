<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestRequirement extends Model
{
    public $table = 'request_requirements';

    public function rfq(){
        return $this->belongsTo('App\RequestForQuotation', 'rfq_id');
    }

    public function invitationForm(){
        return $this->belongsTo('App\Invitation', 'invite_id');
    }
}
