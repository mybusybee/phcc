<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MovingApp extends Model
{
    public function pap(){
    	return $this->belongsTo('App\ProjectsProgramsActivities', 'pap_id');
    }
}
