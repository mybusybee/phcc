<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UploadedPmr extends Model
{
    public function procurement_user() {
        return $this->belongsTo('App\User', 'procurement_user_id');
    }

    public function pmr_items() {
        return $this->hasMany('App\UploadedPmrItem', 'uploaded_pmr_id');
    }
}
