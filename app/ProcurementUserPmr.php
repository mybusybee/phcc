<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProcurementUserPmr extends Model
{
    public function procurement_user(){
        return $this->belongsTo('App\User', 'procurement_user_id');
    }

    public function pmr_items(){
        return $this->hasMany('App\ProcurementUserPmrItem', 'procurement_user_pmr_id');
    }
}
