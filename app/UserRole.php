<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
	protected $fillable = ['id', 'user_role'];

	public function user(){
		return $this->hasMany('App\User');
	}

	public function pr(){
		return $this->hasMany('App\PurchaseRequest');
	}
}
