<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrdersParent extends Model
{
    public function abstract() {
        return $this->belongsTo('App\AbstractParent', 'abstract_id');
    }

    public function po() {
        return $this->hasOne('App\PurchaseOrder', 'orders_parent_id');
    }

    public function jo() {
        return $this->hasOne('App\JobOrder', 'orders_parent_id');
    }

    public function contract(){
        return $this->hasOne('App\Contract','orders_parent_id');
    }

    public function attachments(){
        return $this->hasMany('App\OrderAttachment', 'orders_parent_id');
    }

    public function notifications() {
        return $this->hasMany('App\SystemNotification');
    }
}
