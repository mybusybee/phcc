<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseRequest extends Model
{
    protected $dates = ['date_received', 'date_assigned'];

    public function pr_items(){
        return $this->hasMany('App\PurchaseRequestItem', 'pr_id');
    }

    public function pr_budget(){
        return $this->hasOne('App\PurchaseRequestBudget', 'pr_id');
    }

    public function office(){
        return $this->belongsTo('App\Office');
    }

    public function division(){
        return $this->belongsTo('App\UserRole', 'rc_code');
    }

    public function user_request(){
        return $this->belongsTo('App\User', 'requested_by');
    }

    public function procurement_mode(){
        return $this->belongsTo('App\ProcurementMode', 'mode_of_procurement');
    }

    public function pr_item_total(){
        return $this->hasOne('App\PurchaseRequestItemTotal', 'pr_id');
    }

    public function proc_user(){
        return $this->belongsTo('App\User', 'proc_user_assigned');
    }

    public function revision_requestor(){
        return $this->belongsTo('App\User', 'revision_last_modifier');
    }

    public function rfq(){
        return $this->hasMany('App\RequestForQuotation', 'pr_id');
    }

    public function rfp(){
        return $this->hasMany('App\RequestForProposal', 'pr_id');
    }

    public function pmr(){
        return $this->hasMany('App\Pmr', 'pr_id');
    }

    public function files(){
        return $this->hasMany('App\PurchaseRequestAttachment', 'pr_id');
    }

    public function rei(){
        return $this->hasOne('App\ReqExpInterest', 'pr_id');
    }
    
    public function itb(){
        return $this->hasOne('App\InvitationToBid', 'pr_id');
    }

    public function personal_pmr_item(){
        return $this->hasOne('App\ProcurementUserPmrItem', 'pr_id');
    }

    public function notifications() {
        return $this->hasMany('App\SystemNotification');
    }
}
