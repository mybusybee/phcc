<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PersonalPMRResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                          => $this->id,
            'pr_no'                       => $this->pr->pr_no,
            'code'                        => $this->code,
            'program_project'             => $this->program_projects,
            'end_user'                    => $this->end_user,
            'procurement_mode'            => $this->procurement_mode,
            'pr_received_date'            => $this->pr->date_received === null ? 'Not yet received.' : $this->pr->date_received->format('F d, Y - h:i:s A'),
            'pre_proc_conference'         => $this->pre_proc_conference === null ? '---' : $this->pre_proc_conference,
            'ads_post_of_ib'              => $this->ads_post_of_ib === null ? '---' : $this->ads_post_of_ib,
            'pre_bid_conf'                => $this->pre_bid_conf === null ? '---' : $this->pre_bid_conf,
            'eligibility_check'           => $this->eligibility_check === null ? '---' : $this->eligibility_check,
            'sub_open_of_bids'            => $this->sub_open_of_bids === null ? '---' : $this->sub_open_of_bids,
            'bid_evaluation'              => $this->bid_evaluation === null ? '---' : $this->bid_evaluation,
            'post_qual'                   => $this->post_qual === null ? '---' : $this->post_qual,
            'noa'                         => $this->noa === null ? '---' : $this->noa,
            'contract_signing'            => $this->contract_signing === null ? '---' : $this->contract_signing,
            'ntp'                         => $this->ntp === null ? '---' : $this->ntp,
            'delivery_completion'         => $this->delivery_completion === null ? '---' : $this->delivery_completion,
            'inspection_acceptance'       => $this->inspection_acceptance === null ? '---' : $this->inspection_acceptance,
            'source_of_funds'             => $this->source_of_funds,
            'abc_total'                   => $this->pr->pr_item_total->total_estimate,
            'abc_mooe'                    => $this->abc_mooe,
            'abc_co'                      => $this->abc_co,
            'contract_cost_total'         => $this->total_contract_cost,
            'contract_cost_mooe'          => $this->contract_cost_mooe === null ? '---' : $this->contract_cost_mooe,
            'contract_cost_co'            => $this->contract_cost_co === null ? '---' : $this->contract_cost_co,
            'list_of_invited_observers'   => $this->list_of_invited_observers === null ? '---' : $this->list_of_invited_observers,
            'receipt_pre_bid_conf'        => $this->receipt_pre_bid_conf === null ? '---' : $this->receipt_pre_bid_conf,
            'receipt_eligibility_check'   => $this->receipt_eligibility_check === null ? '---' : $this->receipt_eligibility_check,
            'receipt_sub_open_of_bids'    => $this->receipt_sub_open_of_bids === null ? '---' : $this->receipt_sub_open_of_bids,
            'receipt_bid_evaluation'      => $this->receipt_bid_evaluation === null ? '---' : $this->receipt_bid_evaluation,
            'receipt_post_qual'           => $this->receipt_post_qual === null ? '---' : $this->receipt_post_qual,
            'receipt_delivery_completion' => $this->receipt_delivery_completion === null ? '---' : $this->receipt_delivery_completion,
            'remarks'                     => $this->remarks,
            'status'                      => $this->status,
            'completed'                   => $this->completed
        ];
    }
}
