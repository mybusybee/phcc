<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\UploadedPMRItemsResource;

class UploadedPMRResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                           => $this->id,
            'procurement_user'             => $this->procurement_user,
            'year'                         => $this->year,
            'semester'                     => $this->semester,
            'prepared_by'                  => $this->prepared_by,
            'recommended_approval'         => $this->recommended_approval,
            'approved_by'                  => $this->approved_by,
            'total_alloted_budget'         => $this->total_alloted_budget,
            'total_contract_price'         => $this->total_contract_price,
            'total_savings'                => $this->total_savings,
            'total_alloted_budget_ongoing' => $this->total_alloted_budget_ongoing,
            'items'                        => UploadedPMRItemsResource::collection($this->pmr_items),
        ];
    }
}
