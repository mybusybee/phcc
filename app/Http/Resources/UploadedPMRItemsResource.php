<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UploadedPMRItemsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                                  => $this->id,
            'pr_no'                               => $this->pr_no,
            'code'                                => $this->code,
            'program_project'                     => $this->program_project,
            'end_user'                            => $this->end_user,
            'procurement_mode'                    => $this->procurement_mode,
            'pr_date_received'                    => $this->pr_date_received,
            'source_of_funds'                     => $this->source_of_funds,
            'list_of_invited_observers'           => $this->list_of_invited_observers,
            'abc_total'                           => $this->abc_total,
            'abc_mooe'                            => $this->abc_mooe,
            'abc_co'                              => $this->abc_co,
            'contract_cost_total'                 => $this->contract_cost_total,
            'contract_cost_mooe'                  => $this->contract_cost_mooe,
            'contract_cost_co'                    => $this->contract_cost_co,
            'apa__pre_proc_conference'            => $this->apa__pre_proc_conference,
            'apa__ads_post_of_ib'                 => $this->apa__ads_post_of_ib,
            'apa__pre_bid_conf'                   => $this->apa__pre_bid_conf,
            'apa__eligibility_check'              => $this->apa__eligibility_check,
            'apa__sub_open_of_bids'               => $this->apa__sub_open_of_bids,
            'apa__bid_evaluation'                 => $this->apa__sub_open_of_bids,
            'apa__post_qual'                      => $this->apa__post_qual,
            'apa__notice_of_award'                => $this->apa__notice_of_award,
            'apa__contract_signing'               => $this->apa__contract_signing,
            'apa__notice_to_proceed'              => $this->apa__contract_signing,
            'apa__delivery_completion'            => $this->apa__contract_signing,
            'apa__inspection_and_acceptance'      => $this->apa__contract_signing,
            'dri__pre_bid_conf'                   => $this->dri__pre_bid_conf,
            'dri__eligibility_check'              => $this->dri__eligibility_check,
            'dri__sub_open_of_bids'               => $this->dri__sub_open_of_bids,
            'dri__bid_evaluation'                 => $this->dri__sub_open_of_bids,
            'dri__post_qual'                      => $this->dri__post_qual,
            'dri__delivery_completion_acceptance' => $this->dri__delivery_completion_acceptance,
            'remarks'                             => $this->remarks,
            'status'                              => $this->status,
            'completed'                           => $this->completed,
            'created_at'                          => $this->created_at,
            'updated_at'                          => $this->updated_at
        ];
    }
}
