<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class APPResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'code' => $this->code,
            'program_project' => $this->program_project,
            'end_user' => $this->end_user->office,
            'procurement_mode' => $this->procurementMode->mode,
            'source_of_funds' => $this->source_of_funds,
        ];
    }
}
