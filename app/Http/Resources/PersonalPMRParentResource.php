<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PersonalPMRParentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'total_abc' => $this->total_abc,
            'total_contract_amt' => $this->total_contract_amt,
            'total_savings' => $this->total_savings,
            'total_ongoing_abc' => $this->total_ongoing_abc,
            'total_ongoing_abc_mooe' => $this->total_ongoing_abc_mooe,
            'total_ongoing_abc_co' => $this->total_ongoing_abc_co,
            'total_ongoing_contract_cost_mooe' => $this->total_ongoing_contract_cost_mooe,
            'total_ongoing_contract_cost_co' => $this->total_ongoing_contract_cost_co,
            'total_ongoing_contract_cost' => $this->total_ongoing_contract_cost,
            'total_completed_contract_cost' => $this->total_completed_contract_cost === null ? 0 : $this->total_completed_contract_cost,
            'total_completed_alloted_budget' => $this->total_completed_alloted_budget === null ? 0 : $this->total_completed_alloted_budget,
            'total_completed_savings' => $this->total_completed_alloted_budget - $this->total_completed_contract_cost,
            'prepared_by' => $this->prepared_by,
            'recommended_approval' => $this->recommended_approval,
            'approved_by' => $this->approved_by
        ];
    }
}
