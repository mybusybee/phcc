<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PurchaseRequest;
use App\AnnualProcurementPlan as APP;
use Auth;
use App\PurchaseRequestItem;
use App\PurchaseRequestItemTotal;
use App\PurchaseRequestBudget;
use App\ProjectProcurementManagementPlan as PPMP;
use App\User;
use App\PrProcurementUser as PPU;
use App\ProcurementMode;
use App\PrUnit;
use App\MovingApp;
use App\ProjectsProgramsActivities as PAP;
use PDF;
use App\PurchaseRequestSubItem as PRSI;
use App\PurchaseRequestAttachment as PRFile;
use Carbon\Carbon;
use PersonalPmr;
use SystemNotification;
use App\Notifications\EmailNotification;
use App\UserRole;
use App\Office;
use Excel;

class PurchaseRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->role->id == 7){
            $prs = PurchaseRequest::where([
                'office_id' => Auth::user()->office->id,
                'rc_code' => Auth::user()->role->id,
            ])->get();

        } else if(Auth::user()->role->id == 4){
            $prs = PurchaseRequest::where([
                'office_id' => Auth::user()->office->id,
            ])->get();

        } 
        // else if(Auth::user()->role->id == 6) {
        //     $prs = PurchaseRequest::where([
        //         'status' => 'PR_FOR_BUDGET_ALLOCATION',
        //     ])->get();
            
        // }
        else if(Auth::user()->role->id == 3) {
            $prs = PurchaseRequest::latest()->get();
        } 
        else if(Auth::user()->role->id == 5) {
            $prs = PurchaseRequest::where([
                'status' => 'PR_ASSIGNED',
                'proc_user_assigned' => Auth::user()->id,
            ])
            ->get();
        } else if(Auth::user()->role->id == 1 || Auth::user()->role->id == 6 || Auth::user()->role->id == 8){
            $prs = PurchaseRequest::all();
        }

        if($prs == null){
            return redirect()->back()->with('error', 'No available purchase requests this time.');
        } else {

            $log_arr = [
                "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
                "user_office" => Auth::user()->office->office,
                "activity"  => ' viewed PR List',
            ];
            \ActivityLog::add($log_arr);
        }

        foreach($prs as $pr){
            foreach($pr->pr_items as $pr_item){
                $pr->procurement_mode = $pr_item->app->procurementMode->mode;
                break;
            }
        }

        $roles      = UserRole::whereNotIn('id', [1,2])->get();
        $offices    = Office::whereNotIn('id', [1, 11])->get();
        $end_users  = User::where('user_role', 7)->get();
        $proc_users = User::where('user_role', 5)->get();

        return view('pr.index')->with('prs', $prs)
                               ->with('roles', $roles)
                               ->with('offices', $offices)
                               ->with('end_users', $end_users)
                               ->with('proc_users', $proc_users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->user_role === 7 || Auth::user()->user_role === 1){

            $prs           = PurchaseRequest::all();
            $item_no_count = PurchaseRequestItem::all();
            $units = PrUnit::all();
            $apps = APP::where('pmo_end_user', Auth::user()->office_id)->get();
            $apps = $this->getMovingApp($apps);

            return view('pr.create')->with('pr_count', count($prs))
                                    ->with('item_no_count', count($item_no_count))
                                    ->with('apps', $apps)
                                    ->with('units', $units);
        }else{
            return "You are not allowed to view this page!";
        }
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'pr_no.unique' => 'The Purchase Request # has already been taken. Please refresh the page to obtain a new number.',
            'pr_item_row_counter.required' => 'A purchase request item is required.'
        ];

        $request->validate(
            [
                'pr_no' => 'unique:purchase_requests',
                'pr_item_row_counter' => 'required'
            ], $messages
        );

        $pr                      = new PurchaseRequest;
        $pr->office_id           = Auth::user()->office->id;
        $pr->rc_code             = Auth::user()->role->id;
        $pr->pr_no               = $request->pr_no;
        $pr->pr_prep_date        = $request->pr_date;
        $pr->purpose             = $request->purpose;
        $pr->is_included_in_app  = $request->in_app    == 1 ? 1 : 0;
        $pr->is_pm_dbm_available = $request->ps_dbm_confirm == 1 ? 1 : 0;
        $pr->requested_by        = Auth::id();
        $pr->approved_by         = $request->approved_by;
        $pr->status              = 'PR_FOR_DIRECTOR_APPROVAL';
        $pr->save();

        if($pr){
            for($i = 1; $i <= count($request->pr_item_row_counter); $i++){
                $y = $i - 1;

                $pr_item = new PurchaseRequestItem;
                $pr_item->pr_id = $pr->id;
                $pr_item->app_id = $request->input('pr_item_row_counter')[$y];
                $pr_item->item_no = $request->input('item_no_'.$i);
                $pr_item->unit = $request->input('unit_no_h_'.$i);
                $pr_item->description = $request->input('description_'.$i);

                if ($request->input('quantity_'.$i) !== null) {
                    $pr_item->quantity = $request->input('quantity_'.$i);
                } else {
                    $pr_item->quantity = 1;
                }
                if ($request->input('unit_cost_'.$i) !== null) {
                    $pr_item->est_cost_unit = (float) str_replace(',', '', $request->input('unit_cost_'.$i));
                } else {
                    $pr_item->est_cost_unit = " ";
                }
                $pr_item->est_cost_total = str_replace(',', '', $request->input('total_cost_'.$i));
                $pr_item->save();

                $sub_item_total = 0;

                if($request->input('unitSub'.$i) !== null){
                    for($x = 0; $x < count($request->input('unitSub'.$i)); $x++){
                        $prSubItem = new PRSI;
                        $prSubItem->pr_item_id = $pr_item->id;
                        $prSubItem->item_no = $request->input('itemNoSub'.$i)[$x];
                        $prSubItem->unit = $request->input('unitSub'.$i)[$x];
                        $prSubItem->description = $request->input('descriptionSub'.$i)[$x];
                        $prSubItem->quantity = $request->input('quantitySub'.$i)[$x];
                        $prSubItem->est_cost_unit = (float) str_replace(',', '', $request->input('unitCostSub'.$i)[$x]);
                        $prSubItem->est_cost_total = $request->input('totalCostSub'.$i)[$x];
                        $prSubItem->save();
                        $sub_item_total += str_replace(',', '', $prSubItem->est_cost_total);
                    }
                }

                if ($sub_item_total != 0){
                    $pr_item->est_cost_total = $sub_item_total;
                    $pr_item->save();
                }

                //this is for moving app
                $moving_app_item = MovingApp::where('pap_id', $pr_item->app->pap_id)->first();
                if (!$moving_app_item){
                    $moving_app_item = new MovingApp;
                    $moving_app_item->pap_id = $pr_item->app->pap_id;
                    $moving_app_item->actual_budget = (float)str_replace(',', '', $pr_item->app->pap->estimated_budget) - (float)$pr_item->est_cost_total;
                    $moving_app_item->save();
                }else{
                    (float)$moving_app_item->actual_budget -= (float)$pr_item->est_cost_total;
                    $moving_app_item->save();
                }
                
            }

            $pr_item_total                       = new PurchaseRequestItemTotal;
            $pr_item_total->is_lot_purchase      = $request->purchase_type == 'lot' ? 1 : 0;
            $pr_item_total->is_per_item_purchase = $request->purchase_type == 'per' ? 1 : 0;
            $pr_item_total->total_estimate       = $request->total_estimate;
            $pr->pr_item_total()->save($pr_item_total);
            

            $pr_budget               = new PurchaseRequestBudget;
            $pr->pr_budget()->save($pr_budget);

            if($request->hasFile('pr_requirements')){
                foreach($request->pr_requirements as $file){
                    $filepath = $file->store('pr_file_attachments/'.$pr->pr_no);
                    $attachments[] = new PRFile([
                        'filename' => $file->getClientOriginalName(),
                        'filepath' => $filepath
                    ]);
                }
                $pr->files()->saveMany($attachments);
            }
        }

        $log_arr = [
            "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            "user_office" => Auth::user()->office->office,
            "activity"  => ' created PR#' . $pr->pr_no,
        ];
        \ActivityLog::add($log_arr);

        $officeDirectors = User::where([
            'user_role' => 4,
            'office_id' => $pr->office_id
        ])->get();
        foreach($officeDirectors as $officeDirector) {
            $obj = new \stdClass();
            $obj->message = "A PR is waiting for your approval.";
            $obj->link = url('/pr/'.$pr->id);
            SystemNotification::add($officeDirector, $obj, 'pr', $pr);
            $officeDirector->notify(new EmailNotification($obj));
        }

        return redirect()->route('pr.index')->with('success', 'Purchase Request created successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pr = PurchaseRequest::find($id);
        if(!$pr){
            abort(404);
        }
        $apps = APP::where('pmo_end_user', $pr->office_id)->get();
        $apps = $this->getMovingApp($apps, "show");
        if(
            (Auth::user()->user_role === 4 && $pr->status === 'PR_FOR_DIRECTOR_APPROVAL') ||
            (Auth::user()->user_role === 4 && $pr->status === 'PR_FOR_SUBMISSION') ||
            ($pr->requested_by === Auth::id()) ||
            (Auth::id() === $pr->proc_user_assigned && $pr->status === 'PR_ASSIGNED') ||
            (Auth::user()->user_role === 3 && $pr->status === 'PR_FOR_SUBMISSION')
        ) {
            SystemNotification::checkURL();
        }
        if(Auth::user()->role->id == 3){
            $proc_admins = [];
            $proc_users = User::where([
                'user_role' => 5,
            ])->get();

            foreach($proc_users as $proc_user){
                $proc_admins[$proc_user->id] = $proc_user->first_name . ' ' . $proc_user->last_name;
            }

            $log_arr = [
                "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
                "user_office" => Auth::user()->office->office,
                "activity"  => ' viewed PR#' . $pr->pr_no,
            ];
            \ActivityLog::add($log_arr);
            
            return view('pr.show')->with('pr', $pr)->with('proc_admins', $proc_admins)->with('apps', $apps);
        } else {
            $log_arr = [
                "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
                "user_office" => Auth::user()->office->office,
                "activity"  => ' viewed PR#' . $pr->pr_no,
            ];
            \ActivityLog::add($log_arr);
            return view('pr.show')->with('pr', $pr)->with('apps', $apps);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pr = PurchaseRequest::find($id);
        if(!$pr){
            abort(404);
        }
        $pr_item_no_count = PurchaseRequestItem::where('pr_id', $id)->get();
        $units = PrUnit::all();
        $apps = APP::where('pmo_end_user', $pr->office_id)->get();
        $units_array = PrUnit::all()->pluck('unit', 'unit');

        if(
            (Auth::user()->user_role === 8 && $pr->status !== 'PR_FOR_PROCUREMENT_VALIDATION') ||
            (Auth::user()->user_role === 6 && $pr->status !== 'PR_FOR_BUDGET_ALLOCATION')
        ) {
            return redirect('/pr/'.$pr->id)->with('error', 'Invalid request.');
        }

        $apps = $this->getMovingApp($apps, "show");

        if(
            (Auth::user()->user_role === 8 && $pr->status === 'PR_FOR_PROCUREMENT_VALIDATION') ||
            (Auth::user()->user_role === 6 && $pr->status === 'PR_FOR_BUDGET_ALLOCATION') ||
            ($pr->requested_by === Auth::id() && $pr->status === 'PR_FOR_REVISION')
        ) {
            SystemNotification::checkURL();
        }

        return view('pr.edit')
        ->with('pr', $pr)
        ->with('pr_item_no_count', count($pr_item_no_count))
        ->with('units', $units)
        ->with('apps', $apps)
        ->with('units_array', $units_array);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_roles = [7,8];
        if(in_array(Auth::user()->user_role, $user_roles)) {
            $messages = [
                'pr_item_row_counter.required' => 'A purchase request item is required.'
            ];
    
            $request->validate([
                'pr_item_row_counter' => 'required'
            ], $messages);
        }

        $pr                      = PurchaseRequest::find($id);
        $prItems = PurchaseRequestItem::where('pr_id', $pr->id)->get();
        $pr->purpose             = $request->purpose;
        if(Auth::user()->user_role === 8){
            $pr->pr_item_total->is_lot_purchase      = $request->purchase_type == 'lot' ? 1 : 0;
            $pr->pr_item_total->is_per_item_purchase = $request->purchase_type == 'per' ? 1 : 0;
            $pr->pr_item_total->save();
            $pr->is_included_in_app  = $request->in_app    == 1 ? 1 : 0;
            $pr->is_pm_dbm_available = $request->ps_dbm_confirm == 1 ? 1 : 0;
        }
        //pr statuses here
        if(Auth::user()->user_role === 7 && $pr->status === 'PR_FOR_REVISION'){
            $pr->revision_remarks = null;
            $pr->revision_last_modifier = null;
            $pr->status = 'PR_FOR_DIRECTOR_APPROVAL';
        } else if(Auth::user()->user_role === 8 && $pr->status === 'PR_FOR_PROCUREMENT_VALIDATION'){
            $pr->status = 'PR_FOR_BUDGET_ALLOCATION';
            $notif_recipient = User::find($pr->requested_by);
            $obj = new \stdClass();
            $obj->message = Auth::user()->fullName . ' approved your PR.';
            $obj->link = url('/pr/'.$pr->id);
            SystemNotification::add($notif_recipient, $obj, 'pr', $pr);
            $notif_recipient->notify(new EmailNotification($obj));

            $budgetUsers = User::where('user_role', 6)->get();
            foreach($budgetUsers as $budgetUser) {
                $obj = new \stdClass();
                $obj->message = "A PR is waiting for your validation.";
                $obj->link = url('/pr/'.$pr->id.'/edit');
                SystemNotification::add($budgetUser, $obj, 'pr', $pr);
                $budgetUser->notify(new EmailNotification($obj));
            }
        } else if (Auth::user()->user_role === 6 && $pr->status === 'PR_FOR_BUDGET_ALLOCATION'){
            $pr->status = 'PR_FOR_SUBMISSION';

            $notif_recipient = User::find($pr->requested_by);
            $obj = new \stdClass();
            $obj->message = Auth::user()->fullName . ' validated your PR and ready for export.';
            $obj->link = url('/pr/'.$pr->id);
            SystemNotification::add($notif_recipient, $obj, 'pr', $pr);
            $notif_recipient->notify(new EmailNotification($obj));

            $adminUsers = User::where([
                'user_role' => 3,
            ])->get();
            foreach($adminUsers as $adminUser) {
                $obj = new \stdClass();
                $obj->message = "A PR is waiting for your final approval.";
                $obj->link = url('/pr/'.$pr->id);
                SystemNotification::add($adminUser, $obj, 'pr', $pr);
                $adminUser->notify(new EmailNotification($obj));
            }
        }
        $pr->save();

        if($pr){
            if(Auth::user()->user_role === 7){
                $pr->pr_items()->delete();
                if($prItems != null){
                    foreach($prItems as $i){
                        $i->delete();
                    }
                }
                for($i = 1; $i <= count($request->pr_item_row_counter); $i++){
                    $y = $i - 1;
                    $pr_item = new PurchaseRequestItem;
                    $pr_item->pr_id = $pr->id;
                    $pr_item->app_id = $request->input('pr_item_row_counter')[$y];
                    $pr_item->item_no = $request->input('item_no_'.$i);
                    $pr_item->unit = $request->input('unit_no_h_'.$i);
                    $pr_item->description = $request->input('description_'.$i);

                    if ($request->input('quantity_'.$i) !== null) {
                        $pr_item->quantity = $request->input('quantity_'.$i);
                    } else {
                        $pr_item->quantity = 1;
                    }
                    if ($request->input('unit_cost_'.$i) !== null) {
                        $pr_item->est_cost_unit = (float) str_replace(',', '', $request->input('unit_cost_'.$i));
                    } else {
                        $pr_item->est_cost_unit = " ";
                    }
                    $pr_item->est_cost_total = $request->input('total_cost_'.$i);
                    $pr_item->save();

                    $sub_item_total = 0;

                    if($request->input('unitSub'.$i) !== null){
                        $pr_item->pr_sub_items()->delete();
                        for($x = 0; $x < count($request->input('unitSub'.$i)); $x++){
                            $prSubItem = new PRSI;
                            $prSubItem->pr_item_id = $pr_item->id;
                            $prSubItem->item_no = $request->input('itemNoSub'.$i)[$x];
                            $prSubItem->unit = $request->input('unitSub'.$i)[$x];
                            $prSubItem->description = $request->input('descriptionSub'.$i)[$x];
                            $prSubItem->quantity = $request->input('quantitySub'.$i)[$x];
                            $prSubItem->est_cost_unit = (float) str_replace(',', '', $request->input('unitCostSub'.$i)[$x]);
                            $prSubItem->est_cost_total = $request->input('totalCostSub'.$i)[$x];
                            $prSubItem->save();
                            $sub_item_total += str_replace(',', '', $prSubItem->est_cost_total);
                        }
                    }

                    if ($sub_item_total != 0){
                        $pr_item->est_cost_total = $sub_item_total;
                        $pr_item->save();
                    }

                    $moving_app_item = MovingApp::where('pap_id', $pr_item->app->pap_id)->first();
                    if (!$moving_app_item){
                        $moving_app_item = new MovingApp;
                        $moving_app_item->pap_id = $pr_item->app->pap_id;
                        $moving_app_item->actual_budget = (float)str_replace(',', '', $pr_item->app->pap->estimated_budget) - (float)str_replace(',', '', $pr_item->est_cost_total);
                        $moving_app_item->save();
                    }else{
                        (float)$moving_app_item->actual_budget -= (float)str_replace(',', '', $pr_item->est_cost_total);
                        $moving_app_item->save();
                    }
                    
                }
                $pr_item_total = PurchaseRequestItemTotal::where('pr_id', $pr->id)->first();
                $pr_item_total->is_lot_purchase      = $request->purchase_type == 'lot' ? 1 : 0;
                $pr_item_total->is_per_item_purchase = $request->purchase_type == 'per' ? 1 : 0;
                $pr_item_total->total_estimate       = $request->total_estimate;
                $pr_item_total->save();
            }

            if(Auth::user()->user_role === 6){
                $pr_budget               = PurchaseRequestBudget::where('pr_id', $pr->id)->first();
                $pr_budget->aa_no = $request->aa_no;
                $pr_budget->amount = $request->aa_amount;
                $pr_budget->date_from = $request->valid_from;
                $pr_budget->date_to = $request->valid_to;
                $pr_budget->current_year = $request->cy;
                $pr_budget->save();
            }

            if($request->hasFile('pr_requirements')){
                foreach($request->pr_requirements as $file){
                    $filepath = $file->store('pr_file_attachments/'.$pr->id);
                    $attachments[] = new PRFile([
                        'filename' => $file->getClientOriginalName(),
                        'filepath' => $filepath
                    ]);
                }
                $pr->files()->saveMany($attachments);
            }
        }

        if(Auth::user()->user_role == 4) {
            $log_arr = [
                "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
                "user_office" => Auth::user()->office->office,
                "activity"  => ' updated and approved PR#' . $pr->pr_no,
            ];
            \ActivityLog::add($log_arr);

            return redirect('/pr/'.$pr->id)->with('success', 'Purchase Request updated successfully!');
        } else if(Auth::user()->user_role === 7) {
            $log_arr = [
                "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
                "user_office" => Auth::user()->office->office,
                "activity"  => ' updated PR#' . $pr->pr_no,
            ];
            \ActivityLog::add($log_arr);

            return redirect('/pr/'.$pr->id)->with('success', 'Purchase Request updated successfully!');
        } else if(Auth::user()->user_role === 8) {

            $log_arr = [
                "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
                "user_office" => Auth::user()->office->office,
                "activity"  => ' validated PR#' . $pr->pr_no,
            ];
            \ActivityLog::add($log_arr);

            return redirect('/pr')->with('success', 'Purchase Request validated!');
        } else if(Auth::user()->user_role === 6) {

            $log_arr = [
                "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
                "user_office" => Auth::user()->office->office,
                "activity"  => ' allocated PR#' . $pr->pr_no,
            ];
            \ActivityLog::add($log_arr);

            return redirect('/pr')->with('success', 'Purchase Request allocated!');
        } else {

            $log_arr = [
                "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
                "user_office" => Auth::user()->office->office,
                "activity"  => ' updated PR#' . $pr->pr_no,
            ];
            \ActivityLog::add($log_arr);

            return redirect('/pr')->with('success', 'Purchase Request updated!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * 
     * PR Approve
     * 
     */
    public function pr_approve($id){
        $pr = PurchaseRequest::find($id);
        
        if(Auth::user()->role->id == 4){
            $pr->status = 'PR_FOR_PROCUREMENT_VALIDATION';
            $notif_recipient = User::find($pr->requested_by);
            $obj = new \stdClass();
            $obj->message = Auth::user()->fullName . ' approved your PR.';
            $obj->link = url('/pr/'.$pr->id);
            SystemNotification::add($notif_recipient, $obj, 'pr', $pr);
            $notif_recipient->notify(new EmailNotification($obj));

            $bacSecUsers = User::where('user_role', 8)->get();
            foreach($bacSecUsers as $bacSecUser) {
                $obj = new \stdClass();
                $obj->message = "A PR is waiting for your validation.";
                $obj->link = url('/pr/'.$pr->id.'/edit');
                SystemNotification::add($bacSecUser, $obj, 'pr', $pr);
                $bacSecUser->notify(new EmailNotification($obj));
            }
        }

        $pr->save();

        if($pr){
            $log_arr = [
                "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
                "user_office" => Auth::user()->office->office,
                "activity"  => ' approved PR#' . $pr->pr_no,
            ];
            \ActivityLog::add($log_arr);
            return redirect()->route('pr.index')->with('success', 'Purchase Request approved!');
        }
    }
    /**
     * 
     * PR Reject
     * 
     */
    public function pr_reject(Request $request, $id){
        $pr = PurchaseRequest::find($id);
        $pr->status = 'PR_FOR_REVISION';
        $pr->revision_remarks = $request->pr_reject_remarks;
        $pr->revision_last_modifier = Auth::id();
        $pr->save();

        $notif_recipient = User::find($pr->requested_by);
        $obj = new \stdClass();
        $obj->message = Auth::user()->fullName . ' rejected your PR.';
        $obj->link = url('/pr/'.$pr->id);
        SystemNotification::add($notif_recipient, $obj, 'pr', $pr);
        $notif_recipient->notify(new EmailNotification($obj));

        $log_arr = [
            "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            "user_office" => Auth::user()->office->office,
            "activity"  => ' rejected PR#' . $pr->pr_no,
        ];
        \ActivityLog::add($log_arr);

        return redirect('/pr')->with('info', 'Purchase Request Form sent back to user with remarks.');
    }

    /**
     * 
     * Procurement Admin Assignment
     * 
     */

    public function assign_proc(Request $request, $id){

        $pr = PurchaseRequest::find($id);
        $pr->status = 'PR_ASSIGNED';
        $pr->date_assigned = Carbon::now();
        $pr->proc_user_assigned = $request->proc_admin;
        $pr->save();

        $user = User::find($request->proc_admin);

        $log_arr = [
            "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            "user_office" => Auth::user()->office->office,
            "activity"  => ' assigned ' . $user->first_name . ' ' . $user->last_name . 'to PR#' . $pr->pr_no,
        ];
        \ActivityLog::add($log_arr);

        PersonalPmr::add($pr);

        $notif_recipient = User::find($pr->proc_user_assigned);
        $obj = new \stdClass();
        $obj->message = Auth::user()->fullName . ' assigned you to PR#'.$pr->pr_no;
        $obj->link = url('/pr/'.$pr->id);
        SystemNotification::add($notif_recipient, $obj, 'pr', $pr);
        $notif_recipient->notify(new EmailNotification($obj));

        $notif_recipient = User::find($pr->requested_by);
        $obj = new \stdClass();
        $obj->message = Auth::user()->fullName . ' assigned ' . $pr->proc_user->fullName . ' to PR#'. $pr->pr_no;
        $obj->link = url('/pr/'.$pr->id);
        SystemNotification::add($notif_recipient, $obj, 'pr', $pr);
        $notif_recipient->notify(new EmailNotification($obj));


        return redirect()->route('pr.index')->with('success', 'Procurement User assigned successfully!');
        
        // $row = new PPU;
        // $row->pr_id = $id;
        // $row->procurement_user_id = $request->proc_admin;
        // $row->save();

        // if($row){
        //     $pr = PurchaseRequest::find($id);
        //     $pr->status = 'PR_ASSIGNED';
        //     $pr->save();

        //     if($pr){
        //         return redirect()->route('pr.index')->with('success', 'Procurement User assigned successfully!');
        //     }
        // }
    }

    private function getRemainingMonths(){
        $month_arr = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug' ,'Sept' ,'Oct' ,'Nov' ,'Dec'];
        $month = date('n');
        $month_res = [];
        for($i = 1; $i <= count($month_arr); $i++){
            if ($i >= $month){
                $month_res[] = $month_arr[$i-1];
            }
        }

        return $month_res;
    }

    public function download_attachment($id){
        $file = PRFile::find($id);

        return \Storage::download($file->filepath, $file->filename);
    }

    public function hardCopyReceived($id){
        $pr = PurchaseRequest::find($id);
        // $pr->date_received = Carbon::parse($request->received);
        $pr->is_hardcopy_received = true;
        $pr->save();

        $notif_recipient = User::find($pr->requested_by);
        $obj = new \stdClass();
        $obj->message = Auth::user()->fullName . ' received the hard copy of your PR.';
        $obj->link = url('/pr/'.$pr->id);
        SystemNotification::add($notif_recipient, $obj, 'pr', $pr);
        $notif_recipient->notify(new EmailNotification($obj));

        return redirect()->back()->with('success', 'Purchase Request Hardcopy Received!');
    }

    public function setDateReceived($id, Request $request){
        $pr = PurchaseRequest::find($id);
        $pr->date_received = Carbon::parse($request->received);
        $pr->save();

        return redirect()->back()->with('success', 'Date of Hardcopy Receipt set!');
    }

    public function deleteAttachment($id){
        $file = PRFile::find($id);
        \File::delete(storage_path('app/'.$file->filepath));
        $file->delete();

        return redirect()->back()->with('success', 'File attachment removed successfully!');
    }

    private function getMovingApp($apps, $method = "not show"){
            
        //this is for the budget and if the procurement line item is still viable for procurement based on it allocation schedule reference
        $apps = $apps->keyBy('id');
        $remaining_mos = $this->getRemainingMonths();
        foreach ($apps as $app){
            //get the actual budget
            $current_budget_app = MovingApp::where('pap_id', $app->pap_id)->first();
            $current_budget = 0;

            if (!$current_budget_app){
                $current_budget = $app->total_estimated_budget;
            }else{
                $current_budget = $current_budget_app->actual_budget;
            }
            $app->setAttribute('total_budget', $current_budget);

            //check if the item can still be procured based on schedule
            $is_not_expired = false;
            $app_scheds = $app->pap->pap_schedules;
            foreach ($app_scheds as $app_sched){
                if (in_array($app_sched->month, $remaining_mos)){
                    if ($app_sched->allocation != "0.00" || $app_sched->allocation != "0"){
                        $is_not_expired = true;
                    }
                }
            }
            if ($method == "not show"){
                if (!$is_not_expired || $current_budget == 0){
                    $apps->forget($app->id);
                }
            }
        }

        return $apps;
    }

    public function import(Request $request) {
        // $request->validate([
        //     'pr_no'         => 'required|unique:purchase_requests',
        //     'date'          => 'required|date',
        //     'office'        => 'required',
        //     'rcc'           => 'required',
        //     'purchase_type' => 'required',
        //     'purpose'       => 'required',
        //     'in_app'        => 'required',
        //     'in_ps_dbm'     => 'required',
        //     'requested_by'  => 'required',
        //     'approved_by'   => 'required',
        //     'aa_no'         => 'required',
        //     'valid_from'    => 'required|date',
        //     'valid_to'      => 'required|date',
        //     'cy'            => 'required',
        //     'items'         => 'required|mimes:xls,xlsx,csv'
        // ]);

        // $pr                       = new PurchaseRequest;
        // $pr->office_id            = $request->office;
        // $pr->rc_code              = $request->rcc;
        // $pr->pr_no                = $request->pr_no;
        // $pr->pr_prep_date         = $request->date;
        // $pr->purpose              = $request->purpose;
        // $pr->is_included_in_app   = $request->in_app;
        // $pr->is_pm_dbm_available  = $request->in_ps_dbm;
        // $pr->requested_by         = $request->requested_by;
        // $pr->approved_by          = $request->approved_by;
        // $pr->status               = 'PR_ASSIGNED';
        // $pr->date_assigned        = Carbon::now();
        // $pr->date_received        = Carbon::now();
        // $pr->is_hardcopy_received = 1;
        // $pr->proc_user_assigned   = $request->proc_user;
        // $pr->save();

        $path         = $request->items->getRealPath();
        $total        = new \stdClass();
        $total->total = 0;
        $pr           = new PurchaseRequest;
        $extra        = new \stdClass();
        Excel::selectSheets('main')->load($path, function($reader) use ($pr, $path, $total, $extra) {
            foreach($reader->toArray() as $row) {
                $pr->office_id            = $row['office'];
                $pr->rc_code              = $row['rcc'];
                $pr->pr_no                = $row['pr_no'];
                $pr->pr_prep_date         = $row['date'];
                $pr->purpose              = $row['purpose'];
                $pr->is_included_in_app   = strtolower($row['in_app']) === 'y' ? 1 : 0;
                $pr->is_pm_dbm_available  = strtolower($row['in_ps_dbm']) === 'y' ? 1 : 0;
                $pr->requested_by         = $row['requested_by'];
                $pr->approved_by          = $row['approved_by'];
                $pr->status               = 'PR_ASSIGNED';
                $pr->date_assigned        = $row['date_assigned'];
                $pr->date_received        = $row['date_received'];
                $pr->is_hardcopy_received = 1;
                $pr->proc_user_assigned   = $row['proc_user_assigned'];
                $pr->save();

                $extra->valid_from    = $row['valid_from'];
                $extra->valid_to      = $row['valid_to'];
                $extra->aa_no         = $row['aa_no'];
                $extra->cy            = $row['cy'];
                $extra->purchase_type = $row['purchase_type'];
                break;
            }
        });

        Excel::selectSheets('items')->load($path, function($reader) use($pr, $path, $total) {
            foreach($reader->toArray() as $row) {
                if(strtolower($row['item_type']) === 'parent') {
                    $app                  = APP::find($row['app_id']);
                    $pr_item              = new PurchaseRequestItem;
                    $pr_item->item_no     = $row['item_no'];
                    $pr_item->unit        = $row['unit'];
                    $pr_item->description = $app->program_project;
    
                    if ($row['quantity'] !== null) {
                        $pr_item->quantity = $row['quantity'];
                    } else {
                        $pr_item->quantity = 1;
                    }
                    if ($row['est_cost_unit'] !== null) {
                        $pr_item->est_cost_unit = (float) $row['est_cost_unit'];
                    } else {
                        $pr_item->est_cost_unit = " ";
                    }
                    $pr_item->est_cost_total = (float) $row['est_cost_total'];
                    $total->total += $pr_item->est_cost_total;
                    $pr_item->app()->associate($app);
                    $pr_item->pr()->associate($pr)->save();
                } else {
                    $prSubItem                 = new PRSI;
                    $prSubItem->item_no        = $row['item_no'];
                    $prSubItem->unit           = $row['unit'];
                    $prSubItem->description    = $row['description'];
                    $prSubItem->quantity       = $row['quantity'];
                    $prSubItem->est_cost_unit  = $row['est_cost_unit'];
                    $prSubItem->est_cost_total = $row['est_cost_total'];
                    $prSubItem->pr_item()->associate($pr_item)->save();
                }
            }

            $moving_app_item = MovingApp::where('pap_id', $pr_item->app->pap_id)->first();
            if (!$moving_app_item){
                $moving_app_item = new MovingApp;
                $moving_app_item->pap_id = $pr_item->app->pap_id;
                $moving_app_item->actual_budget = (float)str_replace(',', '', $pr_item->app->pap->estimated_budget) - (float)$pr_item->est_cost_total;
                $moving_app_item->save();
            } else {
                (float)$moving_app_item->actual_budget -= (float)$pr_item->est_cost_total;
                $moving_app_item->save();
            }
        });

        $pr_budget               = new PurchaseRequestBudget;
        $pr_budget->aa_no        = $extra->aa_no;
        $pr_budget->amount       = $total->total;
        $pr_budget->date_from    = $extra->valid_from;
        $pr_budget->date_to      = $extra->valid_to;
        $pr_budget->current_year = $extra->cy;
        $pr->pr_budget()->save($pr_budget);

        $pr_item_total                       = new PurchaseRequestItemTotal;
        $pr_item_total->is_lot_purchase      = $extra->purchase_type === 'lot' ? 1 : 0;
        $pr_item_total->is_per_item_purchase = $extra->purchase_type === 'per_item' ? 1 : 0;
        $pr_item_total->total_estimate       = $total->total;
        $pr->pr_item_total()->save($pr_item_total);

        PersonalPmr::add($pr);

        return redirect()->back()->with('success', 'Successfully uploaded!');
    }

    public function download_template () {
        return response()->download(storage_path('pr-items-template.xlsx'));
    }
}
