<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AnnualProcurementPlan;
use App\ProcurementMode;
use Excel;
use App\Exports;
use App\PrExport;
use App\PurchaseRequest;
use App\AbstractOfQuotation as AOQ;
use App\AbstractOfBids as AOB;
use App\AbstractOfProposal as AOP;
use File;
use App\RequestForQuotation as RFQ;
use App\RequestForProposal as RFP;
use App\Supplier;
use App\PurchaseJobOrder as PJO;
use App\Pmr;
use Auth;
use PDF;
use App\AOQExport;
use App\PmrExport;
use App\ProjectProcurementManagementPlan as PPMP;
use App\AnnualProcurementPlan as APP;
use Carbon\Carbon;
use App\InvitationToBid as ITB;
use App \ReqExpInterest as REI;
use App\PurchaseOrder as PO;
use App\AbstractParent;
use App\JobOrder as JO;
use App\Contract;
use App\PurchaseRequestItem;
use App\PurchaseRequestSubItem;
use App\Http\Controllers\API\PMRAPIController;
use App\ProcurementUserPmr as PersonalPMR;

class ExportAsController extends Controller
{
	public function export_app_csv($year){
		$filename = 'APP-'.$year.'-'.date_timestamp_get(date_create()) . '.xls';

        $log_arr = [
            "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            "user_office" => Auth::user()->office->office,
            "activity"  => ' exported APP for Year - ' . $year . '.',
        ];
        \ActivityLog::add($log_arr);

		// return (new Exports)->download($filename);
		return Excel::download(new Exports($year), $filename);
	}


    public function consolidate_pr(Request $request){

        //set filename
        $filename = 'PRS'."-". date_timestamp_get(date_create()) . ".docx";

        //use template
        $prcon = new \PhpOffice\PhpWord\TemplateProcessor(storage_path('phpwordtemplates/consolidated-pr-template.docx'));

        $prs = PurchaseRequest::findMany($request->consolidate);
        $prcon->cloneBlock('pr', count($prs));

        $pr_count = 1;
        $pr_numbers = [];
        foreach($prs as $pr){
            $prcon->setValue('pr_no#'.$pr_count, $pr->pr_no);
            $prcon->setValue('pr_prep_date#'.$pr_count, $pr->pr_prep_date);
            $prcon->setValue('mode_of_procurement#'.$pr_count, $pr->procurement_mode->mode);
            
            $prcon->cloneRow('item_no#'.$pr_count, count($pr->pr_items));
    
            $item_row = 1;
            foreach($pr->pr_items as $pr_item){
                $prcon->setValue('item_no#'.$pr_count.'#'.$item_row, $pr_item->item_no);
                $prcon->setValue('stock_no#'.$pr_count.'#'.$item_row, $pr_item->stock_no);
                $prcon->setValue('unit#'.$pr_count.'#'.$item_row, $pr_item->unit);
                $prcon->setValue('description#'.$pr_count.'#'.$item_row, $pr_item->description);
                $prcon->setValue('quantity#'.$pr_count.'#'.$item_row, $pr_item->quantity);
                $prcon->setValue('unit_cost#'.$pr_count.'#'.$item_row, number_format($pr_item->est_cost_unit, 2, '.', ','));
                $prcon->setValue('total_cost#'.$pr_count.'#'.$item_row, number_format($pr_item->est_cost_total, 2, '.', ','));
                $item_row++;
            }
            $pr_count++;
            $pr_numbers[] = $pr->pr_no;
        }

        try{
            $prcon->saveAs(storage_path($filename));
        }catch (Exception $e){
            return 'error';
        }

        $log_arr = [
            "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            "user_office" => Auth::user()->office->office,
            "activity"  => ' generated a consolidated PR of PR#s '.implode(', ', $pr_numbers).'.',
        ];
        \ActivityLog::add($log_arr);

        return response()->download(storage_path($filename))->deleteFileAfterSend(true);
    }

    public function export_ppmp($id){
        $ppmp = PPMP::find($id);
        $ppmp->dateToday = Carbon::now()->format('F d, Y h:i:s A eO');
        $filename = 'PPMP-'.date_timestamp_get(date_create());

        //return view('pmr.export')->with('pmr', $pmr);

        Excel::create($filename, function($excel) use ($ppmp) {

            $excel->sheet('Sheet 1', function($sheet) use ($ppmp) {
                
                //sheet options
                $sheet->setOrientation('landscape');
                $sheet->setAutoSize(true);

                $objDrawing = new \PHPExcel_Worksheet_Drawing;
                $objDrawing->setPath(public_path('img/export/logo.png')); //your image path
                $objDrawing->setCoordinates('A1');
                $objDrawing->setWidthAndHeight(185,100);
                $objDrawing->setWorksheet($sheet);
        
                $objDrawing = new \PHPExcel_Worksheet_Drawing;
                $objDrawing->setPath(public_path('img/export/address.png')); //your image path
                $objDrawing->setCoordinates('N1');
                $objDrawing->setWidthAndHeight(185,100);
                $objDrawing->setWorksheet($sheet);
                
                //load view
                $sheet->loadView('ppmps.export')->with('ppmp', $ppmp)->with('date');

                /**
                 * 
                 * Cell Manipulations
                 * 
                 */

                //merge form headers
                $sheet->mergeCells('A7:Q7');
                $sheet->mergeCells('A8:Q8');
                $sheet->mergeCells('A9:Q9');
                $sheet->mergeCells('A10:Q10');
                //format form header (PROJECT PROCUREMENT MANAGEMENT PLAN)
                $sheet->cell('A7', function($cell) {
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

                //variable declarations
                $total_pap_items = count($ppmp->paps);
                $pap_item_row_start = 14;
                $pap_item_row_end = $pap_item_row_start + $total_pap_items - 1;
                $total_estimated_budget_row = $pap_item_row_end + 1;
                $note_row = $pap_item_row_end + 2;
                $generated_row = $pap_item_row_end + 12;

                //merge cells of generated as of row
                $sheet->mergeCells('A'.$generated_row.':E'.$generated_row);

                //merge the cells occupied by the note
                $sheet->mergeCells('A'.$note_row.':Q'.$note_row);

                //merge beside schedule / milestones of activities
                $sheet->mergeCells('A6:E6');

                //set height and width of total estimated budget
                //$sheet->setSize('A'.$total_estimated_budget_row, 30,18);
                
            });
        })->export('xls');
    }

    public function export_app($year) {
        $app = APP::where('app_year', $year)->get();
        $app->dateToday = Carbon::now()->format('F d, Y h:i:s A eO');
        $filename = 'APP-'.date_timestamp_get(date_create());
        
        Excel::create($filename, function($excel) use ($app) {

            $excel->sheet('Sheet 1', function($sheet) use ($app) {
                
                //sheet options
                $sheet->setOrientation('landscape');
                $sheet->setAutoSize(true);
                
                //load view
                $sheet->loadView('app.export')->with('app', $app);

                $objDrawing = new \PHPExcel_Worksheet_Drawing;
                $objDrawing->setPath(public_path('img/export/logo.png')); //your image path
                $objDrawing->setCoordinates('A1');
                $objDrawing->setWidthAndHeight(250,100);
                $objDrawing->setWorksheet($sheet);
        
                $objDrawing = new \PHPExcel_Worksheet_Drawing;
                $objDrawing->setPath(public_path('img/export/address.png')); //your image path
                $objDrawing->setCoordinates('M1');
                $objDrawing->setWidthAndHeight(250,100);
                $objDrawing->setOffsetX(250);
                $objDrawing->setWorksheet($sheet);

                /**
                 * 
                 * Cell Manipulations
                 * 
                 */
                $app_item_row_start = 13;
                $total_app_items = count($app);
                $app_item_row_end = $app_item_row_start + $total_app_items - 1;
                $generated_row = $app_item_row_end + 14;

                 //merge cells for main top form header
                 $sheet->mergeCells('A7:M7');

                 //merge cells of 'generated as of' row
                 $sheet->mergeCells('A'.$generated_row.':C'.$generated_row);
            });
        })->export('xls');
    }

    public function export_pr($id){
        $pr = PurchaseRequest::find($id);
        $pr->dateToday = Carbon::now()->format('F d, Y h:i:s A eO');
        $filename = 'PR#'.$pr->pr_no.date_timestamp_get(date_create());
        
        Excel::create($filename, function($excel) use ($pr) {

            $excel->sheet('Sheet 1', function($sheet) use ($pr) {
                
                //sheet options
                $sheet->setOrientation('landscape');
                $sheet->setAutoSize(true);

                $sheet->setWidth([
                    'I' => 12,
                    'J' => 20,
                    'K' => 20,
                ]);

                $objDrawing = new \PHPExcel_Worksheet_Drawing;
                $objDrawing->setPath(public_path('img/export/logo.png')); //your image path
                $objDrawing->setCoordinates('A1');
                $objDrawing->setWidthAndHeight(250,100);
                $objDrawing->setWorksheet($sheet);
        
                $objDrawing = new \PHPExcel_Worksheet_Drawing;
                $objDrawing->setPath(public_path('img/export/address.png')); //your image path
                $objDrawing->setCoordinates('J1');
                $objDrawing->setWidthAndHeight(250,100);
                $objDrawing->setOffsetX(47);
                $objDrawing->setWorksheet($sheet);
                
                //load view
                $sheet->loadView('pr.export')->with('pr', $pr);

                $prItemStart = 14; //above the first item NOTE: +1 to get to the first ACTUAL ITEM
                $prLastItem = $prItemStart + count($pr->pr_items);
                $budgetSectionStart = $prLastItem + 6;

                $sheet->cell('A9', function($cell){
                    $cell->setBorder('thin','thin','none','thin');
                });
                $borderRightStart = 10;
                $allSubItems = 0;
                foreach($pr->pr_items as $item){
                    foreach($item->pr_sub_items as $subItem){
                        $allSubItems++;
                    }
                }
                $borderRightEnd = $borderRightStart + 4 + count($pr->pr_items) + $allSubItems + 23;
                for ($x = $borderRightStart; $x <= $borderRightEnd; $x++){
                    $sheet->cell('K'.$x, function($cell){
                        $cell->setBorder('none','thin','none','none');
                    });
                    $sheet->cell('A'.$x, function($cell){
                        $cell->setBorder('none','none','none','thin');
                    });
                }
            });
        })->export('xls');
    }

    public function exportITB(ITB $itb){
        $filename = 'ITB-'.$itb->invite->form_no.'-'.date_timestamp_get(date_create()) . '.docx';
        $itb_template = new \PhpOffice\PhpWord\TemplateProcessor(storage_path('phpwordtemplates/itb-template.docx'));

        $itb_template->setValue('name_of_project', $itb->name_of_project);
        $itb_template->setValue('year', $itb->year);
        $itb_template->setValue('itb_no', $itb->invite->form_no);
        $itb_template->setValue('abc', $itb->abc);
        $itb_template->setValue('description_of_goods', $itb->description_of_goods);
        $itb_template->setValue('delivery_date', $itb->delivery_date);
        $itb_template->setValue('relevant_period', $itb->relevant_period);
        $itb_template->setValue('date_of_availability', $itb->date_of_availability);
        $itb_template->setValue('bidding_documents_fee', $itb->bidding_documents_fee);
        $itb_template->setValue('prebid_conf_date', $itb->prebid_conf_date);
        $itb_template->setValue('prebid_conf_address', $itb->prebid_conf_address);
        $itb_template->setValue('bids_deadline', $itb->bids_deadline);
        $itb_template->setValue('bid_opening_datetime', $itb->bid_opening_datetime);
        $itb_template->setValue('bid_opening_address', $itb->bid_opening_address);
        $itb_template->setValue('other_info', $itb->other_info);
        $itb_template->setValue('chairperson_signatory', $itb->chairperson_signatory);
        $itb->signed === 1 ? $itb_template->setValue('sgd', '(Sgd.)') : $itb_template->setValue('sgd', '');
        $itb_template->setValue('generation_date', Carbon::now()->format('F d, Y h:i:s A eO'));
        $itb_template->saveAs(storage_path($filename));
        return response()->download(storage_path($filename))->deleteFileAfterSend(true);
    }

    public function exportREI(REI $rei){
        $filename = 'REI-'.$rei->invite->form_no.'-'.date_timestamp_get(date_create()) . '.docx';
        $rei_template = new \PhpOffice\PhpWord\TemplateProcessor(storage_path('phpwordtemplates/rei-template.docx'));

        $rei_template->setValue('project_name', $rei->project_name);
        $rei_template->setValue('year', $rei->year);
        $rei_template->setValue('rei_no', $rei->invite->form_no);
        $rei_template->setValue('abc', $rei->abc);
        $rei_template->setValue('brief_desc', $rei->brief_desc);
        $rei_template->setValue('opening_date', $rei->opening_date);
        $rei_template->setValue('availability_date', $rei->availability_date);
        $rei_template->setValue('amount_in_peso', $rei->amount_in_peso);
        $rei_template->setValue('shortlist_allowed', $rei->shortlist_allowed);
        $rei_template->setValue('shortlist_criteria', $rei->shortlist_criteria);
        if($rei->procedure === 'QCBE/QCBS'){
            $rei_template->setValue('procedure', 'Quality-Cost Based Evaluation/Selection (QCBE/QCBS)');
            $rei_template->setValue('if_qcbe', 'The Procuring Entity shall indicate the weights to be allocated for the Technical and Financial Proposals. ');
        } else if ($rei->procedure === 'QBE/QBS') {
            $rei_template->setValue('procedure', 'Quality Based Evaluation/Selection (QBE/QBS)');
            $rei_template->setValue('if_qcbe', '');
        } else if ($rei->procedure === 'FBS') {
            $rei_template->setValue('procedure', 'Fixed Budget Selection');
            $rei_template->setValue('if_qcbe', '');
        } else if ($rei->procedure === 'LCS') {
            $rei_template->setValue('procedure', 'Least-Cost Selection');
            $rei_template->setValue('if_qcbe', '');
        }
        $rei->signed === 1 ? $rei_template->setValue('sgd', '(Sgd.)') : $rei_template->setValue('sgd', '');
        $rei_template->setValue('completed_within', $rei->completed_within);
        $rei_template->setValue('chairperson_name', $rei->chairperson_name);
        $rei_template->setValue('generation_date', Carbon::now()->format('F d, Y h:i:s A eO'));

        $rei_template->saveAs(storage_path($filename));
        return response()->download(storage_path($filename))->deleteFileAfterSend(true);
    }

    public function exportRFQ(RFQ $rfq){
        $rfq->dateToday = Carbon::now()->format('F d, Y h:i:s A eO');
        $filename = 'RFQ#'.$rfq->invite->form_no.date_timestamp_get(date_create());
        
        $file = Excel::create($filename, function($excel) use ($rfq) {

            $excel->sheet('Sheet 1', function($sheet) use ($rfq) {
                //sheet options
                $sheet->setOrientation('portrait');
                $sheet->setAutoSize(true);
                //load view
                $sheet->loadView('rfq.export')->with('rfq', $rfq);
                $sheet->setWidth([
                    'A' => 14,
                    'B' => 2.71,
                    'L' => 17.71,
                    'M' => 15,
                    'N' => 17,
                ]);

                $objDrawing = new \PHPExcel_Worksheet_Drawing;
                $objDrawing->setPath(public_path('img/export/logo.png')); //your image path
                $objDrawing->setCoordinates('A1');
                $objDrawing->setWidthAndHeight(250,100);
                $objDrawing->setWorksheet($sheet);
        
                $objDrawing = new \PHPExcel_Worksheet_Drawing;
                $objDrawing->setPath(public_path('img/export/address.png')); //your image path
                $objDrawing->setCoordinates('M1');
                $objDrawing->setWidthAndHeight(250,100);
                $objDrawing->setOffsetX(-10);
                $objDrawing->setWorksheet($sheet);
            });
        })->store('xls', storage_path('exports/rfqs/'), true);
        return response()->download($file['full']);
    }

    public function exportRFP(RFP $rfp){
        $rfp->dateToday = Carbon::now()->format('F d, Y h:i:s A eO');
        $filename = 'RFP#'.$rfp->invite->form_no.date_timestamp_get(date_create());
        
        Excel::create($filename, function($excel) use ($rfp) {

            $excel->sheet('Sheet 1', function($sheet) use ($rfp) {
                //sheet options
                $sheet->setOrientation('portrait');
                $sheet->setAutoSize(true);
                //load view
                $sheet->loadView('rfp.export')->with('rfp', $rfp);
                $sheet->setWidth([
                    'A' => 14,
                    'B' => 2.71,
                    'L' => 17.71,
                    'M' => 15,
                    'N' => 17,
                ]);
                $objDrawing = new \PHPExcel_Worksheet_Drawing;
                $objDrawing->setPath(public_path('img/export/logo.png')); //your image path
                $objDrawing->setCoordinates('A1');
                $objDrawing->setWidthAndHeight(250,100);
                $objDrawing->setWorksheet($sheet);
        
                $objDrawing = new \PHPExcel_Worksheet_Drawing;
                $objDrawing->setPath(public_path('img/export/address.png')); //your image path
                $objDrawing->setCoordinates('M1');
                $objDrawing->setWidthAndHeight(250,100);
                $objDrawing->setOffsetX(-10);
                $objDrawing->setWorksheet($sheet);
            });
        })->export('xls');
    }

    public function exportAOB(AOB $aob){
        $aob->dateToday = Carbon::now()->format('F d, Y h:i:s A eO');
        $filename = 'AOB#'.$aob->abstract_parent->form_no.date_timestamp_get(date_create());

        if($aob->abstract_parent->invite->rfq){
            $aob->pr = $aob->abstract_parent->invite->rfq->pr;
        } else if ($aob->abstract_parent->invite->rfp) {
            $aob->pr = $aob->abstract_parent->invite->rfp->pr;
        } else if ($aob->abstract_parent->invite->itb){
            $aob->pr = $aob->abstract_parent->invite->itb->pr;
        } else if ($aob->abstract_parent->invite->rei){
            $aob->pr = $aob->abstract_parent->invite->rei->pr;
        }

        $aob->extra_rows = count($aob->abstract_parent->docs);

        foreach($aob->abstract_parent->docs as $doc){
            $legal = ['a1', 'a2', 'a3', 'a4', 'a9'];
            $technical = ['b1', 'b2', 'b3', 'b9'];
            $financial = ['c1', 'c2', 'c9'];
            $classb = ['d1', 'd2', 'd9'];
            $other = ['e1', 'e2', 'e3', 'e4', 'e5', 'e9'];

            if (in_array($doc->document_type, $legal)){
                $doc->doc_class = 'legal';
                
                if ($doc->document_type == "a1"){
                    $doc->document_name = "SEC Registration/DTI Registration/CDA";
                }else if ($doc->document_type == "a2"){
                    $doc->document_name = "Mayor's/Business Permit";
                }else if ($doc->document_type == "a3"){
                    $doc->document_name = "Tax Clearance";
                }else if ($doc->document_type == "a4"){
                    $doc->document_name = "PhilGEPS Certificate of Registration & Membership (Platinum)";
                }else if ($doc->document_type == "a9"){
                    $doc->document_name = $doc->extra_document;
                }
            }

            if (in_array($doc->document_type, $technical)){
                $doc->doc_class = 'technical';
                
                if ($doc->document_type == "b1"){
                    $doc->document_name = "Statement of all Ongoing Government and Private Contracts, including awarded but not yet started";
                }else if ($doc->document_type == "b2"){
                    $doc->document_name = "Statement of Single Largest Completed Contract (SLCC) similar to the contract to be bid";
                }else if ($doc->document_type == "b3"){
                    $doc->document_name = "PBAC License (Infra.)";
                }else if ($doc->document_type == "b9"){
                    $doc->document_name = $doc->extra_document;
                }
            }

            if (in_array($doc->document_type, $financial)){
                $doc->doc_class = 'financial';
                
                if ($doc->document_type == "c1"){
                    $doc->document_name = "Audited Financial Statement (AFS)";
                }else if ($doc->document_type == "c2"){
                    $doc->document_name = "Net Financial Contracting Capacity (NFCC) / Committed Line of Credit";
                }else if ($doc->document_type == "c9"){
                    $doc->document_name = $doc->extra_document;
                }
            }

            if (in_array($doc->document_type, $classb)){
                $doc->doc_class = 'classb';
                
                if ($doc->document_type == "d1"){
                    $doc->document_name = "Valid Joint Venture Agreement (JVA)";
                }else if ($doc->document_type == "d2"){
                    $doc->document_name = "N/A";
                }else if ($doc->document_type == "d9"){
                    $doc->document_name = $doc->extra_document;
                }
            }

            if (in_array($doc->document_type, $other)){
                $doc->doc_class = 'other';
                
                if ($doc->document_type == "e1"){
                    $doc->document_name = "Bid Security";
                }else if ($doc->document_type == "e2"){
                    $doc->document_name = "Omnibus Sworn Statement";
                }else if ($doc->document_type == "e3"){
                    $doc->document_name = "Technical Specifications (for Goods)";
                }else if ($doc->document_type == "e4"){
                    $doc->document_name = "Project Requirements (for Infra)";
                }else if ($doc->document_type == "e5"){
                    $doc->document_name = "Corporate Secretary Certificate / Board Resolution for Authorized Representative";
                }else if ($doc->document_type == "e9"){
                    $doc->document_name = $doc->extra_document;
                }
            }
        }

        foreach($aob->abstract_parent->recommendation->recommendation_items as $reco_items){
            $pr_item_ids = explode(",", $reco_items->pr_item_ids);

            $pr_item_nos = "";
            if($reco_items->pr_item_type === 'parent') {
                for($pr_item_ctr = 0; $pr_item_ctr < count($pr_item_ids); $pr_item_ctr++){
                    $pr_item = PurchaseRequestItem::find($pr_item_ids[$pr_item_ctr]);
    
                    if ($pr_item_ctr != (count($pr_item_ids) - 1))
                        $pr_item_nos .= $pr_item->item_no . ",";
                    else
                        $pr_item_nos .= $pr_item->item_no;
                }
            } else {
                if($aob->pr->pr_item_total->is_lot_purchase === 1) {
                    for($pr_item_ctr = 0; $pr_item_ctr < count($pr_item_ids); $pr_item_ctr++){
                        $pr_item = PurchaseRequestItem::find($pr_item_ids[$pr_item_ctr]);
        
                        if ($pr_item_ctr != (count($pr_item_ids) - 1))
                            $pr_item_nos .= $pr_item->item_no . ",";
                        else
                            $pr_item_nos .= $pr_item->item_no;
                    }
                } else {
                    for($pr_sub_item_ctr = 0; $pr_sub_item_ctr < count($pr_item_ids); $pr_sub_item_ctr++){
                        $pr_sub_item = PurchaseRequestSubItem::find($pr_item_ids[$pr_sub_item_ctr]);
        
                        if ($pr_sub_item_ctr != (count($pr_item_ids) - 1))
                            $pr_item_nos .= $pr_sub_item->item_no . ",";
                        else
                            $pr_item_nos .= $pr_sub_item->item_no;
                    }
                }
            }
            $reco_items->pr_item_nos = $pr_item_nos;
        }

        //return view('aob.export')->with('aob', $aob);

        Excel::create($filename, function($excel) use ($aob) {

            $excel->sheet('Sheet 1', function($sheet) use ($aob) {
                
                //sheet options
                $sheet->setOrientation('landscape');
                $sheet->setAutoSize(true);
                
                //load view
                $sheet->loadView('aob.export')->with('aob', $aob);

                foreach(range('A', 'D') as $column){
                    $sheet->mergeCells($column.'12:'.$column.'17');
                    $sheet->cell($column.'12', function($cell) {
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                }

                $totalItems = 0;
                foreach($aob->pr->pr_items as $item){
                    $totalItems++;
                    foreach($item->pr_sub_items as $subItem){
                        $totalItems++;
                    }
                }

                if($aob->pr->pr_item_total->is_per_item_purchase) {
                    if($this->checkIfHasSubItems($aob->pr)) {
                        $totalItems = ($totalItems * 2) - (count($aob->pr->pr_items)) - 1;
                    } else {
                        $totalItems = ($totalItems * 2) - 1;
                    }
                }

                $totalNumberOfEligibilities = 0;
                foreach($aob->abstract_parent->docs as $doc){
                    $totalNumberOfEligibilities++;
                }

                $pr_item_start_header = 17;
                $eligibility_row_start = $pr_item_start_header + $totalItems + 2;
                $eligibility_row_end = $eligibility_row_start + 4 + $totalNumberOfEligibilities;

                if($aob->pr->pr_item_total->is_lot_purchase) {
                    if(!$this->checkIfHasSubItems($aob->pr)) {
                        $eligibility_row_end--;
                    }
                }

                $sheet->mergeCells('A'.$eligibility_row_start.':A'.$eligibility_row_end);
                $sheet->cell('A'.$eligibility_row_start, function($cell) {
                    $cell->setValue('ELIGIBILITY REQUIREMENT');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });
            });
        })->export('xls');
    }
    

    public function exportAOQ(AOQ $aoq){

        $aoq->dateToday = Carbon::now()->format('F d, Y h:i:s A eO');
        $filename = 'AOQ#'.$aoq->abstract_parent->form_no.date_timestamp_get(date_create());

        if($aoq->abstract_parent->invite->rfq){
            $aoq->pr = $aoq->abstract_parent->invite->rfq->pr;
        } else if ($aoq->abstract_parent->invite->rfp) {
            $aoq->pr = $aoq->abstract_parent->invite->rfp->pr;
        } else if ($aoq->abstract_parent->invite->itb){
            $aoq->pr = $aoq->abstract_parent->invite->itb->pr;
        } else if ($aoq->abstract_parent->invite->rei){
            $aoq->pr = $aoq->abstract_parent->invite->rei->pr;
        }

        foreach($aoq->abstract_parent->docs as $doc){
            if($doc->document_type === 'f1') {
                $doc->document_type = 'Mayor\'s Business Permit';
            } else if ($doc->document_type === 'f2') {
                $doc->document_type = 'BIR Certificate of Registration';
            } else if ($doc->document_type === 'f3') {
                $doc->document_type = 'Professional License / Curriculum Vitae (Consulting Services)';
            } else if ($doc->document_type === 'f4') {
                $doc->document_type = 'PhilGEPS Registration Number';
            } else if ($doc->document_type === 'f5') {
                $doc->document_type = 'PCAB License(Infra.)';
            } else if ($doc->document_type === 'f6') {
                $doc->document_type = 'NFCC(Infra.)';
            } else if ($doc->document_type === 'f7') {
                $doc->document_type = 'Income / Business Tax Return';
            } else if ($doc->document_type === 'f8') {
                $doc->document_type = 'Notarized Omnnibus Sworn Statement';
            } else if ($doc->document_type === 'f9') {
                $doc->document_type = 'Others';
            }
        }
        foreach($aoq->abstract_parent->recommendation->recommendation_items as $reco_items){
            $pr_item_ids = explode(",", $reco_items->pr_item_ids);

            $pr_item_nos = "";
            if($reco_items->pr_item_type === 'parent') {
                for($pr_item_ctr = 0; $pr_item_ctr < count($pr_item_ids); $pr_item_ctr++){
                    $pr_item = PurchaseRequestItem::find($pr_item_ids[$pr_item_ctr]);
    
                    if ($pr_item_ctr != (count($pr_item_ids) - 1))
                        $pr_item_nos .= $pr_item->item_no . ",";
                    else
                        $pr_item_nos .= $pr_item->item_no;
                }
            } else {
                if($aoq->pr->pr_item_total->is_lot_purchase === 1) {
                    for($pr_item_ctr = 0; $pr_item_ctr < count($pr_item_ids); $pr_item_ctr++){
                        $pr_item = PurchaseRequestItem::find($pr_item_ids[$pr_item_ctr]);
        
                        if ($pr_item_ctr != (count($pr_item_ids) - 1))
                            $pr_item_nos .= $pr_item->item_no . ",";
                        else
                            $pr_item_nos .= $pr_item->item_no;
                    }
                } else {
                    for($pr_sub_item_ctr = 0; $pr_sub_item_ctr < count($pr_item_ids); $pr_sub_item_ctr++){
                        $pr_sub_item = PurchaseRequestSubItem::find($pr_item_ids[$pr_sub_item_ctr]);
        
                        if ($pr_sub_item_ctr != (count($pr_item_ids) - 1))
                            $pr_item_nos .= $pr_sub_item->item_no . ",";
                        else
                            $pr_item_nos .= $pr_sub_item->item_no;
                    }
                }
            }

            $reco_items->pr_item_nos = $pr_item_nos;
        }

        Excel::create($filename, function($excel) use ($aoq) {

            $excel->sheet('Sheet 1', function($sheet) use ($aoq) {
                
                //sheet options
                $sheet->setOrientation('landscape');
                $sheet->setAutoSize(true);
                
                //load view
                $sheet->loadView('aoq.export')->with('aoq', $aoq);

                foreach(range('A', 'D') as $column){
                    $sheet->mergeCells($column.'11:'.$column.'16');
                    $sheet->cell($column.'11', function($cell) {
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                }

                $totalItems = 0;
                foreach($aoq->pr->pr_items as $item){
                    $totalItems++;
                    foreach($item->pr_sub_items as $subItem){
                        $totalItems++;
                    }
                }

                if($aoq->pr->pr_item_total->is_per_item_purchase) {
                    if($this->checkIfHasSubItems($aoq->pr)) {
                        $totalItems = ($totalItems * 2) - (count($aoq->pr->pr_items)) - 1;
                    } else {
                        $totalItems = ($totalItems * 2) - 1;
                    }
                }

                $pr_item_start_header = 16;
                $eligibility_row_start = $pr_item_start_header + $totalItems + 2;
                $eligibility_row_end = $eligibility_row_start + count($aoq->abstract_parent->docs) - 1;

                $sheet->mergeCells('A'.$eligibility_row_start.':A'.$eligibility_row_end);
                $sheet->cell('A'.$eligibility_row_start, function($cell) {
                    $cell->setValue('ELIGIBILITY REQUIREMENT');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

            });
        })->export('xls');
    }

    public function exportAOP(AOP $aop){
        $aop->dateToday = Carbon::now()->format('F d, Y h:i:s A eO');
        $filename = 'AOP#'.$aop->abstract_parent->form_no.date_timestamp_get(date_create());

        if($aop->abstract_parent->invite->rfq){
            $aop->pr = $aop->abstract_parent->invite->rfq->pr;
        } else if ($aop->abstract_parent->invite->rfp) {
            $aop->pr = $aop->abstract_parent->invite->rfp->pr;
        } else if ($aop->abstract_parent->invite->itb){
            $aop->pr = $aop->abstract_parent->invite->itb->pr;
        } else if ($aop->abstract_parent->invite->rei){
            $aop->pr = $aop->abstract_parent->invite->rei->pr;
        }

        foreach($aop->abstract_parent->docs as $doc){
            if($doc->document_type === 'f1') {
                $doc->document_type = 'Mayor\'s Business Permit';
            } else if ($doc->document_type === 'f2') {
                $doc->document_type = 'BIR Certificate of Registration';
            } else if ($doc->document_type === 'f3') {
                $doc->document_type = 'Professional License / Curriculum Vitae (Consulting Services)';
            } else if ($doc->document_type === 'f4') {
                $doc->document_type = 'PhilGEPS Registration Number';
            } else if ($doc->document_type === 'f5') {
                $doc->document_type = 'PCAB License(Infra.)';
            } else if ($doc->document_type === 'f6') {
                $doc->document_type = 'NFCC(Infra.)';
            } else if ($doc->document_type === 'f7') {
                $doc->document_type = 'Income / Business Tax Return';
            } else if ($doc->document_type === 'f8') {
                $doc->document_type = 'Notarized Omnnibus Sworn Statement';
            } else if ($doc->document_type === 'f9') {
                $doc->document_type = 'Others';
            }
        }

        foreach($aop->abstract_parent->recommendation->recommendation_items as $reco_items){
            $pr_item_ids = explode(",", $reco_items->pr_item_ids);

            $pr_item_nos = "";
            if($reco_items->pr_item_type === 'parent') {
                for($pr_item_ctr = 0; $pr_item_ctr < count($pr_item_ids); $pr_item_ctr++){
                    $pr_item = PurchaseRequestItem::find($pr_item_ids[$pr_item_ctr]);
    
                    if ($pr_item_ctr != (count($pr_item_ids) - 1))
                        $pr_item_nos .= $pr_item->item_no . ",";
                    else
                        $pr_item_nos .= $pr_item->item_no;
                }
            } else {
                if($aop->pr->pr_item_total->is_lot_purchase === 1) {
                    for($pr_item_ctr = 0; $pr_item_ctr < count($pr_item_ids); $pr_item_ctr++){
                        $pr_item = PurchaseRequestItem::find($pr_item_ids[$pr_item_ctr]);
        
                        if ($pr_item_ctr != (count($pr_item_ids) - 1))
                            $pr_item_nos .= $pr_item->item_no . ",";
                        else
                            $pr_item_nos .= $pr_item->item_no;
                    }
                } else {
                    for($pr_sub_item_ctr = 0; $pr_sub_item_ctr < count($pr_item_ids); $pr_sub_item_ctr++){
                        $pr_sub_item = PurchaseRequestSubItem::find($pr_item_ids[$pr_sub_item_ctr]);
        
                        if ($pr_sub_item_ctr != (count($pr_item_ids) - 1))
                            $pr_item_nos .= $pr_sub_item->item_no . ",";
                        else
                            $pr_item_nos .= $pr_sub_item->item_no;
                    }
                }
            }

            $reco_items->pr_item_nos = $pr_item_nos;
        }

        Excel::create($filename, function($excel) use ($aop) {

            $excel->sheet('Sheet 1', function($sheet) use ($aop) {
                
                //sheet options
                $sheet->setOrientation('landscape');
                $sheet->setAutoSize(true);
                
                //load view
                $sheet->loadView('aop.export')->with('aop', $aop);

                foreach(range('A', 'D') as $column){
                    $sheet->mergeCells($column.'11:'.$column.'16');
                    $sheet->cell($column.'11', function($cell) {
                        $cell->setAlignment('center');
                        $cell->setValignment('center');
                    });
                }

                $totalItems = 0;
                foreach($aop->pr->pr_items as $item){
                    $totalItems++;
                    foreach($item->pr_sub_items as $subItem){
                        $totalItems++;
                    }
                }

                if($aop->pr->pr_item_total->is_per_item_purchase) {
                    if($this->checkIfHasSubItems($aop->pr)) {
                        $totalItems = ($totalItems * 2) - (count($aop->pr->pr_items)) - 1;
                    } else {
                        $totalItems = ($totalItems * 2) - 1;
                    }
                }

                $pr_item_start_header = 16;
                $eligibility_row_start = $pr_item_start_header + $totalItems + 3;
                $eligibility_row_end = $eligibility_row_start + count($aop->abstract_parent->docs) - 1;

                $sheet->mergeCells('A'.$eligibility_row_start.':A'.$eligibility_row_end);
                $sheet->cell('A'.$eligibility_row_start, function($cell) {
                    $cell->setValue('ELIGIBILITY REQUIREMENT');
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                });

            });
        })->export('xls');
    }

    public function exportPO(PO $po){
        $po->dateToday = Carbon::now()->format('F d, Y h:i:s A eO');
        $filename = 'PO#'.$po->ordersParent->form_no.date_timestamp_get(date_create());
        $po->items = $this->getRecommendedItems($po->ordersParent->abstract->id, $po->supplier->id);
        if($po->ordersParent->abstract->invite->type === 'RFQ'){
            $po->pr = $po->ordersParent->abstract->invite->rfq->pr;
        } else if ($po->ordersParent->abstract->invite->type === 'RFP') {
            $po->pr = $po->ordersParent->abstract->invite->rfp->pr;
        } else if ($po->ordersParent->abstract->invite->type === 'IB') {
            $po->pr = $po->ordersParent->abstract->invite->itb->pr;
        } else if ($po->ordersParent->abstract->invite->type === 'REI') {
            $po->pr = $po->ordersParent->abstract->invite->rei->pr;
        }

        if($po->ordersParent->abstract->type === 'AOB'){
            $po->procurement_mode = $po->ordersParent->abstract->aob->procurement_mode->mode;
        } else if($po->ordersParent->abstract->type === 'AOQ'){
            $po->procurement_mode = $po->ordersParent->abstract->aoq->procurement_mode->mode;
        } else if($po->ordersParent->abstract->type === 'AOP') {
            $po->procurement_mode = $po->ordersParent->abstract->aop->procurement_mode->mode;
        }
        Excel::create($filename, function($excel) use ($po) {
            $excel->sheet('Sheet 1', function($sheet) use ($po) {
                //sheet options
                $sheet->setOrientation('landscape');
                $sheet->setAutoSize(true);

                $objDrawing = new \PHPExcel_Worksheet_Drawing;
                $objDrawing->setPath(public_path('img/export/logo.png')); //your image path
                $objDrawing->setCoordinates('A1');
                $objDrawing->setWidthAndHeight(250,100);
                $objDrawing->setWorksheet($sheet);

                $objDrawing = new \PHPExcel_Worksheet_Drawing;
                $objDrawing->setPath(public_path('img/export/address.png')); //your image path
                $objDrawing->setCoordinates('K1');
                $objDrawing->setWidthAndHeight(250,100);
                $objDrawing->setWorksheet($sheet);

                $sheet->setWidth([
                    'A' => 7.14,
                    'B' => 8.43,
                    'C' => 10.86,
                    'D' => 11,
                    'H' => 3,
                    'I' => 16.43,
                    'J' => 3,
                    'K' => 16.43,
                    'L' => 16.43,
                ]);

                $sheet->setFontSize(10);
                
                //load view
                $sheet->loadView('orders-parent.purchase-order.export')->with('po', $po);
            });
        })->export('xls');
    }

    public function exportJO(JO $jo){
        $jo->dateToday = Carbon::now()->format('F d, Y h:i:s A eO');
        $filename = 'JO#'.$jo->ordersParent->form_no.date_timestamp_get(date_create());
        $jo->items = $this->getRecommendedItems($jo->ordersParent->abstract->id, $jo->supplier->id);
        $jo->logo = public_path('img/export/logo.png');
        $jo->address = public_path('img/export/address.png');
        if($jo->ordersParent->abstract->invite->type === 'RFQ'){
            $jo->pr = $jo->ordersParent->abstract->invite->rfq->pr;
        } else if ($jo->ordersParent->abstract->invite->type === 'RFP') {
            $jo->pr = $jo->ordersParent->abstract->invite->rfp->pr;
        } else if ($jo->ordersParent->abstract->invite->type === 'IB') {
            $jo->pr = $jo->ordersParent->abstract->invite->itb->pr;
        } else if ($jo->ordersParent->abstract->invite->type === 'REI') {
            $jo->pr = $jo->ordersParent->abstract->invite->rei->pr;
        }

        if($jo->ordersParent->abstract->type === 'AOB'){
            $jo->procurement_mode = $jo->ordersParent->abstract->aob->procurement_mode->mode;
        } else if($jo->ordersParent->abstract->type === 'AOQ'){
            $jo->procurement_mode = $jo->ordersParent->abstract->aoq->procurement_mode->mode;
        } else if($jo->ordersParent->abstract->type === 'AOP') {
            $jo->procurement_mode = $jo->ordersParent->abstract->aop->procurement_mode->mode;
        }
        Excel::create($filename, function($excel) use ($jo) {
            $excel->sheet('Sheet 1', function($sheet) use ($jo) {
                //sheet options
                $sheet->setOrientation('landscape');
                $sheet->setAutoSize(true);

                $objDrawing = new \PHPExcel_Worksheet_Drawing;
                $objDrawing->setPath(public_path('img/export/logo.png')); //your image path
                $objDrawing->setCoordinates('A1');
                $objDrawing->setWidthAndHeight(250,100);
                $objDrawing->setWorksheet($sheet);
        
                $objDrawing = new \PHPExcel_Worksheet_Drawing;
                $objDrawing->setPath(public_path('img/export/address.png')); //your image path
                $objDrawing->setCoordinates('K1');
                $objDrawing->setWidthAndHeight(250,100);
                $objDrawing->setWorksheet($sheet);

                $sheet->setWidth([
                    'A' => 7.14,
                    'B' => 8.43,
                    'C' => 10.86,
                    'D' => 11,
                    'H' => 3,
                    'I' => 16.43,
                    'J' => 3,
                    'K' => 16.43,
                    'L' => 16.43,
                ]);

                $sheet->setFontSize(10);
                
                //load view
                $sheet->loadView('orders-parent.job-order.export')->with('jo', $jo);
            });
        })->export('xls');
    }

    private function getRecommendedItems($abstractID, $supplierID){
        $abstract = AbstractParent::find($abstractID);
        $supplier = Supplier::find($supplierID);
        foreach($abstract->recommendation->recommendation_items->where('supplier_id', $supplier->id) as $recoItem){
            $prItemIDs = explode(',', $recoItem->pr_item_ids);
        }

        $abstract->invite->pr = $this->return_pr($abstract);

        $items = collect();

        if($abstract->invite->pr->pr_item_total->is_lot_purchase === 1) {
            if($this->checkIfHasSubItems($abstract->invite->pr)) {
                //attach parent item to first
                foreach($prItemIDs as $itemID) {
                    $itemParent = PurchaseRequestItem::find($itemID);
                    $itemParent['type'] = 'parent';
                    $items->push($itemParent);
                    foreach($supplier->pr_sub_items->where('pivot.abstract_id', $abstract->id) as $pr_sub_item){
                        $pr_sub_item['type'] = 'sub';
                        $items->push($pr_sub_item);
                    }
                }
            } else {
                foreach($supplier->pr_items->where('pivot.abstract_id', $abstract->id) as $pr_item){
                    $items[] = $pr_item;
                }
            }
            
        } else {
            if($this->checkIfHasSubItems($abstract->invite->pr)) {
                $itemParent = PurchaseRequestItem::find($abstract->invite->pr->pr_items->first()->id);
                $itemParent['type'] = 'parent';
                $items->push($itemParent);
                foreach($supplier->pr_sub_items->where('pivot.abstract_id', $abstract->id)->whereIn('pivot.pr_sub_item_id', $prItemIDs) as $pr_sub_item){
                    $items->push($pr_sub_item);
                }
            } else {
                foreach($supplier->pr_items->where('pivot.abstract_id', $abstract->id) as $pr_item){
                    $items->push($pr_item);
                }
            }
        }

        return $items;
    }

    private function return_pr($abstract){
        if($abstract->invite->type === 'RFQ'){
            $pr = $abstract->invite->rfq->pr;
        } else if ($abstract->invite->type === 'RFP') {
            $pr = $abstract->invite->rfp->pr;
        } else if ($abstract->invite->type === 'IB') {
            $pr = $abstract->invite->itb->pr;
        } else if ($abstract->invite->type === 'REI') {
            $pr = $abstract->invite->rei->pr;
        }

        return $pr;
    }

    private function checkIfHasSubItems($pr){
        foreach($pr->pr_items as $item) {
            if(count($item->pr_sub_items)) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function exportContract(Contract $contract){
        $filename = 'CONTRACT-'.date_timestamp_get(date_create()) . '.docx';
        $contract_template = new \PhpOffice\PhpWord\TemplateProcessor(storage_path('phpwordtemplates/contract-template.docx'));
        $contract_template->setValue('agreement_year', $contract->agreement_year);
        $contract_template->setValue('agreement_day', $contract->agreement_day);
        $contract_template->setValue('agreement_month', $contract->agreement_month);
        $contract_template->setValue('procuring_entity', $contract->procurement_entity_name);
        $contract_template->setValue('supplier_name', $contract->supplier->company_name);
        $contract_template->setValue('supplier_address', $contract->supplier->address);
        $contract_template->setValue('brief_description', $contract->brief_description);
        $contract_template->setValue('contract_price', $contract->contract_price);
        $contract_template->setValue('form_no', $contract->order->form_no);
        $contract_template->setValue('pcc_signatory', $contract->pcc_signatory);
        $contract_template->setValue('pcc_signatory_designation', $contract->pcc_signatory_designation);
        $contract_template->setValue('supplier_signatory', $contract->supplier_signatory);
        $contract_template->setValue('supplier_signatory_designation', $contract->supplier_signatory_designation);
        $contract_template->setValue('witness_1', $contract->witness_1);
        $contract_template->setValue('witness_1_designation', $contract->witness_1_designation);
        $contract_template->setValue('witness_2', $contract->witness_2);
        $contract_template->setValue('witness_2_designation', $contract->witness_2_designation);
        $contract_template->setValue('dateToday', Carbon::now()->format('F d, Y h:i:s A eO'));
        $contract_template->saveAs(storage_path($filename));
        return response()->download(storage_path($filename))->deleteFileAfterSend(true);
    }

    public function exportPersonalPMR(PersonalPmr $pmr){
        $pmr->dateToday = Carbon::now()->format('F d, Y h:i:s A eO');
        $filename = 'PMR-'.date_timestamp_get(date_create());
        $pmr = $this->restructure_pmr($pmr);
        Excel::create($filename, function($excel) use ($pmr) {
            $excel->sheet('Sheet 1', function($sheet) use ($pmr) {
                //sheet options
                $sheet->setOrientation('landscape');
                $sheet->setAutoSize(true);

                $objDrawing = new \PHPExcel_Worksheet_Drawing;
                $objDrawing->setPath(public_path('img/export/logo.png')); //your image path
                $objDrawing->setCoordinates('A1');
                $objDrawing->setWidthAndHeight(250,100);
                $objDrawing->setWorksheet($sheet);
        
                $objDrawing = new \PHPExcel_Worksheet_Drawing;
                $objDrawing->setPath(public_path('img/export/address.png')); //your image path
                $objDrawing->setCoordinates('N1');
                $objDrawing->setWidthAndHeight(250,100);
                $objDrawing->setWorksheet($sheet);
                
                //load view
                $sheet->loadView('pmr.personal.export.export')->with('pmr', $pmr);
            });
        })->export('xls');
    }

    public function exportMainPMR($semester, $year){
        $allPMRs = PersonalPmr::where([
            'year' => $year,
            'semester' => $semester
        ])->get();
        $filename = 'PMR-'.date_timestamp_get(date_create());

        $signatories = PersonalPmr::first();

        $completed = collect();
        $ongoing = collect();
        $pmrDetails = [
            'completed' => [
                'total_alloted_budget' => 0,
                'total_contract_price' => 0,
            ],
            'ongoing_total_budget' => 0
        ];
        foreach($allPMRs as $pmr) {
            foreach($pmr->pmr_items as $item){
                $program_project_array = [];
                $end_user_array = [];
                $procurement_mode_array = [];
                $source_of_funds_array = [];
                $code_array = [];
                foreach($item->pr->pr_items as $pr_item){
                    $code_array[] = $pr_item->app->code;
                    $program_project_array[] = $pr_item->app->program_project;
                    $end_user_array[] = $pr_item->app->office->office;
                    $procurement_mode_array[] = $pr_item->app->procurementMode->mode;
                    $source_of_funds_array[] = $pr_item->app->source_of_funds;
                    $item->total_app_budget += (float)str_replace(',', '', $pr_item->app->total_estimated_budget);
                }

                $item->total_contract_cost = $this->getTotalContractCost($item);
                
                if($item->completed === 1) {
                    $pmrDetails['completed']['total_contract_price'] += $item->total_contract_cost;
                    $pmrDetails['completed']['total_alloted_budget'] += (float)str_replace(',', '', $item->pr->pr_item_total->total_estimate);
                }
    
                if($item->completed === 0){
                    $pmrDetails['ongoing_total_budget'] += (float)str_replace(',', '', $item->pr->pr_item_total->total_estimate);
                    // $pmr->total_ongoing_abc_mooe += $item->abc_mooe;
                    // $pmr->total_ongoing_abc_co += $item->abc_co;
                    // $pmr->total_ongoing_contract_cost_mooe += $item->contract_cost_mooe;
                    // $pmr->total_ongoing_contract_cost_co += $item->contract_cost_co;
                    // $pmr->total_ongoing_contract_cost += $this->getTotalContractCost($item);
                }

                $item->program_projects = $program_project_array;
                $item->end_user = implode(', ', array_unique($end_user_array));
                $item->procurement_mode = implode(', ', array_unique($procurement_mode_array));
                $item->source_of_funds = implode(', ', array_unique($source_of_funds_array));
                $item->code = implode(', ', $code_array);
    
                if($item->completed === 1){
                    $completed->push($item);
                } else {
                    $ongoing->push($item);
                }
            }
        }

        $semester === 1 ? $semester = '1st' : $semester = '2nd';

        $pmrModifiedObj                       = new \stdClass();
        $pmrModifiedObj->prepared_by          = $signatories->prepared_by;
        $pmrModifiedObj->recommended_approval = $signatories->recommended_approval;
        $pmrModifiedObj->approved_by          = $signatories->approved_by;
        $pmrModifiedObj->year                 = $year;
        $pmrModifiedObj->semester             = $semester;
        $pmrModifiedObj->pmrDetails           = $pmrDetails;
        $pmrModifiedObj->completed            = $completed;
        $pmrModifiedObj->ongoing              = $ongoing;

        
        Excel::create($filename, function($excel) use ($pmrModifiedObj) {
            $excel->sheet('Sheet 1', function($sheet) use ($pmrModifiedObj) {
                //sheet options
                $sheet->setOrientation('landscape');
                $sheet->setAutoSize(true);
                
                //load view
                $sheet->loadView('pmr.main.export.export')->with('pmrModifiedObj', $pmrModifiedObj);
            });
        })->export('xls');
    }

    private function exportMainPMRFunction($pmr){

    }
    
    private function restructure_pmr($pmr) {
        foreach($pmr->pmr_items as $item){
            $program_project_array = [];
            $end_user_array = [];
            $procurement_mode_array = [];
            $source_of_funds_array = [];
            $code_array = [];
            foreach($item->pr->pr_items as $pr_item){
                $code_array[] = $pr_item->app->code;
                $program_project_array[] = $pr_item->app->program_project;
                $end_user_array[] = $pr_item->app->office->office;
                $procurement_mode_array[] = $pr_item->app->procurementMode->mode;
                $source_of_funds_array[] = $pr_item->app->source_of_funds;
                $item->total_app_budget += (float)str_replace(',', '', $pr_item->app->total_estimated_budget);
            }

            if($item->completed === 1) {
                $item->total_contract_cost += $this->getTotalContractCost($item);
                $pmr->total_completed_contract_cost += $item->total_contract_cost;
                $pmr->total_completed_alloted_budget += (float)str_replace(',', '', $item->pr->pr_item_total->total_estimate);
            }

            if($item->completed === 0){
                $pmr->total_ongoing_abc += (float)str_replace(',', '', $item->pr->pr_item_total->total_estimate);
                $pmr->total_ongoing_abc_mooe += $item->abc_mooe;
                $pmr->total_ongoing_abc_co += $item->abc_co;
                $pmr->total_ongoing_contract_cost_mooe += $item->contract_cost_mooe;
                $pmr->total_ongoing_contract_cost_co += $item->contract_cost_co;
                $pmr->total_ongoing_contract_cost += $this->getTotalContractCost($item);
            }

            
            $item->program_projects = $program_project_array;
            $item->end_user = implode(', ', array_unique($end_user_array));
            $item->procurement_mode = implode(', ', array_unique($procurement_mode_array));
            $item->source_of_funds = implode(', ', array_unique($source_of_funds_array));
            $item->code = implode(', ', $code_array);
        }

        return $pmr;
    }

    private function getTotalContractCost($item) {
        $total_contract_cost = 0;
        if($item->pr->itb) {
            if(count($item->pr->itb->invite->abstracts)){
                foreach($item->pr->itb->invite->abstracts as $abstract) {
                    $total_contract_cost += $abstract->recommendation->total;
                }
            } else {
                $total_contract_cost = 0;
            }
        } elseif($item->pr->rei) {
            if(count($item->pr->rei->invite->abstracts)){
                foreach($item->pr->rei->invite->abstracts as $abstract) {
                    $total_contract_cost += $abstract->recommendation->total;
                }
            } else {
                $total_contract_cost = 0;
            }
        } elseif(count($item->pr->rfp)) {
            foreach($item->pr->rfp as $pr_rfp) {
                if(count($pr_rfp->invite->abstracts)) {
                    foreach($pr_rfp->invite->abstracts as $invite_abstract) {
                        $total_contract_cost += $invite_abstract->recommendation->total;
                    }
                } else {
                    $total_contract_cost = 0;
                }
            }  $total_contract_cost = 0;
        } elseif(count($item->pr->rfq)) {
            foreach($item->pr->rfq as $pr_rfq) {
                if(count($pr_rfq->invite->abstracts)) {
                    foreach($pr_rfq->invite->abstracts as $invite_abstract) {
                        $total_contract_cost += $invite_abstract->recommendation->total;
                    }
                } else {
                    $total_contract_cost = 0;
                }
            }
        } else {
            $total_contract_cost = 0;
        }

        return $total_contract_cost;
    }
}