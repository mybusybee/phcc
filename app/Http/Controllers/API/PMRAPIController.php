<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ProcurementUserPmr as PersonalPmr;
use App\Http\Resources\PersonalPMRCollection;
use App\Http\Resources\PersonalPMRParentResource;
use App\Http\Resources\UploadedPMRResource;
use App\UploadedPmr;
use Auth;

class PMRAPIController extends Controller
{
    public function getPersonalPMR($semester, $year, $user_id){
        $pmr = PersonalPmr::where([
            'procurement_user_id' => $user_id,
            'year'                => $year,
            'semester'            => $semester,
        ])->first();

        $completed = collect();
        $ongoing = collect();
        foreach($pmr->pmr_items as $item){
            $program_project_array  = [];
            $end_user_array         = [];
            $procurement_mode_array = [];
            $source_of_funds_array  = [];
            $code_array             = [];
            foreach($item->pr->pr_items as $pr_item){
                $code_array            [] = $pr_item->app->code;
                $program_project_array [] = $pr_item->app->program_project;
                $end_user_array        [] = $pr_item->app->office->office;
                $procurement_mode_array[] = $pr_item->app->procurementMode->mode;
                $source_of_funds_array [] = $pr_item->app->source_of_funds;
                $item->total_app_budget  += (float)str_replace(',', '', $pr_item->app->total_estimated_budget);
            }

            $item->total_contract_cost = $this->getTotalContractCost($item);
            
            if($item->completed === 1) {
                $pmr->total_completed_contract_cost  += $item->total_contract_cost;
                $pmr->total_completed_alloted_budget += (float)str_replace(',', '', $item->pr->pr_item_total->total_estimate);
            }

            if($item->completed === 0){
                $pmr->total_ongoing_abc                += (float)str_replace(',', '', $item->pr->pr_item_total->total_estimate);
                $pmr->total_ongoing_abc_mooe           += $item->abc_mooe;
                $pmr->total_ongoing_abc_co             += $item->abc_co;
                $pmr->total_ongoing_contract_cost_mooe += $item->contract_cost_mooe;
                $pmr->total_ongoing_contract_cost_co   += $item->contract_cost_co;
                $pmr->total_ongoing_contract_cost      += $this->getTotalContractCost($item);
            }

            $item->program_projects = $program_project_array;
            $item->end_user         = implode(', ', array_unique($end_user_array));
            $item->procurement_mode = implode(', ', array_unique($procurement_mode_array));
            $item->source_of_funds  = implode(', ', array_unique($source_of_funds_array));
            $item->code             = implode(', ', $code_array);

            if($item->completed === 1){
                $completed->push($item);
            } else {
                $ongoing->push($item);
            }
        }

        return [
            'pmr'       => new PersonalPMRParentResource($pmr),
            'completed' => new PersonalPMRCollection($completed),
            'ongoing'   => new PersonalPMRCollection($ongoing)
        ];
    }

    public function getMAINPmr($semester, $year){
        $allPMRs = PersonalPmr::where([
            'year'     => $year,
            'semester' => $semester
        ])->get();

        $signatories = PersonalPmr::first();

        $completed = collect();
        $ongoing = collect();
        $pmrDetails = [
            'completed' => [
                'total_alloted_budget' => 0,
                'total_contract_price' => 0,
            ],
            'ongoing_total_budget' => 0
        ];
        foreach($allPMRs as $pmr) {
            foreach($pmr->pmr_items as $item){
                $program_project_array  = [];
                $end_user_array         = [];
                $procurement_mode_array = [];
                $source_of_funds_array  = [];
                $code_array             = [];
                foreach($item->pr->pr_items as $pr_item){
                    $code_array            [] = $pr_item->app->code;
                    $program_project_array [] = $pr_item->app->program_project;
                    $end_user_array        [] = $pr_item->app->office->office;
                    $procurement_mode_array[] = $pr_item->app->procurementMode->mode;
                    $source_of_funds_array [] = $pr_item->app->source_of_funds;
                    $item->total_app_budget  += (float)str_replace(',', '', $pr_item->app->total_estimated_budget);
                }

                $item->total_contract_cost = $this->getTotalContractCost($item);
                
                if($item->completed === 1) {
                    $pmrDetails['completed']['total_contract_price'] += $item->total_contract_cost;
                    $pmrDetails['completed']['total_alloted_budget'] += (float)str_replace(',', '', $item->pr->pr_item_total->total_estimate);
                }
    
                if($item->completed === 0){
                    $pmrDetails['ongoing_total_budget'] += (float)str_replace(',', '', $item->pr->pr_item_total->total_estimate);
                    // $pmr->total_ongoing_abc_mooe += $item->abc_mooe;
                    // $pmr->total_ongoing_abc_co += $item->abc_co;
                    // $pmr->total_ongoing_contract_cost_mooe += $item->contract_cost_mooe;
                    // $pmr->total_ongoing_contract_cost_co += $item->contract_cost_co;
                    // $pmr->total_ongoing_contract_cost += $this->getTotalContractCost($item);
                }

                $item->program_projects = $program_project_array;
                $item->end_user         = implode(', ', array_unique($end_user_array));
                $item->procurement_mode = implode(', ', array_unique($procurement_mode_array));
                $item->source_of_funds  = implode(', ', array_unique($source_of_funds_array));
                $item->code             = implode(', ', $code_array);
    
                if($item->completed === 1){
                    $completed->push($item);
                } else {
                    $ongoing->push($item);
                }
            }
        }

        $semester === 1 ? $semester = '1st' : $semester = '2nd';

        return [
            'pmr' => [
                'prepared_by'          => $signatories->prepared_by,
                'recommended_approval' => $signatories->recommended_approval,
                'approved_by'          => $signatories->approved_by,
                'year'                 => $year,
                'semester'             => $semester,
                'pmrDetails'           => $pmrDetails
            ],
            'completed' => new PersonalPMRCollection($completed),
            'ongoing'   => new PersonalPMRCollection($ongoing)
        ];
    }

    public static function getTotalContractCost($item) {
        $total_contract_cost = 0;
        if($item->pr->itb) {
            if(count($item->pr->itb->invite->abstracts)){
                foreach($item->pr->itb->invite->abstracts as $abstract) {
                    $total_contract_cost += $abstract->recommendation->total;
                }
            } else {
                $total_contract_cost = 0;
            }
        } elseif($item->pr->rei) {
            if(count($item->pr->rei->invite->abstracts)){
                foreach($item->pr->rei->invite->abstracts as $abstract) {
                    $total_contract_cost += $abstract->recommendation->total;
                }
            } else {
                $total_contract_cost = 0;
            }
        } elseif(count($item->pr->rfp)) {
            foreach($item->pr->rfp as $pr_rfp) {
                if(count($pr_rfp->invite->abstracts)) {
                    foreach($pr_rfp->invite->abstracts as $invite_abstract) {
                        $total_contract_cost += $invite_abstract->recommendation->total;
                    }
                } else {
                    $total_contract_cost = 0;
                }
            }
        } elseif(count($item->pr->rfq)) {
            foreach($item->pr->rfq as $pr_rfq) {
                if(count($pr_rfq->invite->abstracts)) {
                    foreach($pr_rfq->invite->abstracts as $invite_abstract) {
                        $total_contract_cost += $invite_abstract->recommendation->total;
                    }
                } else {
                    $total_contract_cost = 0;
                }
            }
        } else {
            $total_contract_cost = 0;
        }

        return $total_contract_cost;
    }

    public function getUploadedPMR($year, $semester) {
        $data = UploadedPmr::where(['year' => $year, 'semester' => $semester])->get();
        
        return UploadedPMRResource::collection($data);
    }
}