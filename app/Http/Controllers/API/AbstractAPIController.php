<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Invitation;
use App\Supplier;
use App\AbstractOfBids as AOB;
use App\AbstractParent;
use App\PurchaseRequestItem;
use App\RecommendationPrItemsResource as RecoItemResource;
use App\RecommendationPrItemsCollection as RecoItemCollection;

class AbstractAPIController extends Controller
{
    public function getSuppliers($id) {
        $invite = Invitation::find($id);
        
        return $invite->suppliers;
    }

    public function addNewSupplier(Request $request){
        $supplier = new Supplier;
        $supplier->authorized_personnel = $request->authorized_personnel;
        $supplier->company_name = $request->company_name;
        $supplier->address = $request->address;
        $supplier->telephone = $request->telephone;
        $supplier->email = $request->email;
        $supplier->tin_number = $request->tin_number;
        $supplier->category = $request->category;
        $supplier->mayors_permit = $request->mayors_permit;
        $supplier->philgeps_number = $request->philgeps;
        $supplier->save();

        return $supplier;
    }

    public function getRemainingSuppliers($id) {
        $suppliers = Supplier::whereDoesntHave('invitationForms', function($q) use ($id){
            $q->where('invitation_id', $id);
        })->get();

        return $suppliers;
    }

    public function getAttachedSuppliersAOB(AOB $aob){
        $suppliers = $aob->abstract_parent->suppliers;

        foreach($suppliers as $supplier){
            foreach($supplier->pr_items as $pr_item){
                $supplier->pr_item_unit_cost = $pr_item->pivot->unit_cost;
                $supplier->pr_item_total_cost = $pr_item->pivot->total;
            }
        }

        return $suppliers;
    }

    public function notAttachedAOB(AOB $aob){
        $suppliers = Supplier::whereDoesntHave('pr_items', function($q) use ($aob){
            $q->where('abstract_id', $aob->abstract_parent->id);
        })->get();

        return $suppliers;
    }

    public function getRecommendedItems(AbstractParent $abstract, Supplier $supplier){
        foreach($abstract->recommendation->recommendation_items->where('supplier_id', $supplier->id) as $recoItem){
            $prItemIDs = explode(',', $recoItem->pr_item_ids);
        }

        $abstract->invite->pr = $this->return_pr($abstract);

        $items = collect();

        if($abstract->invite->pr->pr_item_total->is_lot_purchase === 1) {
            if($this->checkIfHasSubItems($abstract->invite->pr)) {
                //attach parent item to first
                foreach($prItemIDs as $itemID) {
                    $itemParent = PurchaseRequestItem::find($itemID);
                    $itemParent['type'] = 'parent';
                    $items->push($itemParent);
                    foreach($supplier->pr_sub_items->where('pivot.abstract_id', $abstract->id) as $pr_sub_item){
                        $pr_sub_item['type'] = 'sub';
                        $items->push($pr_sub_item);
                    }
                }
            } else {
                foreach($supplier->pr_items->where('pivot.abstract_id', $abstract->id) as $pr_item){
                    $items[] = $pr_item;
                }
            }
            
        } else {
            if($this->checkIfHasSubItems($abstract->invite->pr)) {
                $itemParent = PurchaseRequestItem::find($abstract->invite->pr->pr_items->first()->id);
                $itemParent['type'] = 'parent';
                $items->push($itemParent);
                foreach($supplier->pr_sub_items->where('pivot.abstract_id', $abstract->id)->whereIn('pivot.pr_sub_item_id', $prItemIDs) as $pr_sub_item){
                    $items->push($pr_sub_item);
                }
            } else {
                foreach($supplier->pr_items->where('pivot.abstract_id', $abstract->id) as $pr_item){
                    $items->push($pr_item);
                }
            }
        }

        return $items;
    }

    private function return_pr($abstract){
        if($abstract->invite->type === 'RFQ'){
            $pr = $abstract->invite->rfq->pr;
        } else if ($abstract->invite->type === 'RFP') {
            $pr = $abstract->invite->rfp->pr;
        } else if ($abstract->invite->type === 'IB') {
            $pr = $abstract->invite->itb->pr;
        } else if ($abstract->invite->type === 'REI') {
            $pr = $abstract->invite->rei->pr;
        }

        return $pr;
    }

    private function checkIfHasSubItems($pr){
        foreach($pr->pr_items as $item) {
            if(count($item->pr_sub_items)) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function getAbstract(AbstractParent $abstract){
        $suppliersCollection = collect();
        $abstract->pr = $this->return_pr($abstract);
        foreach($abstract->suppliers as $supplier) {
            $items = collect();
            $eligibility_compliance = collect();
            $amount_totals = 0;
            $total = 0;

            foreach($supplier->supplier_eligibilities as $supplier_eligibility){
                $eligibility_compliance->push($supplier_eligibility);
                $supplier->eligibilities = $eligibility_compliance;
            }

            if($abstract->pr->pr_item_total->is_lot_purchase) {
                if($this->checkIfSubItemsExist($abstract->pr)){
                    foreach($supplier->pr_sub_items->where('pivot.abstract_id', $abstract->id) as $supplier_sub_item){
                        $items->push($supplier_sub_item);
                        $total += $supplier_sub_item->pivot->total;
                        $supplier->items = $items;
                    }
                    $amount_totals = $total;
                } else {
                    foreach($supplier->pr_items->where('pivot.abstract_id', $abstract->id) as $supplier_pr_item){
                        $items->push($supplier_pr_item);
                        $total += $supplier_pr_item->pivot->total;
                        $supplier->items = $items;
                    }
                    $amount_totals = $total;
                }
            } else {
                if($this->checkIfSubItemsExist($abstract->pr)){
                    foreach($supplier->pr_sub_items->where('pivot.abstract_id', $abstract->id) as $supplier_sub_item){
                        $items->push($supplier_sub_item);
                        $total += $supplier_sub_item->pivot->total;
                        $supplier->items = $items;
                    }
                    $amount_totals = $total;
                } else {
                    foreach($supplier->pr_items->where('pivot.abstract_id', $abstract->id) as $supplier_pr_item){
                        $items->push($supplier_pr_item);
                        $total += $supplier_pr_item->pivot->total;
                        $supplier->items = $items;
                    }
                    $amount_totals = $total;
                }
                // foreach($supplier->pr_sub_items->where('pivot.abstract_id', $abstract->id) as $supplier_sub_item){
                //     $items->push($supplier_sub_item);
                //     $total += $supplier_sub_item->pivot->total;
                //     $supplier->items = $items;
                // }
                $amount_totals = $total;
            }
            $supplier->amount_total = $amount_totals;
            $suppliersCollection->push($supplier);
        }

        return $suppliersCollection;
    }

    private function checkIfSubItemsExist($pr){
        foreach($pr->pr_items as $pr_item) {
            if(count($pr_item->pr_sub_items)) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function getAbstractSupplierUninvited(AbstractParent $abstract){
        $suppliers = Supplier::whereDoesntHave('abstracts', function($q) use ($abstract){
            $q->where('abstract_id', $abstract->id);
        })->get();

        return $suppliers;
    }

    public function showSupplier(Supplier $supplier) {
        return $supplier;
    }
}
