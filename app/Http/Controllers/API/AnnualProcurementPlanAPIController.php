<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AnnualProcurementPlan as APP;

class AnnualProcurementPlanAPIController extends Controller
{
    public function getAPP($year){
        $items = APP::where('app_year', $year)->get();

        foreach($items as $item) {
            $item->pmo_end_user        = $item->office->office;
            $item->mode_of_procurement = $item->procurementMode->mode;
            $item->category_code       = $item->pap->category_code_field->category_code;
            $item->ads                 = $item->app_schedule->advertisement;
            $item->sub                 = $item->app_schedule->submission;
            $item->noa                 = $item->app_schedule->notice_of_award;
            $item->contract            = $item->app_schedule->contract_signing;
        }

        return $items;
    }
}
