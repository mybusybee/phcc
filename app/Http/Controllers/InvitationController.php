<?php

namespace App\Http\Controllers;

use App\Invitation as Invite;
use App\InvitationAttachment as InviteFile;
use Illuminate\Http\Request;
use Auth;
use App\User;
use SystemNotification;
use Mail;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use Excel;

class InvitationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pr_exclusive_invites = collect();
        $invites = Invite::orderBy('form_no', 'desc')->get();
        if (Auth::user()->user_role === 5){
            foreach($invites as $invite){
                if ($invite->type == 'RFQ'){
                    if ($invite->rfq->pr->proc_user->id === Auth::user()->id)
                        $pr_exclusive_invites->push($invite);
                }elseif ($invite->type == 'RFP'){
                    if ($invite->rfp->pr->proc_user->id === Auth::user()->id)
                        $pr_exclusive_invites->push($invite);
                }elseif ($invite->type == 'IB'){
                    if ($invite->itb->pr->proc_user->id === Auth::user()->id)
                        $pr_exclusive_invites->push($invite);
                }elseif ($invite->type == 'REI'){
                    if ($invite->rei->pr->proc_user->id === Auth::user()->id)
                        $pr_exclusive_invites->push($invite);
                }
            }
            $invites = $pr_exclusive_invites;
        }

        foreach($invites as $invite){
            if ($invite->type == 'RFQ'){
                $invite->pr = $invite->rfq->pr;
            }elseif ($invite->type == 'RFP'){
                $invite->pr = $invite->rfp->pr;
            }elseif ($invite->type == 'IB'){
                $invite->pr = $invite->itb->pr;
            }elseif ($invite->type == 'REI'){
                $invite->pr = $invite->rei->pr;
            }
        }

        return view('invitations.index')->with('invites', $invites);
    }

    public function approve(Invite $invitation){

        $invitation->status = 'FOR_EXPORT';
        $invitation->save();

        if($invitation){
            $log_arr = [
                "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
                "user_office" => Auth::user()->office->office,
                "activity"  => ' approved ' . $invitation->type . '#: ' . $invitation->form_no,
            ];
            \ActivityLog::add($log_arr);

            $obj = new \stdClass();
            if ($invitation->type == 'RFQ'){
                $pr = $invitation->rfq->pr;
                $obj->message = Auth::user()->fullName . ' approved RFQ#'.$invitation->form_no;
                $obj->link = url('/rfq/'.$invitation->rfq->id);
            }elseif ($invitation->type == 'RFP'){
                $pr = $invitation->rfp->pr;
                $obj->message = Auth::user()->fullName . ' approved RFP#'.$invitation->form_no;
                $obj->link = url('/rfp/'.$invitation->rfp->id);
            }elseif ($invitation->type == 'IB'){
                $pr = $invitation->itb->pr;
                $obj->message = Auth::user()->fullName . ' approved IB#'.$invitation->form_no;
                $obj->link = url('/itb/'.$invitation->itb->id);
            }elseif ($invitation->type == 'REI'){
                $pr = $invitation->rei->pr;
                $obj->message = Auth::user()->fullName . ' approved REI#'.$invitation->form_no;
                $obj->link = url('/rei/'.$invitation->rei->id);
            }

            $notif_recipient = User::find($pr->proc_user_assigned);
            SystemNotification::add($notif_recipient, $obj, 'inv', $invitation);

            return redirect('/invitations')->with('success', $invitation->type . '#: ' . $invitation->form_no . ' approved!');
        }
    }

    public function reject(Request $request, Invite $invitation){
        $invitation->status = 'FOR_REVISION';
        $invitation->rejects_remarks = $request->reject_remarks;
        $invitation->save();

        if ($invitation){
            $log_arr = [
                "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
                "user_office" => Auth::user()->office->office,
                "activity"  => ' rejected ' . $invitation->type . '#: ' . $invitation->form_no,
            ];
            \ActivityLog::add($log_arr);
            
            $obj = new \stdClass();
            $obj->message = Auth::user()->fullName . ' rejected ' . $invitation->type . '#' . $invitation->form_no;
            if ($invitation->type == 'RFQ'){
                $obj->link = url('/rfq/'.$invitation->rfq->id);
                $notif_recipient = User::find($invitation->rfq->pr->proc_user_assigned);
            }elseif ($invitation->type == 'RFP'){
                $obj->link = url('/rfp/'.$invitation->rfp->id);
                $notif_recipient = User::find($invitation->rfp->pr->proc_user_assigned);
            }elseif ($invitation->type == 'IB'){
                $obj->link = url('/itb/'.$invitation->itb->id);
                $notif_recipient = User::find($invitation->itb->pr->proc_user_assigned);
            }elseif ($invitation->type == 'REI'){
                $obj->link = url('/rei/'.$invitation->rei->id);
                $notif_recipient = User::find($invitation->rei->pr->proc_user_assigned);
            }
            SystemNotification::add($notif_recipient, $obj, 'inv', $invitation);

            return redirect('/invitations')->with('success', $invitation->type . '#: ' . $invitation->form_no . ' is sent back to procurement user with remarks.');
        }
    }

    public function cancel(Invite $invitation){
        $invitation->status = 'CANCELLED';
        $invitation->save();

        if($invitation){
            $log_arr = [
                "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
                "user_office" => Auth::user()->office->office,
                "activity"  => ' cancelled ' . $invitation->type . '#: ' . $invitation->form_no,
            ];
            \ActivityLog::add($log_arr);
            return redirect('/invitations')->with('success', $invitation->type . '#: ' . $invitation->form_no . ' has been cancelled!');
        }
    }

    public function download_attachment($id){
        $file = InviteFile::find($id);

        return \Storage::download($file->filepath, $file->filename);
    }

    public function attachSupplier(Request $request){
        $invitation = Invite::find($request->invitation_id);

        $invitation->suppliers()->detach();
        if($request->submitType === 'preselect'){
            foreach($request->selectedSuppliers as $supID){
                $invitation->suppliers()->attach($supID, ['email_status' => 'EMAIL_PENDING']);
            }
        } else {
            foreach($request->selectedSuppliers as $supID){
                $invitation->suppliers()->attach($supID, ['email_status' => 'EMAIL_SENT']);
            }
            $invitation->status = 'FARMED_OUT';
            $invitation->save();
            $this->sendEmailsToSuppliers($invitation);
        }
        return redirect('/invitations')->with('success', 'Suppliers successfully selected!');
    }

    public function sendEmailsToSuppliers($invitation){
        if ($invitation->type == 'RFQ'){
            $title    = $invitation->rfq->pr->pr_no;
            $total    = $invitation->rfq->pr->pr_item_total->total_estimate;
            $deadline = $invitation->rfq->submission_date_time;
        }elseif ($invitation->type == 'RFP'){
            $title    = $invitation->rfp->pr->pr_no;
            $total    = $invitation->rfp->pr->pr_item_total->total_estimate;
            $deadline = $invitation->rfp->submission_date_time;
        }elseif ($invitation->type == 'IB'){
            $title    = $invitation->itb->name_of_project;
            $total    = $invitation->itb->pr->pr_item_total->total_estimate;
            $deadline = $invitation->itb->bids_deadline;
        }elseif ($invitation->type == 'REI'){
            $title    = $invitation->rei->project_name;
            $total    = $invitation->rei->pr->pr_item_total->total_estimate;
            $deadline = $invitation->rei->completed_within;
        }
        foreach($invitation->suppliers as $supplier) {
            foreach(explode(', ', $supplier->email) as $email) {
                Mail::send('emails.attachments', ['supplier' => $supplier->company_name, 'title' => $title, 'total' => $total, 'deadline' => $deadline], function ($message) use ($email, $invitation)
                {
                    $message->from('info@phcc.gov.ph', 'Philippine Competition Commission');
                    if ($invitation->type == 'RFQ'){
                        $subject = "Philippine Competition Commission - Request for Quotation#" . $invitation->form_no;
                        $filepath = $this->attachRFQ($invitation);
                        $message->attach($filepath);
                        Storage::delete($filepath);
                    }elseif ($invitation->type == 'RFP'){
                        $subject = "Philippine Competition Commission - Request for Proposal#" . $invitation->form_no;
                        $filepath = $this->attachRFP($invitation);
                        $message->attach($filepath);
                        Storage::delete($filepath);
                    }elseif ($invitation->type == 'IB'){
                        $subject = "Philippine Competition Commission - Invitation to Bid#" . $invitation->form_no;
                        $filepath = $this->attachIB($invitation);
                        $message->attach($filepath);
                        Storage::delete($filepath);
                    }elseif ($invitation->type == 'REI'){
                        $subject = "Philippine Competition Commission - Request for Expression of Interest#" . $invitation->form_no;
                        $filepath = $this->attachREI($invitation);
                        $message->attach($filepath);
                        Storage::delete($filepath);
                    }
                    $message->subject($subject);
                    $message->to($email);
                    if(count($invitation->files)) {
                        foreach($invitation->files as $attachment) {
                            $message->attach(storage_path('app/'.$attachment->filepath));
                        }
                    }
                });
            }
        }
    }

    public function attachRFQ($invitation) {
        $rfq = $invitation->rfq;
        $rfq->dateToday = Carbon::now()->format('F d, Y h:i:s A eO');
        $filename = 'RFQ#'.$rfq->invite->form_no.date_timestamp_get(date_create());
        
        $file = Excel::create($filename, function($excel) use ($rfq) {

            $excel->sheet('Sheet 1', function($sheet) use ($rfq) {
                //sheet options
                $sheet->setOrientation('portrait');
                $sheet->setAutoSize(true);
                //load view
                $sheet->loadView('rfq.export')->with('rfq', $rfq);
                $sheet->setWidth([
                    'A' => 14,
                    'B' => 2.71,
                    'L' => 17.71,
                    'M' => 15,
                    'N' => 17,
                ]);

                $objDrawing = new \PHPExcel_Worksheet_Drawing;
                $objDrawing->setPath(public_path('img/export/logo.png')); //your image path
                $objDrawing->setCoordinates('A1');
                $objDrawing->setWidthAndHeight(250,100);
                $objDrawing->setWorksheet($sheet);
        
                $objDrawing = new \PHPExcel_Worksheet_Drawing;
                $objDrawing->setPath(public_path('img/export/address.png')); //your image path
                $objDrawing->setCoordinates('M1');
                $objDrawing->setWidthAndHeight(250,100);
                $objDrawing->setOffsetX(-10);
                $objDrawing->setWorksheet($sheet);
            });
        })->store('xls', null, true);
        return $file['full'];
    }

    public function attachREI($invitation) {
        $rei = $invitation->rei;
        $filename = 'REI-'.$rei->invite->form_no.'-'.date_timestamp_get(date_create()) . '.docx';
        $rei_template = new \PhpOffice\PhpWord\TemplateProcessor(storage_path('phpwordtemplates/rei-template.docx'));

        $rei_template->setValue('project_name', $rei->project_name);
        $rei_template->setValue('year', $rei->year);
        $rei_template->setValue('rei_no', $rei->invite->form_no);
        $rei_template->setValue('abc', $rei->abc);
        $rei_template->setValue('brief_desc', $rei->brief_desc);
        $rei_template->setValue('opening_date', $rei->opening_date);
        $rei_template->setValue('availability_date', $rei->availability_date);
        $rei_template->setValue('amount_in_peso', $rei->amount_in_peso);
        $rei_template->setValue('shortlist_allowed', $rei->shortlist_allowed);
        $rei_template->setValue('shortlist_criteria', $rei->shortlist_criteria);
        if($rei->procedure === 'QCBE/QCBS'){
            $rei_template->setValue('procedure', 'Quality-Cost Based Evaluation/Selection (QCBE/QCBS)');
            $rei_template->setValue('if_qcbe', 'The Procuring Entity shall indicate the weights to be allocated for the Technical and Financial Proposals. ');
        } else if ($rei->procedure === 'QBE/QBS') {
            $rei_template->setValue('procedure', 'Quality Based Evaluation/Selection (QBE/QBS)');
            $rei_template->setValue('if_qcbe', '');
        } else if ($rei->procedure === 'FBS') {
            $rei_template->setValue('procedure', 'Fixed Budget Selection');
            $rei_template->setValue('if_qcbe', '');
        } else if ($rei->procedure === 'LCS') {
            $rei_template->setValue('procedure', 'Least-Cost Selection');
            $rei_template->setValue('if_qcbe', '');
        }
        $rei->signed === 1 ? $rei_template->setValue('sgd', '(Sgd.)') : $rei_template->setValue('sgd', '');
        $rei_template->setValue('completed_within', $rei->completed_within);
        $rei_template->setValue('chairperson_name', $rei->chairperson_name);
        $rei_template->setValue('generation_date', Carbon::now()->format('F d, Y h:i:s A eO'));

        $rei_template->saveAs(storage_path($filename));
        return storage_path($filename);
    }

    public function attachIB($invitation) {
        $itb = $invitation->itb;
        $filename = 'ITB-'.$itb->invite->form_no.'-'.date_timestamp_get(date_create()) . '.docx';
        $itb_template = new \PhpOffice\PhpWord\TemplateProcessor(storage_path('phpwordtemplates/itb-template.docx'));

        $itb_template->setValue('name_of_project', $itb->name_of_project);
        $itb_template->setValue('year', $itb->year);
        $itb_template->setValue('itb_no', $itb->invite->form_no);
        $itb_template->setValue('abc', $itb->abc);
        $itb_template->setValue('description_of_goods', $itb->description_of_goods);
        $itb_template->setValue('delivery_date', $itb->delivery_date);
        $itb_template->setValue('relevant_period', $itb->relevant_period);
        $itb_template->setValue('date_of_availability', $itb->date_of_availability);
        $itb_template->setValue('bidding_documents_fee', $itb->bidding_documents_fee);
        $itb_template->setValue('prebid_conf_date', $itb->prebid_conf_date);
        $itb_template->setValue('prebid_conf_address', $itb->prebid_conf_address);
        $itb_template->setValue('bids_deadline', $itb->bids_deadline);
        $itb_template->setValue('bid_opening_datetime', $itb->bid_opening_datetime);
        $itb_template->setValue('bid_opening_address', $itb->bid_opening_address);
        $itb_template->setValue('other_info', $itb->other_info);
        $itb_template->setValue('chairperson_signatory', $itb->chairperson_signatory);
        $itb->signed === 1 ? $itb_template->setValue('sgd', '(Sgd.)') : $itb_template->setValue('sgd', '');
        $itb_template->setValue('generation_date', Carbon::now()->format('F d, Y h:i:s A eO'));
        $itb_template->saveAs(storage_path($filename));
        return storage_path($filename);
    }

    public function attachRFP($invitation) {
        $rfp = $invitation->rfp;
        $rfp->dateToday = Carbon::now()->format('F d, Y h:i:s A eO');
        $filename = 'RFP#'.$rfp->invite->form_no.date_timestamp_get(date_create());
        
        $file = Excel::create($filename, function($excel) use ($rfp) {

            $excel->sheet('Sheet 1', function($sheet) use ($rfp) {
                //sheet options
                $sheet->setOrientation('portrait');
                $sheet->setAutoSize(true);
                //load view
                $sheet->loadView('rfp.export')->with('rfp', $rfp);
                $sheet->setWidth([
                    'A' => 14,
                    'B' => 2.71,
                    'L' => 17.71,
                    'M' => 15,
                    'N' => 17,
                ]);
                $objDrawing = new \PHPExcel_Worksheet_Drawing;
                $objDrawing->setPath(public_path('img/export/logo.png')); //your image path
                $objDrawing->setCoordinates('A1');
                $objDrawing->setWidthAndHeight(250,100);
                $objDrawing->setWorksheet($sheet);
        
                $objDrawing = new \PHPExcel_Worksheet_Drawing;
                $objDrawing->setPath(public_path('img/export/address.png')); //your image path
                $objDrawing->setCoordinates('M1');
                $objDrawing->setWidthAndHeight(250,100);
                $objDrawing->setOffsetX(-10);
                $objDrawing->setWorksheet($sheet);
            });
        })->store('xls', null, true);

        return $file['full'];
    }
}
