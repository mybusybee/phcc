<?php

namespace App\Http\Controllers;

use App\RequestForProposal as RFP;
use App\PurchaseRequest as PR;
use App\Invitation as Invite;
use App\InvitationAttachment as InviteFile;
use App\ProcurementMode as PM;
use App\RequestRequirement;
use App\Supplier;
use Illuminate\Http\Request;
use Auth;
use App\User;
use SystemNotification;
use App\Notifications\EmailNotification;

class RFPController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($pr_id)
    {
        $pr                = PR::find($pr_id);
        $invitationForms   = Invite::all();
        $procurement_modes = PM::where('id', '!=', 8)->get()->pluck('mode', 'id');

        return view('rfp.create')
        ->with('pr', $pr)
        ->with('invitationFormsCount', count($invitationForms))
        ->with('pms', $procurement_modes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'form_no' => 'unique:invitations',
        ]);

        $new_invite = new Invite;
        $new_invite->form_no = $request->form_no;
        $new_invite->type = "RFP";
        $new_invite->status = "FOR_REVIEW/APPROVAL";
        $new_invite->save();
        
        if($new_invite){
            $rfp                            = new RFP;
            $rfp->pr_id                     = $request->pr_id;
            $rfp->rfp_prep_date             = $request->rfp_prep_date;
            $rfp->invite_id                 = $new_invite->id;
            $rfp->submission_date_time      = $request->submission_date_time;
            $rfp->rfp_sender                = $request->rfp_sender;
            $rfp->rfp_sender_designation    = $request->rfp_sender_designation;
            $rfp->submission_type           = $request->submission_type;
            $rfp->rfp_receiver              = $request->rfp_receiver;
            $rfp->not_awarded_notification  = $request->not_awarded;
            $rfp->notes                     = $request->notes;
        }
        // if(Auth::user()->user_role == 5) {
        //     $rfp->pr->mode_of_procurement  = $request->mode_of_procurement;
        // }
        $rfp->save();

        if($rfp){
            $rfp_req                                 = new RequestRequirement;
            $rfp_req->invite_id                      = $new_invite->id;
            $rfp_req->philgeps_reg_no                = $request->philgeps ? 1 : 0;
            $rfp_req->mayors_business_permit_bir_cor = $request->business_permit ? 1 : 0;
            $rfp_req->income_business_tax_return     = $request->latest_income ? 1 : 0;
            $rfp_req->professional_license_cv        = $request->professional_license_cv ? 1 : 0;
            $rfp_req->omnibus_sworn_statement        = $request->omnibus ? 1 : 0;
            $rfp_req->signed_terms_of_reference      = $request->tor ? 1 : 0;
            $rfp_req->govt_tax_inclusive             = $request->tax_inclusive ? 1 : 0;
            $rfp_req->used_form                      = $request->used_form ? 1 : 0;
            $rfp_req->not_exceeding_abc              = $request->not_exceeding_abc ? 1 : 0;
            $rfp_req->abc_amount                     = $request->abc_amount;
            $rfp_req->award_by_lot                   = $request->by_lot ? 1 : 0;
            $rfp_req->award_by_line_item             = $request->by_line_item ? 1 : 0;
            $rfp_req->one_month_validity             = $request->validity ? 1 : 0;
            $rfp_req->delivered_to_pcc               = $request->delivery ? 1 : 0;
            $rfp_req->delivery_place                 = $request->delivery_place;
            $rfp_req->payment_terms_cb               = $request->payment_terms_cb ? 1 : 0;
            $rfp_req->payment_terms                  = $request->payment_terms;
            $rfp_req->signatory_refusal              = $request->refusal ? 1 : 0;
            $rfp_req->others                         = $request->others;
            $rfp_req->save();
        }

        //upload attachment snippet
        if($request->hasFile('invite_requirements')){
            foreach($request->invite_requirements as $file){
                $filepath = $file->store('invite_file_attachments/'.$rfp->invite->form_no);
                $attachments[] = new InviteFile([
                    'filename' => $file->getClientOriginalName(),
                    'filepath' => $filepath
                ]);
            }
            $rfp->invite->files()->saveMany($attachments);
        }

        $log_arr = [
            "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            "user_office" => Auth::user()->office->office,
            "activity"  => ' created RFP#' . $rfp->invite->form_no,
        ];
        \ActivityLog::add($log_arr);

        $pr = PR::find($request->pr_id);
        $notif_recipient = User::find($pr->requested_by);
        $obj = new \stdClass();
        $obj->message = Auth::user()->fullName . ' generated an RFQ for PR#'.$pr->pr_no;
        $obj->link = url('/pr/'.$pr->id);
        SystemNotification::add($notif_recipient, $obj, 'inv', $new_invite);
        $notif_recipient->notify(new EmailNotification($obj));

        $adminUsers = User::where('user_role', 3)->get();
        foreach($adminUsers as $adminUser) {
            $obj = new \stdClass();
            $obj->message = 'An RFP is waiting for your approval.';
            $obj->link = url('/rfp/'.$rfp->id);
            SystemNotification::add($adminUser, $obj, 'inv', $new_invite);
            $adminUser->notify(new EmailNotification($obj));
        }
        
        return redirect('/invitations')->with('success', 'RFP saved successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(RFP $rfp)
    {
        if(!$rfp) {
            abort(404);
        }
        if(
            (Auth::user()->user_role === 3 && $rfp->invite->status === 'FOR_REVIEW/APPROVAL') ||
            (Auth::user()->user_role === 5 && $rfp->invite->status === 'FOR_EXPORT')
        ) {
            SystemNotification::checkURL();
        }

        $log_arr = [
            "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            "user_office" => Auth::user()->office->office,
            "activity"  => ' viewed RFP#' . $rfp->invite->form_no,
        ];
        \ActivityLog::add($log_arr);

        $selected_suppliers = [];
        if ($rfp->invite->suppliers()->exists()) {
            foreach($rfp->invite->suppliers as $supplier){
                $selected_suppliers[] = $supplier->id;
            }
        }

        if($rfp->invite->status === 'FOR_EXPORT' && Auth::user()->user_role === 5){
            $suppliers = Supplier::all();
            return view('rfp.show')->with('rfp', $rfp)->with('suppliers', $suppliers)->with('selected_suppliers', $selected_suppliers);
        } else {
            return view('rfp.show')->with('rfp', $rfp);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(RFP $rfp)
    {
        if(!$rfp) {
            abort(404);
        }
        $suppliers = Supplier::all();

        return view('rfp.edit')->with('rfp', $rfp)->with('suppliers', $suppliers);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RFP $rfp)
    {
        $invite = Invite::find($rfp->invite->id);
        $invite->status = 'FOR_REVIEW/APPROVAL';
        $invite->rejects_remarks .= " (UPDATED)";
        $invite->save();

        $rfp->submission_date_time     = $request->submission_date_time;
        $rfp->submission_type          = $request->submission_type;
        $rfp->rfp_receiver             = $request->rfp_receiver;
        $rfp->rfp_sender               = $request->rfp_sender;
        $rfp->rfp_sender_designation   = $request->rfp_sender_designation;
        $rfp->not_awarded_notification = $request->not_awarded;
        $rfp->notes                    = $request->notes;
        $rfp->save();

        if($rfp){
            $rfp_req                                 = RequestRequirement::where('invite_id', $rfp->invite->id)->first();
            $rfp_req->omnibus_sworn_statement        = $request->omnibus ? 1 : 0;
            $rfp_req->philgeps_reg_no                = $request->philgeps ? 1 : 0;
            $rfp_req->mayors_business_permit_bir_cor = $request->business_permit ? 1 : 0;
            $rfp_req->income_business_tax_return     = $request->latest_income ? 1 : 0;
            $rfp_req->professional_license_cv        = $request->professional_license_cv ? 1 : 0;
            $rfp_req->signed_terms_of_reference      = $request->tor ? 1 : 0;
            $rfp_req->govt_tax_inclusive             = $request->tax_inclusive ? 1 : 0;
            $rfp_req->used_form                      = $request->used_form ? 1 : 0;
            $rfp_req->not_exceeding_abc              = $request->not_exceeding_abc ? 1 : 0;
            $rfp_req->abc_amount                     = $request->abc_amount;
            $rfp_req->award_by_lot                   = $request->by_lot ? 1 : 0;
            $rfp_req->award_by_line_item             = $request->by_line_item ? 1 : 0;
            $rfp_req->one_month_validity             = $request->validity ? 1 : 0;
            $rfp_req->delivered_to_pcc               = $request->delivery ? 1 : 0;
            $rfp_req->payment_terms_cb               = $request->payment_terms_cb ? 1 : 0;
            $rfp_req->payment_terms                  = $request->payment_terms;
            $rfp_req->signatory_refusal              = $request->refusal ? 1 : 0;
            $rfp_req->others                         = $request->others;
            $rfp_req->save();
        }

        $log_arr = [
            "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            "user_office" => Auth::user()->office->office,
            "activity"  => ' edited RFP#' . $rfp->rfp_no,
        ];
        \ActivityLog::add($log_arr);

        return redirect('/rfp/'.$rfp->id)->with('success', 'RFP updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function signed(RFP $rfp){
        $rfp->signed = 1;
        $rfp->save();

        $log_arr = [
            "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            "user_office" => Auth::user()->office->office,
            "activity"  => ' received the signed copy of RFP#' . $rfp->invite->form_no,
        ];
        \ActivityLog::add($log_arr);

        return redirect()->back()->with('success', 'Successfully received signed copy.');
    }
}
