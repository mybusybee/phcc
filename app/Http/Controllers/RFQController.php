<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PurchaseRequest as PR;
use App\Supplier;
use App\RequestForQuotation as RFQ;
use App\RequestRequirement;
use App\SupplierRfq;
use PDF;
use Mail;
use App\ProcurementMode as PM;
use Auth;
use App\Invitation as Invite;
use App\InvitationAttachment as InviteFile;
use App\User;
use SystemNotification;
use App\Notifications\EmailNotification;

class RFQController extends Controller
{
    /**
     * 
     * Create RFQ
     * 
     */
    public function create($id){
        $pr                = PR::find($id);
        $invitationForms   = Invite::all();
        $procurement_modes = PM::where('id', '!=', 8)->get()->pluck('mode', 'id');

        return view('rfq.create')
        ->with('pr', $pr)
        ->with('invitationFormsCount', count($invitationForms))
        ->with('pms', $procurement_modes);
    }

    /**
     *  
     * Store RFQ
     * 
     */
    public function store(Request $request){

        $request->validate([
            'form_no' => 'unique:invitations',
        ]);

        $new_invite = new Invite;
        $new_invite->form_no = $request->form_no;
        $new_invite->type = "RFQ";
        $new_invite->status = "FOR_REVIEW/APPROVAL";
        $new_invite->save();
        
        if($new_invite){
            $rfq                           = new RFQ;
            $rfq->pr_id                    = $request->pr_id;
            $rfq->rfq_prep_date            = $request->rfq_prep_date;
            $rfq->invite_id = $new_invite->id;
            $rfq->submission_date_time     = $request->submission_date_time;
            $rfq->rfq_sender = $request->rfq_sender;
            $rfq->rfq_sender_designation = $request->rfq_sender_designation;
            $rfq->submission_type = $request->submission_type;
            $rfq->rfq_receiver = $request->rfq_receiver;
            $rfq->not_awarded_notification = $request->not_awarded;
            $rfq->notes                    = $request->notes;
        }

        if(Auth::user()->user_role == 5) {
            $rfq->pr->mode_of_procurement  = $request->mode_of_procurement;
        }
        $rfq->save();

        if($rfq){
            $rfq_req                                 = new RequestRequirement;
            $rfq_req->invite_id                      = $new_invite->id;
            $rfq_req->philgeps_reg_no                = $request->philgeps ? 1 : 0;
            $rfq_req->mayors_business_permit_bir_cor = $request->business_permit ? 1 : 0;
            $rfq_req->income_business_tax_return     = $request->latest_income ? 1 : 0;
            $rfq_req->certificate_of_exclusivity            = $request->cert_of_exclusivity ? 1 : 0;
            $rfq_req->omnibus_sworn_statement        = $request->omnibus ? 1 : 0;
            $rfq_req->signed_terms_of_reference      = $request->tor ? 1 : 0;
            $rfq_req->govt_tax_inclusive             = $request->tax_inclusive ? 1 : 0;
            $rfq_req->used_form                      = $request->used_form ? 1 : 0;
            $rfq_req->not_exceeding_abc              = $request->not_exceeding_abc ? 1 : 0;
            $rfq_req->abc_amount                     = $request->abc_amount;
            $rfq_req->award_by_lot                   = $request->by_lot ? 1 : 0;
            $rfq_req->award_by_line_item             = $request->by_line_item ? 1 : 0;
            $rfq_req->one_month_validity             = $request->validity ? 1 : 0;
            $rfq_req->delivered_to_pcc               = $request->delivery ? 1 : 0;
            $rfq_req->delivery_place                 = $request->delivery_place;
            $rfq_req->payment_terms_cb               = $request->payment_terms_cb ? 1 : 0;
            $rfq_req->payment_terms                  = $request->payment_terms;
            $rfq_req->signatory_refusal              = $request->refusal ? 1 : 0;
            $rfq_req->others                         = $request->others;
            $rfq_req->save();
        }

        //upload attachment snippet
        if($request->hasFile('invite_requirements')){
            foreach($request->invite_requirements as $file){
                $filepath = $file->store('invite_file_attachments/'.$rfq->invite->form_no);
                $attachments[] = new InviteFile([
                    'filename' => $file->getClientOriginalName(),
                    'filepath' => $filepath
                ]);
            }
            $rfq->invite->files()->saveMany($attachments);
        }

        $log_arr = [
            "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            "user_office" => Auth::user()->office->office,
            "activity"  => ' created RFQ#' . $rfq->invite->form_no,
        ];
        \ActivityLog::add($log_arr);

        $pr = PR::find($request->pr_id);
        $notif_recipient = User::find($pr->requested_by);
        $obj = new \stdClass();
        $obj->message = Auth::user()->fullName . ' generated an RFQ for PR#'.$pr->pr_no;
        $obj->link = url('/pr/'.$pr->id);
        SystemNotification::add($notif_recipient, $obj, 'inv', $rfq->invite);
        $notif_recipient->notify(new EmailNotification($obj));

        $adminUsers = User::where('user_role', 3)->get();
        foreach($adminUsers as $adminUser) {
            $obj = new \stdClass();
            $obj->message = 'An RFQ is waiting for your approval.';
            $obj->link = url('/rfq/'.$rfq->id);
            SystemNotification::add($adminUser, $obj, 'inv', $rfq->invite);
            $adminUser->notify(new EmailNotification($obj));
        }
        
        return redirect('/invitations')->with('success', 'RFQ saved successfully!');
        
    }
    
    /**
     * 
     * Index
     * 
     */
    public function index(){
        $rfqs = RFQ::orderBy('created_at', 'desc')->get();

        $log_arr = [
            "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            "user_office" => Auth::user()->office->office,
            "activity"  => ' viewed RFQ List',
        ];
        \ActivityLog::add($log_arr);

        return view('rfq.index')->with('rfqs', $rfqs);
    }

    /**
     * 
     * Show
     * 
     */
    public function show($id){
        $rfq = RFQ::find($id);
        if(!$rfq) {
            abort(404);
        }
        if(
            (Auth::user()->user_role === 3 && $rfq->invite->status === 'FOR_REVIEW/APPROVAL') ||
            (Auth::user()->user_role === 5 && $rfq->invite->status === 'FOR_EXPORT')
        ) {
            SystemNotification::checkURL();
        }

        $log_arr = [
            "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            "user_office" => Auth::user()->office->office,
            "activity"  => ' viewed RFQ#' . $rfq->invite->form_no,
        ];
        \ActivityLog::add($log_arr);

        $selected_suppliers = [];
        if ($rfq->invite->suppliers()->exists()) {
            foreach($rfq->invite->suppliers as $supplier){
                $selected_suppliers[] = $supplier->id;
            }
        }

        if($rfq->invite->status === 'FOR_EXPORT' && Auth::user()->user_role === 5){
            $suppliers = Supplier::all();
            return view('rfq.show')->with('rfq', $rfq)->with('suppliers', $suppliers)->with('selected_suppliers', $selected_suppliers);
        } else {
            return view('rfq.show')->with('rfq', $rfq);
        }
    }

    public function edit($id){
        $rfq = RFQ::find($id);
        if(!$rfq) {
            abort(404);
        }
        $suppliers = Supplier::all();

        return view('rfq.edit')->with('rfq', $rfq)->with('suppliers', $suppliers);
    }

    public function update(Request $request, $id){

        $rfq                           = RFQ::find($id);
        $rfq->submission_date_time     = $request->submission_date_time;
        $rfq->submission_type          = $request->submission_type;
        $rfq->rfq_receiver             = $request->rfq_receiver;
        $rfq->rfq_sender               = $request->rfq_sender;
        $rfq->rfq_sender_designation   = $request->rfq_sender_designation;
        $rfq->not_awarded_notification = $request->not_awarded;
        $rfq->notes                    = $request->notes;
        $rfq->save();

        $invite = Invite::find($rfq->invite->id);
        $invite->status = 'FOR_REVIEW/APPROVAL';
        if($invite->rejects_remarks !== null){
            $invite->rejects_remarks .= " (UPDATED)";
        }
        $invite->save();

        if($rfq){
            $rfq_req                                 = RequestRequirement::where('invite_id', $rfq->invite->id)->first();
            $rfq_req->omnibus_sworn_statement        = $request->omnibus ? 1 : 0;
            $rfq_req->philgeps_reg_no                = $request->philgeps ? 1 : 0;
            $rfq_req->mayors_business_permit_bir_cor = $request->business_permit ? 1 : 0;
            $rfq_req->income_business_tax_return     = $request->latest_income ? 1 : 0;
            $rfq_req->certificate_of_exclusivity     = $request->cert_of_exclusivity ? 1 : 0;
            $rfq_req->signed_terms_of_reference      = $request->tor ? 1 : 0;
            $rfq_req->govt_tax_inclusive             = $request->tax_inclusive ? 1 : 0;
            $rfq_req->used_form                      = $request->used_form ? 1 : 0;
            $rfq_req->not_exceeding_abc              = $request->not_exceeding_abc ? 1 : 0;
            $rfq_req->abc_amount                     = $request->abc_amount;
            $rfq_req->award_by_lot                   = $request->by_lot ? 1 : 0;
            $rfq_req->award_by_line_item             = $request->by_line_item ? 1 : 0;
            $rfq_req->one_month_validity             = $request->validity ? 1 : 0;
            $rfq_req->delivered_to_pcc               = $request->delivery ? 1 : 0;
            $rfq_req->delivery_place                 = $request->delivery_place;
            $rfq_req->payment_terms_cb               = $request->payment_terms_cb ? 1 : 0;
            $rfq_req->payment_terms                  = $request->payment_terms;
            $rfq_req->signatory_refusal              = $request->refusal ? 1 : 0;
            $rfq_req->others                         = $request->others;
            $rfq_req->save();
        }

        $log_arr = [
            "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            "user_office" => Auth::user()->office->office,
            "activity"  => ' edited RFQ#' . $rfq->invite->form_no,
        ];
        \ActivityLog::add($log_arr);


        return redirect('/rfq/'.$rfq->id)->with('success', 'RFQ updated successfully!');
    }

    public function signed(RFQ $rfq){
        $rfq->signed = 1;
        $rfq->save();

        $log_arr = [
            "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            "user_office" => Auth::user()->office->office,
            "activity"  => ' received the signed copy of RFQ#' . $rfq->invite->form_no,
        ];
        \ActivityLog::add($log_arr);

        return redirect()->back()->with('success', 'Successfully received signed copy.');
    }
}   
