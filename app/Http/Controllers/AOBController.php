<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Invitation;
use App\AbstractOfBids as AOB;
use App\AbstractParent;
use App\ProcurementMode;
use App\AbstractRecommendation;
use App\AbstractRecoItem;
use App\AbstractDocument;
use App\AbstractSupplierEligibility;
use App\PurchaseRequestItem;
use App\PurchaseRequestSubItem;
use App\Notifications\EmailNotification;
use SystemNotification;
use Auth;
use App\User;

class AOBController extends Controller
{
    public function create(Invitation $invitation){

        $public_bidding_procs = ProcurementMode::whereIn('id', [1, 2, 3])->get();
        
        $abstract_parents = AbstractParent::all();

        if($invitation->type === 'IB'){
            $invite = $invitation->itb;
        } else if ($invitation->type === 'REI') {
            $invite = $invitation->rei;
        } else if ($invitation->type === 'RFQ') {
            $invite = $invitation->rfq;
        } else if ($invitation->type === 'RFP') {
            $invite = $invitation->rfp;
        }

        $supplier_array = [];
        foreach($invitation->suppliers as $supplier) {
            $supplier_array[$supplier->id] = $supplier->company_name;
        }
        
        return view('aob.create')->with('invitation', $invite)
                                 ->with('abstract_type', 'aob')
                                 ->with('supplier_array', $supplier_array)
                                 ->with('abstract_parent_count', count($abstract_parents))
                                 ->with('public_bidding_procs', $public_bidding_procs);
    }

    public function store(Request $request){

        // dd($request->input("reco_item_no_1"));
        // exit;

        $request->validate([
            'form_no' => 'required|unique:abstracts'
        ]);
        
        //create abstract parent
        $abstract = new AbstractParent;
        $abstract->form_no  = $request->form_no;
        $abstract->invite_id = $request->invite_id;
        $abstract->type = 'AOB';
        $abstract->status = 'SAVED';

        $aob = new AOB;

        //get id form pb_type because mode of procurement dropdown is disabled
        $aob->procurement_mode_id = $request->pb_type;
        $aob->bid_type = $request->bid_type;

        if ($request->pb_type == '1'){
            $aob->pb_type = "For Infrastructure Projects";
        }else if($request->pb_type == '2'){
            $aob->pb_type = "For Goods and Services";
        }else if($request->pb_type == '3'){
            $aob->pb_type = "For Consulting Services";
        }
        
        $aob->pbac_chairperson = $request->pbac_chairperson;
        $aob->pbac_vice_chairperson = $request->pbac_vice_chairperson;
        $aob->pbac_member_1 = $request->pbac_member_1;
        $aob->pbac_member_2 = $request->pbac_member_2;
        $aob->provisional_member = $request->provisional_member;
        $aob->coa_representative = $request->coa_representative;
        $aob->private_observer_1 = $request->private_observer_1;
        $aob->private_observer_2 = $request->private_observer_2;
        $aob->twg_chairman = $request->twg_chairman;
        $aob->twg_member_1 = $request->twg_member_1;
        $aob->twg_member_2 = $request->twg_member_2;
        $aob->opening_date = $request->opening_date;
        $aob->remarks = $request->remarks;

        $abstract->save();
        $abstract->aob()->save($aob);

        $purchase_type = "";
        if ($abstract->invite->type == 'REI'){
            if($abstract->invite->rei->pr->pr_item_total->is_lot_purchase){
                $purchase_type = "per lot";
            }else if ($abstract->invite->rei->pr->pr_item_total->is_per_item_purchase){
                $purchase_type = "per item";
            }
            $pr_items = $abstract->invite->rei->pr->pr_items;
        }else if ($abstract->invite->type == 'RFQ'){
            if($abstract->invite->rfq->pr->pr_item_total->is_lot_purchase){
                $purchase_type = "per lot";
            }else if ($abstract->invite->rfq->pr->pr_item_total->is_per_item_purchase){
                $purchase_type = "per item";
            }
            $pr_items = $abstract->invite->rfq->pr->pr_items;
        }else if ($abstract->invite->type == 'RFP'){
            if($abstract->invite->rfp->pr->pr_item_total->is_lot_purchase){
                $purchase_type = "per lot";
            }else if ($abstract->invite->rfp->pr->pr_item_total->is_per_item_purchase){
                $purchase_type = "per item";
            }
            $pr_items = $abstract->invite->rfp->pr->pr_items;
        }else if ($abstract->invite->type == 'IB'){
            if($abstract->invite->itb->pr->pr_item_total->is_lot_purchase){
                $purchase_type = "per lot";
            }else if ($abstract->invite->itb->pr->pr_item_total->is_per_item_purchase){
                $purchase_type = "per item";
            }
            $pr_items = $abstract->invite->itb->pr->pr_items;
        }

        $supplier_ctr = 0;
        foreach($request->supplierList as $supplier){
            if ($purchase_type == "per lot") {
                $abstract->suppliers()->attach($supplier, [
                    'ranking'       => $request->input("supplier_ranking".$supplier),
                    'compliance'    => $request->input("result_compliance_supplier_id_".$supplier),
                ]);

                //store unit cost, total cost, and per item ranking
                foreach($pr_items as $pr_item){
                    // if pr item doesnt have sub item
                    if(!count($pr_item->pr_sub_items)){
                        $pr_item->suppliers()->attach($supplier,[
                            'abstract_id' => $abstract->id,
                            'unit_cost'   => $request->input('unit_cost_supplier_'.$supplier.'_pr_item_'.$pr_item->id),
                            'total'       => $request->input('total_cost_supplier_'.$supplier.'_pr_item_'.$pr_item->id)
                        ]);
                    } else {
                        foreach($pr_item->pr_sub_items as $subItem){
                            $subItem->suppliers()->attach($supplier, [
                                'abstract_id' => $abstract->id,
                                'unit_cost' => $request->input('sub_unit_cost_supplier_'.$supplier.'_pr_sub_item_'.$subItem->id),
                                'total'       => $request->input('sub_total_cost_supplier_'.$supplier.'_pr_sub_item_'.$subItem->id)
                            ]);
                        }
                    }
                }
            } else if ($purchase_type == "per item") {
                $abstract->suppliers()->attach($supplier, [
                    'compliance'    => $request->input("result_compliance_supplier_id_".$supplier),
                ]);

                //store unit cost, total cost, and per item ranking
                foreach($pr_items as $pr_item){
                    // if pr item doesnt have sub item
                    if(!count($pr_item->pr_sub_items)){
                        $pr_item->suppliers()->attach($supplier,[
                            'abstract_id' => $abstract->id,
                            'unit_cost'   => $request->input('unit_cost_supplier_'.$supplier.'_pr_item_'.$pr_item->id),
                            'total'       => $request->input('total_cost_supplier_'.$supplier.'_pr_item_'.$pr_item->id),
                            'per_item_ranking' => $request->input('ranking_supplier_'.$supplier.'_pr_item_'.$pr_item->id.'_col_'.$supplier_ctr)
                        ]);
                    } else {
                        foreach($pr_item->pr_sub_items as $subItem){
                            $subItem->suppliers()->attach($supplier, [
                                'abstract_id' => $abstract->id,
                                'unit_cost' => $request->input('sub_unit_cost_supplier_'.$supplier.'_pr_sub_item_'.$subItem->id),
                                'total'       => $request->input('sub_total_cost_supplier_'.$supplier.'_pr_sub_item_'.$subItem->id),
                                'per_item_ranking' => $request->input('ranking_supplier_'.$supplier.'_pr_sub_item_'.$subItem->id.'_col_'.$supplier_ctr)
                            ]);
                        }
                    }
                }
            }
            $supplier_ctr++;
        }

        //save eligibility
        $this->documentEligibilityStore($abstract->id, $request->legal_docs_count, "legal_docs_", $request);
        $this->documentEligibilityStore($abstract->id, $request->tech_docs_count, "technical_docs_", $request);
        $this->documentEligibilityStore($abstract->id, $request->financial_docs_count, "financial_docs_", $request);
        $this->documentEligibilityStore($abstract->id, $request->classb_docs_count, "classb_docs_", $request);
        $this->documentEligibilityStore($abstract->id, $request->other_docs_count, "other_docs_", $request);

        //save recommendations
        $abstract_reco = new AbstractRecommendation;
        $abstract_reco->abstract_id = $abstract->id;
        $abstract_reco->total = $request->reco_total;
        $abstract_reco->save();

        if ($abstract_reco){
            $reco_count = count($request->reco_row_count) - 1;
            if ($reco_count > 0){
                $saved_reco = 0;
                $reco_ctr = 1;
                foreach($pr_items as $item){
                    $subItemCount = count($item->pr_sub_items);
                }
                while($saved_reco != $reco_count){
                    if ($request->input("reco_supplier_" . $reco_ctr) !== null){
                        $reco_item              = new AbstractRecoItem;
                        $reco_item->reco_id     = $abstract_reco->id;
                        $reco_item->supplier_id = $request->input("reco_supplier_".$reco_ctr);
                        $reco_item->pr_item_ids  = implode(',', $request->input("reco_item_no_".$reco_ctr));
                        $reco_item->subtotal    = $request->input("reco_sub_total_".$reco_ctr);
                        $reco_item->remarks     = $request->input("reco_remarks_".$reco_ctr);
                        if($subItemCount > 0) {
                            $reco_item->pr_item_type = 'sub';
                        } else {
                            $reco_item->pr_item_type = 'parent';
                        }
                        $reco_item->save();
                        
                        $saved_reco++;
                    }
                    $reco_ctr++;
                } 
            }
        }

        $obj = new \stdClass();
        if($aob->abstract_parent->invite->rfq){
            $aob->pr = $aob->abstract_parent->invite->rfq->pr;
        } else if ($aob->abstract_parent->invite->rfp) {
            $aob->pr = $aob->abstract_parent->invite->rfp->pr;
        } else if ($aob->abstract_parent->invite->itb){
            $aob->pr = $aob->abstract_parent->invite->itb->pr;
        } else if ($aob->abstract_parent->invite->rei){
            $aob->pr = $aob->abstract_parent->invite->rei->pr;
        }

        $notif_recipient = User::find($aob->pr->requested_by);
        $obj->message = Auth::user()->fullName . ' generated AOB#'.$aob->abstract_parent->form_no.' for PR#'.$aob->pr->pr_no;
        $obj->link = url('/pr/'.$aob->pr->id);
        SystemNotification::add($notif_recipient, $obj, 'abstract', $aob->abstract_parent);
        $notif_recipient->notify(new EmailNotification($obj));

        $adminUsers = User::where('user_role', 3)->get();
        foreach($adminUsers as $adminUser) {
            $obj = new \stdClass();
            $obj->message = Auth::user()->fullName.' generated AOB#'.$aob->abstract_parent->form_no.'.';
            $obj->link = url('/aob/'.$aob->id);
            SystemNotification::add($adminUser, $obj, 'abstract', $aob->abstract_parent);
            $adminUser->notify(new EmailNotification($obj));
        }

        if(Auth::user()->user_role === 3) {
            SystemNotification::checkURL();
        }

        return redirect('/aob/'.$aob->id)->with('success', 'Abstract of Bids successfully saved!');
    }

    public function show(AOB $aob) {
        if(!$aob) {
            abort(404);
        }
        if($aob->abstract_parent->invite->rfq){
            $aob->pr = $aob->abstract_parent->invite->rfq->pr;
        } else if ($aob->abstract_parent->invite->rfp) {
            $aob->pr = $aob->abstract_parent->invite->rfp->pr;
        } else if ($aob->abstract_parent->invite->itb){
            $aob->pr = $aob->abstract_parent->invite->itb->pr;
        } else if ($aob->abstract_parent->invite->rei){
            $aob->pr = $aob->abstract_parent->invite->rei->pr;
        }

        $aob->extra_rows = count($aob->abstract_parent->docs);

        foreach($aob->abstract_parent->docs as $doc){
            $legal = ['a1', 'a2', 'a3', 'a4', 'a9'];

            if (in_array($doc->document_type, $legal)){
                $doc->doc_class = 'legal';
                
                if ($doc->document_type == "a1"){
                    $doc->document_name = "SEC Registration/DTI Registration/CDA";
                }else if ($doc->document_type == "a2"){
                    $doc->document_name = "Mayor's/Business Permit";
                }else if ($doc->document_type == "a3"){
                    $doc->document_name = "Tax Clearance";
                }else if ($doc->document_type == "a4"){
                    $doc->document_name = "PhilGEPS Certificate of Registration & Membership (Platinum)";
                }else if ($doc->document_type == "a9"){
                    $doc->document_name = $doc->extra_document;
                }
            }

            $technical = ['b1', 'b2', 'b3', 'b9'];

            if (in_array($doc->document_type, $technical)){
                $doc->doc_class = 'technical';
                
                if ($doc->document_type == "b1"){
                    $doc->document_name = "Statement of all Ongoing Government and Private Contracts, including awarded but not yet started";
                }else if ($doc->document_type == "b2"){
                    $doc->document_name = "Statement of Single Largest Completed Contract (SLCC) similar to the contract to be bid";
                }else if ($doc->document_type == "b3"){
                    $doc->document_name = "PBAC License (Infra.)";
                }else if ($doc->document_type == "b9"){
                    $doc->document_name = $doc->extra_document;
                }
            }

            $financial = ['c1', 'c2', 'c9'];

            if (in_array($doc->document_type, $financial)){
                $doc->doc_class = 'financial';
                
                if ($doc->document_type == "c1"){
                    $doc->document_name = "Audited Financial Statement (AFS)";
                }else if ($doc->document_type == "c2"){
                    $doc->document_name = "Net Financial Contracting Capacity (NFCC) / Committed Line of Credit";
                }else if ($doc->document_type == "c9"){
                    $doc->document_name = $doc->extra_document;
                }
            }

            $classb = ['d1', 'd2', 'd9'];

            if (in_array($doc->document_type, $classb)){
                $doc->doc_class = 'classb';
                
                if ($doc->document_type == "d1"){
                    $doc->document_name = "Valid Joint Venture Agreement (JVA)";
                }else if ($doc->document_type == "d2"){
                    $doc->document_name = "N/A";
                }else if ($doc->document_type == "d9"){
                    $doc->document_name = $doc->extra_document;
                }
            }

            $other = ['e1', 'e2', 'e3', 'e4', 'e5', 'e9'];

            if (in_array($doc->document_type, $other)){
                $doc->doc_class = 'other';
                
                if ($doc->document_type == "e1"){
                    $doc->document_name = "Bid Security";
                }else if ($doc->document_type == "e2"){
                    $doc->document_name = "Omnibus Sworn Statement";
                }else if ($doc->document_type == "e3"){
                    $doc->document_name = "Technical Specifications (for Goods)";
                }else if ($doc->document_type == "e4"){
                    $doc->document_name = "Project Requirements (for Infra)";
                }else if ($doc->document_type == "e5"){
                    $doc->document_name = "Corporate Secretary Certificate / Board Resolution for Authorized Representative";
                }else if ($doc->document_type == "e9"){
                    $doc->document_name = $doc->extra_document;
                }
            }
        }

        foreach($aob->abstract_parent->recommendation->recommendation_items as $reco_items){
            $pr_item_ids = explode(",", $reco_items->pr_item_ids);

            $pr_item_nos = "";
            if($reco_items->pr_item_type === 'parent') {
                for($pr_item_ctr = 0; $pr_item_ctr < count($pr_item_ids); $pr_item_ctr++){
                    $pr_item = PurchaseRequestItem::find($pr_item_ids[$pr_item_ctr]);
    
                    if ($pr_item_ctr != (count($pr_item_ids) - 1))
                        $pr_item_nos .= $pr_item->item_no . ",";
                    else
                        $pr_item_nos .= $pr_item->item_no;
                }
            } else {
                if($aob->pr->pr_item_total->is_lot_purchase === 1) {
                    for($pr_item_ctr = 0; $pr_item_ctr < count($pr_item_ids); $pr_item_ctr++){
                        $pr_item = PurchaseRequestItem::find($pr_item_ids[$pr_item_ctr]);
        
                        if ($pr_item_ctr != (count($pr_item_ids) - 1))
                            $pr_item_nos .= $pr_item->item_no . ",";
                        else
                            $pr_item_nos .= $pr_item->item_no;
                    }
                } else {
                    for($pr_sub_item_ctr = 0; $pr_sub_item_ctr < count($pr_item_ids); $pr_sub_item_ctr++){
                        $pr_sub_item = PurchaseRequestSubItem::find($pr_item_ids[$pr_sub_item_ctr]);
        
                        if ($pr_sub_item_ctr != (count($pr_item_ids) - 1))
                            $pr_item_nos .= $pr_sub_item->item_no . ",";
                        else
                            $pr_item_nos .= $pr_sub_item->item_no;
                    }
                }
            }
            $reco_items->pr_item_nos = $pr_item_nos;
        }

        return view('aob.show')->with('abstract_form', $aob);
    }

    private function documentEligibilityStore($abstract_id, $doc_counts, $doc_prefix, $request){
        $doc_count = count($doc_counts) - 1;
        if ($doc_count > 0){
            $saved_docs = 0;
            $doc_ctr = 1;
            while($saved_docs != $doc_count){
                if ($request->input($doc_prefix . $doc_ctr) !== null){
                    $extra_docs_array = ['a9', 'b9', 'c9', 'd9', 'e9'];

                    $abstract_doc = new AbstractDocument;
                    $abstract_doc->abstract_id = $abstract_id;
                    $abstract_doc->document_type = $request->input($doc_prefix . $doc_ctr);
                    if (in_array($request->input($doc_prefix . $doc_ctr), $extra_docs_array)){
                        $abstract_doc->extra_document = $request->input($doc_prefix ."extra_".$doc_ctr);
                    }
                    $abstract_doc->save();

                    if($abstract_doc){
                        $supplier_count = count($request->supplierList);
                        for($i=0;$i<$supplier_count;$i++){
                            $abs_sup_eligibility = new AbstractSupplierEligibility;
                            $abs_sup_eligibility->abstract_docs_id = $abstract_doc->id;
                            $abs_sup_eligibility->supplier_id = $request->input("supplier".$i);
                            // dd(("ID:" .  $abstract_doc->id . $doc_prefix . $doc_ctr."_supplier".$i));
                            $abs_sup_eligibility->eligibility = $request->input($doc_prefix . $doc_ctr."_supplier".$i);
                            $abs_sup_eligibility->save();
                        }
                    }
                    $saved_docs++;
                } 
                $doc_ctr++;
            }
        }
    }

    public function edit(AOB $aob){
        if(!$aob) {
            abort(404);
        }
        if($aob->abstract_parent->invite->type === 'RFQ'){
            $aob->pr = $aob->abstract_parent->invite->rfq->pr;
        } else if ($aob->abstract_parent->invite->type === 'RFP'){
            $aob->pr = $aob->abstract_parent->invite->rfp->pr;
        } else if ($aob->abstract_parent->invite->type === 'IB'){
            $aob->pr = $aob->abstract_parent->invite->itb->pr;
        } else if ($aob->abstract_parent->invite->type === 'REI'){
            $aob->pr = $aob->abstract_parent->invite->rei->pr;
        }

        $aob->legal_docs = AbstractDocument::where('document_type', 'like', 'a%')->where('abstract_id', $aob->abstract_parent->id)->get();
        $aob->tech_docs = AbstractDocument::where('document_type', 'like', 'b%')->where('abstract_id', $aob->abstract_parent->id)->get();
        $aob->financial_docs = AbstractDocument::where('document_type', 'like', 'c%')->where('abstract_id', $aob->abstract_parent->id)->get();
        $aob->class_b_docs = AbstractDocument::where('document_type', 'like', 'd%')->where('abstract_id', $aob->abstract_parent->id)->get();
        $aob->other_tech_docs = AbstractDocument::where('document_type', 'like', 'e%')->where('abstract_id', $aob->abstract_parent->id)->get();

        $abstracts = AbstractParent::all();
        $public_bidding_procs = ProcurementMode::whereIn('id', [1, 2, 3])->get();

        $reco_suppliers = [];
        foreach($aob->abstract_parent->suppliers as $supplier) {
            $reco_suppliers[$supplier->id] = $supplier->company_name;
        }

        $reco_items_edit = [];
        foreach($aob->abstract_parent->recommendation->recommendation_items as $reco_items){
            $pr_item_ids = explode(",", $reco_items->pr_item_ids);
            $reco_items->pr_item_ids_array = $pr_item_ids;
            $pr_item_nos = "";
            if($reco_items->pr_item_type === 'parent') {
                for($pr_item_ctr = 0; $pr_item_ctr < count($pr_item_ids); $pr_item_ctr++){
                    $pr_item = PurchaseRequestItem::find($pr_item_ids[$pr_item_ctr]);
    
                    if ($pr_item_ctr != (count($pr_item_ids) - 1)) {
                        $pr_item_nos .= $pr_item->item_no . ",";
                        $reco_items_edit[$pr_item->id] = $pr_item->item_no;
                        $pr_item_ids_edit[] = $pr_item->id;
                    } else {
                        $pr_item_nos .= $pr_item->item_no;
                        $reco_items_edit[$pr_item->id] = $pr_item->item_no;
                        $pr_item_ids_edit[] = $pr_item->id;
                    }
                }
            } else {
                if($aob->pr->pr_item_total->is_lot_purchase === 1) {
                    for($pr_item_ctr = 0; $pr_item_ctr < count($pr_item_ids); $pr_item_ctr++){
                        $pr_item = PurchaseRequestItem::find($pr_item_ids[$pr_item_ctr]);
        
                        if ($pr_item_ctr != (count($pr_item_ids) - 1)) {
                            $pr_item_nos .= $pr_item->item_no . ",";
                            $reco_items_edit[$pr_item->id] = $pr_item->item_no;
                            $pr_item_ids_edit[] = $pr_item->id;
                        } else {
                            $pr_item_nos .= $pr_item->item_no;
                            $reco_items_edit[$pr_item->id] = $pr_item->item_no;
                            $pr_item_ids_edit[] = $pr_item->id;
                        }
                    }
                } else {
                    for($pr_sub_item_ctr = 0; $pr_sub_item_ctr < count($pr_item_ids); $pr_sub_item_ctr++){
                        $pr_sub_item = PurchaseRequestSubItem::find($pr_item_ids[$pr_sub_item_ctr]);
        
                        if ($pr_sub_item_ctr != (count($pr_item_ids) - 1)) {
                            $pr_item_nos .= $pr_sub_item->item_no . ",";
                            $reco_items_edit[$pr_sub_item->id] = $pr_sub_item->item_no;
                            $pr_item_ids_edit[] = $pr_sub_item->id;
                        } else {
                            $pr_item_nos .= $pr_sub_item->item_no;
                            $reco_items_edit[$pr_sub_item->id] = $pr_sub_item->item_no;
                            $pr_item_ids_edit[] = $pr_sub_item->id;
                        }
                    }
                }
            }

            $reco_items->pr_item_nos = $pr_item_nos;
        }

        foreach($aob->abstract_parent->docs as $doc){
            $legal = ['a1', 'a2', 'a3', 'a4', 'a9'];

            if (in_array($doc->document_type, $legal)){
                $doc->doc_class = 'legal';
                
                if ($doc->document_type == "a1"){
                    $doc->document_name = "SEC Registration/DTI Registration/CDA";
                }else if ($doc->document_type == "a2"){
                    $doc->document_name = "Mayor's/Business Permit";
                }else if ($doc->document_type == "a3"){
                    $doc->document_name = "Tax Clearance";
                }else if ($doc->document_type == "a4"){
                    $doc->document_name = "PhilGEPS Certificate of Registration & Membership (Platinum)";
                }else if ($doc->document_type == "a9"){
                    $doc->document_name = $doc->extra_document;
                }
            }

            $technical = ['b1', 'b2', 'b3', 'b9'];

            if (in_array($doc->document_type, $technical)){
                $doc->doc_class = 'technical';
                
                if ($doc->document_type == "b1"){
                    $doc->document_name = "Statement of all Ongoing Government and Private Contracts, including awarded but not yet started";
                }else if ($doc->document_type == "b2"){
                    $doc->document_name = "Statement of Single Largest Completed Contract (SLCC) similar to the contract to be bid";
                }else if ($doc->document_type == "b3"){
                    $doc->document_name = "PBAC License (Infra.)";
                }else if ($doc->document_type == "b9"){
                    $doc->document_name = $doc->extra_document;
                }
            }

            $financial = ['c1', 'c2', 'c9'];

            if (in_array($doc->document_type, $financial)){
                $doc->doc_class = 'financial';
                
                if ($doc->document_type == "c1"){
                    $doc->document_name = "Audited Financial Statement (AFS)";
                }else if ($doc->document_type == "c2"){
                    $doc->document_name = "Net Financial Contracting Capacity (NFCC) / Committed Line of Credit";
                }else if ($doc->document_type == "c9"){
                    $doc->document_name = $doc->extra_document;
                }
            }

            $classb = ['d1', 'd2', 'd9'];

            if (in_array($doc->document_type, $classb)){
                $doc->doc_class = 'classb';
                
                if ($doc->document_type == "d1"){
                    $doc->document_name = "Valid Joint Venture Agreement (JVA)";
                }else if ($doc->document_type == "d2"){
                    $doc->document_name = "N/A";
                }else if ($doc->document_type == "d9"){
                    $doc->document_name = $doc->extra_document;
                }
            }

            $other = ['e1', 'e2', 'e3', 'e4', 'e5', 'e9'];

            if (in_array($doc->document_type, $other)){
                $doc->doc_class = 'other';
                
                if ($doc->document_type == "e1"){
                    $doc->document_name = "Bid Security";
                }else if ($doc->document_type == "e2"){
                    $doc->document_name = "Omnibus Sworn Statement";
                }else if ($doc->document_type == "e3"){
                    $doc->document_name = "Technical Specifications (for Goods)";
                }else if ($doc->document_type == "e4"){
                    $doc->document_name = "Project Requirements (for Infra)";
                }else if ($doc->document_type == "e5"){
                    $doc->document_name = "Corporate Secretary Certificate / Board Resolution for Authorized Representative";
                }else if ($doc->document_type == "e9"){
                    $doc->document_name = $doc->extra_document;
                }
            }
        }

        return view('aob.edit')->with('abstract_form', $aob)
                               ->with('abstract_type', 'aob')
                               ->with('abstract_parent_count', count($abstracts))
                               ->with('public_bidding_procs', $public_bidding_procs)
                               ->with('reco_suppliers', $reco_suppliers)
                               ->with('reco_items_edit', $reco_items_edit);
    }

    public function update(Request $request, AbstractParent $abstract){
        $abstract->delete();
        $abstract->save();
        $aob = new AOB;

        //get id form pb_type because mode of procurement dropdown is disabled
        $aob->procurement_mode_id = $request->pb_type;
        $aob->bid_type = $request->bid_type;

        if ($request->pb_type == '1'){
            $aob->pb_type = "For Infrastructure Projects";
        }else if($request->pb_type == '2'){
            $aob->pb_type = "For Goods and Services";
        }else if($request->pb_type == '3'){
            $aob->pb_type = "For Consulting Services";
        }
        
        $aob->pbac_chairperson = $request->pbac_chairperson;
        $aob->pbac_vice_chairperson = $request->pbac_vice_chairperson;
        $aob->pbac_member_1 = $request->pbac_member_1;
        $aob->pbac_member_2 = $request->pbac_member_2;
        $aob->provisional_member = $request->provisional_member;
        $aob->coa_representative = $request->coa_representative;
        $aob->private_observer_1 = $request->private_observer_1;
        $aob->private_observer_2 = $request->private_observer_2;
        $aob->twg_chairman = $request->twg_chairman;
        $aob->twg_member_1 = $request->twg_member_1;
        $aob->twg_member_2 = $request->twg_member_2;
        $aob->opening_date = $request->opening_date;
        $aob->remarks = $request->remarks;

        $abstract->save();
        $abstract->aob()->save($aob);

        $purchase_type = "";
        if ($abstract->invite->type == 'REI'){
            if($abstract->invite->rei->pr->pr_item_total->is_lot_purchase){
                $purchase_type = "per lot";
            }else if ($abstract->invite->rei->pr->pr_item_total->is_per_item_purchase){
                $purchase_type = "per item";
            }
            $pr_items = $abstract->invite->rei->pr->pr_items;
        }else if ($abstract->invite->type == 'RFQ'){
            if($abstract->invite->rfq->pr->pr_item_total->is_lot_purchase){
                $purchase_type = "per lot";
            }else if ($abstract->invite->rfq->pr->pr_item_total->is_per_item_purchase){
                $purchase_type = "per item";
            }
            $pr_items = $abstract->invite->rfq->pr->pr_items;
        }else if ($abstract->invite->type == 'RFP'){
            if($abstract->invite->rfp->pr->pr_item_total->is_lot_purchase){
                $purchase_type = "per lot";
            }else if ($abstract->invite->rfp->pr->pr_item_total->is_per_item_purchase){
                $purchase_type = "per item";
            }
            $pr_items = $abstract->invite->rfp->pr->pr_items;
        }else if ($abstract->invite->type == 'IB'){
            if($abstract->invite->itb->pr->pr_item_total->is_lot_purchase){
                $purchase_type = "per lot";
            }else if ($abstract->invite->itb->pr->pr_item_total->is_per_item_purchase){
                $purchase_type = "per item";
            }
            $pr_items = $abstract->invite->itb->pr->pr_items;
        }

        $supplier_ctr = 0;
        foreach($request->supplierList as $supplier){
            if ($purchase_type == "per lot") {
                $abstract->suppliers()->attach($supplier, [
                    'ranking'       => $request->input("supplier_ranking".$supplier),
                    'compliance'    => $request->input("result_compliance_supplier_id_".$supplier),
                ]);

                //store unit cost, total cost, and per item ranking
                foreach($pr_items as $pr_item){
                    // if pr item doesnt have sub item
                    if(!count($pr_item->pr_sub_items)){
                        $pr_item->suppliers()->attach($supplier,[
                            'abstract_id' => $abstract->id,
                            'unit_cost'   => $request->input('unit_cost_supplier_'.$supplier.'_pr_item_'.$pr_item->id),
                            'total'       => $request->input('total_cost_supplier_'.$supplier.'_pr_item_'.$pr_item->id)
                        ]);
                    } else {
                        foreach($pr_item->pr_sub_items as $subItem){
                            $subItem->suppliers()->attach($supplier, [
                                'abstract_id' => $abstract->id,
                                'unit_cost' => $request->input('sub_unit_cost_supplier_'.$supplier.'_pr_sub_item_'.$subItem->id),
                                'total'       => $request->input('sub_total_cost_supplier_'.$supplier.'_pr_sub_item_'.$subItem->id)
                            ]);
                        }
                    }
                }
            } else if ($purchase_type == "per item") {
                $abstract->suppliers()->attach($supplier, [
                    'compliance'    => $request->input("result_compliance_supplier_id_".$supplier),
                ]);

                //store unit cost, total cost, and per item ranking
                foreach($pr_items as $pr_item){
                    // if pr item doesnt have sub item
                    if(!count($pr_item->pr_sub_items)){
                        $pr_item->suppliers()->attach($supplier,[
                            'abstract_id' => $abstract->id,
                            'unit_cost'   => $request->input('unit_cost_supplier_'.$supplier.'_pr_item_'.$pr_item->id),
                            'total'       => $request->input('total_cost_supplier_'.$supplier.'_pr_item_'.$pr_item->id),
                            'per_item_ranking' => $request->input('ranking_supplier_'.$supplier.'_pr_item_'.$pr_item->id.'_col_'.$supplier_ctr)
                        ]);
                    } else {
                        foreach($pr_item->pr_sub_items as $subItem){
                            $subItem->suppliers()->attach($supplier, [
                                'abstract_id' => $abstract->id,
                                'unit_cost' => $request->input('sub_unit_cost_supplier_'.$supplier.'_pr_sub_item_'.$subItem->id),
                                'total'       => $request->input('sub_total_cost_supplier_'.$supplier.'_pr_sub_item_'.$subItem->id),
                                'per_item_ranking' => $request->input('ranking_supplier_'.$supplier.'_pr_sub_item_'.$subItem->id.'_col_'.$supplier_ctr)
                            ]);
                        }
                    }
                }
            }
            $supplier_ctr++;
        }

        //save eligibility
        $this->documentEligibilityStore($abstract->id, $request->legal_docs_count, "legal_docs_", $request);
        $this->documentEligibilityStore($abstract->id, $request->tech_docs_count, "technical_docs_", $request);
        $this->documentEligibilityStore($abstract->id, $request->financial_docs_count, "financial_docs_", $request);
        $this->documentEligibilityStore($abstract->id, $request->classb_docs_count, "classb_docs_", $request);
        $this->documentEligibilityStore($abstract->id, $request->other_docs_count, "other_docs_", $request);

        //save recommendations
        $abstract_reco = new AbstractRecommendation;
        $abstract_reco->abstract_id = $abstract->id;
        $abstract_reco->total = $request->reco_total;
        $abstract_reco->save();

        if ($abstract_reco){
            $reco_count = count($request->reco_row_count) - 1;
            if ($reco_count > 0){
                $saved_reco = 0;
                $reco_ctr = 1;
                foreach($pr_items as $item){
                    $subItemCount = count($item->pr_sub_items);
                }
                while($saved_reco != $reco_count){
                    if ($request->input("reco_supplier_" . $reco_ctr) !== null){
                        $reco_item              = new AbstractRecoItem;
                        $reco_item->reco_id     = $abstract_reco->id;
                        $reco_item->supplier_id = $request->input("reco_supplier_".$reco_ctr);
                        $reco_item->pr_item_ids  = implode(',', $request->input("reco_item_no_".$reco_ctr));
                        $reco_item->subtotal    = $request->input("reco_sub_total_".$reco_ctr);
                        $reco_item->remarks     = $request->input("reco_remarks_".$reco_ctr);
                        if($subItemCount > 0) {
                            $reco_item->pr_item_type = 'sub';
                        } else {
                            $reco_item->pr_item_type = 'parent';
                        }
                        $reco_item->save();
                        
                        $saved_reco++;
                    }
                    $reco_ctr++;
                } 
            }
        }

        return redirect('/aob/'.$aob->id)->with('success', 'Abstract of Bids successfully updated!');

    }
}
