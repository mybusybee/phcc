<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserRole;
use App\User;
use App\Office;
use Auth;
use EmailNotifier;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('id', '!=', 1)->get();

        $log_arr = [
            "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            "user_office" => Auth::user()->office->office,
            "activity"  => 'viewed Users List',
        ];
        \ActivityLog::add($log_arr);

        return view('users.index')->with('users', $users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = UserRole::where('id', '!=', 1)->get();
        $offices = Office::where('id', '!=', 1)->pluck('office', 'id');
        return view('users.create')
        ->with('roles', $roles)
        ->with('offices', $offices);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'fname' => 'required',
            'lname' => 'required',
            'email' => 'unique:users|required|email',
            'user_role' => 'required',
            'office_id' => 'required'
        ]);

        $user = new User;
        $user->first_name = $request->fname;
        $user->last_name = $request->lname;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->user_role = $request->user_role;
        $user->office_id = $request->office_id;
        $user->save();

        $log_arr = [
            "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            "user_office" => Auth::user()->office->office,
            "activity"  => 'created new user ('. $user->first_name . ' ' . $user->last_name .').',
        ];
        \ActivityLog::add($log_arr);

        EmailNotifier::sendMail($user->email, $request->password);

        return redirect('/user')->with('success', 'User successfully created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        $user->role = UserRole::where('id', $user->user_role)->first();

        $log_arr = [
            "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            "user_office" => Auth::user()->office->office,
            "activity"  => 'viewed user ' . $user->first_name . ' ' . $user->last_name,
        ];
        \ActivityLog::add($log_arr);

        return view('users.show')->with('user', $user);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = UserRole::where('id', '!=', 1)->pluck('user_role', 'id');
        $offices = Office::where('id', '!=', 1)->pluck('office', 'id');
        return view('users.edit')->with('user', $user)->with('roles', $roles)->with('offices', $offices);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->first_name = $request->fname;
        $user->last_name = $request->lname;
        $user->email = $request->email;
        $user->user_role = $request->user_role;
        $user->office_id = $request->office;
        $user->save();

        $log_arr = [
            "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            "user_office" => Auth::user()->office->office,
            "activity"  => 'edited user ' . $user->first_name . ' ' . $user->last_name,
        ];
        \ActivityLog::add($log_arr);

        return redirect('/user')->with('success', 'User updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);

        $log_arr = [
            "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            "user_office" => Auth::user()->office->office,
            "activity"  => 'deleted user ' . $user->first_name . ' ' . $user->last_name,
        ];
        \ActivityLog::add($log_arr);

        $user->delete();

        return back()->with('success', 'User successfully deleted!');
    }

    public function activate($id)
    {
        $user = User::find($id);

        $log_arr = [
            "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            "user_office" => Auth::user()->office->office,
            "activity"  => 'activated user ' . $user->first_name . ' ' . $user->last_name,
        ];
        \ActivityLog::add($log_arr);

        $user->is_active = true;
        $user->save();

        return back()->with('success', 'User successfully activated!');
    }

    public function deactivate($id)
    {
        $user = User::find($id);

        $log_arr = [
            "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            "user_office" => Auth::user()->office->office,
            "activity"  => 'deactivated user ' . $user->first_name . ' ' . $user->last_name,
        ];
        \ActivityLog::add($log_arr);

        $user->is_active = false;
        $user->save();

        return back()->with('success', 'User successfully deactivated!');
    }
}
