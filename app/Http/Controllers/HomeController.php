<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\PurchaseRequest as PR;
use App\Invitation;
use App\AbstractParent;
use App\OrdersParent;
use App\Supplier;
use File;
// use App\Mail\TestEmail;
// use Mail;
use App\SystemNotification;
use App\User;
use App\ProcurementUserPmr as Pmr;
use App\Announcement;
use App\Reference;
use App\UploadedPmr;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!Auth::check()){
            return redirect('/login');
        }else{
            return redirect('/dashboard');
        }
    }

    public function show_dashboard(){
        $counts = new \stdClass();
        $counts->invitations = 0;
        if (Auth::user()->user_role == 5) {
            $counts->pr = count(PR::where('proc_user_assigned', Auth::user()->id)->get());
            foreach(PR::where('proc_user_assigned', Auth::user()->id)->get() as $pr) {
                if (count($pr->rfp)) {
                    foreach($pr->rfp as $pr_rfp) {
                        $counts->invitations++;
                    }
                } else if ($pr->itb) {
                    $counts->invitations++;
                } else if ($pr->rei) {
                    $counts->invitations++;
                } else if (count($pr->rfq)) {
                    foreach($pr->rfq as $pr_rfq) {
                        $counts->invitations++;
                    }
                }
            }
        } else {
            $counts->pr = count(PR::all());
            $counts->invitations = count(Invitation::all());
        }
        $counts->abstract_parents = count(AbstractParent::all());
        $counts->order_parents = count(OrdersParent::all());
        $counts->supplier = count(Supplier::all());
        $user = Auth::user();

        // Mail::to($user->email)
        //     ->send(new TestEmail($user));

        $data = [0,0,0,0,0];
        $data_office_wide = [0,0,0,0,0];
        $pmrs = Pmr::all();
        $uploaded_pmrs = UploadedPmr::all();
        foreach($pmrs as $pmr) {
            foreach($pmr->pmr_items as $item) {
                $data[0]++;
                if($item->status === 'Cancelled') {
                    $data[3]++;
                } else if($item->status === 'Failed') {
                    $data[4]++;
                } else if($item->contract_signing !== null || $item->contract_signing !== 'N/A' || $item->contract_signing !== 'n/a') {
                    $data[2]++;
                } else {
                    $data[1]++;
                }

                if($item->pr->office_id === Auth::user()->office_id) {
                    $data_office_wide[0]++;
                    if($item->status === 'Cancelled') {
                        $data_office_wide[3]++;
                    } else if($item->status === 'Failed') {
                        $data_office_wide[4]++;
                    } else if($item->contract_signing !== null || $item->contract_signing !== 'N/A' || $item->contract_signing !== 'n/a') {
                        $data_office_wide[2]++;
                    } else {
                        $data_office_wide[1]++;
                    }
                }
            }
        }

        foreach($uploaded_pmrs as $up) {
            foreach($up->pmr_items as $item) {
                $data[0]++;
                if($item->status === 'Cancelled') {
                    $data[3]++;
                } else if($item->status === 'Failed') {
                    $data[4]++;
                } else if($item->contract_signing !== null || $item->contract_signing !== 'N/A' || $item->contract_signing !== 'n/a') {
                    $data[2]++;
                } else {
                    $data[1]++;
                }

                if($item->end_user_id === Auth::user()->office_id) {
                    $data_office_wide[0]++;
                    if($item->status === 'Cancelled') {
                        $data_office_wide[3]++;
                    } else if($item->status === 'Failed') {
                        $data_office_wide[4]++;
                    } else if($item->contract_signing !== null || $item->contract_signing !== 'N/A' || $item->contract_signing !== 'n/a') {
                        $data_office_wide[2]++;
                    } else {
                        $data_office_wide[1]++;
                    }
                }
            }
        }

        $announcements = Announcement::latest()->take(5)->get();

        $references = Reference::latest()->get();

        $pr_status_data = $this->getPRStatusData();

        return view('dashboard.dashboard2')->with('counts', $counts)
                                           ->with('graphData', implode(',',$data))
                                           ->with('graphDataOffice', implode(',',$data_office_wide))
                                           ->with('announcements', $announcements)
                                           ->with('references', $references)
                                           ->with('pr_status_data',implode(',',$pr_status_data));
    }

    private function getPRStatusData() {
        $prs = PR::all();
        $uploaded_pmrs = UploadedPmr::all();
        $pr_status_array = [
            'revision'          => 0,
            'director_approval' => 0,
            'procurement'       => 0,
            'budget'            => 0,
            'submission'        => 0,
            'assignment'        => 0,
            'assigned'          => 0
        ];
        foreach($prs as $pr) {
            if($pr->status === 'PR_FOR_REVISION'){
                $pr_status_array['revision']++;
            } else if($pr->status === 'PR_FOR_DIRECTOR_APPROVAL') {
                $pr_status_array['director_approval']++;
            }  else if($pr->status === 'PR_FOR_PROCUREMENT_VALIDATION') {
                $pr_status_array['procurement']++;
            }  else if($pr->status === 'PR_FOR_BUDGET_ALLOCATION') {
                $pr_status_array['budget']++;
            }  else if($pr->status === 'PR_FOR_SUBMISSION') {
                if($pr->is_hardcopy_received === 0) {
                    $pr_status_array['submission']++;
                } else {
                    $pr_status_array['assignment']++;
                }
            }  else if($pr->status === 'PR_ASSIGNED') {
                $pr_status_array['assigned']++;
            }
        }
        foreach($uploaded_pmrs as $up) {
            foreach($up->pmr_items as $item) {
                $pr_status_array['assigned']++;
            }
        }
        return $pr_status_array;
    }

    public function download_ppmp(){
        $filepath = storage_path('dashboard/faqs/ppmp.pdf');

        if(!file_exists($filepath)){
            return redirect()->back()->with('error', 'File not found!');
        }

        return response()->download($filepath);
    }

    public function download_pr(){
        $filepath = storage_path('dashboard/faqs/pr.pdf');

        if(!file_exists($filepath)){
            return redirect()->back()->with('error', 'File not found!');
        }

        return response()->download($filepath);
    }

    public function download_rfq(){
        $filepath = storage_path('dashboard/faqs/rfq.pdf');

        if(!file_exists($filepath)){
            return redirect()->back()->with('error', 'File not found!');
        }

        return response()->download($filepath);
    }

    public function download_aoq(){
        $filepath = storage_path('dashboard/faqs/aoq.pdf');

        if(!file_exists($filepath)){
            return redirect()->back()->with('error', 'File not found!');
        }

        return response()->download($filepath);
    }

    public function download_pojo(){
        $filepath = storage_path('dashboard/faqs/pojo.pdf');

        if(!file_exists($filepath)){
            return redirect()->back()->with('error', 'File not found!');
        }

        return response()->download($filepath);
    }

    public function download_proc_manual(){
        $filepath = storage_path('dashboard/manual/proc_manual.pdf');

        if(!file_exists($filepath)){
            return redirect()->back()->with('error', 'File not found!');
        }

        return response()->download($filepath);
    }

    public function upload_proc_manual(Request $request){
        if($request->hasFile('proc_file')){
            $message = [
                'proc_file.mimes' => 'The uploaded file must be a file type of pdf, docx, or doc.'
            ];
            $request->validate([
                'proc_file' => 'mimes:pdf,docx,doc'
            ], $message);

            $filename = 'proc_manual.pdf';
            $file_contents = file_get_contents($request->file('proc_file'));

            if(file_exists(storage_path('dashboard/manual/'.$filename))){
                File::delete(storage_path('dashboard/manual/'.$filename));
                File::put(storage_path('dashboard/manual/'.$filename), $file_contents);
            } else {
                File::put(storage_path('dashboard/manual/'.$filename), $file_contents);
            }

            return redirect()->back()->with('success', 'New Procurement Manual uploaded!');
        }
    }

    public function showForbidden(){
        return "You are not allowed to view this page!";
    }

    public function showNotifications(){
        $notifications = SystemNotification::where('user_id', Auth::id())->latest()->paginate(30);
        return view('notification.index')->with('notifications', $notifications);
    }

    public function downloadPPMPImportTemplate() {
        return response()->download(storage_path('ppmpimporttemplate.xlsx'), time() . '.xlsx');
    }
}
