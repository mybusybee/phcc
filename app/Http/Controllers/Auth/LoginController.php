<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;
use Session;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request){
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password, 'is_active' => 1])){
            $user = User::where('id', Auth::id())->first();
            session()->put('user_role', $user->user_role);
            session()->put('name', $user->first_name.' '.$user->last_name);

            $log_arr = [
                "user_name" => $user->first_name.' '.$user->last_name,
                "user_office" => $user->office->office,
                "activity"  => 'logged in.',
            ];
            \ActivityLog::add($log_arr);

            return redirect('/dashboard');
        } else {
            session()->flash('error', 'Invalid login credentials! or User has been Deactivated!');
            return redirect('/login');
        }
    }
}
