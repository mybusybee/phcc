<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Invitation;
use App\ProcurementMode;
use App\AbstractParent;
use App\AbstractOfQuotation as AOQ;
use App\AbstractDocument;
use App\AbstractSupplierEligibility;
use App\AbstractRecommendation;
use App\AbstractRecoItem;
use App\PurchaseRequestItem;
use App\PurchaseRequestSubItem;
use App\User;
use Auth;
use SystemNotification;
use App\Notifications\EmailNotification;

class AOQController extends Controller
{
    public function create(Invitation $invitation){

        $proc_modes = ProcurementMode::whereNotIn('id', ['1', '2', '3'])->get();
        
        $abstract_parents = AbstractParent::all();

        if($invitation->type === 'IB'){
            $invite = $invitation->itb;
        } else if ($invitation->type === 'REI') {
            $invite = $invitation->rei;
        } else if ($invitation->type === 'RFQ') {
            $invite = $invitation->rfq;
        } else if ($invitation->type === 'RFP') {
            $invite = $invitation->rfp;
        }

        $supplier_array = [];
        foreach($invitation->suppliers as $supplier) {
            $supplier_array[$supplier->id] = $supplier->company_name;
        }
        
        return view('aoq.create')->with('invitation', $invite)
                                 ->with('abstract_type', 'aoq')
                                 ->with('supplier_array', $supplier_array)
                                 ->with('abstract_parent_count', count($abstract_parents))
                                 ->with('proc_modes', $proc_modes);
    }

    public function store(Request $request){
        $request->validate([
            'opening_date'            => 'required',
            'canvassed_by'            => 'required',
            'canvassed_designation'   => 'required',
            'approved_by'             => 'required',
            'approved_by_designation' => 'required',
            'form_no'                 => 'required|unique:abstracts'
        ]);

        $abstract = new AbstractParent;
        $abstract->form_no  = $request->form_no;
        $abstract->invite_id = $request->invite_id;
        $abstract->type = 'AOQ';
        $abstract->status = 'SAVED';
        $abstract->save();

        $aoq = new AOQ;
        $aoq->opening_date = $request->opening_date;
        $aoq->procurement_mode_id = $request->proc_mode_type;
        $aoq->abstract_id = $abstract->id;
        $aoq->canvassed_by = $request->canvassed_by;
        $aoq->canvassed_by_designation = $request->canvassed_designation;
        $aoq->recommending_approval = $request->recommending_approval;
        $aoq->recommending_designation = $request->recommending_designation;
        $aoq->approved_by = $request->approved_by;
        $aoq->approved_by_designation = $request->approved_by_designation;
        $aoq->save();

        if($aoq){

            $purchase_type = "";

            //save supplier cost on line items
            if ($abstract->invite->type == 'REI'){
                if($abstract->invite->rei->pr->pr_item_total->is_lot_purchase){
                    $purchase_type = "per lot";
                }else if ($abstract->invite->rei->pr->pr_item_total->is_per_item_purchase){
                    $purchase_type = "per item";
                }
                $pr_items = $abstract->invite->rei->pr->pr_items;
            }else if ($abstract->invite->type == 'RFQ'){
                if($abstract->invite->rfq->pr->pr_item_total->is_lot_purchase){
                    $purchase_type = "per lot";
                }else if ($abstract->invite->rfq->pr->pr_item_total->is_per_item_purchase){
                    $purchase_type = "per item";
                }
                $pr_items = $abstract->invite->rfq->pr->pr_items;
            }else if ($abstract->invite->type == 'RFP'){
                if($abstract->invite->rfp->pr->pr_item_total->is_lot_purchase){
                    $purchase_type = "per lot";
                }else if ($abstract->invite->rfp->pr->pr_item_total->is_per_item_purchase){
                    $purchase_type = "per item";
                }
                $pr_items = $abstract->invite->rfp->pr->pr_items;
            }else if ($abstract->invite->type == 'IB'){
                if($abstract->invite->itb->pr->pr_item_total->is_lot_purchase){
                    $purchase_type = "per lot";
                }else if ($abstract->invite->itb->pr->pr_item_total->is_per_item_purchase){
                    $purchase_type = "per item";
                }
                $pr_items = $abstract->invite->itb->pr->pr_items;
            }

            $supplier_ctr = 0;
            foreach($request->supplierList as $supplier){
                if ($purchase_type == "per lot") {
                    $abstract->suppliers()->attach($supplier, [
                        'ranking'       => $request->input("supplier_ranking".$supplier),
                        'compliance'    => $request->input("result_compliance_supplier_id_".$supplier),
                    ]);

                    foreach($pr_items as $pr_item){
                        // if pr item doesnt have sub item
                        if(!count($pr_item->pr_sub_items)){
                            $pr_item->suppliers()->attach($supplier,[
                                'abstract_id' => $abstract->id,
                                'unit_cost'   => $request->input('unit_cost_supplier_'.$supplier.'_pr_item_'.$pr_item->id),
                                'total'       => $request->input('total_cost_supplier_'.$supplier.'_pr_item_'.$pr_item->id)
                            ]);
                        } else {
                            foreach($pr_item->pr_sub_items as $subItem){
                                $subItem->suppliers()->attach($supplier, [
                                    'abstract_id' => $abstract->id,
                                    'unit_cost' => $request->input('sub_unit_cost_supplier_'.$supplier.'_pr_sub_item_'.$subItem->id),
                                    'total'       => $request->input('sub_total_cost_supplier_'.$supplier.'_pr_sub_item_'.$subItem->id)
                                ]);
                            }
                        }
                    }
                } else if ($purchase_type == "per item") {
                    $abstract->suppliers()->attach($supplier, [
                        'compliance'    => $request->input("result_compliance_supplier_id_".$supplier),
                    ]);

                    foreach($pr_items as $pr_item){
                        // if pr item doesnt have sub item
                        if(!count($pr_item->pr_sub_items)){
                            $pr_item->suppliers()->attach($supplier,[
                                'abstract_id' => $abstract->id,
                                'unit_cost'   => $request->input('unit_cost_supplier_'.$supplier.'_pr_item_'.$pr_item->id),
                                'total'       => $request->input('total_cost_supplier_'.$supplier.'_pr_item_'.$pr_item->id),
                                'per_item_ranking' => $request->input('ranking_supplier_'.$supplier.'_pr_item_'.$pr_item->id.'_col_'.$supplier_ctr)
                            ]);
                        } else {
                            foreach($pr_item->pr_sub_items as $subItem){
                                $subItem->suppliers()->attach($supplier, [
                                    'abstract_id' => $abstract->id,
                                    'unit_cost' => $request->input('sub_unit_cost_supplier_'.$supplier.'_pr_sub_item_'.$subItem->id),
                                    'total'       => $request->input('sub_total_cost_supplier_'.$supplier.'_pr_sub_item_'.$subItem->id),
                                    'per_item_ranking' => $request->input('ranking_supplier_'.$supplier.'_pr_sub_item_'.$subItem->id.'_col_'.$supplier_ctr)
                                ]);
                            }
                        }
                    }
                }
                $supplier_ctr++;
            }
            

            /********************************************************************************/

            //save the eligibility docs
            $doc_count = count($request->eligibility_docs_count) - 1;
            if ($doc_count > 0){
                $saved_docs = 0;
                $doc_ctr = 1;
                while($saved_docs != $doc_count){
                    if ($request->input("eligibility_docs_" . $doc_ctr) !== null){

                        $abstract_doc = new AbstractDocument;
                        $abstract_doc->abstract_id = $abstract->id;
                        $abstract_doc->document_type = $request->input("eligibility_docs_" . $doc_ctr);
                        if ($request->input("eligibility_docs_" . $doc_ctr) == "f9"){
                            $abstract_doc->extra_document = $request->input("eligibility_docs_extra_".$doc_ctr);
                        }
                        $abstract_doc->save();

                        if($abstract_doc){
                            $supplier_count = count($request->supplierList);
                            for($i=0;$i<$supplier_count;$i++){
                                $abs_sup_eligibility = new AbstractSupplierEligibility;
                                $abs_sup_eligibility->abstract_docs_id = $abstract_doc->id;
                                $abs_sup_eligibility->supplier_id = $request->input("supplier".$i);
                                $abs_sup_eligibility->eligibility = $request->input("eligibility_docs_" . $doc_ctr."_supplier".$i);
                                $abs_sup_eligibility->save();
                            }
                        }
                        $saved_docs++;
                    } 
                    $doc_ctr++;
                }
            }

            /********************************************************************************/

            //Save the recommendations
            $abstract_reco = new AbstractRecommendation;
            $abstract_reco->abstract_id = $abstract->id;
            $abstract_reco->total = $request->reco_total;
            $abstract_reco->save();

            if ($abstract_reco){
                $reco_count = count($request->reco_row_count) - 1;
                if ($reco_count > 0){
                    $saved_reco = 0;
                    $reco_ctr = 1;
                    foreach($pr_items as $item){
                        $subItemCount = count($item->pr_sub_items);
                    }
                    while($saved_reco != $reco_count){
                        if ($request->input("reco_supplier_" . $reco_ctr) !== null){
                            $reco_item              = new AbstractRecoItem;
                            $reco_item->reco_id     = $abstract_reco->id;
                            $reco_item->supplier_id = $request->input("reco_supplier_".$reco_ctr);
                            $reco_item->pr_item_ids  = implode(',', $request->input("reco_item_no_".$reco_ctr));
                            $reco_item->subtotal    = $request->input("reco_sub_total_".$reco_ctr);
                            $reco_item->remarks     = $request->input("reco_remarks_".$reco_ctr);
                            if($subItemCount > 0) {
                                $reco_item->pr_item_type = 'sub';
                            } else {
                                $reco_item->pr_item_type = 'parent';
                            }
                            $reco_item->save();
                            $saved_reco++;
                        }
                        $reco_ctr++;
                    }
                }
            }

            $obj = new \stdClass();
            if($aoq->abstract_parent->invite->rfq){
                $aoq->pr = $aoq->abstract_parent->invite->rfq->pr;
            } else if ($aoq->abstract_parent->invite->rfp) {
                $aoq->pr = $aoq->abstract_parent->invite->rfp->pr;
            } else if ($aoq->abstract_parent->invite->itb){
                $aoq->pr = $aoq->abstract_parent->invite->itb->pr;
            } else if ($aoq->abstract_parent->invite->rei){
                $aoq->pr = $aoq->abstract_parent->invite->rei->pr;
            }

            $notif_recipient = User::find($aoq->pr->requested_by);
            $obj->message = Auth::user()->fullName . ' generated AOQ#'.$aoq->abstract_parent->form_no.' for PR#'.$aoq->pr->pr_no;
            $obj->link = url('/pr/'.$aoq->pr->id);
            SystemNotification::add($notif_recipient, $obj, 'abstract', $aoq->abstract_parent);
            $notif_recipient->notify(new EmailNotification($obj));

            $adminUsers = User::where('user_role', 3)->get();
            foreach($adminUsers as $adminUser) {
                $obj = new \stdClass();
                $obj->message = Auth::user()->fullName . ' generated AOQ#'.$aoq->abstract_parent->form_no.'.';
                $obj->link = url('/aoq/'.$aoq->id);
                SystemNotification::add($adminUser, $obj, 'abstract', $aoq->abstract_parent);
                $adminUser->notify(new EmailNotification($obj));
            }

            return redirect('/aoq/'.$aoq->id)->with('success', 'Abstract of Quotation successfully saved!');
        }
    }

    public function show(AOQ $aoq){
        if(!$aoq) {
            abort(404);
        }
        if($aoq->abstract_parent->invite->rfq){
            $aoq->pr = $aoq->abstract_parent->invite->rfq->pr;
        } else if ($aoq->abstract_parent->invite->rfp) {
            $aoq->pr = $aoq->abstract_parent->invite->rfp->pr;
        } else if ($aoq->abstract_parent->invite->itb){
            $aoq->pr = $aoq->abstract_parent->invite->itb->pr;
        } else if ($aoq->abstract_parent->invite->rei){
            $aoq->pr = $aoq->abstract_parent->invite->rei->pr;
        }

        foreach($aoq->abstract_parent->docs as $doc){
            if($doc->document_type === 'f1') {
                $doc->document_type = 'Mayor\'s Business Permit';
            } else if ($doc->document_type === 'f2') {
                $doc->document_type = 'BIR Certificate of Registration';
            } else if ($doc->document_type === 'f3') {
                $doc->document_type = 'Professional License / Curriculum Vitae (Consulting Services)';
            } else if ($doc->document_type === 'f4') {
                $doc->document_type = 'PhilGEPS Registration Number';
            } else if ($doc->document_type === 'f5') {
                $doc->document_type = 'PCAB License(Infra.)';
            } else if ($doc->document_type === 'f6') {
                $doc->document_type = 'NFCC(Infra.)';
            } else if ($doc->document_type === 'f7') {
                $doc->document_type = 'Income / Business Tax Return';
            } else if ($doc->document_type === 'f8') {
                $doc->document_type = 'Notarized Omnnibus Sworn Statement';
            } else if ($doc->document_type === 'f9') {
                $doc->document_type = $doc->extra_document;
            }
        }

        foreach($aoq->abstract_parent->recommendation->recommendation_items as $reco_items){
            $pr_item_ids = explode(",", $reco_items->pr_item_ids);

            $pr_item_nos = "";
            if($reco_items->pr_item_type === 'parent') {
                for($pr_item_ctr = 0; $pr_item_ctr < count($pr_item_ids); $pr_item_ctr++){
                    $pr_item = PurchaseRequestItem::find($pr_item_ids[$pr_item_ctr]);
    
                    if ($pr_item_ctr != (count($pr_item_ids) - 1))
                        $pr_item_nos .= $pr_item->item_no . ",";
                    else
                        $pr_item_nos .= $pr_item->item_no;
                }
            } else {
                if($aoq->pr->pr_item_total->is_lot_purchase === 1) {
                    for($pr_item_ctr = 0; $pr_item_ctr < count($pr_item_ids); $pr_item_ctr++){
                        $pr_item = PurchaseRequestItem::find($pr_item_ids[$pr_item_ctr]);
        
                        if ($pr_item_ctr != (count($pr_item_ids) - 1))
                            $pr_item_nos .= $pr_item->item_no . ",";
                        else
                            $pr_item_nos .= $pr_item->item_no;
                    }
                } else {
                    for($pr_sub_item_ctr = 0; $pr_sub_item_ctr < count($pr_item_ids); $pr_sub_item_ctr++){
                        $pr_sub_item = PurchaseRequestSubItem::find($pr_item_ids[$pr_sub_item_ctr]);
        
                        if ($pr_sub_item_ctr != (count($pr_item_ids) - 1))
                            $pr_item_nos .= $pr_sub_item->item_no . ",";
                        else
                            $pr_item_nos .= $pr_sub_item->item_no;
                    }
                }
            }

            $reco_items->pr_item_nos = $pr_item_nos;
        }

        if(Auth::user()->user_role === 3) {
            SystemNotification::checkURL();
        }
        
        return view('aoq.show')->with('abstract_form', $aoq);
    }

    public function edit(AOQ $aoq){
        if(!$aoq) {
            abort(404);
        }
        $proc_modes = ProcurementMode::whereNotIn('id', ['1', '2', '3'])->pluck('mode', 'id');

        if($aoq->abstract_parent->invite->rfq){
            $aoq->pr = $aoq->abstract_parent->invite->rfq->pr;
        } else if ($aoq->abstract_parent->invite->rfp) {
            $aoq->pr = $aoq->abstract_parent->invite->rfp->pr;
        } else if ($aoq->abstract_parent->invite->itb){
            $aoq->pr = $aoq->abstract_parent->invite->itb->pr;
        } else if ($aoq->abstract_parent->invite->rei){
            $aoq->pr = $aoq->abstract_parent->invite->rei->pr;
        }

        $document_types = [
            'f1' => 'Mayor\'s Business Permit',
            'f2' => 'BIR Certificate of Registration',
            'f3' => 'Professional License / Curriculum Vitae (Consulting Services)',
            'f4' => 'PhilGEPS Registration Number',
            'f5' => 'PBAC License(Infra.)',
            'f6' => 'NFCC(Infra.)',
            'f7' => 'Income / Business Tax Return',
            'f8' => 'Notarized Omnibus Sworn Statement',
            'f9' => 'Others'
        ];

        $reco_items_edit = [];
        foreach($aoq->abstract_parent->recommendation->recommendation_items as $reco_items){
            $pr_item_ids = explode(",", $reco_items->pr_item_ids);
            $reco_items->pr_item_ids_array = $pr_item_ids;
            $pr_item_nos = "";
            if($reco_items->pr_item_type === 'parent') {
                for($pr_item_ctr = 0; $pr_item_ctr < count($pr_item_ids); $pr_item_ctr++){
                    $pr_item = PurchaseRequestItem::find($pr_item_ids[$pr_item_ctr]);
    
                    if ($pr_item_ctr != (count($pr_item_ids) - 1)) {
                        $pr_item_nos .= $pr_item->item_no . ",";
                        $reco_items_edit[$pr_item->id] = $pr_item->item_no;
                        $pr_item_ids_edit[] = $pr_item->id;
                    } else {
                        $pr_item_nos .= $pr_item->item_no;
                        $reco_items_edit[$pr_item->id] = $pr_item->item_no;
                        $pr_item_ids_edit[] = $pr_item->id;
                    }
                }
            } else {
                if($aoq->pr->pr_item_total->is_lot_purchase === 1) {
                    for($pr_item_ctr = 0; $pr_item_ctr < count($pr_item_ids); $pr_item_ctr++){
                        $pr_item = PurchaseRequestItem::find($pr_item_ids[$pr_item_ctr]);
        
                        if ($pr_item_ctr != (count($pr_item_ids) - 1)) {
                            $pr_item_nos .= $pr_item->item_no . ",";
                            $reco_items_edit[$pr_item->id] = $pr_item->item_no;
                            $pr_item_ids_edit[] = $pr_item->id;
                        } else {
                            $pr_item_nos .= $pr_item->item_no;
                            $reco_items_edit[$pr_item->id] = $pr_item->item_no;
                            $pr_item_ids_edit[] = $pr_item->id;
                        }
                    }
                } else {
                    for($pr_sub_item_ctr = 0; $pr_sub_item_ctr < count($pr_item_ids); $pr_sub_item_ctr++){
                        $pr_sub_item = PurchaseRequestSubItem::find($pr_item_ids[$pr_sub_item_ctr]);
        
                        if ($pr_sub_item_ctr != (count($pr_item_ids) - 1)) {
                            $pr_item_nos .= $pr_sub_item->item_no . ",";
                            $reco_items_edit[$pr_sub_item->id] = $pr_sub_item->item_no;
                            $pr_item_ids_edit[] = $pr_sub_item->id;
                        } else {
                            $pr_item_nos .= $pr_sub_item->item_no;
                            $reco_items_edit[$pr_sub_item->id] = $pr_sub_item->item_no;
                            $pr_item_ids_edit[] = $pr_sub_item->id;
                        }
                    }
                }
            }

            $reco_items->pr_item_nos = $pr_item_nos;
        }
        
        
        $reco_suppliers = [];
        foreach($aoq->abstract_parent->suppliers as $supplier) {
            $reco_suppliers[$supplier->id] = $supplier->company_name;
        }

        return view('aoq.edit')->with('abstract_form', $aoq)
                               ->with('document_types', $document_types)
                               ->with('proc_modes', $proc_modes)
                               ->with('reco_suppliers', $reco_suppliers)
                               ->with('reco_items_edit', $reco_items_edit)
                               ->with('abstract_type', 'aoq');
    }

    public function update(Request $request, AbstractParent $abstract){
        $abstract->delete();

        $abstract->save();

        $aoq = new AOQ;
        $aoq->opening_date = $request->opening_date;
        $aoq->procurement_mode_id = $request->proc_mode_type;
        $aoq->abstract_id = $abstract->id;
        $aoq->canvassed_by = $request->canvassed_by;
        $aoq->canvassed_by_designation = $request->canvassed_designation;
        $aoq->recommending_approval = $request->recommending_approval;
        $aoq->recommending_designation = $request->recommending_designation;
        $aoq->approved_by = $request->approved_by;
        $aoq->approved_by_designation = $request->approved_by_designation;
        $aoq->save();

        if($aoq){

            $purchase_type = "";

            //save supplier cost on line items
            if ($abstract->invite->type == 'REI'){
                if($abstract->invite->rei->pr->pr_item_total->is_lot_purchase){
                    $purchase_type = "per lot";
                }else if ($abstract->invite->rei->pr->pr_item_total->is_per_item_purchase){
                    $purchase_type = "per item";
                }
                $pr_items = $abstract->invite->rei->pr->pr_items;
            }else if ($abstract->invite->type == 'RFQ'){
                if($abstract->invite->rfq->pr->pr_item_total->is_lot_purchase){
                    $purchase_type = "per lot";
                }else if ($abstract->invite->rfq->pr->pr_item_total->is_per_item_purchase){
                    $purchase_type = "per item";
                }
                $pr_items = $abstract->invite->rfq->pr->pr_items;
            }else if ($abstract->invite->type == 'RFP'){
                if($abstract->invite->rfp->pr->pr_item_total->is_lot_purchase){
                    $purchase_type = "per lot";
                }else if ($abstract->invite->rfp->pr->pr_item_total->is_per_item_purchase){
                    $purchase_type = "per item";
                }
                $pr_items = $abstract->invite->rfp->pr->pr_items;
            }else if ($abstract->invite->type == 'IB'){
                if($abstract->invite->itb->pr->pr_item_total->is_lot_purchase){
                    $purchase_type = "per lot";
                }else if ($abstract->invite->itb->pr->pr_item_total->is_per_item_purchase){
                    $purchase_type = "per item";
                }
                $pr_items = $abstract->invite->itb->pr->pr_items;
            }

            $supplier_ctr = 0;
            foreach($request->supplierList as $supplier){
                if ($purchase_type == "per lot") {
                    $abstract->suppliers()->attach($supplier, [
                        'ranking'       => $request->input("supplier_ranking".$supplier),
                        'compliance'    => $request->input("result_compliance_supplier_id_".$supplier),
                    ]);

                    foreach($pr_items as $pr_item){
                        // if pr item doesnt have sub item
                        if(!count($pr_item->pr_sub_items)){
                            $pr_item->suppliers()->attach($supplier,[
                                'abstract_id' => $abstract->id,
                                'unit_cost'   => $request->input('unit_cost_supplier_'.$supplier.'_pr_item_'.$pr_item->id),
                                'total'       => $request->input('total_cost_supplier_'.$supplier.'_pr_item_'.$pr_item->id)
                            ]);
                        } else {
                            foreach($pr_item->pr_sub_items as $subItem){
                                $subItem->suppliers()->attach($supplier, [
                                    'abstract_id' => $abstract->id,
                                    'unit_cost' => $request->input('sub_unit_cost_supplier_'.$supplier.'_pr_sub_item_'.$subItem->id),
                                    'total'       => $request->input('sub_total_cost_supplier_'.$supplier.'_pr_sub_item_'.$subItem->id)
                                ]);
                            }
                        }
                    }
                } else if ($purchase_type == "per item") {
                    $abstract->suppliers()->attach($supplier, [
                        'compliance'    => $request->input("result_compliance_supplier_id_".$supplier),
                    ]);

                    foreach($pr_items as $pr_item){
                        // if pr item doesnt have sub item
                        if(!count($pr_item->pr_sub_items)){
                            $pr_item->suppliers()->attach($supplier,[
                                'abstract_id' => $abstract->id,
                                'unit_cost'   => $request->input('unit_cost_supplier_'.$supplier.'_pr_item_'.$pr_item->id),
                                'total'       => $request->input('total_cost_supplier_'.$supplier.'_pr_item_'.$pr_item->id),
                                'per_item_ranking' => $request->input('ranking_supplier_'.$supplier.'_pr_item_'.$pr_item->id.'_col_'.$supplier_ctr)
                            ]);
                        } else {
                            foreach($pr_item->pr_sub_items as $subItem){
                                $subItem->suppliers()->attach($supplier, [
                                    'abstract_id' => $abstract->id,
                                    'unit_cost' => $request->input('sub_unit_cost_supplier_'.$supplier.'_pr_sub_item_'.$subItem->id),
                                    'total'       => $request->input('sub_total_cost_supplier_'.$supplier.'_pr_sub_item_'.$subItem->id),
                                    'per_item_ranking' => $request->input('ranking_supplier_'.$supplier.'_pr_sub_item_'.$subItem->id.'_col_'.$supplier_ctr)
                                ]);
                            }
                        }
                    }
                }
                $supplier_ctr++;
            }
            

            /********************************************************************************/

            //save the eligibility docs
            $doc_count = count($request->eligibility_docs_count) - 1;
            if ($doc_count > 0){
                $saved_docs = 0;
                $doc_ctr = 1;
                while($saved_docs != $doc_count){
                    if ($request->input("eligibility_docs_" . $doc_ctr) !== null){

                        $abstract_doc = new AbstractDocument;
                        $abstract_doc->abstract_id = $abstract->id;
                        $abstract_doc->document_type = $request->input("eligibility_docs_" . $doc_ctr);
                        if ($request->input("eligibility_docs_" . $doc_ctr) == "f9"){
                            $abstract_doc->extra_document = $request->input("eligibility_docs_extra_".$doc_ctr);
                        }
                        $abstract_doc->save();

                        if($abstract_doc){
                            $supplier_count = count($request->supplierList);
                            for($i=0;$i<$supplier_count;$i++){
                                $abs_sup_eligibility = new AbstractSupplierEligibility;
                                $abs_sup_eligibility->abstract_docs_id = $abstract_doc->id;
                                $abs_sup_eligibility->supplier_id = $request->input("supplier".$i);
                                $abs_sup_eligibility->eligibility = $request->input("eligibility_docs_" . $doc_ctr."_supplier".$i);
                                $abs_sup_eligibility->save();
                            }
                        }
                        $saved_docs++;
                    } 
                    $doc_ctr++;
                }
            }

            /********************************************************************************/

            //Save the recommendations
            $abstract_reco = new AbstractRecommendation;
            $abstract_reco->abstract_id = $abstract->id;
            $abstract_reco->total = $request->reco_total;
            $abstract_reco->save();

            if ($abstract_reco){
                $reco_count = count($request->reco_row_count) - 1;
                if ($reco_count > 0){
                    $saved_reco = 0;
                    $reco_ctr = 1;
                    foreach($pr_items as $item){
                        $subItemCount = count($item->pr_sub_items);
                    }
                    while($saved_reco != $reco_count){
                        if ($request->input("reco_supplier_" . $reco_ctr) !== null){
                            $reco_item              = new AbstractRecoItem;
                            $reco_item->reco_id     = $abstract_reco->id;
                            $reco_item->supplier_id = $request->input("reco_supplier_".$reco_ctr);
                            $reco_item->pr_item_ids  = implode(',', $request->input("reco_item_no_".$reco_ctr));
                            $reco_item->subtotal    = $request->input("reco_sub_total_".$reco_ctr);
                            $reco_item->remarks     = $request->input("reco_remarks_".$reco_ctr);
                            if($subItemCount > 0) {
                                $reco_item->pr_item_type = 'sub';
                            } else {
                                $reco_item->pr_item_type = 'parent';
                            }
                            $reco_item->save();
                            $saved_reco++;
                        }
                        $reco_ctr++;
                    }
                }
            }
        }
        return redirect('/aoq/'.$aoq->id)->with('success', 'Abstract of Quotation successfully updated!');
    }
}
