<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProjectProcurementManagementPlan;
use App\ProcurementMode;
use App\AnnualProcurementPlan;
use App\ProjectsProgramsActivities;
use App\APPSchedule;
use Auth;
use App\AbstractRecoItem as RecoItem;
use App\Http\Resources\APPCollection;
use App\MovingApp;
use SystemNotification;
use App\User;
use App\Notifications\EmailNotification;
use App\CategoryCode;
use Excel;
use Illuminate\Support\Str;

class APPController extends Controller
{
    public function indexApp()
    {
        $apps = AnnualProcurementPlan::all()->unique('app_year');

        return view('app.index')->with('apps', $apps);
    }

    public function movingApp(){
        $current_year = date("Y");
        $apps = AnnualProcurementPlan::where('app_year', $current_year)->get();

        foreach($apps as $app){
            if ($app->pap->moving_app){
                $app->total_estimated_budget = number_format($app->pap->moving_app->actual_budget, 2, '.', ',');
            }
        }

        return view('app.moving-index')->with('moving_apps', $apps)
                                       ->with('app_year', $current_year);
    }

    public function generateApp(Request $request){
        $ppmps = ProjectProcurementManagementPlan::whereIn('id', $request->p_id)->get();
        $app_rows_delete = AnnualProcurementPlan::where('app_year', $ppmps[0]->for_year)->get();
        if($app_rows_delete){
            foreach($app_rows_delete as $row_delete){
                $row_delete->delete();
            }
        }
        $all_paps = collect();
        foreach($ppmps as $ppmp) {
            foreach($ppmp->paps as $pap){
                $all_paps->push($pap); 
            }
        }
        $sorted_by_category_code = $all_paps->sortBy('category_code');
        $sorted_by_cc_and_description = collect();
        foreach(CategoryCode::all() as $code) {
            foreach($sorted_by_category_code->where('category_code', $code->id)->sortBy('general_description') as $pap_sorted_code) {
                $sorted_by_cc_and_description->push($pap_sorted_code);
            }
        }
        foreach($sorted_by_cc_and_description as $sbccd) {
            $app                         = new AnnualProcurementPlan;
            $app->code                   = $sbccd->code;
            $app->program_project        = $sbccd->general_description;
            $app->pmo_end_user           = $sbccd->ppmp->end_user;
            $app->mode_of_procurement    = $sbccd->procurement_modes[0]->id;
            $app->source_of_funds        = 'GoP';
            $app->total_estimated_budget = $sbccd->estimated_budget;
            $app->remarks                = null;
            $app->app_year               = $sbccd->ppmp->for_year;
            $fy                          = $sbccd->ppmp->for_year;
            $app->mooe                   = $sbccd->mooe;
            $app->co                     = $sbccd->co;
            $app->pap()->associate($sbccd);
            $app->save();

            $months = [];
            foreach($sbccd->pap_schedules as $pap_sched){
                if($pap_sched->allocation != "0.00" || $pap_sched->allocation != '0'){
                    $months[] = $pap_sched->month;
                }
            }
            $months_string        = implode(', ', $months);
            $mode                 = $app->mode_of_procurement;
            $app_schedule         = new APPSchedule;
            $app_schedule->app_id = $app->id;
            $complete             = [1,2,3,4];
            $empty_1_2            = [5,6,7,12,13,14,15,16,17,18,21,22,24,25];
            $empty_2_only         = [9,10,11,19,20,23];

            if(in_array($mode, $complete)){
                $app_schedule->advertisement    = $months_string;
                $app_schedule->submission       = $months_string;
                $app_schedule->notice_of_award  = $months_string;
                $app_schedule->contract_signing = $months_string;
            } else if (in_array($mode, $empty_1_2)){
                $app_schedule->advertisement    = 'N/A';
                $app_schedule->submission       = 'N/A';
                $app_schedule->notice_of_award  = $months_string;
                $app_schedule->contract_signing = $months_string;
            } else if (in_array($mode, $empty_2_only)){
                $app_schedule->advertisement    = $months_string;
                $app_schedule->submission       = 'N/A';
                $app_schedule->notice_of_award  = $months_string;
                $app_schedule->contract_signing = $months_string;
            } else {
                $app_schedule->advertisement    = 'N/A';
                $app_schedule->submission       = 'N/A';
                $app_schedule->notice_of_award  = 'N/A';
                $app_schedule->contract_signing = 'N/A';
            }
            $app_schedule->save();
        }

        $log_arr = [
            "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            "user_office" => Auth::user()->office->office,
            "activity"  => 'generated an APP for Year - ' . $ppmps[0]->for_year . '.',
        ];
        \ActivityLog::add($log_arr);

        return redirect('/app/view/'.$ppmps[0]->for_year);
    }

    public function generateApp2(Request $request){
        $ppmps = ProjectProcurementManagementPlan::whereIn('id', $request->p_id)->get();
        $ppmps->fy = ProjectProcurementManagementPlan::where('id', $request->p_id[0])->pluck('for_year');
        $procurement_modes = ProcurementMode::where('id', '!==', 8)->get();

        $app_rows_delete = AnnualProcurementPlan::where('app_year', $ppmps->fy[0])->get();
        if($app_rows_delete){
            foreach($app_rows_delete as $row_delete){
                $row_delete->delete();
            }
        }

        foreach($ppmps as $ppmp){
            foreach($ppmp->paps as $pap){
                foreach($pap->procurement_modes as $pap_procurement_mode){
                    $app                         = new AnnualProcurementPlan;
                    $app->code                   = $pap->code;
                    $app->program_project        = $pap->general_description;
                    $app->pmo_end_user           = $ppmp->end_user;
                    $app->mode_of_procurement    = $pap_procurement_mode->id;
                    $app->source_of_funds        = 'GoP';
                    $app->total_estimated_budget = $pap->estimated_budget;
                    $app->remarks                = null;
                    $app->app_year               = $ppmp->for_year;
                    $fy                          = $ppmp->for_year;
                    $app->mooe                   = $pap->mooe;
                    $app->co                     = $pap->co;
                    $app->pap()->associate($pap);
                    $app->save();

                    $months = [];
                    foreach($pap->pap_schedules as $pap_sched){
                        if($pap_sched->allocation != "0.00" || $pap_sched->allocation != '0'){
                            $months[] = $pap_sched->month;
                        }
                    }

                    $months_string = implode(', ', $months);
                    $mode = $app->mode_of_procurement;

                    $app_schedule                   = new APPSchedule;

                    $app_schedule->app_id           = $app->id;
                    $complete = [1,2,3,4];
                    $empty_1_2 = [5,6,7,12,13,14,15,16,17,18,21,22,24,25];
                    $empty_2_only = [9,10,11,19,20,23];

                    if(in_array($mode, $complete)){
                        $app_schedule->advertisement    = $months_string;
                        $app_schedule->submission       = $months_string;
                        $app_schedule->notice_of_award  = $months_string;
                        $app_schedule->contract_signing = $months_string;
                    } else if (in_array($mode, $empty_1_2)){
                        $app_schedule->advertisement    = 'N/A';
                        $app_schedule->submission       = 'N/A';
                        $app_schedule->notice_of_award  = $months_string;
                        $app_schedule->contract_signing = $months_string;
                    } else if (in_array($mode, $empty_2_only)){
                        $app_schedule->advertisement    = $months_string;
                        $app_schedule->submission       = 'N/A';
                        $app_schedule->notice_of_award  = $months_string;
                        $app_schedule->contract_signing = $months_string;
                    } else {
                        $app_schedule->advertisement    = 'N/A';
                        $app_schedule->submission       = 'N/A';
                        $app_schedule->notice_of_award  = 'N/A';
                        $app_schedule->contract_signing = 'N/A';
                    }
                    $app_schedule->save();
                }
            }

            $notif_recipient = User::find($ppmp->prepared_by);
            $obj = new \stdClass();
            $obj->message = "You can now view the APP for Year - ". $fy;
            $obj->link = url('/app/view/'.$fy);
            SystemNotification::add($notif_recipient, $obj);
            $notif_recipient->notify(new EmailNotification($obj));

        }

        $app_rows = AnnualProcurementPlan::where('app_year', $ppmps->fy[0])->get();

        foreach($app_rows as $app_row) {
            foreach($procurement_modes as $mode) {
                if($app_row->mode_of_procurement == $mode->id){
                    $app_row->mode_of_procurement = $mode->mode;
                }
            }
        }

        $log_arr = [
            "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            "user_office" => Auth::user()->office->office,
            "activity"  => 'generated an APP for Year - ' . $ppmps->fy . '.',
        ];
        \ActivityLog::add($log_arr);

        return redirect('/app/view/'.$fy);
    }

    public function showApp($year){
        $app = AnnualProcurementPlan::where('app_year', $year)->get();

        if(Auth::user()->user_role === 7) {
            SystemNotification::checkURL();
        }
        
        if (Auth::user()->user_role == 4 || Auth::user()->user_role == 7){
            $app = AnnualProcurementPlan::where('pmo_end_user', Auth::user()->office_id)->get();
        }

        $procurement_modes = ProcurementMode::all();
        foreach($procurement_modes as $procurement_mode){
            foreach($app as $app_row){
                if($app_row->mode_of_procurement == $procurement_mode->id){
                    $app_row->mode_of_procurement = $procurement_mode->mode;
                }
            }
        }

        $log_arr = [
            "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            "user_office" => Auth::user()->office->office,
            "activity"  => ' vieweed APP for Year - ' . $year . '.',
        ];
        \ActivityLog::add($log_arr);

        return view('app.show')
        ->with('app', $app)
        ->with('app_year', $year);
    }

    public function edit($year){
        $appEditUsers = [1,3,4,8];
	    abort_if(!in_array(Auth::user()->user_role, $appEditUsers), 404);
        $app = AnnualProcurementPlan::where('app_year', $year)->get();
        $fy = AnnualProcurementPlan::where('app_year', $year)->pluck('app_year')[0];
        $procurement_modes = ProcurementMode::all();

        foreach($app as $app_row) {
            foreach($procurement_modes as $mode) {
                if($app_row->mode_of_procurement == $mode->id){
                    $app_row->mode_of_procurement = $mode->mode;
                }
            }
        }

        return view('app.edit')->with('app', $app)->with('year', $fy);
    }

    public function update(Request $request, $year){
        for($i = 1; $i <= count($request->input('row_counter')); $i++){
            $app = AnnualProcurementPlan::updateOrCreate(
                ['id' => $request->input('app_id'.$i)],
                [
                    // 'code' => $request->input('app_code'.$i),
                    'remarks' => $request->input('app_remarks'.$i)
                ]
            );

            $app_sched = APPSchedule::updateOrCreate(
                ['app_id' => $request->input('app_id'.$i)],
                [
                    'advertisement'     => $request->input('advertisement'.$i),
                    'submission'        => $request->input('submission'.$i),
                    'notice_of_award'   => $request->input('notice_of_award'.$i),
                    'contract_signing'  => $request->input('contract_signing'.$i)
                ]
            );
        }

        $log_arr = [
            "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            "user_office" => Auth::user()->office->office,
            "activity"  => ' edited APP for Year - ' . $year . '.',
        ];
        \ActivityLog::add($log_arr);

        return back()->with('success', 'APP Successfully updated!');
    }

    public function getAPPs($year){
        $apps = AnnualProcurementPlan::where('app_year', $year)->get();
        $completed = collect();
        $ongoing = collect();
        $recoItems = RecoItem::all();
        $mergedRecoItemIDsArray = [];
        foreach($recoItems as $recoItem){
            $prItemIDsFromReco = explode(',', $recoItem->pr_item_ids);
            foreach($prItemIDsFromReco as $a){
                $mergedRecoItemIDsArray[] = $a;
            }
        }
        $recoItemUnique = array_unique($mergedRecoItemIDsArray);

        foreach($apps as $app){
            if(count($app->pr_item)){
                foreach($app->pr_item as $item){
                    foreach($recoItems as $recoItem){
                        $prItemIDsFromReco = explode(',', $recoItem->pr_item_ids);
                        if(in_array($item->id, $prItemIDsFromReco)){
                            if(count($recoItem->recommendation->abstract_parent->contracts)) {
                                $completed->push($app);
                            } else {
                                $ongoing->push($app);
                            }
                        } else {
                            $ongoing->push($app);
                        }
                    }
                }
            }
        }

        return [
            'completed' => new APPCollection($completed),
            'ongoing' => new APPCollection($ongoing)
        ];
    }

    public function newAPPShow() {
        return view('app.show-2');
    }

    public function import(Request $request) {
        $request->validate([
            'file' => 'required',
            'year' => 'required'
        ]);

        if($request->hasFile('file')) {
            $extension = \File::extension($request->file->getClientOriginalName());
            $year = $request->year;
            $valid = ['xlsx', 'xls', 'csv'];
            if(in_array($extension, $valid)) {
                $path = $request->file->getRealPath();
                Excel::selectSheets('apps')->load($path, function($reader) use ($year) {
                    foreach($reader->toArray() as $row) {
                        if($row['pap_id'] !== '') {
                            $pap                         = ProjectsProgramsActivities::find($row['pap_id']);
                            $app                         = new AnnualProcurementPlan;
                            $app->code                   = $pap->code;
                            $app->program_project        = $pap->general_description;
                            $app->pmo_end_user           = $pap->ppmp->end_user;
                            $app->mode_of_procurement    = $pap->procurement_modes[0]->id;
                            $app->source_of_funds        = 'GoP';
                            $app->total_estimated_budget = $pap->estimated_budget;
                            $app->remarks                = null;
                            $app->app_year               = $year;
                            $app->mooe                   = $pap->mooe;
                            $app->co                     = $pap->co;
                            $app->pap()->associate($pap)->save();

                            $months = [];
                            foreach($pap->pap_schedules as $pap_sched) {
                                if($pap_sched->allocation != '0.00' || $pap_sched->allocation != '0') {
                                    $months[] = $pap_sched->month;
                                }
                            }
                            $months_string = implode(', ', $months);
                            $mode = $app->mode_of_procurement;
                            $app_schedule = new APPSchedule;
                            $app_schedule->app_id = $app->id;
                            $complete = [1,2,3,4];
                            $empty_1_2 = [5,6,7,12,13,14,15,16,17,18,21,22,24,25];
                            $empty_2_only = [9,10,11,19,20,23];

                            if(in_array($mode, $complete)) {
                                $app_schedule->advertisement    = $months_string;
                                $app_schedule->submission       = $months_string;
                                $app_schedule->notice_of_award  = $months_string;
                                $app_schedule->contract_signing = $months_string;
                            } else if (in_array($mode, $empty_1_2)) {
                                $app_schedule->advertisement    = 'N/A';
                                $app_schedule->submission       = 'N/A';
                                $app_schedule->notice_of_award  = $months_string;
                                $app_schedule->contract_signing = $months_string;
                            } else if (in_array($mode, $empty_2_only)) {
                                $app_schedule->advertisement    = $months_string;
                                $app_schedule->submission       = 'N/A';
                                $app_schedule->notice_of_award  = $months_string;
                                $app_schedule->contract_signing = $months_string;
                            } else {
                                $app_schedule->advertisement    = 'N/A';
                                $app_schedule->submission       = 'N/A';
                                $app_schedule->notice_of_award  = 'N/A';
                                $app_schedule->contract_signing = 'N/A';
                            }
                            $app_schedule->save();
                        }
                    }
                });
                return redirect()->back()->with('success', 'APP imported successfully.');
            } else {
                return redirect()->back()->with('error', 'Please upload "csv/xls/xlsx" files only.');
            }
        } else {
            return redirect()->back()->with('error', 'Something went wrong. Please try again later.');
        }
    }

    public function downloadTemplate() {
        $filepath = storage_path('appimporttemplate.xlsx');
        return response()->download($filepath, time() . '.xlsx');
    }

    public function downloadDeductedTemplate() {
        $filepath = storage_path('appdeductedtemplate.xlsx');
        return response()->download($filepath, time().'.xlsx');
    }

    public function import_deducted_app(Request $request) {
        $request->validate([
            'file' => 'required',
        ]);

        if($request->hasFile('file')) {
            $extension = \File::extension($request->file->getClientOriginalName());
            $valid = ['xlsx', 'xls', 'csv'];
            if(in_array($extension, $valid)) {
                $path = $request->file->getRealPath();
                Excel::selectSheets('apps')->load($path, function($reader) {
                    foreach($reader->toArray() as $row) {
                        if($row['pap_id'] !== '') {
                            $app                = new MovingApp;
                            $app->pap_id        = $row['pap_id'];
                            $app->actual_budget = $row['actual_budget'];
                            $app->save();
                        }
                    }
                });
                return redirect()->back()->with('success', 'APP imported successfully.');
            } else {
                return redirect()->back()->with('error', 'Please upload "csv/xls/xlsx" files only.');
            }
        } else {
            return redirect()->back()->with('error', 'Something went wrong. Please try again later.');
        }
    }
}