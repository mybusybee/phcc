<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AbstractParent;
use App\Contract;
use App\OrdersParent;
use App\Notifications\EmailNotification;
use SystemNotification;
use Auth;
use App\User;

class ContractController extends Controller
{
    private function ordinalTransform($number) {
        $ends = array('th','st','nd','rd','th','th','th','th','th','th');
        if ((($number % 100) >= 11) && (($number%100) <= 13))
            return $number. 'th';
        else
            return $number. $ends[$number % 10];
    }

    public function create(AbstractParent $abstract){
        
        $orders = OrdersParent::all();
        $date = new \stdClass();
        $date->ordinalDays = [];
        $date->months = [];
        $date->years = [];
        $months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        foreach($months as $month){
            $date->months[$month] = $month;
        }
        foreach(range(date('Y'), date('Y') + 10) as $year){
            $date->years[$year] = $year;
        }
        foreach(range(1, 31) as $days){
            $date->ordinalDays[$this->ordinalTransform($days)] = $this->ordinalTransform($days);
        }
        return view('contract.create')->with('abstract', $abstract)
                                      ->with('ordersCount', count($orders))
                                      ->with('date', $date);
    }

    public function store(Request $request) {
        $request->validate([
            'form_no' => 'required|unique:orders_parents'
        ]);
        $order              = new OrdersParent;
        $order->form_no     = $request->form_no;
        $order->abstract_id = $request->abstract_id;
        $order->status      = 'FOR_EXPORT';
        $order->type        = 'CONTRACT';
        $order->save();

        $contract                                 = new Contract;
        $contract->supplier_id                    = $request->supplier_id;
        $contract->agreement_year                 = $request->agreement_year;
        $contract->agreement_day                  = $request->agreement_day;
        $contract->agreement_month                = $request->agreement_month;
        $contract->procurement_entity_name        = $request->procuring_entity;
        $contract->brief_description              = $request->brief_description;
        $contract->contract_price                 = $request->contract_price;
        $contract->pcc_signatory                  = $request->pcc_signatory;
        $contract->pcc_signatory_designation      = $request->pcc_signatory_designation;
        $contract->supplier_signatory             = $request->supplier_signatory;
        $contract->supplier_signatory_designation = $request->supplier_signatory_designation;
        $contract->witness_1                      = $request->witness_1;
        $contract->witness_1_designation          = $request->witness_1_designation;
        $contract->witness_2                      = $request->witness_2;
        $contract->witness_2_designation          = $request->witness_2_designation;
        $contract->order()->associate($order)->save();

        if($contract->order->abstract->invite->type === 'RFQ'){
            $contract->pr = $contract->order->abstract->invite->rfq->pr;
        } else if ($contract->order->abstract->invite->type === 'RFP') {
            $contract->pr = $contract->order->abstract->invite->rfp->pr;
        } else if ($contract->order->abstract->invite->type === 'IB') {
            $contract->pr = $contract->order->abstract->invite->itb->pr;
        } else if ($contract->order->abstract->invite->type === 'REI') {
            $contract->pr = $contract->order->abstract->invite->rei->pr;
        }

        $notif_recipient = User::find($contract->pr->requested_by);
        $obj = new \stdClass();
        $obj->message = Auth::user()->fullName . ' generated Contract#'.$contract->order->form_no.' for PR#'.$contract->pr->pr_no;
        $obj->link = url('/pr/'.$contract->pr->id);
        SystemNotification::add($notif_recipient, $obj);
        $notif_recipient->notify(new EmailNotification($obj));

        $adminUsers = User::where('user_role', 3)->get();
        foreach($adminUsers as $adminUser){
            $obj = new \stdClass();
            $obj->message = Auth::user()->fullName . ' generated JO#'.$contract->order->form_no.'.';
            $obj->link = url('/contract/'.$contract->id);
            SystemNotification::add($adminUser, $obj);
            $adminUser->notify(new EmailNotification($obj));
        }

        return redirect('/contract/'.$contract->id)->with('success', 'Contract successfully saved!');
    }

    public function show(Contract $contract){
        if(Auth::user()->user_role === 3) {
            SystemNotification::checkURL();
        }
        return view('contract.show')->with('contract', $contract);
    }

    public function index(){
        $contracts = Contract::orderBy('created_at', 'desc')->get();
        foreach($contracts as $contract){
            if ($contract->abstract->invite->type == 'RFQ'){
                $contract->pr = $contract->abstract->invite->rfq->pr;
            }elseif ($contract->abstract->invite->type == 'RFP'){
                $contract->pr = $contract->abstract->invite->rfp->pr;
            }elseif ($contract->abstract->invite->type == 'IB'){
                $contract->pr = $contract->abstract->invite->itb->pr;
            }elseif ($contract->abstract->invite->type == 'REI'){
                $contract->pr = $contract->abstract->invite->rei->pr;
            }
        }

        return view('contract.index')->with('contracts', $contracts);
    }

    public function edit(Contract $contract) {
        $date = new \stdClass();
        $date->ordinalDays = [];
        $date->months = [];
        $date->years = [];
        $months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
        foreach($months as $month){
            $date->months[$month] = $month;
        }
        foreach(range(date('Y'), date('Y') + 10) as $year){
            $date->years[$year] = $year;
        }
        foreach(range(1, 31) as $days){
            $date->ordinalDays[$this->ordinalTransform($days)] = $this->ordinalTransform($days);
        }
        return view('contract.edit')->with('date', $date)->with('contract', $contract);
    }

    public function update(Request $request, Contract $contract) {
        $contract->agreement_year                 = $request->agreement_year;
        $contract->agreement_day                  = $request->agreement_day;
        $contract->agreement_month                = $request->agreement_month;
        $contract->procurement_entity_name        = $request->procuring_entity;
        $contract->brief_description              = $request->brief_description;
        $contract->contract_price                 = $request->contract_price;
        $contract->pcc_signatory                  = $request->pcc_signatory;
        $contract->pcc_signatory_designation      = $request->pcc_signatory_designation;
        $contract->supplier_signatory             = $request->supplier_signatory;
        $contract->supplier_signatory_designation = $request->supplier_signatory_designation;
        $contract->witness_1                      = $request->witness_1;
        $contract->witness_1_designation          = $request->witness_1_designation;
        $contract->witness_2                      = $request->witness_2;
        $contract->witness_2_designation          = $request->witness_2_designation;
        $contract->save();

        return redirect('/contract/'.$contract->id)->with('success', 'Contract successfully updated!');
    }
}
