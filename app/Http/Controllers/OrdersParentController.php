<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OrdersParent as OP;
use Auth;
use App\OrderAttachment as OA;
use EmailNotifier;
use Mail;
use Carbon\Carbon;
use App\AbstractParent;
use App\Supplier;
use App\PurchaseRequestItem;
use Excel;
use Storage;

class OrdersParentController extends Controller
{
    public function index(){
        $order_exclusive_forms = collect();
        $orders = OP::orderBy('form_no', 'desc')->get();
        if (Auth::user()->user_role === 5){
            foreach($orders as $order){
                if ($order->abstract->invite->type == 'RFQ'){
                    if ($order->abstract->invite->rfq->pr->proc_user->id === Auth::user()->id)
                        $order_exclusive_forms->push($order);
                }elseif ($order->abstract->invite->type == 'RFP'){
                    if ($order->abstract->invite->rfp->pr->proc_user->id === Auth::user()->id)
                        $order_exclusive_forms->push($order);
                }elseif ($order->abstract->invite->type == 'IB'){
                    if ($order->abstract->invite->itb->pr->proc_user->id === Auth::user()->id)
                        $order_exclusive_forms->push($order);
                }elseif ($order->abstract->invite->type == 'REI'){
                    if ($order->abstract->invite->rei->pr->proc_user->id === Auth::user()->id)
                        $order_exclusive_forms->push($order);
                }
            }
            $orders = $order_exclusive_forms;
        }

        foreach($orders as $order){
            if ($order->abstract->invite->type == 'RFQ'){
                $order->pr = $order->abstract->invite->rfq->pr;
            }elseif ($order->abstract->invite->type == 'RFP'){
                $order->pr = $order->abstract->invite->rfp->pr;
            }elseif ($order->abstract->invite->type == 'IB'){
                $order->pr = $order->abstract->invite->itb->pr;
            }elseif ($order->abstract->invite->type == 'REI'){
                $order->pr = $order->abstract->invite->rei->pr;
            }
        }
        
        return view('orders-parent.index')->with('orders', $orders);
    }
    
    public function attach_files(Request $request, OP $order){
        if($request->hasFile('attachments')){
            foreach($request->attachments as $file){
                $filepath = $file->store('jo_po_contracts_attachment/'.$order->form_no);
                $attachments[] = new OA([
                    'original_filename' => $file->getClientOriginalName(),
                    'filepath' => $filepath
                ]);
            }
            $order->attachments()->saveMany($attachments);
            return redirect()->back()->with('success', 'Files successfully uploaded!');
        } else {
            return redirect()->back()->with('error', 'Something went wrong. Please try again.');
        }
    }

    public function delete_files($id){
        $file = OA::find($id);
        \File::delete(storage_path('app/'.$file->filepath));
        $file->delete();

        return redirect()->back()->with('success', 'File attachment removed successfully!');
    }

    public function download_files($id){
        $file = OA::find($id);

        return \Storage::download($file->filepath, $file->original_filename);
    }

    public function send_notice(OP $order){
        $order->status = 'NOTICE_SENT';
        $order->save();

        if($order->type === 'PO') {
            $form_link = null;
            foreach(explode(', ', $order->po->supplier->email) as $email) {
                Mail::send('emails.attachments', ['title' => 'Dear ' . $order->po->supplier->company_name, 'content' => 'Please see attached files.'], function ($message) use ($email, $order)
                {
                    $message->from('info@phcc.gov.ph', 'Philippine Competition Commission');
                    $message->to($email);
                    $subject = 'Philippine Competition Commission Purchase Order#'.$order->form_no;
                    $message->subject($subject);
                    if(count($order->attachments)) {
                        foreach($order->attachments as $attachment) {
                            $message->attach(storage_path('app/'.$attachment->filepath));
                        }
                    }
                    $form_link = $this->exportPO($order->po);
                    $message->attach($form_link);
                });
                Storage::delete($form_link);
            }
        } else {
            $form_link = null;
            foreach(explode(', ', $order->jo->supplier->email) as $email) {
                Mail::send('emails.attachments', ['title' => 'Dear ' . $order->jo->supplier->company_name, 'content' => 'Please see attached files.'], function ($message) use ($email, $order)
                {
                    $message->from('info@phcc.gov.ph', 'Philippine Competition Commission');
                    $message->to($email);
                    $subject = 'Philippine Competition Commission Job Order#'.$order->form_no;
                    $message->subject($subject);
                    if(count($order->attachments)) {
                        foreach($order->attachments as $attachment) {
                            $message->attach(storage_path('app/'.$attachment->filepath));
                        }
                    }
                    $form_link = $this->exportJO($order->jo);
                    $message->attach($form_link);
                });
                Storage::delete($form_link);
            }
        }

        return redirect()->back()->with('success', $order->type . ' successfully sent to supplier.');
    }

    public function exportJO($jo){
        $jo->dateToday = Carbon::now()->format('F d, Y h:i:s A eO');
        $filename = 'JO#'.$jo->ordersParent->form_no.date_timestamp_get(date_create());
        $jo->items = $this->getRecommendedItems($jo->ordersParent->abstract->id, $jo->supplier->id);
        $jo->logo = public_path('img/export/logo.png');
        $jo->address = public_path('img/export/address.png');
        if($jo->ordersParent->abstract->invite->type === 'RFQ'){
            $jo->pr = $jo->ordersParent->abstract->invite->rfq->pr;
        } else if ($jo->ordersParent->abstract->invite->type === 'RFP') {
            $jo->pr = $jo->ordersParent->abstract->invite->rfp->pr;
        } else if ($jo->ordersParent->abstract->invite->type === 'IB') {
            $jo->pr = $jo->ordersParent->abstract->invite->itb->pr;
        } else if ($jo->ordersParent->abstract->invite->type === 'REI') {
            $jo->pr = $jo->ordersParent->abstract->invite->rei->pr;
        }

        if($jo->ordersParent->abstract->type === 'AOB'){
            $jo->procurement_mode = $jo->ordersParent->abstract->aob->procurement_mode->mode;
        } else if($jo->ordersParent->abstract->type === 'AOQ'){
            $jo->procurement_mode = $jo->ordersParent->abstract->aoq->procurement_mode->mode;
        } else if($jo->ordersParent->abstract->type === 'AOP') {
            $jo->procurement_mode = $jo->ordersParent->abstract->aop->procurement_mode->mode;
        }
        $file = Excel::create($filename, function($excel) use ($jo) {
            $excel->sheet('Sheet 1', function($sheet) use ($jo) {
                //sheet options
                $sheet->setOrientation('landscape');
                $sheet->setAutoSize(true);

                $objDrawing = new \PHPExcel_Worksheet_Drawing;
                $objDrawing->setPath(public_path('img/export/logo.png')); //your image path
                $objDrawing->setCoordinates('A1');
                $objDrawing->setWidthAndHeight(250,100);
                $objDrawing->setWorksheet($sheet);
        
                $objDrawing = new \PHPExcel_Worksheet_Drawing;
                $objDrawing->setPath(public_path('img/export/address.png')); //your image path
                $objDrawing->setCoordinates('K1');
                $objDrawing->setWidthAndHeight(250,100);
                $objDrawing->setWorksheet($sheet);

                $sheet->setWidth([
                    'A' => 7.14,
                    'B' => 8.43,
                    'C' => 10.86,
                    'D' => 11,
                    'H' => 3,
                    'I' => 16.43,
                    'J' => 3,
                    'K' => 16.43,
                    'L' => 16.43,
                ]);

                $sheet->setFontSize(10);
                
                //load view
                $sheet->loadView('orders-parent.job-order.export')->with('jo', $jo);
            });
        })->store('xls', storage_path('exports/orders/'), true);

        return $file['full'];
    }

    public function exportPO($po){
        $po->dateToday = Carbon::now()->format('F d, Y h:i:s A eO');
        $filename = 'PO#'.$po->ordersParent->form_no.date_timestamp_get(date_create());
        $po->items = $this->getRecommendedItems($po->ordersParent->abstract->id, $po->supplier->id);
        if($po->ordersParent->abstract->invite->type === 'RFQ'){
            $po->pr = $po->ordersParent->abstract->invite->rfq->pr;
        } else if ($po->ordersParent->abstract->invite->type === 'RFP') {
            $po->pr = $po->ordersParent->abstract->invite->rfp->pr;
        } else if ($po->ordersParent->abstract->invite->type === 'IB') {
            $po->pr = $po->ordersParent->abstract->invite->itb->pr;
        } else if ($po->ordersParent->abstract->invite->type === 'REI') {
            $po->pr = $po->ordersParent->abstract->invite->rei->pr;
        }

        if($po->ordersParent->abstract->type === 'AOB'){
            $po->procurement_mode = $po->ordersParent->abstract->aob->procurement_mode->mode;
        } else if($po->ordersParent->abstract->type === 'AOQ'){
            $po->procurement_mode = $po->ordersParent->abstract->aoq->procurement_mode->mode;
        } else if($po->ordersParent->abstract->type === 'AOP') {
            $po->procurement_mode = $po->ordersParent->abstract->aop->procurement_mode->mode;
        }
        $file = Excel::create($filename, function($excel) use ($po) {
            $excel->sheet('Sheet 1', function($sheet) use ($po) {
                //sheet options
                $sheet->setOrientation('landscape');
                $sheet->setAutoSize(true);

                $objDrawing = new \PHPExcel_Worksheet_Drawing;
                $objDrawing->setPath(public_path('img/export/logo.png')); //your image path
                $objDrawing->setCoordinates('A1');
                $objDrawing->setWidthAndHeight(250,100);
                $objDrawing->setWorksheet($sheet);

                $objDrawing = new \PHPExcel_Worksheet_Drawing;
                $objDrawing->setPath(public_path('img/export/address.png')); //your image path
                $objDrawing->setCoordinates('K1');
                $objDrawing->setWidthAndHeight(250,100);
                $objDrawing->setWorksheet($sheet);

                $sheet->setWidth([
                    'A' => 7.14,
                    'B' => 8.43,
                    'C' => 10.86,
                    'D' => 11,
                    'H' => 3,
                    'I' => 16.43,
                    'J' => 3,
                    'K' => 16.43,
                    'L' => 16.43,
                ]);

                $sheet->setFontSize(10);
                
                //load view
                $sheet->loadView('orders-parent.purchase-order.export')->with('po', $po);
            });
        })->store('xls', storage_path('exports/orders/'), true);

        return $file['full'];
    }

    private function getRecommendedItems($abstractID, $supplierID){
        $abstract = AbstractParent::find($abstractID);
        $supplier = Supplier::find($supplierID);
        foreach($abstract->recommendation->recommendation_items->where('supplier_id', $supplier->id) as $recoItem){
            $prItemIDs = explode(',', $recoItem->pr_item_ids);
        }

        $abstract->invite->pr = $this->return_pr($abstract);

        $items = collect();

        if($abstract->invite->pr->pr_item_total->is_lot_purchase === 1) {
            if($this->checkIfHasSubItems($abstract->invite->pr)) {
                //attach parent item to first
                foreach($prItemIDs as $itemID) {
                    $itemParent = PurchaseRequestItem::find($itemID);
                    $itemParent['type'] = 'parent';
                    $items->push($itemParent);
                    foreach($supplier->pr_sub_items->where('pivot.abstract_id', $abstract->id) as $pr_sub_item){
                        $pr_sub_item['type'] = 'sub';
                        $items->push($pr_sub_item);
                    }
                }
            } else {
                foreach($supplier->pr_items->where('pivot.abstract_id', $abstract->id) as $pr_item){
                    $items[] = $pr_item;
                }
            }
            
        } else {
            if($this->checkIfHasSubItems($abstract->invite->pr)) {
                $itemParent = PurchaseRequestItem::find($abstract->invite->pr->pr_items->first()->id);
                $itemParent['type'] = 'parent';
                $items->push($itemParent);
                foreach($supplier->pr_sub_items->where('pivot.abstract_id', $abstract->id)->whereIn('pivot.pr_sub_item_id', $prItemIDs) as $pr_sub_item){
                    $items->push($pr_sub_item);
                }
            } else {
                foreach($supplier->pr_items->where('pivot.abstract_id', $abstract->id) as $pr_item){
                    $items->push($pr_item);
                }
            }
        }

        return $items;
    }

    private function return_pr($abstract){
        if($abstract->invite->type === 'RFQ'){
            $pr = $abstract->invite->rfq->pr;
        } else if ($abstract->invite->type === 'RFP') {
            $pr = $abstract->invite->rfp->pr;
        } else if ($abstract->invite->type === 'IB') {
            $pr = $abstract->invite->itb->pr;
        } else if ($abstract->invite->type === 'REI') {
            $pr = $abstract->invite->rei->pr;
        }

        return $pr;
    }

    private function checkIfHasSubItems($pr){
        foreach($pr->pr_items as $item) {
            if(count($item->pr_sub_items)) {
                return true;
            } else {
                return false;
            }
        }
    }
}
