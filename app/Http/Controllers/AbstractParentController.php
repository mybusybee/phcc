<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AbstractParent;
use Auth;

class AbstractParentController extends Controller
{
    public function index(){
    	$pr_exclusive_abstracts = collect();
        $abstracts = AbstractParent::orderBy('form_no', 'desc')->get();
        if (Auth::user()->user_role === 5){
            foreach($abstracts as $abstract){
                if ($abstract->invite->type == 'RFQ'){
                    if ($abstract->invite->rfq->pr->proc_user->id === Auth::user()->id)
                        $pr_exclusive_abstracts->push($abstract);
                }elseif ($abstract->invite->type == 'RFP'){
                    if ($abstract->invite->rfp->pr->proc_user->id === Auth::user()->id)
                        $pr_exclusive_abstracts->push($abstract);
                }elseif ($abstract->invite->type == 'IB'){
                    if ($abstract->invite->itb->pr->proc_user->id === Auth::user()->id)
                        $pr_exclusive_abstracts->push($abstract);
                }elseif ($abstract->invite->type == 'REI'){
                    if ($abstract->invite->rei->pr->proc_user->id === Auth::user()->id)
                        $pr_exclusive_abstracts->push($abstract);
                }
            }
            $abstracts = $pr_exclusive_abstracts;
        }

        foreach($abstracts as $abstract){
            if ($abstract->invite->type == 'RFQ'){
                $abstract->pr = $abstract->invite->rfq->pr;
            }elseif ($abstract->invite->type == 'RFP'){
                $abstract->pr = $abstract->invite->rfp->pr;
            }elseif ($abstract->invite->type == 'IB'){
                $abstract->pr = $abstract->invite->itb->pr;
            }elseif ($abstract->invite->type == 'REI'){
                $abstract->pr = $abstract->invite->rei->pr;
            }
        }

        return view('abstract.index')->with('abstracts', $abstracts);
    }

    public function cancel(AbstractParent $abstract){
        $abstract->status = 'CANCELLED';
        $abstract->save();
        
        return redirect('/abstracts')->with('info', 'Abstract #'.$abstract->form_no.' successfully cancelled');
    }
}
