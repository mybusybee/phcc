<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Supplier;
use App\SupplierDoc;
use Auth;
use Excel;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $suppliers = Supplier::all();

        $log_arr = [
            "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            "user_office" => Auth::user()->office->office,
            "activity"  => ' viewed Suppliers List.',
        ];
        \ActivityLog::add($log_arr);
        
        return view('suppliers.index')->with('suppliers', $suppliers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('suppliers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $supplier = new Supplier;
        $supplier->authorized_personnel = $request->authorized_personnel;
        $supplier->company_name = $request->company_name;
        $supplier->address = $request->address;
        $supplier->telephone = $request->telephone;
        $supplier->email = implode(', ', $request->email);
        $supplier->tin_number = $request->tin_number;
        $supplier->category = $request->category;
        $supplier->mayors_permit = $request->mayors_permit;
        $supplier->philgeps_number = $request->philgeps_number;
        $supplier->save();

        //upload attachment snippet
        if($request->hasFile('supplier_docs')){
            foreach($request->supplier_docs as $file){
                $filepath = $file->store('supplier_docs/'.$supplier->id);
                $docus[] = new SupplierDoc([
                    'filename' => $file->getClientOriginalName(),
                    'filepath' => $filepath
                ]);
            }
            $supplier->docs()->saveMany($docus);
        }

        $log_arr = [
            "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            "user_office" => Auth::user()->office->office,
            "activity"  => ' created new supplier. ('.$supplier->company_name.')',
        ];
        \ActivityLog::add($log_arr);

        return redirect()->route('supplier.index')->with('success', 'New supplier added successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $supplier = Supplier::findOrFail($id);
        
        $log_arr = [
            "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            "user_office" => Auth::user()->office->office,
            "activity"  => ' viewed supplier ' . $supplier->company_name,
        ];
        \ActivityLog::add($log_arr);

        return view('suppliers.show')->with('supplier', $supplier);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $supplier = Supplier::find($id);

        return view('suppliers.edit')->with('supplier', $supplier);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $supplier = Supplier::find($id);
        $supplier->authorized_personnel = $request->authorized_personnel;
        $supplier->company_name = $request->company_name;
        $supplier->address = $request->address;
        $supplier->telephone = $request->telephone;
        $supplier->email = implode(', ', $request->email);
        $supplier->tin_number = $request->tin_number;
        $supplier->category = $request->category;
        $supplier->mayors_permit = $request->mayors_permit;
        $supplier->philgeps_number = $request->philgeps_number;
        $supplier->save();

        //upload attachment snippet
        if($request->hasFile('supplier_docs')){
            foreach($request->supplier_docs as $file){
                $filepath = $file->store('supplier_docs/'.$supplier->id);
                $docus[] = new SupplierDoc([
                    'filename' => $file->getClientOriginalName(),
                    'filepath' => $filepath
                ]);
            }
            $supplier->docs()->saveMany($docus);
        }

        $log_arr = [
            "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            "user_office" => Auth::user()->office->office,
            "activity"  => ' edited supplier ' . $supplier->company_name,
        ];
        \ActivityLog::add($log_arr);

        return redirect()->route('supplier.index')->with('Supplier succesfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Supplier $supplier)
    {
        $supplier->delete();

        $log_arr = [
            "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            "user_office" => Auth::user()->office->office,
            "activity"  => ' deleted supplier ' . $supplier->company_name,
        ];
        \ActivityLog::add($log_arr);

        return redirect('/supplier')->with('success', 'Supplier successfully deleted!');
    }

    public function import(Request $request) {
        $request->validate([
            'suppliers' => 'required'
        ]);

        if($request->hasFile('suppliers')) {
            $extension = \File::extension($request->suppliers->getClientOriginalName());
            $valid_file_types = ['xlsx', 'xls', 'csv'];

            if(in_array($extension, $valid_file_types)) {
                $path = $request->suppliers->getRealPath();
                Excel::selectSheets('suppliers_list')->load($path, function($reader) use($path) {
                    foreach($reader->toArray() as $row) {
                        $supplier                       = new Supplier;
                        $supplier->authorized_personnel = $row['authorized_personnel'] ? $row['authorized_personnel'] : 'N/A';
                        $supplier->company_name         = $row['company_name'] ? $row['company_name'] : 'N/A';
                        $supplier->category             = $row['category'] ? $row['category'] : 'N/A';
                        $supplier->address              = $row['address'] ? $row['address'] : 'N/A';
                        $supplier->telephone            = $row['telephone'] ? $row['telephone'] : 'N/A';
                        $supplier->email                = $row['email'] ? $row['email'] : 'N/A';
                        $supplier->tin_number           = $row['tin'] ? $row['tin'] : 'N/A';
                        $supplier->mayors_permit        = $row['mayors_permit'] ? $row['mayors_permit'] : 'N/A';
                        $supplier->philgeps_number      = $row['philgeps_number'] ? $row['philgeps_number'] : 'N/A';
                        $supplier->save();
                    }
                });
                return redirect()->back()->with('success', 'Successfully imported data.');
            } else {
                return redirect()->back()->with('error', 'Invalid file type.');
            }
        } else {
            return redirect()->back()->with('error', 'Something went wrong. Please try again.');
        }
    }
}
