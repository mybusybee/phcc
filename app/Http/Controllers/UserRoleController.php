<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\UserRole;
use App\User;

class UserRoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = UserRole::where('id', '>', 2)->get();

        return view('user_roles.edit')->with('roles', $roles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        $roles = UserRole::where('id', '>', 2)->get();

        //user_role_ids of all users
        $users = User::where('id', '>', 2)->get();
        $users_table_user_role = [];
        foreach($users as $user){
            $users_table_user_role[] = $user->user_role;
        }

        //all ids from request
        $new_ids = [];
        foreach($request->role_id as $id){
            $new_ids[] = $id;
        }

        //remove all same ids from request and in from db array(users_table_user_role)
        foreach($new_ids as $id_from_request){
            if(in_array($id_from_request, $users_table_user_role)){
                $key = array_search($id_from_request, $users_table_user_role);
                unset($users_table_user_role[$key]);
            }
        }

        $ids_for_deletion = [];
        foreach($users_table_user_role as $id){
            $ids_for_deletion[] = $id;
        }

        //users with that recently deleted role, will be assigned a new role (unassigned = ID#2)
        //$unassigned_users MUST be an array, and contains the ids in userroles table about to be deleted
        foreach($ids_for_deletion as $unassigned_user){
            $users = User::where('user_role', $unassigned_user)->get();
            foreach($users as $user){
                $user->user_role = 2;
                $user->save();
            }
        }

        foreach(array_combine($request->role_id, $request->role) as $id => $role){
            if(in_array($id, $ids_for_deletion)){
                $role = UserRole::find($id);
                $role->delete();
            } else {
                $role = UserRole::updateOrCreate(
                    ['id' => $id],
                    ['user_role' => $role]
                );
            }
        }

        \DB::statement('SET FOREIGN_KEY_CHECKS = 1');
        return back()->with('success', 'Successfully saved!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
