<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AbstractParent;
use App\OrdersParent;
use App\Supplier;
use App\JobOrder as JO;
use Auth;
use App\User;
use SystemNotification;
use App\Notifications\EmailNotification;
use App\PurchaseRequestItem;

class JobOrderController extends Controller
{
    public function create(AbstractParent $abstract){
        
        if($abstract->type === 'AOP'){
            $abstract = $abstract->aop;
        } else if ($abstract->type === 'AOB'){
            $abstract = $abstract->aob;
        } else if ($abstract->type === 'AOQ') {
            $abstract = $abstract->aoq;
        }

        if($abstract->abstract_parent->invite->type === 'RFQ'){
            $abstract->pr = $abstract->abstract_parent->invite->rfq->pr;
        } else if ($abstract->abstract_parent->invite->type === 'RFP') {
            $abstract->pr = $abstract->abstract_parent->invite->rfp->pr;
        } else if ($abstract->abstract_parent->invite->type === 'IB') {
            $abstract->pr = $abstract->abstract_parent->invite->itb->pr;
        } else if ($abstract->abstract_parent->invite->type === 'REI') {
            $abstract->pr = $abstract->abstract_parent->invite->rei->pr;
        }

        $orders = OrdersParent::all();

        return view('orders-parent.job-order.create')->with('abstract', $abstract)
                                                          ->with('ordersCount', count($orders));
    }

    public function store(Request $request){
        $request->validate([
            'form_no' => 'required|unique:orders_parents',
        ]);
        $orderParent = new OrdersParent;
        $orderParent->abstract_id = $request->abstractID;
        $orderParent->type = 'JO';
        $orderParent->form_no = $request->form_no;
        $orderParent->status = 'FOR_EXPORT';
        $orderParent->save();

        $jo = new JO;
        $jo->supplier_id = $request->supplier_id;
        $jo->jo_date = $request->jo_date;
        $jo->amount_in_words = $request->amount_in_words;
        $jo->place_of_delivery = $request->place_of_delivery;
        if($request->delivery_term === 'pickup'){
            $jo->delivery_term = 'pickup';
        } else {
            $jo->delivery_term = $request->calendar_days;
        }
        $jo->date_of_delivery = $request->date_of_delivery;
        if($request->payment_term === 'cod') {
            $jo->payment_term = 'cod';
        } else {
            $jo->payment_term = $request->payment_after_delivery;
        }
        $jo->ordersParent()->associate($orderParent)->save();

        if($jo->ordersParent->abstract->invite->type === 'RFQ'){
            $jo->pr = $jo->ordersParent->abstract->invite->rfq->pr;
        } else if ($jo->ordersParent->abstract->invite->type === 'RFP') {
            $jo->pr = $jo->ordersParent->abstract->invite->rfp->pr;
        } else if ($jo->ordersParent->abstract->invite->type === 'IB') {
            $jo->pr = $jo->ordersParent->abstract->invite->itb->pr;
        } else if ($jo->ordersParent->abstract->invite->type === 'REI') {
            $jo->pr = $jo->ordersParent->abstract->invite->rei->pr;
        }

        $notif_recipient = User::find($jo->pr->requested_by);
        $obj = new \stdClass();
        $obj->message = Auth::user()->fullName . ' generated JO#'.$jo->ordersParent->form_no.' for PR#'.$jo->pr->pr_no;
        $obj->link = url('/pr/'.$jo->pr->id);
        SystemNotification::add($notif_recipient, $obj, 'order', $jo->ordersParent);
        $notif_recipient->notify(new EmailNotification($obj));

        $adminUsers = User::where('user_role', 3)->get();
        foreach($adminUsers as $adminUser){
            $obj = new \stdClass();
            $obj->message = Auth::user()->fullName . ' generated JO#'.$jo->ordersParent->form_no.'.';
            $obj->link = url('/jo/'.$jo->id);
            SystemNotification::add($adminUser, $obj, 'order', $jo->ordersParent);
            $adminUser->notify(new EmailNotification($obj));
        }

        return redirect('/jo/'.$jo->id)->with('success', 'Job Order successfully saved!');
    }

    public function show(JO $jo){
        if(!$jo) {
            abort(404);
        }
        if(Auth::user()->user_role === 3) {
            SystemNotification::checkURL();
        }
        if($jo->ordersParent->abstract->invite->type === 'RFQ'){
            $jo->pr = $jo->ordersParent->abstract->invite->rfq->pr;
        } else if ($jo->ordersParent->abstract->invite->type === 'RFP') {
            $jo->pr = $jo->ordersParent->abstract->invite->rfp->pr;
        } else if ($jo->ordersParent->abstract->invite->type === 'IB') {
            $jo->pr = $jo->ordersParent->abstract->invite->itb->pr;
        } else if ($jo->ordersParent->abstract->invite->type === 'REI') {
            $jo->pr = $jo->ordersParent->abstract->invite->rei->pr;
        }

        if($jo->ordersParent->abstract->type === 'AOB'){
            $jo->procurement_mode = $jo->ordersParent->abstract->aob->procurement_mode->mode;
        } else if($jo->ordersParent->abstract->type === 'AOQ'){
            $jo->procurement_mode = $jo->ordersParent->abstract->aoq->procurement_mode->mode;
        } else if($jo->ordersParent->abstract->type === 'AOP') {
            $jo->procurement_mode = $jo->ordersParent->abstract->aop->procurement_mode->mode;
        }

        $jo->items = $this->getRecommendedItems($jo->ordersParent->abstract->id, $jo->supplier->id);
        
        return view('orders-parent.job-order.show')->with('jo', $jo);
    }

    private function getRecommendedItems($abstractID, $supplierID){
        $abstract = AbstractParent::find($abstractID);
        $supplier = Supplier::find($supplierID);
        foreach($abstract->recommendation->recommendation_items->where('supplier_id', $supplier->id) as $recoItem){
            $prItemIDs = explode(',', $recoItem->pr_item_ids);
        }

        $abstract->invite->pr = $this->return_pr($abstract);

        $items = collect();

        if($abstract->invite->pr->pr_item_total->is_lot_purchase === 1) {
            if($this->checkIfHasSubItems($abstract->invite->pr)) {
                //attach parent item to first
                foreach($prItemIDs as $itemID) {
                    $itemParent = PurchaseRequestItem::find($itemID);
                    $itemParent['type'] = 'parent';
                    $items->push($itemParent);
                    foreach($supplier->pr_sub_items->where('pivot.abstract_id', $abstract->id) as $pr_sub_item){
                        $pr_sub_item['type'] = 'sub';
                        $items->push($pr_sub_item);
                    }
                }
            } else {
                foreach($supplier->pr_items->where('pivot.abstract_id', $abstract->id) as $pr_item){
                    $items[] = $pr_item;
                }
            }
            
        } else {
            if($this->checkIfHasSubItems($abstract->invite->pr)) {
                $itemParent = PurchaseRequestItem::find($abstract->invite->pr->pr_items->first()->id);
                $itemParent['type'] = 'parent';
                $items->push($itemParent);
                foreach($supplier->pr_sub_items->where('pivot.abstract_id', $abstract->id)->whereIn('pivot.pr_sub_item_id', $prItemIDs) as $pr_sub_item){
                    $items->push($pr_sub_item);
                }
            } else {
                foreach($supplier->pr_items->where('pivot.abstract_id', $abstract->id) as $pr_item){
                    $items->push($pr_item);
                }
            }
        }

        return $items;
    }

    private function return_pr($abstract){
        if($abstract->invite->type === 'RFQ'){
            $pr = $abstract->invite->rfq->pr;
        } else if ($abstract->invite->type === 'RFP') {
            $pr = $abstract->invite->rfp->pr;
        } else if ($abstract->invite->type === 'IB') {
            $pr = $abstract->invite->itb->pr;
        } else if ($abstract->invite->type === 'REI') {
            $pr = $abstract->invite->rei->pr;
        }

        return $pr;
    }

    private function checkIfHasSubItems($pr){
        foreach($pr->pr_items as $item) {
            if(count($item->pr_sub_items)) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function edit(JO $jo){
        if(!$jo) {
            abort(404);
        }
        if($jo->ordersParent->abstract->type === 'AOP'){
            $jo->abstract = $jo->ordersParent->abstract->aop;
        } else if ($jo->ordersParent->abstract->type === 'AOB'){
            $jo->abstract = $jo->ordersParent->abstract->aob;
        } else if ($jo->ordersParent->abstract->type === 'AOQ') {
            $jo->abstract = $jo->ordersParent->abstract->aoq;
        }

        if($jo->ordersParent->abstract->invite->type === 'RFQ'){
            $jo->abstract->pr = $jo->ordersParent->abstract->invite->rfq->pr;
        } else if ($jo->ordersParent->abstract->invite->type === 'RFP') {
            $jo->abstract->pr = $jo->ordersParent->abstract->invite->rfp->pr;
        } else if ($jo->ordersParent->abstract->invite->type === 'IB') {
            $jo->abstract->pr = $jo->ordersParent->abstract->invite->itb->pr;
        } else if ($jo->ordersParent->abstract->invite->type === 'REI') {
            $jo->abstract->pr = $jo->ordersParent->abstract->invite->rei->pr;
        }
        
        return view('orders-parent.job-order.edit')->with('jo', $jo);
    }

    public function update(Request $request, JO $jo){
        $jo->supplier_id = $request->supplier_id;
        $jo->jo_date = $request->jo_date;
        $jo->amount_in_words = $request->amount_in_words;
        $jo->place_of_delivery = $request->place_of_delivery;
        if($request->delivery_term === 'pickup'){
            $jo->delivery_term = 'pickup';
        } else {
            $jo->delivery_term = $request->calendar_days;
        }
        $jo->date_of_delivery = $request->date_of_delivery;
        if($request->payment_term === 'cod') {
            $jo->payment_term = 'cod';
        } else {
            $jo->payment_term = $request->payment_after_delivery;
        }
        $jo->save();

        return redirect('/jo/'.$jo->id)->with('success', 'Job Order successfully updated!');
    }
}
