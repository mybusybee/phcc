<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AbstractParent;
use App\OrdersParent;
use App\PurchaseOrder as PO;
use App\Supplier;
use Auth;
use App\User;
use SystemNotification;
use App\Notifications\EmailNotification;
use App\PurchaseRequestItem;

class PurchaseOrderController extends Controller
{
    public function create(AbstractParent $abstract){
        
        if($abstract->type === 'AOP'){
            $abstract = $abstract->aop;
        } else if ($abstract->type === 'AOB'){
            $abstract = $abstract->aob;
        } else if ($abstract->type === 'AOQ') {
            $abstract = $abstract->aoq;
        }

        if($abstract->abstract_parent->invite->type === 'RFQ'){
            $abstract->pr = $abstract->abstract_parent->invite->rfq->pr;
        } else if ($abstract->abstract_parent->invite->type === 'RFP') {
            $abstract->pr = $abstract->abstract_parent->invite->rfp->pr;
        } else if ($abstract->abstract_parent->invite->type === 'IB') {
            $abstract->pr = $abstract->abstract_parent->invite->itb->pr;
        } else if ($abstract->abstract_parent->invite->type === 'REI') {
            $abstract->pr = $abstract->abstract_parent->invite->rei->pr;
        }

        $orders = OrdersParent::all();

        return view('orders-parent.purchase-order.create')->with('abstract', $abstract)
                                                          ->with('ordersCount', count($orders));
    }

    public function store(Request $request){
        $request->validate([
            'form_no' => 'required|unique:orders_parents'
        ]);
        $orderParent = new OrdersParent;
        $orderParent->abstract_id = $request->abstractID;
        $orderParent->type = 'PO';
        $orderParent->form_no = $request->form_no;
        $orderParent->status = 'FOR_EXPORT';
        $orderParent->save();

        $po = new PO;
        $po->supplier_id = $request->supplier_id;
        $po->po_date = $request->po_date;
        $po->amount_in_words = $request->amount_in_words;
        $po->place_of_delivery = $request->place_of_delivery;
        if($request->delivery_term === 'pickup'){
            $po->delivery_term = 'pickup';
        } else {
            $po->delivery_term = $request->calendar_days;
        }
        $po->date_of_delivery = $request->date_of_delivery;
        if($request->payment_term === 'cod') {
            $po->payment_term = 'cod';
        } else {
            $po->payment_term = $request->payment_after_delivery;
        }
        $po->ordersParent()->associate($orderParent)->save();

        if($po->ordersParent->abstract->invite->type === 'RFQ'){
            $po->pr = $po->ordersParent->abstract->invite->rfq->pr;
        } else if ($po->ordersParent->abstract->invite->type === 'RFP') {
            $po->pr = $po->ordersParent->abstract->invite->rfp->pr;
        } else if ($po->ordersParent->abstract->invite->type === 'IB') {
            $po->pr = $po->ordersParent->abstract->invite->itb->pr;
        } else if ($po->ordersParent->abstract->invite->type === 'REI') {
            $po->pr = $po->ordersParent->abstract->invite->rei->pr;
        }

        $notif_recipient = User::find($po->pr->requested_by);
        $obj = new \stdClass();
        $obj->message = Auth::user()->fullName . ' generated PO#'.$po->ordersParent->form_no.' for PR#'.$po->pr->pr_no;
        $obj->link = url('/pr/'.$po->pr->id);
        SystemNotification::add($notif_recipient, $obj, 'order', $po->ordersParent);
        $notif_recipient->notify(new EmailNotification($obj));

        $adminUsers = User::where('user_role', 3)->get();
        foreach($adminUsers as $adminUser){
            $obj = new \stdClass();
            $obj->message = Auth::user()->fullName . ' generated PO#'.$po->ordersParent->form_no.'.';
            $obj->link = url('/po/'.$po->id);
            SystemNotification::add($adminUser, $obj, 'order', $po->ordersParent);
            $adminUser->notify(new EmailNotification($obj));
        }

        return redirect('/po/'.$po->id)->with('success', 'Purchase Order successfully saved!');
    }

    public function show(PO $po){
        if(!$po) {
            abort(404);
        }
        if(Auth::user()->user_role === 3) {
            SystemNotification::checkURL();
        }
        
        if($po->ordersParent->abstract->invite->type === 'RFQ'){
            $po->pr = $po->ordersParent->abstract->invite->rfq->pr;
        } else if ($po->ordersParent->abstract->invite->type === 'RFP') {
            $po->pr = $po->ordersParent->abstract->invite->rfp->pr;
        } else if ($po->ordersParent->abstract->invite->type === 'IB') {
            $po->pr = $po->ordersParent->abstract->invite->itb->pr;
        } else if ($po->ordersParent->abstract->invite->type === 'REI') {
            $po->pr = $po->ordersParent->abstract->invite->rei->pr;
        }

        if($po->ordersParent->abstract->type === 'AOB'){
            $po->procurement_mode = $po->ordersParent->abstract->aob->procurement_mode->mode;
        } else if($po->ordersParent->abstract->type === 'AOQ'){
            $po->procurement_mode = $po->ordersParent->abstract->aoq->procurement_mode->mode;
        } else if($po->ordersParent->abstract->type === 'AOP') {
            $po->procurement_mode = $po->ordersParent->abstract->aop->procurement_mode->mode;
        }

        $po->items = $this->getRecommendedItems($po->ordersParent->abstract->id, $po->supplier->id);

        return view('orders-parent.purchase-order.show')->with('po', $po);
    }

    private function getRecommendedItems($abstractID, $supplierID){
        $abstract = AbstractParent::find($abstractID);
        $supplier = Supplier::find($supplierID);
        foreach($abstract->recommendation->recommendation_items->where('supplier_id', $supplier->id) as $recoItem){
            $prItemIDs = explode(',', $recoItem->pr_item_ids);
        }

        $abstract->invite->pr = $this->return_pr($abstract);

        $items = collect();

        if($abstract->invite->pr->pr_item_total->is_lot_purchase === 1) {
            if($this->checkIfHasSubItems($abstract->invite->pr)) {
                //attach parent item to first
                foreach($prItemIDs as $itemID) {
                    $itemParent = PurchaseRequestItem::find($itemID);
                    $itemParent['type'] = 'parent';
                    $items->push($itemParent);
                    foreach($supplier->pr_sub_items->where('pivot.abstract_id', $abstract->id) as $pr_sub_item){
                        $pr_sub_item['type'] = 'sub';
                        $items->push($pr_sub_item);
                    }
                }
            } else {
                foreach($supplier->pr_items->where('pivot.abstract_id', $abstract->id) as $pr_item){
                    $items[] = $pr_item;
                }
            }
            
        } else {
            if($this->checkIfHasSubItems($abstract->invite->pr)) {
                $itemParent = PurchaseRequestItem::find($abstract->invite->pr->pr_items->first()->id);
                $itemParent['type'] = 'parent';
                $items->push($itemParent);
                foreach($supplier->pr_sub_items->where('pivot.abstract_id', $abstract->id)->whereIn('pivot.pr_sub_item_id', $prItemIDs) as $pr_sub_item){
                    $items->push($pr_sub_item);
                }
            } else {
                foreach($supplier->pr_items->where('pivot.abstract_id', $abstract->id) as $pr_item){
                    $items->push($pr_item);
                }
            }
        }

        return $items;
    }

    private function return_pr($abstract){
        if($abstract->invite->type === 'RFQ'){
            $pr = $abstract->invite->rfq->pr;
        } else if ($abstract->invite->type === 'RFP') {
            $pr = $abstract->invite->rfp->pr;
        } else if ($abstract->invite->type === 'IB') {
            $pr = $abstract->invite->itb->pr;
        } else if ($abstract->invite->type === 'REI') {
            $pr = $abstract->invite->rei->pr;
        }

        return $pr;
    }

    private function checkIfHasSubItems($pr){
        foreach($pr->pr_items as $item) {
            if(count($item->pr_sub_items)) {
                return true;
            } else {
                return false;
            }
        }
    }

    public function edit(PO $po) {
        if(!$po) {
            abort(404);
        }
        if($po->ordersParent->abstract->type === 'AOP'){
            $po->abstract = $po->ordersParent->abstract->aop;
        } else if ($po->ordersParent->abstract->type === 'AOB'){
            $po->abstract = $po->ordersParent->abstract->aob;
        } else if ($po->ordersParent->abstract->type === 'AOQ') {
            $po->abstract = $po->ordersParent->abstract->aoq;
        }

        if($po->ordersParent->abstract->invite->type === 'RFQ'){
            $po->abstract->pr = $po->ordersParent->abstract->invite->rfq->pr;
        } else if ($po->ordersParent->abstract->invite->type === 'RFP') {
            $po->abstract->pr = $po->ordersParent->abstract->invite->rfp->pr;
        } else if ($po->ordersParent->abstract->invite->type === 'IB') {
            $po->abstract->pr = $po->ordersParent->abstract->invite->itb->pr;
        } else if ($po->ordersParent->abstract->invite->type === 'REI') {
            $po->abstract->pr = $po->ordersParent->abstract->invite->rei->pr;
        }
        
        return view('orders-parent.purchase-order.edit')->with('po', $po);
    }

    public function update(Request $request, PO $po){
        $po->supplier_id = $request->supplier_id;
        $po->po_date = $request->po_date;
        $po->amount_in_words = $request->amount_in_words;
        $po->place_of_delivery = $request->place_of_delivery;
        if($request->delivery_term === 'pickup'){
            $po->delivery_term = 'pickup';
        } else {
            $po->delivery_term = $request->calendar_days;
        }
        $po->date_of_delivery = $request->date_of_delivery;
        if($request->payment_term === 'cod') {
            $po->payment_term = 'cod';
        } else {
            $po->payment_term = $request->payment_after_delivery;
        }
        $po->save();

        return redirect('/po/'.$po->id)->with('success', 'Purchase Order successfully updated!');
    }
}
