<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProcurementMode;
use Auth;


class ProcurementModeController extends Controller
{
    public function edit(){
        $modes = ProcurementMode::orderBy('id')->get();
        return view('procurement_modes.edit')->with('modes', $modes);
    }
    public function update(Request $request){

        foreach($request->id as $key => $id){
            $mode = ProcurementMode::find($id);

            $log_arr = [
                "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
                "user_office" => Auth::user()->office->office,
                "activity"  => ' updated Procurement Mode - ' . $mode->mode . '\'s minimum value from '. $mode->min_value . ' to ' . $request->max_value[$key] . ', and maximum value from ' . $mode->max_value . ' to ' . $request->max_value[$key],
            ];
            \ActivityLog::add($log_arr);

            $mode->mode = $request->mode[$key];
            $mode->min_value = $request->min_value[$key];
            $mode->max_value = $request->max_value[$key];
            $mode->save();
        }
        
        return back()->with('success', 'Procurement mode/s updated successfully!');
    }

    public function create() {
        return view('procurement_modes.create');
    }

    public function store(Request $request) {
        $mode = new ProcurementMode;
        $mode->mode = $request->mode;
        $mode->min_value = $request->min_value;
        $mode->max_value = $request->max_value;
        $mode->turnaround_time = 'TBD';
        $mode->save();

        return redirect('/procurement-modes/edit')->with('success', 'Successfully saved!');
    }
}
