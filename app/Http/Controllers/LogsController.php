<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Log;
use App\User;
use Auth;

class LogsController extends Controller
{
    public function index(){
		$logs = Log::orderBy('created_at','desc')
        			->get();


        if (Auth::user()->user_role == 8){
            $procurement_users = User::where('user_role', 5)->get();

            $temp_logs = collect();

            $bac_sec_logs = Log::orderBy('created_at','desc')
                                ->where('user_name', Auth::user()->fullname)
                                ->get();

            foreach($procurement_users as $proc_user){
                $proc_user_logs = Log::orderBy('created_at','desc')
                                ->where('user_name', $proc_user->fullname)
                                ->get();

                foreach($proc_user_logs as $proc_log){
                    $temp_logs->push($proc_log);
                }
            }

            $final_logs = $bac_sec_logs->merge($temp_logs);

            $logs = $final_logs->sortByDesc(function($col){
                return $col;
            })->values();

        }else if (Auth::user()->user_role == 4){
            $office_id = Auth::user()->office_id;
            $office_end_users = User::where('user_role', 7)
                                    ->where('office_id', $office_id)
                                    ->get();

            $temp_logs = collect();

            $director_logs = Log::orderBy('created_at','desc')
                                ->where('user_name', Auth::user()->fullname)
                                ->get();

            foreach($office_end_users as $end_user){
                $end_user_logs = Log::orderBy('created_at','desc')
                                ->where('user_name', $end_user->fullname)
                                ->get();

                foreach($end_user_logs as $user_log){
                    $temp_logs->push($user_log);
                }
            }

            $final_logs = $director_logs->merge($temp_logs);

            $logs = $final_logs->sortByDesc(function($col){
                return $col;
            })->values();
        }else if (Auth::user()->user_role != 1){
    		$user_fullname = Auth::user()->fullname;

	        $logs = Log::orderBy('created_at','desc')
	        			->where('user_name', $user_fullname)
	        			->get();
    	}
		
        return view('logs.index')->with('logs', $logs);
    }
}
