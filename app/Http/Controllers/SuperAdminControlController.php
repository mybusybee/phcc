<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SuperAdminControl;

class SuperAdminControlController extends Controller
{
    public function index(){
    	$settings = SuperAdminControl::all();

    	return view('users.settings')->with('settings', $settings);
    }

    public function update(Request $request){

    	$ppmp_control = SuperAdminControl::where('control', 'PPMP')->first();
    	$ppmp_control->is_active = $request->ppmp_control == 'on' ? 1 : 0;
    	$ppmp_control->save();

    	return redirect()->back()->with('success', 'Settings Updated!');
    }
}
