<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UploadedPmrController extends Controller
{
    public function index() {
        return view('pmr.uploaded.index');
    }
}
