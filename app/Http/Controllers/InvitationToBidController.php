<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Invitation as Invite;
use App\PurchaseRequest as PR;
use App\InvitationToBid as ITB;
use App\Supplier;
use Auth;
use SystemNotification;
use App\User;
use App\Notifications\EmailNotification;
use App\InvitationAttachment as InviteFile;

class InvitationToBidController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $itbs = ITB::all();

        return view('itb.index')->with('itbs', $itbs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $pr = PR::find($id);
        $itb = Invite::all();

        $yearsArray = [];
        for ($year = date('Y'); $year <= date('Y') + 10; $year++ ) {
            $yearsArray[$year] = $year;
        }

        return view('itb.create')->with('pr', $pr)->with('itb_count', count($itb))->with('yearsArray', $yearsArray);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name_of_project' => 'required',
            'year' => 'required',
            'abc' => 'required',
            'form_no' => 'unique:invitations',
            'description_of_goods' => 'required',
            'delivery_date' => 'required',
            'relevant_period' => 'required',
            'date_of_availability' => 'required',
            'bidding_documents_fee' => 'required',
            'prebid_conf_date' => 'required',
            'bids_deadline' => 'required',
            'bid_opening_datetime' => 'required',
            'bid_opening_address' => 'required',
            'other_info' => 'required',
            'chairperson_signatory' => 'required',
        ]);

        $new_invite = new Invite;
        $new_invite->form_no = $request->form_no;
        $new_invite->type = 'IB';
        $new_invite->status = 'FOR_REVIEW/APPROVAL';
        $new_invite->save();

        if ($new_invite){
            $pr = PR::find($request->pr_id);
            $itb = new ITB;
            $itb->name_of_project = $request->name_of_project;
            $itb->year = $request->year;
            $itb->abc = $request->abc;
            // $itb->itb_no = $request->itb_no;
            $itb->description_of_goods = $request->description_of_goods;
            $itb->delivery_date = $request->delivery_date;
            $itb->relevant_period = $request->relevant_period;
            $itb->date_of_availability = $request->date_of_availability;
            $itb->bidding_documents_fee = $request->bidding_documents_fee;
            $itb->prebid_conf_date = $request->prebid_conf_date;
            $itb->prebid_conf_address = $request->prebid_conf_address;
            $itb->bids_deadline = $request->bids_deadline;
            $itb->bid_opening_datetime = $request->bid_opening_datetime;
            $itb->bid_opening_address = $request->bid_opening_address;
            $itb->other_info = $request->other_info;
            $itb->chairperson_signatory = $request->chairperson_signatory;
            $itb->pr()->associate($pr);
            $itb->invite()->associate($new_invite)->save();

            //upload attachment snippet
            if($request->hasFile('invite_requirements')){
                foreach($request->invite_requirements as $file){
                    $filepath = $file->store('invite_file_attachments/'.$itb->invite->form_no);
                    $attachments[] = new InviteFile([
                        'filename' => $file->getClientOriginalName(),
                        'filepath' => $filepath
                    ]);
                }
                $itb->invite->files()->saveMany($attachments);
            }

            $notif_recipient = User::find($pr->requested_by);
            $obj = new \stdClass();
            $obj->message = Auth::user()->fullName . ' generated an Invitation to Bid for PR#'.$pr->pr_no;
            $obj->link = url('/pr/'.$pr->id);
            SystemNotification::add($notif_recipient, $obj, 'inv', $new_invite);
            $notif_recipient->notify(new EmailNotification($obj));

            $adminUsers = User::where('user_role', 3)->get();
            foreach($adminUsers as $adminUser) {
                $obj = new \stdClass();
                $obj->message = 'An IB is waiting for your approval.';
                $obj->link = url('/itb/'.$itb->id);
                SystemNotification::add($adminUser, $obj, 'inv', $new_invite);
                $adminUser->notify(new EmailNotification($obj));
            }


            return redirect('/itb/'.$itb->id)->with('success', 'Invitation to Bid successfully submitted!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InvitationToBid  $invitationToBid
     * @return \Illuminate\Http\Response
     */
    public function show(ITB $invitationToBid)
    {
        if(!$invitationToBid) {
            abort(404);
        }
        if(
            (Auth::user()->user_role === 3 && $invitationToBid->invite->status === 'FOR_REVIEW/APPROVAL') ||
            (Auth::user()->user_role === 5 && $invitationToBid->invite->status === 'FOR_EXPORT')
        ) {
            SystemNotification::checkURL();
        }


        $selected_suppliers = [];
        if ($invitationToBid->invite->suppliers()->exists()) {
            foreach($invitationToBid->invite->suppliers as $supplier){
                $selected_suppliers[] = $supplier->id;
            }
        }

        if($invitationToBid->invite->status === 'FOR_EXPORT' && Auth::user()->user_role === 5){
            $suppliers = Supplier::all();
            return view('itb.show')->with('itb', $invitationToBid)->with('suppliers', $suppliers)->with('selected_suppliers', $selected_suppliers);
        } else {
            return view('itb.show')->with('itb', $invitationToBid);
        }
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InvitationToBid  $invitationToBid
     * @return \Illuminate\Http\Response
     */
    public function edit(ITB $invitationToBid)
    {
        if(!$invitationToBid) {
            abort(404);
        }
        $yearsArray = [];
        for ($year = date('Y'); $year <= date('Y') + 10; $year++ ) {
            $yearsArray[$year] = $year;
        }
        return view('itb.edit')->with('itb', $invitationToBid)->with('yearsArray', $yearsArray);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InvitationToBid  $invitationToBid
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ITB $invitationToBid)
    {
        $request->validate([
            'name_of_project' => 'required',
            'year' => 'required',
            'abc' => 'required',
            'itb_no' => 'required',
            'description_of_goods' => 'required',
            'delivery_date' => 'required',
            'relevant_period' => 'required',
            'date_of_availability' => 'required',
            'bidding_documents_fee' => 'required',
            'prebid_conf_date' => 'required',
            'bids_deadline' => 'required',
            'bid_opening_datetime' => 'required',
            'bid_opening_address' => 'required',
            'other_info' => 'required',
            'chairperson_signatory' => 'required'
        ]);
        
        $invite = Invite::find($invitationToBid->invite->id);
        $invite->status = 'FOR_REVIEW/APPROVAL';
        $invite->rejects_remarks .= " (UPDATED)";
        $invite->save();

        $invitationToBid->name_of_project = $request->name_of_project;
        $invitationToBid->year = $request->year;
        $invitationToBid->abc = $request->abc;
        $invitationToBid->description_of_goods = $request->description_of_goods;
        $invitationToBid->delivery_date = $request->delivery_date;
        $invitationToBid->relevant_period = $request->relevant_period;
        $invitationToBid->date_of_availability = $request->date_of_availability;
        $invitationToBid->bidding_documents_fee = $request->bidding_documents_fee;
        $invitationToBid->prebid_conf_date = $request->prebid_conf_date;
        $invitationToBid->prebid_conf_address = $request->prebid_conf_address;
        $invitationToBid->bids_deadline = $request->bids_deadline;
        $invitationToBid->bid_opening_datetime = $request->bid_opening_datetime;
        $invitationToBid->bid_opening_address = $request->bid_opening_address;
        $invitationToBid->other_info = $request->other_info;
        $invitationToBid->chairperson_signatory = $request->chairperson_signatory;
        $invitationToBid->save();

        return redirect('/itb/'.$invitationToBid->id)->with('success', 'Invitation to Bid successfully updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InvitationToBid  $invitationToBid
     * @return \Illuminate\Http\Response
     */
    public function destroy(ITB $invitationToBid)
    {

    }

    public function signed(ITB $invitationToBid){
        $invitationToBid->signed = 1;
        $invitationToBid->save();

        $log_arr = [
            "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            "user_office" => Auth::user()->office->office,
            "activity"  => ' received the signed copy of IB#' . $invitationToBid->invite->form_no,
        ];
        \ActivityLog::add($log_arr);

        return redirect()->back()->with('success', 'Successfully received signed copy.');
    }
}
