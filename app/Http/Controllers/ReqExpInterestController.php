<?php

namespace App\Http\Controllers;

use App\ReqExpInterest;
use App\Invitation as Invite;
use Illuminate\Http\Request;
use Auth;
use App\Supplier;
use App\PurchaseRequest as PR;
use SystemNotification;
use App\User;
use App\Notifications\EmailNotification;
use App\InvitationAttachment as InviteFile;

class ReqExpInterestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reis = ReqExpInterest::all();
        return view('req_exp_interests.index')->with('reis', $reis);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($pr_id)
    {
        $invites = Invite::all();
        $pr = PR::find($pr_id);
        $yearsArray = [];
        for ($year = date('Y'); $year <= date('Y') + 10; $year++ ) {
            $yearsArray[$year] = $year;
        }

        return view('req_exp_interests.create')->with('pr', $pr)->with('rei_count', count($invites))->with('yearsArray', $yearsArray);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
                'project_name'          => 'required',
                'year'                  => 'required',
                'abc'                   => 'required',
                'form_no'               => 'unique:invitations',
                'brief_desc'            => 'required',
                'opening_date'          => 'required',
                'availability_date'     => 'required',
                'amount_in_peso'        => 'required',
                'shortlist_allowed'     => 'required',
                'procedure'             => 'required',
                'completed_within'      => 'required',
                'chairperson_name'      => 'required',
        ]);

        $new_invite = new Invite;
        $new_invite->form_no = $request->form_no;
        $new_invite->type = 'REI';
        $new_invite->status = 'FOR_REVIEW/APPROVAL';
        $new_invite->save();

        if ($new_invite){
            $new_rei = new ReqExpInterest;
            $new_rei->pr_id = $request->pr_id;
            $new_rei->invite_id = $new_invite->id;
            $new_rei->project_name = $request->project_name;
            $new_rei->year = $request->year;
            $new_rei->abc = $request->abc;
            $new_rei->brief_desc = $request->brief_desc;
            $new_rei->opening_date = $request->opening_date;
            $new_rei->availability_date = $request->availability_date;
            $new_rei->amount_in_peso = $request->amount_in_peso;
            $new_rei->shortlist_allowed = $request->shortlist_allowed;
            $new_rei->shortlist_criteria = $request->shortlist_criteria;
            $new_rei->procedure = $request->procedure;
            $new_rei->completed_within = $request->completed_within;
            $new_rei->chairperson_name = $request->chairperson_name;
            $new_rei->save();

            //upload attachment snippet
            if($request->hasFile('invite_requirements')){
                foreach($request->invite_requirements as $file){
                    $filepath = $file->store('invite_file_attachments/'.$new_rei->invite->form_no);
                    $attachments[] = new InviteFile([
                        'filename' => $file->getClientOriginalName(),
                        'filepath' => $filepath
                    ]);
                }
                $new_rei->invite->files()->saveMany($attachments);
            }

            $pr = PR::find($request->pr_id);

            if ($new_rei) {
                $notif_recipient = User::find($pr->requested_by);
                $obj = new \stdClass();
                $obj->message = Auth::user()->fullName . ' generated an REI for PR#'.$pr->pr_no;
                $obj->link = url('/pr/'.$pr->id);
                SystemNotification::add($notif_recipient, $obj, 'inv', $new_invite);
                $notif_recipient->notify(new EmailNotification($obj));

                $adminUsers = User::where('user_role', 3)->get();
                foreach($adminUsers as $adminUser) {
                    $obj = new \stdClass();
                    $obj->message = 'An REI is waiting for your approval.';
                    $obj->link = url('/rei/'.$new_rei->id);
                    SystemNotification::add($adminUser, $obj, 'inv', $new_invite);
                    $adminUser->notify(new EmailNotification($obj));
                }

                return redirect('/rei/'.$new_rei->id)->with('rei', $new_rei)->with('success', 'Request for Expression of Interest created successfully!');
            }
        }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ReqExpInterest  $reqExpInterest
     * @return \Illuminate\Http\Response
     */
    public function show(ReqExpInterest $reqExpInterest)
    {
        if(!$reqExpInterest) {
            abort(404);
        }
        if(
            (Auth::user()->user_role === 3 && $reqExpInterest->invite->status === 'FOR_REVIEW/APPROVAL') ||
            (Auth::user()->user_role === 5 && $reqExpInterest->invite->status === 'FOR_EXPORT')
        ) {
            SystemNotification::checkURL();
        }

        $selected_suppliers = [];
        if ($reqExpInterest->invite->suppliers()->exists()) {
            foreach($reqExpInterest->invite->suppliers as $supplier){
                $selected_suppliers[] = $supplier->id;
            }
        }

        if($reqExpInterest->invite->status === 'FOR_EXPORT' && Auth::user()->user_role === 5){
            $suppliers = Supplier::all();
            return view('req_exp_interests.show')->with('rei', $reqExpInterest)->with('suppliers', $suppliers)->with('selected_suppliers', $selected_suppliers);;
        } else {
            return view('req_exp_interests.show')->with('rei', $reqExpInterest);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ReqExpInterest  $reqExpInterest
     * @return \Illuminate\Http\Response
     */
    public function edit(ReqExpInterest $reqExpInterest)
    {
        if(!$reqExpInterest) {
            abort(404);
        }
        $yearsArray = [];
        for ($year = date('Y'); $year <= date('Y') + 10; $year++ ) {
            $yearsArray[$year] = $year;
        }
        return view('req_exp_interests.edit')->with('rei', $reqExpInterest)->with('yearsArray', $yearsArray);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ReqExpInterest  $reqExpInterest
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReqExpInterest $reqExpInterest)
    {
        $request->validate([
                'project_name'          => 'required',
                'year'                  => 'required',
                'abc'                   => 'required',
                'brief_desc'            => 'required',
                'opening_date'          => 'required',
                'availability_date'     => 'required',
                'amount_in_peso'        => 'required',
                'shortlist_allowed'     => 'required',
                'shortlist_criteria'    => 'required',
                'procedure'             => 'required',
                'completed_within'      => 'required',
                'chairperson_name'      => 'required',
        ]);

        $invite = Invite::find($reqExpInterest->invite->id);
        $invite->status = 'FOR_REVIEW/APPROVAL';
        $invite->rejects_remarks .= " (UPDATED)";
        $invite->save();

        $reqExpInterest->project_name = $request->project_name;
        $reqExpInterest->year = $request->year;
        $reqExpInterest->abc = $request->abc;
        $reqExpInterest->brief_desc = $request->brief_desc;
        $reqExpInterest->opening_date = $request->opening_date;
        $reqExpInterest->availability_date = $request->availability_date;
        $reqExpInterest->amount_in_peso = $request->amount_in_peso;
        $reqExpInterest->shortlist_allowed = $request->shortlist_allowed;
        $reqExpInterest->shortlist_criteria = $request->shortlist_criteria;
        $reqExpInterest->procedure = $request->procedure;
        $reqExpInterest->completed_within = $request->completed_within;
        $reqExpInterest->chairperson_name = $request->chairperson_name;
        $reqExpInterest->save();

        if ($reqExpInterest)
            return redirect('/rei/'.$reqExpInterest->id)->with('success', 'Request for Expression of Interest updated successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ReqExpInterest  $reqExpInterest
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReqExpInterest $reqExpInterest)
    {
        //
    }

    public function signed(ReqExpInterest $rei){
        $rei->signed = 1;
        $rei->save();

        $log_arr = [
            "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            "user_office" => Auth::user()->office->office,
            "activity"  => ' received the signed copy of REI#' . $rei->invite->form_no,
        ];
        \ActivityLog::add($log_arr);

        return redirect()->back()->with('success', 'Successfully received signed copy.');
    }
}
