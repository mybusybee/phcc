<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PurchaseRequest as PR;
use App\Pmr;
use App\PmrItem;
use App\Office;
use App\PmrItemDetail as PID;
use App\AnnualProcurementPlan as APP;
use App\ProcurementUserPmr as PersonalPmr;
use Auth;
use App\ProcurementUserPmrItem as PersonalPmrItem;
use Excel;
use App\UploadedPmr;
use App\UploadedPmrItem;

class PMRController extends Controller
{
    public function personalPMRShow(){
        return view('pmr.personal.show.show');
    }

    public function personalPMREdit($semester, $year){
        $pmr = PersonalPmr::where([
            'procurement_user_id' => Auth::id(),
            'year' => $year,
            'semester' => $semester
        ])->first();

        $modified = $this->restructure_pmr($pmr);

        if($pmr->semester === 2){
            $pmr->sem = '2nd Semester';
        } else {
            $pmr->sem = '1st Semester';
        }

        return view('pmr.personal.edit.edit')->with('pmr', $modified);
    }

    public function personalPMRUpdate(Request $request, PersonalPmr $pmr){
        $firstPMR                       = PersonalPmr::first();
        $firstPMR->prepared_by          = $request->prepared_by;
        $firstPMR->recommended_approval = $request->recommended_approval;
        $firstPMR->approved_by          = $request->approved_by;
        $firstPMR->save();

        $i = 0;
        if($request->ongoing !== null) {
            foreach($request->ongoing as $ongoing_item_id){
                $item                              = PersonalPmrItem::find($ongoing_item_id);
                $item->pre_proc_conference         = $request->input('o_pre_proc_conference')[$i];
                $item->ads_post_of_ib              = $request->input('o_ads_post_of_ib')[$i];
                $item->pre_bid_conf                = $request->input('o_pre_bid_conf')[$i];
                $item->eligibility_check           = $request->input('o_eligibility_check')[$i];
                $item->sub_open_of_bids            = $request->input('o_sub_open_of_bids')[$i];
                $item->bid_evaluation              = $request->input('o_bid_evaluation')[$i];
                $item->post_qual                   = $request->input('o_post_qual')[$i];
                $item->noa                         = $request->input('o_noa')[$i];
                $item->contract_signing            = $request->input('o_contract_signing')[$i];
                $item->ntp                         = $request->input('o_ntp')[$i];
                $item->delivery_completion         = $request->input('o_delivery_completion')[$i];
                $item->inspection_acceptance       = $request->input('o_inspection_acceptance')[$i];
                $item->abc_mooe                    = $request->input('o_mooe')[$i];
                $item->abc_co                      = $request->input('o_co')[$i];
                $item->contract_cost_mooe          = $request->input('o_contract_cost_mooe')[$i];
                $item->contract_cost_co            = $request->input('o_contract_cost_co')[$i];
                $item->list_of_invited_observers   = $request->input('o_list_of_invited_observers')[$i];
                $item->receipt_pre_bid_conf        = $request->input('o_receipt_pre_bid_conf')[$i];
                $item->receipt_eligibility_check   = $request->input('o_receipt_eligibility_check')[$i];
                $item->receipt_sub_open_of_bids    = $request->input('o_receipt_sub_open_of_bids')[$i];
                $item->receipt_bid_evaluation      = $request->input('o_receipt_bid_evaluation')[$i];
                $item->receipt_post_qual           = $request->input('o_receipt_post_qual')[$i];
                $item->receipt_delivery_completion = $request->input('o_receipt_delivery_completion')[$i];
                $item->remarks                     = $request->input('o_remarks')[$i];
                $item->status                      = $request->input('o_status')[$i];
                if($item->status === 'Cancelled' || $item->status === 'Failed'){
                    $item->completed = 1;
                    $this->returnBudget($item->pr_id);
                } else {
                    if($item->contract_signing !== null) {
                        $item->completed = 1;
                    }
                }
                $item->save();
                $i++;
            }
        }

        $x = 0;
        if($request->completed !== null){
            foreach($request->completed as $completed_item_id) {
                $item_c                              = PersonalPmrItem::find($completed_item_id);
                $item_c->pre_proc_conference         = $request->input('c_pre_proc_conference')[$x];
                $item_c->ads_post_of_ib              = $request->input('c_ads_post_of_ib')[$x];
                $item_c->pre_bid_conf                = $request->input('c_pre_bid_conf')[$x];
                $item_c->eligibility_check           = $request->input('c_eligibility_check')[$x];
                $item_c->sub_open_of_bids            = $request->input('c_sub_open_of_bids')[$x];
                $item_c->bid_evaluation              = $request->input('c_bid_evaluation')[$x];
                $item_c->post_qual                   = $request->input('c_post_qual')[$x];
                $item_c->noa                         = $request->input('c_noa')[$x];
                $item_c->contract_signing            = $request->input('c_contract_signing')[$x];
                $item_c->ntp                         = $request->input('c_ntp')[$x];
                $item_c->delivery_completion         = $request->input('c_delivery_completion')[$x];
                $item_c->inspection_acceptance       = $request->input('c_inspection_acceptance')[$x];
                $item_c->abc_mooe                    = $request->input('c_mooe')[$x];
                $item_c->abc_co                      = $request->input('c_co')[$x];
                $item_c->contract_cost_mooe          = $request->input('c_contract_cost_mooe')[$x];
                $item_c->contract_cost_co            = $request->input('c_contract_cost_co')[$x];
                $item_c->list_of_invited_observers   = $request->input('c_list_of_invited_observers')[$x];
                $item_c->receipt_pre_bid_conf        = $request->input('c_receipt_pre_bid_conf')[$x];
                $item_c->receipt_eligibility_check   = $request->input('c_receipt_eligibility_check')[$x];
                $item_c->receipt_sub_open_of_bids    = $request->input('c_receipt_sub_open_of_bids')[$x];
                $item_c->receipt_bid_evaluation      = $request->input('c_receipt_bid_evaluation')[$x];
                $item_c->receipt_post_qual           = $request->input('c_receipt_post_qual')[$x];
                $item_c->receipt_delivery_completion = $request->input('c_receipt_delivery_completion')[$x];
                $item_c->remarks                     = $request->input('c_remarks')[$x];
                $old_status                          = $item_c->status;
                $item_c->status                      = $request->input('c_status')[$x];
                if($item_c->status === 'Failed' || $item_c->status === 'Cancelled') {
                    $item_c->completed = 1;
                    if($old_status != 'Failed'){
                        if($old_status != 'Cancelled'){
                            $this->returnBudget($item_c->pr_id);
                        }
                    }
                } else {
                    if($item_c->contract_signing === null) {
                        $item_c->completed = 0;
                    } else {
                        $item_c->completed = 1;
                    }

                    if ($old_status == 'Failed' || $old_status == 'Cancelled'){

                        $this->redeductBudget($item_c->pr_id);
                    }
                }
                $item_c->save();
                $x++;
            }
        }
        return redirect()->back()->with('success', 'Successfully updated!');
    }

    private function returnBudget($pr_id){
        $pr = PR::where('id', $pr_id)->first();

        foreach($pr->pr_items as $pr_item){
            $est_cost_total                                = $pr_item->est_cost_total;
            $pr_item->app->pap->moving_app->actual_budget += $est_cost_total;
            $pr_item->app->pap->moving_app->save();    
        }
    }

    private function redeductBudget($pr_id){
        $pr = PR::where('id', $pr_id)->first();

        foreach($pr->pr_items as $pr_item){
            $est_cost_total = $pr_item->est_cost_total;
            $pr_item->app->pap->moving_app->actual_budget -= $est_cost_total;
            $pr_item->app->pap->moving_app->save();
        }
    }

    public function mainPMRShow(){
        return view('pmr.main.show.show');
    }

    private function restructure_pmr($pmr) {
        foreach($pmr->pmr_items as $item){
            $program_project_array  = [];
            $end_user_array         = [];
            $procurement_mode_array = [];
            $source_of_funds_array  = [];
            $code_array             = [];
            foreach($item->pr->pr_items as $pr_item){
                $code_array            [] = $pr_item->app->code;
                $program_project_array [] = $pr_item->app->program_project;
                $end_user_array        [] = $pr_item->app->office->office;
                $procurement_mode_array[] = $pr_item->app->procurementMode->mode;
                $source_of_funds_array [] = $pr_item->app->source_of_funds;
                $item->total_app_budget  += (float)str_replace(',', '', $pr_item->app->total_estimated_budget);
            }

            if($item->completed === 1) {
                $item->total_contract_cost           += $this->getTotalContractCost($item);
                $pmr->total_completed_contract_cost  += $item->total_contract_cost;
                $pmr->total_completed_alloted_budget += (float)str_replace(',', '', $item->pr->pr_item_total->total_estimate);
            }

            if($item->completed === 0){
                $pmr->total_ongoing_abc                += (float)str_replace(',', '', $item->pr->pr_item_total->total_estimate);
                $pmr->total_ongoing_abc_mooe           += $item->abc_mooe;
                $pmr->total_ongoing_abc_co             += $item->abc_co;
                $pmr->total_ongoing_contract_cost_mooe += $item->contract_cost_mooe;
                $pmr->total_ongoing_contract_cost_co   += $item->contract_cost_co;
                $pmr->total_ongoing_contract_cost      += $this->getTotalContractCost($item);
            }

            
            $item->program_projects = $program_project_array;
            $item->end_user = implode(', ', array_unique($end_user_array));
            $item->procurement_mode = implode(', ', array_unique($procurement_mode_array));
            $item->source_of_funds = implode(', ', array_unique($source_of_funds_array));
            $item->code = implode(', ', $code_array);
        }

        return $pmr;
    }

    private function getTotalContractCost($item) {
        $total_contract_cost = 0;
        if($item->pr->itb) {
            if(count($item->pr->itb->invite->abstracts)){
                foreach($item->pr->itb->invite->abstracts as $abstract) {
                    $total_contract_cost += $abstract->recommendation->total;
                }
            } else {
                $total_contract_cost = 0;
            }
        } elseif($item->pr->rei) {
            if(count($item->pr->rei->invite->abstracts)){
                foreach($item->pr->rei->invite->abstracts as $abstract) {
                    $total_contract_cost += $abstract->recommendation->total;
                }
            } else {
                $total_contract_cost = 0;
            }
        } elseif(count($item->pr->rfp)) {
            foreach($item->pr->rfp as $pr_rfp) {
                if(count($pr_rfp->invite->abstracts)) {
                    foreach($pr_rfp->invite->abstracts as $invite_abstract) {
                        $total_contract_cost += $invite_abstract->recommendation->total;
                    }
                } else {
                    $total_contract_cost = 0;
                }
            }  $total_contract_cost = 0;
        } elseif(count($item->pr->rfq)) {
            foreach($item->pr->rfq as $pr_rfq) {
                if(count($pr_rfq->invite->abstracts)) {
                    foreach($pr_rfq->invite->abstracts as $invite_abstract) {
                        $total_contract_cost += $invite_abstract->recommendation->total;
                    }
                } else {
                    $total_contract_cost = 0;
                }
            }
        } else {
            $total_contract_cost = 0;
        }

        return $total_contract_cost;
    }

    public function downloadTemplate() {
        $filepath = storage_path('pmruploadtemplate.xlsx');
        $filename = time().'.xlsx';
        return response()->download($filepath, $filename);
    }

    public function import (Request $request) {

        $request->validate([
            'file' => 'required',
        ]);

        if($request->hasFile('file')) {
            $extension = \File::extension($request->file->getClientOriginalName());
            $valid = ['xlsx', 'xls', 'csv'];
            if(in_array($extension, $valid)) {
                $path = $request->file->getRealPath();
                Excel::selectSheets('main')->load($path, function($reader) use ($path) {
                    foreach($reader->toArray() as $row) {
                        $pmr                               = new UploadedPmr;
                        $pmr->procurement_user_id          = $row['procurement_user_id'];
                        $pmr->year                         = $row['year'];
                        $pmr->semester                     = $row['semester'];
                        $pmr->prepared_by                  = $row['prepared_by'];
                        $pmr->recommended_approval         = $row['recommended_approval'];
                        $pmr->approved_by                  = $row['approved_by'];
                        $pmr->total_alloted_budget         = $row['total_alloted_budget'];
                        $pmr->total_contract_price         = $row['total_contract_price'];
                        $pmr->total_savings                = $row['total_savings'];
                        $pmr->total_alloted_budget_ongoing = $row['total_alloted_budget_ongoing'];
                        
                        if($pmr->save()) {
                            Excel::selectSheets('items')->load($path, function($r) use ($pmr) {
                                foreach($r->toArray() as $row){
                                    if($row['pr_no'] === null) {
                                        break;
                                    }
                                    $item                                      = new UploadedPmrItem;
                                    $item->uploaded_pmr_id                     = $pmr->id;
                                    $item->pr_no                               = $row['pr_no'];
                                    $item->code                                = $row['code'];
                                    $item->program_project                     = $row['program_project'];
                                    $item->end_user_id                         = $row['end_user_id'];
                                    $item->procurement_mode_id                 = $row['procurement_mode_id'];
                                    $item->pr_date_received                    = $row['pr_date_received'];
                                    $item->source_of_funds                     = $row['source_of_funds'];
                                    $item->list_of_invited_observers           = $row['list_of_invited_observers'];
                                    $item->abc_total                           = $row['abc_total'];
                                    $item->abc_mooe                            = $row['abc_mooe'];
                                    $item->abc_co                              = $row['abc_co'];
                                    $item->contract_cost_total                 = $row['contract_cost_total'];
                                    $item->contract_cost_mooe                  = $row['contract_cost_mooe'];
                                    $item->contract_cost_co                    = $row['contract_cost_co'];
                                    $item->apa__pre_proc_conference            = $row['actual_pre_proc_conference'];
                                    $item->apa__ads_post_of_ib                 = $row['actual_ads_post_of_ib'];
                                    $item->apa__pre_bid_conf                   = $row['actual_pre_bid_conf'];
                                    $item->apa__eligibility_check              = $row['actual_eligibility_check'];
                                    $item->apa__sub_open_of_bids               = $row['actual_sub_open_of_bids'];
                                    $item->apa__bid_evaluation                 = $row['actual_bid_evaluation'];
                                    $item->apa__post_qual                      = $row['actual_post_qual'];
                                    $item->apa__notice_of_award                = $row['actual_notice_of_award'];
                                    $item->apa__contract_signing               = $row['actual_contract_signing'];
                                    $item->apa__notice_to_proceed              = $row['actual_notice_to_proceed'];
                                    $item->apa__delivery_completion            = $row['actual_delivery_completion'];
                                    $item->apa__inspection_and_acceptance      = $row['actual_inspection_and_acceptance'];
                                    $item->dri__pre_bid_conf                   = $row['receipt_pre_bid_conf'];
                                    $item->dri__eligibility_check              = $row['receipt_eligibility_check'];
                                    $item->dri__sub_open_of_bids               = $row['receipt_sub_open_of_bids'];
                                    $item->dri__bid_evaluation                 = $row['receipt_bid_evaluation'];
                                    $item->dri__post_qual                      = $row['receipt_post_qual'];
                                    $item->dri__delivery_completion_acceptance = $row['receipt_delivery_completion_acceptance'];
                                    $item->remarks                             = $row['remarks'];
                                    $item->status                              = $row['status'];
                                    $completed = ['Failed', 'Cancelled', 'Completed'];
                                    if(in_array($row['status'], $completed)) {
                                        $item->completed = 1;
                                    } else {
                                        $item->completed = 0;
                                    }
                                    $item->save();
                                }
                            });
                        }
                    }
                });
                return redirect()->back()->with('success', 'PMR imported successfully.');
            } else {
                return redirect()->back()->with('error', 'Please upload "csv/xls/xlsx" files only.');
            }
        } else {
            return redirect()->back()->with('error', 'Something went wrong. Please try again later.');
        }
    }
}