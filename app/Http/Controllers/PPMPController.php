<?php

namespace App\Http\Controllers;

use App\ProjectProcurementManagementPlan;
use App\User;
use Illuminate\Http\Request;
use App\ProcurementMode;
use App\ProjectsProgramsActivities;
use App\PAPProcurementMode;
use App\PapSchedule;
use Auth;
use App\APPSchedule;
use SystemNotification;
use App\Notifications\EmailNotification;
use App\SuperAdminControl;
use Excel;
use App\UacsObject;
use App\Office;
use App\CategoryCode;
use App\AnnualProcurementPlan;

class PPMPController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        /****************
        ** ADMIN / GOD **
        *****************/
        $offices = Office::all();
        
        if(session()->get('user_role') == 1 || session()->get('user_role') == 3 || session()->get('user_role') == 5){
            $ppmps = ProjectProcurementManagementPlan::orderBy('created_at', 'desc')->get();
            $users = User::all();
            $offices = Office::where([
                ['id', '!=', '1'],
                ['id', '!=', '11']
            ])->get();

            $log_arr = [
                "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
                "user_office" => Auth::user()->office->office,
                "activity"  => ' viewed PPMP List.',
            ];
            \ActivityLog::add($log_arr);

            return view('ppmps.index')->with('ppmps', $ppmps)
                                      ->with('offices', $offices);
            /*************
            ** DIRECTOR **
            **************/
        } else if( session()->get('user_role') == 4 || session()->get('user_role') == 7 ){
            $ppmps = ProjectProcurementManagementPlan::orderBy('created_at', 'desc')
                                                      ->where('end_user', \Auth::user()->office_id)
                                                      ->get();
            $users = User::all();
            foreach($ppmps as $ppmp){
                $log_arr = [
                    "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
                    "user_office" => Auth::user()->office->office,
                    "activity"  => ' viewed PPMP List.',
                ];
                \ActivityLog::add($log_arr);
            }
            return view('ppmps.index')->with('ppmps', $ppmps);
        /***********
        ** BUDGET **
        ************/

        } else if( session()->get('user_role') == 6 ){
            // $ppmps = ProjectProcurementManagementPlan::orderBy('created_at', 'desc')->where(['status' => 'FOR_BUDGET_VALIDATION'])->get();
            $ppmps = ProjectProcurementManagementPlan::orderBy('created_at', 'desc')->get();
            $users = User::all();
            $log_arr = [
                "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
                "user_office" => Auth::user()->office->office,
                "activity"  => ' viewed PPMP List.',
            ];
            \ActivityLog::add($log_arr);

            return view('ppmps.index')->with('ppmps', $ppmps);
            /****************
            ** PROCUREMENT **
            *****************/
        } else if( session()->get('user_role') == 8 ){
            $ppmps = ProjectProcurementManagementPlan::orderBy('created_at', 'desc')->get();
            $users = User::all();
            $offices = Office::where([
                ['id', '!=', '1'],
                ['id', '!=', '11']
            ])->get();
            $log_arr = [
                "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
                "user_office" => Auth::user()->office->office,
                "activity"  => ' viewed PPMP List.',
            ];
            \ActivityLog::add($log_arr);
            return view('ppmps.index')->with('ppmps', $ppmps)
                                      ->with('offices', $offices);
        }
    }

    public function movingPPMP(){
        $ppmps = ProjectProcurementManagementPlan::orderBy('created_at', 'desc')->get();

        if(session()->get('user_role') == 4 || session()->get('user_role') == 7){
            
            $ppmps = ProjectProcurementManagementPlan::orderBy('created_at', 'desc')
                                                      ->where('end_user', \Auth::user()->office_id)
                                                      ->get();
        }

        foreach($ppmps as $ppmp){
            $actual_budget = 0;

            foreach($ppmp->paps as $pap){
                if($pap->moving_app){
                    $actual_budget += $pap->moving_app->actual_budget;
                }else{
                    $actual_budget += (float)str_replace(",", "", $pap->estimated_budget);
                }
            }

            $ppmp->actual_budget = $actual_budget;
        }

        return view('ppmps.moving-index')->with('ppmps', $ppmps);

    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        if(Auth::user()->user_role === 4){
            return redirect()->back()->with('error', "You're not allowed to access that page.");
        }
        
        if(SuperAdminControl::where('control', 'PPMP')->first()->is_active === 0){
            return redirect()->back()->with('error', 'PPMP creation is disabled.');
        }
        
        $users = User::all()->toArray();
        $users_array = [];
        foreach($users as $user){
            $users_array[$user['id']] = $user['first_name'] . ' ' . $user['last_name'];
        }

        $directors = User::where(['user_role' => 4, 'office_id' => Auth::user()->office->id])->get()->toArray();
        $directors_array = [];
        foreach($directors as $director){
            $directors_array[$director['id']] = $director['first_name'] . ' ' . $director['last_name'];
        }

        $procurement_modes = ProcurementMode::where('id','!=', 8)->get();
        $objects = UacsObject::all();

        return view('ppmps.create')
        ->with('users', $users_array)
        ->with('procurement_modes', $procurement_modes)
        ->with('directors', $directors_array)
        ->with('objects', $objects);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {

        $request->validate([
            'end_user'                  => 'required',
            'total_estimated_budget'    => 'required',
            'prepared_by'               => 'required',
            'approved_by'               => 'required'
        ]);

        $ppmp = new ProjectProcurementManagementPlan;
        $ppmp->end_user                 = Auth::user()->office->id;
        $ppmp->total_estimated_budget   = $request->total_estimated_budget;
        $ppmp->prepared_by              = $request->prepared_by;
        $ppmp->approved_by              = $request->approved_by;
        $ppmp->for_year                 = $request->ppmp_year;
        $ppmp->created_by               = Auth::id();
        if($request->saveType === 'submit') {
            $ppmp->status                   = "FOR_DIRECTOR_APPROVAL";
        } else {
            $ppmp->status 				= 'DRAFT';
        }
        $ppmp->save();
        if($ppmp){
            for ($i = 1; $i <= count($request->row_counter); $i++){
                $pap                      = new ProjectsProgramsActivities;
                $pap->ppmp_id             = $ppmp->id;
                $pap->code                = $request->input('pap_code'.$i);
                $pap->general_description = $request->input('pap_gen_desc'.$i);
                $pap->size                = $request->input('pap_qty'.$i);
                $pap->uacs_object_id      = $request->input('pap_uacs_object'.$i);
                $pap->estimated_budget    = $request->input('pap_est_budget'.$i);
                $pap->allotment_type      = strtoupper($request->input('allotment_type'.$i));
                if($pap->allotment_type === 'MOOE') {
                    $pap->co = '0.00';
                    $pap->mooe = $pap->estimated_budget;
                } else {
                    $pap->mooe = '0.00';
                    $pap->co = $pap->estimated_budget;
                }
                $pap->save();

                if($pap){
                    $pap_mode_of_procurement                      = new PAPProcurementMode;
                    $pap_mode_of_procurement->pap_id              = $pap->id;
                    $pap_mode_of_procurement->procurement_mode_id = $request->input('pap_mode_of_procurement'.$i);
                    $pap_mode_of_procurement->save();
                    // $sched->advertisement    = $request->input('advertisement_date'.$i);
                    // $sched->submission       = $request->input('submission_date'.$i);
                    // $sched->noa              = $request->input('noa_date'.$i);
                    // $sched->contract_signing = $request->input('contract_signing_date'.$i);
                    $months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];

                    $month_count = 0;
                    foreach($request->input('allocation'.$i) as $amount) {
                        $sched                   = new PapSchedule;
                        $sched->pap_id           = $pap->id;
                        $sched->month = $months[$month_count];
                        $sched->allocation = $amount == '' ? 0.00 : $amount;
                        $sched->save();
                        $month_count++;
                    }
                    
                }
            }
        }

        if($request->saveType === 'submit') {
            $directorUsers = User::where([
                'user_role' => 4,
                'office_id' => $ppmp->end_user,
            ])->get();
            
            foreach($directorUsers as $directorUser){
                $obj = new \stdClass();
                $obj->message = "A new PPMP is waiting for your approval.";
                $obj->link = url('/ppmps/'.$ppmp->id);
                SystemNotification::add($directorUser, $obj, 'ppmp', $ppmp);
                $directorUser->notify(new EmailNotification($obj));
            }

            $log_arr = [
                "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
                "user_office" => Auth::user()->office->office,
                "activity"  => ' created a new PPMP for Year - '.$ppmp->for_year,
            ];
            \ActivityLog::add($log_arr);
            return redirect('/ppmps')->with('success', 'PPMP successfully created!');
        } else {
            $log_arr = [
                "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
                "user_office" => Auth::user()->office->office,
                "activity"  => ' created a new draft PPMP for Year - '.$ppmp->for_year,
            ];
            \ActivityLog::add($log_arr);
            return redirect('/ppmps')->with('success', 'PPMP successfully saved as draft!');
        }
    }

    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $ppmp = ProjectProcurementManagementPlan::find($id);
        if(!$ppmp) {
            abort(404);
        }
        $users = User::all();
        $procurement_modes = ProcurementMode::where('id', '!=', 8)->get();

        foreach($users as $user) {
            if($user->id == $ppmp->remarks_last_modifier){
                $ppmp->remarks_author = $user->first_name . ' ' . $user->last_name;
            }
        }

        $log_arr = [
            "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            "user_office" => Auth::user()->office->office,
            "activity"  => ' viewed PPMP for Year - '.$ppmp->for_year,
        ];
        \ActivityLog::add($log_arr);
        SystemNotification::checkURL();

        return view('ppmps.show')->with('ppmp', $ppmp)->with('procurement_modes', $procurement_modes);
    }

    public function showMoving(ProjectProcurementManagementPlan $ppmp){
        $procurement_modes = ProcurementMode::where('id', '!=', 8)->get();

        $actual_budget = 0;

        foreach($ppmp->paps as $pap){
            if($pap->moving_app){
                $actual_budget += $pap->moving_app->actual_budget;
            }else{
                $actual_budget += (float)str_replace(",", "", $pap->estimated_budget);
            }
        }

        $ppmp->actual_budget = $actual_budget;

        return view('ppmps.moving-show')->with('ppmp', $ppmp)->with('procurement_modes', $procurement_modes);
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {

        $ppmp = ProjectProcurementManagementPlan::find($id);
        if(!$ppmp) {
            abort(404);
        }

        if( ( $ppmp->created_by !== Auth::id() ) &&
            ( Auth::user()->user_role === 7 )
        ) {
            return redirect()->back()->with('error', 'Unauthorized!');
        }

        $users = User::all()->toArray();
        $users_array = [];
        foreach($users as $user){
            $users_array[$user['id']] = $user['first_name'] . ' ' . $user['last_name'];
            if($user['id'] == $ppmp->created_by){
                $ppmp->prep_name = $user['first_name'] . ' ' . $user['last_name'];
            }
            if($user['id'] == $ppmp->remarks_last_modifier){
                $ppmp->remarks_author = $user['first_name'] . ' ' . $user['last_name'];
            }
        }

        $directors = User::where('user_role', 4)->get()->toArray();
        $directors_array = [];
        foreach($directors as $director){
            $directors_array[$director['id']] = $director['first_name'] . ' ' . $director['last_name'];
        }

        $procurement_modes = ProcurementMode::where('id', '!=', 8)->get();

        if(
            (Auth::user()->user_role === 6 && $ppmp->status !== 'FOR_BUDGET_VALIDATION') ||
            (Auth::user()->user_role === 8 && $ppmp->status !== 'FOR_PROCUREMENT_VALIDATION')
        ) {
            SystemNotification::checkURL();
            return redirect('ppmps/'.$ppmp->id)->with('error', 'Invalid request.');
        }
        if(Auth::user()->user_role === 6 || Auth::user()->user_role === 8 || (Auth::user()->user_role === 7 || $ppmp->status === 'FOR_END_USER_REVISION')) {
            SystemNotification::checkURL();
        }

        $pap_ids_array = [];
        foreach($ppmp->paps as $pap) {
            $pap_ids_array[] = $pap->id;
        }

        $objects = UacsObject::all();
        $category_codes = CategoryCode::all();

        return view('ppmps.edit')
        ->with('ppmp', $ppmp)
        ->with('users', $users_array)
        ->with('procurement_modes', $procurement_modes)
        ->with('directors', $directors_array)
        ->with('pap_ids_array', $pap_ids_array)
        ->with('objects', $objects)
        ->with('category_codes', $category_codes);
    }
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $request->validate([
            'end_user'                  => 'required',
            'total_estimated_budget'    => 'required',
            'prepared_by'               => 'required',
            'approved_by'              => 'required'
        ]);

        \DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        $ppmp = ProjectProcurementManagementPlan::find($id);
        $ppmp->end_user                 = $ppmp->end_user;
        $ppmp->total_estimated_budget   = $request->total_estimated_budget;
        $ppmp->prepared_by              = $request->prepared_by;
        $ppmp->for_year                 = $request->ppmp_year;
        $ppmp->approved_by              = $request->approved_by;
        if(session()->get('user_role') == 7){
            if($ppmp->status === 'FOR_END_USER_REVISION') {
                $directorUsers = User::where([
                    'user_role' => 4,
                    'office_id' => $ppmp->end_user,
                ])->get();
                
                foreach($directorUsers as $directorUser){
                    $obj = new \stdClass();
                    $obj->message = "A new PPMP is waiting for your approval.";
                    $obj->link = url('/ppmps/'.$ppmp->id);
                    SystemNotification::add($directorUser, $obj, 'ppmp', $ppmp);
                    $directorUser->notify(new EmailNotification($obj));
                }
            }
            if($request->saveType === 'submit') {
                $ppmp->status                   = "FOR_DIRECTOR_APPROVAL";
            } else {
                $ppmp->status                   = "DRAFT";
            }
            
        } else if(session()->get('user_role') == 4) {
            $ppmp->remarks = null;
            $ppmp->remarks_last_modifier = null;
            $ppmp->status = 'PPMP_APPROVED';

            $notif_recipient = User::find($ppmp->created_by);
            $obj = new \stdClass();
            $obj->message = "Your PPMP is now approved.";
            $obj->link = url('/ppmps/'.$ppmp->id);
            SystemNotification::add($notif_recipient, $obj, 'ppmp', $ppmp);
            $notif_recipient->notify(new EmailNotification($obj));

        } else if(session()->get('user_role') == 6){
            if($request->budgetValidation === '1') {
                //within wfp
                $ppmp->remarks = null;
                $ppmp->remarks_last_modifier = null;
                $ppmp->status = 'FOR_PROCUREMENT_VALIDATION';

                $obj = new \stdClass();
                $obj->message = Auth::user()->fullName . ' validated your PPMP.';
                $obj->link = url('/ppmps/'.$ppmp->id);
                SystemNotification::add($ppmp->prepared_by_user, $obj, 'ppmp', $ppmp);
                $ppmp->prepared_by_user->notify(new EmailNotification($obj));
            } else {
                //rejected
                $ppmp->status = 'FOR_END_USER_REVISION';
                $ppmp->remarks_last_modifier = Auth::id();

                $obj = new \stdClass();
                $obj->message = Auth::user()->fullName . ' rejected your PPMP.';
                $obj->link = url('/ppmps/'.$ppmp->id.'/edit');
                SystemNotification::add($ppmp->prepared_by_user, $obj, 'ppmp', $ppmp);
                $ppmp->prepared_by_user->notify(new EmailNotification($obj));
            }

            $bacSecUsers = User::where('user_role', 8)->get();
            foreach($bacSecUsers as $bacSecUser) {
                $obj = new \stdClass();
                $obj->message = "A PPMP is waiting for your validation.";
                $obj->link = url('/ppmps/'.$ppmp->id.'/edit');
                SystemNotification::add($bacSecUser, $obj, 'ppmp', $ppmp);
                $bacSecUser->notify(new EmailNotification($obj));
            }

        } else if (Auth::user()->user_role === 8) {
            if($request->procValidation === '1') {
                //validated
                $ppmp->remarks = null;
                $ppmp->remarks_last_modifier = null;
                $ppmp->status = 'FOR_DIRECTOR_APPROVAL_FINAL';
    
                $obj = new \stdClass();
                $obj->message = Auth::user()->fullName . ' validated your PPMP.';
                $obj->link = url('/ppmps/'.$ppmp->id);
                SystemNotification::add($ppmp->prepared_by_user, $obj, 'ppmp', $ppmp);
                $ppmp->prepared_by_user->notify(new EmailNotification($obj));
    
                $directorUsers = User::where([
                    'user_role' => 4,
                    'office_id' => $ppmp->end_user,
                ])->get();
                
                foreach($directorUsers as $directorUser){
                    $obj = new \stdClass();
                    $obj->message = "A new PPMP is waiting for your approval.";
                    $obj->link = url('/ppmps/'.$ppmp->id);
                    SystemNotification::add($directorUser, $obj, 'ppmp', $ppmp);
                    $directorUser->notify(new EmailNotification($obj));
                }
            } else {
                //rejected
                $ppmp = ProjectProcurementManagementPlan::find($id);
                $ppmp->status = 'FOR_END_USER_REVISION';
                $ppmp->remarks = $request->ppmp_reject_remarks;
                $ppmp->remarks_last_modifier = Auth::id();
                $ppmp->save();

                $log_arr = [
                    "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
                    "user_office" => Auth::user()->office->office,
                    "activity"  => ' rejected PPMP for Year - '.$ppmp->for_year,
                ];
                \ActivityLog::add($log_arr);
                
                $notif_recipient = User::find($ppmp->created_by);
                $obj = new \stdClass();
                $obj->message = Auth::user()->fullName . ' rejected your PPMP.';
                $obj->link = url('/ppmps/'.$ppmp->id.'/edit');
                SystemNotification::add($notif_recipient, $obj, 'ppmp', $ppmp);
                $notif_recipient->notify(new EmailNotification($obj));
            }
        }
        $ppmp->save();
        if($ppmp){

            // $i = 1;
            // foreach($request->id as $pap_id){

            //     $i++;
            // }

            // $pap = ProjectsProgramsActivities::updateOrCreate(
            //     ['id' => $request->id],
            //     ['field' => $request->field_name]
            // );

            $remarksUser = [8, 6];
            if(!in_array(Auth::user()->user_role, $remarksUser)) {
                //if user IS NOT procurement or budget (use updateOrCreate based on ID)
                $i = 1;
                foreach($request->id as $pap_id){
                    $pap = ProjectsProgramsActivities::updateOrCreate(
                        ['id' => $pap_id],
                        [
                            'code'                => $request->input('pap_code'.$i),
                            'general_description' => $request->input('pap_gen_desc'.$i),
                            'size'                => $request->input('pap_qty'.$i),
                            'category_code'       => $request->input('category_code'.$i),
                            'uacs_object_id'      => $request->input('pap_uacs_object'.$i),
                            'estimated_budget'    => $request->input('pap_est_budget'.$i),
                            'allotment_type'      => $request->input('allotment_type'.$i),
                        ]
                    );
                    if($pap){
                        //Look for schedules with the same pap id, then delete them
                        // $schedules = PapSchedule::whereIn('pap_id', $request->prev_pap_id)->get();
                        // if($schedules){
                        //     foreach($schedules as $schedule){
                        //         $schedule->delete();
                        //     }
                        // }
                        if(count($pap->pap_schedules)) {
                            foreach($pap->pap_schedules as $allotment) {
                                $allotment->delete();
                            }
                        }
                        //Create new schedule
                        $months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];

                        $month_count = 0;
                        foreach($request->input('allocation'.$i) as $amount) {
                            $sched                   = new PapSchedule;
                            $sched->pap_id           = $pap->id;
                            $sched->month = $months[$month_count];
                            $sched->allocation = $amount == '' ? 0.00 : $amount;
                            $sched->save();
                            $month_count++;
                        }
                        
                        //Create new modes of procurement for each pap
                        $pap->procurement_modes()->detach();
                        $pap_mode_of_procurement                      = new PAPProcurementMode;
                        $pap_mode_of_procurement->pap_id              = $pap->id;
                        $pap_mode_of_procurement->procurement_mode_id = $request->input('pap_mode_of_procurement'.$i);
                        $pap_mode_of_procurement->save();
                    }
                    $i++;
                }
            } else {
                //if user IS procurement or budget (normally find then update/save)
                $i = 1;
                foreach($request->id as $pap_id) {
                    $pap                      = ProjectsProgramsActivities::find($pap_id);
                    $pap->code                = $request->input('pap_code'.$i);
                    $pap->general_description = $request->input('pap_gen_desc'.$i);
                    $pap->category_code       = $request->input('category_code'.$i);
                    $pap->size                = $request->input('pap_qty'.$i);
                    $pap->uacs_object_id      = $request->input('pap_uacs_object'.$i);
                    $pap->estimated_budget    = $request->input('pap_est_budget'.$i);
                    $pap->allotment_type      = $request->input('allotment_type'.$i);
                    if($pap->allotment_type === 'MOOE') {
                        $pap->mooe = $pap->estimated_budget;
                    } else {
                        $pap->co = $pap->estimated_budget;
                    }
                    if($request->input('budget_remarks')[$i - 1] !== null && Auth::user()->user_role === 6) {
                        $pap->budget_remarks      = $request->input('budget_remarks')[$i - 1];
                        $pap->budget_user_remarks = Auth::id();
                    }
                    if($request->input('procurement_remarks')[$i - 1] !== null && Auth::user()->user_role === 8) {
                        $pap->procurement_remarks      = $request->input('procurement_remarks')[$i - 1];
                        $pap->procurement_user_remarks = Auth::id();
                    }
                    $pap->save();
                    $i++;
                }
            }

            // for ($i = 1; $i <= count($request->row_counter); $i++){
            //     $pap                      = new ProjectsProgramsActivities;
            //     $pap->ppmp_id             = $ppmp->id;
            //     $pap->code                = $request->input('pap_code'.$i);
            //     $pap->general_description = $request->input('pap_gen_desc'.$i);
            //     $pap->size                = $request->input('pap_qty'.$i);
            //     $pap->uacs_object_id      = $request->input('pap_uacs_object'.$i);
            //     $pap->estimated_budget    = $request->input('pap_est_budget'.$i);
            //     $pap->allotment_type      = $request->input('allotment_type'.$i);
            //     if($pap->allotment_type === 'MOOE') {
            //         $pap->mooe = $pap->estimated_budget;
            //     } else {
            //         $pap->co = $pap->estimated_budget;
            //     }
            //     if($request->input('budget_remarks')[$i - 1] !== null && Auth::user()->user_role === 6) {
            //         $pap->budget_remarks      = $request->input('budget_remarks')[$i - 1];
            //         $pap->budget_user_remarks = Auth::id();
            //     }
            //     if($request->input('procurement_remarks')[$i - 1] !== null && Auth::user()->user_role === 8) {
            //         $pap->procurement_remarks      = $request->input('procurement_remarks')[$i - 1];
            //         $pap->procurement_user_remarks = Auth::id();
            //     }
            //     $pap->save();

            //     if($pap){
            //         //Look for schedules with the same pap id, then delete them
            //         $schedules = PapSchedule::whereIn('pap_id', $request->prev_pap_id)->get();
            //         if($schedules){
            //             foreach($schedules as $schedule){
            //                 $schedule->delete();
            //             }
            //         }
            //         //Create new schedule
            //         $months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];

            //         $month_count = 0;
            //         foreach($request->input('allocation'.$i) as $amount) {
            //             $sched                   = new PapSchedule;
            //             $sched->pap_id           = $pap->id;
            //             $sched->month = $months[$month_count];
            //             $sched->allocation = $amount == '' ? 0.00 : $amount;
            //             $sched->save();
            //             $month_count++;
            //         }
                    
            //         //Create new modes of procurement for each pap
            //         $pap_mode_of_procurement                      = new PAPProcurementMode;
            //         $pap_mode_of_procurement->pap_id              = $pap->id;
            //         $pap_mode_of_procurement->procurement_mode_id = $request->input('pap_mode_of_procurement'.$i);
            //         $pap_mode_of_procurement->save();
            //     }
            // }
        }

        \DB::statement('SET FOREIGN_KEY_CHECKS = 1');

        //user roles 1, 3, 5, 6, 7 can edit ppmp


        if(Auth::user()->user_role === 6) {
            //budget
            if($request->budgetValidation === '1') {
                //approved
                $log_arr = [
                    "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
                    "user_office" => Auth::user()->office->office,
                    "activity"  => ' approved PPMP ID - '.$ppmp->id,
                ];
                \ActivityLog::add($log_arr);
                return redirect('/ppmps')->with('success', 'PPMP validated.');
            } else {
                //rejected
                $log_arr = [
                    "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
                    "user_office" => Auth::user()->office->office,
                    "activity"  => ' rejected PPMP ID - '.$ppmp->id,
                ];
                \ActivityLog::add($log_arr);
                return redirect('/ppmps')->with('info', 'PPMP sent back to end user with remarks.');
            }
        } else if (Auth::user()->user_role === 8) {
            //bacsec
            if($request->procValidation === '1') {
                //approved
                $log_arr = [
                    "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
                    "user_office" => Auth::user()->office->office,
                    "activity"  => ' approved PPMP of ID '.$ppmp->id,
                ];
                \ActivityLog::add($log_arr);
                return redirect('/ppmps')->with('success', 'PPMP validated.');
            } else {
                //rejected
                $log_arr = [
                    "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
                    "user_office" => Auth::user()->office->office,
                    "activity"  => ' rejected PPMP ID '.$ppmp->id,
                ];
                \ActivityLog::add($log_arr);
                return redirect('/ppmps')->with('info', 'PPMP sent back to end user with remarks.');
            }
        } else {
            $log_arr = [
                "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
                "user_office" => Auth::user()->office->office,
                "activity"  => ' edited PPMP for Year - '.$ppmp->for_year,
            ];
            \ActivityLog::add($log_arr);
            return redirect('/ppmps')->with('info', 'PPMP updated.');
        }
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy(ProjectProcurementManagementPlan $ppmp)
    {
        // $ppmp = ProjectProcurementManagementPlan::find($id);

        $log_arr = [
            "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            "user_office" => Auth::user()->office->office,
            "activity"  => ' deleted PPMP for Year - '.$ppmp->for_year,
        ];
        \ActivityLog::add($log_arr);

        $ppmp->delete();

        return back()->with('success', 'Successfully deleted PPMP!');
    }

    public function delete(ProjectProcurementManagementPlan $ppmp)
    {
        $log_arr = [
            "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            "user_office" => Auth::user()->office->office,
            "activity"  => ' deleted PPMP for Year - '.$ppmp->for_year,
        ];
        \ActivityLog::add($log_arr);

        $ppmp->delete();

        return back()->with('success', 'Successfully deleted PPMP!');
    }

    public function approvePpmp($id){
        $ppmp = ProjectProcurementManagementPlan::find($id);

        if($ppmp->remarks_last_modifier == Auth::id()){
            $ppmp->remarks = null;
            $ppmp->remarks_last_modifier = null;
        }

        if(session()->get('user_role') == 4){
            if($ppmp->status == "FOR_DIRECTOR_APPROVAL"){
                $ppmp->status = 'FOR_BUDGET_VALIDATION';
            } else if($ppmp->status == 'FOR_DIRECTOR_APPROVAL_FINAL'){
                $ppmp->status = 'PPMP_APPROVED';
            }
            $notif_recipient = User::find($ppmp->created_by);
            $obj = new \stdClass();
            $obj->message = Auth::user()->fullName . ' approved your PPMP.';
            $obj->link = url('/ppmps/'.$ppmp->id);
            SystemNotification::add($notif_recipient, $obj, 'ppmp', $ppmp);
            $notif_recipient->notify(new EmailNotification($obj));

            $budgetUsers = User::where('user_role', 6)->get();
            foreach($budgetUsers as $budgetUser) {
                $obj = new \stdClass();
                $obj->message = 'A ppmp is waiting for your validation';
                $obj->link = url('/ppmps/'.$ppmp->id.'/edit');
                SystemNotification::add($budgetUser, $obj, 'ppmp', $ppmp);
                $budgetUser->notify(new EmailNotification($obj));
            }
            
        } else if(session()->get('user_role')  == 5){
            $ppmp->status = 'FOR_DIRECTOR_APPROVAL_FINAL';
        } else if (Auth::user()->user_role == 6){
            $ppmp->status = 'FOR_PROCUREMENT_VALIDATION';
        }

        $ppmp->save();

        if (Auth::user()->user_role == 6 || (Auth::user()->user_role == 5)){
            $log_arr = [
                "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
                "user_office" => Auth::user()->office->office,
                "activity"  => ' validated PPMP for Year - '.$ppmp->for_year,
            ];
            \ActivityLog::add($log_arr);
        }else{
            $log_arr = [
                "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
                "user_office" => Auth::user()->office->office,
                "activity"  => ' approved PPMP for Year - '.$ppmp->for_year,
            ];
            \ActivityLog::add($log_arr);
        }

        return redirect('/ppmps')->with('success', 'PPMP approved successfully!');
    }

    public function rejectPpmp(Request $request, $id){
        $ppmp = ProjectProcurementManagementPlan::find($id);
        $ppmp->status = 'FOR_END_USER_REVISION';
        $ppmp->remarks = $request->ppmp_reject_remarks;
        $ppmp->remarks_last_modifier = Auth::id();
        $ppmp->save();

        $log_arr = [
            "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            "user_office" => Auth::user()->office->office,
            "activity"  => ' rejected PPMP for Year - '.$ppmp->for_year,
        ];
        \ActivityLog::add($log_arr);
        
        $notif_recipient = User::find($ppmp->created_by);
        $obj = new \stdClass();
        $obj->message = Auth::user()->fullName . ' rejected your PPMP.';
        $obj->link = url('/ppmps/'.$ppmp->id.'/edit');
        SystemNotification::add($notif_recipient, $obj, 'ppmp', $ppmp);
        $notif_recipient->notify(new EmailNotification($obj));

        return redirect('/ppmps')->with('info', 'PPMP sent back to end user with remarks.');
    }

    public function allApproved(){
        $approved_ppmps = ProjectProcurementManagementPlan::where('status', 'PPMP_APPROVED')->get();
        $users = User::all();

        $ppmp_year = "";
        foreach($approved_ppmps as $ppmp){
            $ppmp_year = $ppmp->for_year;
            foreach($users as $user){
                if($user->id == $ppmp->created_by){
                    $ppmp->created_by = $user->first_name . ' ' . $user->last_name;
                }
            }
        }

        $apps = AnnualProcurementPlan::all()->count();

        $log_arr = [
            "user_name" => Auth::user()->first_name . ' ' . Auth::user()->last_name,
            "user_office" => Auth::user()->office->office,
            "activity"  => ' viewed all PPMP for Year - '.$ppmp_year,
        ];
        \ActivityLog::add($log_arr);

        return view('ppmps.all_approved')->with('ppmps', $approved_ppmps)
                                         ->with('apps', $apps);
    }

    public function import(Request $request) {
        $request->validate([
            'file' => 'required'
        ]);

        if($request->hasFile('file')) {
            $extension = \File::extension($request->file->getClientOriginalName());
            if($extension == "xlsx" || $extension == "xls" || $extension == "csv") {
                $path                         = $request->file->getRealPath();
                Excel::selectSheets('main')->load($path, function($reader) use ($path) {
                    foreach($reader->toArray() as $row){
                        $ppmp                         = new ProjectProcurementManagementPlan;
                        $ppmp->id                     = $row['id'];
                        $ppmp->end_user               = $row['office_id'];
                        $ppmp->total_estimated_budget = number_format($row['total_estimated_budget'], 2, '.', ',');
                        $ppmp->for_year               = $row['for_year'];
                        $ppmp->status                 = 'PPMP_APPROVED';
                        $ppmp->prepared_by            = $row['prepared_by'];
                        $ppmp->approved_by            = $row['approved_by'];
                        $ppmp->created_by             = Auth::id();
                        $ppmp->save();
                    }
                });
                // ProjectProcurementManagementPlan::create($row);
                Excel::selectSheets('paps')->load($path, function($reader) use($path) {
                    foreach($reader->toArray() as $row) {
                        $pap = new ProjectsProgramsActivities;
                        $pap->ppmp_id = $row['ppmp_id'];
                        $pap->allotment_type = $row['allotment_type'];
                        if(strtolower($row['allotment_type']) === 'mooe') {
                            $pap->mooe = number_format($row['est._budget'], 2, '.', ',');
                        } elseif(strtolower($row['allotment_type']) === 'co') {
                            $pap->co = number_format($row['est._budget'], 2, '.', ',');
                        } else {
                            ProjectProcurementManagementPlan::find($ppmp->id)->delete();
                            return redirect()->back()->with('error', 'Unsupported allotment type. Please check.');
                        }
                        $pap->code                = $row['code'];
                        $pap->general_description = $row['general_description'];
                        $pap->category_code       = $row['category_code'];
                        $pap->uacs_object_id      = $row['uacs_object'];
                        $pap->size                = $row['qtysize'];
                        $pap->estimated_budget    = number_format($row['est._budget'], 2, '.', ',');
                        $pap->save();
                        $pap->procurement_modes()->sync($row['mode_of_procurement']);

                        $months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];

                        for($month_count = 0; $month_count <=11; $month_count++) {
                            $sched                   = new PapSchedule;
                            $sched->pap_id           = $pap->id;
                            $sched->month = $months[$month_count];
                            $sched->allocation = $row[lcfirst($months[$month_count])] == '' ? '0.00' : number_format($row[lcfirst($months[$month_count])], 2, '.', ',');
                            $sched->save();
                        }
                    }
                });
                return redirect()->back()->with('success', 'PPMP imported successfully.');
            } else {
                return redirect()->back()->with('error', 'Please upload "csv/xls/xlsx" file only.');
            }
        }
    }
}