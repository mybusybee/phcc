<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobOrder extends Model
{
    public function ordersParent(){
        return $this->belongsTo('App\OrdersParent', 'orders_parent_id');
    }

    public function supplier(){
        return $this->belongsTo('App\Supplier', 'supplier_id');
    }
}
