<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseRequestBudget extends Model
{
    public function pr(){
        return $this->belongsTo('App\PurchaseRequest', 'pr_id');
    }
}
