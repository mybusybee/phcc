<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnnualProcurementPlan extends Model
{
    protected $table = 'apps';

    protected $fillable = ['code', 'remarks'];

    public function app_schedule(){
    	return $this->hasOne('App\APPSchedule', 'app_id');
    }

    public function office(){
    	return $this->belongsTo('App\Office', 'pmo_end_user');
    }

    public function procurementMode(){
        return $this->belongsTo('App\ProcurementMode', 'mode_of_procurement');
    }

    public function pap(){
        return $this->belongsTo('App\ProjectsProgramsActivities', 'pap_id');
    }

    public function pr_item(){
        return $this->hasMany('App\PurchaseRequestItem', 'app_id');
    }

    public function pmr_items(){
        return $this->hasMany('App\PmrItem', 'app_id');
    }

    public function end_user(){
        return $this->belongsTo('App\Office', 'pmo_end_user');
    }
}
