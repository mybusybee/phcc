<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseRequestItem extends Model
{
    protected $fillable = ['app_id', 'item_no', 'stock_no', 'unit', 'description', 'specs', 'quantity', 'est_cost_unit', 'est_cost_total'];

    public function pr(){
        return $this->belongsTo('App\PurchaseRequest');
    }

    public function pr_sub_items(){
        return $this->hasMany('App\PurchaseRequestSubItem', 'pr_item_id');
    }

    public function app(){
        return $this->belongsTo('App\AnnualProcurementPlan', 'app_id');
    }

    public function suppliers(){
        return $this->belongsToMany('App\Supplier', 'suppliers_pr_items', 'pr_item_id', 'supplier_id')->withPivot('abstract_id', 'unit_cost', 'total', 'per_item_ranking');
    }

    public function recommendation_items(){
        return $this->hasMany('App\AbstractRecoItem', 'pr_item_id');
    }
}
