<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function role(){
        return $this->belongsTo('App\UserRole', 'user_role');
    }

    public function office(){
        return $this->belongsTo('App\Office');
    }

    public function pr_request(){
        return $this->hasMany('App\PurchaseRequest');
    }

    // public function pr_approve(){
    //     return $this->hasMany('App\PurchaseRequest');
    // }

    public function ppmp_prepared_by(){
        return $this->hasMany('App\ProjectProcurementManagementPlan', 'prepared_by');
    }

    // public function pr(){
    //     return $this->belongsToMany('App\PurchaseRequest', 'pr_procurement_users', 'procurement_user_id', 'pr_id');
    // }
    
    public function assignedPr(){
        return $this->hasMany('App\PurchaseRequest', 'proc_user_assigned');
    }

    public function revised_pr(){
        return $this->hasMany('App\PurchaseRequest');
    }

    public function aoq_prepared_by(){
        return $this->hasMany('App\AbstractOfQuotation', 'prepared_by');
    }

    public function aoq_reviewed_by(){
        return $this->hasMany('App\AbstractOfQuotation', 'reviewed_by');
    }

    public function aoq_approved_by(){
        return $this->hasMany('App\AbstractOfQuotation', 'approved_by');
    }

    public function getFullNameAttribute(){
        return $this->first_name . ' ' . $this->last_name;
    }

    public function personal_pmr(){
        return $this->hasOne('App\ProcurementUserPmr', 'procurement_user_id');
    }

    public function notifications(){
        return $this->hasMany('App\SystemNotifications', 'user_id');
    }

    public function announcements(){
        return $this->hasMany('App\Announcement', 'user_id');
    }

    public function uploaded_pmrs() {
        return $this->hasMany('App\UploadedPmr', 'procurement_user_id');
    }
}
