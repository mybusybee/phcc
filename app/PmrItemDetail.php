<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PmrItemDetail extends Model
{
    protected $guarded = ['id'];

    public function pmr_item(){
        return $this->belongsTo('App\PmrItem');
    }

    public function pr_item(){
        return $this->belongsTo('App\PurchaseRequestItem');
    }
}
