<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AoqProspectiveConsultant extends Model
{
    protected $table = 'aoq_prospective_consultants';
}
