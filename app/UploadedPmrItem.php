<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UploadedPmrItem extends Model
{
    public function end_user() {
        return $this->belongsTo('App\Office', 'end_user_id');
    }

    public function procurement_mode() {
        return $this->belongsTo('App\ProcurementMode', 'procurement_mode_id');
    }

    public function main_pmr() {
        return $this->belongsTo('App\UploadedPmr', 'uploaded_pmr_id');
    }
}
