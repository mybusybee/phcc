<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestForProposal extends Model
{
    public $table = 'rfps';

    public function pr(){
        return $this->belongsTo('App\PurchaseRequest', 'pr_id');
    }

    public function invite(){
        return $this->belongsTo('App\Invitation', 'invite_id');
    }
}
