<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseRequestAttachment extends Model
{
    protected $fillable = ['filepath', 'filename'];

    public function pr(){
        return $this->belongsTo('App\PurchaseRequest', 'pr_id');
    }
}
