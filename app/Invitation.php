<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invitation extends Model
{
    public function rfq(){
        return $this->hasOne('App\RequestForQuotation', 'invite_id');
    }

    public function rfp(){
        return $this->hasOne('App\RequestForProposal', 'invite_id');
    }

    public function rei(){
        return $this->hasOne('App\ReqExpInterest', 'invite_id');
    }

    public function itb(){
        return $this->hasOne('App\InvitationToBid', 'invite_id');
    }

    public function suppliers() {
        return $this->belongsToMany('App\Supplier', 'suppliers_invitations', 'invitation_id', 'supplier_id')->withPivot('email_status');
    }

    public function requirement(){
        return $this->hasOne('App\RequestRequirement', 'invite_id');
    }

    public function files(){
        return $this->hasMany('App\InvitationAttachment', 'invite_id');
    }

    public function aobs(){
        return $this->hasMany('App\AbstractOfBids', 'invite_id');
    }

    public function aoqs(){
        return $this->hasMany('App\AbstractOfQuotation', 'invite_id');
    }

    public function abstracts(){
        return $this->hasMany('App\AbstractParent', 'invite_id');
    }

    public function notifications() {
        return $this->hasMany('App\SystemNotification');
    }
}
