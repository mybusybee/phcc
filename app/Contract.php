<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    public function supplier() {
        return $this->belongsTo('App\Supplier', 'supplier_id');
    }

    public function order(){
        return $this->belongsTo('App\OrdersParent', 'orders_parent_id');
    }
}
