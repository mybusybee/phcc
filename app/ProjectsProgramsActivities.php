<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectsProgramsActivities extends Model
{
    protected $table = 'paps';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code', 
        'size', 
        'estimated_budget'
    ];

    public function ppmp(){
    	return $this->belongsTo('App\ProjectProcurementManagementPlan');
    }

    public function pap_schedules(){
    	return $this->hasMany('App\PapSchedule', 'pap_id');
    }

    public function procurement_modes(){
    	return $this->belongsToMany('App\ProcurementMode', 'paps_procurement_modes', 'pap_id', 'procurement_mode_id');
    }

    public function app(){
        return $this->hasOne('App\AnnualProcurementPlan', 'pap_id');
    }

    public function moving_app(){
        return $this->hasOne('App\MovingApp', 'pap_id');
    }

    public function uacs_object() {
        return $this->belongsTo('App\UacsObject', 'uacs_object_id');
    }

    public function category_code_field() {
        return $this->belongsTo('App\CategoryCode', 'category_code');
    }
}
