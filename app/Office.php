<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Office extends Model
{
    public function user(){
    	return $this->hasMany('App\User');
    }

    public function ppmp(){
    	return $this->hasMany('App\ProjectProcurementManagementPlan');
    }

    public function app(){
    	return $this->hasMany('App\AnnualProcurementPlan', 'pmo_end_user');
    }

    public function purchase_request(){
        return $this->hasMany('App\PurchaseRequest');
    }

    public function pmr(){
        return $this->hasMany('App\Pmr', 'office_id');
    }

    public function uploaded_pmr_items() {
        return $this->hasMany('App\UploadedPmrItem', 'end_user_id');
    }
}
