let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')

    //Vue Authentication Page
   .js('resources/assets/js/auth/main.js', 'public/js/auth')

   .js('resources/assets/js/supplier/create.js', 'public/js/supplier')

   //Dashboard
   .js('resources/assets/js/dashboard/index.js', 'public/js/dashboard')
   .js('resources/assets/js/dashboard/chart-data.js', 'public/js/dashboard')
   .js('resources/assets/js/dashboard/chart-data-2.js', 'public/js/dashboard')
   .js('resources/assets/js/dashboard/references.js', 'public/js/dashboard')

   //PPMP
   .js('resources/assets/js/ppmp/create.js', 'public/js/ppmp')
   .js('resources/assets/js/ppmp/edit.js', 'public/js/ppmp')
   .js('resources/assets/js/ppmp/allocation-computation.js', 'public/js/ppmp')
   .js('resources/assets/js/ppmp/form-validation.js', 'public/js/ppmp')
   .js('resources/assets/js/ppmp/currency-auto-format.js', 'public/js/ppmp')

   //APP
   .js('resources/assets/js/ppmp/annual-proc-plan.js', 'public/js/ppmp')
   .js('resources/assets/js/app/show.js', 'public/js/app')

    // PMR
   .js('resources/assets/js/pmr/create.js', 'public/js/pmr')
   .js('resources/assets/js/pmr/edit.js', 'public/js/pmr')
   .js('resources/assets/js/pmr/show.js', 'public/js/pmr')
   .js('resources/assets/js/pmr/main/show.js', 'public/js/pmr/main')
   .js('resources/assets/js/pmr/uploaded/show.js', 'public/js/pmr/uploaded')

   //Abstract
   .sass('resources/assets/sass/abstract/table.sass', 'public/css/abstract')
   .js('resources/assets/js/abstracts/index.js', 'public/js/abstracts')
   .js('resources/assets/js/abstracts/total-cost-calculator.js', 'public/js/abstracts')
   .js('resources/assets/js/abstracts/sub-total-calculator.js', 'public/js/abstracts')
   .js('resources/assets/js/abstracts/abstract-column-edit.js', 'public/js/abstracts')
   .js('resources/assets/js/abstracts/add-eligibility-docs.js', 'public/js/abstracts')
   .js('resources/assets/js/abstracts/filter-per-item-supplier-by-bid.js', 'public/js/abstracts')

   // AOB
   .js('resources/assets/js/aob/create.js', 'public/js/aob')
   .js('resources/assets/js/aob/edit.js', 'public/js/aob')
   .js('resources/assets/js/aob/add-reco-row.js', 'public/js/aob')
   .js('resources/assets/js/aob/add-legal-docs.js', 'public/js/aob')
   .js('resources/assets/js/aob/add-supplier-column.js', 'public/js/aob')
   .js('resources/assets/js/aob/initiators.js', 'public/js/aob')
   .js('resources/assets/js/aob/add-technical-docs.js', 'public/js/aob')
   .js('resources/assets/js/aob/add-financial-docs.js', 'public/js/aob')
   .js('resources/assets/js/aob/add-classb-docs.js', 'public/js/aob')
   .js('resources/assets/js/aob/add-other-docs.js', 'public/js/aob')
   .js('resources/assets/js/aob/remove-doc-row.js', 'public/js/aob')
   .js('resources/assets/js/aob/remove-reco-row.js', 'public/js/aob')

   // AOQ
   .js('resources/assets/js/aoq/show.js', 'public/js/aoq')
   .js('resources/assets/js/aoq/create.js', 'public/js/aoq')
   .js('resources/assets/js/aoq/edit.js', 'public/js/aoq')
   .js('resources/assets/js/aoq/aoq-create-validation.js', 'public/js/aoq')
   .js('resources/assets/js/aoq/aoq_qty_unit.js', 'public/js/aoq')

   // AOP
   .js('resources/assets/js/aop/create.js', 'public/js/aop')
   .js('resources/assets/js/aop/edit.js', 'public/js/aop')
   .js('resources/assets/js/aop/compute-technical-proposal.js', 'public/js/aop')

   // PO/JO
   .js('resources/assets/js/orders-parent/create.js', 'public/js/orders-parent')
   .js('resources/assets/js/orders-parent/form-number-generator.js', 'public/js/orders-parent')
   .js('resources/assets/js/orders-parent/show.js', 'public/js/orders-parent')

   // PR
   .js('resources/assets/js/pr/index.js', 'public/js/pr')
   .js('resources/assets/js/pr/pr_create.js', 'public/js/pr')
   .js('resources/assets/js/pr/pr_edit.js', 'public/js/pr')
   .js('resources/assets/js/pr/add-sub-item.js', 'public/js/pr')
   .js('resources/assets/js/pr/remove-sub-item.js', 'public/js/pr')
   .js('resources/assets/js/pr/form-validation.js', 'public/js/pr')
   .js('resources/assets/js/pr/edit.js', 'public/js/pr')
   .js('resources/assets/js/pr/add-new-file-input.js', 'public/js/pr')
   .js('resources/assets/js/pr/show.js', 'public/js/pr')
   .js('resources/assets/js/pr/item-numbering.js', 'public/js/pr')
   .sass('resources/assets/sass/pr/form.sass', 'public/css/pr')

   //Invitations
   .js('resources/assets/js/invitations/index.js', 'public/js/invitations')

   //ITB
   .js('resources/assets/js/itb/create.js', 'public/js/itb')
   .js('resources/assets/js/itb/show.js', 'public/js/itb')
   .js('resources/assets/js/itb/edit.js', 'public/js/itb')

   //REI
   .js('resources/assets/js/rei/create.js', 'public/js/rei')
   .js('resources/assets/js/rei/show.js', 'public/js/rei')

   // RFQ
   .js('resources/assets/js/rfq/create.js', 'public/js/rfq')
   .js('resources/assets/js/rfq/edit.js', 'public/js/rfq')
   .js('resources/assets/js/rfq/show.js', 'public/js/rfq')

   // RFP
   .js('resources/assets/js/rfp/create.js', 'public/js/rfp')
   .js('resources/assets/js/rfp/show.js', 'public/js/rfp')

   //Contract
   .js('resources/assets/js/contract/create.js', 'public/js/contract')

   .sass('resources/assets/sass/app.scss', 'public/css')
   .sass('resources/assets/sass/styles.sass', 'public/css')
   .sass('resources/assets/sass/ppmp.sass', 'public/css/ppmp')
   .sass('resources/assets/sass/ppmp_approved.sass', 'public/css/ppmp')
   .sass('resources/assets/sass/form-validation.sass', 'public/css')
   .sass('resources/assets/sass/pmrtable.sass', 'public/css')
   .sass('resources/assets/sass/footer.sass', 'public/css')
   .sass('resources/assets/sass/drawer.sass', 'public/css')

   .mix.disableNotifications();