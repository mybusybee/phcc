<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForYearAndDirectorColumnsToPpmpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ppmps', function (Blueprint $table) {
            $table->year('for_year')->after('total_estimated_budget');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ppmps', function (Blueprint $table) {
            $table->dropColumn('for_year');
        });
    }
}
