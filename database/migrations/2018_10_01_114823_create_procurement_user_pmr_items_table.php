<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcurementUserPmrItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('procurement_user_pmr_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('procurement_user_pmr_id')->unsigned();
            $table->foreign('procurement_user_pmr_id')->references('id')->on('procurement_user_pmrs')->onDelete('cascade');
            $table->integer('pr_id')->unsigned();
            $table->foreign('pr_id')->references('id')->on('purchase_requests')->onDelete('cascade');
            $table->string('pre_proc_conference')->nullable();
            $table->string('ads_post_of_ib')->nullable();
            $table->string('pre_bid_conf')->nullable();
            $table->string('eligibility_check')->nullable();
            $table->string('sub_open_of_bids')->nullable();
            $table->string('bid_evaluation')->nullable();
            $table->string('post_qual')->nullable();
            $table->string('noa')->nullable();
            $table->string('contract_signing')->nullable();
            $table->string('ntp')->nullable();
            $table->string('delivery_completion')->nullable();
            $table->string('inspection_acceptance')->nullable();
            $table->string('abc_mooe')->nullable();
            $table->string('abc_co')->nullable();
            $table->string('contract_cost_mooe')->nullable();
            $table->string('contract_cost_co')->nullable();
            $table->string('list_of_invited_observers')->nullable();
            $table->string('receipt_pre_bid_conf')->nullable();
            $table->string('receipt_eligibility_check')->nullable();
            $table->string('receipt_sub_open_of_bids')->nullable();
            $table->string('receipt_bid_evaluation')->nullable();
            $table->string('receipt_post_qual')->nullable();
            $table->string('receipt_delivery_completion')->nullable();
            $table->text('remarks')->nullable();
            $table->string('status')->nullable();
            $table->boolean('completed')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('procurement_user_pmr_items');
    }
}
