<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRequestRequirementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('request_requirements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('invite_id')->unsigned();
            $table->foreign('invite_id')->references('id')->on('invitations')->onDelete('cascade');
            $table->boolean('philgeps_reg_no')->default(0);
            $table->boolean('mayors_business_permit_bir_cor')->default(0);
            $table->boolean('income_business_tax_return')->default(0);
            $table->boolean('professional_license_cv')->default(0);
            $table->boolean('omnibus_sworn_statement')->default(0);
            $table->boolean('signed_terms_of_reference')->default(0);
            $table->boolean('govt_tax_inclusive')->default(0);
            $table->boolean('used_form')->default(0);
            $table->boolean('not_exceeding_abc')->default(0);
            $table->string('abc_amount')->nullable();
            $table->boolean('award_by_lot')->default(0);
            $table->boolean('award_by_line_item')->default(0);
            $table->boolean('one_month_validity')->default(0);
            $table->boolean('delivered_to_pcc')->default(0);
            $table->string('delivery_place');
            $table->boolean('payment_terms_cb')->default(0);
            $table->string('payment_terms')->nullable();
            $table->boolean('signatory_refusal')->default(0);
            $table->string('others')->nullable();
            $table->boolean('certificate_of_exclusivity')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('request_requirements');
    }
}
