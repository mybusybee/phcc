<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAbstractOfBidsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('abstract_of_bids', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('abstract_id')->unsigned();
            $table->foreign('abstract_id')->references('id')->on('abstracts')->onDelete('cascade');
            $table->integer('procurement_mode_id')->unsigned();
            $table->foreign('procurement_mode_id')->references('id')->on('procurement_modes')->onDelete('cascade');
            $table->string('bid_type');
            $table->string('pb_type');
            $table->string('pbac_chairperson');
            $table->string('pbac_vice_chairperson')->nullable();
            $table->string('pbac_member_1')->nullable();
            $table->string('pbac_member_2')->nullable();
            $table->string('provisional_member')->nullable();
            $table->string('coa_representative')->nullable();
            $table->string('private_observer_1')->nullable();
            $table->string('private_observer_2')->nullable();
            $table->string('twg_chairman')->nullable();
            $table->string('twg_member_1')->nullable();
            $table->string('twg_member_2')->nullable();
            $table->string('opening_date');
            $table->string('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('abstract_of_bids');
    }
}
