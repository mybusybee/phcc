<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUploadedPmrsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uploaded_pmrs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('procurement_user_id');
            $table->foreign('procurement_user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('year');
            $table->tinyInteger('semester');
            $table->string('prepared_by')->nullable();
            $table->string('recommended_approval')->nullable();
            $table->string('approved_by')->nullable();
            $table->string('total_alloted_budget')->nullable();
            $table->string('total_contract_price')->nullable();
            $table->string('total_savings')->nullable();
            $table->string('total_alloted_budget_ongoing')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uploaded_pmrs');
    }
}
