<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProcurementUserPmrsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('procurement_user_pmrs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('procurement_user_id')->unsigned();
            $table->foreign('procurement_user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('year');
            $table->tinyInteger('semester');
            $table->string('prepared_by')->nullable();
            $table->string('recommended_approval')->nullable();
            $table->string('approved_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('procurement_user_pmrs');
    }
}
