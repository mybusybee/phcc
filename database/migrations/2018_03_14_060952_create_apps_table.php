<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pap_id')->unsigned();
            $table->foreign('pap_id')->references('id')->on('paps')->onDelete('cascade');
            $table->string('code')->nullable();
            $table->string('program_project')->nullable();
            $table->integer('pmo_end_user')->unsigned();
            $table->foreign('pmo_end_user')->references('id')->on('offices');
            $table->integer('mode_of_procurement')->unsigned();
            $table->foreign('mode_of_procurement')->references('id')->on('procurement_modes');
            $table->string('source_of_funds');
            $table->string('total_estimated_budget');
            $table->string('mooe')->nullable();
            $table->string('co')->nullable();
            $table->text('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('apps');
    }
}
