<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseRequestSubItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_request_sub_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pr_item_id')->unsigned();
            $table->foreign('pr_item_id')->references('id')->on('purchase_request_items')->onDelete('cascade');
            $table->string('item_no')->nullable();
            $table->string('unit')->nullable();
            $table->text('description');
            $table->string('quantity');
            $table->string('est_cost_unit');
            $table->string('est_cost_total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_request_sub_items');
    }
}
