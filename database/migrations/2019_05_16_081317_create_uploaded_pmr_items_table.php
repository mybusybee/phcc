<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUploadedPmrItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('uploaded_pmr_items', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('uploaded_pmr_id');
            $table->foreign('uploaded_pmr_id')->references('id')->on('uploaded_pmrs')->onDelete('cascade');
            $table->string('pr_no');
            $table->string('code');
            $table->text('program_project');
            $table->unsignedInteger('end_user_id');
            $table->foreign('end_user_id')->references('id')->on('offices');
            $table->unsignedInteger('procurement_mode_id');
            $table->foreign('procurement_mode_id')->references('id')->on('procurement_modes');
            $table->string('pr_date_received');
            $table->string('source_of_funds');
            $table->text('list_of_invited_observers')->nullable();
            $table->string('abc_total')->nullable();
            $table->string('abc_mooe')->nullable();
            $table->string('abc_co')->nullable();
            $table->string('contract_cost_total')->nullable();
            $table->string('contract_cost_mooe')->nullable();
            $table->string('contract_cost_co')->nullable();
            //ACTUAL PROCUREMENT ACTIVITY
            $table->string('apa__pre_proc_conference')->nullable();
            $table->string('apa__ads_post_of_ib')->nullable();
            $table->string('apa__pre_bid_conf')->nullable();
            $table->string('apa__eligibility_check')->nullable();
            $table->string('apa__sub_open_of_bids')->nullable();
            $table->string('apa__bid_evaluation')->nullable();
            $table->string('apa__post_qual')->nullable();
            $table->string('apa__notice_of_award')->nullable();
            $table->string('apa__contract_signing')->nullable();
            $table->string('apa__notice_to_proceed')->nullable();
            $table->string('apa__delivery_completion')->nullable();
            $table->string('apa__inspection_and_acceptance')->nullable();
            //DATE OF RECEIPT OF INVITITAION
            $table->string('dri__pre_bid_conf')->nullable();
            $table->string('dri__eligibility_check')->nullable();
            $table->string('dri__sub_open_of_bids')->nullable();
            $table->string('dri__bid_evaluation')->nullable();
            $table->string('dri__post_qual')->nullable();
            $table->string('dri__delivery_completion_acceptance')->nullable();
            $table->text('remarks')->nullable();
            $table->string('status');
            $table->boolean('completed')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('uploaded_pmr_items');
    }
}
