<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('office_id')->unsigned();
            $table->foreign('office_id')->references('id')->on('offices');
            $table->integer('rc_code')->unsigned();
            $table->foreign('rc_code')->references('id')->on('user_roles');
            $table->string('pr_no');
            $table->string('pr_prep_date');
            $table->text('purpose')->nullable();
            $table->boolean('is_included_in_app')->default(0);
            $table->boolean('is_pm_dbm_available')->default(0);
            $table->integer('requested_by')->unsigned();
            $table->foreign('requested_by')->references('id')->on('users');
            $table->string('approved_by');
            $table->timestamp('date_assigned')->nullable();
            $table->timestamp('date_received')->nullable();
            $table->boolean('is_hardcopy_received')->default(0);
            $table->integer('proc_user_assigned')->nullable()->unsigned();
            $table->foreign('proc_user_assigned')->references('id')->on('users')->onDelete('cascade');
            // $table->integer('approved_by')->unsigned();
            // $table->foreign('approved_by')->references('id')->on('users');
            $table->text('revision_remarks')->nullable();
            $table->integer('revision_last_modifier')->nullable()->unsigned();
            $table->foreign('revision_last_modifier')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_requests');
    }
}
