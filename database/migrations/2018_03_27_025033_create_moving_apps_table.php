<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMovingAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moving_apps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pap_id')->unsigned();
            $table->foreign('pap_id')->references('id')->on('paps')->onDelete('cascade');
            //$table->string('actual_quantity');
            $table->string('actual_budget');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('moving_apps');
    }
}
