<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePapsProcurementModesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paps_procurement_modes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pap_id')->unsigned();
            $table->foreign('pap_id')->references('id')->on('paps')->onDelete('cascade');
            $table->integer('procurement_mode_id')->unsigned();
            $table->foreign('procurement_mode_id')->references('id')->on('procurement_modes')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paps_procurement_modes');
    }
}
