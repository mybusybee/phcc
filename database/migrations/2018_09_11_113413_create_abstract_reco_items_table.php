<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAbstractRecoItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('abstract_reco_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reco_id')->unsigned();
            $table->foreign('reco_id')->references('id')->on('abstract_recommendations')->onDelete('cascade');
            $table->integer('supplier_id')->unsigned();
            $table->foreign('supplier_id')->references('id')->on('suppliers')->onDelete('cascade');
            $table->string('pr_item_type')->nullable();
            $table->string('pr_item_ids');
            $table->string('subtotal');
            $table->string('remarks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('abstract_reco_items');
    }
}
