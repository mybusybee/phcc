<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePapsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ppmp_id')->unsigned();
            $table->foreign('ppmp_id')->references('id')->on('ppmps')->onDelete('cascade');
            $table->integer('uacs_object_id')->unsigned();
            $table->foreign('uacs_object_id')->references('id')->on('uacs_objects')->onDelete('cascade');
            $table->integer('category_code')->nullable()->unsigned();
            $table->foreign('category_code')->references('id')->on('category_codes')->onDelete('cascade');
            $table->string('allotment_type');
            $table->string('mooe')->nullable();
            $table->string('co')->nullable();
            $table->string('code')->nullable();
            $table->string('size')->nullable();
            $table->string('estimated_budget');
            $table->integer('budget_user_remarks')->unsigned()->nullable();
            $table->foreign('budget_user_remarks')->references('id')->on('users')->onDelete('cascade');
            $table->string('budget_remarks')->nullable();
            $table->integer('procurement_user_remarks')->unsigned()->nullable();
            $table->foreign('procurement_user_remarks')->references('id')->on('users')->onDelete('cascade');
            $table->string('procurement_remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paps');
    }
}
