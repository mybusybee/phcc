<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRemarksColumnToPpmpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('ppmps', function (Blueprint $table) {
            $table->text('remarks')->after('status')->nullable();
            $table->integer('remarks_last_modifier')->after('remarks')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('ppmps', function (Blueprint $table) {
            $table->dropColumn('remarks');
            $table->dropColumn('remarks_last_modifier');
        });
    }
}
