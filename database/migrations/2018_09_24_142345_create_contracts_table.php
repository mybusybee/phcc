<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('supplier_id')->unsigned();
            $table->foreign('supplier_id')->references('id')->on('suppliers')->onDelete('cascade');
            $table->integer('orders_parent_id')->unsigned();
            $table->foreign('orders_parent_id')->references('id')->on('orders_parents')->onDelete('cascade');
            $table->string('agreement_month');
            $table->string('agreement_day');
            $table->string('agreement_year');
            $table->string('brief_description');
            $table->string('contract_price');
            $table->string('procurement_entity_name');
            $table->string('pcc_signatory')->nullable();
            $table->string('pcc_signatory_designation')->nullable();
            $table->string('supplier_signatory')->nullable();
            $table->string('supplier_signatory_designation')->nullable();
            $table->string('witness_1')->nullable();
            $table->string('witness_1_designation')->nullable();
            $table->string('witness_2')->nullable();
            $table->string('witness_2_designation')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts');
    }
}
