<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAbstractOfQuotationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('abstract_of_quotations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('abstract_id')->unsigned();
            $table->foreign('abstract_id')->references('id')->on('abstracts')->onDelete('cascade');
            $table->integer('procurement_mode_id')->unsigned();
            $table->foreign('procurement_mode_id')->references('id')->on('procurement_modes')->onDelete('cascade');
            $table->string('canvassed_by');
            $table->string('canvassed_by_designation');
            $table->string('recommending_approval')->nullable();
            $table->string('recommending_designation')->nullable();
            $table->string('approved_by');
            $table->string('approved_by_designation');
            $table->string('opening_date');
            $table->string('remarks')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('abstract_of_quotations');
    }
}
