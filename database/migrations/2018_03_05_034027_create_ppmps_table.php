<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePpmpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ppmps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('end_user')->unsigned();
            $table->foreign('end_user')->references('id')->on('offices');
            $table->string('total_estimated_budget');
            // $table->integer('prepared_by')->unsigned();
            // $table->foreign('prepared_by')->references('id')->on('users');
            $table->string('prepared_by');
            $table->string('approved_by');
            $table->unsignedInteger('created_by');
            $table->foreign('created_by')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ppmps');
    }
}
