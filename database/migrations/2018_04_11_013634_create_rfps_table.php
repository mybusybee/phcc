<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRfpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rfps', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pr_id')->unsigned();
            $table->foreign('pr_id')->references('id')->on('purchase_requests')->onDelete('cascade');;
            $table->integer('invite_id')->unsigned();
            $table->foreign('invite_id')->references('id')->on('invitations');
            $table->string('rfp_prep_date');
            $table->string('submission_date_time');
            $table->string('submission_type');
            $table->string('rfp_receiver');
            $table->string('rfp_sender');
            $table->string('rfp_sender_designation');
            $table->string('not_awarded_notification')->nullable();
            $table->boolean('signed')->default(0);
            $table->text('notes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rfps');
    }
}
