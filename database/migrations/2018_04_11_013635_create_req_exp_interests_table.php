<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReqExpInterestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('req_exp_interests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pr_id')->unsigned();
            $table->foreign('pr_id')->references('id')->on('purchase_requests')->onDelete('cascade');
            $table->integer('invite_id')->unsigned();
            $table->foreign('invite_id')->references('id')->on('invitations');
            $table->string('project_name');
            $table->string('year');
            $table->string('abc');
            $table->text('brief_desc');
            $table->string('opening_date');
            $table->string('availability_date');
            $table->string('amount_in_peso');
            $table->integer('shortlist_allowed');
            $table->text('shortlist_criteria');
            $table->string('procedure');
            $table->string('completed_within');
            $table->string('chairperson_name');
            $table->boolean('signed')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('req_exp_interests');
    }
}
