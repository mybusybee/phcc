<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSystemNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('ppmp_id')->unsigned()->nullable();
            $table->foreign('ppmp_id')->references('id')->on('ppmps')->onDelete('cascade');
            $table->integer('pr_id')->unsigned()->nullable();
            $table->foreign('pr_id')->references('id')->on('purchase_requests')->onDelete('cascade');
            $table->integer('invitation_id')->unsigned()->nullable();
            $table->foreign('invitation_id')->references('id')->on('invitations')->onDelete('cascade');
            $table->integer('abstract_id')->unsigned()->nullable();
            $table->foreign('abstract_id')->references('id')->on('abstracts')->onDelete('cascade');
            $table->integer('order_id')->unsigned()->nullable();
            $table->foreign('order_id')->references('id')->on('orders_parents')->onDelete('cascade');
            $table->text('message');
            $table->string('link');
            $table->boolean('is_read')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('system_notifications');
    }
}
