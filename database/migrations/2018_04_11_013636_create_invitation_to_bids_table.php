<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvitationToBidsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invitation_to_bids', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pr_id')->unsigned();
            $table->foreign('pr_id')->references('id')->on('purchase_requests')->onDelete('cascade');
            $table->integer('invite_id')->unsigned();
            $table->foreign('invite_id')->references('id')->on('invitations');
            $table->text('name_of_project');
            $table->string('year');
            $table->string('abc');
            $table->text('description_of_goods');
            $table->string('delivery_date');
            $table->string('relevant_period');
            $table->string('date_of_availability');
            $table->string('bidding_documents_fee');
            $table->string('prebid_conf_date');
            $table->text('prebid_conf_address')->nullable();
            $table->string('bids_deadline');
            $table->string('bid_opening_datetime');
            $table->text('bid_opening_address');
            $table->longText('other_info');
            $table->string('chairperson_signatory');
            $table->boolean('signed')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invitation_to_bids');
    }
}
