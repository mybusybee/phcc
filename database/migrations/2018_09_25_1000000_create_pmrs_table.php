<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePmrsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pmrs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('year');
            $table->string('total_abc');
            $table->string('total_contract_amt');
            $table->string('total_savings');
            $table->string('total_ongoing_abc');
            $table->string('prepared_by');
            $table->string('recommended_approval');
            $table->string('approved_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pmrs');
    }
}
