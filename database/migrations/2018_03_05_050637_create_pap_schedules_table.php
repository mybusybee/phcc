<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePapSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pap_schedule', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pap_id')->unsigned();
            $table->foreign('pap_id')->references('id')->on('paps')->onDelete('cascade');
            $table->string('month');
            $table->string('allocation');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pap_schedule');
    }
}
