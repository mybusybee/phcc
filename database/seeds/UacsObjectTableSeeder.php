<?php

use Illuminate\Database\Seeder;
use App\UacsObject;

class UacsObjectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $objects = [
            'Traveling Expenses - Local',
            'Traveling Expenses - Foreign',
            'ICT Training Expenses',
            'Training Expenses',
            'ICT Office Supplies',
            'Office Supplies Expenses',
            'Accountable Forms Expenses',
            'Medical, Dental and Laboratory Supplies Expenses',
            'Fuel, Oil and Lubricants Expenses',
            'Other Supplies and Materials Expenses',
            'Water Expenses',
            'Electricity Expenses',
            'Postage and Courier Services',
            'Mobile',
            'Landline',
            'Internet Subscription Expenses',
            'Cable, Satellite, Telegraph and Radio Expenses',
            'Awards/Rewards Expenses',
            'Survey Expenses',
            'Research, Exploration and Development Expenses',
            'Extraordinary and Miscellaneous Expenses',
            'Legal Services',
            'Auditing Services',
            'ICT Consultancy Services',
            'Consultancy Services',
            'Other Professional Services',
            'Janitorial Services',
            'Security Services',
            'Other General Services - ICT Services',
            'Other General Services',
            'Repairs and Maintenance - Machinery and Equipment | Office Equipment',
            'Repairs and Maintenance - Machinery and Equipment | Information and Communication Technology Equipment',
            'Repairs and Maintenance - Transportation Equipment | Motor Vehicles',
            'Repairs and Maintenance -  Furniture and Fixtures',
            'Repairs and Maintenance - Leased Assets Improvements | Buildings',
            'Repairs and Maintenance - Other Property, Plant and Equipment | Other Property, Plant and Equipment',
            'Taxes, Duties and Licenses',
            'Fidelity Bond Premiums',
            'Insurance Expenses',
            'Advertising Expenses',
            'Printing and Publication Expenses',
            'Representation Expenses',
            'Transportation and Delivery Expenses',
            'Rents - Building and Structures',
            'Rents - Motor Vehicles',
            'Rents - Equipment',
            'Membership Dues and Contributions to Organizations',
            'ICT Software Subscription',
            'Library and Other Reading Materials Subscription Expenses',
            'Other Subscription Expenses',
            'Litigation/Acquired Assets Expenses',
            'Other Maintenance and Operating Expenses | Website Maintenance',
            'Other Maintenance and Operating Expenses | Other Maintenance and Operating Expenses',
            'Machinery and Equipment Outlay | Office Equipment',
            'Machinery and Equipment Outlay | Information and Communication Technology Equipment',
            'Machinery and Equipment Outlay | ICT Software',
            'Transportation Equipment Outlay | Motor Vehicles',
            'Furniture, Fixtures and Books Outlay | Furniture and Fixtures',
            'Furniture, Fixtures and Books Outlay | Books'
        ];
        $count = 1;
        foreach($objects as $object) {
            if($count <= 53) {
                UacsObject::create([
                    'uacs_object_name' => $object,
                    'type' => 'mooe'
                ]);
            } else {
                UacsObject::create([
                    'uacs_object_name' => $object,
                    'type' => 'co'
                ]);
            }
            
            $count++;
        }
    }
}
