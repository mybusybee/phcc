<?php

use Illuminate\Database\Seeder;

class UnitsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pr_units')->insert([
            'unit' => 'Ampule'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Bag'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Bar'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Board Feet'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Book'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Bottle'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Box'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Bundle'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Can'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Capsule'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Carton'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Cartridge'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Copy'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Crate'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Cubic Meter'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Cycle'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Dose'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Dozen'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Each'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Feet'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Gallon'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Gram'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Hectare'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Inch'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Jar'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Kilo'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Kilometer'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'License'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Liter'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Lot'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Lump Sum'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Meter'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Mile'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Nebule'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Pack'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Package'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Packet'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Pad'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Pack'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Package'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Packet'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Pad'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Pail'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Pair'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Piece'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Quart'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Quire'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Ream'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Roll'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Sack'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Set'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Sheet'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Spool'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Square Meter'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Tablet'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Tube'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Unit'
        ]);
        DB::table('pr_units')->insert([
            'unit' => 'Vial'
        ]);
    }
}
