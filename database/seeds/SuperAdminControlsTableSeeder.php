<?php

use Illuminate\Database\Seeder;

class SuperAdminControlsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('super_admin_controls')->insert([
            'control' 	=> 'PPMP',
            'is_active' => true
        ]);
    }
}
