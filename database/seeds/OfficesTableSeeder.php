<?php

use Illuminate\Database\Seeder;

class OfficesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('offices')->insert([
    		'office' => 'Asgard',
    	]);
    	DB::table('offices')->insert([
    		'office' => 'Office of the Chairperson',
    	]);
    	DB::table('offices')->insert([
    		'office' => 'Administrative Office',
    	]);
    	DB::table('offices')->insert([
    		'office' => 'Finance, Planning and Management Office',
    	]);
    	DB::table('offices')->insert([
    		'office' => 'Mergers and Acquisition Office',
    	]);
    	DB::table('offices')->insert([
    		'office' => 'Competition Enforcement Office',
    	]);
    	DB::table('offices')->insert([
    		'office' => 'Economics Office',
    	]);
    	DB::table('offices')->insert([
    		'office' => 'Legal and Adjudication Office',
    	]);
        DB::table('offices')->insert([
            'office' => 'Office of the Executive Director',
        ]);
        DB::table('offices')->insert([
            'office' => 'Office of the Commissioners',
        ]);
        DB::table('offices')->insert([
            'office' => 'Unassigned',
        ]);
    }
}
