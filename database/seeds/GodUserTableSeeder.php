<?php

use Illuminate\Database\Seeder;

class GodUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	DB::table('users')->insert([
    		'first_name' => 'Thor',
    		'last_name' => 'Odinson',
    		'email' => 'admin@email.com',
    		'password' => bcrypt('password'),
    		'user_role' => 1,
            'office_id' => 1,
    	]);
    }
}
