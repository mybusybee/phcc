<?php

use Illuminate\Database\Seeder;
use App\CategoryCode;

class CategoryCodeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CategoryCode::create([
    		'category_code'	=> 'Professional Services'
        ]);
        
        CategoryCode::create([
    		'category_code'	=> 'Lease of Property/Venue'
        ]);
        
        CategoryCode::create([
    		'category_code'	=> 'Food Provision/Catering Services'
        ]);
        
        CategoryCode::create([
    		'category_code'	=> 'Corporate Giveaways/Tokens/Awards'
        ]);
        
        CategoryCode::create([
    		'category_code'	=> 'Office Supplies, Materials and Equipment (Not Available at PS-DBM)'
        ]);
        
        CategoryCode::create([
    		'category_code'	=> 'IT Supplies, Materials and Equipment (Not Available at PS-DBM)'
        ]);
        
        CategoryCode::create([
    		'category_code'	=> 'Collaterals Printing'
        ]);
        
        CategoryCode::create([
    		'category_code'	=> 'Publication/Advertising Services (Print/Online/Media)'
        ]);
        
        CategoryCode::create([
    		'category_code'	=> 'Subscription Services'
        ]);
        
        CategoryCode::create([
    		'category_code'	=> 'Repair and Maintenance Services'
        ]);
        
        CategoryCode::create([
    		'category_code'	=> 'References and Reading Materials (Hardcopy/Online/E-copy)'
        ]);
        
        CategoryCode::create([
    		'category_code'	=> 'Other General Administrative Services'
    	]);
    }
}
