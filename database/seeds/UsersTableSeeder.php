<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'first_name' => 'Unassigned',
            'last_name' => 'User',
            'email' => 'unassigned@email.com',
            'password' => bcrypt('password'),
            'user_role' => 2,
            'office_id' => 11,
        ]);

        DB::table('users')->insert([
            'first_name' => 'Admin',
            'last_name' => 'User',
            'email' => 'adminuser@email.com',
            'password' => bcrypt('password'),
            'user_role' => 3,
            'office_id' => 2,
        ]);

        DB::table('users')->insert([
            'first_name' => 'Head',
            'last_name' => 'Director 1',
            'email' => 'director1@email.com',
            'password' => bcrypt('password'),
            'user_role' => 4,
            'office_id' => 3,
        ]);
        
        DB::table('users')->insert([
            'first_name' => 'Head',
            'last_name' => 'Director 2',
            'email' => 'director2@email.com',
            'password' => bcrypt('password'),
            'user_role' => 4,
            'office_id' => 4,
        ]);

        DB::table('users')->insert([
            'first_name' => 'Head',
            'last_name' => 'Director 3',
            'email' => 'director3@email.com',
            'password' => bcrypt('password'),
            'user_role' => 4,
            'office_id' => 5,
        ]);

        DB::table('users')->insert([
            'first_name' => 'Procurement User',
            'last_name' => 'User',
            'email' => 'procurement@email.com',
            'password' => bcrypt('password'),
            'user_role' => 5,
            'office_id' => 6,
        ]);

        DB::table('users')->insert([
            'first_name' => 'Budget',
            'last_name' => 'User',
            'email' => 'budget@email.com',
            'password' => bcrypt('password'),
            'user_role' => 6,
            'office_id' => 7,
        ]);

        DB::table('users')->insert([
            'first_name' => 'End',
            'last_name' => 'User',
            'email' => 'enduser@email.com',
            'password' => bcrypt('password'),
            'user_role' => 7,
            'office_id' => 3,
        ]);

        DB::table('users')->insert([
            'first_name' => 'Secretariat',
            'last_name' => '1',
            'email' => 'sec@email.com',
            'password' => bcrypt('password'),
            'user_role' => 8,
            'office_id' => 8
        ]);

        factory(App\User::class, 20)->create();
    }
}
