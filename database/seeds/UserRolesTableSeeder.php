<?php

use Illuminate\Database\Seeder;

class UserRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_roles')->insert([
            'user_role' => 'God',
        ]);
        DB::table('user_roles')->insert([
            'user_role' => 'Unassigned',
        ]);
        DB::table('user_roles')->insert([
            'user_role' => 'Admin',
        ]);
        DB::table('user_roles')->insert([
            'user_role' => 'Director',
        ]);
        DB::table('user_roles')->insert([
            'user_role' => 'Procurement',
        ]);
        DB::table('user_roles')->insert([
            'user_role' => 'Budget',
        ]);
        DB::table('user_roles')->insert([
            'user_role' => 'End User',
        ]);
        DB::table('user_roles')->insert([
            'user_role' => 'BAC Secretariat',
        ]);
    }
}
