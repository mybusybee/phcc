<?php

use Illuminate\Database\Seeder;
use App\ProcurementMode;

class ProcurementModesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	ProcurementMode::create([
    		'mode' 				=> 'Competitive Bidding - Infrastructure',
    		'turnaround_time'	=> '26 calendar days to 156 calendar days'
    	]);

    	ProcurementMode::create([
    		'mode' 				=> 'Competitive Bidding - Goods/Services',
    		'turnaround_time'	=> '26 calendar days to 136 calendar days'
    	]);

    	ProcurementMode::create([
    		'mode' 				=> 'Competitive Bidding - Consulting Services',
    		'turnaround_time'	=> '36 calendar days to 180 calendar days'
    	]);

    	ProcurementMode::create([
    		'mode' 				=> 'Limited Source Bidding',
    		'turnaround_time'	=> 'TBD'
    	]);

    	ProcurementMode::create([
    		'mode' 				=> 'Direct Contracting - Local',
    		'turnaround_time'	=> 'At least 5 working days'
    	]);

    	ProcurementMode::create([
    		'mode' 				=> 'Direct Contracting - Foreign',
    		'turnaround_time'	=> 'At least 20 working days'
    	]);

    	ProcurementMode::create([
    		'mode' 				=> 'Repeat Order',
    		'turnaround_time'	=> 'TBD'
    	]);

    	ProcurementMode::create([
    		'mode' 				=> 'Shopping - Unforeseen Contingency',
    		'turnaround_time'	=> 'At least 5 working days'
    	]);

        ProcurementMode::create([
            'mode'              => 'Shopping - Above 50k to 1m',
            'turnaround_time'   => 'At least 9 working days',
            'min_value'         => 50001,
            'max_value'         => 1000000,
        ]);

        ProcurementMode::create([
            'mode'              => 'Shopping - 50k and below',
            'turnaround_time'   => 'At least 7 working days',
            'min_value'         => 0,
            'max_value'         => 50000,
        ]);

    	ProcurementMode::create([
    		'mode' 				=> 'NP - Two Failed Biddings',
    		'turnaround_time'	=> 'At least 10 working days'
    	]);

    	ProcurementMode::create([
    		'mode' 				=> 'NP - Emergency Cases',
    		'turnaround_time'	=> 'At least 5 working days'
    	]);

    	ProcurementMode::create([
    		'mode' 				=> 'NP - Take-Over of Contracts',
    		'turnaround_time'	=> 'TBD'
    	]);

    	ProcurementMode::create([
    		'mode' 				=> 'NP - Adjacent or Continguous',
    		'turnaround_time'	=> 'TBD'
    	]);

    	ProcurementMode::create([
    		'mode' 				=> 'NP - Agency-to-Agency',
    		'turnaround_time'	=> 'At least 15 working days'
    	]);

    	ProcurementMode::create([
    		'mode' 				=> 'NP - Scientific, Scholarly or Artistic Work, Exclusive Technology and Media Services',
    		'turnaround_time'	=> 'At least 14 working days'
    	]);

    	ProcurementMode::create([
    		'mode' 				=> 'NP - Highly Technical Consultants',
    		'turnaround_time'	=> 'At least 14 working days'
    	]);

    	ProcurementMode::create([
    		'mode' 				=> 'NP - Defense Cooperation Agreement',
    		'turnaround_time'	=> 'TBD'
    	]);

    	ProcurementMode::create([
    		'mode' 				=> 'NP - Small Value Procurement - Above 50k to 1m',
    		'turnaround_time'	=> 'At least 11 working days',
            'min_value'         => 50001,
            'max_value'         => 1000000,
    	]);

    	ProcurementMode::create([
    		'mode' 				=> 'NP - Small Value Procurement - 50k and below',
    		'turnaround_time'	=> 'At least 8 working days',
            'min_value'         => 0,
            'max_value'         => 50000,
    	]);

    	ProcurementMode::create([
    		'mode' 				=> 'NP - Lease of Venue',
    		'turnaround_time'	=> 'At least 7 working days'
    	]);

    	ProcurementMode::create([
    		'mode' 				=> 'NP - Lease of Property',
    		'turnaround_time'	=> 'At least 24 working days'
    	]);

    	ProcurementMode::create([
    		'mode' 				=> 'NP - NGO Participation',
    		'turnaround_time'	=> 'TBD'
    	]);

    	ProcurementMode::create([
    		'mode' 				=> 'NP - Community Participation',
    		'turnaround_time'	=> 'TBD'
    	]);

    	ProcurementMode::create([
    		'mode' 				=> 'NP - United Nations Agencies, International Organizations or International Financing Institutions',
    		'turnaround_time'	=> 'TBD'
		]);
		
		ProcurementMode::create([
			'mode' 				=> 'Competitive Bidding - Ordering Agreement',
			'turnaround_time'   => 'TBD'
		]);
    }
}
