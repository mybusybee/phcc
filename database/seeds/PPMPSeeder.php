<?php

use Illuminate\Database\Seeder;
use App\PapSchedule;
use Faker\Generator as Faker;

class PPMPSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\ProjectProcurementManagementPlan::class, 5)->create()->each(function ($a) {
            $a->paps()
                ->saveMany( factory(App\ProjectsProgramsActivities::class, 5)->make() )
                ->each(function ($b) {
                    $months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
                    
                    foreach($months as $month){
                        if($month === 'Jul'){
                            $schedule[] = new PapSchedule([
                                'month' => $month,
                                'allocation' => "10,000"
                            ]);
                        } else {
                            $schedule[] = new PapSchedule([
                                'month' => $month,
                                'allocation' => "0.00"
                            ]);
                        }
                    }
                    $b->pap_schedules()->saveMany($schedule);

                    $ids = 1;
                    $b->procurement_modes()->sync($ids);
                });
        });
    }
}
