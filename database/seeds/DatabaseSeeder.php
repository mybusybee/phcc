<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$this->call([
            UserRolesTableSeeder::class,
            UacsObjectTableSeeder::class,
            OfficesTableSeeder::class,
            GodUserTableSeeder::class,
            UsersTableSeeder::class,
            ProcurementModesTableSeeder::class,
            SuppliersTableSeeder::class,
            UnitsTableSeeder::class,
            CategoryCodeTableSeeder::class,
            //PPMPSeeder::class,
            SuperAdminControlsTableSeeder::class,
    	]);
    }
}
