<?php

use Faker\Generator as Faker;

$factory->define(App\ProjectProcurementManagementPlan::class, function (Faker $faker) {
    return [
        'end_user' => 3,
        'total_estimated_budget' => "50,000",
        'for_year' => date('Y'),
        'status' => 'PPMP_APPROVED',
        'prepared_by' => 'Juan Dela Cruz',
        'approved_by' => 'Juan Dela Cruz',
        'created_by' => 9
    ];
});
