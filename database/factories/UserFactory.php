<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$Ho26g9YMzirOm24hVFIMn.5SGjjQghiMU.c2bDmV.IP6Vjb7kY.BW', // secret
        'user_role' => $faker->numberBetween($min = 3, $max = 8),
        'office_id' => $faker->numberBetween($min = 2, $max = 10),
    ];
});
