<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Supplier::class, function (Faker $faker) {
    return [
        'authorized_personnel' => $faker->name,
        'company_name' => $faker->company,
        'category' => $faker->text($maxNbChars = 50),
        'address' => $faker->address,
        'telephone' => $faker->phoneNumber,
        'email' => $faker->unique()->safeEmail,
        'tin_number' => $faker->bankAccountNumber,
        'mayors_permit' => $faker->date($format = 'm/d/Y'),
        'philgeps_number' => $faker->randomNumber($nbDigits = 5, $strict = false),
    ];
});
