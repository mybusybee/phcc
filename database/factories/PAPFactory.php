<?php

use Faker\Generator as Faker;

$factory->define(App\ProjectsProgramsActivities::class, function (Faker $faker) {
    return [
        'allotment_type' => 'MOOE',
        'mooe' => "10,000",
        'co' => "0.00",
        'uacs_object_id' => "1",
        'category_code' => $faker->numberBetween($min = 1, $max = 12),
        'code' => $faker->regexify('[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}'),
        'general_description' => $faker->sentence($nbWords = 4, $variableNbWords = true),
        'size' => $faker->randomDigitNotNull,
        'estimated_budget' => "10,000"
    ];
});
