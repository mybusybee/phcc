/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 130);
/******/ })
/************************************************************************/
/******/ ({

/***/ 130:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(131);


/***/ }),

/***/ 131:
/***/ (function(module, exports) {



var validated = function validated(row) {
    var code = $('tr#' + row).find('.pap_code').val();
    var desc = $('tr#' + row).find('.pap_gen_desc').val();
    var qty = $('tr#' + row).find('.pap_qty').val();
    var budget = $('tr#' + row).find('.pap_est_budget').val();
    var mode = $('tr#' + row).find('.procurement_modes_select').val();
    var allocation = false;
    $('tr#' + row).find('.allocation-month').each(function () {
        var allocationValue = $(this).val();
        if (allocationValue != '') {
            allocation = true;
        } else {
            allocation = false;
            return false;
        }
    });

    if (desc != '' && budget != '' && mode != null && allocation == true) {
        return true;
    } else {
        return false;
    }
};

// $(document).on('keypress, blur', '.pap_code, .pap_gen_desc, .pap_qty, .pap_est_budget, .allocation-month', function() {
//     let row = $(this).closest('tr').attr('id')
//     if(validated(row)){
//         $('.add-pap').attr('disabled', false)
//         // $('#submit-btn').attr('disabled', false)
//         console.log('validated')
//     } else {
//         $('.add-pap').attr('disabled', true)
//         // $('#submit-btn').attr('disabled', true)
//         console.log('not validated')
//     }
// })

$(document).on('change', '.procurement_modes_select', function () {
    var row = $(this).closest('tr').attr('id');
    validated(row);
});

$(document).on('keyup', '.pap_est_budget', function () {
    var row = $(this).closest('tr').attr('id');
    var budget = $('tr#' + row).find('.pap_est_budget').val();
    var budget_float = budget;
    if (budget != "0.00") {
        budget = budget.replace(/\,/g, '');
        budget_float = parseFloat(budget);
    }
    var budget_formatted = budget_float.toLocaleString();
    if (budget.charAt(budget.length - 1) == ".") {
        budget_formatted += ".";
    }

    if (isNaN(budget_float)) {
        $('tr#' + row).find('.pap_est_budget').val("0.00");
    } else {
        $('tr#' + row).find('.pap_est_budget').val(budget_formatted);
    }
});

// $(document).on('keyup', '.allocation-month', function() {
//     let row = $(this).closest('tr').attr('id')
//     $(`tr#${row}`).find('.allocation-month').each(function(){
//         let allocationValue = $(this).val()
//         let allocationValue_float = allocationValue
//         if (allocationValue != "0.00"){
//             allocationValue=allocationValue.replace(/\,/g,'')
//             allocationValue_float = parseFloat(allocationValue)
//         }
//         let allocationValue_formatted = allocationValue_float.toLocaleString()
//         if (allocationValue.charAt(allocationValue.length - 1) == "."){
//             allocationValue_formatted += "."
//         }

//         if (isNaN(allocationValue_float)){
//             $(this).val("0.00")
//         }else{
//             $(this).val(allocationValue_formatted)
//         }

//     })
// })

//procurement mode price validation on change of allocation price per month
$(document).on('change', '.allocation-month', function () {
    var totalAllocation = parseFloat(removeCommas($(this).closest('tr').find('input.pap_est_budget').val()));
    var procModeMax = $(this).closest('tr').find('select.procurement_modes_select').find(':selected').data('max-value');
    var procModeMin = $(this).closest('tr').find('select.procurement_modes_select').find(':selected').data('min-value');

    if (totalAllocation > procModeMax && procModeMax !== 0) {
        alert('Total allocation exceeded the maximum amount!');
        $(this).val('0.00');
        var row_id = $(this).closest('tr').attr('id');
        recalculateTotalAllocation(row_id);
    } else if (totalAllocation < procModeMin && procModeMin !== 0) {
        alert('Total allocation is below the minimum amount!');
        $(this).val('0.00');
        var _row_id = $(this).closest('tr').attr('id');
        recalculateTotalAllocation(_row_id);
    }
});

$(document).on('change', 'select.procurement_modes_select', function () {
    var procModeMax = $(this).find(':selected').data('max-value');
    var procModeMin = $(this).find(':selected').data('min-value');
    var totalAllocation = $(this).closest('tr').find('.pap_est_budget').val();

    if (parseFloat(removeCommas(totalAllocation)) > procModeMax && procModeMax !== 0) {
        alert('Selected mode of procurement\'s maximum amount exceeds the current total budget!');
        $(this).val("").trigger('change');
    } else if (parseFloat(removeCommas(totalAllocation)) < procModeMin && procModeMin !== 0 && parseFloat(removeCommas(totalAllocation)) !== 0) {
        alert('Selected mode of procurement\'s minimum amount is lower than the current total budget!');
        $(this).val("").trigger('change');
    }
});

var recalculateTotalAllocation = function recalculateTotalAllocation(row_id) {
    var totalAllocation = 0;
    $('tr#' + row_id).find('.allocation-month').each(function () {
        var allocation = $(this).val();
        totalAllocation += parseFloat(removeCommas(allocation));
    });
    $('tr#' + row_id).find('.pap_est_budget').val(currencyConvert(totalAllocation));
};

var removeCommas = function removeCommas(num) {
    return num.replace(/\,/g, '');
};

var currencyConvert = function currencyConvert(num) {
    return num.toLocaleString(undefined, {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
    });
};

/***/ })

/******/ });