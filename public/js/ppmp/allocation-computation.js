/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 128);
/******/ })
/************************************************************************/
/******/ ({

/***/ 128:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(129);


/***/ }),

/***/ 129:
/***/ (function(module, exports) {

$(document).on('keyup', '.allocation-month', function () {

    //Prompt alert if user entered value in allocation first before total estimated budget
    if ($(this).closest('tr').find('.pap_est_budget').val() == '') {
        //alert('Estimated budget is empty!')
        $(this).val('0.00');
    }

    var row_counter = $(this).closest('tr').find('.row_counter').val();
    var total_allocation = 0;
    var total_estimated_budget = $(this).closest('tr').find('.pap_est_budget').val();
    total_estimated_budget = total_estimated_budget.replace(/\,/g, '');

    $('.alloc-month-' + row_counter).each(function () {
        var allocationValue = $(this).val();
        allocationValue = allocationValue.replace(/\,/g, '');
        total_allocation += parseFloat(allocationValue);
    });

    // if(total_allocation != parseFloat(total_estimated_budget)){
    //     $(this).closest('table.table').find('.add-pap').attr('disabled', true)
    //     $(this).closest('tr').addClass('est-budget-error')

    // } else {
    //     $(this).closest('table.table').find('.add-pap').attr('disabled', false)
    //     $(this).closest('tr').removeClass('est-budget-error')
    // }
    var row = $(this).closest('tr').attr('id');
    computeTotalAllocation(row);
});

$(document).on('keyup', '.pap_est_budget', function () {
    //Prompt alert if user entered value in allocation first before total estimated budget
    if ($(this).closest('tr').find('.pap_est_budget').val() == '') {
        //alert('Estimated budget is empty!')
        $(this).val('0.00');
    }

    var row_counter = $(this).closest('tr').find('.row_counter').val();
    var total_allocation = 0;
    var total_estimated_budget = $(this).closest('tr').find('.pap_est_budget').val();
    total_estimated_budget = total_estimated_budget.replace(/\,/g, '');

    $('.alloc-month-' + row_counter).each(function () {
        var allocationValue = $(this).val();
        allocationValue = allocationValue.replace(/\,/g, '');
        total_allocation += parseFloat(allocationValue);
    });

    // if(total_allocation != parseFloat(total_estimated_budget)){
    //     $(this).closest('table.table').find('.add-pap').attr('disabled', true)
    //     $(this).closest('tr').addClass('est-budget-error')

    // } else {
    //     $(this).closest('table.table').find('.add-pap').attr('disabled', false)
    //     $(this).closest('tr').removeClass('est-budget-error')

    // }
});

var computeTotalAllocation = function computeTotalAllocation(row) {
    var totalAllocation = 0;
    $('tr#' + row).find('.allocation-month').each(function () {
        var allocation = $(this).val();
        totalAllocation += parseFloat(removeCommas(allocation));
    });
    var formattedValue = currencyConvert(totalAllocation);
    $('tr#' + row).find('.pap_est_budget').val(formattedValue);
};

$(document).on('blur', '.allocation-month', function () {
    var val = $(this).val();
    if (val === '') {
        $(this).val('0.00');
    }
});

var removeCommas = function removeCommas(num) {
    return num.replace(/\,/g, '');
};

var currencyConvert = function currencyConvert(num) {
    return num.toLocaleString(undefined, {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2
    });
};

/***/ })

/******/ });