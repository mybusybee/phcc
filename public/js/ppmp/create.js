/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 124);
/******/ })
/************************************************************************/
/******/ ({

/***/ 124:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(125);


/***/ }),

/***/ 125:
/***/ (function(module, exports) {

/*** Global Declarations Start ***/
var procurement_ids = [];
var max_value = 0;
var min_value = 0;
/*** Global Declarations End ***/

/**** Functions START ****/
var compute_budgets = function compute_budgets() {
	var total_budget = 0;
	$('.pap_est_budget').each(function () {
		var budgetValue = $(this).val();
		budgetValue = budgetValue.replace(/\,/g, '');
		total_budget += parseFloat(budgetValue);
	});
	if (isNaN(total_budget)) {
		total_budget = '0.00';
	} else {
		total_budget = total_budget.toLocaleString(undefined, {
			minFractionNumber: 2,
			maxFractionNumber: 2
		});
	}

	$('#total_estimated_budget').val(total_budget);
};

/**** Functions END ****/

$(function () {
	$('[data-toggle="tooltip"]').tooltip();
});

$(document).ready(function () {
	$('.datepicker_default').datepicker({
		changeYear: true,
		minDate: 0,
		yearRange: new Date().getFullYear() + ':+5',
		dateFormat: 'M-dd'
	});

	$('.procurement_modes_select').select2({
		placeholder: 'Please select one.',
		width: 'resolve'
	});

	for (i = new Date().getFullYear() + 10; i >= new Date().getFullYear(); i--) {
		$('#ppmp_year').append($('<option />').val(i).html(i));
	}

	$('#ppmp_year > option:first').attr('disabled', true);
	$('#ppmp_year').find('option[value="' + new Date().getFullYear() + '"]').attr('selected', true);

	//autoselect ppmp year from hidden field
	var ppmp_year = $('#ppmp_year_hidden').val();
	$('#ppmp_year_container').find('#ppmp_year option[value="' + ppmp_year + '"]').attr('selected', true);

	var is_budget_session = $('#hidden_session').val();
	if (is_budget_session == 6) {
		$('.multiselect.dropdown-toggle').attr('disabled', true);
	}
});

//add new pap row
var row_counter = $('.pap_est_budget').length;
$(document).on('click', '.add-pap', function () {
	max_value = 0;
	min_value = 0;
	// $(this).attr('disabled', true)
	$('#submit-btn').attr('disabled', false);
	var selected_modes = [];
	row_counter += 1;
	var procurement_modes = $('select.hidden-procurement-modes option').clone();
	var uacs_objects = $('select[name="pap_uacs_object"] option').clone();

	var pap_row = $('\n\t\t<tr id="' + row_counter + '" class="pap-row">\n\t\t\t<td style="display: none;">\n\t\t\t\t<input type="hidden" name="row_counter[]" value="' + row_counter + '" class="row_counter" />\n\t\t\t</td>\n\t\t\t<td>\n\t\t\t\t<input class="pap_code form-control" name="pap_code' + row_counter + '" type="text" required>\n\t\t\t</td>\n\t\t\t<td>\n\t\t\t\t<textarea class="gen-desc form-control pap_gen_desc" rows="2" name="pap_gen_desc' + row_counter + '" required></textarea>\n\t\t\t</td>\n\t\t\t\n\t\t\t<td>\n\t\t\t\t<input class="form-control pap_qty" name="pap_qty' + row_counter + '" type="text" required>\n\t\t\t</td>\n\t\t\t<td>\n\t\t\t\t<select name="pap_uacs_object' + row_counter + '" id="" class="pap-uacs-object form-control"></select>\n\t\t\t</td>\n\t\t\t<td>\n\t\t\t\t<input class="form-control" name="allotment_type' + row_counter + '" readonly required type="text" />\n\t\t\t</td>\n\t\t\t<td>\n\t\t\t\t<div class="input-group pap_est_budget_container">\n\t\t\t\t\t<div class="input-group-prepend">\n\t\t\t\t\t\t<span class="input-group-text">&#8369;</span>\n\t\t\t\t\t</div>\n\t\t\t\t\t<input readonly class="form-control pap_est_budget" name="pap_est_budget' + row_counter + '" type="text" value="0.00" required>\n\t\t\t\t</div>\n\t\t\t</td>\n\t\t\t<td>\n\t\t\t\t<select name="pap_mode_of_procurement' + row_counter + '" class="form-control procurement_modes_select" required style="width: 100%"></select>\n\t\t\t</td>\n\t\t\t<td>\n\t\t\t\t<div class="input-group pap_est_budget_container">\n\t\t\t\t\t<div class="input-group-prepend">\n\t\t\t\t\t\t<span class="input-group-text">&#8369;</span>\n\t\t\t\t\t</div>\n\t\t\t\t\t<input type="text" class="form-control allocation-month alloc-month-' + row_counter + '" name="allocation' + row_counter + '[]" value="0.00" required>\n\t\t\t\t</div>\n\t\t\t</td>\n\t\t\t<td>\n\t\t\t\t<div class="input-group pap_est_budget_container">\n\t\t\t\t\t<div class="input-group-prepend">\n\t\t\t\t\t\t<span class="input-group-text">&#8369;</span>\n\t\t\t\t\t</div>\n\t\t\t\t\t<input type="text" class="form-control allocation-month alloc-month-' + row_counter + '" name="allocation' + row_counter + '[]" value="0.00" required>\n\t\t\t\t</div>\n\t\t\t</td>\n\t\t\t<td>\n\t\t\t\t<div class="input-group pap_est_budget_container">\n\t\t\t\t\t<div class="input-group-prepend">\n\t\t\t\t\t\t<span class="input-group-text">&#8369;</span>\n\t\t\t\t\t</div>\n\t\t\t\t\t<input type="text" class="form-control allocation-month alloc-month-' + row_counter + '" name="allocation' + row_counter + '[]" value="0.00" required>\n\t\t\t\t</div>\n\t\t\t</td>\n\t\t\t<td>\n\t\t\t\t<div class="input-group pap_est_budget_container">\n\t\t\t\t\t<div class="input-group-prepend">\n\t\t\t\t\t\t<span class="input-group-text">&#8369;</span>\n\t\t\t\t\t</div>\n\t\t\t\t\t<input type="text" class="form-control allocation-month alloc-month-' + row_counter + '" name="allocation' + row_counter + '[]" value="0.00" required>\n\t\t\t\t</div>\n\t\t\t</td>\n\t\t\t<td>\n\t\t\t\t<div class="input-group pap_est_budget_container">\n\t\t\t\t\t<div class="input-group-prepend">\n\t\t\t\t\t\t<span class="input-group-text">&#8369;</span>\n\t\t\t\t\t</div>\n\t\t\t\t\t<input type="text" class="form-control allocation-month alloc-month-' + row_counter + '" name="allocation' + row_counter + '[]" value="0.00" required>\n\t\t\t\t</div>\n\t\t\t</td>\n\t\t\t<td>\n\t\t\t\t<div class="input-group pap_est_budget_container">\n\t\t\t\t\t<div class="input-group-prepend">\n\t\t\t\t\t\t<span class="input-group-text">&#8369;</span>\n\t\t\t\t\t</div>\n\t\t\t\t\t<input type="text" class="form-control allocation-month alloc-month-' + row_counter + '" name="allocation' + row_counter + '[]" value="0.00" required>\n\t\t\t\t</div>\n\t\t\t</td>\n\t\t\t<td>\n\t\t\t\t<div class="input-group pap_est_budget_container">\n\t\t\t\t\t<div class="input-group-prepend">\n\t\t\t\t\t\t<span class="input-group-text">&#8369;</span>\n\t\t\t\t\t</div>\n\t\t\t\t\t<input type="text" class="form-control allocation-month alloc-month-' + row_counter + '" name="allocation' + row_counter + '[]" value="0.00" required>\n\t\t\t\t</div>\n\t\t\t</td>\n\t\t\t<td>\n\t\t\t\t<div class="input-group pap_est_budget_container">\n\t\t\t\t\t<div class="input-group-prepend">\n\t\t\t\t\t\t<span class="input-group-text">&#8369;</span>\n\t\t\t\t\t</div>\n\t\t\t\t\t<input type="text" class="form-control allocation-month alloc-month-' + row_counter + '" name="allocation' + row_counter + '[]" value="0.00" required>\n\t\t\t\t</div>\n\t\t\t</td>\n\t\t\t<td>\n\t\t\t\t<div class="input-group pap_est_budget_container">\n\t\t\t\t\t<div class="input-group-prepend">\n\t\t\t\t\t\t<span class="input-group-text">&#8369;</span>\n\t\t\t\t\t</div>\n\t\t\t\t\t<input type="text" class="form-control allocation-month alloc-month-' + row_counter + '" name="allocation' + row_counter + '[]" value="0.00" required>\n\t\t\t\t</div>\n\t\t\t</td>\n\t\t\t<td>\n\t\t\t\t<div class="input-group pap_est_budget_container">\n\t\t\t\t\t<div class="input-group-prepend">\n\t\t\t\t\t\t<span class="input-group-text">&#8369;</span>\n\t\t\t\t\t</div>\n\t\t\t\t\t<input type="text" class="form-control allocation-month alloc-month-' + row_counter + '" name="allocation' + row_counter + '[]" value="0.00" required>\n\t\t\t\t</div>\n\t\t\t</td>\n\t\t\t<td>\n\t\t\t\t<div class="input-group pap_est_budget_container">\n\t\t\t\t\t<div class="input-group-prepend">\n\t\t\t\t\t\t<span class="input-group-text">&#8369;</span>\n\t\t\t\t\t</div>\n\t\t\t\t\t<input type="text" class="form-control allocation-month alloc-month-' + row_counter + '" name="allocation' + row_counter + '[]" value="0.00" required>\n\t\t\t\t</div>\n\t\t\t</td>\n\t\t\t<td>\n\t\t\t\t<div class="input-group pap_est_budget_container">\n\t\t\t\t\t<div class="input-group-prepend">\n\t\t\t\t\t\t<span class="input-group-text">&#8369;</span>\n\t\t\t\t\t</div>\n\t\t\t\t\t<input type="text" class="form-control allocation-month alloc-month-' + row_counter + '" name="allocation' + row_counter + '[]" value="0.00" required>\n\t\t\t\t</div>\n\t\t\t</td>\n\t\t\t' + ($('#formType').val() !== 'create' ? '<td><textarea name="remarks[]" class="form-control" rows="3" readonly></textarea></td>' : null) + '\n\t\t\t<td>\n\t\t\t\t<button type="button" class="btn btn-danger remove-pap-row"> - </button>\n\t\t\t</td>\n\t\t</tr>\n\t');

	pap_row.appendTo('tbody.pap-body');

	$('select[name="pap_mode_of_procurement' + row_counter + '"]').select2({
		placeholder: 'Please select one.',
		width: 'resolve'
	});

	procurement_modes.appendTo('select[name="pap_mode_of_procurement' + row_counter + '"]');
	uacs_objects.appendTo('select[name="pap_uacs_object' + row_counter + '"]');

	// $('.procurement_modes_select').on('change', function() {

	// 	let budget = $(this).closest('tr').find('.pap_est_budget').val()
	// 	budget=budget.replace(/\,/g,'')
	// 	var budget_value = parseFloat(budget);


	// 	max_value = $(this).find(`:selected`).data('max-value');
	// 	min_value = $(this).find(`:selected`).data('min-value');

	// 	if((min_value !== 0) && (max_value !== 0)){
	// 		if(budget_value < min_value) {
	// 			alert('Budget too low for selected mode of procurement!');
	// 			$(this).val("").trigger('change');
	// 		}
	// 		if(budget_value > max_value){
	// 			alert('Budget too high for selected mode of procurement!');
	// 			$(this).val("").trigger('change');
	// 		}
	// 	} else if ((min_value === 0) && (max_value !== 0)){
	// 		if(budget_value < min_value) {
	// 			alert('Budget too low for selected mode of procurement!');
	// 			$(this).val("").trigger('change');
	// 		}
	// 		if(budget_value > max_value){
	// 			alert('Budget too high for selected mode of procurement!');
	// 			$(this).val("").trigger('change');
	// 		}
	// 	}
	// })

	$('.allocation-month').mask('Z##,###,###,###,###.00', {
		reverse: true,
		translation: {
			'Z': {
				pattern: /[1-9]/
			},
			'Y': {
				pattern: /[0-9]/
			}
		}
	});

	$('.pap_qty').mask('#0000000');
});

//delete row
$('table.ppmp-tbl').on('click', '.remove-pap-row', function () {

	max_value = 0;
	min_value = 0;

	$(this).closest('tr').nextAll().find(".pap_code").each(function () {
		var name = $(this).attr('name');
		var i = name.substring(8).slice(0);
		i -= 1;
		$(this).attr('name', 'pap_code' + i);
	});

	$(this).closest('tr').nextAll().find(".pap_gen_desc").each(function () {
		var name = $(this).attr('name');
		var i = name.substring(12).slice(0);
		i -= 1;
		$(this).attr('name', 'pap_gen_desc' + i);
	});

	// $(this).closest('tr').nextAll().find(".category_code").each(function(){
	// 	var name = $(this).attr('name');
	// 	var i = name.substring(13).slice(0);
	// 	i -= 1;
	// 	$(this).attr('name', 'category_code' + i);
	// });

	$(this).closest('tr').nextAll().find(".allotment_type").each(function () {
		var name = $(this).attr('name');
		var i = name.substring(14).slice(0);
		i -= 1;
		$(this).attr('name', 'allotment_type' + i);
	});

	$(this).closest('tr').nextAll().find(".pap_qty").each(function () {
		var name = $(this).attr('name');
		var i = name.substring(7).slice(0);
		i -= 1;
		$(this).attr('name', 'pap_qty' + i);
	});

	$(this).closest('tr').nextAll().find(".pap-uacs-object").each(function () {
		var name = $(this).attr('name');
		var i = name.substring(15).slice(0);
		i -= 1;
		$(this).attr('name', 'pap-uacs-object' + i);
	});

	$(this).closest('tr').nextAll().find(".pap_est_budget").each(function () {
		var name = $(this).attr('name');
		var i = name.substring(14).slice(0);
		i -= 1;
		$(this).attr('name', 'pap_est_budget' + i);
	});

	$(this).closest('tr').nextAll().find(".procurement_modes_select").each(function () {
		var name = $(this).attr('name');
		var count = name.substring(23);
		var i = count.slice(0, count.length - 2);
		i -= 1;
		$(this).attr('name', 'pap_mode_of_procurement' + i + '[]');
	});

	$(this).closest('tr').nextAll().find(".allocation-month").each(function () {
		var name = $(this).attr('name');
		var count = name.substring(10);
		var i = count.slice(0, count.length - 2);
		i -= 1;
		$(this).attr('name', 'allocation' + i + '[]');
	});

	$(this).closest('tr.pap-row').nextAll().each(function () {
		var id = $(this).attr('id');
		var count = id.substring(0);
		count -= 1;
		$(this).attr('id', count);
	});

	$(this).parent().parent().remove();

	//compute budget, inflation, contingency, total_est_budget
	compute_budgets();
	row_counter -= 1;
});

//allow numeric inputs only
$(document).on('keydown', '.pap_est_budget', function (e) {
	if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true) || e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true) || e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true) || e.keyCode >= 35 && e.keyCode <= 39) {
		return;
	}
	if ((e.shiftKey || e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105)) {
		e.preventDefault();
	}
});

//budget computation
$(document).on('keyup', '.allocation-month', function () {
	compute_budgets();
});

// $('table.ppmp-tbl').on('change', '.pap_est_budget', function(){
// 	var budget = $(this).val()
// 	budget = budget.replace(/\,/g,'')
// 	budget_value = parseFloat(budget)
// 	let row = $(this).closest('tr').attr('id')

// 	max_value = $(`select[name="pap_mode_of_procurement${row}"]`).find(':selected').data('max-value')
// 	min_value = $(`select[name="pap_mode_of_procurement${row}"]`).find(':selected').data('min-value')

// 	if((min_value !== 0) && (max_value !== 0)){
// 		if(budget_value < min_value) {
// 			alert('Budget too low for selected mode of procurement!');
// 			$(this).closest('tr').find(`select.procurement_modes_select`).val("").trigger('change');
// 		}
// 		if(budget_value > max_value){
// 			alert('Budget too high for selected mode of procurement!');
// 			$(this).closest('tr').find(`select.procurement_modes_select`).val("").trigger('change');
// 		}
// 	} else if ((min_value === 0) && (max_value !== 0)){
// 		if(budget_value < min_value) {
// 			alert('Budget too low for selected mode of procurement!');
// 			$(this).closest('tr').find(`select.procurement_modes_select`).val("").trigger('change');
// 		}
// 		if(budget_value > max_value){
// 			alert('Budget too high for selected mode of procurement!');
// 			$(this).closest('tr').find(`select.procurement_modes_select`).val("").trigger('change');
// 		}
// 	}
// });

$(function () {
	$('input[name="saveType"]').on('change', function () {
		var val = $('input[name="saveType"]:checked').val();
		if (val === 'draft') {
			$('#submit-btn').val('Save as draft');
		} else {
			$('#submit-btn').val('Submit');
		}
	});
});

//autofill allotment type on change of uacsobject
$(document).on('change', 'select.pap-uacs-object', function () {
	var type = $(this).find(':selected').data('type');
	var row_id = $(this).closest('tr').attr('id');
	$(this).closest('tr').find('input[name="allotment_type' + row_id + '"]').val(type.toUpperCase());
});

/***/ })

/******/ });