$('#confirm_pwd').on('keyup', function(){
	var confirm = $(this).val();
	var pw = $('#pwd').val();

	if(confirm != pw){
		$(this).addClass('not-confirmed');
		$('#submit-btn').attr('disabled', true);
	} else {
		$(this).removeClass('not-confirmed');
		$(this).addClass('confirmed');
		$('#submit-btn').attr('disabled', false);
	}
});