//remove user role row
$(document).on('click', '.remove-role-btn', function(){
	$(this).parent().parent().parent().remove();
	role_counter -= 1;
});

var role_counter = parseInt($('.role_id').last().val());
console.log(role_counter);
//add new user role row
$('.add-role-btn').on('click', function(){
	role_counter += 1;
	var row = $('<div class="row role-div"><div class="col-md-10"><div class="form-group"><input type="hidden" class="role_id" name="role_id[]" value="'+role_counter+'" /><input class="form-control text-center role" name="role[]" type="text" value=""></div></div><div class="col-md-1"><div class="form-group"><button type="button" class="btn btn-danger remove-role-btn">-</button></div></div></div>');

	$('div.role-container').append(row);
});