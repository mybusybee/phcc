/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 145);
/******/ })
/************************************************************************/
/******/ ({

/***/ 145:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(146);


/***/ }),

/***/ 146:
/***/ (function(module, exports) {

$(function () {
	$('.date').datepicker({
		dateFormat: 'MM d, yy'
	});
});

$(document).on('change', '.pr-mooe', function () {

	var pr_mooe = parseFloat($(this).val());
	var pr_mooe_id = $(this).attr('id');
	var pr_mooe_id_tokens = pr_mooe_id.split('-');
	var row_num = pr_mooe_id_tokens[pr_mooe_id_tokens.length - 1];

	var total_estimate = parseFloat($('#total_estimate_' + row_num).text().replace(/,/g, ''));
	var pr_co = parseFloat($('#pr-co-' + row_num).val().replace(/,/g, ''));

	if (total_estimate == pr_mooe + pr_co) {
		$('#ongoing-row-' + row_num).removeClass('with-error');
		$('input[type="submit"]').attr('disabled', false);
	} else {
		$('#ongoing-row-' + row_num).addClass('with-error');
		$('input[type="submit"]').attr('disabled', true);
	}
});

$(document).on('change', '.pr-co', function () {
	var pr_co = parseFloat($(this).val());
	var pr_co_id = $(this).attr('id');
	var pr_co_id_tokens = pr_co_id.split('-');
	var row_num = pr_co_id_tokens[pr_co_id_tokens.length - 1];

	var total_estimate = parseFloat($('#total_estimate_' + row_num).text().replace(/,/g, ''));
	var pr_mooe = parseFloat($('#pr-mooe-' + row_num).val().replace(/,/g, ''));

	if (total_estimate == pr_mooe + pr_co) {
		$('#ongoing-row-' + row_num).removeClass('with-error');
		$('input[type="submit"]').attr('disabled', false);
	} else {
		$('#ongoing-row-' + row_num).addClass('with-error');
		$('input[type="submit"]').attr('disabled', true);
	}
});

$(document).on('change', '.contract-mooe', function () {
	var contract_mooe = parseFloat($(this).val());
	var contract_mooe_id = $(this).attr('id');
	var contract_mooe_id_tokens = contract_mooe_id.split('-');
	var row_num = contract_mooe_id_tokens[contract_mooe_id_tokens.length - 1];

	var contract_cost = parseFloat($('#contract_cost_' + row_num).text().replace(/,/g, ''));
	var contract_co = parseFloat($('#contract-co-' + row_num).val().replace(/,/g, ''));

	if (contract_cost == contract_mooe + contract_co) {
		$('#ongoing-row-' + row_num).removeClass('with-error');
		$('input[type="submit"]').attr('disabled', false);
	} else {
		$('#ongoing-row-' + row_num).addClass('with-error');
		$('input[type="submit"]').attr('disabled', true);
	}
});

$(document).on('change', '.contract-co', function () {
	var contract_co = parseFloat($(this).val());
	var contract_co_id = $(this).attr('id');
	var contract_co_id_tokens = contract_co_id.split('-');
	var row_num = contract_co_id_tokens[contract_co_id_tokens.length - 1];

	var contract_cost = parseFloat($('#contract_cost_' + row_num).text().replace(/,/g, ''));
	var contract_mooe = parseFloat($('#contract-mooe-' + row_num).val().replace(/,/g, ''));

	if (contract_cost == contract_mooe + contract_co) {
		$('#ongoing-row-' + row_num).removeClass('with-error');
		$('input[type="submit"]').attr('disabled', false);
	} else {
		$('#ongoing-row-' + row_num).addClass('with-error');
		$('input[type="submit"]').attr('disabled', true);
	}
});

/*********************FOR COMPLETED PRs*********************/

$(document).on('change', '.pr-com-mooe', function () {
	var pr_mooe = parseFloat($(this).val());
	var pr_mooe_id = $(this).attr('id');
	var pr_mooe_id_tokens = pr_mooe_id.split('-');
	var row_num = pr_mooe_id_tokens[pr_mooe_id_tokens.length - 1];

	var total_estimate = parseFloat($('#completed_total_estimate_' + row_num).text().replace(/,/g, ''));
	var pr_co = parseFloat($('#pr-com-co-' + row_num).val().replace(/,/g, ''));

	if (total_estimate == pr_mooe + pr_co) {
		$('#completed-row-' + row_num).removeClass('with-error');
		$('input[type="submit"]').attr('disabled', false);
	} else {
		$('#completed-row-' + row_num).addClass('with-error');
		$('input[type="submit"]').attr('disabled', true);
	}
});

$(document).on('change', '.pr-com-co', function () {
	debugger;
	var pr_co = parseFloat($(this).val());
	var pr_co_id = $(this).attr('id');
	var pr_co_id_tokens = pr_co_id.split('-');
	var row_num = pr_co_id_tokens[pr_co_id_tokens.length - 1];

	var total_estimate = parseFloat($('#completed_total_estimate_' + row_num).text().replace(/,/g, ''));
	var pr_mooe = parseFloat($('#pr-com-mooe-' + row_num).val().replace(/,/g, ''));

	if (total_estimate == pr_mooe + pr_co) {
		$('#completed-row-' + row_num).removeClass('with-error');
		$('input[type="submit"]').attr('disabled', false);
	} else {
		$('#completed-row-' + row_num).addClass('with-error');
		$('input[type="submit"]').attr('disabled', true);
	}
});

$(document).on('change', '.com-contract-mooe', function () {
	var contract_mooe = parseFloat($(this).val());
	var contract_mooe_id = $(this).attr('id');
	var contract_mooe_id_tokens = contract_mooe_id.split('-');
	var row_num = contract_mooe_id_tokens[contract_mooe_id_tokens.length - 1];

	var contract_cost = parseFloat($('#completed_contract_cost_' + row_num).text().replace(/,/g, ''));
	var contract_co = parseFloat($('#com-contract-co-' + row_num).val().replace(/,/g, ''));

	if (contract_cost == contract_mooe + contract_co) {
		$('#completed-row-' + row_num).removeClass('with-error');
		$('input[type="submit"]').attr('disabled', false);
	} else {
		$('#completed-row-' + row_num).addClass('with-error');
		$('input[type="submit"]').attr('disabled', true);
	}
});

$(document).on('change', '.com-contract-co', function () {
	var contract_co = parseFloat($(this).val());
	var contract_co_id = $(this).attr('id');
	var contract_co_id_tokens = contract_co_id.split('-');
	var row_num = contract_co_id_tokens[contract_co_id_tokens.length - 1];

	var contract_cost = parseFloat($('#completed_contract_cost_' + row_num).text().replace(/,/g, ''));
	var contract_mooe = parseFloat($('#com-contract-mooe-' + row_num).val().replace(/,/g, ''));

	if (contract_cost == contract_mooe + contract_co) {
		$('#completed-row-' + row_num).removeClass('with-error');
		$('input[type="submit"]').attr('disabled', false);
	} else {
		$('#completed-row-' + row_num).addClass('with-error');
		$('input[type="submit"]').attr('disabled', true);
	}
});

/***/ })

/******/ });