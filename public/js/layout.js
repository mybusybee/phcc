$(function($) {
	var slider = $("nav.sidebar").slideReveal({
		trigger: $(".menu-icon"),
	});

	display_ct()
});

$( document ).ready(function() {
	$( ".cross" ).hide();
	$( ".menu" ).hide();
	$( ".hamburger" ).click(function() {
		$( ".hamburger" ).hide();
		$( ".cross" ).show();
		$( ".menu" ).slideToggle( "slow" );
	});

	$( ".cross" ).click(function() {
		$( ".cross" ).hide();
		$( ".hamburger" ).show();
		$( ".menu" ).slideToggle( "slow" );
	});
});


//time display
let options = {
	day: '2-digit',
	weekday: 'long',
	year: 'numeric',
	month: 'short',
	hour: '2-digit',
	minute: '2-digit',
	second: '2-digit'
}

function display_c(){
	var refresh=1000; // Refresh rate in milli seconds
	mytime=setTimeout('display_ct()',refresh)
}
	
function display_ct() {
	var x = new Date().toLocaleDateString('en-US', options)
	document.getElementById('ct').innerHTML = x;
	display_c();
}


//confirmation before leaving page if user has unsaved work
var formHasChanged = false;
var submitted = false;

$(document).on('change', 'form input, form select, form textarea', function (e) {
   formHasChanged = true;
});

$(document).ready(function () {
   window.onbeforeunload = function (e) {
	   if (formHasChanged && !submitted) {
		   var message = "You have not saved your changes.", e = e || window.event;
		   if (e) {
			   e.returnValue = message;
		   }
		   return message;
	   }
   }
$("form").submit(function() {
	submitted = true;
	});
});


//reload if idle
var idleTime = 0;
$(document).ready(function () {
    //Increment the idle time counter every minute.
	var idleInterval = setInterval(timerIncrement, 60000); // 1 minute
	console.log('Page auto reload after 3 mins idle time.');

    //Zero the idle timer on mouse movement.
    $(this).mousemove(function (e) {
        idleTime = 0;
    });
    $(this).keypress(function (e) {
        idleTime = 0;
    });
});

function timerIncrement() {
	idleTime = idleTime + 1;
	console.log('Idle minutes: ' + idleTime);
    if (idleTime > 2) { // 3 minutes
        window.location.reload();
    }
}

//blur time container on hover and restore default on leave
$('#time-container').on('mouseover', function(){
	$(this).css('opacity', 0.3);
})

$('#time-container').on('mouseleave', function(){
	$(this).css('opacity', 1);
})

$(function(){
	$('form').on('submit', function(){
		$('input[type="submit"]').attr('disabled', true)
	})
})


$(function() {
	$('.btn').each(function() {
		$(this).addClass('ripple')
	})
})