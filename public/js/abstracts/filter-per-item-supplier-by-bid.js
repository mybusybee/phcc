/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 171);
/******/ })
/************************************************************************/
/******/ ({

/***/ 171:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(172);


/***/ }),

/***/ 172:
/***/ (function(module, exports) {

var purchase_type = $('.purchase_type').val();
var selected_items_arr = [];

$(document).on('change', '.reco_suppliers', function () {
	// debugger
	var sub_item_count = $('#sub_item_count').val();
	if (sub_item_count == undefined) sub_item_count = "0";

	if (purchase_type == "per item") {
		var supplier_id = $(this).val();
		var supplier_elem_id = $(this).attr('id');
		var supplier_elem_id_tokens = supplier_elem_id.split("_");
		var row_no = supplier_elem_id_tokens[supplier_elem_id_tokens.length - 1];

		var supplier_count = $("input[name='supplierList[]']").length;

		var base_item_nos = $('select#reco_item_no option').clone();
		//debugger
		$('#reco_sub_total_' + row_no).val(0);
		$('#reco_total').val(0);

		var row_item_no_dropdown = $('select#reco_item_no_' + row_no + ' option').remove();
		$('select#reco_item_no_' + row_no).append('<option value="0">Please select the items</option>');

		for (var supplier_ctr = 0; supplier_ctr < supplier_count; supplier_ctr++) {

			for (var base_itm_ctr = 0; base_itm_ctr < base_item_nos.length; base_itm_ctr++) {
				var item_id = base_item_nos[base_itm_ctr].value;
				var compliance_cell = $('#ranking_supplier_' + supplier_id + '_pr_item_' + item_id + '_col_' + supplier_ctr);
				if (sub_item_count != "0") compliance_cell = $('#ranking_supplier_' + supplier_id + '_pr_sub_item_' + item_id + '_col_' + supplier_ctr);

				if (compliance_cell.length > 0) {
					var item_compliance = $('#ranking_supplier_' + supplier_id + '_pr_item_' + item_id + '_col_' + supplier_ctr).val();
					if (sub_item_count != "0") item_compliance = $('#ranking_supplier_' + supplier_id + '_pr_sub_item_' + item_id + '_col_' + supplier_ctr).val();

					if (item_compliance == "Bid Over ABC" || item_compliance == "N/A" || item_compliance == "-" || item_compliance == "NO BID") {} else {
						$('select#reco_item_no_' + row_no).append(base_item_nos[base_itm_ctr]);
					}
				}
			}
		}
	}
});

$(document).on('change', '.reco_suppliers', function () {
	//kunin ung selected values
	//add sa array if wala
	//
	//iterate sa lahat ng reco_items para hanapin ung idisable

	var reco_row_count = $('.reco-row').length;
	if (reco_row_count > 1) {
		var supplier_elem_id = $(this).attr('id');
		var supplier_elem_id_tokens = supplier_elem_id.split("_");
		var row_no = supplier_elem_id_tokens[supplier_elem_id_tokens.length - 1];

		$('select#reco_item_no_' + row_no).each(function () {
			for (var sel_ctr = 0; sel_ctr < selected_items_arr.length; sel_ctr++) {
				$(this).find("option[value = " + selected_items_arr[sel_ctr] + "]").attr("disabled", true);
			}
		});
	}
});

$(document).on('change', '.reco_item_nos', function () {
	selected_items_arr = [];

	$('select.reco_item_nos').each(function () {
		$(this).find(":selected").each(function () {
			var selected_id = $(this).val();
			if (selected_id != '0') selected_items_arr.push(selected_id);
		});
	});

	selected_items_arr = selected_items_arr.filter(function (v, i, a) {
		return a.indexOf(v) === i;
	});

	var reco_row_count = $('.reco-row').length;
	if (reco_row_count > 1) {
		$('select.reco_item_nos').each(function () {
			var options = $(this).find("option");
			// for(let opt_ctr = 0; opt_ctr < options.length; opt_ctr++){
			// 	options[opt_ctr].attr("disabled", false)	
			// }

			$(options).each(function () {
				$(this).attr("disabled", false);
				// debugger
			});

			for (var sel_ctr = 0; sel_ctr < selected_items_arr.length; sel_ctr++) {
				// debugger
				$(this).find("option[value = " + selected_items_arr[sel_ctr] + "]").attr("disabled", true);
			}
		});
	}
});

/***/ })

/******/ });