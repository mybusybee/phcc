/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 166);
/******/ })
/************************************************************************/
/******/ ({

/***/ 166:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(167);


/***/ }),

/***/ 167:
/***/ (function(module, exports) {

$(document).on('change', '.reco_item_nos', function () {
	var subtotal = 0;
	var sub_item_count = $('#sub_item_count').val();
	var purchase_type = $('#purchase_type').val();
	debugger;
	if (sub_item_count == undefined) {
		sub_item_count = 0;
	}

	var reco_item_nos = $(this).val();
	var reco_row_id = $(this).closest('.reco-row').attr('id');
	var reco_row_id_tokens = reco_row_id.split("_");
	var reco_row_no = reco_row_id_tokens[reco_row_id_tokens.length - 1];
	var supplier_id = $('#reco_supplier_' + reco_row_no).val();

	if (purchase_type == 'per lot') {
		subtotal = $('#total_cost_total_supplier' + supplier_id).val();
	} else {
		reco_item_nos.forEach(function (item, index) {
			var supplier_total_cost = 0;
			if (item != "0") {
				supplier_total_cost = parseFloat($('#total_cost_supplier_' + supplier_id + '_pr_item_' + item).val());
				if (sub_item_count != "0" || sub_item_count != 0) {
					supplier_total_cost = parseFloat($('#sub_total_cost_supplier_' + supplier_id + '_pr_sub_item_' + item).val());
				}
			}
			subtotal += supplier_total_cost;
		});
	}

	$("#reco_sub_total_" + reco_row_no).val(subtotal);

	getRecoTotal();
});

$(document).on('change', '.reco_suppliers', function () {
	var subtotal = 0;
	var sub_item_count = $('#sub_item_count').val();
	var purchase_type = $('#purchase_type').val();
	// debugger
	if (sub_item_count == undefined) {
		sub_item_count = 0;
	}

	var supplier_id = $(this).val();
	var reco_row_id = $(this).closest('.reco-row').attr('id');
	var reco_row_id_tokens = reco_row_id.split("_");
	var reco_row_no = reco_row_id_tokens[reco_row_id_tokens.length - 1];
	var reco_item_nos = $('#reco_item_no_' + reco_row_no).val();

	// reco_item_nos.forEach(function(item, index){
	// 	let supplier_total_cost = parseFloat($('#total_cost_supplier_'+supplier_id+'_pr_item_'+item).val());
	// 	subtotal += supplier_total_cost;
	// })

	if (purchase_type == 'per lot') {
		subtotal = $('#total_cost_total_supplier' + supplier_id).val();
	} else {
		reco_item_nos.forEach(function (item, index) {
			var supplier_total_cost = 0;
			if (item != "0") {
				supplier_total_cost = parseFloat($('#total_cost_supplier_' + supplier_id + '_pr_item_' + item).val());
				if (sub_item_count != "0" || sub_item_count != 0) {
					supplier_total_cost = parseFloat($('#sub_total_cost_supplier_' + supplier_id + '_pr_sub_item_' + item).val());
				}
			}
			subtotal += supplier_total_cost;
		});
	}

	$("#reco_sub_total_" + reco_row_no).val(subtotal);

	getRecoTotal();
});

function getRecoTotal() {
	var reco_total = 0.00;

	var reco_sub_totals = document.getElementsByClassName("reco_sub_totals");

	for (var i = 0; i < reco_sub_totals.length; i++) {
		var reco_row_subtotal = parseFloat(reco_sub_totals[i].value);
		reco_total += reco_row_subtotal;
	}

	$("#reco_total").val(reco_total);
}

/***/ })

/******/ });