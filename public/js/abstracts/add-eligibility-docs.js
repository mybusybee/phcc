/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 170);
/******/ })
/************************************************************************/
/******/ ({

/***/ 170:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(42);


/***/ }),

/***/ 42:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addEligibilityDocs", function() { return addEligibilityDocs; });
var eligibility_docs_counter = void 0;
$(function () {
	eligibility_docs_counter = $('tr.eligibility_docs').length - 1;
});

var addEligibilityDocs = function addEligibilityDocs(supplierCount) {
	var supplier_count = parseInt(supplierCount);

	var eli_title_rowspan = document.getElementById("eli_req_title").rowSpan;
	eligibility_docs_counter++;

	var eligibility_docs_row = $('\n\t\t<tr>\n\t\t\t<td colspan="3" class="eligibility_docs_cell" id="eligibility_docs_cell_' + eligibility_docs_counter + '">\n\t\t\t\t<input type="hidden" name="eligibility_docs_count[]" id="eligibility_docs_count">\n\t\t\t\t<button type="button" class="remove-doc-row btn btn-danger btn-xs" style="float: left;">-</button>\n\t\t\t\t<select name="eligibility_docs_' + eligibility_docs_counter + '" id="" required class="document-select form-control form-control-sm border border-info" style="width: 95%; float: right;">\n\t\t\t\t\t<option value="" disabled selected>Select One</option>\n\t\t\t\t\t<option value="f1">Mayor\'s/Business Permit</option>\n\t\t\t\t\t<option value="f2">BIR Certificate of Registration</option>\n\t\t\t\t\t<option value="f3">Professional License / Curriculum Vitae (Consulting Services)</option>\n\t\t\t\t\t<option value="f4">PhilGEPS Registration Number</option>\n\t\t\t\t\t<option value="f5">PCAB License(Infra.)</option>\n\t\t\t\t\t<option value="f6">NFCC(Infra.)</option>\n\t\t\t\t\t<option value="f7">Income / Business Tax Return</option>\n\t\t\t\t\t<option value="f8">Notarized Omnnibus Sworn Statement</option>\n\t\t\t\t\t<option value="f9">Others</option>\n\t\t\t\t</select>\n\t\t\t</td>\n\t\t\t' + Array(supplier_count).join(0).split(0).map(function (item, i) {
		return '\n\t\t\t<td colspan="2">\n\t\t\t\t<select name="eligibility_docs_' + eligibility_docs_counter + '_supplier' + i + '" id="legal_docs_' + eligibility_docs_counter + '_supplier' + i + '" required class="compliance-select compliance_supplier' + i + ' form-control form-control-sm border border-info">\n\t\t\t\t\t<option value="" disabled selected>Select One</option>\n\t\t\t\t\t<option value="Comply">Comply</option>\n\t\t\t\t\t<option value="Not Comply">Not Comply</option>\n\t\t\t\t</select>\n\t\t\t</td>\n\t\t\t';
	}).join('') + '\n\t\t</tr>\n\t');

	$('.add-eligibility-docs').closest('tr').after(eligibility_docs_row);

	document.getElementById("eli_req_title").rowSpan = eli_title_rowspan + 1;
};

/***/ })

/******/ });