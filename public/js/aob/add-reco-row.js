/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 177);
/******/ })
/************************************************************************/
/******/ ({

/***/ 177:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(178);


/***/ }),

/***/ 178:
/***/ (function(module, exports) {

/*
*
*
Add Recommendation Row
*
*
*/
var reco_row_counter = $('div.reco-section').length;
$(document).on('click', 'button.add-recommendation-row', function () {

	if (reco_row_counter < 30) {
		var reco_row = $('\n\t\t\t<div class="row reco-row mb-2" id="reco_row_' + reco_row_counter + '">\n\t\t\t\t<input type="hidden" name="reco_row_count[]" id="reco_row_count">\n\t\t\t\t<div class="col-xl-3">\n\t\t\t\t\t<select name="reco_supplier_' + reco_row_counter + '" id="reco_supplier_' + reco_row_counter + '" required class="reco_suppliers form-control form-control-sm border border-info w-50">\n\t\t\t\t\t\t<option value="" disabled selected>Please select one...</option>\n\t\t\t\t\t</select>\n\t\t\t\t</div>\n\t\t\t\t<div class="col-xl-2">\n\t\t\t\t\t<select name="reco_item_no_' + reco_row_counter + '[]" id="reco_item_no_' + reco_row_counter + '" required class="reco_item_nos form-control form-control-sm border border-info w-50" multiple>\n\t\t\t\t\t\t<option value="0">Please select the items</option>\n\t\t\t\t\t</select>\n\t\t\t\t</div>\n\t\t\t\t<div class="col-xl-3">\n\t\t\t\t\t<input type="text" name="reco_sub_total_' + reco_row_counter + '" id="reco_sub_total_' + reco_row_counter + '" class="reco_sub_totals form-control form-control-sm border border-info w-50" value="0" readonly>\n\t\t\t\t</div>\n\t\t\t\t<div class="col-xl-3">\n\t\t\t\t\t<input type="text" name="reco_remarks_' + reco_row_counter + '" class="form-control form-control-sm border border-info" required>\n\t\t\t\t</div>\n\t\t\t\t<div class="col-xl-1">\n\t\t\t\t\t<button type="button" class="remove-reco-row btn btn-danger btn-xs" style="float: right;">-</button>\n\t\t\t\t</div>\n\t\t\t</div>\n\t\t\t');

		reco_row.appendTo('div.reco-section');

		$('select#reco_supplier_updated option').clone().appendTo('select[name="reco_supplier_' + reco_row_counter + '"]');
		$('select#reco_item_no option').clone().appendTo('select[name="reco_item_no_' + reco_row_counter + '[]"]');

		reco_row_counter++;
	} else {
		alert('Cannot add more than 30 recommendations!');
	}
});

/***/ })

/******/ });