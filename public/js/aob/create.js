/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 173);
/******/ })
/************************************************************************/
/******/ ({

/***/ 173:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(174);


/***/ }),

/***/ 174:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__abstracts_total_cost_calculator__ = __webpack_require__(32);


$(function () {
	// let pr_item_unit_costs = document.getElementsByClassName("pr-item-unit-cost");
	// alert(pr_item_unit_costs.length);

	$('#openingDate').datepicker({
		dateFormat: 'MM d, yy',
		minDate: 0
	});

	/**
 * 
 * RFP Number Generator
 * 
 */
	var abs_counter = $('#abstract_co').val();

	var year = String(new Date().getFullYear());
	var month = String(new Date().getMonth() + 1);
	month = month <= 9 ? "0" + month : month;
	var unique = void 0;

	if (abs_counter == 0) {
		unique = '0001';
	} else {
		var new_abs_count = parseInt(abs_counter) + 1;
		if (new_abs_count.toString().length == 1) {
			unique = '000' + parseInt(new_abs_count);
		} else if (new_abs_count.toString().length == 2) {
			unique = '00' + parseInt(new_abs_count);
		} else if (new_abs_count.toString().length == 3) {
			unique = '0' + parseInt(new_abs_count);
		} else {
			unique = parseInt(new_abs_count);
		}
	}

	var aob_no = year + month + '-' + unique;

	$('#aob_no').val(aob_no);
	$('#aobProcurementType').on('change', function () {
		var pb_type = $(this).val();

		$('#proc_mode_type').val(pb_type);

		var bid_type = $('#aobType').val();

		sentenceChanger(bid_type, pb_type);
	});

	$('#aobType').on('change', function () {
		var bid_type = $(this).val();
		var pb_type = $('#aobProcurementType').val();

		sentenceChanger(bid_type, pb_type);

		if (bid_type === 'As Read') {
			var obs_header = document.getElementById('obs-header');
			var obs_content = document.getElementById('obs-content');
			obs_header.classList.remove('d-none');
			obs_content.classList.remove('d-none');

			var twg_header = document.getElementById('twg-header');
			var twg_content = document.getElementById('twg-content');
			twg_header.classList.add("d-none");
			twg_content.classList.add("d-none");
		} else if (bid_type === 'As Calculated') {
			var _obs_header = document.getElementById('obs-header');
			var _obs_content = document.getElementById('obs-content');
			_obs_header.classList.add('d-none');
			_obs_content.classList.add('d-none');

			var _twg_header = document.getElementById('twg-header');
			var _twg_content = document.getElementById('twg-content');
			_twg_header.classList.remove("d-none");
			_twg_content.classList.remove("d-none");
		}
	});
});

function sentenceChanger(bid_type, pb_type) {
	if (bid_type === 'As Read') {
		if (pb_type === "1" || pb_type === "2") $('#bidder_grammar').text('Single/Lowest Calculated Bidder');else if (pb_type === "3") $('#bidder_grammar').text('Single/Highest Rated Bidder');
	} else if (bid_type === 'As Calculated') {
		if (pb_type === "1" || pb_type === "2") $('#bidder_grammar').text('Single/Lowest Calculated and Responsive Bidder');else if (pb_type === "3") $('#bidder_grammar').text('Single/Highest Rated and Responsive Bidder');
	}
}

//this will dynamically add a text input below the dropdown and remove it on change
$(document).on('change', '.document-select', function () {
	var document_type = $(this).val();
	var doc_container_id = $(this).closest('td').attr('id');
	var doc_container_id_tokens = doc_container_id.split("_");
	var doc_container_row_no = doc_container_id_tokens[doc_container_id_tokens.length - 1];

	var extra_document_values = ['a9', 'b9', 'c9', 'd9', 'e9'];

	if (extra_document_values.indexOf(document_type) != -1) {
		var other_doc_input = "";

		if (document_type == 'a9') {
			other_doc_input = $('<input type="text" name="legal_docs_extra_' + doc_container_row_no + '" id="additional_doc" class="additional_doc form-control form-control-sm border border-info mt-2" style="width:95%; float:right">');
		} else if (document_type == 'b9') {
			other_doc_input = $('<input type="text" name="technical_docs_extra_' + doc_container_row_no + '" id="additional_doc" class="additional_doc form-control form-control-sm border border-info mt-2" style="width:95%; float:right">');
		} else if (document_type == 'c9') {
			other_doc_input = $('<input type="text" name="financial_docs_extra_' + doc_container_row_no + '" id="additional_doc" class="additional_doc form-control form-control-sm border border-info mt-2" style="width:95%; float:right">');
		} else if (document_type == 'd9') {
			other_doc_input = $('<input type="text" name="classb_docs_extra_' + doc_container_row_no + '" id="additional_doc" class="additional_doc form-control form-control-sm border border-info mt-2" style="width:95%; float:right">');
		} else if (document_type == 'e9') {
			other_doc_input = $('<input type="text" name="other_docs_extra_' + doc_container_row_no + '" id="additional_doc" class="additional_doc form-control form-control-sm border border-info mt-2" style="width:95%; float:right">');
		}

		other_doc_input.appendTo('#' + doc_container_id);
	} else {
		$(this).next('.additional_doc').remove();
	}

	var supplier_count = $("input[name='supplierList[]']").length;
	if (document_type == 'd2') {
		for (var i = 0; i < supplier_count; i++) {
			$('#classb_docs_' + doc_container_row_no + '_supplier' + i).val("N/A");
		}
	}
});

$(document).on('change', '.compliance-select', function () {

	var compliance_cell_id = $(this).attr('id');
	var compliance_cell_id_tokens = compliance_cell_id.split("_");
	var compliance_suffix = compliance_cell_id_tokens[compliance_cell_id_tokens.length - 1];

	var supplier_compliance_array = document.getElementsByClassName("compliance_" + compliance_suffix);

	var compliance = true;
	for (var sup_ctr = 0; sup_ctr < supplier_compliance_array.length; sup_ctr++) {
		var supplier_compliance = supplier_compliance_array[sup_ctr].value;
		if (supplier_compliance == "Not Comply") {
			compliance = false;
		}
	}

	var compliance_suffix_tokens = compliance_suffix.split('supplier');
	var supplier_index = compliance_suffix_tokens[1];
	var pr_item_unit_costs = document.getElementsByClassName("pr-item-unit-cost" + supplier_index);
	var pr_sub_item_unit_costs = document.getElementsByClassName("pr-sub-item-unit-cost" + supplier_index);

	if (compliance) {
		$('#result_compliance_' + compliance_suffix).val('PASSED');
		// debugger

		var pr_item_total_costs = document.getElementsByClassName("pr-item-total-cost" + supplier_index);
		for (var pr_total_ctr = 0; pr_total_ctr < pr_item_total_costs.length; pr_total_ctr++) {
			if (pr_item_total_costs[pr_total_ctr].value == "0.00" || pr_item_total_costs[pr_total_ctr].value == "n/a") pr_item_total_costs[pr_total_ctr].value = "0.00";else break;
		}

		for (var pr_item_ctr = 0; pr_item_ctr < pr_item_unit_costs.length; pr_item_ctr++) {

			if (pr_item_unit_costs[pr_item_ctr].value == "n/a") {
				pr_item_unit_costs[pr_item_ctr].value = "";
				pr_item_unit_costs[pr_item_ctr].readOnly = false;
			}
		}

		var pr_sub_item_total_costs = document.getElementsByClassName("pr-sub-item-total-cost" + supplier_index);
		for (var pr_sub_total_ctr = 0; pr_sub_total_ctr < pr_sub_item_total_costs.length; pr_sub_total_ctr++) {
			if (pr_sub_item_total_costs[pr_sub_total_ctr].value == "0.00" || pr_sub_item_total_costs[pr_sub_total_ctr].value == "n/a") pr_sub_item_total_costs[pr_sub_total_ctr].value = "0.00";else break;
		}

		for (var pr_sub_item_ctr = 0; pr_sub_item_ctr < pr_sub_item_unit_costs.length; pr_sub_item_ctr++) {

			if (pr_sub_item_unit_costs[pr_sub_item_ctr].value == "n/a") {
				pr_sub_item_unit_costs[pr_sub_item_ctr].value = "";
				pr_sub_item_unit_costs[pr_sub_item_ctr].readOnly = false;
			}
		}

		$('#supplier_ranking' + supplier_index).val("-");
	} else {
		$('#result_compliance_' + compliance_suffix).val('FAILED');

		var _pr_item_total_costs = document.getElementsByClassName("pr-item-total-cost" + supplier_index);
		for (var _pr_total_ctr = 0; _pr_total_ctr < _pr_item_total_costs.length; _pr_total_ctr++) {
			_pr_item_total_costs[_pr_total_ctr].value = "n/a";
		}

		for (var _pr_item_ctr = 0; _pr_item_ctr < pr_item_unit_costs.length; _pr_item_ctr++) {

			pr_item_unit_costs[_pr_item_ctr].value = "n/a";
			pr_item_unit_costs[_pr_item_ctr].readOnly = true;
		}

		$('.total_cost_total_supplier' + supplier_index).text('0.00');

		$('#supplier_ranking' + supplier_index).val("N/A");
	}

	Object(__WEBPACK_IMPORTED_MODULE_0__abstracts_total_cost_calculator__["rankSuppliers"])();
});

function updateCompliantSupplier() {
	var current_suppliers = $('select#reco_supplier option').clone();

	var updated_supplier_select = $('select#reco_supplier_updated option').remove();

	for (var ctr = 0; ctr < current_suppliers.length; ctr++) {
		var sup_val = current_suppliers[ctr].value;

		var sup_compliance = $('.result_compliance_supplier_id_' + sup_val).val();
		var ranking = $('#supplier_ranking' + ctr).val();

		if (sup_compliance === 'PASSED' && ranking !== 'Bid Over ABC' && ranking !== 'N/A') {
			$('select#reco_supplier_updated').append(current_suppliers[ctr]);
		}
	}
}

/***/ }),

/***/ 32:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "rankSuppliers", function() { return rankSuppliers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateCompliantSupplier", function() { return updateCompliantSupplier; });
var supplier_id = 0;
var purchase_type = $('.purchase_type').val();

$(function () {
	$('.abstract-form').submit(function () {
		$('select.reco_item_nos').each(function () {
			var options = $(this).find("option");

			$(options).each(function () {
				$(this).attr("disabled", false);
			});
		});
	});
});

$(document).on('keyup', '.pr-item-unit-cost', function () {

	var supplier_unit_cost_str = $(this).val();
	var supplier_unit_cost = parseFloat(supplier_unit_cost_str);
	var supplier_unit_cost_id = $(this).attr('id');
	var supplier_unit_cost_id_tokens = supplier_unit_cost_id.split("_");
	var item_id = supplier_unit_cost_id_tokens[supplier_unit_cost_id_tokens.length - 1];
	var item_quantity = parseFloat($("#item_quantity_" + item_id).text());
	var supplier_total_cost = supplier_unit_cost * item_quantity;

	var supplier_id_tokens = supplier_unit_cost_id.split("unit_");
	var total_cost_id = "#total_" + supplier_id_tokens[1];

	if (supplier_unit_cost_str === 'n/a') {
		$(total_cost_id).val('n/a');
	} else if (supplier_unit_cost_str === 'no bid') {
		$(total_cost_id).val('no bid');
	} else if (supplier_unit_cost_str == '') {
		$(total_cost_id).val('0.00');
	} else {
		if (supplier_total_cost != null) {
			$(total_cost_id).val(supplier_total_cost);
		} else {
			$(total_cost_id).val('0.00');
		}

		//make border red for a visual indicator
		if (purchase_type == "per item") {
			var purchase_total_cost = parseFloat($("#item_cost_total_" + item_id).text());

			if (supplier_total_cost > purchase_total_cost) {
				$(total_cost_id).parent().addClass('with-error');
			} else {
				$(total_cost_id).parent().removeClass('with-error');
			}
		}
	}

	//pr-item-total-cost
	supplier_id = supplier_unit_cost_id_tokens[3];
});

$(document).on('keyup', '.pr-sub-item-unit-cost', function () {
	// debugger
	var supplier_unit_cost_str = $(this).val();
	var supplier_unit_cost = parseFloat(supplier_unit_cost_str);
	var supplier_unit_cost_id = $(this).attr('id');
	var supplier_unit_cost_id_tokens = supplier_unit_cost_id.split("_");
	var sub_item_id = supplier_unit_cost_id_tokens[supplier_unit_cost_id_tokens.length - 1];
	var item_quantity = parseFloat($("#sub_item_quantity_" + sub_item_id).text());
	var supplier_total_cost = supplier_unit_cost * item_quantity;

	var supplier_id_tokens = supplier_unit_cost_id.split("sub_unit_");
	var total_cost_id = "#sub_total_" + supplier_id_tokens[1];

	if (supplier_unit_cost_str === 'n/a') {
		$(total_cost_id).val('n/a');
	} else if (supplier_unit_cost_str === 'no bid') {
		$(total_cost_id).val('no bid');
	} else if (supplier_unit_cost_str == '') {
		$(total_cost_id).val('0.00');
	} else {
		if (supplier_total_cost != null) {
			$(total_cost_id).val(supplier_total_cost);
		} else {
			$(total_cost_id).val('0.00');
		}

		//make border red for a visual indicator
		if (purchase_type == "per item") {
			var purchase_total_cost = parseFloat($("#sub_item_cost_total_" + sub_item_id).text().replace(/,/g, ''));
			if (supplier_total_cost > purchase_total_cost) {
				$(total_cost_id).parent().addClass('with-error');
			} else {
				$(total_cost_id).parent().removeClass('with-error');
			}
		}
	}

	//pr-item-total-cost
	supplier_id = supplier_unit_cost_id_tokens[4];
});

$(document).on('change', '.pr-item-unit-cost', function () {
	if (purchase_type == 'per lot') {
		var item_ids = document.getElementsByClassName("item_ids");
		var amount_total = 0;
		for (var item_ctr = 0; item_ctr < item_ids.length; item_ctr++) {
			var item_id = item_ids[item_ctr].innerHTML;

			var purchase_total_cost = parseFloat($("#item_cost_total_" + item_id).text());
			var item_total_cost_str = $("#total_cost_supplier_" + supplier_id + "_pr_item_" + item_id).val();

			if (item_total_cost_str !== 'n/a') {
				if (item_total_cost_str !== 'no bid') {
					var item_total_cost = parseFloat(item_total_cost_str);

					amount_total += item_total_cost;
				} else {
					amount_total = 'no bid';
					break;
				}
			} else {
				amount_total = 'n/a';
				break;
			}
		}
		$("#total_cost_total_supplier" + supplier_id).val(amount_total);
		rankSuppliers();
	} else if (purchase_type == 'per item') {
		var item_cell_id = $(this).attr('id');
		var item_cell_id_tokens = item_cell_id.split('_');
		var pr_id = item_cell_id_tokens[item_cell_id_tokens.length - 1];
		//debugger
		rankSuppliers(pr_id);
	}
});

$(document).on('change', '.pr-sub-item-unit-cost', function () {
	if (purchase_type == 'per lot') {
		var sub_item_ids = document.getElementsByClassName("sub_item_ids");
		var amount_total = 0;
		for (var sub_item_ctr = 0; sub_item_ctr < sub_item_ids.length; sub_item_ctr++) {
			var sub_item_id = sub_item_ids[sub_item_ctr].innerHTML;

			var purchase_total_cost = parseFloat($("#sub_item_cost_total_" + sub_item_id).text().replace(/,/g, ''));
			var sub_item_total_cost_str = $("#sub_total_cost_supplier_" + supplier_id + "_pr_sub_item_" + sub_item_id).val();

			if (sub_item_total_cost_str !== 'n/a') {
				if (sub_item_total_cost_str !== 'no bid') {
					var item_total_cost = parseFloat(sub_item_total_cost_str);

					amount_total += item_total_cost;
				} else {
					amount_total = 'no bid';
					break;
				}
			} else {
				amount_total = 'n/a';
				break;
			}
		}
		$("#total_cost_total_supplier" + supplier_id).val(amount_total);
		rankSuppliers();
	} else if (purchase_type == 'per item') {
		var item_cell_id = $(this).attr('id');
		var item_cell_id_tokens = item_cell_id.split('_');
		var pr_id = item_cell_id_tokens[item_cell_id_tokens.length - 1];
		// debugger
		rankSuppliers(pr_id);
	}
});

var rankSuppliers = function rankSuppliers(pr_id) {
	var supplier_amount_arr = [];

	if (purchase_type == "per lot" && pr_id === undefined) {

		var total_estimate = $('#total_estimate').text();
		total_estimate = parseFloat(total_estimate.slice(1, total_estimate.length).replace(/,/g, ''));

		var supplier_amounts = document.getElementsByClassName("total_cost_total_suppliers");

		for (var amt_ctr = 0; amt_ctr < supplier_amounts.length; amt_ctr++) {
			var supplier_column_id = ".total_cost_total_supplier" + amt_ctr;
			// let amount_value_str = $(supplier_column_id).text();
			var amount_value_str = $(supplier_column_id).val();
			var amount_value = parseFloat(amount_value_str);

			if (amount_value_str != 'n/a') {
				if (amount_value_str != 'no bid') {
					if (!isNaN(amount_value)) {
						if (amount_value < total_estimate && amount_value > 0 || amount_value == total_estimate) {
							supplier_amount_arr.push({
								supplier_col_index: amt_ctr,
								amount: amount_value
							});
						} else if (amount_value > total_estimate) {
							$("#supplier_ranking" + amt_ctr).val("Bid Over ABC");
						}
					}
				} else {
					$("#supplier_ranking" + amt_ctr).val("NO BID");
				}
			} else {
				$("#supplier_ranking" + amt_ctr).val("N/A");
			}
		}

		supplier_amount_arr.sort(function (a, b) {
			return a.amount - b.amount;
		});
		//debugger

		var final_ranking_arr = [];
		for (var sorted_ctr = 0; sorted_ctr < supplier_amount_arr.length; sorted_ctr++) {
			if (sorted_ctr == 0) {
				final_ranking_arr.push({
					rank: 1,
					supplier_col_index: supplier_amount_arr[sorted_ctr].supplier_col_index,
					amount: supplier_amount_arr[sorted_ctr].amount
				});
			} else {
				var base_entry = final_ranking_arr[sorted_ctr - 1];
				var base_amount = base_entry.amount;

				if (supplier_amount_arr[sorted_ctr].amount == base_amount) {
					final_ranking_arr.push({
						rank: base_entry.rank,
						supplier_col_index: supplier_amount_arr[sorted_ctr].supplier_col_index,
						amount: supplier_amount_arr[sorted_ctr].amount
					});
				} else {
					final_ranking_arr.push({
						rank: base_entry.rank + 1,
						supplier_col_index: supplier_amount_arr[sorted_ctr].supplier_col_index,
						amount: supplier_amount_arr[sorted_ctr].amount
					});
				}
			}
		}

		for (var final_arr_ctr = 0; final_arr_ctr < final_ranking_arr.length; final_arr_ctr++) {
			$("#supplier_ranking" + final_ranking_arr[final_arr_ctr].supplier_col_index).val(final_ranking_arr[final_arr_ctr].rank);
		}
	} else if (purchase_type == "per item" && pr_id !== undefined) {
		var sub_item_count = $('#sub_item_count').val();
		if (sub_item_count == undefined) sub_item_count = "0";

		var _total_estimate = $('#item_cost_total_' + pr_id).text();
		// debugger
		if (sub_item_count != "0") _total_estimate = $('#sub_item_cost_total_' + pr_id).text();
		// total_estimate = parseFloat(total_estimate.slice(1, total_estimate.length).replace(/,/g, ''));
		_total_estimate = parseFloat(_total_estimate.replace(/,/g, ''));

		var row_total_costs = document.getElementsByClassName('pr-item_' + pr_id + '_total-cost');
		if (sub_item_count != "0") row_total_costs = document.getElementsByClassName('pr-sub-item_' + pr_id + '_total-cost');

		for (var item_ctr = 0; item_ctr < row_total_costs.length; item_ctr++) {
			var total_supplier_cost = row_total_costs[item_ctr].value;
			var _amount_value = parseFloat(total_supplier_cost);

			var total_supplier_cost_id = row_total_costs[item_ctr].id;
			var total_supplier_cost_tokens = total_supplier_cost_id.split('_');
			var item_supplier_id = total_supplier_cost_tokens[3];
			if (sub_item_count != "0") item_supplier_id = total_supplier_cost_tokens[4];

			if (total_supplier_cost != 'n/a') {
				if (total_supplier_cost != 'no bid') {
					if (total_supplier_cost != '0') {
						if (!isNaN(_amount_value)) {
							if (_amount_value < _total_estimate && _amount_value > 0 || _amount_value == _total_estimate) {
								supplier_amount_arr.push({
									supplier_col_index: item_ctr,
									supplier_id: item_supplier_id,
									amount: _amount_value
								});
							} else if (_amount_value > _total_estimate) {
								$("#ranking_supplier_" + item_supplier_id + "_pr_item_" + pr_id + "_col_" + item_ctr).val("Bid Over ABC");
								if (sub_item_count != "0") $("#ranking_supplier_" + item_supplier_id + "_pr_sub_item_" + pr_id + "_col_" + item_ctr).val("Bid Over ABC");
							}
						}
					} else {
						$("#ranking_supplier_" + item_supplier_id + "_pr_item_" + pr_id + "_col_" + item_ctr).val("FREE");
						if (sub_item_count != "0") $("#ranking_supplier_" + item_supplier_id + "_pr_sub_item_" + pr_id + "_col_" + item_ctr).val("FREE");
					}
				} else {
					$("#ranking_supplier_" + item_supplier_id + "_pr_item_" + pr_id + "_col_" + item_ctr).val("NO BID");
					if (sub_item_count != "0") $("#ranking_supplier_" + item_supplier_id + "_pr_sub_item_" + pr_id + "_col_" + item_ctr).val("NO BID");
				}
			} else {
				$("#ranking_supplier_" + item_supplier_id + "_pr_item_" + pr_id + "_col_" + item_ctr).val("N/A");
				if (sub_item_count != "0") $("#ranking_supplier_" + item_supplier_id + "_pr_sub_item_" + pr_id + "_col_" + item_ctr).val("N/A");
			}
		}

		supplier_amount_arr.sort(function (a, b) {
			return a.amount - b.amount;
		});

		var _final_ranking_arr = [];
		for (var _sorted_ctr = 0; _sorted_ctr < supplier_amount_arr.length; _sorted_ctr++) {
			if (_sorted_ctr == 0) {
				_final_ranking_arr.push({
					rank: 1,
					supplier_id: supplier_amount_arr[_sorted_ctr].supplier_id,
					supplier_col_index: supplier_amount_arr[_sorted_ctr].supplier_col_index,
					amount: supplier_amount_arr[_sorted_ctr].amount
				});
			} else {
				var _base_entry = _final_ranking_arr[_sorted_ctr - 1];
				var _base_amount = _base_entry.amount;

				if (supplier_amount_arr[_sorted_ctr].amount == _base_amount) {
					_final_ranking_arr.push({
						rank: _base_entry.rank,
						supplier_id: supplier_amount_arr[_sorted_ctr].supplier_id,
						supplier_col_index: supplier_amount_arr[_sorted_ctr].supplier_col_index,
						amount: supplier_amount_arr[_sorted_ctr].amount
					});
				} else {
					_final_ranking_arr.push({
						rank: _base_entry.rank + 1,
						supplier_id: supplier_amount_arr[_sorted_ctr].supplier_id,
						supplier_col_index: supplier_amount_arr[_sorted_ctr].supplier_col_index,
						amount: supplier_amount_arr[_sorted_ctr].amount
					});
				}
			}
		}

		for (var _final_arr_ctr = 0; _final_arr_ctr < _final_ranking_arr.length; _final_arr_ctr++) {
			$("#ranking_supplier_" + _final_ranking_arr[_final_arr_ctr].supplier_id + "_pr_item_" + pr_id + "_col_" + _final_ranking_arr[_final_arr_ctr].supplier_col_index).val(_final_ranking_arr[_final_arr_ctr].rank);
			if (sub_item_count != "0") $("#ranking_supplier_" + _final_ranking_arr[_final_arr_ctr].supplier_id + "_pr_sub_item_" + pr_id + "_col_" + _final_ranking_arr[_final_arr_ctr].supplier_col_index).val(_final_ranking_arr[_final_arr_ctr].rank);
		}

		// for (let sorted_ctr = 0; sorted_ctr < supplier_amount_arr.length; sorted_ctr++){
		// 	$("#ranking_supplier_" + supplier_amount_arr[sorted_ctr].supplier_id +"_pr_item_" + pr_id + "_col_" + supplier_amount_arr[sorted_ctr].supplier_col_index).val(sorted_ctr + 1);
		// 	if (sub_item_count != "0")
		// 		$("#ranking_supplier_"+supplier_amount_arr[sorted_ctr].supplier_id+"_pr_sub_item_"+pr_id+"_col_"+supplier_amount_arr[sorted_ctr].supplier_col_index).val(sorted_ctr + 1);
		// }
	}

	updateCompliantSupplier();
};

var updateCompliantSupplier = function updateCompliantSupplier() {
	//get the based supplier options from vue state
	var current_suppliers = $('select#reco_supplier option').clone();

	//clear the new dropdown of old options
	var updated_supplier_select = $('select#reco_supplier_updated option').remove();

	//get the abstract type
	var abstract_type = $('#abstract_type').val();

	//loop through the based suppliers
	for (var ctr = 0; ctr < current_suppliers.length; ctr++) {
		var sup_val = current_suppliers[ctr].value;
		var sup_compliance = $('.result_compliance_supplier_id_' + sup_val).val();

		if (purchase_type == "per lot") {
			var ranking = $('#supplier_ranking' + ctr).val();

			if (abstract_type == 'aob') {
				if (sup_compliance === 'PASSED' && ranking !== 'Bid Over ABC' && ranking !== 'N/A' && ranking !== 'NO BID') {
					$('select#reco_supplier_updated').append(current_suppliers[ctr]);
				}
			} else if (abstract_type == 'aop') {
				//debugger
				var tech_proposal_res = $('#tech_proposal_res_' + ctr).val();
				if (sup_compliance === 'COMPLIANT' && ranking !== 'Bid Over ABC' && ranking !== 'N/A' && ranking !== 'NO BID' && tech_proposal_res == "PASSED") {
					$('select#reco_supplier_updated').append(current_suppliers[ctr]);
				}
			} else {
				if (sup_compliance === 'COMPLIANT' && ranking !== 'Bid Over ABC' && ranking !== 'N/A' && ranking !== 'NO BID') {
					$('select#reco_supplier_updated').append(current_suppliers[ctr]);
				}
			}
		} else if (purchase_type == "per item") {
			// debugger
			if (abstract_type == 'aob') {
				if (sup_compliance === 'PASSED') {
					$('select#reco_supplier_updated').append(current_suppliers[ctr]);
				}
			} else if (abstract_type == 'aop') {
				//debugger
				var _tech_proposal_res = $('#tech_proposal_res_' + ctr).val();
				if (sup_compliance === 'COMPLIANT' && _tech_proposal_res == "PASSED") {
					$('select#reco_supplier_updated').append(current_suppliers[ctr]);
				}
			} else {
				if (sup_compliance === 'COMPLIANT') {
					$('select#reco_supplier_updated').append(current_suppliers[ctr]);
				}
			}
		}
	}
};

/***/ })

/******/ });