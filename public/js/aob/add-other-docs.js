/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 186);
/******/ })
/************************************************************************/
/******/ ({

/***/ 186:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(40);


/***/ }),

/***/ 40:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addOtherDocs", function() { return addOtherDocs; });
var other_docs_counter = void 0;
$(function () {
	other_docs_counter = $('tr.other_docs').length - 1;
});

var addOtherDocs = function addOtherDocs(supplierCount) {
	var supplier_count = parseInt(supplierCount);
	var eli_title_rowspan = document.getElementById("eli_req_title").rowSpan;

	other_docs_counter++;

	var other_docs_row = $('\n\t\t<tr>\n\t\t\t<td colspan="3" class="other_docs_cell" id="other_docs_cell_' + other_docs_counter + '">\n\t\t\t\t<input type="hidden" name="other_docs_count[]" id="legal_docs_count">\n\t\t\t\t<button type="button" class="remove-doc-row btn btn-danger btn-xs" style="float: left;">-</button>\n\t            <select name="other_docs_' + other_docs_counter + '" id="" required class="document-select form-control form-control-sm border border-info" style="width: 95%; float: right;">\n\t                <option value="" disabled selected>Select One</option>\n\t                <option value="e1">Bid Security</option>\n\t                <option value="e2">Omnibus Sworn Statement</option>\n\t                <option value="e3">Technical Specifications (for Goods)</option>\n\t                <option value="e4">Project Requirements (for Infra)</option>\n\t                <option value="e5">Corporate Secretary Certificate / Board Resolution for Authorized Representative</option>\n\t                <option value="e9">Others</option>\n\t            </select>\n\t        </td>\n\t        ' + Array(supplier_count).join(0).split(0).map(function (item, i) {
		return '\n\t\t    <td colspan="2">\n                <select name="other_docs_' + other_docs_counter + '_supplier' + i + '" id="other_docs_' + other_docs_counter + '_supplier' + i + '" required class="compliance-select compliance_supplier' + i + ' form-control form-control-sm border border-info">\n                    <option value="" disabled selected>Select One</option>\n                    <option value="Comply">Comply</option>\n                    <option value="Not Comply">Not Comply</option>\n                </select>\n            </td>\n\t\t  \t';
	}).join('') + '\n\t    </tr>\n\t');

	$('.add-other-docs').closest('tr').after(other_docs_row);

	document.getElementById("eli_req_title").rowSpan = eli_title_rowspan + 1;
};

/***/ })

/******/ });