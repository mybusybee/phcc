/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 183);
/******/ })
/************************************************************************/
/******/ ({

/***/ 183:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(41);


/***/ }),

/***/ 41:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addTechDocs", function() { return addTechDocs; });
var technical_docs_counter = void 0;
$(function () {
	technical_docs_counter = $('tr.tech_docs').length - 1;
});
var addTechDocs = function addTechDocs(supplierCount) {
	var supplier_count = parseInt(supplierCount);
	var eli_title_rowspan = document.getElementById("eli_req_title").rowSpan;

	technical_docs_counter++;

	var tech_docs_row = $('\n\t\t<tr>\n\t\t\t<td colspan="3" class="technical_docs_cell" id="technical_docs_cell_' + technical_docs_counter + '">\n\t\t\t\t<input type="hidden" name="tech_docs_count[]" id="legal_docs_count">\n\t\t\t\t<button type="button" class="remove-doc-row btn btn-danger btn-xs" style="float: left;">-</button>\n\t            <select name="technical_docs_' + technical_docs_counter + '" id="" required class="document-select form-control form-control-sm border border-info" style="width: 95%; float: right;">\n\t                <option value="" disabled selected>Select One</option>\n\t                <option value="b1">Statement of all Ongoing Government and Private Contracts, including awarded but not yet started</option>\n\t                <option value="b2">Statement of Single Largest Completed Contract (SLCC) similar to the contract to be bid</option>\n\t                <option value="b3">PBAC License (Infra.)</option>\n\t                <option value="b9">Others</option>\n\t            </select>\n\t        </td>\n\t        ' + Array(supplier_count).join(0).split(0).map(function (item, i) {
		return '\n\t\t    <td colspan="2">\n                <select name="technical_docs_' + technical_docs_counter + '_supplier' + i + '" id="technical_docs_' + technical_docs_counter + '_supplier' + i + '" required class="compliance-select compliance_supplier' + i + ' form-control form-control-sm border border-info">\n                    <option value="" disabled selected>Select One</option>\n                    <option value="Comply">Comply</option>\n                    <option value="Not Comply">Not Comply</option>\n                </select>\n            </td>\n\t\t  \t';
	}).join('') + '\n\t    </tr>\n\t');

	$('.add-technical-docs').closest('tr').after(tech_docs_row);

	document.getElementById("eli_req_title").rowSpan = eli_title_rowspan + 1;
};

/***/ })

/******/ });