let data_array = document.getElementById('prStatusGraph1').value.split(',')
var ctx = document.getElementById("prStatusCanvasChart");
var myChart = new Chart(ctx, {
    type: 'radar',
    data: {
        labels: ["EU Revision", "Dir. Approval", "Proc. Validation", "Budget Alloc.", "Submission", 'Assignment', 'Assigned'],
        datasets: [{
            label: 'Total (PHP)',
            data: data_array,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
            ],
            borderColor: [
                'rgba(255,99,132,1)',
            ],
            borderWidth: 1
        }]
    },
});