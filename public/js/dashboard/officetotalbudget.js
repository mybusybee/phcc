let officeTotalBudgetArray = document.getElementById('officeTotalBudget').value.split(',')
var ctx = document.getElementById("officeTotalBudgetCanvasChart");
var myChart = new Chart(ctx, {
    type: 'pie',
    data: {
        labels: ["Office of the Chairperson", "AO", "FPMO", "MAO", "CEO", 'EO', 'LAO', "OED", 'Office of Commissioners'],
        datasets: [{
            label: 'Total (PHP)',
            data: officeTotalBudgetArray,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(4, 255, 0, 0.2)',
                'rgba(255, 0, 247, 0.2)',
                'rgba(88, 88, 88, 0.2)',
                'rgba(106, 228, 200, 0.2)'
            ],
            borderColor: [
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(4, 255, 0, 1)',
                'rgba(255, 0, 247, 1)',
                'rgba(88, 88, 88, 1)',
                'rgba(106, 228, 200, 1)'
            ],
            borderWidth: 1
        }]
    },
});