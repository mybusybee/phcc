/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 223);
/******/ })
/************************************************************************/
/******/ ({

/***/ 223:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(224);


/***/ }),

/***/ 224:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__form_validation__ = __webpack_require__(36);

//add subrow counter then +1 immediately then pass to element
$(document).on('click', '.add-sub-item', function () {
    $('.add-pr-row').attr('disabled', true);
    var rowNumber = $(this).closest('tr').attr('id');
    var row = $(this).closest('tr');

    var subRowNumber = $(this).closest('tr').find('[data-parent="' + rowNumber + '"]').length / 5 + 1;

    var newItemNoSub = $('\n        <div class="input-group sub-item">\n            <input class="form-control form-control-sm sub-item-no" readonly data-parent="' + rowNumber + '" data-sub-row="' + subRowNumber + '" name="itemNoSub' + rowNumber + '[]" value="' + checkPurchaseType() + '">\n        </div>\n    ');
    newItemNoSub.appendTo('table#pr_table tbody tr#' + rowNumber + ' td.item-no-container');

    var newUnitSub = $('\n        <div class="input-group">\n            <select required class="form-control form-control-sm" data-parent="' + rowNumber + '" data-sub-row="' + subRowNumber + '" name="unitSub' + rowNumber + '[]"></select>\n        </div>\n    ');
    newUnitSub.appendTo('table#pr_table tbody tr#' + rowNumber + ' td.unit-container');
    $('select#all_units option').clone().appendTo('select[data-parent=' + rowNumber + '][data-sub-row="' + subRowNumber + '"]');

    var newDescriptionSub = $('\n        <div class="input-group" data-parent="' + rowNumber + '" data-sub-row="' + subRowNumber + '">\n            <input type="text" required class="form-control form-control-sm" name="descriptionSub' + rowNumber + '[]">\n            <div class="input-group-append">\n                <button type="button" class="btn btn-danger btn-xs remove-sub-item-row">-</button>\n            </div>\n        </div>\n    ');
    newDescriptionSub.appendTo('table#pr_table tbody tr#' + rowNumber + ' td.description-container');

    var newQuantitySub = $('\n        <div class="input-group">\n            <input required type="text" class="form-control form-control-sm subQuantity" data-parent="' + rowNumber + '" data-sub-row="' + subRowNumber + '" name="quantitySub' + rowNumber + '[]" value="0">\n        </div>\n    ');
    newQuantitySub.appendTo('table#pr_table tbody tr#' + rowNumber + ' td.quantity-container');

    var newUnitCostSub = $('\n        <div class="input-group form-text-extend input-group-sm">\n            <div class="input-group-prepend">\n                <span class="input-group-text">&#8369;</span>\n            </div>\n            <input required type="text" class="form-control form-control-sm subUnitCost" data-parent="' + rowNumber + '" data-sub-row="' + subRowNumber + '" name="unitCostSub' + rowNumber + '[]" value="0">\n        </div>\n    ');
    newUnitCostSub.appendTo('table#pr_table tbody tr#' + rowNumber + ' td.unit-cost-container');

    var newTotalCostSub = $('\n        <div class="input-group form-text-extend input-group-sm">\n            <div class="input-group-prepend">\n                <span class="input-group-text">&#8369;</span>\n            </div>\n            <input required readonly type="text" class="form-control form-control-sm subTotalCost" data-parent="' + rowNumber + '" data-sub-row="' + subRowNumber + '" name="totalCostSub' + rowNumber + '[]" value="0">\n        </div>\n    ');
    newTotalCostSub.appendTo('table#pr_table tbody tr#' + rowNumber + ' td.total-cost-container');

    subRowNumber++;

    if (subRowNumber !== 0) {
        $(row).find('select[name="unit_no_' + rowNumber + '"]').attr('disabled', true);
        $(row).find('input[name="quantity_' + rowNumber + '"]').attr('readonly', true).val('');
        $(row).find('input[name="unit_cost_' + rowNumber + '"]').attr('readonly', true).val('');

        var totalBudget = $(row).find('select[name="description_' + rowNumber + '"]').find(':selected').data('budget');

        $(row).find('input[name="total_cost_' + rowNumber + '"]').attr('readonly', true).val(totalBudget);
    }

    Object(__WEBPACK_IMPORTED_MODULE_0__form_validation__["computeTotalBudgetOfAllItems"])();

    $('.subUnitCost').mask('Z##,###,###,###,###.00', {
        reverse: true,
        translation: {
            'Z': {
                pattern: /[1-9]/
            }
        }
    });

    $('.subQuantity').mask('#0000000');
});

function checkPurchaseType() {
    var purchaseType = $('input[name="purchase_type"]:checked').val();
    if (purchaseType === 'lot') {
        return '';
    } else if (purchaseType === 'per') {
        var subItemCount = $('div.sub-item').length;
        return subItemCount + 1;
    } else {
        return '';
    }
}

/***/ }),

/***/ 36:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "computeTotalBudgetOfAllItems", function() { return computeTotalBudgetOfAllItems; });
//Sub Quantity
$(document).on('keyup', '.subQuantity', function () {
    var rowNumber = $(this).closest('tr').attr('id');
    var subRowNumber = $(this).data('sub-row');
    var value = $(this).val();
    var el = $(this);

    computeSubUnitQuantityTotal(el, rowNumber, subRowNumber);
});

//Unit Cost
$(document).on('keyup', '.subUnitCost', function () {

    var rowNumber = $(this).closest('tr').attr('id');
    var subRowNumber = $(this).data('sub-row');
    var value = $(this).val();
    var el = $(this);

    computeSubUnitQuantityTotal(el, rowNumber, subRowNumber);
});

var computeSubUnitQuantityTotal = function computeSubUnitQuantityTotal(el, rowNumber, subRowNumber) {
    var totalCost = 0;
    var quantity = 0;
    var unit = 0;

    if ($(el).hasClass('subQuantity')) {
        quantity = $(el).val();
        unit = parseFloat($(el).closest('tr').find('input.subUnitCost[data-parent="' + rowNumber + '"][data-sub-row="' + subRowNumber + '"]').val().replace(/,/g, ''));
        totalCost = parseFloat(quantity) * parseFloat(unit);
    } else {
        unit = parseFloat($(el).val().replace(/,/g, ''));
        quantity = $(el).closest('tr').find('input.subQuantity[data-parent="' + rowNumber + '"][data-sub-row="' + subRowNumber + '"]').val();
        totalCost = quantity * unit;
    }

    isNaN(totalCost) ? totalCost = 0 : totalCost;

    $(el).closest('tr').find('input.subTotalCost[data-parent="' + rowNumber + '"][data-sub-row="' + subRowNumber + '"]').val(totalCost.toLocaleString());

    var parentRowTotalCost = computeParentTotalCost(rowNumber);

    checkIfBudgetExceeds($(el), rowNumber, parentRowTotalCost);

    computeTotalBudgetOfAllItems();
};

var checkIfBudgetExceeds = function checkIfBudgetExceeds(el, rowNumber, parentRowTotalCost) {

    var totalBudget = $(el).closest('tr').find('select[name="description_' + rowNumber + '"]').find(':selected').data('budget').toLocaleString().replace(/,/g, '');

    parentRowTotalCost = parseFloat(parentRowTotalCost);

    if (parentRowTotalCost > totalBudget) {
        $(el).closest('tr').find('td.total-cost-container').addClass('form-has-error');
    } else {
        $(el).closest('tr').find('td.total-cost-container').removeClass('form-has-error');
    }
};

var computeParentTotalCost = function computeParentTotalCost(rowNumber) {
    var total = 0;
    $('input.subTotalCost[data-parent="' + rowNumber + '"]').each(function () {
        total += parseFloat($(this).val().replace(/,/g, ''));
    });
    return total;
};

var computeTotalBudgetOfAllItems = function computeTotalBudgetOfAllItems() {
    var totalEstimate = 0;
    $('table#pr_table').find('tr.pr-item').each(function () {
        var rowNumber = $(this).attr('id');
        var subRowNumber = 0;
        $(this).find('*[data-parent="' + rowNumber + '"][data-sub-row]').each(function () {
            subRowNumber++;
        });

        if (subRowNumber != 0) {
            $(this).find('input.subTotalCost[data-parent="' + rowNumber + '"]').each(function () {
                totalEstimate += parseFloat($(this).val().replace(/,/g, ''));
            });
        } else {
            totalEstimate += parseFloat($('input[name="total_cost_' + rowNumber + '"]').val().replace(/,/g, ''));
        }
    });

    if (isNaN(totalEstimate)) {
        totalEstimate = 0;
    }

    $('#total_estimate').val(totalEstimate.toLocaleString());
};

/**
 * 
 *  ON CHANGE OF SELECTED DESCRIPTION, REMOVE QUANTITIES AND COSTS FOR EACH SUBITEM
 * 
 */

//cant add sub item if description is null
$(document).on('change', 'select.description', function () {
    var val = $(this).val();
    if (val != "") {
        $(this).closest('tr').find('.add-sub-item').attr('disabled', false);
    }
});

//auto fill total budget field and remove readonly of quantity and unit cost of parent item
$(document).on('change', '.description', function () {
    var appId = $(this).find(':selected').data('app-id');
    $(this).closest('tr').find('.app_id').val(appId);

    var rowNumber = $(this).closest('tr').attr('id');

    var subRowNumber = $(this).closest('tr').find('[data-parent="' + rowNumber + '"]').length / 5;

    if (subRowNumber !== 0) {
        var totalBudget = $(this).find(':selected').data('budget');
        $(this).closest('tr').find('input[name="total_cost_' + rowNumber + '"]').val(totalBudget);
    }
    $(this).closest('tr').find('input[name="unit_cost_' + rowNumber + '"]').attr('readonly', false);
    $(this).closest('tr').find('input[name="quantity_' + rowNumber + '"]').attr('readonly', false);
});

$(document).on('keyup', '.quantity:not([readonly]), .unit_cost:not([readonly])', function () {
    var el = $(this);
    var rowNumber = $(this).closest('tr').attr('id');
    var quantity = 0;
    var unit = 0;
    var total = 0;

    if ($(el).hasClass('quantity')) {
        quantity = parseFloat($(this).val().replace(/,/g, ''));
        unit = parseFloat($('tr#' + rowNumber).find('input[name="unit_cost_' + rowNumber + '"]').val().replace(/,/g, ''));
    } else {
        unit = parseFloat($(this).val().replace(/,/g, ''));
        quantity = parseFloat($('tr#' + rowNumber).find('input[name="quantity_' + rowNumber + '"]').val().replace(/,/g, ''));
    }
    total = unit * quantity;
    if (isNaN(total)) {
        total = 0;
    }

    $('tr#' + rowNumber).find('input[name="total_cost_' + rowNumber + '"]').val(total.toLocaleString());

    computeTotalBudgetOfAllItems();

    totalCostValidation(el, total);
});

var totalCostValidation = function totalCostValidation(el, total) {
    var budgetAlloted = parseFloat($(el).closest('tr').find('select.description').find(':selected').data('budget').toLocaleString().replace(/,/g, ''));

    if (total > budgetAlloted) {
        $(el).closest('tr').find('.total-cost-container').addClass('form-has-error');
    } else {
        $(el).closest('tr').find('.total-cost-container').removeClass('form-has-error');
    }
};

$(document).on('keyup', '.unit_cost, .quantity, .subQuantity, .subUnitCost', function () {
    var errorCount = $('table#pr_table tr.pr-item td.total-cost-container.form-has-error').length;
    if (errorCount > 0) {
        $('#prSubmit').attr('disabled', true);
    } else {
        $('#prSubmit').attr('disabled', false);
    }
});

/***/ })

/******/ });