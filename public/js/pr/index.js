/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 217);
/******/ })
/************************************************************************/
/******/ ({

/***/ 14:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return baseURL; });
var baseURL = function baseURL() {
  //change to base url of the server
  return 'http://192.168.1.3/phcc/public/';
};

/***/ }),

/***/ 217:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(218);


/***/ }),

/***/ 218:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__auth_baseURL__ = __webpack_require__(14);

$(function () {
    $('#prTable').DataTable({
        "order": [[0, "desc"]]
    });
});

$('td.consolidate-cb-container').on('click', function () {
    var checkbox = $(this).children('input.consolidate-cb');
    var id = $(checkbox).val();

    if ($(checkbox).is(':checked')) {
        //add hidden textbox with checkbox value
        var row = $('\n            <input type="text" class="form-control form-control-sm" value="' + id + '" name="consolidate[]">\n        ');

        row.appendTo('div#pr_ids_container');
    } else {
        console.log('not checked');
        //remove
        $('div#pr_ids_container').find('input[value="' + id + '"]').remove();
    }

    $(checkbox).click();
});

$('input.consolidate-cb').click(function (e) {
    e.stopPropagation();

    var checkbox = $(this);
    var id = $(this).val();

    if ($(checkbox).is(':checked')) {
        console.log('checked');
        //add hidden textbox with checkbox value
        var row = $('\n            <input type="hidden" class="form-control form-control-sm" value="' + id + '" name="consolidate[]">\n        ');

        row.appendTo('div#pr_ids_container');
    } else {
        console.log('not checked');
        //remove
        $('div#pr_ids_container').find('input[value="' + id + '"]').remove();
    }
});

$('.formSelector').on('change', function () {
    var val = $(this).val();
    var id = $(this).closest('tr').find('.pr_id').val();
    if (val === 'rfp') {
        $(this).closest('tr').find('.generateFormBtn').attr('href', Object(__WEBPACK_IMPORTED_MODULE_0__auth_baseURL__["a" /* baseURL */])() + 'rfp/create/' + id);
    } else if (val === 'rfq') {
        $(this).closest('tr').find('.generateFormBtn').attr('href', Object(__WEBPACK_IMPORTED_MODULE_0__auth_baseURL__["a" /* baseURL */])() + 'rfq/create/' + id);
    } else if (val === 'rei') {
        $(this).closest('tr').find('.generateFormBtn').attr('href', Object(__WEBPACK_IMPORTED_MODULE_0__auth_baseURL__["a" /* baseURL */])() + 'rei/create/' + id);
    } else if (val === 'itb') {
        $(this).closest('tr').find('.generateFormBtn').attr('href', Object(__WEBPACK_IMPORTED_MODULE_0__auth_baseURL__["a" /* baseURL */])() + 'itb/create/pr/' + id);
    } else {
        $(this).closest('tr').find('.generateFormBtn').attr('href', '/');
    }
});

/***/ })

/******/ });