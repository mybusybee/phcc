/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 225);
/******/ })
/************************************************************************/
/******/ ({

/***/ 225:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(226);


/***/ }),

/***/ 226:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__form_validation__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__item_numbering__ = __webpack_require__(45);


$(document).on('click', '.remove-sub-item-row', function () {
    var subRow = $(this).parent().parent().data('sub-row');
    var parentRow = $(this).parent().parent().data('parent');

    var rowNumber = $(this).closest('tr').attr('id');
    var row = $(this).closest('tr');
    var subRowNumber = $(this).closest('tr').find('[data-parent="' + rowNumber + '"]').length / 5 - 1;

    if (subRowNumber === 0) {
        $(row).find('select[name="unit_no_' + rowNumber + '"]').attr('disabled', false);
        $(row).find('input[name="quantity_' + rowNumber + '"]').attr('readonly', false);
        $(row).find('input[name="unit_cost_' + rowNumber + '"]').attr('readonly', false);
        $(this).closest('tr').find('input.total_cost').val('');
    }

    $(this).closest('tr').find('[data-parent="' + parentRow + '"][data-sub-row="' + subRow + '"]').each(function () {
        $(this).closest('div.input-group').remove();
    });

    computeParentTotalCost(rowNumber);
    Object(__WEBPACK_IMPORTED_MODULE_0__form_validation__["computeTotalBudgetOfAllItems"])();

    var subItemCount = $('div.sub-item').length;
    if (subItemCount === 0) {
        $('.add-pr-row').attr('disabled', false);
        $('tr#' + rowNumber).find('input[name="quantity_' + rowNumber + '"]').attr('readonly', false);
        $('tr#' + rowNumber).find('input[name="unit_cost_' + rowNumber + '"]').attr('readonly', false);
        $('tr#' + rowNumber).find('input[name="total_cost_' + rowNumber + '"]').val('');
    }

    Object(__WEBPACK_IMPORTED_MODULE_1__item_numbering__["reCountSubItemNo"])();
});

var computeParentTotalCost = function computeParentTotalCost(rowNumber) {

    var totalBudget = $('tr#' + rowNumber).find('select.description').find(':selected').data('budget');

    if (~String(totalBudget).indexOf(',')) {
        totalBudget = parseFloat(String(totalBudget).replace(/,/g, ''));
    }

    var parentRowTotalCost = 0;
    $('input.subTotalCost[data-parent="' + rowNumber + '"]').each(function () {
        parentRowTotalCost += parseFloat($(this).val().replace(/,/g, ''));
    });

    if (parentRowTotalCost > totalBudget) {
        $('tr#' + rowNumber).find('td.total-cost-container').addClass('form-has-error');
    } else {
        $('tr#' + rowNumber).find('td.total-cost-container').removeClass('form-has-error');
    }
};

/***/ }),

/***/ 36:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "computeTotalBudgetOfAllItems", function() { return computeTotalBudgetOfAllItems; });
//Sub Quantity
$(document).on('keyup', '.subQuantity', function () {
    var rowNumber = $(this).closest('tr').attr('id');
    var subRowNumber = $(this).data('sub-row');
    var value = $(this).val();
    var el = $(this);

    computeSubUnitQuantityTotal(el, rowNumber, subRowNumber);
});

//Unit Cost
$(document).on('keyup', '.subUnitCost', function () {

    var rowNumber = $(this).closest('tr').attr('id');
    var subRowNumber = $(this).data('sub-row');
    var value = $(this).val();
    var el = $(this);

    computeSubUnitQuantityTotal(el, rowNumber, subRowNumber);
});

var computeSubUnitQuantityTotal = function computeSubUnitQuantityTotal(el, rowNumber, subRowNumber) {
    var totalCost = 0;
    var quantity = 0;
    var unit = 0;

    if ($(el).hasClass('subQuantity')) {
        quantity = $(el).val();
        unit = parseFloat($(el).closest('tr').find('input.subUnitCost[data-parent="' + rowNumber + '"][data-sub-row="' + subRowNumber + '"]').val().replace(/,/g, ''));
        totalCost = parseFloat(quantity) * parseFloat(unit);
    } else {
        unit = parseFloat($(el).val().replace(/,/g, ''));
        quantity = $(el).closest('tr').find('input.subQuantity[data-parent="' + rowNumber + '"][data-sub-row="' + subRowNumber + '"]').val();
        totalCost = quantity * unit;
    }

    isNaN(totalCost) ? totalCost = 0 : totalCost;

    $(el).closest('tr').find('input.subTotalCost[data-parent="' + rowNumber + '"][data-sub-row="' + subRowNumber + '"]').val(totalCost.toLocaleString());

    var parentRowTotalCost = computeParentTotalCost(rowNumber);

    checkIfBudgetExceeds($(el), rowNumber, parentRowTotalCost);

    computeTotalBudgetOfAllItems();
};

var checkIfBudgetExceeds = function checkIfBudgetExceeds(el, rowNumber, parentRowTotalCost) {

    var totalBudget = $(el).closest('tr').find('select[name="description_' + rowNumber + '"]').find(':selected').data('budget').toLocaleString().replace(/,/g, '');

    parentRowTotalCost = parseFloat(parentRowTotalCost);

    if (parentRowTotalCost > totalBudget) {
        $(el).closest('tr').find('td.total-cost-container').addClass('form-has-error');
    } else {
        $(el).closest('tr').find('td.total-cost-container').removeClass('form-has-error');
    }
};

var computeParentTotalCost = function computeParentTotalCost(rowNumber) {
    var total = 0;
    $('input.subTotalCost[data-parent="' + rowNumber + '"]').each(function () {
        total += parseFloat($(this).val().replace(/,/g, ''));
    });
    return total;
};

var computeTotalBudgetOfAllItems = function computeTotalBudgetOfAllItems() {
    var totalEstimate = 0;
    $('table#pr_table').find('tr.pr-item').each(function () {
        var rowNumber = $(this).attr('id');
        var subRowNumber = 0;
        $(this).find('*[data-parent="' + rowNumber + '"][data-sub-row]').each(function () {
            subRowNumber++;
        });

        if (subRowNumber != 0) {
            $(this).find('input.subTotalCost[data-parent="' + rowNumber + '"]').each(function () {
                totalEstimate += parseFloat($(this).val().replace(/,/g, ''));
            });
        } else {
            totalEstimate += parseFloat($('input[name="total_cost_' + rowNumber + '"]').val().replace(/,/g, ''));
        }
    });

    if (isNaN(totalEstimate)) {
        totalEstimate = 0;
    }

    $('#total_estimate').val(totalEstimate.toLocaleString());
};

/**
 * 
 *  ON CHANGE OF SELECTED DESCRIPTION, REMOVE QUANTITIES AND COSTS FOR EACH SUBITEM
 * 
 */

//cant add sub item if description is null
$(document).on('change', 'select.description', function () {
    var val = $(this).val();
    if (val != "") {
        $(this).closest('tr').find('.add-sub-item').attr('disabled', false);
    }
});

//auto fill total budget field and remove readonly of quantity and unit cost of parent item
$(document).on('change', '.description', function () {
    var appId = $(this).find(':selected').data('app-id');
    $(this).closest('tr').find('.app_id').val(appId);

    var rowNumber = $(this).closest('tr').attr('id');

    var subRowNumber = $(this).closest('tr').find('[data-parent="' + rowNumber + '"]').length / 5;

    if (subRowNumber !== 0) {
        var totalBudget = $(this).find(':selected').data('budget');
        $(this).closest('tr').find('input[name="total_cost_' + rowNumber + '"]').val(totalBudget);
    }
    $(this).closest('tr').find('input[name="unit_cost_' + rowNumber + '"]').attr('readonly', false);
    $(this).closest('tr').find('input[name="quantity_' + rowNumber + '"]').attr('readonly', false);
});

$(document).on('keyup', '.quantity:not([readonly]), .unit_cost:not([readonly])', function () {
    var el = $(this);
    var rowNumber = $(this).closest('tr').attr('id');
    var quantity = 0;
    var unit = 0;
    var total = 0;

    if ($(el).hasClass('quantity')) {
        quantity = parseFloat($(this).val().replace(/,/g, ''));
        unit = parseFloat($('tr#' + rowNumber).find('input[name="unit_cost_' + rowNumber + '"]').val().replace(/,/g, ''));
    } else {
        unit = parseFloat($(this).val().replace(/,/g, ''));
        quantity = parseFloat($('tr#' + rowNumber).find('input[name="quantity_' + rowNumber + '"]').val().replace(/,/g, ''));
    }
    total = unit * quantity;
    if (isNaN(total)) {
        total = 0;
    }

    $('tr#' + rowNumber).find('input[name="total_cost_' + rowNumber + '"]').val(total.toLocaleString());

    computeTotalBudgetOfAllItems();

    totalCostValidation(el, total);
});

var totalCostValidation = function totalCostValidation(el, total) {
    var budgetAlloted = parseFloat($(el).closest('tr').find('select.description').find(':selected').data('budget').toLocaleString().replace(/,/g, ''));

    if (total > budgetAlloted) {
        $(el).closest('tr').find('.total-cost-container').addClass('form-has-error');
    } else {
        $(el).closest('tr').find('.total-cost-container').removeClass('form-has-error');
    }
};

$(document).on('keyup', '.unit_cost, .quantity, .subQuantity, .subUnitCost', function () {
    var errorCount = $('table#pr_table tr.pr-item td.total-cost-container.form-has-error').length;
    if (errorCount > 0) {
        $('#prSubmit').attr('disabled', true);
    } else {
        $('#prSubmit').attr('disabled', false);
    }
});

/***/ }),

/***/ 45:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "applyNumbers", function() { return applyNumbers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "reCountSubItemNo", function() { return reCountSubItemNo; });
$('input[name="purchase_type"]').on('change', function () {
    var purchaseType = $(this).val();
    applyNumbers(purchaseType);
});

var applyNumbers = function applyNumbers(purchaseType) {
    if (purchaseType === 'lot') {
        var i = 1;
        $('.parent_item_no').each(function () {
            $(this).val(i);
            i++;
        });
        $('.sub-item-no').each(function () {
            $(this).val('');
        });
    } else if (purchaseType === 'per') {
        var subItemCount = $('.sub-item-no').length;
        if (subItemCount > 0) {
            var _i = 1;
            $('.parent_item_no').each(function () {
                $(this).val('');
            });
            $('.sub-item-no').each(function () {
                $(this).val(_i);
                _i++;
            });
        } else {
            var _i2 = 1;
            $('.parent_item_no').each(function () {
                $(this).val(_i2);
                _i2++;
            });
            $('.sub-item-no').each(function () {
                $(this).val('');
            });
        }
    }
};

var reCountSubItemNo = function reCountSubItemNo() {
    var i = 1;
    $('.sub-item-no').each(function () {
        $(this).val(i);
        i++;
    });
};

/***/ })

/******/ });