/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 219);
/******/ })
/************************************************************************/
/******/ ({

/***/ 219:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(220);


/***/ }),

/***/ 220:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__form_validation__ = __webpack_require__(36);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__item_numbering__ = __webpack_require__(45);
/**
 * 
 * Add new PR Row
 * 
 */



var row_counter = $('tr.pr-item').length;
var item_no_counter = parseInt($('#item_no_count').val());
var item_id;
$('table#pr_table').on('click', 'button.add-pr-row', function () {

	row_counter += 1;

	var row = $('\n\t\t<tr id="' + row_counter + '" class="pr-item">\n\t\t\t<td class="d-none"><input type="hidden" class="app_id" name="pr_item_row_counter[]" value="x"></td>\n\t\t\t<td class="item-no-container">\n\t\t\t\t<input required type="text" readonly class="form-control form-control-sm item_no parent_item_no" name="item_no_' + row_counter + ('" value="">\n\t\t\t</td>\n\t\t\t<td class="unit-container">\n\t\t\t\t<input required type="hidden" value="Ampule" class="form-control hidden_unit" name="unit_no_h_' + row_counter + '" />\n\t\t\t\t<select class="form-control form-control-sm unit_no" required name="unit_no_') + row_counter + '" id="unit_list" ></select>\n\t\t\t</td>\n\t\t\t<td class="description-container">\n\t\t\t\t<div class="input-group">\n\t\t\t\t\t<select required class="form-control form-control-sm description" name="description_' + row_counter + '" ></select>\n\t\t\t\t\t<div class="input-group-append">\n\t\t\t\t\t\t<button type="button" class="btn btn-primary btn-xs add-sub-item" disabled>+</button>\n\t\t\t\t\t</div>\n\t\t\t\t</div>\n\t\t\t</td>\n\t\t\t<td class="quantity-container">\n\t\t\t\t<input readonly type="text" class="form-control form-control-sm quantity" name="quantity_' + row_counter + '">\n\t\t\t</td>\n\t\t\t<td class="unit-cost-container">\n\t\t\t\t<div class="input-group form-text-extend input-group-sm">\n\t\t\t\t\t<div class="input-group-prepend">\n\t\t\t\t\t\t<span class="input-group-text">&#8369;</span>\n\t\t\t\t\t</div>\n\t\t\t\t\t<input readonly type="text" class="form-control form-control-sm unit_cost" name="unit_cost_' + row_counter + '">\n\t\t\t\t</div>\n\t\t\t</td>\n\t\t\t<td class="total-cost-container">\n\t\t\t\t<div class="input-group form-text-extend input-group-sm">\n\t\t\t\t\t<div class="input-group-prepend">\n\t\t\t\t\t\t<span class="input-group-text">&#8369;</span>\n\t\t\t\t\t</div>\n\t\t\t\t\t<input readonly type="text" class="form-control form-control-sm total_cost" name="total_cost_' + row_counter + '">\n\t\t\t\t</div>\n\t\t\t</td>\n\t\t\t<td>\n\t\t\t\t<button type="button" class="btn btn-danger btn-xs remove-pr-row">-</button>\n\t\t\t</td>\n\t\t</tr>\n\t');

	row.appendTo('table#pr_table tbody');

	$('select#description_controller option').clone().appendTo('select[name="description_' + row_counter + '"]');
	$('select#all_units option').clone().appendTo('select[name=\'unit_no_' + row_counter + '\'');

	var purchaseType = $('input[name="purchase_type"]:checked').val();
	Object(__WEBPACK_IMPORTED_MODULE_1__item_numbering__["applyNumbers"])(purchaseType);

	$('.unit_cost').mask('Z##,###,###,###,###.00', {
		reverse: true,
		translation: {
			'Z': {
				pattern: /[1-9]/
			},
			'Y': {
				pattern: /[0-9]/
			}
		}
	});

	$('.quantity').mask('#0000000');
});

/**
 * 
 * Remove PR Row
 * 
 */

$('table#pr_table').on('click', 'button.remove-pr-row', function () {
	row_counter -= 1;

	$(this).closest('tr').nextAll().find(".item_no").each(function () {
		var name = $(this).attr('name');
		var i = name.substring(8).slice(0);
		i -= 1;
		$(this).attr('name', 'item_no_' + i);

		/** Item No. Value Recount **/
		var item_no = parseInt($(this).val());
		item_no -= 1;
		$(this).val(item_no);
	});

	$(this).closest('tr').nextAll().find(".unit_no").each(function () {
		var name = $(this).attr('name');
		var i = name.substring(8).slice(0);
		i -= 1;
		$(this).attr('name', 'unit_no_' + i);
	});

	$(this).closest('tr').nextAll().find(".hidden_unit").each(function () {
		var name = $(this).attr('name');
		var i = name.substring(10).slice(0);
		i -= 1;
		$(this).attr('name', 'unit_no_h_' + i);
	});

	$(this).closest('tr').nextAll().find(".description").each(function () {
		var name = $(this).attr('name');
		var i = name.substring(12).slice(0);
		i -= 1;
		$(this).attr('name', 'description_' + i);
	});

	$(this).closest('tr').nextAll().find(".quantity").each(function () {
		var name = $(this).attr('name');
		var i = name.substring(9).slice(0);
		i -= 1;
		$(this).attr('name', 'quantity_' + i);
	});

	$(this).closest('tr').nextAll().find(".unit_cost").each(function () {
		var name = $(this).attr('name');
		var i = name.substring(10).slice(0);
		i -= 1;
		$(this).attr('name', 'unit_cost_' + i);
	});

	$(this).closest('tr').nextAll().find(".total_cost").each(function () {
		var name = $(this).attr('name');
		var i = name.substring(11).slice(0);
		i -= 1;
		$(this).attr('name', 'total_cost_' + i);
	});

	$(this).closest('tr').nextAll().each(function () {
		var id = $(this).attr('id');
		var i = id.substring(0).slice(0);
		i -= 1;
		$(this).attr('id', i);
	});

	$(this).closest('tr').remove();

	item_no_counter -= 1;
	$('#item_no_count').val(item_no_counter);
	Object(__WEBPACK_IMPORTED_MODULE_0__form_validation__["computeTotalBudgetOfAllItems"])();

	if (row_counter === 0) {
		resetHiddenDescriptionSelect();
	}
});

var resetHiddenDescriptionSelect = function resetHiddenDescriptionSelect() {
	$('select.description').each(function () {
		$(this).find('option').each(function () {
			$(this).show();
		});
	});
};

/**
 * 
 * Disable keydown events on
 * several elements
 * 
 */
$(document).on('keydown', '#pr_no, #office_name, #sai_no, #rc_code, #pr_date, #end_user, .item_no, .total_cost, #requested_by, input[name=cy], #mode_of_procurement', function () {
	return false;
});

/**
 * 
 * PR Number Generator
 * 
 */
$(document).ready(function () {
	var pr_counter = $('#pr_count').val();

	var month = String(new Date().getMonth() + 1);
	var year = String(new Date().getFullYear());
	month = month <= 9 ? "0" + month : month;
	var unique;

	if (pr_counter == 0) {
		unique = '0001';
	} else {
		var new_pr_count = parseInt(pr_counter) + 1;
		if (new_pr_count.toString().length == 1) {
			unique = '000' + parseInt(new_pr_count);
		} else if (new_pr_count.toString().length == 2) {
			unique = '00' + parseInt(new_pr_count);
		} else if (new_pr_count.toString().length == 3) {
			unique = '0' + parseInt(new_pr_count);
		} else {
			unique = parseInt(new_pr_count);
		}
	}

	var pr_no = year + month + '-' + unique;
	$('#pr_no').val(pr_no);
});

//allow numeric inputs only
$(document).on('keydown', '.quantity, .unit_cost', function (e) {
	if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true) || e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true) || e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true) || e.keyCode >= 35 && e.keyCode <= 39) {
		return;
	}
	if ((e.shiftKey || e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105)) {
		e.preventDefault();
	}
});

//populate hidden unit field (for disabled select workaround)
$(document).on('change', 'select.unit_no', function () {
	var value = $(this).val();
	$(this).closest('tr').find('input.hidden_unit').val(value);
});

//for edit form
$(function () {
	var formType = $('#pr_form_type').val();
	if (formType === 'edit') {
		$('tr.pr-item').each(function () {
			var rowNumber = $(this).attr('id');
			var appId = $(this).find('input[data-parent="' + rowNumber + '"]').val();
			$('select.description[data-parent="' + rowNumber + '"]').find('option[value="' + appId + '"]').attr('selected', true);
		});
	}
});

// $(document).on('change', '.description', function(){
// 	let mode = $(this).find(':selected').data('procurement-mode')

// 	$('.description').each(function(){
// 		$(this).find('option').each(function(){
// 			let optionMode = $(this).data('procurement-mode')
// 			if(optionMode !== mode) {
// 				$(this).hide()
// 			} else {
// 				$(this).show()
// 			}
// 		})
// 	})
// })

$(document).on('change', 'select.description', function () {
	var allotmentType = $(this).find(':selected').data('allotment-type');
	$('select.description').find('option').each(function () {
		if ($(this).data('allotment-type') !== allotmentType) {
			$(this).hide();
		}
	});
});

$(document).on('click change', function () {
	var parentItemCount = $('.pr-item').length;
	if (parentItemCount > 1) {
		$('.add-sub-item').attr('disabled', true);
	} else {
		$('.add-sub-item').attr('disabled', false);
	}
});

/***/ }),

/***/ 36:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "computeTotalBudgetOfAllItems", function() { return computeTotalBudgetOfAllItems; });
//Sub Quantity
$(document).on('keyup', '.subQuantity', function () {
    var rowNumber = $(this).closest('tr').attr('id');
    var subRowNumber = $(this).data('sub-row');
    var value = $(this).val();
    var el = $(this);

    computeSubUnitQuantityTotal(el, rowNumber, subRowNumber);
});

//Unit Cost
$(document).on('keyup', '.subUnitCost', function () {

    var rowNumber = $(this).closest('tr').attr('id');
    var subRowNumber = $(this).data('sub-row');
    var value = $(this).val();
    var el = $(this);

    computeSubUnitQuantityTotal(el, rowNumber, subRowNumber);
});

var computeSubUnitQuantityTotal = function computeSubUnitQuantityTotal(el, rowNumber, subRowNumber) {
    var totalCost = 0;
    var quantity = 0;
    var unit = 0;

    if ($(el).hasClass('subQuantity')) {
        quantity = $(el).val();
        unit = parseFloat($(el).closest('tr').find('input.subUnitCost[data-parent="' + rowNumber + '"][data-sub-row="' + subRowNumber + '"]').val().replace(/,/g, ''));
        totalCost = parseFloat(quantity) * parseFloat(unit);
    } else {
        unit = parseFloat($(el).val().replace(/,/g, ''));
        quantity = $(el).closest('tr').find('input.subQuantity[data-parent="' + rowNumber + '"][data-sub-row="' + subRowNumber + '"]').val();
        totalCost = quantity * unit;
    }

    isNaN(totalCost) ? totalCost = 0 : totalCost;

    $(el).closest('tr').find('input.subTotalCost[data-parent="' + rowNumber + '"][data-sub-row="' + subRowNumber + '"]').val(totalCost.toLocaleString());

    var parentRowTotalCost = computeParentTotalCost(rowNumber);

    checkIfBudgetExceeds($(el), rowNumber, parentRowTotalCost);

    computeTotalBudgetOfAllItems();
};

var checkIfBudgetExceeds = function checkIfBudgetExceeds(el, rowNumber, parentRowTotalCost) {

    var totalBudget = $(el).closest('tr').find('select[name="description_' + rowNumber + '"]').find(':selected').data('budget').toLocaleString().replace(/,/g, '');

    parentRowTotalCost = parseFloat(parentRowTotalCost);

    if (parentRowTotalCost > totalBudget) {
        $(el).closest('tr').find('td.total-cost-container').addClass('form-has-error');
    } else {
        $(el).closest('tr').find('td.total-cost-container').removeClass('form-has-error');
    }
};

var computeParentTotalCost = function computeParentTotalCost(rowNumber) {
    var total = 0;
    $('input.subTotalCost[data-parent="' + rowNumber + '"]').each(function () {
        total += parseFloat($(this).val().replace(/,/g, ''));
    });
    return total;
};

var computeTotalBudgetOfAllItems = function computeTotalBudgetOfAllItems() {
    var totalEstimate = 0;
    $('table#pr_table').find('tr.pr-item').each(function () {
        var rowNumber = $(this).attr('id');
        var subRowNumber = 0;
        $(this).find('*[data-parent="' + rowNumber + '"][data-sub-row]').each(function () {
            subRowNumber++;
        });

        if (subRowNumber != 0) {
            $(this).find('input.subTotalCost[data-parent="' + rowNumber + '"]').each(function () {
                totalEstimate += parseFloat($(this).val().replace(/,/g, ''));
            });
        } else {
            totalEstimate += parseFloat($('input[name="total_cost_' + rowNumber + '"]').val().replace(/,/g, ''));
        }
    });

    if (isNaN(totalEstimate)) {
        totalEstimate = 0;
    }

    $('#total_estimate').val(totalEstimate.toLocaleString());
};

/**
 * 
 *  ON CHANGE OF SELECTED DESCRIPTION, REMOVE QUANTITIES AND COSTS FOR EACH SUBITEM
 * 
 */

//cant add sub item if description is null
$(document).on('change', 'select.description', function () {
    var val = $(this).val();
    if (val != "") {
        $(this).closest('tr').find('.add-sub-item').attr('disabled', false);
    }
});

//auto fill total budget field and remove readonly of quantity and unit cost of parent item
$(document).on('change', '.description', function () {
    var appId = $(this).find(':selected').data('app-id');
    $(this).closest('tr').find('.app_id').val(appId);

    var rowNumber = $(this).closest('tr').attr('id');

    var subRowNumber = $(this).closest('tr').find('[data-parent="' + rowNumber + '"]').length / 5;

    if (subRowNumber !== 0) {
        var totalBudget = $(this).find(':selected').data('budget');
        $(this).closest('tr').find('input[name="total_cost_' + rowNumber + '"]').val(totalBudget);
    }
    $(this).closest('tr').find('input[name="unit_cost_' + rowNumber + '"]').attr('readonly', false);
    $(this).closest('tr').find('input[name="quantity_' + rowNumber + '"]').attr('readonly', false);
});

$(document).on('keyup', '.quantity:not([readonly]), .unit_cost:not([readonly])', function () {
    var el = $(this);
    var rowNumber = $(this).closest('tr').attr('id');
    var quantity = 0;
    var unit = 0;
    var total = 0;

    if ($(el).hasClass('quantity')) {
        quantity = parseFloat($(this).val().replace(/,/g, ''));
        unit = parseFloat($('tr#' + rowNumber).find('input[name="unit_cost_' + rowNumber + '"]').val().replace(/,/g, ''));
    } else {
        unit = parseFloat($(this).val().replace(/,/g, ''));
        quantity = parseFloat($('tr#' + rowNumber).find('input[name="quantity_' + rowNumber + '"]').val().replace(/,/g, ''));
    }
    total = unit * quantity;
    if (isNaN(total)) {
        total = 0;
    }

    $('tr#' + rowNumber).find('input[name="total_cost_' + rowNumber + '"]').val(total.toLocaleString());

    computeTotalBudgetOfAllItems();

    totalCostValidation(el, total);
});

var totalCostValidation = function totalCostValidation(el, total) {
    var budgetAlloted = parseFloat($(el).closest('tr').find('select.description').find(':selected').data('budget').toLocaleString().replace(/,/g, ''));

    if (total > budgetAlloted) {
        $(el).closest('tr').find('.total-cost-container').addClass('form-has-error');
    } else {
        $(el).closest('tr').find('.total-cost-container').removeClass('form-has-error');
    }
};

$(document).on('keyup', '.unit_cost, .quantity, .subQuantity, .subUnitCost', function () {
    var errorCount = $('table#pr_table tr.pr-item td.total-cost-container.form-has-error').length;
    if (errorCount > 0) {
        $('#prSubmit').attr('disabled', true);
    } else {
        $('#prSubmit').attr('disabled', false);
    }
});

/***/ }),

/***/ 45:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "applyNumbers", function() { return applyNumbers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "reCountSubItemNo", function() { return reCountSubItemNo; });
$('input[name="purchase_type"]').on('change', function () {
    var purchaseType = $(this).val();
    applyNumbers(purchaseType);
});

var applyNumbers = function applyNumbers(purchaseType) {
    if (purchaseType === 'lot') {
        var i = 1;
        $('.parent_item_no').each(function () {
            $(this).val(i);
            i++;
        });
        $('.sub-item-no').each(function () {
            $(this).val('');
        });
    } else if (purchaseType === 'per') {
        var subItemCount = $('.sub-item-no').length;
        if (subItemCount > 0) {
            var _i = 1;
            $('.parent_item_no').each(function () {
                $(this).val('');
            });
            $('.sub-item-no').each(function () {
                $(this).val(_i);
                _i++;
            });
        } else {
            var _i2 = 1;
            $('.parent_item_no').each(function () {
                $(this).val(_i2);
                _i2++;
            });
            $('.sub-item-no').each(function () {
                $(this).val('');
            });
        }
    }
};

var reCountSubItemNo = function reCountSubItemNo() {
    var i = 1;
    $('.sub-item-no').each(function () {
        $(this).val(i);
        i++;
    });
};

/***/ })

/******/ });