/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 221);
/******/ })
/************************************************************************/
/******/ ({

/***/ 221:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(222);


/***/ }),

/***/ 222:
/***/ (function(module, exports) {

/**
 * 
 * Add new PR Row
 * 
 */
var row_counter = parseInt($('#pr_item_no_count').val());
var item_no_counter = parseInt($('#item_no_count').val());
var rc2 = row_counter;
var item_id;
$('table#pr_table').on('click', 'button.add-pr-row', function () {
	if (item_no_counter == 0) {
		item_id = 'I-0001';
	} else {
		var new_item_no_counter = parseInt(item_no_counter) + 1;
		if (new_item_no_counter.toString().length == 1) {
			item_id = 'I-000' + new_item_no_counter.toString();
		} else if (new_item_no_counter.toString().length == 2) {
			item_id = 'I-00' + new_item_no_counter.toString();
		} else if (new_item_no_counter.toString().length == 3) {
			item_id = 'I-0' + new_item_no_counter.toString();
		} else {
			item_id = 'I-' + new_item_no_counter.toString();
		}
	}

	item_no_counter += 1;
	$('#item_no_count').val(item_no_counter);
	rc2 += 1;
	var row = $('\n\t\t<tr>\n\t\t\t<td class="d-none"><input type="hidden" name="pr_item_rc2[]" value="x"></td>\n\t\t\t<td><input type="text" class="form-control form-control-sm item_no" name="item_no_' + rc2 + '" value="' + item_id + '"></td>\n\t\t\t<td><input type="text" class="form-control form-control-sm stock_no" name="stock_no_' + rc2 + '"></td>\n\t\t\t<td>\n\t\t\t\t<select class="form-control form-control-sm unit_no" name="unit_no_' + rc2 + '" id="unit_list" ></select>\n\t\t\t</td>\n\t\t\t<td>\n\t\t\t\t<select class="form-control form-control-sm description" name="description_' + rc2 + '" ></select>\n\t\t\t</td>\n\t\t\t<td><input type="text" class="form-control form-control-sm quantity" name="quantity_' + rc2 + '" ></td>\n\t\t\t<td><input type="text" class="form-control form-control-sm unit_cost" name="unit_cost_' + rc2 + '" ></td>\n\t\t\t<td><input type="text" class="form-control form-control-sm total_cost" name="total_cost_' + rc2 + '" ></td>\n\t\t\t<td><button type="button" class="btn btn-danger btn-sm remove-pr-row">-</button></td>\n\t\t</tr>\n\t');

	row.appendTo('table#pr_table tbody');

	var app_json_data = JSON.parse($('#app_json').val());

	var options = {
		data: app_json_data,
		list: {
			match: {
				enabled: true
			}
		},
		theme: "blue-light"
	};

	$(".description").easyAutocomplete(options);
});

/**
 * 
 * Remove PR Row
 * 
 */

$('table#pr_table').on('click', 'button.remove-pr-row', function () {
	rc2 -= 1;

	$(this).closest('tr').nextAll().find(".item_no").each(function () {
		var name = $(this).attr('name');
		var i = name.substring(8).slice(0);
		i -= 1;
		$(this).attr('name', 'item_no_' + i);
	});

	$(this).closest('tr').nextAll().find(".stock_no").each(function () {
		var name = $(this).attr('name');
		var i = name.substring(9).slice(0);
		i -= 1;
		$(this).attr('name', 'stock_no_' + i);
	});

	$(this).closest('tr').nextAll().find(".unit_no").each(function () {
		var name = $(this).attr('name');
		var i = name.substring(8).slice(0);
		i -= 1;
		$(this).attr('name', 'unit_no_' + i);
	});

	$(this).closest('tr').nextAll().find(".description").each(function () {
		var name = $(this).attr('name');
		var i = name.substring(12).slice(0);
		i -= 1;
		$(this).attr('name', 'description_' + i);
	});

	$(this).closest('tr').nextAll().find(".quantity").each(function () {
		var name = $(this).attr('name');
		var i = name.substring(9).slice(0);
		i -= 1;
		$(this).attr('name', 'quantity_' + i);
	});

	$(this).closest('tr').nextAll().find(".unit_cost").each(function () {
		var name = $(this).attr('name');
		var i = name.substring(10).slice(0);
		i -= 1;
		$(this).attr('name', 'unit_cost_' + i);
	});

	$(this).closest('tr').nextAll().find(".total_cost").each(function () {
		var name = $(this).attr('name');
		var i = name.substring(11).slice(0);
		i -= 1;
		$(this).attr('name', 'total_cost_' + i);
	});

	$(this).closest('tr').remove();

	item_no_counter -= 1;
	$('#item_no_count').val(item_no_counter);

	var total_estimate = 0;
	$('.total_cost').each(function () {
		total_estimate += Math.round(parseFloat($(this).val()) * 100) / 100;
	});

	$('#total_estimate').val(total_estimate);
});

/**
 * 
 * Disable keydown events on
 * several elements
 * 
 */

$(document).on('keydown', '#pr_no, #office_name, #sai_no, #rc_code, #pr_date, #end_user, .item_no, .total_cost, #approved_by,  #requested_by, input[name=cy], #mode_of_procurement, input[name="valid_from"], input[name="valid_to"]', function () {
	return false;
});

//allow numeric inputs only
$(document).on('keydown', '.quantity, .unit_cost', function (e) {
	if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true) || e.keyCode == 67 && (e.ctrlKey === true || e.metaKey === true) || e.keyCode == 88 && (e.ctrlKey === true || e.metaKey === true) || e.keyCode >= 35 && e.keyCode <= 39) {
		return;
	}
	if ((e.shiftKey || e.keyCode < 48 || e.keyCode > 57) && (e.keyCode < 96 || e.keyCode > 105)) {
		e.preventDefault();
	}
});

/**
 * 
 * PR Number Generator
 * 
 */
$(document).ready(function () {
	var pr_counter = $('#pr_count').val();

	var year = String(new Date().getFullYear());
	var month = String(new Date().getMonth() + 1);
	month = month < 9 ? "0" + month : month;
	var unique;

	if (pr_counter == 0) {
		unique = '0001';
	} else {
		var new_pr_count = parseInt(pr_counter) + 1;
		if (new_pr_count.toString().length == 1) {
			unique = '000' + parseInt(new_pr_count);
		} else if (new_pr_count.toString().length == 2) {
			unique = '00' + parseInt(new_pr_count);
		} else if (new_pr_count.toString().length == 3) {
			unique = '0' + parseInt(new_pr_count);
		} else {
			unique = parseInt(new_pr_count);
		}
	}

	var pr_no = year + month + '-' + unique;
	$('#pr_no').val(pr_no);
});

/**
 * 
 * Estimated Cost Total
 * Computation
 * 
 */

$(document).on('keyup', 'table#pr_table .quantity, table#pr_table .unit_cost', function () {
	var quantity = $(this).closest('tr').find('.quantity').val();
	var cost = $(this).closest('tr').find('.unit_cost').val();
	var total_cost = $(this).closest('tr').find('.total_cost');

	if (quantity != '' || cost != '') {
		var total = Math.round(parseFloat(quantity) * parseFloat(cost) * 100) / 100; //Math.round(float/double * 100) / 100 rounds of the result to 2 decimal places.
		if (!isNaN(total)) {
			$(total_cost).val(total);
		} else {
			$(total_cost).val('0');
		}
	}

	var total_estimate = 0;
	$(this).closest('tbody').find('.total_cost').each(function () {
		total_estimate += Math.round(parseFloat($(this).val()) * 100) / 100;
	});

	$('#total_estimate').val(total_estimate);
});

/**
 * 
 * Auto select unit and item description
 * 
 */
$(function () {
	var item_row_counter_onload = $('input[name="pr_item_row_counter[]"]').length;
	for (var y = 1; y <= item_row_counter_onload; y++) {
		var unit_value = $('#item_unit_' + y).val();
		$('#item_unit_' + y).closest('tr').find('select.unit_no').find('option[value="' + unit_value + '"]').attr('selected', true);

		var desc_value = $('#item_desc_' + y).val();
		$('#item_desc_' + y).closest('tr').find('select[name="description_' + y + '"]').find('option[value="' + desc_value + '"]').attr('selected', true);
	}
});

/***/ })

/******/ });