/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 228);
/******/ })
/************************************************************************/
/******/ ({

/***/ 228:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(229);


/***/ }),

/***/ 229:
/***/ (function(module, exports) {

$(function () {
	var year = new Date().getFullYear();
	$('#valid_from, #valid_to').datepicker({
		dateFormat: 'MM dd, yy',
		minDate: 0,
		maxDate: new Date(year, 11, 31)
	});

	for (i = new Date().getFullYear(); i <= new Date().getFullYear() + 10; i++) {
		$('#cy').append($('<option />').val(i).html(i));
	}

	var subItemCount = $('div.sub-item').length;

	if (subItemCount !== 0) {
		$('.add-pr-row').attr('disabled', true);
	}
});

//auto compute 3 months on validity
// $(function() {
// 	$('#valid_from, #valid_to').on('change', function(){
// 		let id = $(this).attr('id')
// 		let date = $(this).val()

// 		if(id === 'valid_from') {
// 			let validTo = new Date(date)
// 			validTo.setMonth(validTo.getMonth() + 3)
// 			$('#valid_to').datepicker('setDate', validTo)
// 		} else {
// 			let validFrom = new Date(date)
// 			validFrom.setMonth(validFrom.getMonth() - 3)
// 			$('#valid_from').datepicker('setDate', validFrom)
// 		}
// 	})
// })

/***/ })

/******/ });