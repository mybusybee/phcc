/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 234);
/******/ })
/************************************************************************/
/******/ ({

/***/ 234:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(45);


/***/ }),

/***/ 45:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "applyNumbers", function() { return applyNumbers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "reCountSubItemNo", function() { return reCountSubItemNo; });
$('input[name="purchase_type"]').on('change', function () {
    var purchaseType = $(this).val();
    applyNumbers(purchaseType);
});

var applyNumbers = function applyNumbers(purchaseType) {
    if (purchaseType === 'lot') {
        var i = 1;
        $('.parent_item_no').each(function () {
            $(this).val(i);
            i++;
        });
        $('.sub-item-no').each(function () {
            $(this).val('');
        });
    } else if (purchaseType === 'per') {
        var subItemCount = $('.sub-item-no').length;
        if (subItemCount > 0) {
            var _i = 1;
            $('.parent_item_no').each(function () {
                $(this).val('');
            });
            $('.sub-item-no').each(function () {
                $(this).val(_i);
                _i++;
            });
        } else {
            var _i2 = 1;
            $('.parent_item_no').each(function () {
                $(this).val(_i2);
                _i2++;
            });
            $('.sub-item-no').each(function () {
                $(this).val('');
            });
        }
    }
};

var reCountSubItemNo = function reCountSubItemNo() {
    var i = 1;
    $('.sub-item-no').each(function () {
        $(this).val(i);
        i++;
    });
};

/***/ })

/******/ });