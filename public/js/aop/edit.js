/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 203);
/******/ })
/************************************************************************/
/******/ ({

/***/ 203:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(204);


/***/ }),

/***/ 204:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__abstracts_total_cost_calculator__ = __webpack_require__(32);


$(function () {

    $('input[name="opening_date"]').datepicker({
        dateFormat: 'MM d, yy',
        minDate: 0
    });
});

//this will dynamically add a text input below the dropdown and remove it on change
$(document).on('change', '.document-select', function () {
    var document_type = $(this).val();
    var doc_container_id = $(this).closest('td').attr('id');
    var doc_container_id_tokens = doc_container_id.split("_");
    var doc_container_row_no = doc_container_id_tokens[doc_container_id_tokens.length - 1];

    if (document_type == 'f9') {
        var other_doc_input = "";

        other_doc_input = $('<input type="text" name="eligibility_docs_extra_' + doc_container_row_no + '" id="additional_doc" class="additional_doc form-control form-control-sm border border-info mt-2" style="width:95%; float:right">');

        other_doc_input.appendTo('#' + doc_container_id);
    } else {
        $(this).next('.additional_doc').remove();
    }
});

$(document).on('change', '.compliance-select', function () {
    // debugger
    var compliance_cell_id = $(this).attr('id');
    var compliance_cell_id_tokens = compliance_cell_id.split("_");
    var compliance_suffix = compliance_cell_id_tokens[compliance_cell_id_tokens.length - 1];

    var supplier_compliance_array = document.getElementsByClassName("compliance_" + compliance_suffix);

    var compliance = true;
    for (var sup_ctr = 0; sup_ctr < supplier_compliance_array.length; sup_ctr++) {
        var supplier_compliance = supplier_compliance_array[sup_ctr].value;
        if (supplier_compliance == "Not Comply") {
            compliance = false;
        }
    }

    if (compliance) {
        $('#result_compliance_' + compliance_suffix).val('COMPLIANT');
    } else {
        $('#result_compliance_' + compliance_suffix).val('NON-COMPLIANT');
    }

    Object(__WEBPACK_IMPORTED_MODULE_0__abstracts_total_cost_calculator__["updateCompliantSupplier"])();
});

/***/ }),

/***/ 32:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "rankSuppliers", function() { return rankSuppliers; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "updateCompliantSupplier", function() { return updateCompliantSupplier; });
var supplier_id = 0;
var purchase_type = $('.purchase_type').val();

$(function () {
	$('.abstract-form').submit(function () {
		$('select.reco_item_nos').each(function () {
			var options = $(this).find("option");

			$(options).each(function () {
				$(this).attr("disabled", false);
			});
		});
	});
});

$(document).on('keyup', '.pr-item-unit-cost', function () {

	var supplier_unit_cost_str = $(this).val();
	var supplier_unit_cost = parseFloat(supplier_unit_cost_str);
	var supplier_unit_cost_id = $(this).attr('id');
	var supplier_unit_cost_id_tokens = supplier_unit_cost_id.split("_");
	var item_id = supplier_unit_cost_id_tokens[supplier_unit_cost_id_tokens.length - 1];
	var item_quantity = parseFloat($("#item_quantity_" + item_id).text());
	var supplier_total_cost = supplier_unit_cost * item_quantity;

	var supplier_id_tokens = supplier_unit_cost_id.split("unit_");
	var total_cost_id = "#total_" + supplier_id_tokens[1];

	if (supplier_unit_cost_str === 'n/a') {
		$(total_cost_id).val('n/a');
	} else if (supplier_unit_cost_str === 'no bid') {
		$(total_cost_id).val('no bid');
	} else if (supplier_unit_cost_str == '') {
		$(total_cost_id).val('0.00');
	} else {
		if (supplier_total_cost != null) {
			$(total_cost_id).val(supplier_total_cost);
		} else {
			$(total_cost_id).val('0.00');
		}

		//make border red for a visual indicator
		if (purchase_type == "per item") {
			var purchase_total_cost = parseFloat($("#item_cost_total_" + item_id).text());

			if (supplier_total_cost > purchase_total_cost) {
				$(total_cost_id).parent().addClass('with-error');
			} else {
				$(total_cost_id).parent().removeClass('with-error');
			}
		}
	}

	//pr-item-total-cost
	supplier_id = supplier_unit_cost_id_tokens[3];
});

$(document).on('keyup', '.pr-sub-item-unit-cost', function () {
	// debugger
	var supplier_unit_cost_str = $(this).val();
	var supplier_unit_cost = parseFloat(supplier_unit_cost_str);
	var supplier_unit_cost_id = $(this).attr('id');
	var supplier_unit_cost_id_tokens = supplier_unit_cost_id.split("_");
	var sub_item_id = supplier_unit_cost_id_tokens[supplier_unit_cost_id_tokens.length - 1];
	var item_quantity = parseFloat($("#sub_item_quantity_" + sub_item_id).text());
	var supplier_total_cost = supplier_unit_cost * item_quantity;

	var supplier_id_tokens = supplier_unit_cost_id.split("sub_unit_");
	var total_cost_id = "#sub_total_" + supplier_id_tokens[1];

	if (supplier_unit_cost_str === 'n/a') {
		$(total_cost_id).val('n/a');
	} else if (supplier_unit_cost_str === 'no bid') {
		$(total_cost_id).val('no bid');
	} else if (supplier_unit_cost_str == '') {
		$(total_cost_id).val('0.00');
	} else {
		if (supplier_total_cost != null) {
			$(total_cost_id).val(supplier_total_cost);
		} else {
			$(total_cost_id).val('0.00');
		}

		//make border red for a visual indicator
		if (purchase_type == "per item") {
			var purchase_total_cost = parseFloat($("#sub_item_cost_total_" + sub_item_id).text().replace(/,/g, ''));
			if (supplier_total_cost > purchase_total_cost) {
				$(total_cost_id).parent().addClass('with-error');
			} else {
				$(total_cost_id).parent().removeClass('with-error');
			}
		}
	}

	//pr-item-total-cost
	supplier_id = supplier_unit_cost_id_tokens[4];
});

$(document).on('change', '.pr-item-unit-cost', function () {
	if (purchase_type == 'per lot') {
		var item_ids = document.getElementsByClassName("item_ids");
		var amount_total = 0;
		for (var item_ctr = 0; item_ctr < item_ids.length; item_ctr++) {
			var item_id = item_ids[item_ctr].innerHTML;

			var purchase_total_cost = parseFloat($("#item_cost_total_" + item_id).text());
			var item_total_cost_str = $("#total_cost_supplier_" + supplier_id + "_pr_item_" + item_id).val();

			if (item_total_cost_str !== 'n/a') {
				if (item_total_cost_str !== 'no bid') {
					var item_total_cost = parseFloat(item_total_cost_str);

					amount_total += item_total_cost;
				} else {
					amount_total = 'no bid';
					break;
				}
			} else {
				amount_total = 'n/a';
				break;
			}
		}
		$("#total_cost_total_supplier" + supplier_id).val(amount_total);
		rankSuppliers();
	} else if (purchase_type == 'per item') {
		var item_cell_id = $(this).attr('id');
		var item_cell_id_tokens = item_cell_id.split('_');
		var pr_id = item_cell_id_tokens[item_cell_id_tokens.length - 1];
		//debugger
		rankSuppliers(pr_id);
	}
});

$(document).on('change', '.pr-sub-item-unit-cost', function () {
	if (purchase_type == 'per lot') {
		var sub_item_ids = document.getElementsByClassName("sub_item_ids");
		var amount_total = 0;
		for (var sub_item_ctr = 0; sub_item_ctr < sub_item_ids.length; sub_item_ctr++) {
			var sub_item_id = sub_item_ids[sub_item_ctr].innerHTML;

			var purchase_total_cost = parseFloat($("#sub_item_cost_total_" + sub_item_id).text().replace(/,/g, ''));
			var sub_item_total_cost_str = $("#sub_total_cost_supplier_" + supplier_id + "_pr_sub_item_" + sub_item_id).val();

			if (sub_item_total_cost_str !== 'n/a') {
				if (sub_item_total_cost_str !== 'no bid') {
					var item_total_cost = parseFloat(sub_item_total_cost_str);

					amount_total += item_total_cost;
				} else {
					amount_total = 'no bid';
					break;
				}
			} else {
				amount_total = 'n/a';
				break;
			}
		}
		$("#total_cost_total_supplier" + supplier_id).val(amount_total);
		rankSuppliers();
	} else if (purchase_type == 'per item') {
		var item_cell_id = $(this).attr('id');
		var item_cell_id_tokens = item_cell_id.split('_');
		var pr_id = item_cell_id_tokens[item_cell_id_tokens.length - 1];
		// debugger
		rankSuppliers(pr_id);
	}
});

var rankSuppliers = function rankSuppliers(pr_id) {
	var supplier_amount_arr = [];

	if (purchase_type == "per lot" && pr_id === undefined) {

		var total_estimate = $('#total_estimate').text();
		total_estimate = parseFloat(total_estimate.slice(1, total_estimate.length).replace(/,/g, ''));

		var supplier_amounts = document.getElementsByClassName("total_cost_total_suppliers");

		for (var amt_ctr = 0; amt_ctr < supplier_amounts.length; amt_ctr++) {
			var supplier_column_id = ".total_cost_total_supplier" + amt_ctr;
			// let amount_value_str = $(supplier_column_id).text();
			var amount_value_str = $(supplier_column_id).val();
			var amount_value = parseFloat(amount_value_str);

			if (amount_value_str != 'n/a') {
				if (amount_value_str != 'no bid') {
					if (!isNaN(amount_value)) {
						if (amount_value < total_estimate && amount_value > 0 || amount_value == total_estimate) {
							supplier_amount_arr.push({
								supplier_col_index: amt_ctr,
								amount: amount_value
							});
						} else if (amount_value > total_estimate) {
							$("#supplier_ranking" + amt_ctr).val("Bid Over ABC");
						}
					}
				} else {
					$("#supplier_ranking" + amt_ctr).val("NO BID");
				}
			} else {
				$("#supplier_ranking" + amt_ctr).val("N/A");
			}
		}

		supplier_amount_arr.sort(function (a, b) {
			return a.amount - b.amount;
		});
		//debugger

		var final_ranking_arr = [];
		for (var sorted_ctr = 0; sorted_ctr < supplier_amount_arr.length; sorted_ctr++) {
			if (sorted_ctr == 0) {
				final_ranking_arr.push({
					rank: 1,
					supplier_col_index: supplier_amount_arr[sorted_ctr].supplier_col_index,
					amount: supplier_amount_arr[sorted_ctr].amount
				});
			} else {
				var base_entry = final_ranking_arr[sorted_ctr - 1];
				var base_amount = base_entry.amount;

				if (supplier_amount_arr[sorted_ctr].amount == base_amount) {
					final_ranking_arr.push({
						rank: base_entry.rank,
						supplier_col_index: supplier_amount_arr[sorted_ctr].supplier_col_index,
						amount: supplier_amount_arr[sorted_ctr].amount
					});
				} else {
					final_ranking_arr.push({
						rank: base_entry.rank + 1,
						supplier_col_index: supplier_amount_arr[sorted_ctr].supplier_col_index,
						amount: supplier_amount_arr[sorted_ctr].amount
					});
				}
			}
		}

		for (var final_arr_ctr = 0; final_arr_ctr < final_ranking_arr.length; final_arr_ctr++) {
			$("#supplier_ranking" + final_ranking_arr[final_arr_ctr].supplier_col_index).val(final_ranking_arr[final_arr_ctr].rank);
		}
	} else if (purchase_type == "per item" && pr_id !== undefined) {
		var sub_item_count = $('#sub_item_count').val();
		if (sub_item_count == undefined) sub_item_count = "0";

		var _total_estimate = $('#item_cost_total_' + pr_id).text();
		// debugger
		if (sub_item_count != "0") _total_estimate = $('#sub_item_cost_total_' + pr_id).text();
		// total_estimate = parseFloat(total_estimate.slice(1, total_estimate.length).replace(/,/g, ''));
		_total_estimate = parseFloat(_total_estimate.replace(/,/g, ''));

		var row_total_costs = document.getElementsByClassName('pr-item_' + pr_id + '_total-cost');
		if (sub_item_count != "0") row_total_costs = document.getElementsByClassName('pr-sub-item_' + pr_id + '_total-cost');

		for (var item_ctr = 0; item_ctr < row_total_costs.length; item_ctr++) {
			var total_supplier_cost = row_total_costs[item_ctr].value;
			var _amount_value = parseFloat(total_supplier_cost);

			var total_supplier_cost_id = row_total_costs[item_ctr].id;
			var total_supplier_cost_tokens = total_supplier_cost_id.split('_');
			var item_supplier_id = total_supplier_cost_tokens[3];
			if (sub_item_count != "0") item_supplier_id = total_supplier_cost_tokens[4];

			if (total_supplier_cost != 'n/a') {
				if (total_supplier_cost != 'no bid') {
					if (total_supplier_cost != '0') {
						if (!isNaN(_amount_value)) {
							if (_amount_value < _total_estimate && _amount_value > 0 || _amount_value == _total_estimate) {
								supplier_amount_arr.push({
									supplier_col_index: item_ctr,
									supplier_id: item_supplier_id,
									amount: _amount_value
								});
							} else if (_amount_value > _total_estimate) {
								$("#ranking_supplier_" + item_supplier_id + "_pr_item_" + pr_id + "_col_" + item_ctr).val("Bid Over ABC");
								if (sub_item_count != "0") $("#ranking_supplier_" + item_supplier_id + "_pr_sub_item_" + pr_id + "_col_" + item_ctr).val("Bid Over ABC");
							}
						}
					} else {
						$("#ranking_supplier_" + item_supplier_id + "_pr_item_" + pr_id + "_col_" + item_ctr).val("FREE");
						if (sub_item_count != "0") $("#ranking_supplier_" + item_supplier_id + "_pr_sub_item_" + pr_id + "_col_" + item_ctr).val("FREE");
					}
				} else {
					$("#ranking_supplier_" + item_supplier_id + "_pr_item_" + pr_id + "_col_" + item_ctr).val("NO BID");
					if (sub_item_count != "0") $("#ranking_supplier_" + item_supplier_id + "_pr_sub_item_" + pr_id + "_col_" + item_ctr).val("NO BID");
				}
			} else {
				$("#ranking_supplier_" + item_supplier_id + "_pr_item_" + pr_id + "_col_" + item_ctr).val("N/A");
				if (sub_item_count != "0") $("#ranking_supplier_" + item_supplier_id + "_pr_sub_item_" + pr_id + "_col_" + item_ctr).val("N/A");
			}
		}

		supplier_amount_arr.sort(function (a, b) {
			return a.amount - b.amount;
		});

		var _final_ranking_arr = [];
		for (var _sorted_ctr = 0; _sorted_ctr < supplier_amount_arr.length; _sorted_ctr++) {
			if (_sorted_ctr == 0) {
				_final_ranking_arr.push({
					rank: 1,
					supplier_id: supplier_amount_arr[_sorted_ctr].supplier_id,
					supplier_col_index: supplier_amount_arr[_sorted_ctr].supplier_col_index,
					amount: supplier_amount_arr[_sorted_ctr].amount
				});
			} else {
				var _base_entry = _final_ranking_arr[_sorted_ctr - 1];
				var _base_amount = _base_entry.amount;

				if (supplier_amount_arr[_sorted_ctr].amount == _base_amount) {
					_final_ranking_arr.push({
						rank: _base_entry.rank,
						supplier_id: supplier_amount_arr[_sorted_ctr].supplier_id,
						supplier_col_index: supplier_amount_arr[_sorted_ctr].supplier_col_index,
						amount: supplier_amount_arr[_sorted_ctr].amount
					});
				} else {
					_final_ranking_arr.push({
						rank: _base_entry.rank + 1,
						supplier_id: supplier_amount_arr[_sorted_ctr].supplier_id,
						supplier_col_index: supplier_amount_arr[_sorted_ctr].supplier_col_index,
						amount: supplier_amount_arr[_sorted_ctr].amount
					});
				}
			}
		}

		for (var _final_arr_ctr = 0; _final_arr_ctr < _final_ranking_arr.length; _final_arr_ctr++) {
			$("#ranking_supplier_" + _final_ranking_arr[_final_arr_ctr].supplier_id + "_pr_item_" + pr_id + "_col_" + _final_ranking_arr[_final_arr_ctr].supplier_col_index).val(_final_ranking_arr[_final_arr_ctr].rank);
			if (sub_item_count != "0") $("#ranking_supplier_" + _final_ranking_arr[_final_arr_ctr].supplier_id + "_pr_sub_item_" + pr_id + "_col_" + _final_ranking_arr[_final_arr_ctr].supplier_col_index).val(_final_ranking_arr[_final_arr_ctr].rank);
		}

		// for (let sorted_ctr = 0; sorted_ctr < supplier_amount_arr.length; sorted_ctr++){
		// 	$("#ranking_supplier_" + supplier_amount_arr[sorted_ctr].supplier_id +"_pr_item_" + pr_id + "_col_" + supplier_amount_arr[sorted_ctr].supplier_col_index).val(sorted_ctr + 1);
		// 	if (sub_item_count != "0")
		// 		$("#ranking_supplier_"+supplier_amount_arr[sorted_ctr].supplier_id+"_pr_sub_item_"+pr_id+"_col_"+supplier_amount_arr[sorted_ctr].supplier_col_index).val(sorted_ctr + 1);
		// }
	}

	updateCompliantSupplier();
};

var updateCompliantSupplier = function updateCompliantSupplier() {
	//get the based supplier options from vue state
	var current_suppliers = $('select#reco_supplier option').clone();

	//clear the new dropdown of old options
	var updated_supplier_select = $('select#reco_supplier_updated option').remove();

	//get the abstract type
	var abstract_type = $('#abstract_type').val();

	//loop through the based suppliers
	for (var ctr = 0; ctr < current_suppliers.length; ctr++) {
		var sup_val = current_suppliers[ctr].value;
		var sup_compliance = $('.result_compliance_supplier_id_' + sup_val).val();

		if (purchase_type == "per lot") {
			var ranking = $('#supplier_ranking' + ctr).val();

			if (abstract_type == 'aob') {
				if (sup_compliance === 'PASSED' && ranking !== 'Bid Over ABC' && ranking !== 'N/A' && ranking !== 'NO BID') {
					$('select#reco_supplier_updated').append(current_suppliers[ctr]);
				}
			} else if (abstract_type == 'aop') {
				//debugger
				var tech_proposal_res = $('#tech_proposal_res_' + ctr).val();
				if (sup_compliance === 'COMPLIANT' && ranking !== 'Bid Over ABC' && ranking !== 'N/A' && ranking !== 'NO BID' && tech_proposal_res == "PASSED") {
					$('select#reco_supplier_updated').append(current_suppliers[ctr]);
				}
			} else {
				if (sup_compliance === 'COMPLIANT' && ranking !== 'Bid Over ABC' && ranking !== 'N/A' && ranking !== 'NO BID') {
					$('select#reco_supplier_updated').append(current_suppliers[ctr]);
				}
			}
		} else if (purchase_type == "per item") {
			// debugger
			if (abstract_type == 'aob') {
				if (sup_compliance === 'PASSED') {
					$('select#reco_supplier_updated').append(current_suppliers[ctr]);
				}
			} else if (abstract_type == 'aop') {
				//debugger
				var _tech_proposal_res = $('#tech_proposal_res_' + ctr).val();
				if (sup_compliance === 'COMPLIANT' && _tech_proposal_res == "PASSED") {
					$('select#reco_supplier_updated').append(current_suppliers[ctr]);
				}
			} else {
				if (sup_compliance === 'COMPLIANT') {
					$('select#reco_supplier_updated').append(current_suppliers[ctr]);
				}
			}
		}
	}
};

/***/ })

/******/ });