$('tr.clickable-row').click(function () {
	// var url = $(this).attr('data-href');
	// window.location.href = url;
});

$(function(){
	$('.total_estimated_budget').mask('Z##,###,###,###,###.00', {
		reverse: true,
		translation: {
			'Z': {
				pattern: /[1-9]/
			},
			'Y': {
				pattern: /[0-9]/
			}
		}
	});
})

$(function() {
	for (i = new Date().getFullYear(); i <= new Date().getFullYear() + 10; i++)
	{
		$('.uploadForYear').append($('<option />').val(i).html(i));
	}

	$('.uploadForYear > option:first').attr('disabled', true);
})

$(`select[name="prepared_by"]`).select2({
	dropdownParent: $("#uploadModal"),
	width: "100%"
});