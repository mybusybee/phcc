/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 247);
/******/ })
/************************************************************************/
/******/ ({

/***/ 247:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(248);


/***/ }),

/***/ 248:
/***/ (function(module, exports) {

$(function () {
    $('#suppliers-datatable').DataTable({
        colReorder: true,
        responsive: true,
        "lengthMenu": [[5, 10, 20, 50, -1], [5, 10, 20, 50, 'All']],
        'order': [],
        columnDefs: [{
            targets: [0],
            orderable: false
        }]
    });

    $('input[name="submission_date_time"]').datetimepicker({
        format: 'F d, Y h:i A',
        step: 30,
        formatTime: 'h:i A',
        minDate: 0,
        validateOnBlur: false
    });

    /**
    * 
    * RFQ Number Generator
    * 
    */
    var rfq_counter = $('#rfq_co').val();

    var year = String(new Date().getFullYear());
    var month = String(new Date().getMonth() + 1);
    month = month <= 9 ? "0" + month : month;
    var unique;

    if (rfq_counter == 0) {
        unique = '0001';
    } else {
        var new_rfq_count = parseInt(rfq_counter) + 1;
        if (new_rfq_count.toString().length == 1) {
            unique = '000' + parseInt(new_rfq_count);
        } else if (new_rfq_count.toString().length == 2) {
            unique = '00' + parseInt(new_rfq_count);
        } else if (new_rfq_count.toString().length == 3) {
            unique = '0' + parseInt(new_rfq_count);
        } else {
            unique = parseInt(new_rfq_count);
        }
    }

    var rfq_no = year + month + '-' + unique;

    $('#rfq_no').val(rfq_no);
});

$('.sup_id').on('change', function () {
    var id = $(this).val();

    if ($(this).is(':checked')) {
        var row = $('\n            <input type="hidden" class="form-control form-control-sm" value="' + id + '" name="supplier_id[]">\n        ');

        row.appendTo('.sup_ids_container');
    } else {
        $('div.sup_ids_container').find('input[value="' + id + '"]').remove();
    }
});

$(document).on('click', '.add_new_file', function () {
    var prFilesCounter = $('.rfq-files').length;

    if (prFilesCounter < 5) {
        var newFileRow = $('\n            <div class="input-group">\n                <input type="file" name="invite_requirements[]" class="form-control rfq-files" accept=".pdf">\n                <div class="input-group-append">\n                    <button class="btn btn-danger remove_file_row" type="button"><i class="fa fa-trash"></i></button>\n                </div>\n            </div>\n            ');

        newFileRow.appendTo('div#files_section');
    } else {
        alert('Attached files must not be more than 5!');
    }
});

$(document).on('click', '.remove_file_row', function () {
    $(this).closest('div.input-group').remove();
});

$('#delivery').on('click', function () {
    if ($(this).is(':checked')) {
        $('#deliveryPlace').attr('readonly', false);
        console.log('checked');
    } else {
        $('#deliveryPlace').attr('readonly', true);
        console.log('not checked');
    }
});

$('#paymentTerms').on('click', function () {
    if ($(this).is(':checked')) {
        $('input[name="payment_terms"]').attr('readonly', false);
    } else {
        $('input[name="payment_terms"]').attr('readonly', true);
    }
});

/***/ })

/******/ });