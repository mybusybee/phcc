$('tr.clickable-row').click(function () {
	var url = $(this).attr('data-href');
	window.location.href = url;
});

$('table.approved_ppmps').on('click', '.generate_app_cb', function(){
	var selected_year = $(this).parent().parent().find('.year-cell').text();

	if($(this).is(':checked')){
		$(this).closest('table.approved_ppmps').find('.year-cell').each(function(){
			var ppmp_year = $(this).text()
			if(ppmp_year != selected_year) {
				$(this).parent().hide();	
			}
		});
	} else {
		if($('table.approved_ppmps input[type=checkbox]:checked').length == 0) {
			$('table.approved_ppmps').find('tr').each(function(){
				$(this).show();
			});
		}
	}

});